using UnityEngine;

public interface IChordSelectionCellUIValuesProvider
{
    Color GetCellBackgroundColor();

    Color GetCellSelectedBackgroundColor();

    Color GetCellSelectedTextColor();

    Color GetCellTextColor();

    Color GetCellAddedTextColor();
}