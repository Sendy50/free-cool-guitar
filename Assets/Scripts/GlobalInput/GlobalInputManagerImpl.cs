using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GlobalInput
{
    public class GlobalInputManagerImpl : MonoBehaviour, GlobalInputManager
    {
        public enum Priority
        {
            Quit,
            Pause
        }

        private Coroutine _handleKeyCodeEscapeCoroutine;

        private Dictionary<Priority, List<OnBackPressedListener>> _onBackPressedListeners;

        private void Awake()
        {
            _onBackPressedListeners = new Dictionary<Priority, List<OnBackPressedListener>>();
            _onBackPressedListeners[Priority.Quit] = new List<OnBackPressedListener>();
            _onBackPressedListeners[Priority.Pause] = new List<OnBackPressedListener>();
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                if (_handleKeyCodeEscapeCoroutine != null) StopCoroutine(_handleKeyCodeEscapeCoroutine);
                _handleKeyCodeEscapeCoroutine = StartCoroutine(HandleKeyCodeEscapeJob());
            }
        }

        public bool AddOnBackPressedListeners(OnBackPressedListener listener, Priority priority)
        {
            if (listener == null || _onBackPressedListeners[priority].Contains(listener)) return false;
            _onBackPressedListeners[priority].Add(listener);
            return true;
        }

        public bool RemoveOnBackPressedListeners(OnBackPressedListener listener)
        {
            foreach (var key in _onBackPressedListeners.Keys)
                if (_onBackPressedListeners[key].Remove(listener))
                    return true;
            return false;
        }

        private IEnumerator HandleKeyCodeEscapeJob()
        {
            yield return new WaitForSeconds(0.25f);
            if (!NotifyListenersByPriority(Priority.Pause)) NotifyListenersByPriority(Priority.Quit);
        }

        private bool NotifyListenersByPriority(Priority priority)
        {
            var list = _onBackPressedListeners[priority];
            foreach (var item in list)
                if (item.OnBackPressed())
                    return true;
            return false;
        }
    }
}