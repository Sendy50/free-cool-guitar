namespace GlobalInput
{
    public interface OnBackPressedListener
    {
        bool OnBackPressed();
    }
}