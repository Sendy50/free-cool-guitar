namespace GlobalInput
{
    public interface GlobalInputManager
    {
        bool AddOnBackPressedListeners(OnBackPressedListener listener, GlobalInputManagerImpl.Priority priority);

        bool RemoveOnBackPressedListeners(OnBackPressedListener listener);
    }
}