using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[RequireComponent(typeof(ScrollRect))]
[AddComponentMenu("Layout/Extensions/Intro Carousel Horizontal Scroll Snap")]
public class IntroCarouselScrollSnap : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler,
    IEventSystemHandler
{
    private const float kTransitionDuration = 0.4f;

    public ScrollRect ScrollRect;

    [Tooltip("The screen / page to start the control on\n*Note, this is a 0 indexed array")] [SerializeField]
    public int StartingScreen;

    [Tooltip("The distance between two pages based on page height, by default pages are next to each other")]
    [SerializeField]
    [Range(0f, 8f)]
    public float PageStep = 1f;

    [Tooltip(
        "The visible bounds area, controls which items are visible/enabled. *Note Should use a RectMask. (optional)")]
    public RectTransform MaskArea;

    [Tooltip("Pixel size to buffer arround Mask Area. (optional)")]
    public float MaskBuffer = 1f;

    [Tooltip(
        "By default the container will lerp to the start when enabled in the scene, this option overrides this and forces it to simply jump without lerping")]
    public bool JumpOnEnable;

    [Tooltip(
        "(Experimental)\nBy default, child array objects will use the parent transform\nHowever you can disable this for some interesting effects")]
    public bool UseParentTransform = true;

    [Tooltip(
        "Scroll Snap children. (optional)\nEither place objects in the scene as children OR\nPrefabs in this array, NOT BOTH")]
    public GameObject[] ChildObjects;

    [SerializeField] [Tooltip("Event fires when a user starts to change the selection")]
    private CarouselTransitionStartEvent _carouselTransitionStartEvent = new CarouselTransitionStartEvent();

    [SerializeField] [Tooltip("Event fires when the page settles after a user has dragged")]
    private CarouselTransitionEndEvent _carouseltransitionEndEvent = new CarouselTransitionEndEvent();

    private bool _animatingPageChange;

    private int _bottomItem;

    private Vector2 _childAnchorPoint;

    private float _childPos;

    private float _childSize;

    private int _currentPageIndex;

    private int _halfNoVisibleItems;

    private bool _ignoreDrag;

    private float _maskSize;

    private Rect _panelDimensions;

    private int _screens = 1;

    private RectTransform _screensContainer;

    private ScrollRect _scroll_rect;

    private float _scrollStartPosition;

    private int _topItem;

    private Coroutine _transitionCoroutine;

    public int CurrentPageIndex
    {
        get => _currentPageIndex;
        internal set
        {
            if (value == _currentPageIndex)
            {
                PreviousPageIndex = _currentPageIndex;
            }
            else if (value != _currentPageIndex && value >= 0 && value < _screensContainer.childCount ||
                     value == 0 && _screensContainer.childCount == 0)
            {
                PreviousPageIndex = _currentPageIndex;
                _currentPageIndex = value;
                if ((bool) MaskArea) UpdateVisible();
                if (!_animatingPageChange) ScreenChange();
            }
        }
    }

    public int PreviousPageIndex { get; private set; }

    public CarouselTransitionStartEvent OnCarouselTransitionStartEvent
    {
        get => _carouselTransitionStartEvent;
        set => _carouselTransitionStartEvent = value;
    }

    public CarouselTransitionEndEvent OnCarouselTransitionEndEvent
    {
        get => _carouseltransitionEndEvent;
        set => _carouseltransitionEndEvent = value;
    }

    private void Awake()
    {
        if (_scroll_rect == null) _scroll_rect = gameObject.GetComponent<ScrollRect>();
        _panelDimensions = gameObject.GetComponent<RectTransform>().rect;
        if (StartingScreen < 0) StartingScreen = 0;
        _screensContainer = _scroll_rect.content;
        InitialiseChildObjectsFromScene();
    }

    private void Start()
    {
        _childAnchorPoint = new Vector2(0f, 0.5f);
        _currentPageIndex = StartingScreen;
        _panelDimensions = gameObject.GetComponent<RectTransform>().rect;
        UpdateLayout();
    }

    private void OnEnable()
    {
        InitialiseChildObjectsFromScene();
        DistributePages();
        if ((bool) MaskArea) UpdateVisible();
        if (JumpOnEnable) SetScrollContainerPosition();
    }

    private void OnRectTransformDimensionsChange()
    {
        if (_childAnchorPoint != Vector2.zero) UpdateLayout();
    }

    private void OnValidate()
    {
        if (_scroll_rect == null) _scroll_rect = GetComponent<ScrollRect>();
        var childCount = gameObject.GetComponent<ScrollRect>().content.childCount;
        if (childCount != 0 || ChildObjects != null)
        {
            var num = ChildObjects != null && ChildObjects.Length != 0 ? ChildObjects.Length : childCount;
            if (StartingScreen > num - 1) StartingScreen = num - 1;
            if (StartingScreen < 0) StartingScreen = 0;
        }

        if (MaskBuffer <= 0f) MaskBuffer = 1f;
        if (PageStep < 0f) PageStep = 0f;
        if (PageStep > 8f) PageStep = 9f;
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        if (!_animatingPageChange)
        {
            ScrollRect.horizontal = true;
            _ignoreDrag = false;
        }
    }

    public void OnDrag(PointerEventData eventData)
    {
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        if (!_animatingPageChange && !_ignoreDrag) TransitionToClosestPage();
    }

    private void StartTransitionCoroutine(float destinationPositionX)
    {
        if (_transitionCoroutine == null)
            _transitionCoroutine = StartCoroutine(TransitionRoutine(destinationPositionX));
    }

    private void StopTransitionCoroutine()
    {
        if (_transitionCoroutine != null)
        {
            StopCoroutine(_transitionCoroutine);
            _transitionCoroutine = null;
        }
    }

    private IEnumerator TransitionRoutine(float destinationPositionX)
    {
        var destination = new Vector3(destinationPositionX, _screensContainer.localPosition.y,
            _screensContainer.localPosition.z);
        var time = 0f;
        while (time <= 0.4f)
        {
            _screensContainer.localPosition = Vector3.Lerp(_screensContainer.localPosition, destination, time / 0.4f);
            time += Time.deltaTime;
            yield return null;
        }

        var pos = _screensContainer.localPosition;
        pos.x = destinationPositionX;
        _screensContainer.localPosition = pos;
        EndScreenChange();
    }

    private void InitialiseChildObjectsFromScene()
    {
        var childCount = _screensContainer.childCount;
        ChildObjects = new GameObject[childCount];
        for (var i = 0; i < childCount; i++)
        {
            ChildObjects[i] = _screensContainer.transform.GetChild(i).gameObject;
            if ((bool) MaskArea && ChildObjects[i].activeSelf) ChildObjects[i].SetActive(false);
        }
    }

    private void UpdateVisible()
    {
        if (!MaskArea || ChildObjects == null || ChildObjects.Length < 1 || _screensContainer.childCount < 1) return;
        _maskSize = MaskArea.rect.width;
        _halfNoVisibleItems =
            (int) Math.Round(_maskSize / (_childSize * MaskBuffer), MidpointRounding.AwayFromZero) / 2;
        _bottomItem = _topItem = 0;
        for (var num = _halfNoVisibleItems + 1; num > 0; num--)
        {
            _bottomItem = _currentPageIndex - num >= 0 ? num : 0;
            if (_bottomItem > 0) break;
        }

        for (var num2 = _halfNoVisibleItems + 1; num2 > 0; num2--)
        {
            _topItem = _screensContainer.childCount - _currentPageIndex - num2 >= 0 ? num2 : 0;
            if (_topItem > 0) break;
        }

        for (var i = CurrentPageIndex - _bottomItem; i < CurrentPageIndex + _topItem; i++)
            try
            {
                ChildObjects[i].SetActive(true);
            }
            catch
            {
                Debug.Log("Failed to setactive child [" + i + "]");
            }

        if (_currentPageIndex > _halfNoVisibleItems) ChildObjects[CurrentPageIndex - _bottomItem].SetActive(false);
        if (_screensContainer.childCount - _currentPageIndex > _topItem)
            ChildObjects[CurrentPageIndex + _topItem].SetActive(false);
    }

    private void DistributePages()
    {
        _panelDimensions = gameObject.GetComponent<RectTransform>().rect;
        _screens = _screensContainer.childCount;
        _scroll_rect.horizontalNormalizedPosition = 0f;
        var num = 0;
        var num2 = 0f;
        var num3 = 0f;
        var num4 = _childSize = (int) _panelDimensions.width * (PageStep != 0f ? PageStep : 3f);
        for (var i = 0; i < _screensContainer.transform.childCount; i++)
        {
            var component = _screensContainer.transform.GetChild(i).gameObject.GetComponent<RectTransform>();
            num3 = num + (int) (i * num4);
            component.sizeDelta = new Vector2(_panelDimensions.width, _panelDimensions.height);
            component.anchoredPosition = new Vector2(num3, 0f);
            var vector = component.pivot = _childAnchorPoint;
            vector = component.anchorMin = component.anchorMax = vector;
        }

        num2 = num3 + num * -1;
        _screensContainer.offsetMax = new Vector2(num2, 0f);
    }

    private void SetScrollContainerPosition()
    {
        _scrollStartPosition = _screensContainer.localPosition.x;
        _scroll_rect.horizontalNormalizedPosition = _currentPageIndex / (float) (_screens - 1);
    }

    public void CancelDrag()
    {
        _ignoreDrag = true;
    }

    public GameObject GetChildObjectAtIndex(int index)
    {
        if (index < 0 || index > ChildObjects.Length - 1) return null;
        return ChildObjects[index];
    }

    public bool IsAnimating()
    {
        return _animatingPageChange;
    }

    public void UpdateLayout()
    {
        _animatingPageChange = false;
        DistributePages();
        if ((bool) MaskArea) UpdateVisible();
        SetScrollContainerPosition();
    }

    public void AddChild(GameObject GO)
    {
        AddChild(GO, false);
    }

    public void AddChild(GameObject GO, bool WorldPositionStays)
    {
        _scroll_rect.horizontalNormalizedPosition = 0f;
        GO.transform.SetParent(_screensContainer, WorldPositionStays);
        InitialiseChildObjectsFromScene();
        DistributePages();
        if ((bool) MaskArea) UpdateVisible();
        SetScrollContainerPosition();
    }

    public void TransitionToNextScreen()
    {
        if (_currentPageIndex < _screens - 1)
        {
            CurrentPageIndex = _currentPageIndex + 1;
            if (!_animatingPageChange) StartScreenChange();
            _animatingPageChange = true;
            var xPositionforPage = GetXPositionforPage(_currentPageIndex);
            StartTransitionCoroutine(xPositionforPage);
            ScreenChange();
        }
    }

    public void TransitionToPreviousScreen()
    {
        if (_currentPageIndex > 0)
        {
            CurrentPageIndex = _currentPageIndex - 1;
            if (!_animatingPageChange) StartScreenChange();
            _animatingPageChange = true;
            var xPositionforPage = GetXPositionforPage(_currentPageIndex);
            StartTransitionCoroutine(xPositionforPage);
            ScreenChange();
        }
    }

    private void TransitionToClosestPage()
    {
        var pageforPosition = GetPageforPosition(_screensContainer.localPosition);
        if (pageforPosition >= 0 && pageforPosition <= _screens - 1)
        {
            CurrentPageIndex = pageforPosition;
            if (!_animatingPageChange) StartScreenChange();
            _animatingPageChange = true;
            var xPositionforPage = GetXPositionforPage(_currentPageIndex);
            StartTransitionCoroutine(xPositionforPage);
            ScreenChange();
        }
    }

    private int GetPageforPosition(Vector3 pos)
    {
        return (int) Math.Round((_scrollStartPosition - pos.x) / _childSize);
    }

    private float GetXPositionforPage(int page)
    {
        _childPos = (0f - _childSize) * page;
        return _childPos + _scrollStartPosition;
    }

    public void StartScreenChange()
    {
        OnCarouselTransitionStartEvent.Invoke();
    }

    private void ScreenChange()
    {
        ScrollRect.horizontal = false;
    }

    private void EndScreenChange()
    {
        OnCarouselTransitionEndEvent.Invoke(_currentPageIndex);
        StopTransitionCoroutine();
        _animatingPageChange = false;
    }

    [Serializable]
    public class CarouselTransitionStartEvent : UnityEvent
    {
    }

    [Serializable]
    public class CarouselTransitionEndEvent : UnityEvent<int>
    {
    }
}