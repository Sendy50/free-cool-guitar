using System;
using System.Collections.Generic;

public class Pool<T>
{
    private readonly List<T> _all = new List<T>();
    private readonly List<T> _available = new List<T>();

    private readonly List<T> _inUse = new List<T>();

    public Pool(int capacity, Func<T> alloc, bool enableAllReference = true)
    {
        for (var i = 0; i < capacity; i++)
        {
            var item = alloc();
            _available.Add(item);
        }

        if (enableAllReference)
        {
            _all.AddRange(_available);
        }
    }

    public List<T> Available()
    {
        return _available;
    }

    public List<T> InUse()
    {
        return _inUse;
    }

    public List<T> All()
    {
        return _all;
    }

    public T GetObject()
    {
        lock (_available)
        {
            if (_available.Count != 0)
            {
                var val = _available[0];
                _inUse.Add(val);
                _available.RemoveAt(0);
                return val;
            }

            throw new Exception("Pool: out of available capacity");
        }
    }

    public void ReleaseObject(T candidate, Action<T> cleanUp = null)
    {
        cleanUp?.Invoke(candidate);
        lock (_available)
        {
            _available.Add(candidate);
            _inUse.Remove(candidate);
        }
    }

    public void ReleaseAll(Action<T> cleanUp = null)
    {
        if (cleanUp != null)
        {
            _inUse.ForEach(delegate(T c) { cleanUp(c); });
        }

        _available.AddRange(_inUse);
        _inUse.Clear();
    }
}