using System;
using UnityEngine;
using UnityEngine.UI;

public class IntroCarouselPager : MonoBehaviour
{
    [SerializeField] private Image[] Dots;

    [SerializeField] private Color NotSelectedColor;

    [SerializeField] private Color SelectedColor;

    private void Awake()
    {
        OnPageIndexChanged(0);
    }

    public void OnPageIndexChanged(int newPageIndex)
    {
        var num = Math.Min(Math.Max(0, newPageIndex), Dots.Length - 1);
        var dots = Dots;
        foreach (var image in dots) image.color = NotSelectedColor;
        Dots[num].color = SelectedColor;
    }
}