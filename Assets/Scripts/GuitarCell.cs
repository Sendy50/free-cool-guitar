using System;
using JetBrains.Annotations;
using Store;
using UnityEngine;
using UnityEngine.UI;

public class GuitarCell : MonoBehaviour
{
    private const string FreeStatusText = "FREE";

    private const string LockStatusText = "PREMIUM";

    private const string UnlockStatusText = "UNLOCKED";

    [SerializeField] private Sprite _defaultImageBackground;

    [SerializeField] private Sprite _selectedImageBackground;

  

    [SerializeField] private Image _roundedBackground;

    [SerializeField] private Text _guitarStateText;

    [SerializeField] private Image _unlockedIcon;

    [SerializeField] private Image _premiumIcon;

    [SerializeField] private Image _watchAdIcon;

    [SerializeField] private GameObject _titleContainerSelected;

    [SerializeField] private GameObject _titleContainerNotSelected;

    [SerializeField] private RectTransform _guitarStatusRectTransform;

    [SerializeField] private Guitar _guitarModel;

    private InAppManager _inAppManager;

    private bool _isSelectedState;

    private void Awake()
    {
        // _inAppManager = ApplicationGraph.GetInappManager();
        UpdateGuitarCell();
    }

    private void OnEnable()
    {
        // _inAppManager.OnInAppsLockedDelegate += UpdateGuitarCell;
        // _inAppManager.OnInAppsUnlockedDelegate += UpdateGuitarCell;
    }

    private void OnDisable()
    {
        // _inAppManager.OnInAppsLockedDelegate -= UpdateGuitarCell;
        // _inAppManager.OnInAppsUnlockedDelegate -= UpdateGuitarCell;
    }

    public event Action<IGuitar> OnGuitarCellClicked;

    public void UpdateGuitarCell(string inapId = null)
    {
        SetIsSelectedState(_isSelectedState);
        // if (_guitarModel.IsPremium)
        //     if (IAPManager.Instance.IsPrimeMember())
        //     {
        //     	SetGuitarState("UNLOCKED", GuitarCellIcon.Unlocked);
        //     }
        //     else
        //     {
        //     SetGuitarState("PREMIUM", GuitarCellIcon.Premium);
        // }
        // else
            SetGuitarState("FREE", GuitarCellIcon.Unlocked);
    }

    private void SetIsSelectedState(bool isSelected)
    {
        if (isSelected)
        {
            _roundedBackground.sprite = _selectedImageBackground;
            // _titleContainerSelected.SetActive(true);
            // _titleContainerNotSelected.SetActive(false);
        }
        else
        {
            _roundedBackground.sprite = _defaultImageBackground;
            // _titleContainerSelected.SetActive(false);
            // _titleContainerNotSelected.SetActive(true);
            
            // if (_guitarModel.IsPremium)
            //     if (IAPManager.Instance.IsPrimeMember())
            //     {
            //         //_titleContainerNotSelected.GetComponent<Image>().sprite = unlockedSprite;
            //     }
            //     else
            //     {
            //        // _titleContainerNotSelected.GetComponent<Image>().sprite = premiumSprite;
            //     }
            // else
            //     //_titleContainerNotSelected.GetComponent<Image>().sprite = freeSprite;
        }
    }

    private void SetGuitarState(string text, GuitarCellIcon icon)
    {
        _guitarStateText.text = text;
        switch (icon)
        {
            case GuitarCellIcon.Unlocked:
                _unlockedIcon.gameObject.SetActive(true);
                _premiumIcon.gameObject.SetActive(false);
                _watchAdIcon.gameObject.SetActive(false);
                break;
            case GuitarCellIcon.Premium:
                _unlockedIcon.gameObject.SetActive(false);
                _premiumIcon.gameObject.SetActive(true);
                _watchAdIcon.gameObject.SetActive(false);
                break;
            case GuitarCellIcon.WatchRewardAd:
                _unlockedIcon.gameObject.SetActive(false);
                _premiumIcon.gameObject.SetActive(false);
                _watchAdIcon.gameObject.SetActive(true);
                break;
            default:
                throw new ArgumentException("This GuitarCellIcon isn't managed : " + icon);
        }

        LayoutRebuilder.ForceRebuildLayoutImmediate(_guitarStatusRectTransform);
    }

    [UsedImplicitly]
    public void OnGuitarClicked()
    {
        _roundedBackground.sprite = _selectedImageBackground;
        _titleContainerSelected.SetActive(true);
        _titleContainerNotSelected.SetActive(false);
        if (OnGuitarCellClicked != null) OnGuitarCellClicked(_guitarModel);
    }

    public IGuitar GetGuitarModel()
    {
        return _guitarModel;
    }

    public void SetSelectedGuitar(bool isSelectedState)
    {
        if (isSelectedState != _isSelectedState)
        {
            _isSelectedState = isSelectedState;
        }
        if (isActiveAndEnabled) UpdateGuitarCell();
    }
}