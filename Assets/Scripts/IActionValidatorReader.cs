public interface IActionValidatorReader
{
    IUserActionValidator currentActionValidator { get; }

    IUserActionValidator[] actionValidators { get; }
}