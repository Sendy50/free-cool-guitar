using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class SkinableButton : MonoBehaviour
{
    [SerializeField] public ImageSkinTarget normalTarget;

    [SerializeField] public ImageSkinTarget highlightedTarget;

    [SerializeField] public GuitarHolder guitarHolder;

    private Button _button;

    private SkinImageUpdator _highlightedImageUpdator;

    private SkinImageUpdator _normalImageUpdator;

    private void Start()
    {
        _button = GetComponent<Button>();
        _normalImageUpdator = new SkinImageUpdator();
        _normalImageUpdator.target = normalTarget;
        _normalImageUpdator.SkinableSetup(guitarHolder, UpdateNormalImage);
        _highlightedImageUpdator = new SkinImageUpdator();
        _highlightedImageUpdator.target = highlightedTarget;
        _highlightedImageUpdator.SkinableSetup(guitarHolder, UpdateHighlightedImage);
    }

    private void UpdateNormalImage(Sprite sprite)
    {
        // Debug.LogError("_button null : " + (_button == null) );
        // _button.image.sprite = sprite;
    }

    private void UpdateHighlightedImage(Sprite sprite)
    {
        // SpriteState spriteState = default;
        // spriteState.highlightedSprite = sprite;
        // spriteState.pressedSprite = sprite;
        // spriteState.disabledSprite = sprite;
        // _button.spriteState = spriteState;
    }
}