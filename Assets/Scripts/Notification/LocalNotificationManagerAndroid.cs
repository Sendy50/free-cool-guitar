using System;
using MWM.GameEvent;
using UnityEngine;
using Utils;

namespace Notification
{
    public class LocalNotificationManagerAndroid : LocalNotificationManager
    {
        private readonly GameEventSender _gameEventSender;

        public LocalNotificationManagerAndroid(GameEventSender gameEventSender)
        {
            Precondition.CheckNotNull(gameEventSender);
            _gameEventSender = gameEventSender;
        }

        public void RegisterForNotifications()
        {
            Debug.LogError("LocalNotification.RegisterForNotifications isn't implemented yet");
        }

        public void ScheduleNotification(string id, string title, string message, DateTime schedule, string songPath)
        {
            Debug.LogError("LocalNotification.ScheduleNotification isn't implemented yet");
        }

        public void CancelAllNotifications()
        {
            Debug.LogError("LocalNotification.CancelAllNotifications isn't implemented yet");
        }

        public void CheckReceivedLocalNotification()
        {
            Debug.LogError("LocalNotification.CheckReceivedLocalNotification isn't implemented yet");
        }
    }
}