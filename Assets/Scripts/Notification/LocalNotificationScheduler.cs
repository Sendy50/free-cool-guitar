namespace Notification
{
    public interface LocalNotificationScheduler
    {
        void ScheduleRetentionNotifications();
    }
}