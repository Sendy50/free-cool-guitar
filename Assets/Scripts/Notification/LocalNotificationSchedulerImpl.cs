using System;
using i18n;
using Utils;

namespace Notification
{
    public class LocalNotificationSchedulerImpl : LocalNotificationScheduler
    {
        private const string SongNotificationPath = "AllMusic/notification_sound.mp3";

        private const string RetentionD1Id = "D1_RETENTION";

        private const string RetentionD1TitleKey = "local_notification_d1_title";

        private const string RetentionD1ContentKey = "local_notification_d1_content";

        private const string RetentionD3Id = "D3_RETENTION";

        private const string RetentionD3TitleKey = "local_notification_d3_title";

        private const string RetentionD3ContentKey = "local_notification_d3_content";

        private const string RetentionD7Id = "D7_RETENTION";

        private const string RetentionD7TitleKey = "local_notification_d7_title";

        private const string RetentionD7ContentKey = "local_notification_d7_content";

        private readonly LocalizationManager _localizationManager;

        private readonly LocalNotificationManager _localNotificationManager;

        public LocalNotificationSchedulerImpl(LocalNotificationManager localNotificationManager,
            LocalizationManager localizationManager)
        {
            Precondition.CheckNotNull(localNotificationManager);
            Precondition.CheckNotNull(localizationManager);
            _localNotificationManager = localNotificationManager;
            _localizationManager = localizationManager;
        }

        public void ScheduleRetentionNotifications()
        {
            _localNotificationManager.CancelAllNotifications();
            var now = DateTime.Now;
            var dateTime = now.AddHours(-now.Hour).AddMinutes(-now.Minute).AddSeconds(-now.Second);
            var schedule = dateTime.AddDays(1.0).AddHours(17.0);
            var schedule2 = dateTime.AddDays(3.0).AddHours(17.0);
            var schedule3 = dateTime.AddDays(7.0).AddHours(17.0);
            _localNotificationManager.ScheduleNotification("D1_RETENTION",
                _localizationManager.GetLocalizedValue("local_notification_d1_title"),
                _localizationManager.GetLocalizedValue("local_notification_d1_content"), schedule,
                "AllMusic/notification_sound.mp3");
            _localNotificationManager.ScheduleNotification("D3_RETENTION",
                _localizationManager.GetLocalizedValue("local_notification_d3_title"),
                _localizationManager.GetLocalizedValue("local_notification_d3_content"), schedule2,
                "AllMusic/notification_sound.mp3");
            _localNotificationManager.ScheduleNotification("D7_RETENTION",
                _localizationManager.GetLocalizedValue("local_notification_d7_title"),
                _localizationManager.GetLocalizedValue("local_notification_d7_content"), schedule3,
                "AllMusic/notification_sound.mp3");
        }
    }
}