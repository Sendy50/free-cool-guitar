using System;

namespace Notification
{
    public interface LocalNotificationManager
    {
        void RegisterForNotifications();

        void ScheduleNotification(string id, string title, string message, DateTime schedule, string songPath = null);

        void CancelAllNotifications();

        void CheckReceivedLocalNotification();
    }
}