using System.Collections;
using GuitarApplication;
using UnityEngine;

public class GodMode : MonoBehaviour
{
    public float strumInterleaceBetweenNoteTime = 0.01f;
    private ChordsStrings _chordsStrings;

    private Coroutine _routine;

    private void Start()
    {
        _chordsStrings = ApplicationGraph.GetApplicationRouter().GetChordsStrings();
    }

    public void StartGodMode(MusicSheetReader musicSheetReader)
    {
        if (_routine == null) _routine = StartCoroutine(Routine(musicSheetReader));
    }

    public void StopGodMode()
    {
        if (_routine != null)
        {
            StopCoroutine(_routine);
            _routine = null;
        }
    }

    private IEnumerator Routine(MusicSheetReader musicSheetReader)
    {
        var progression = musicSheetReader.GameProgression;
        var musicSheet = musicSheetReader.MusicSheet;
        var userActions = musicSheet.UserActions;
        foreach (var action in userActions)
        {
            var triggerTime = action.time;
            yield return new WaitWhile(() => triggerTime >= progression.CurrentTime);
            yield return PlayAction(action);
        }

        _routine = null;
    }

    private IEnumerator PlayAction(IUserAction action)
    {
        if (action is StrockUserAction) yield return PlayStrokAction(action as StrockUserAction);
        if (action is StrumUserAction) yield return PlayStrumAction(action as StrumUserAction);
    }

    private IEnumerator PlayStrokAction(StrockUserAction action)
    {
        var stringIndex = action.note.StringIndex;
        _chordsStrings.HandleStringTouched(_chordsStrings.guitarStrings[stringIndex], stringIndex);
        yield break;
    }

    private IEnumerator PlayStrumAction(StrumUserAction action)
    {
        var wait = new WaitForSeconds(strumInterleaceBetweenNoteTime);
        var notes = action.notes;
        foreach (var note in notes)
        {
            var index = note.StringIndex;
            _chordsStrings.HandleStringTouched(_chordsStrings.guitarStrings[index], index);
            yield return wait;
        }
    }
}