using GlobalInput;

namespace QuitDialog
{
    public class QuitDialogManagerImpl : QuitDialogManager, OnBackPressedListener
    {
        private readonly GlobalInputManager _globalInputManager;

        private bool _popupDisplayed;

        //private MNPopup _popup;

        public QuitDialogManagerImpl(GlobalInputManager globalInputManager)
        {
            _globalInputManager = globalInputManager;
            _globalInputManager.AddOnBackPressedListeners(this, GlobalInputManagerImpl.Priority.Quit);
            // _popup = new MNPopup("Guitar", "You are about to quit the game");
            // _popup.AddAction("Quit", delegate
            // {
            // 	Application.Quit();
            // 	_popupDisplayed = false;
            // });
            // _popup.AddAction("Cancel", delegate
            // {
            // 	_popupDisplayed = false;
            // });
        }

        public bool OnBackPressed()
        {
            //_popup.Show();
            return true;
        }

        public void Destroy()
        {
            _globalInputManager.RemoveOnBackPressedListeners(this);
        }
    }
}