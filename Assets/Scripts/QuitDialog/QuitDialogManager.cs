namespace QuitDialog
{
    public interface QuitDialogManager
    {
        void Destroy();
    }
}