using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class StrumValidator : IUserActionValidator
{
    private Coroutine _detectionCoroutine;
    private readonly List<int> _stringIndexesDetected;

    private readonly List<int[]> _stringIndexesPatternDetectable;

    public StrumValidator(MonoBehaviour monoBehaviour)
    {
        this.monoBehaviour = monoBehaviour;
        _stringIndexesDetected = new List<int>();
        _stringIndexesPatternDetectable = new List<int[]>();
        validationStatus = UserActionValidationStatus.unstarted;
    }

    public int[] stringIndexesToDetect { get; private set; }

    public MonoBehaviour monoBehaviour { get; }

    public int minStringForValidate { get; private set; }

    public UserActionValidationStatus validationStatus { get; private set; }

    public GameTimeInterleave interleave { get; private set; }

    public void StartDetection()
    {
        if (interleave.IsTooLate())
        {
            validationStatus = UserActionValidationStatus.unvalidated;
        }
        else if (_detectionCoroutine == null)
        {
            validationStatus = UserActionValidationStatus.detecting;
            _detectionCoroutine = monoBehaviour.StartCoroutine(DetectionRoutine(interleave));
        }
    }

    public UserActionValidationTouchStringResponse OnStringTouched(int stringIndex)
    {
        if (!interleave.IsOnInterleave() || validationStatus == UserActionValidationStatus.unstarted)
            return UserActionValidationTouchStringResponse.notOnTiming;
        if (!_stringIndexesPatternDetectable.Any(c => c.Contains(stringIndex)))
            return UserActionValidationTouchStringResponse.stringIsNotValide;
        _stringIndexesDetected.Add(stringIndex);
        return UserActionValidationTouchStringResponse.stringIsValide;
    }

    public void SetupDetection(int[] stringIndexesToDetect, GameTimeInterleave interleave,
        int minStringForValidate = -1)
    {
        this.interleave = interleave;
        _stringIndexesDetected.Clear();
        _stringIndexesPatternDetectable.Clear();
        if (minStringForValidate == -1) minStringForValidate = stringIndexesToDetect.Length;
        if (minStringForValidate > stringIndexesToDetect.Length) minStringForValidate = stringIndexesToDetect.Length;
        this.minStringForValidate = minStringForValidate;
        this.stringIndexesToDetect = stringIndexesToDetect;
        GenerateDetectablePatterns(_stringIndexesPatternDetectable, stringIndexesToDetect, minStringForValidate);
    }

    public void StopDetection()
    {
        if (_detectionCoroutine != null)
        {
            monoBehaviour.StopCoroutine(_detectionCoroutine);
            _detectionCoroutine = null;
        }
    }

    private IEnumerator DetectionRoutine(GameTimeInterleave interleave)
    {
        yield return new WaitUntil(() => interleave.IsOnInterleave());
        while (interleave.IsOnInterleave())
        {
            yield return null;
            foreach (var item in _stringIndexesPatternDetectable)
                if (CheckPatternInEnumerable(_stringIndexesDetected, item))
                {
                    validationStatus = UserActionValidationStatus.validated;
                    yield break;
                }
        }

        validationStatus = UserActionValidationStatus.unvalidated;
        _detectionCoroutine = null;
    }

    private static bool CheckPatternInEnumerable(IEnumerable<int> enumerable, int[] pattern)
    {
        var num = 0;
        foreach (var item in enumerable)
        {
            num = item == pattern[num] ? num + 1 : 0;
            if (num == pattern.Length) return true;
        }

        return false;
    }

    private static void GenerateDetectablePatterns(List<int[]> io, int[] pattern, int lengthMin)
    {
        if (pattern.Length <= lengthMin)
        {
            io.Add(pattern);
            return;
        }

        var list = new List<int>();
        var num = pattern.Length - lengthMin + 1;
        for (var i = 0; i < num; i++)
        {
            for (var j = i; j < i + lengthMin; j++) list.Add(pattern[j]);
            io.Add(list.ToArray());
            list.Clear();
        }
    }
}