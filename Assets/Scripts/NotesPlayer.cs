using System.Collections;
using AdditionalContent;
using GuitarApplication;
using MWM.Audio;
using Song;
using UnityEngine;

public class NotesPlayer : MonoBehaviour
{
    public delegate void OnMusicPlayToEnd();

    private Player _player;

    private void Start()
    {
        _player = ApplicationGraph.GetMusicGamingAudioEngine().GetMultiPlayerUnit().GetPlayer(0);
    }

    public event OnMusicPlayToEnd OnMusicPlayToEndDelegate;

    public IEnumerator StartPlayingRoutine(MusicSheetReader musicSheetReader, ISong song, bool skipLoadMusic)
    {
        var finished = false;
        if (song.GetType() == typeof(ExternalSong))
            _player.LoadExternal(song.GetAccompanimentName(), delegate
            {
                StartCoroutine(MusicPlayerStartPlayRoutine(_player, musicSheetReader.GameProgression));
                finished = true;
            });
        else
            _player.LoadInternal("AllMusic/" + song.GetAccompanimentName(), delegate
            {
                StartCoroutine(MusicPlayerStartPlayRoutine(_player, musicSheetReader.GameProgression));
                finished = true;
            });
        yield return new WaitUntil(() => finished);
    }

    public void Pause()
    {
        _player.EndOfFileReached -= OnPlayerReachedEOF;
        _player.Pause();
    }

    public void Resume()
    {
        _player.Play();
        _player.EndOfFileReached += OnPlayerReachedEOF;
    }

    public void StopPlaying()
    {
        _player.Stop();
        _player.EndOfFileReached -= OnPlayerReachedEOF;
    }

    public bool GetIsPlaying()
    {
        return _player.IsPlaying();
    }

    private IEnumerator MusicPlayerStartPlayRoutine(Player player, GameProgression progression)
    {
        yield return new WaitWhile(() => (double) progression.CurrentTime < -0.0032);
        player.Play();
        player.EndOfFileReached += OnPlayerReachedEOF;
    }

    private void OnPlayerReachedEOF(Player player)
    {
        if (OnMusicPlayToEndDelegate != null) OnMusicPlayToEndDelegate();
    }
}