namespace Store
{
    public interface StoreView
    {
        void SetVisible(bool visible);

        void DisplaySubscription(string freeTrialPeriod, string price);
    }
}