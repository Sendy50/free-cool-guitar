using System;
using System.Runtime.Serialization;
using UnityEngine;

namespace Store
{
    [Serializable]
    public class InappTransactionEvent
    {
        [OptionalField] public string transaction_identifier;

        public string product_identifier;

        public string ToJSONString()
        {
            return JsonUtility.ToJson(this);
        }
    }
}