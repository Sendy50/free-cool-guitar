using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Store
{
    [RequireComponent(typeof(TextMeshProUGUI))]
    public class URLStoreManager : MonoBehaviour, IPointerClickHandler, IEventSystemHandler
    {
        public Camera camera;

        private TextMeshProUGUI text;

        private void Awake()
        {
            text = GetComponent<TextMeshProUGUI>();
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            var num = TMP_TextUtilities.FindIntersectingLink(text, Input.mousePosition, camera);
            if (num != -1)
            {
                var tMP_LinkInfo = text.textInfo.linkInfo[num];
                switch (tMP_LinkInfo.GetLinkID())
                {
                    case "link1":
                        Application.OpenURL("https://musicworldmedia.com/products/guitar/tos");
                        break;
                    case "link2":
                        Application.OpenURL("https://musicworldmedia.com/products/guitar/tos");
                        break;
                    case "link3":
                        Application.OpenURL(
                            "https://storage.googleapis.com/learning-guitar.appspot.com/subscriptions/terms_of_service/android/android_tos_subscriptions_guitar.html");
                        break;
                }
            }
        }
    }
}