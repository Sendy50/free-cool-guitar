using System;
using System.Collections.Generic;
using MWM.GameEvent;
using UnityEngine;
using UnityEngine.Purchasing;
using Utils;
using EventType = MWM.GameEvent.EventType;

namespace Store
{
    public class InAppManagerExtensionIOS : InAppManagerExtension
    {
        private IExtensionProvider _extensions;

        private Action<bool> _onRestoreCompletedCallback;

        private List<Payment> _pendingIndirectPayments;
        private Payment _pendingInitialisationPayment;

        public void Restore(Action<bool> callback)
        {
            if (_onRestoreCompletedCallback != null)
            {
                callback(false);
            }

            // _extensions.GetExtension<IAppleExtensions>().RestoreTransactions(delegate(bool success)
            // {
            // 	if (!success)
            // 	{
            // 		callback(obj: false);
            // 	}
            // 	else if (_pendingIndirectPayments.Count == 0)
            // 	{
            // 		callback(obj: true);
            // 	}
            // 	else
            // 	{
            // 		_onRestoreCompletedCallback = callback;
            // 	}
            // });
        }

        public void RestoreCompleted(bool isSuccess)
        {
            if (_onRestoreCompletedCallback != null)
            {
                _onRestoreCompletedCallback(true);
                _onRestoreCompletedCallback = null;
            }
        }

        public IReceiptValidator Validator(MonoBehaviour monoBehaviour, string endAppId, string endAppKey)
        {
            var installationId = GameEventSender.Instance().InstallationId;
            return new MWMReceiptValidator(monoBehaviour, endAppId, endAppKey, installationId, Device.Default(),
                string.Empty, GetVerifyReceiptURLType());
        }

        public void OnPromotionalPurchase(Product item)
        {
            // _extensions.GetExtension<IAppleExtensions>().ContinuePromotionalPurchases();
        }

        public void SendTransactionEvents(string productId, string transactionId)
        {
            var receiptEvent = new ReceiptEvent();
            // receiptEvent.receipt = InAppReceiptVerificator.AppStoreReceipt;
            var receiptEvent2 = receiptEvent;
            GameEventSender.Instance().SendEvent(EventType.InappReceipt, receiptEvent2.ToJSONString());
        }

        public string GetTransactionId(PurchaseEventArgs e)
        {
            return e.purchasedProduct.transactionID;
        }

        public void SetPendingIndirectPayments(List<Payment> pendingIndirectPayments)
        {
            _pendingIndirectPayments = pendingIndirectPayments;
        }

        public void SetExtensions(IExtensionProvider extensions)
        {
            _extensions = extensions;
        }

        private MWMReceiptValidator.VerifyReceiptURLType GetVerifyReceiptURLType()
        {
            return MWMReceiptValidator.VerifyReceiptURLType.production;
        }
    }
}