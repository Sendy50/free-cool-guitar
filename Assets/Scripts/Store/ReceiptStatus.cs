namespace Store
{
    public enum ReceiptStatus
    {
        Unknown = -2,
        None = -1,
        Valid = 0,
        JsonNotReadable = 21000,
        MalformedOrMissingData = 21002,
        ReceiptCouldNotBeAuthenticated = 21003,
        SecretNotMatching = 21004,
        ReceiptServerUnavailable = 21005,
        SubscriptionExpired = 21006,
        TestReceipt = 21007,
        ProductionEnvironment = 21008
    }
}