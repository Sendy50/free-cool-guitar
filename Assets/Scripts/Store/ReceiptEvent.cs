using System;
using UnityEngine;

namespace Store
{
    [Serializable]
    public class ReceiptEvent
    {
        public string receipt;

        public string ToJSONString()
        {
            return JsonUtility.ToJson(this);
        }
    }
}