namespace Store
{
    public class InAppData
    {
        public readonly string CurrencyCode;

        public readonly float Price;
        public readonly string ProductId;

        public InAppData(string productId, string currencyCode, float price)
        {
            ProductId = productId;
            CurrencyCode = currencyCode;
            Price = price;
        }
    }
}