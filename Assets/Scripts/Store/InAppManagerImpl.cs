using System;
using System.Collections.Generic;
using System.Linq;
using Config;
using Devices;
using JetBrains.Annotations;
using MWM.GameEvent;
using Store.Android;
using UnityEngine;
using UnityEngine.Purchasing;
using Utils;

namespace Store
{
    public class InAppManagerImpl : InAppManager, IStoreListener
    {
        private const string EndAppId = "d18e79f1-feca-4a87-8b37-a8c194949018";

        private const string EndAppKey = "3af26ba3-4083-44e2-bc59-49bf7abc2042";

        private readonly ConfigStorageContainer.ConfigStorage _configStoragePersistent;

        private readonly DeviceManager _deviceManager;

        private readonly InAppManagerExtension _inAppManagerExtension;

        private readonly InAppReceiptVerificator _inAppReceiptVerificator;

        private readonly MonoBehaviour _monoBehaviour;

        private readonly List<Payment> _pendingIndirectPayments;

        private readonly InAppStorage _storage;

        private readonly List<Payment> _userProcessingPayments;

        private IStoreController _controller;

        private List<string> _premiumPassIds;

        private Dictionary<string, ProductDetails> _productDetails;

        private Coroutine _scheduleIndirectPaymentsProcessingRoutine;

        private Status _status;

        public InAppManagerImpl(MonoBehaviour monoBehaviour,
            ConfigStorageContainer.ConfigStorage configStoragePersistent, DeviceManager deviceManager)
        {
            Precondition.CheckNotNull(monoBehaviour);
            Precondition.CheckNotNull(configStoragePersistent);
            Precondition.CheckNotNull(deviceManager);
            _monoBehaviour = monoBehaviour;
            _configStoragePersistent = configStoragePersistent;
            _deviceManager = deviceManager;
            _inAppReceiptVerificator = new InAppReceiptVerificator();
            _userProcessingPayments = new List<Payment>();
            _pendingIndirectPayments = new List<Payment>();
            _storage = InAppStorage.LoadFromFile();
            _inAppManagerExtension = CreateInAppManagerExtension();
            _inAppManagerExtension.SetPendingIndirectPayments(_pendingIndirectPayments);
            _productDetails = new Dictionary<string, ProductDetails>();
            Initialize();
        }

        public event OnInAppsUnlocked OnInAppsUnlockedDelegate;

        public event OnInAppsLocked OnInAppsLockedDelegate;

        public event OnInAppInitialized OnInAppInitializedDelegate;

        public bool IsSubscribe()
        {
            var config = _configStoragePersistent.GetConfig();
            if (config != null && config.HasWonSubscriptionValue) return true;
            return _storage.UnlockedIaPs.Any(c => _premiumPassIds.Contains(c));
        }

        public Status GetStatus()
        {
            return _status;
        }

        [CanBeNull]
        public ProductDetails ProductDetailsForId(string sku)
        {
            var product = ProductForId(sku);
            var subscriptionAbTestSkuPrefix =
                InAppConst.GetSubscriptionAbTestSkuPrefix(_deviceManager.GetDeviceTarget());
            ProductMetadata productMetadata;
            if (_deviceManager.GetRuntimeType() == RuntimeType.Editor)
            {
                var text = sku.Substring(subscriptionAbTestSkuPrefix.Length);
                var skuWithoutPrefixSplit = text.Split('.');
                var text2 = ExtractEditorPrice(skuWithoutPrefixSplit, _premiumPassIds.Contains(sku));
                var localizedPrice = decimal.Parse(text2);
                var priceString = text2 + "$";
                productMetadata = new ProductMetadata(priceString, string.Empty, string.Empty, "USD", localizedPrice);
            }
            else
            {
                productMetadata = product.metadata;
            }

            return new ProductDetails(sku, productMetadata.localizedPriceString, productMetadata.localizedPrice,
                productMetadata.isoCurrencyCode);
        }

        public void Restore(Action<bool> callback)
        {
            _inAppManagerExtension.Restore(callback);
        }

        public void VerifySubscription()
        {
        }

        [CanBeNull]
        public InAppData Buy(string productId, Action<Payment.Result> callback = null)
        {
            return Buy(new Payment(productId, callback));
        }

        public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
        {
            _controller = controller;
            _inAppManagerExtension.SetExtensions(extensions);
            if (_deviceManager.GetRuntimeType() == RuntimeType.Android)
            {
                var list = new List<GooglePlaySubInfos>();
                var all = controller.products.all;
                foreach (var product in all)
                    if (!string.IsNullOrEmpty(product.receipt))
                    {
                        var googlePlaySubInfosFromJsonString =
                            GooglePlaySubInfos.GetGooglePlaySubInfosFromJsonString(product.receipt);
                        list.Add(googlePlaySubInfosFromJsonString);
                        _inAppManagerExtension.SendTransactionEvents(googlePlaySubInfosFromJsonString.Sku,
                            googlePlaySubInfosFromJsonString.PurchaseToken);
                    }

                if (list.Count > 0)
                {
                    var mWMGooglePlaySubscriptionVerificator =
                        CreateMwmGooglePlaySubscriptionVerificator(_monoBehaviour);
                    mWMGooglePlaySubscriptionVerificator.VerifySubscription(list, AndroidVerificatorHandler);
                }
                else
                {
                    LockAllProducts();
                }
            }

            _status = Status.Initialized;
            if (OnInAppInitializedDelegate != null) OnInAppInitializedDelegate();
        }

        public void OnInitializeFailed(InitializationFailureReason error)
        {
            _status = Status.InitializationFailed;
        }

        public void OnPurchaseFailed(Product i, PurchaseFailureReason p)
        {
            try
            {
                var item = _userProcessingPayments.First(c => i.definition.id == c.ProductId);
                _userProcessingPayments.Remove(item);
                item.OnFailure();
            }
            catch
            {
            }
        }

        public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs e)
        {
            try
            {
                var payment = _userProcessingPayments.First(c => e.purchasedProduct.definition.id == c.ProductId);
                payment.TransactionId = _inAppManagerExtension.GetTransactionId(e);
                return ProcessUserInitiatedPurchase(payment);
            }
            catch
            {
                return PurchaseProcessingResult.Complete;
            }
        }
        //
        // [DllImport("__Internal")]
        // private static extern void unityPurchasingFinishTransaction(string productJson, string transactionId);

        private void Initialize()
        {
            _status = Status.Initializing;
            var deviceTarget = _deviceManager.GetDeviceTarget();
            // ConfigurationBuilder configurationBuilder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());
            _premiumPassIds = new List<string>
            {
                "googleplay.com.mwm.guitar.weeklypremiumsub",
                "googleplay.com.mwm.guitar.annuallypremiumsub"
            };
            _premiumPassIds.AddRange(InAppConst.GetSubscriptionAbTestSkus(deviceTarget));
            // configurationBuilder.AddProducts(new ProductDefinition[11]
            // {
            // 	new ProductDefinition("googleplay.com.mwm.guitar.weeklypremiumsub", ProductType.Subscription),
            // 	new ProductDefinition("googleplay.com.mwm.guitar.annuallypremiumsub", ProductType.Subscription),
            // 	new ProductDefinition(InAppConst.GetSubscriptionVariationSku("subscription.weekly.07.04.99", deviceTarget), ProductType.Subscription),
            // 	new ProductDefinition(InAppConst.GetSubscriptionVariationSku("subscription.weekly.07.07.99", deviceTarget), ProductType.Subscription),
            // 	new ProductDefinition(InAppConst.GetSubscriptionVariationSku("subscription.weekly.07.09.99", deviceTarget), ProductType.Subscription),
            // 	new ProductDefinition(InAppConst.GetSubscriptionVariationSku("subscription.monthly.07.19.99", deviceTarget), ProductType.Subscription),
            // 	new ProductDefinition(InAppConst.GetSubscriptionVariationSku("subscription.monthly.07.29.99", deviceTarget), ProductType.Subscription),
            // 	new ProductDefinition(InAppConst.GetSubscriptionVariationSku("subscription.monthly.07.39.99", deviceTarget), ProductType.Subscription),
            // 	new ProductDefinition(InAppConst.GetSubscriptionVariationSku("subscription.annually.07.49.99", deviceTarget), ProductType.Subscription),
            // 	new ProductDefinition(InAppConst.GetSubscriptionVariationSku("subscription.annually.07.79.99", deviceTarget), ProductType.Subscription),
            // 	new ProductDefinition(InAppConst.GetSubscriptionVariationSku("subscription.annually.07.99.99", deviceTarget), ProductType.Subscription)
            // });
            // UnityPurchasing.Initialize(this, configurationBuilder);
        }

        [CanBeNull]
        private Product ProductForId(string id)
        {
            if (_status == Status.Initialized) return _controller.products.WithID(id);
            return null;
        }

        [CanBeNull]
        private InAppData Buy(Payment payment)
        {
            if (Application.internetReachability == NetworkReachability.NotReachable)
            {
                DisplayNoInternetDialog();
                payment.OnFailure();
                return null;
            }

            if (_status != Status.Initialized)
            {
                payment.OnFailure();
                return null;
            }

            if (IsSubscribe())
            {
                payment.OnFailure();
                return null;
            }

            var product = ProductForId(payment.ProductId);
            if (product == null || !product.availableToPurchase)
            {
                payment.OnFailure();
                return null;
            }

            _userProcessingPayments.Add(payment);
            _controller.InitiatePurchase(product);
            var metadata = product.metadata;
            return new InAppData(payment.ProductId, metadata.isoCurrencyCode, (float) metadata.localizedPrice);
        }

        private PurchaseProcessingResult ProcessUserInitiatedPurchase(Payment payment)
        {
            return ProcessUserInitiatedPurchaseAndroid(payment);
        }

        private void AndroidVerificatorHandler(Dictionary<string, bool> verifResult)
        {
            foreach (var premiumPassId in _premiumPassIds)
                if (verifResult.ContainsKey(premiumPassId) && verifResult[premiumPassId])
                    Unlock(premiumPassId);
                else
                    Lock(premiumPassId);
        }

        private IReceiptValidator Validator()
        {
            return _inAppManagerExtension.Validator(_monoBehaviour, "d18e79f1-feca-4a87-8b37-a8c194949018",
                "3af26ba3-4083-44e2-bc59-49bf7abc2042");
        }

        private void Unlock(string productId)
        {
            if (!_storage.UnlockedIaPs.Contains(productId))
            {
                _storage.UnlockedIaPs.Add(productId);
                _storage.SaveToFile();
            }

            if (OnInAppsUnlockedDelegate != null) OnInAppsUnlockedDelegate(productId);
        }

        private void Lock(string productId)
        {
            if (_storage.UnlockedIaPs.Contains(productId))
            {
                _storage.UnlockedIaPs.Remove(productId);
                _storage.SaveToFile();
            }

            if (OnInAppsLockedDelegate != null) OnInAppsLockedDelegate(productId);
        }

        private void LockAllProducts()
        {
            for (var i = 0; i < _premiumPassIds.Count; i++)
            {
                var productId = _premiumPassIds[i];
                Lock(productId);
            }
        }

        private MWMGooglePlaySubscriptionVerificator CreateMwmGooglePlaySubscriptionVerificator(
            MonoBehaviour monoBehaviour)
        {
            var installationId = GameEventSender.Instance().InstallationId;
            var editor = _deviceManager.GetRuntimeType() == RuntimeType.Editor;
            return null;
            // return new MWMGooglePlaySubscriptionVerificator(monoBehaviour, editor, "d18e79f1-feca-4a87-8b37-a8c194949018", "3af26ba3-4083-44e2-bc59-49bf7abc2042", installationId, string.Empty);
        }

        private static void DisplayNoInternetDialog()
        {
            var mNPopup = new MNPopup("No internet connection", "Connect to internet and try again.");
            mNPopup.AddAction("OK", delegate { });
            mNPopup.Show();
        }

        public static string ExtractEditorPrice(string[] skuWithoutPrefixSplit, bool isSkuSubscription)
        {
            string text;
            string text2;
            if (isSkuSubscription)
            {
                text = skuWithoutPrefixSplit[3];
                text2 = skuWithoutPrefixSplit[4];
            }
            else
            {
                text = skuWithoutPrefixSplit[2];
                text2 = skuWithoutPrefixSplit[3];
            }

            if (text2.Equals("00")) return text;
            var num = int.Parse(text);
            return num + "." + text2;
        }

        private PurchaseProcessingResult ProcessUserInitiatedPurchaseAndroid(Payment payment)
        {
            VerifyPayment(payment, AndroidVerificatorHandler);
            _inAppManagerExtension.SendTransactionEvents(payment.ProductId, payment.TransactionId);
            return PurchaseProcessingResult.Pending;
        }

        private void VerifyPayment(Payment payment, Action<Dictionary<string, bool>> completion)
        {
            var item = new GooglePlaySubInfos(payment.ProductId, payment.TransactionId);
            var list = new List<GooglePlaySubInfos>();
            list.Add(item);
            var subsInfos = list;
            var mWMGooglePlaySubscriptionVerificator = CreateMwmGooglePlaySubscriptionVerificator(_monoBehaviour);
            mWMGooglePlaySubscriptionVerificator.VerifySubscription(subsInfos,
                delegate(Dictionary<string, bool> verificationResults)
                {
                    foreach (var verificationResult in verificationResults)
                        if (verificationResult.Key.Equals(payment.ProductId))
                        {
                            if (verificationResult.Value)
                                payment.OnSuccess();
                            else
                                payment.OnFailure();
                        }

                    completion(verificationResults);
                });
        }

        private static InAppManagerExtension CreateInAppManagerExtension()
        {
            return new InAppManagerExtensionAndroid();
        }
    }
}