using System;
using Devices;
using GuitarApplication;
using JetBrains.Annotations;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Store
{
    public class StoreScreen : MonoBehaviour, StoreView
    {
        [SerializeField] private Text _purchaseButtonTitle;

        [SerializeField] private Text _purchaseButtonSubtitle;

        [SerializeField] private GameObject _iOSLegalText;

        [SerializeField] private GameObject _androidLegalText;

        [SerializeField] private GameObject _restoreButton;

        [SerializeField] private TextMeshProUGUI _androidLegalTextMeshPro;

        [SerializeField] private TextMeshProUGUI _iOSLegalTextMeshPro;

        // [SerializeField]
        // private Material _backgroundBlurMaterial;

        private int _screentTextureId;

        public StorePresenter presenter { get; private set; }

        private void Awake()
        {
            var deviceManager = ApplicationGraph.GetDeviceManager();
            switch (deviceManager.GetDeviceTarget())
            {
                case DeviceTarget.Android:
                    _androidLegalTextMeshPro.text = ApplicationGraph.GetLocalizationManager()
                        .GetLocalizedValue("store_android_terms_of_user");
                    _iOSLegalText.gameObject.SetActive(false);
                    _androidLegalText.gameObject.SetActive(true);
                    _restoreButton.gameObject.SetActive(false);
                    break;
                case DeviceTarget.Ios:
                    _iOSLegalTextMeshPro.text = ApplicationGraph.GetLocalizationManager()
                        .GetLocalizedValue("store_apple_terms_of_user");
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public void SetVisible(bool visible)
        {
            gameObject.SetActive(visible);
        }

        public void DisplaySubscription(string freeTrialPeriod, string price)
        {
            _purchaseButtonTitle.text = freeTrialPeriod;
            _purchaseButtonSubtitle.text = price;
        }

        public void Initialize(StorePresenter presenter)
        {
            this.presenter = presenter;
        }

        [UsedImplicitly]
        public void OnBackButtonClicked()
        {
            presenter.OnBackButtonClicked();
        }

        [UsedImplicitly]
        public void OnBuyButtonClicked()
        {
            presenter.OnBuyButtonClicked();
        }

        [UsedImplicitly]
        public void OnRestoreButtonClicked()
        {
            presenter.OnRestoreButtonClicked();
        }
    }
}