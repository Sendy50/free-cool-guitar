using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Purchasing;

namespace Store
{
    public interface InAppManagerExtension
    {
        void Restore(Action<bool> callback);

        IReceiptValidator Validator(MonoBehaviour monoBehaviour, string endAppId, string endAppKey);

        void SendTransactionEvents(string productId, string transactionId);

        string GetTransactionId(PurchaseEventArgs e);

        void RestoreCompleted(bool isSuccess);

        void SetExtensions(IExtensionProvider extensions);

        void SetPendingIndirectPayments(List<Payment> pendingIndirectPayments);

        void OnPromotionalPurchase(Product item);
    }
}