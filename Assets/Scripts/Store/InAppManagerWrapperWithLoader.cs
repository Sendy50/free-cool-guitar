using System;
using Utils;

namespace Store
{
    public class InAppManagerWrapperWithLoader : InAppManager
    {
        private readonly InAppManager _inAppManager;
        private readonly Action<bool> _loaderVisibilyAction;

        public InAppManagerWrapperWithLoader(InAppManager inAppManager, Action<bool> loaderVisibilyAction)
        {
            Precondition.CheckNotNull(inAppManager);
            Precondition.CheckNotNull(loaderVisibilyAction);
            _loaderVisibilyAction = loaderVisibilyAction;
            _inAppManager = inAppManager;
            _inAppManager.OnInAppInitializedDelegate += InAppManagerOnOnInAppInitializedDelegate;
            _inAppManager.OnInAppsLockedDelegate += InAppManagerOnOnInAppsLockedDelegate;
            _inAppManager.OnInAppsUnlockedDelegate += InAppManagerOnOnInAppsUnlockedDelegate;
        }

        public event OnInAppsUnlocked OnInAppsUnlockedDelegate;

        public event OnInAppsLocked OnInAppsLockedDelegate;

        public event OnInAppInitialized OnInAppInitializedDelegate;

        public bool IsSubscribe()
        {
            return _inAppManager.IsSubscribe();
        }

        public Status GetStatus()
        {
            return _inAppManager.GetStatus();
        }

        public ProductDetails ProductDetailsForId(string id)
        {
            return _inAppManager.ProductDetailsForId(id);
        }

        public InAppData Buy(string productId, Action<Payment.Result> callback = null)
        {
            _loaderVisibilyAction(true);
            return _inAppManager.Buy(productId, delegate(Payment.Result result)
            {
                _loaderVisibilyAction(false);
                if (callback != null) callback(result);
            });
        }

        public void VerifySubscription()
        {
            _inAppManager.VerifySubscription();
        }

        public void Restore(Action<bool> callback)
        {
            _loaderVisibilyAction(true);
            _inAppManager.Restore(delegate(bool b)
            {
                _loaderVisibilyAction(false);
                callback(b);
            });
        }

        private void InAppManagerOnOnInAppsUnlockedDelegate(string inappId)
        {
            if (OnInAppsUnlockedDelegate != null) OnInAppsUnlockedDelegate(inappId);
        }

        private void InAppManagerOnOnInAppsLockedDelegate(string inappId)
        {
            if (OnInAppsLockedDelegate != null) OnInAppsLockedDelegate(inappId);
        }

        private void InAppManagerOnOnInAppInitializedDelegate()
        {
            if (OnInAppInitializedDelegate != null) OnInAppInitializedDelegate();
        }
    }
}