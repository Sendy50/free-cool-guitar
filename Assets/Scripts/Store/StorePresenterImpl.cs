using System;
using i18n;
using JetBrains.Annotations;
using MWM.GameEvent;
using Subscription;
using Utils;

namespace Store
{
    public class StorePresenterImpl : StorePresenter
    {
        private readonly IGameEventSender _eventSender;

        private readonly InAppManager _inappManager;

        private readonly LocalizationManager _localizationManager;

        private readonly SubscriptionManager _subscriptionManager;
        private readonly StoreView _view;

        private string _sku;

        public StorePresenterImpl(StoreView view, InAppManager inappManager, IGameEventSender eventSender,
            SubscriptionManager subscriptionManager, LocalizationManager localizationManager)
        {
            Precondition.CheckNotNull(view);
            Precondition.CheckNotNull(inappManager);
            Precondition.CheckNotNull(eventSender);
            Precondition.CheckNotNull(subscriptionManager);
            Precondition.CheckNotNull(localizationManager);
            _view = view;
            _inappManager = inappManager;
            _eventSender = eventSender;
            _subscriptionManager = subscriptionManager;
            _localizationManager = localizationManager;
        }

        [CanBeNull] public StoreRouter router { get; private set; }

        public void SetupRouter(StoreRouter router)
        {
            this.router = router;
        }

        public void ShowScreen()
        {
            _view.SetVisible(true);
            if (_inappManager.GetStatus() == Status.Initialized)
            {
                GetAndDisplayInApp();
                return;
            }

            _view.DisplaySubscription("----", "----");
            _inappManager.OnInAppInitializedDelegate += OnInAppManagerInitialized;
        }

        public void CloseStore()
        {
            _view.SetVisible(false);
            if (router != null) router.OnStoreHide();
        }

        public void OnBackButtonClicked()
        {
            _eventSender.SendEvent(EventType.StoreDismiss, StoreDismissReason.CloseButton.StringValue());
            CloseStore();
        }

        public void OnBuyButtonClicked()
        {
            if (_inappManager.IsSubscribe()) throw new Exception("Buy button should not be visible if subscribed");
            var inAppData = _inappManager.Buy(_sku, BuyCallback);
            if (inAppData != null)
            {
                var clickBuyEvent = new ClickBuyEvent(ClickBuyEvent.Sources.Store, inAppData.ProductId,
                    inAppData.CurrencyCode, inAppData.Price);
                _eventSender.SendEvent(EventType.ClickBuy, clickBuyEvent.ToJsonString());
            }
        }

        public void OnRestoreButtonClicked()
        {
            if (_inappManager.IsSubscribe()) throw new Exception("Restore button should not be visible if subscribed");
            _inappManager.Restore(RestoreCallback);
        }

        private void RestoreCallback(bool isProcessFinished)
        {
            if (_inappManager.IsSubscribe())
            {
                _eventSender.SendEvent(EventType.StoreDismiss, StoreDismissReason.Restore.StringValue());
                CloseStore();
            }
        }

        private void OnInAppManagerInitialized()
        {
            GetAndDisplayInApp();
        }

        private void BuyCallback(Payment.Result result)
        {
            if (result.IsSuccess)
            {
                _eventSender.SendEvent(EventType.StoreDismiss, result.PurchasedProductId);
                CloseStore();
            }
        }

        private void GetAndDisplayInApp()
        {
            _sku = _subscriptionManager.GetSku();
            var subscriptionDetails = _subscriptionManager.GetSubscriptionDetails();
            var flag = subscriptionDetails.GetFreeTrialPeriod() > 0;
            var freeTrialTextIdFromFreeTrial = GetFreeTrialTextIdFromFreeTrial(flag);
            var key = !flag
                ? _localizationManager.GetLocalizedValue(freeTrialTextIdFromFreeTrial)
                : string.Format(_localizationManager.GetLocalizedValue(freeTrialTextIdFromFreeTrial),
                    subscriptionDetails.GetFreeTrialPeriod());
            var priceContainerTextIdAccordingToSubscriptionDuration =
                GetPriceContainerTextIdAccordingToSubscriptionDuration(subscriptionDetails.GetSubscriptionDuration(),
                    flag);
            var key2 = string.Format(
                _localizationManager.GetLocalizedValue(priceContainerTextIdAccordingToSubscriptionDuration),
                subscriptionDetails.GetPrice());
            _view.DisplaySubscription(_localizationManager.GetLocalizedValue(key),
                _localizationManager.GetLocalizedValue(key2));
        }

        private string GetFreeTrialTextIdFromFreeTrial(bool hasFreeTrial)
        {
            return !hasFreeTrial ? "store_button_title_start" : "store_button_title_free_trial";
        }

        private string GetPriceContainerTextIdAccordingToSubscriptionDuration(SubscriptionDuration subTime,
            bool hasFreeTrial)
        {
            switch (subTime)
            {
                case SubscriptionDuration.Weekly:
                    return !hasFreeTrial
                        ? "store_button_subtitle_price_per_week_2"
                        : "store_button_subtitle_price_per_week";
                case SubscriptionDuration.Monthly:
                    return !hasFreeTrial
                        ? "store_button_subtitle_price_per_month_2"
                        : "store_button_subtitle_price_per_month";
                case SubscriptionDuration.Annually:
                    return !hasFreeTrial
                        ? "store_button_subtitle_price_per_year_2"
                        : "store_button_subtitle_price_per_year";
                default:
                    throw new ArgumentOutOfRangeException("subTime", subTime, null);
            }
        }
    }
}