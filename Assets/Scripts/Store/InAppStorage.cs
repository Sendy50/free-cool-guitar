using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

namespace Store
{
    [Serializable]
    public class InAppStorage
    {
        public readonly List<string> UnlockedIaPs = new List<string>();

        public static InAppStorage LoadFromFile()
        {
            if (File.Exists(Application.persistentDataPath + "/iapData.dat"))
            {
                var binaryFormatter = new BinaryFormatter();
                var serializationStream = File.Open(Application.persistentDataPath + "/iapData.dat", FileMode.Open);
                return (InAppStorage) binaryFormatter.Deserialize(serializationStream);
            }

            return new InAppStorage();
        }

        public void SaveToFile()
        {
            var binaryFormatter = new BinaryFormatter();
            var serializationStream = File.Create(Application.persistentDataPath + "/iapData.dat");
            binaryFormatter.Serialize(serializationStream, this);
        }
    }
}