using Newtonsoft.Json.Linq;

namespace Store
{
    public struct VerifyReceiptResult
    {
        public ReceiptStatus ReceiptStatus;

        public JToken ReceiptInfo;

        public bool IsValide()
        {
            return ReceiptStatus == ReceiptStatus.Valid && ReceiptInfo != null;
        }

        public bool IsExplicitWrong()
        {
            return ReceiptStatus == ReceiptStatus.JsonNotReadable ||
                   ReceiptStatus == ReceiptStatus.MalformedOrMissingData ||
                   ReceiptStatus == ReceiptStatus.ReceiptCouldNotBeAuthenticated ||
                   ReceiptStatus == ReceiptStatus.SecretNotMatching;
        }
    }
}