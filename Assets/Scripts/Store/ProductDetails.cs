using JetBrains.Annotations;
using Utils;

namespace Store
{
    public class ProductDetails
    {
        private readonly decimal _decimalPrice;

        private readonly string _isoCurrencyCode;

        private readonly string _readablePrice;
        private readonly string _sku;

        public ProductDetails(string sku, string readablePrice, decimal decimalPrice, string isoCurrencyCode)
        {
            Precondition.CheckNotNull(sku);
            Precondition.CheckNotNull(readablePrice);
            Precondition.CheckNotNull(decimalPrice);
            Precondition.CheckNotNull(isoCurrencyCode);
            _sku = sku;
            _readablePrice = readablePrice;
            _decimalPrice = decimalPrice;
            _isoCurrencyCode = isoCurrencyCode;
        }

        [NotNull]
        public string GetSku()
        {
            return _sku;
        }

        [NotNull]
        public string GetReadablePrice()
        {
            return _readablePrice;
        }

        public decimal GetDecimalPrice()
        {
            return _decimalPrice;
        }

        [NotNull]
        public string GetIsoCurrencyCode()
        {
            return _isoCurrencyCode;
        }
    }
}