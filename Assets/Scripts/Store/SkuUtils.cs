using Devices;

namespace Store
{
    public static class SkuUtils
    {
        public static string GetPriceFromSku(string sku, DeviceTarget deviceTarget, string defaultPrice)
        {
            var subscriptionAbTestSkuPrefix = InAppConst.GetSubscriptionAbTestSkuPrefix(deviceTarget);
            if (sku.StartsWith(subscriptionAbTestSkuPrefix))
            {
                var text = sku.Substring(subscriptionAbTestSkuPrefix.Length);
                var array = text.Split('.');
                var text2 = array[3];
                var text3 = array[4];
                if (text3.Equals("00")) return text2 + "$";
                var num = int.Parse(text2);
                return num + "." + text3 + "$";
            }

            return defaultPrice;
        }
    }
}