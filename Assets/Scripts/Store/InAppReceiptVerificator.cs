using System;
using System.Linq;
using Newtonsoft.Json.Linq;

namespace Store
{
    public class InAppReceiptVerificator
    {
        // public static string AppStoreReceipt => ConfigurationBuilder.Instance(StandardPurchasingModule.Instance()).Configure<IAppleConfiguration>().appReceipt;

        public static string AppStoreReceiptDecodedString =>
            // string appStoreReceipt = AppStoreReceipt;
            // if (appStoreReceipt == null)
            // {
            // 	return null;
            // }
            // byte[] bytes = Convert.FromBase64String(appStoreReceipt);
            // return Encoding.UTF8.GetString(bytes);
            null;

        public static byte[] AppStoreReceiptData =>
            // string appStoreReceipt = AppStoreReceipt;
            // if (appStoreReceipt == null)
            // {
            // 	return null;
            // }
            // return Convert.FromBase64String(appStoreReceipt);
            null;

        public VerifySubscriptionResult VerifySubscriptions(string[] productIds, JToken receiptInfo)
        {
            var jToken = receiptInfo["latest_receipt_info"];
            if (jToken == null)
            {
                VerifySubscriptionResult result = default;
                result.HasSubsciription = false;
                return result;
            }

            var source = jToken.Where(delegate(JToken c)
            {
                var jToken2 = c["product_id"];
                if (jToken2 == null) return false;
                var value = (string) jToken2;
                return productIds.Contains(value);
            });
            var source2 = source.Where(c => c["cancellation_date"] == null ? true : false);
            if (source2.Count() == 0)
            {
                VerifySubscriptionResult result2 = default;
                result2.HasSubsciription = false;
                return result2;
            }

            var receiptRequestDate = GetReceiptRequestDate(receiptInfo);
            var source3 =
                source2.Select(c => ConvertFromUnixTimestamp(c["expires_date_ms"].ToObject<double>() / 1000.0));
            var source4 = source3.OrderByDescending(c => c);
            try
            {
                var t = source4.First();
                if (t > receiptRequestDate)
                {
                    VerifySubscriptionResult result3 = default;
                    result3.HasSubsciription = true;
                    return result3;
                }

                VerifySubscriptionResult result4 = default;
                result4.HasSubsciription = false;
                return result4;
            }
            catch
            {
                VerifySubscriptionResult result5 = default;
                result5.HasSubsciription = false;
                return result5;
            }
        }

        public void Verify(IReceiptValidator validator, Action<VerifyReceiptResult> completion)
        {
            // validator.Validate(AppStoreReceipt, completion);
        }

        public void Verify(string appReceipt, IReceiptValidator validator, Action<VerifyReceiptResult> completion)
        {
            if (appReceipt == null)
                completion(new VerifyReceiptResult
                {
                    ReceiptStatus = ReceiptStatus.Unknown
                });
            else
                validator.Validate(appReceipt, completion);
        }

        private DateTime GetReceiptRequestDate(JToken receipt)
        {
            var jToken = receipt["receipt"];
            if (jToken == null) return DateTime.Now;
            var jToken2 = jToken["request_date_ms"];
            if (jToken2 == null) return DateTime.Now;
            return ConvertFromUnixTimestamp(jToken2.ToObject<double>() / 1000.0);
        }

        private static DateTime ConvertFromUnixTimestamp(double timestamp)
        {
            return new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc).AddSeconds(timestamp);
        }
    }
}