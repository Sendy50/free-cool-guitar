namespace Store
{
    public enum Status
    {
        Initializing,
        Initialized,
        InitializationFailed
    }
}