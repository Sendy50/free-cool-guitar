using System;
using System.Collections.Generic;
using Devices;

namespace Store
{
    public class InAppConst
    {
        public const string VariationStoreSubsWeekly_07_04_99 = "subscription.weekly.07.04.99";

        public const string VariationStoreSubsWeekly_07_07_99 = "subscription.weekly.07.07.99";

        public const string VariationStoreSubsWeekly_07_09_99 = "subscription.weekly.07.09.99";

        public const string VariationStoreSubsMonthly_07_19_99 = "subscription.monthly.07.19.99";

        public const string VariationStoreSubsMonthly_07_29_99 = "subscription.monthly.07.29.99";

        public const string VariationStoreSubsMonthly_07_39_99 = "subscription.monthly.07.39.99";

        public const string VariationStoreSubsAnnually_07_49_99 = "subscription.annually.07.49.99";

        public const string VariationStoreSubsAnnually_07_79_99 = "subscription.annually.07.79.99";

        public const string VariationStoreSubsAnnually_07_99_99 = "subscription.annually.07.99.99";

        private const string SkuPrefixIos = "appstore.com.mwm.guitarapp.";

        private const string SkuPrefixAndroid = "gplay.com.mwm.guitarapp.";

        public const string WeeklyPremiumPassId = "googleplay.com.mwm.guitar.weeklypremiumsub";

        public const string AnnuallyPremiumPassId = "googleplay.com.mwm.guitar.annuallypremiumsub";

        public static string GetSubscriptionAbTestSkuPrefix(DeviceTarget deviceTarget)
        {
            switch (deviceTarget)
            {
                case DeviceTarget.Android:
                    return "gplay.com.mwm.guitarapp.";
                case DeviceTarget.Ios:
                    return "appstore.com.mwm.guitarapp.";
                default:
                    throw new Exception("Unknown device target : " + deviceTarget);
            }
        }

        public static string GetSubscriptionVariationSku(string variation, DeviceTarget deviceTarget)
        {
            var subscriptionAbTestSkuPrefix = GetSubscriptionAbTestSkuPrefix(deviceTarget);
            return subscriptionAbTestSkuPrefix + variation;
        }

        public static List<string> GetSubscriptionAbTestSkus(DeviceTarget deviceTarget)
        {
            var list = new List<string>();
            list.Add(GetSubscriptionVariationSku("subscription.weekly.07.04.99", deviceTarget));
            list.Add(GetSubscriptionVariationSku("subscription.weekly.07.07.99", deviceTarget));
            list.Add(GetSubscriptionVariationSku("subscription.weekly.07.09.99", deviceTarget));
            list.Add(GetSubscriptionVariationSku("subscription.monthly.07.19.99", deviceTarget));
            list.Add(GetSubscriptionVariationSku("subscription.monthly.07.29.99", deviceTarget));
            list.Add(GetSubscriptionVariationSku("subscription.monthly.07.39.99", deviceTarget));
            list.Add(GetSubscriptionVariationSku("subscription.annually.07.49.99", deviceTarget));
            list.Add(GetSubscriptionVariationSku("subscription.annually.07.79.99", deviceTarget));
            list.Add(GetSubscriptionVariationSku("subscription.annually.07.99.99", deviceTarget));
            return list;
        }
    }
}