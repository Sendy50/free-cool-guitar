using System;
using InHouseAds;
using MWM.GameEvent;
using Utils;

namespace Store
{
    public class StoreRouterImpl : StoreRouter, IInHouseStoreDisplayer
    {
        private const string freeModeRefName = "free_mode";

        private const string pauseMenuModeRefName = "ingame_pause_menu";

        private const string guitarSelectionMenuRefName = "guitar_selection_menu";

        private const string trackListRefName = "track_list";

        private const string inHouseRefName = "inHouse";

        private string _origineInterfaceName;

        public StoreRouterImpl(StorePresenter storePresenter, IGameEventSender gameEventSender)
        {
            Precondition.CheckNotNull(storePresenter);
            Precondition.CheckNotNull(gameEventSender);
            this.storePresenter = storePresenter;
            this.gameEventSender = gameEventSender;
            storePresenter.SetupRouter(this);
        }

        public StorePresenter storePresenter { get; }

        public IGameEventSender gameEventSender { get; }

        public event Action OnInHouseStoreDismiss;

        public void DisplayInHouseStore()
        {
            _origineInterfaceName = "inHouse";
            storePresenter.ShowScreen();
        }

        public event Action ShowFreeModeInterface;

        public event Action ShowPauseInGameInterface;

        public event Action ShowGuitarSelectionMenuInterface;

        public event Action ShowTrackList;

        public void ShowStoreFromFreeModeInterface()
        {
            _origineInterfaceName = "free_mode";
            storePresenter.ShowScreen();
            gameEventSender.SendEvent(EventType.StoreDisplay, StoreDisplaySource.FreeModeInterface.StringValue());
        }

        public void ShowStoreFromOpeningApp()
        {
            _origineInterfaceName = "free_mode";
            storePresenter.ShowScreen();
            // gameEventSender.SendEvent(EventType.StoreDisplay, StoreDisplaySource.OpeningApp.StringValue());
        }

        public void ShowStoreFromPauseInGameInterface()
        {
            _origineInterfaceName = "ingame_pause_menu";
            storePresenter.ShowScreen();
            // gameEventSender.SendEvent(EventType.StoreDisplay, StoreDisplaySource.IngamePauseNavBar.StringValue());
        }

        public void ShowStoreFromSelectLockedGuitar()
        {
            _origineInterfaceName = "guitar_selection_menu";
            storePresenter.ShowScreen();
            // gameEventSender.SendEvent(EventType.StoreDisplay, StoreDisplaySource.ClickLockedGuitar.StringValue());
        }

        public void ShowStoreFromClickUnlockAllOnGuitarList()
        {
            _origineInterfaceName = "guitar_selection_menu";
            storePresenter.ShowScreen();
            // gameEventSender.SendEvent(EventType.StoreDisplay, StoreDisplaySource.GuitarStoreNavBar.StringValue());
        }

        public void ShowStoreFromClickVIPTrack()
        {
            _origineInterfaceName = "track_list";
            storePresenter.ShowScreen();
            // gameEventSender.SendEvent(EventType.StoreDisplay, StoreDisplaySource.ClickPremiumTrack.StringValue());
        }

        public void ShowStoreFromTrackListNavBar()
        {
            _origineInterfaceName = "track_list";
            storePresenter.ShowScreen();
            // gameEventSender.SendEvent(EventType.StoreDisplay, StoreDisplaySource.TrackListNavBar.StringValue());
        }

        public void OnStoreHide()
        {
            switch (_origineInterfaceName)
            {
                case "free_mode":
                    if (ShowFreeModeInterface != null) ShowFreeModeInterface();
                    break;
                case "ingame_pause_menu":
                    if (ShowPauseInGameInterface != null) ShowPauseInGameInterface();
                    break;
                case "guitar_selection_menu":
                    if (ShowGuitarSelectionMenuInterface != null) ShowGuitarSelectionMenuInterface();
                    break;
                case "track_list":
                    if (ShowTrackList != null) ShowTrackList();
                    break;
                case "inHouse":
                    if (OnInHouseStoreDismiss != null) OnInHouseStoreDismiss();
                    break;
            }
        }
    }
}