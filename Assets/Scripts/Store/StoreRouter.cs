using System;

namespace Store
{
    public interface StoreRouter
    {
        event Action ShowFreeModeInterface;

        event Action ShowPauseInGameInterface;

        event Action ShowGuitarSelectionMenuInterface;

        event Action ShowTrackList;

        void ShowStoreFromFreeModeInterface();

        void ShowStoreFromOpeningApp();

        void ShowStoreFromPauseInGameInterface();

        void ShowStoreFromSelectLockedGuitar();

        void ShowStoreFromClickUnlockAllOnGuitarList();

        void ShowStoreFromClickVIPTrack();

        void ShowStoreFromTrackListNavBar();

        void OnStoreHide();
    }
}