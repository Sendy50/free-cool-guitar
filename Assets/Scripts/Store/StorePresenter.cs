namespace Store
{
    public interface StorePresenter
    {
        void SetupRouter(StoreRouter router);

        void ShowScreen();

        void CloseStore();

        void OnBackButtonClicked();

        void OnBuyButtonClicked();

        void OnRestoreButtonClicked();
    }
}