using System;

namespace Store
{
    public interface InAppManager
    {
        event OnInAppsUnlocked OnInAppsUnlockedDelegate;

        event OnInAppsLocked OnInAppsLockedDelegate;

        event OnInAppInitialized OnInAppInitializedDelegate;

        bool IsSubscribe();

        Status GetStatus();

        ProductDetails ProductDetailsForId(string id);

        InAppData Buy(string productId, Action<Payment.Result> callback = null);

        void VerifySubscription();

        void Restore(Action<bool> callback);
    }
}