using System;

namespace Store
{
    public interface IReceiptValidator
    {
        void Validate(string appReceipt, Action<VerifyReceiptResult> completion);
    }
}