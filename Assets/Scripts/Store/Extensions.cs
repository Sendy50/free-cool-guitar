using UnityEngine.Purchasing;

namespace Store
{
    public static class Extensions
    {
        public static InappTransactionEvent ToEvent(this Product product)
        {
            var inappTransactionEvent = new InappTransactionEvent();
            inappTransactionEvent.transaction_identifier = product.transactionID;
            inappTransactionEvent.product_identifier = product.definition.id;
            return inappTransactionEvent;
        }

        public static string URLString(this MWMReceiptValidator.VerifyReceiptURLType type)
        {
            if (type == MWMReceiptValidator.VerifyReceiptURLType.production)
                return "https://djit-end.appspot.com/0.4/in_app/validation/ios";
            return "https://dev-dot-djit-end.appspot.com/0.4/in_app/validation/ios";
        }
    }
}