using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using UnityEngine;
using UnityEngine.Networking;
using Utils;

namespace Store
{
    public class MWMReceiptValidator : IReceiptValidator
    {
        public enum VerifyReceiptURLType
        {
            production,
            sandbox
        }

        public MWMReceiptValidator(MonoBehaviour monoBehaviour, string endAppId, string endAppKey,
            string eventInstallationId, IDeviceInfoProvider deviceInfoProvider, string endInstallationId = null,
            VerifyReceiptURLType service = VerifyReceiptURLType.production)
        {
            MonoBehaviour = monoBehaviour;
            EndAppId = endAppId;
            EndAppKey = endAppKey;
            EventInstallationId = eventInstallationId;
            DeviceInfoProvider = deviceInfoProvider;
            EndInstallationId = endInstallationId;
            Service = service;
        }

        public string EndAppId { get; }

        public string EndAppKey { get; }

        public string EventInstallationId { get; }

        public string EndInstallationId { get; }

        public VerifyReceiptURLType Service { get; }

        public MonoBehaviour MonoBehaviour { get; }

        public IDeviceInfoProvider DeviceInfoProvider { get; }

        public void Validate(string appReceipt, Action<VerifyReceiptResult> completion)
        {
            var dictionary = new Dictionary<string, object>();
            dictionary.Add("receipt", appReceipt);
            dictionary.Add("install_id", EventInstallationId);
            dictionary.Add("device_type", DeviceInfoProvider.GetDeviceModel());
            dictionary.Add("country", DeviceInfoProvider.GetCountryCode());
            var value = dictionary;
            var s = JsonConvert.SerializeObject(value);
            var bytes = new UTF8Encoding().GetBytes(s);
            var unityWebRequest = new UnityWebRequest(Service.URLString());
            unityWebRequest.uploadHandler = new UploadHandlerRaw(bytes);
            unityWebRequest.downloadHandler = new DownloadHandlerBuffer();
            unityWebRequest.method = "POST";
            var request = unityWebRequest;
            AddCustomHeaders(request);
            MonoBehaviour.StartCoroutine(POST(request, delegate(UnityWebRequest result)
            {
                var receiptStatus = ReceiptStatus.Unknown;
                JToken jToken;
                try
                {
                    if (result.isNetworkError || result.isHttpError)
                    {
                        completion(new VerifyReceiptResult
                        {
                            ReceiptStatus = receiptStatus
                        });
                        return;
                    }

                    if (result.responseCode != 200 && result.responseCode != 203)
                    {
                        completion(new VerifyReceiptResult
                        {
                            ReceiptStatus = receiptStatus
                        });
                        return;
                    }

                    var text = result.downloadHandler.text;
                    if (text == null)
                    {
                        completion(new VerifyReceiptResult
                        {
                            ReceiptStatus = receiptStatus
                        });
                        return;
                    }

                    var jObject = JObject.Parse(text);
                    if (jObject == null)
                    {
                        completion(new VerifyReceiptResult
                        {
                            ReceiptStatus = receiptStatus
                        });
                        return;
                    }

                    jToken = jObject["receipt"];
                    var num = jToken["status"].ToObject<int>();
                    if (!Enum.IsDefined(typeof(ReceiptStatus), num))
                    {
                        completion(new VerifyReceiptResult
                        {
                            ReceiptStatus = receiptStatus
                        });
                        return;
                    }

                    receiptStatus = (ReceiptStatus) num;
                    if (receiptStatus == ReceiptStatus.TestReceipt)
                    {
                        var mWMReceiptValidator = new MWMReceiptValidator(MonoBehaviour, EndAppId, EndAppKey,
                            EventInstallationId, DeviceInfoProvider, EndInstallationId, VerifyReceiptURLType.sandbox);
                        mWMReceiptValidator.Validate(appReceipt, completion);
                        return;
                    }
                }
                catch
                {
                    completion(new VerifyReceiptResult
                    {
                        ReceiptStatus = ReceiptStatus.Unknown
                    });
                    return;
                }

                completion(new VerifyReceiptResult
                {
                    ReceiptStatus = receiptStatus,
                    ReceiptInfo = jToken
                });
            }));
        }

        private IEnumerator POST(UnityWebRequest request, Action<UnityWebRequest> completion)
        {
            yield return request.SendWebRequest();
            completion(request);
        }

        private void AddCustomHeaders(UnityWebRequest request)
        {
            request.SetRequestHeader("Content-Type", "application/json");
            request.SetRequestHeader("X-End-App-Id", EndAppId);
            request.SetRequestHeader("X-End-Key", EndAppKey);
            if (EndInstallationId != null) request.SetRequestHeader("X-End-Installation-Id", EndInstallationId);
        }
    }
}