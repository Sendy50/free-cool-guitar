using System;

namespace Store
{
    public struct Payment
    {
        public class Result
        {
            private Result(string purchasedProductId)
            {
                PurchasedProductId = purchasedProductId;
            }

            public bool IsSuccess => !string.IsNullOrEmpty(PurchasedProductId);

            public string PurchasedProductId { get; }

            public static Result Failure()
            {
                return new Result(null);
            }

            public static Result Success(string purchasedProductId)
            {
                return new Result(purchasedProductId);
            }
        }

        public readonly string ProductId;

        public string TransactionId;

        private readonly Action<Result> _callback;

        public Payment(string productId, Action<Result> callback = null)
        {
            ProductId = productId;
            _callback = callback;
            TransactionId = string.Empty;
        }

        public void OnSuccess()
        {
            if (_callback != null) _callback(Result.Success(ProductId));
        }

        public void OnFailure()
        {
            if (_callback != null) _callback(Result.Failure());
        }

        public bool IsValide()
        {
            return !string.IsNullOrEmpty(ProductId);
        }
    }
}