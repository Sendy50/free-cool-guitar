using System;
using System.Collections.Generic;
using MWM.GameEvent;
using Store.Android;
using UnityEngine;
using UnityEngine.Purchasing;
using EventType = MWM.GameEvent.EventType;

namespace Store
{
    public class InAppManagerExtensionAndroid : InAppManagerExtension
    {
        public IReceiptValidator Validator(MonoBehaviour monoBehaviour, string endAppId, string endAppKey)
        {
            throw new Exception("This function should not be call for Android platform.");
        }

        public void SendTransactionEvents(string productId, string transactionId)
        {
            var value = "{\"transaction_identifier\": \"" + transactionId + "\", \"product_identifier\": \"" +
                        productId + "\"}";
            GameEventSender.Instance().SendEvent(EventType.InappTransaction, value);
        }

        public string GetTransactionId(PurchaseEventArgs e)
        {
            var googlePlaySubInfosFromJsonString =
                GooglePlaySubInfos.GetGooglePlaySubInfosFromJsonString(e.purchasedProduct.receipt);
            return googlePlaySubInfosFromJsonString.PurchaseToken;
        }

        public void OnPromotionalPurchase(Product item)
        {
        }

        public void Restore(Action<bool> callback)
        {
        }

        public void RestoreCompleted(bool isSuccess)
        {
        }

        public void SetExtensions(IExtensionProvider extensions)
        {
        }

        public void SetPendingIndirectPayments(List<Payment> pendingIndirectPayments)
        {
        }
    }
}