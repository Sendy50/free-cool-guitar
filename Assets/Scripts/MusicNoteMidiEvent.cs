public class MusicNoteMidiEvent : MidiEvent
{
    public readonly MidiNote MidiNote;

    public readonly int StringIndex;

    public readonly int TrackIndex;

    public MusicNoteMidiEvent(MidiNote midiNote, int trackIndex, int stringIndex)
        : base(midiNote.time)
    {
        MidiNote = midiNote;
        TrackIndex = trackIndex;
        StringIndex = stringIndex;
    }
}