using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace MWM.Core.StartApplication
{
    public class RequiredResourceLoader : MonoBehaviour
    {
        public Text InternalStatusText;

        private void Awake()
        {
            if (InternalStatusText == null) throw new ArgumentException("Status text cannot be null.");
        }

        public void Load(List<IRequiredResource> requiredResources, Action<bool> doOnFinish)
        {
            StartCoroutine(LoadJob(requiredResources, doOnFinish));
        }

        private IEnumerator LoadJob(List<IRequiredResource> requiredResources, Action<bool> doOnFinish)
        {
            yield return null;
            foreach (var resource in requiredResources)
                if (resource.NeedToBeLoaded())
                    yield return resource.Load(InternalStatusText);
            var success = true;
            foreach (var requiredResource in requiredResources)
                if (requiredResource.NeedToBeLoaded())
                    success = false;
            doOnFinish(success);
        }
    }
}