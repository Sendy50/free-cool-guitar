using System.Collections;
using UnityEngine.UI;

namespace MWM.Core.StartApplication
{
    public interface IRequiredResource
    {
        bool NeedToBeLoaded();

        IEnumerator Load(Text statusText);
    }
}