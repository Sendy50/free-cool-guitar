using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace MWM.Core.StartApplication.Demo
{
    public class DummyRequiredResource : IRequiredResource
    {
        private readonly string _id;

        private bool _loaded;

        public DummyRequiredResource(string id)
        {
            _id = id;
        }

        public bool NeedToBeLoaded()
        {
            return !_loaded;
        }

        public IEnumerator Load(Text statusText)
        {
            statusText.text = "Dummy Resource " + _id + " loading...";
            yield return new WaitForSeconds(1f);
            statusText.text = "Dummy Resource " + _id + " loaded!";
            _loaded = true;
        }
    }
}