using System;
using System.Collections.Generic;
using MWM.Core.Demo;
using UnityEngine;

namespace MWM.Core.StartApplication.Demo
{
    public class StartApplicationDemoController : MonoBehaviour
    {
        public RequiredResourceLoader InternalResourceLoader;

        private List<IRequiredResource> _requiredResources;

        private void Awake()
        {
            if (InternalResourceLoader == null) throw new ArgumentException("Resource Loader cannot be null.");
            var item = new DummyRequiredResource("ID#1");
            _requiredResources = new List<IRequiredResource>
            {
                item
            };
        }

        private void Start()
        {
            LoadRequiredResources();
        }

        private void LoadRequiredResources()
        {
            InternalResourceLoader.Load(_requiredResources, delegate(bool success)
            {
                if (success)
                {
                    StartApp();
                    return;
                }

                throw new Exception("Required resources failed");
            });
        }

        private static void StartApp()
        {
            Router.GoToMainDemoScreen();
        }
    }
}