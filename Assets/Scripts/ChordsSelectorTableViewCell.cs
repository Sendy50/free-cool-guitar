using UnityEngine;
using UnityEngine.UI;

public class ChordsSelectorTableViewCell : TableViewCell, IChordsSelectorTableViewCell, ITableViewCell
{
    public delegate void OnAddEvent(IChordsSelectorTableViewCell cell);

    public delegate void OnRemoveEvent(IChordsSelectorTableViewCell cell);

    public delegate void OnSelectEvent(IChordsSelectorTableViewCell cell);

    public Image backgroundImage;

    public Text chordNameText;

    public Button addButton;

    public Button removeButton;

    public event OnSelectEvent OnSelect;

    public event OnAddEvent OnAdd;

    public event OnRemoveEvent OnRemove;

    public void SetChordName(string name)
    {
        chordNameText.text = name;
    }

    public void SetChordNameColor(Color color)
    {
        chordNameText.color = color;
    }

    public void SetBackgroundImageColor(Color color)
    {
        backgroundImage.color = color;
    }

    public void SetAddButtonVisibility(bool visible)
    {
        addButton.gameObject.SetActive(visible);
    }

    public void SetRemoveButtonVisibility(bool visible)
    {
        removeButton.gameObject.SetActive(visible);
    }

    public void onSelectButtonClick()
    {
        if (OnSelect != null) OnSelect(this);
    }

    public void onAddButtonClick()
    {
        if (OnAdd != null) OnAdd(this);
    }

    public void onRemoveButtonClick()
    {
        if (OnRemove != null) OnRemove(this);
    }
}