using System;

public abstract class MidiEvent
{
    public static readonly Comparison<MidiEvent> MidiEventComparison =
        delegate(MidiEvent midiEvent1, MidiEvent midiEvent2)
        {
            var num = midiEvent1.StartTime.CompareTo(midiEvent2.StartTime);
            return num != 0 ? num : midiEvent2.Priority - midiEvent1.Priority;
        };

    public readonly int Priority;

    protected MidiEvent(float startTime, int priority = 0)
    {
        StartTime = startTime;
        Priority = priority;
    }

    public float StartTime { get; private set; }

    public void OffsetStartTime(float offset)
    {
        StartTime += offset;
        if (GetType() == typeof(MusicNoteMidiEvent)) ((MusicNoteMidiEvent) this).MidiNote.time += offset;
    }
}