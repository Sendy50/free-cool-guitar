using System.Collections;
using UnityEngine;

public struct StoppableCoroutine
{
    private Coroutine _coroutine;

    public bool GetIsRunning()
    {
        return _coroutine != null;
    }

    public bool StartRoutine(MonoBehaviour monoBehaviour, IEnumerator routine)
    {
        if (_coroutine == null)
        {
            _coroutine = monoBehaviour.StartCoroutine(routine);
            return true;
        }

        return false;
    }

    public bool StopRoutine(MonoBehaviour monoBehaviour)
    {
        if (_coroutine != null)
        {
            monoBehaviour.StopCoroutine(_coroutine);
            _coroutine = null;
            return true;
        }

        return false;
    }

    public void OnRoutineCompleted()
    {
        _coroutine = null;
    }
}