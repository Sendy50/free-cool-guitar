using System.Linq;

public class StrumUserAction : IUserAction
{
    public enum Orientation
    {
        Up,
        Down
    }

    public MusicNoteMidiEvent[] notes;

    public Orientation orientation;

    public float time => notes[0].MidiNote.time;

    public int SpawningStringIndex()
    {
        var source = from c in notes
            where c.MidiNote.midi != 0
            select c.StringIndex
            into c
            orderby c
            select c;
        return orientation != Orientation.Down ? source.Last() : source.First();
    }
}