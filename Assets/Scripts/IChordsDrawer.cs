public interface IChordsDrawer
{
    void DrawChord(IChordContainer chord);
}