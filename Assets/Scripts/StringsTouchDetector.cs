using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class StringsTouchDetector : MonoBehaviour
{
    public delegate void OnStringTouched(GuitarString touchedString, int stringIndex);

    public Camera targetCamera;

    public bool hasTouch;

    private Vector2 _limit;

    private Coroutine _routine;

    private RectTransform[] _stringRecs;

    private readonly List<int> currentTouchesIndex = new List<int>();

    private readonly List<Coroutine> currentTouchesRoutines = new List<Coroutine>();

    private readonly List<int> currentTrackingTouches = new List<int>();

    public GuitarString[] guitarStrings { get; private set; }

    private void Update()
    {
        hasTouch = ComputeCurrentTouches();
    }

    public event OnStringTouched OnStringTouchedDelegate;

    public void SetupStrings(GuitarString[] guitarStrings)
    {
        this.guitarStrings = guitarStrings;
        _stringRecs = guitarStrings.Select(c => c.GetComponent<RectTransform>()).ToArray();
    }

    public void StartDetecting()
    {
        if (_routine == null) _routine = StartCoroutine(Routine());
    }

    public void StopDetecting()
    {
        if (_routine == null) return;
        StopCoroutine(_routine);
        _routine = null;
        foreach (var currentTouchesRoutine in currentTouchesRoutines) StopCoroutine(currentTouchesRoutine);
        currentTouchesRoutines.Clear();
        currentTrackingTouches.Clear();
    }

    private IEnumerator Routine()
    {
        var indexes = new List<int>();
        var stringCoordinates = _stringRecs.Select(delegate(RectTransform c)
        {
            StringCoordinate result = default;
            result.position = c.position;
            result.rect = c.rect;
            return result;
        }).ToArray();
        var halfHeight = stringCoordinates[0].rect.height * 0.5f;
        var yMin = stringCoordinates.Min(c => c.position.y);
        var yMax = stringCoordinates.Max(c => c.position.y);
        _limit = new Vector2(yMin - halfHeight, yMax + halfHeight);
        yield return new WaitUntil(() => !hasTouch);
        while (true)
        {
            yield return new WaitUntil(() => hasTouch);
            var tmpTouches = currentTouchesIndex.ToList();
            foreach (var item in tmpTouches)
                if (!currentTrackingTouches.Contains(item))
                {
                    currentTrackingTouches.Add(item);
                    currentTouchesRoutines.Add(StartCoroutine(RoutineForTouchIndex(item, stringCoordinates)));
                }
        }
    }

    private IEnumerator RoutineForTouchIndex(int touchIndex, StringCoordinate[] stringCoordinates)
    {
        var _previousTouchInWorld = TouchInWorld(targetCamera, touchIndex);
        while (true)
        {
            yield return TouchingRoutine(touchIndex, _previousTouchInWorld, stringCoordinates);
            yield return new WaitUntil(() => currentTouchesIndex.Contains(touchIndex));
            _previousTouchInWorld = TouchInWorld(targetCamera, touchIndex);
        }
    }

    private IEnumerator TouchingRoutine(int touchIndex, Vector2 _previousTouchInWorld,
        StringCoordinate[] stringCoordinates)
    {
        var indexes = new List<int>();
        var previousIndexes = new List<int>();
        while (currentTouchesIndex.Contains(touchIndex))
        {
            var touchInWorld = TouchInWorld(targetCamera, touchIndex);
            indexes.Clear();
            FindIndexOfMatchingRec(stringCoordinates, touchInWorld, _previousTouchInWorld, indexes);
            foreach (var item in indexes)
                if (!previousIndexes.Contains(item) && OnStringTouchedDelegate != null)
                    OnStringTouchedDelegate(guitarStrings[item], item);
            previousIndexes.Clear();
            previousIndexes.AddRange(indexes);
            _previousTouchInWorld = touchInWorld;
            yield return null;
        }
    }

    private bool ComputeCurrentTouches()
    {
        currentTouchesIndex.Clear();
        var touchCount = Input.touchCount;
        for (var i = 0; i < touchCount; i++) currentTouchesIndex.Add(i);
        var count = currentTouchesIndex.Count;
        for (var j = 0; j < count; j++)
        {
            var vector = TouchInWorld(targetCamera, j);
            if (vector.y < _limit.x || vector.y > _limit.y) currentTouchesIndex.Remove(j);
        }

        return currentTouchesIndex.Count > 0;
    }

    private static Vector2 TouchInWorld(Camera camera, int touchIndex)
    {
        var position = Input.GetTouch(touchIndex).position;
        var v = camera.ScreenToWorldPoint(position);
        return v;
    }

    private static void FindIndexOfMatchingRec(StringCoordinate[] stringCoordinates, Vector2 position,
        Vector2 lastPosition, List<int> io)
    {
        var positionY = position.y;
        var lastPositionY = lastPosition.y;
        var halfHeight = stringCoordinates[0].rect.height * 0.5f;
        var i = 0;
        Action action = delegate
        {
            var y = stringCoordinates[i].position.y;
            var vy = y + halfHeight;
            var vx = y - halfHeight;
            if (VectorContainVector(lastPositionY, positionY, vx, vy)) io.Add(i);
        };
        if (positionY <= lastPositionY)
            for (i = 0; i < stringCoordinates.Length; i++)
                action();
        else
            for (i = stringCoordinates.Length - 1; i >= 0; i--)
                action();
    }

    private static bool VectorContainPoint(float vx, float vy, float value)
    {
        return value >= vx && value <= vy || value <= vx && value >= vy;
    }

    private static bool VectorContainVector(float vx1, float vy1, float vx2, float vy2)
    {
        return VectorContainPoint(vx1, vy1, vx2) || VectorContainPoint(vx1, vy1, vy2) ||
               VectorContainPoint(vx2, vy2, vx1);
    }

    private struct StringCoordinate
    {
        public Vector3 position;

        public Rect rect;
    }
}