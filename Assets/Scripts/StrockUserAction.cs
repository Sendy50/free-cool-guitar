public class StrockUserAction : IUserAction
{
    public MusicNoteMidiEvent note;

    public float time => note.MidiNote.time;

    public int SpawningStringIndex()
    {
        return note.StringIndex;
    }
}