using System.Collections;
using System.Linq;
using UnityEngine;
using Utils;

public class UserActionCatcher : MonoBehaviour, IActionValidatorReader
{
    public delegate void OnActionCatched(IUserAction action);

    public delegate void OnActionMissed(IUserAction action);

    public delegate void OnTouchStringResponse(UserActionValidationTouchStringResponse response, int stringIndex);

    public GameplayParameters gameplayParameters;

    public UserActionSpawner userActionSpawner;

    private Coroutine _catchCoroutine;

    private ChordsStrings _chordsStrings;

    public IUserActionValidator[] actionValidators { get; private set; }

    public IUserActionValidator currentActionValidator { get; private set; }

    public event OnActionCatched OnActionCatchedDelegate;

    public event OnActionMissed OnActionMissedDelegate;

    public event OnTouchStringResponse OnTouchStringResponseDelegate;

    public void StartCatch(MusicSheetReader musicSheetReader, ChordsStrings chordsStrings)
    {
        _chordsStrings = chordsStrings;
        if (_catchCoroutine == null)
        {
            _catchCoroutine = StartCoroutine(CatchRoutine(musicSheetReader));
            _chordsStrings.OnStringTouchedDelegate -= OnStringTouched;
            _chordsStrings.OnStringTouchedDelegate += OnStringTouched;
        }
    }

    public void StopCatch()
    {
        if (_catchCoroutine != null)
        {
            StopCoroutine(_catchCoroutine);
            _catchCoroutine = null;
        }

        _chordsStrings.OnStringTouchedDelegate -= OnStringTouched;
        currentActionValidator = null;
        _chordsStrings.EnabledStringIndexes.Fill(true);
    }

    private IEnumerator CatchRoutine(MusicSheetReader musicSheetReader)
    {
        var actions = musicSheetReader.MusicSheet.UserActions;
        actionValidators = (from c in actions
            select c.Validator(this, gameplayParameters, musicSheetReader)).ToArray();
        int i;
        for (i = 0; i < actionValidators.Length; i++)
        {
            var validator = actionValidators[i];
            currentActionValidator = validator;
            SpawnObject spawnObject = null;
            yield return new WaitWhile(delegate
            {
                spawnObject = FindSpawnObjectForAction(validator, i, userActionSpawner);
                return spawnObject == null;
            });
            validator.StartDetection();
            if (validator is StrumValidator)
            {
                var v = validator as StrumValidator;
                _chordsStrings.EnabledStringIndexes.Populate(index => v.stringIndexesToDetect.Contains(index));
            }

            yield return new WaitWhile(() => validator.validationStatus == UserActionValidationStatus.detecting);
            if (validator.validationStatus == UserActionValidationStatus.validated)
            {
                spawnObject.OnCatched();
                if (OnActionCatchedDelegate != null) OnActionCatchedDelegate(actions[i]);
                if (validator is StrumValidator)
                    yield return new WaitUntil(() => !validator.interleave.IsOnInterleave());
            }
            else
            {
                spawnObject.OnMissed();
                if (OnActionMissedDelegate != null) OnActionMissedDelegate(actions[i]);
            }

            _chordsStrings.EnabledStringIndexes.Fill(true);
        }

        _chordsStrings.OnStringTouchedDelegate -= OnStringTouched;
        currentActionValidator = null;
        _catchCoroutine = null;
    }

    private void OnStringTouched(GuitarString sender, int stringIndex)
    {
        if (currentActionValidator != null)
        {
            var response = currentActionValidator.OnStringTouched(stringIndex);
            if (OnTouchStringResponseDelegate != null) OnTouchStringResponseDelegate(response, stringIndex);
        }
    }

    public static SpawnObject FindSpawnObjectForAction(IUserActionValidator validator, int actionIndex,
        UserActionSpawner spawner)
    {
        if (validator is StrokValidator)
            return spawner.strokPool.InUse().FirstOrDefault(c => c.actionIndex == actionIndex);
        if (validator is StrumValidator)
            return spawner.strumPool.InUse().FirstOrDefault(c => c.actionIndex == actionIndex);
        return null;
    }
}