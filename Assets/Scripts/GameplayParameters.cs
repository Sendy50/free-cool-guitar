using UnityEngine;

[CreateAssetMenu]
public class GameplayParameters : ScriptableObject
{
    public float catchTolerance;

    public float actionSpawningTimeHorizon;
}