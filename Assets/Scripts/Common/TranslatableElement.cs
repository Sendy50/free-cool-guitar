using i18n;
using UnityEngine;
using UnityEngine.UI;

namespace Common
{
    public class TranslatableElement : MonoBehaviour
    {
        public string key;

        public bool ForceAllCaps;
        private Text _textContainer;

        private void Awake()
        {
            _textContainer = gameObject.GetComponent<Text>();
            var text = key.Translate();
            _textContainer.text = !ForceAllCaps ? text : text.ToUpper();
        }
    }
}