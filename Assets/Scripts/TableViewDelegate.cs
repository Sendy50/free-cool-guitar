public interface TableViewDelegate
{
    int numberOfRows();

    void configureTableViewCellAtIndex(ITableViewCell cell, int index);

    void topCellIndexChange(int topCellIndex);
}