using System;

namespace UnityEngine.UI.Extensions.ColorPicker
{
    public class ColorPickerControl : MonoBehaviour
    {
        public ColorChangedEvent onValueChanged = new ColorChangedEvent();

        private float _alpha = 1f;

        private float _blue;

        private float _brightness;

        private float _green;
        private float _hue;

        private float _red;

        private float _saturation;

        public HSVChangedEvent onHSVChanged = new HSVChangedEvent();

        public Color CurrentColor
        {
            get => new Color(_red, _green, _blue, _alpha);
            set
            {
                if (!(CurrentColor == value))
                {
                    _red = value.r;
                    _green = value.g;
                    _blue = value.b;
                    _alpha = value.a;
                    RGBChanged();
                    SendChangedEvent();
                }
            }
        }

        public float H
        {
            get => _hue;
            set
            {
                if (_hue != value)
                {
                    _hue = value;
                    HSVChanged();
                    SendChangedEvent();
                }
            }
        }

        public float S
        {
            get => _saturation;
            set
            {
                if (_saturation != value)
                {
                    _saturation = value;
                    HSVChanged();
                    SendChangedEvent();
                }
            }
        }

        public float V
        {
            get => _brightness;
            set
            {
                if (_brightness != value)
                {
                    _brightness = value;
                    HSVChanged();
                    SendChangedEvent();
                }
            }
        }

        public float R
        {
            get => _red;
            set
            {
                if (_red != value)
                {
                    _red = value;
                    RGBChanged();
                    SendChangedEvent();
                }
            }
        }

        public float G
        {
            get => _green;
            set
            {
                if (_green != value)
                {
                    _green = value;
                    RGBChanged();
                    SendChangedEvent();
                }
            }
        }

        public float B
        {
            get => _blue;
            set
            {
                if (_blue != value)
                {
                    _blue = value;
                    RGBChanged();
                    SendChangedEvent();
                }
            }
        }

        private float A
        {
            get => _alpha;
            set
            {
                if (_alpha != value)
                {
                    _alpha = value;
                    SendChangedEvent();
                }
            }
        }

        private void Start()
        {
            SendChangedEvent();
        }

        private void RGBChanged()
        {
            var hsvColor = HSVUtil.ConvertRgbToHsv(CurrentColor);
            _hue = hsvColor.NormalizedH;
            _saturation = hsvColor.NormalizedS;
            _brightness = hsvColor.NormalizedV;
        }

        private void HSVChanged()
        {
            var color = HSVUtil.ConvertHsvToRgb(_hue * 360f, _saturation, _brightness, _alpha);
            _red = color.r;
            _green = color.g;
            _blue = color.b;
        }

        private void SendChangedEvent()
        {
            onValueChanged.Invoke(CurrentColor);
            onHSVChanged.Invoke(_hue, _saturation, _brightness);
        }

        public void AssignColor(ColorValues type, float value)
        {
            switch (type)
            {
                case ColorValues.R:
                    R = value;
                    break;
                case ColorValues.G:
                    G = value;
                    break;
                case ColorValues.B:
                    B = value;
                    break;
                case ColorValues.A:
                    A = value;
                    break;
                case ColorValues.Hue:
                    H = value;
                    break;
                case ColorValues.Saturation:
                    S = value;
                    break;
                case ColorValues.Value:
                    V = value;
                    break;
            }
        }

        public float GetValue(ColorValues type)
        {
            switch (type)
            {
                case ColorValues.R:
                    return R;
                case ColorValues.G:
                    return G;
                case ColorValues.B:
                    return B;
                case ColorValues.A:
                    return A;
                case ColorValues.Hue:
                    return H;
                case ColorValues.Saturation:
                    return S;
                case ColorValues.Value:
                    return V;
                default:
                    throw new NotImplementedException(string.Empty);
            }
        }
    }
}