using System;
using System.Linq;
using GuitarApplication;
using MWM.Guitar.FreeMode;
using Utils;

public class ChordsSelectorPresenter : IChordsSelectorPresenter, TableViewDelegate
{
    private int _selectedChordIndex;

    public ChordsSelectorPresenter(IChordContainer[] allChords, IChordsList freeModeChords,
        ISelectCordsControl selectCordsControl, ITableView tableView, IChordsDrawer chordsDrawer,
        IChordSelectionCellUIValuesProvider UIValuesPovider, IChordsFastAccessor chordsfastAccessor,
        ApplicationRouter router)
    {
        Precondition.CheckNotNull(allChords);
        Precondition.CheckNotNull(freeModeChords);
        Precondition.CheckNotNull(selectCordsControl);
        Precondition.CheckNotNull(tableView);
        Precondition.CheckNotNull(chordsDrawer);
        Precondition.CheckNotNull(UIValuesPovider);
        Precondition.CheckNotNull(chordsfastAccessor);
        Precondition.CheckNotNull(router);
        this.allChords = allChords;
        this.freeModeChords = freeModeChords;
        this.selectCordsControl = selectCordsControl;
        this.tableView = tableView;
        this.chordsDrawer = chordsDrawer;
        this.UIValuesPovider = UIValuesPovider;
        this.chordsfastAccessor = chordsfastAccessor;
        this.router = router;
        _selectedChordIndex = 0;
        tableView.tableViewDelegate = this;
        tableView.startTableView();
        chordsDrawer.DrawChord(allChords[_selectedChordIndex]);
        chordsfastAccessor.onFastAccessorSelect += OnFastAccessorSelectChord;
        selectCordsControl.OnSelectedUITouched += OnSelectedUITouched;
        freeModeChords.OnSelectedChodsListChanged += OnSelectedChodsListChanged;
    }

    public IChordContainer[] allChords { get; }

    public IChordsList freeModeChords { get; }

    public ISelectCordsControl selectCordsControl { get; }

    public ITableView tableView { get; }

    public IChordsDrawer chordsDrawer { get; }

    public IChordSelectionCellUIValuesProvider UIValuesPovider { get; }

    public IChordsFastAccessor chordsfastAccessor { get; }

    public ApplicationRouter router { get; }

    public int numberOfRows()
    {
        return allChords.Length;
    }

    public void configureTableViewCellAtIndex(ITableViewCell cell, int index)
    {
        var chordsSelectorTableViewCell = cell as IChordsSelectorTableViewCell;
        if (chordsSelectorTableViewCell != null)
        {
            var chordInfo = allChords[index];
            chordsSelectorTableViewCell.SetChordName(chordInfo.GetChord().GetName());
            chordsSelectorTableViewCell.OnSelect -= onSelect;
            chordsSelectorTableViewCell.OnAdd -= onAdd;
            chordsSelectorTableViewCell.OnRemove -= onRemove;
            chordsSelectorTableViewCell.OnSelect += onSelect;
            chordsSelectorTableViewCell.OnAdd += onAdd;
            chordsSelectorTableViewCell.OnRemove += onRemove;
            var flag = freeModeChords.GetPlayableChords().Any(c => chordInfo.GetMidiChord().IsEqual(c));
            if (_selectedChordIndex == index)
            {
                chordsSelectorTableViewCell.SetBackgroundImageColor(UIValuesPovider.GetCellSelectedBackgroundColor());
                chordsSelectorTableViewCell.SetChordNameColor(UIValuesPovider.GetCellSelectedTextColor());
                chordsSelectorTableViewCell.SetAddButtonVisibility(!flag);
                chordsSelectorTableViewCell.SetRemoveButtonVisibility(flag);
            }
            else
            {
                chordsSelectorTableViewCell.SetBackgroundImageColor(UIValuesPovider.GetCellBackgroundColor());
                chordsSelectorTableViewCell.SetChordNameColor(!flag
                    ? UIValuesPovider.GetCellTextColor()
                    : UIValuesPovider.GetCellAddedTextColor());
                chordsSelectorTableViewCell.SetAddButtonVisibility(false);
                chordsSelectorTableViewCell.SetRemoveButtonVisibility(flag);
            }
        }
    }

    public void topCellIndexChange(int topCellIndex)
    {
        chordsfastAccessor.updateHighlightedButton(topCellIndex);
    }

    public void onSelect(IChordsSelectorTableViewCell cell)
    {
        _selectedChordIndex = cell.GetCellIndex();
        chordsDrawer.DrawChord(allChords[_selectedChordIndex]);
        tableView.reloadVisiblesCell();
    }

    public void onAdd(IChordsSelectorTableViewCell cell)
    {
        freeModeChords.AddChord(allChords[cell.GetCellIndex()].GetMidiChord());
        tableView.reloadVisiblesCell();
    }

    public void onRemove(IChordsSelectorTableViewCell cell)
    {
        freeModeChords.RemoveChord(allChords[cell.GetCellIndex()].GetMidiChord());
        tableView.reloadVisiblesCell();
    }

    public void OnBackButtonClick()
    {
        router.RouteToFreeModeFromChordSelectior();
    }

    private void OnFastAccessorSelectChord(int chordId)
    {
        tableView.jumpToRow(chordId);
    }

    private void OnSelectedUITouched(IMidiChord chord)
    {
        var num = Array.FindIndex(allChords, c => c.GetMidiChord().IsEqual(chord));
        if (num != -1)
        {
            tableView.jumpToRow(num);
            _selectedChordIndex = num;
            chordsDrawer.DrawChord(allChords[_selectedChordIndex]);
            tableView.reloadVisiblesCell();
        }
    }

    private void OnSelectedChodsListChanged(IChordsList list)
    {
        tableView.reloadVisiblesCell();
    }
}