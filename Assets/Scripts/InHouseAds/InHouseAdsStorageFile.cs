using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

namespace InHouseAds
{
    public class InHouseAdsStorageFile : InHouseAdsStorage
    {
        private static readonly string InHouseNumberOfAdsShownDataPath =
            Application.persistentDataPath + "/in-house-number-of-ads-shown";

        public int ReadNumberOfAdsShown()
        {
            return ReadNumberOfAdsShown(InHouseNumberOfAdsShownDataPath);
        }

        public void WriteNumberOfAdsShown(int numberOfAdsShown)
        {
            WriteNumberOfAdsShown(InHouseNumberOfAdsShownDataPath, numberOfAdsShown);
        }

        private static int ReadNumberOfAdsShown(string dataPath)
        {
            if (!File.Exists(dataPath)) return 0;
            var serializationStream = File.OpenRead(dataPath);
            var binaryFormatter = new BinaryFormatter();
            return (int) binaryFormatter.Deserialize(serializationStream);
        }

        private static void WriteNumberOfAdsShown(string dataPath, int numberOfAdsShown)
        {
            var serializationStream = File.Create(dataPath);
            var binaryFormatter = new BinaryFormatter();
            binaryFormatter.Serialize(serializationStream, numberOfAdsShown);
        }
    }
}