namespace InHouseAds
{
    public enum InHouseAdsPlacement
    {
        InvalidPlacement,
        ExternalInterstitialAd,
        InternalStoreAd
    }
}