namespace InHouseAds
{
    public interface InHouseAdsStorage
    {
        int ReadNumberOfAdsShown();

        void WriteNumberOfAdsShown(int numberOfAdsShown);
    }
}