using System;
using GuitarApplication;
using Store;
using UnityEngine;
using Utils;

namespace InHouseAds
{
    public class InHouseAdsManagerPersistent : InHouseAdsManager
    {
        public const float MinimumTimeBetweenTwoInHouseAds = 30f;

        private readonly ApplicationStatusHolder _applicationStatusHolder;

        private readonly float _getCurrentTimeFunction;

        private readonly InAppManager _inappManager;

        private readonly InHouseAdsStorage _storage;

        private float _lastShowTime;

        private int _numberOfAdsShown;

        public InHouseAdsManagerPersistent(InHouseAdsStorage inHouseStorage, float getCurrentTimeFunction,
            InAppManager inappManager, ApplicationStatusHolder applicationStatusHolder)
        {
            Precondition.CheckNotNull(inHouseStorage);
            Precondition.CheckNotNull(getCurrentTimeFunction);
            Precondition.CheckNotNull(inappManager);
            Precondition.CheckNotNull(applicationStatusHolder);
            _storage = inHouseStorage;
            _getCurrentTimeFunction = getCurrentTimeFunction;
            _inappManager = inappManager;
            _applicationStatusHolder = applicationStatusHolder;
            _lastShowTime = _getCurrentTimeFunction;
            _numberOfAdsShown = _storage.ReadNumberOfAdsShown();
        }

        public bool ShowInHouseAdsIfAppropriate(Func<InHouseAdsPlacement, bool> showInHouseScreenFunction)
        {
            if (_inappManager.IsSubscribe()) return false;
            if (Application.internetReachability == NetworkReachability.NotReachable) return false;
            var num = _getCurrentTimeFunction;
            var num2 = num - _lastShowTime;
            var flag = num2 >= 30f;
            var nextPlacement = GetNextPlacement();
            if (flag && showInHouseScreenFunction(nextPlacement))
            {
                _numberOfAdsShown++;
                _storage.WriteNumberOfAdsShown(_numberOfAdsShown);
                _lastShowTime = num;
                return true;
            }

            return false;
        }

        private InHouseAdsPlacement GetNextPlacement()
        {
            if (_numberOfAdsShown < 5 && _applicationStatusHolder.IsDeployed())
                return InHouseAdsPlacement.InternalStoreAd;
            return InHouseAdsPlacement.ExternalInterstitialAd;
        }
    }
}