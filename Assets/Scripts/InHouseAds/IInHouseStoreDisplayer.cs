using System;

namespace InHouseAds
{
    public interface IInHouseStoreDisplayer
    {
        event Action OnInHouseStoreDismiss;

        void DisplayInHouseStore();
    }
}