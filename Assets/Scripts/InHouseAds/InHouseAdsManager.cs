using System;

namespace InHouseAds
{
    public interface InHouseAdsManager
    {
        bool ShowInHouseAdsIfAppropriate(Func<InHouseAdsPlacement, bool> showInHouseScreenFunction);
    }
}