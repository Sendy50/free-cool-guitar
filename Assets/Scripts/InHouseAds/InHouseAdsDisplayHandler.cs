using System;
using Ads;
using GuitarApplication;
using MWM.Ads;
using MWM.GameEvent;
using Store;
using Utils;

namespace InHouseAds
{
    public class InHouseAdsDisplayHandler
    {
        private readonly ApplicationRouter _applicationRouter;

        private readonly IGameEventSender _eventSender;

        private readonly ExternalAdsWrapper _externalAdsWrapper;

        private readonly InAppManager _inAppManager;
        private readonly IInHouseStoreDisplayer _storeDisplayer;

        public InHouseAdsDisplayHandler(IInHouseStoreDisplayer storeDisplayer, ExternalAdsWrapper externalAdsWrapper,
            IGameEventSender eventSender, InAppManager inAppManager, ApplicationRouter applicationRouter,
            string adPlacementId)
        {
            Precondition.CheckNotNull(storeDisplayer);
            Precondition.CheckNotNull(externalAdsWrapper);
            Precondition.CheckNotNull(eventSender);
            Precondition.CheckNotNull(inAppManager);
            Precondition.CheckNotNull(applicationRouter);
            Precondition.CheckNotNull(adPlacementId);
            _storeDisplayer = storeDisplayer;
            _externalAdsWrapper = externalAdsWrapper;
            _eventSender = eventSender;
            _inAppManager = inAppManager;
            _applicationRouter = applicationRouter;
            _inAppManager = inAppManager;
            this.adPlacementId = adPlacementId;
        }

        public string adPlacementId { get; }

        public event Action<InHouseAdsDisplayHandler> CloseDelegate;

        public void PreloadExternalAds()
        {
            if (!_inAppManager.IsSubscribe())
            {
                if (_externalAdsWrapper.IsInitialized())
                    PreloadAd();
                else
                    _externalAdsWrapper.RegisterToInitialization(OnAdsInit);
            }
        }

        public void Show(InHouseAdsPlacement placement, InHouseDisplaySource source)
        {
            switch (placement)
            {
                case InHouseAdsPlacement.InternalStoreAd:
                    ShowStore(source);
                    break;
                case InHouseAdsPlacement.ExternalInterstitialAd:
                    ShowAd();
                    break;
                default:
                    throw new Exception("InHouseAdsPlacement invalide");
            }
        }

        private void ShowStore(InHouseDisplaySource source)
        {
            switch (source)
            {
                case InHouseDisplaySource.EndGame:
                    _eventSender.SendEvent(EventType.StoreDisplay, StoreDisplaySource.EndGame.StringValue());
                    break;
                case InHouseDisplaySource.PauseMenu:
                    _eventSender.SendEvent(EventType.StoreDisplay, StoreDisplaySource.PauseMenu.StringValue());
                    break;
            }

            _storeDisplayer.OnInHouseStoreDismiss -= Close;
            _storeDisplayer.OnInHouseStoreDismiss += Close;
            _storeDisplayer.DisplayInHouseStore();
        }

        private void ShowAd()
        {
            _externalAdsWrapper.DisplayContent(PlacementType.Interstitial, adPlacementId,
                delegate(PlacementShowHideResult result)
                {
                    if (result == PlacementShowHideResult.ShowHideFailed)
                    {
                        Close();
                        PreloadAd();
                    }
                }, delegate
                {
                    OnAdsClosed();
                    PreloadAd();
                });
        }

        private void OnAdsInit()
        {
            _externalAdsWrapper.UnregisterToInitialization(OnAdsInit);
            PreloadAd();
        }

        private void PreloadAd()
        {
            _externalAdsWrapper.LoadContent(PlacementType.Interstitial, "InterstitialPlacement", delegate { });
        }

        private void OnAdsClosed()
        {
            Close();
        }

        private void Close()
        {
            _storeDisplayer.OnInHouseStoreDismiss -= Close;
            if (CloseDelegate != null) CloseDelegate(this);
        }
    }
}