public class IntroCarouselPersistenceWrapper : IIntroCarouselDisplayPolicy
{
    private readonly IntroCarouselPersistence _persistence;

    public IntroCarouselPersistenceWrapper()
    {
        _persistence = IntroCarouselPersistence.LoadFromFile();
    }

    public bool HasCarouselBeenCompleted()
    {
        return _persistence.DidCompleteCarousel;
    }

    public void OnCarouselCompleted()
    {
        _persistence.DidCompleteCarousel = true;
        _persistence.SaveToFile();
    }

    public bool HasFourthSlideBeenSeen()
    {
        return _persistence.HasUserSeenfourthSlide;
    }

    public void OnFourthSlideSeen()
    {
        _persistence.HasUserSeenfourthSlide = true;
        _persistence.SaveToFile();
    }
}