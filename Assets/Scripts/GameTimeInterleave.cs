using UnityEngine;

public class GameTimeInterleave
{
    public GameTimeInterleave(Vector2 timeInterleave, GameProgression gameProgression)
    {
        this.timeInterleave = timeInterleave;
        this.gameProgression = gameProgression;
    }

    public Vector2 timeInterleave { get; }

    public GameProgression gameProgression { get; }

    public bool IsOnInterleave()
    {
        return timeInterleave.x <= gameProgression.CurrentTime && timeInterleave.y >= gameProgression.CurrentTime;
    }

    public bool IsTooLate()
    {
        return NormalizeDistanceToMiddle() >= 1f;
    }

    public float Middle()
    {
        var num = timeInterleave.y - timeInterleave.x;
        var num2 = num * 0.5f;
        return timeInterleave.x + num2;
    }

    public float NormalizeDistanceToMiddle()
    {
        var currentTime = gameProgression.CurrentTime;
        var num = timeInterleave.y - timeInterleave.x;
        var num2 = num * 0.5f;
        var num3 = timeInterleave.x + num2;
        var num4 = currentTime - num3;
        return num4 / num2;
    }

    public float ClampedNormalizeDistanceToMiddle()
    {
        return Mathf.Clamp(NormalizeDistanceToMiddle(), -1f, 1f);
    }

    public float Accuracy()
    {
        var f = ClampedNormalizeDistanceToMiddle();
        return 1f - Mathf.Abs(f);
    }
}