using System;
using GuitarApplication;
using JetBrains.Annotations;
using MWM.Guitar.Quality;
using UnityEngine;
using UnityEngine.UI;

namespace MWM.Guitar.Settings
{
    public class SettingsScreen : MonoBehaviour
    {
        [SerializeField] private Canvas _canvas;

        [SerializeField] private Image _normalQualityOptionImage;

        [SerializeField] private Image _lowQualityOptionImage;

        [SerializeField] private Color _defaultImageColor;

        [SerializeField] private Color _selectedImageColor;

        [SerializeField] private Material _backgroundBlurMaterial;

        [SerializeField] private Image _backgroundPopup;

        private ApplicationRouter _applicationRouter;

        [CanBeNull] private Blur _blurObject;

        private QualityManager _qualityManager;

        private int _screentTextureId;

        private void Start()
        {
            _qualityManager = ApplicationGraph.GetQualityManager();
            _applicationRouter = ApplicationGraph.GetApplicationRouter();
            _screentTextureId = Shader.PropertyToID("_ScreenTexture");
        }

        public void Show()
        {
            _blurObject = _applicationRouter.GetBlurObject();
            if (_blurObject != null)
            {
                _backgroundPopup.material = _backgroundBlurMaterial;
                _blurObject.cameraBlur.onCameraBlurRenderImage += OnCameraBlurRenderImage;
            }
            else
            {
                _backgroundPopup.material = null;
            }

            _canvas.enabled = true;
            SyncUiWithCurrentQualityOption();
        }

        public void Hide()
        {
            _canvas.enabled = false;
            if (_blurObject != null) _blurObject.cameraBlur.onCameraBlurRenderImage -= OnCameraBlurRenderImage;
        }


        private void OnCameraBlurRenderImage(RenderTexture originalTex, RenderTexture blurTex, bool blurActive)
        {
            _backgroundBlurMaterial.SetTexture(_screentTextureId, !blurActive ? originalTex : blurTex);
        }


        [UsedImplicitly]
        public void OnNormalQualityOptionButtonClicked()
        {
            var qualityOption = _qualityManager.GetQualityOption();
            if (qualityOption != QualityOption.Normal)
            {
                _qualityManager.SetQualityOption(QualityOption.Normal);
                _applicationRouter.SetActiveBlurCamera(true);
                _backgroundPopup.material = _backgroundBlurMaterial;
                _blurObject = _applicationRouter.GetBlurObject();
                if (_blurObject != null) _blurObject.cameraBlur.onCameraBlurRenderImage += OnCameraBlurRenderImage;
                SyncUiWithCurrentQualityOption();
            }
        }


        [UsedImplicitly]
        public void OnLowQualityOptionButtonClicked()
        {
            if (_qualityManager.GetQualityOption() != 0)
            {
                _backgroundPopup.material = null;
                _applicationRouter.SetActiveBlurCamera(false);
                if (_blurObject != null)
                {
                    _blurObject.cameraBlur.onCameraBlurRenderImage -= OnCameraBlurRenderImage;
                    _blurObject = null;
                }

                _qualityManager.SetQualityOption(QualityOption.Low);
                SyncUiWithCurrentQualityOption();
            }
        }

        private void SyncUiWithCurrentQualityOption()
        {
            switch (_qualityManager.GetQualityOption())
            {
                case QualityOption.Low:
                    _normalQualityOptionImage.color = _defaultImageColor;
                    _lowQualityOptionImage.color = _selectedImageColor;
                    break;
                case QualityOption.Normal:
                    _normalQualityOptionImage.color = _selectedImageColor;
                    _lowQualityOptionImage.color = _defaultImageColor;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}