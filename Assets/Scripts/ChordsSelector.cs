using System;
using System.Linq;
using GuitarApplication;
using JetBrains.Annotations;
using MWM.Guitar.FreeMode;
using UnityEngine;
using UnityEngine.UI;

public class ChordsSelector : MonoBehaviour, IChordSelectionCellUIValuesProvider
{
    public TableView tableView;

    public ChordsFastAccessor chordsfastAccessor;

    public ChordsDrawer chordsDrawer;

    public SelectCordsControl selectCordsControl;

    public Material cellBackgroundBlurMaterial;

    public Image chordsSelectorTableBackground;

    public Image chordsFastAccessorBackground;

    public Image chordsDrawerBackground;

    [Space(10f)] [Header("Color")] public Color cellBackgroundColor;

    public Color cellSelectedBackgroundColor;

    public Color cellSelectedTextColor;

    public Color cellTextColor;

    public Color cellAddedTextColor;

    public ChordsSelectorPresenter presenter { get; private set; }

    private void Start()
    {
        presenter = new ChordsSelectorPresenter(
            ChordMap.Map.Values.Where(c => !c.Chord.IsSolo && !c.Chord.IsEmptyChord())
                .Select((Func<ChordContainer, IChordContainer>) (c => c)).ToArray(), SelectedChordsList.DefaultList(),
            selectCordsControl, tableView, chordsDrawer, this, chordsfastAccessor,
            ApplicationGraph.GetApplicationRouter());
    }

    private void OnEnable()
    {
        // QualityManager qualityManager = ApplicationGraph.GetQualityManager();
        // if (qualityManager.GetQualityOption() != 0)
        // {
        // 	chordsSelectorTableBackground.material = cellBackgroundBlurMaterial;
        // 	chordsFastAccessorBackground.material = cellBackgroundBlurMaterial;
        // 	chordsDrawerBackground.material = cellBackgroundBlurMaterial;
        // 	int screentTextureId = Shader.PropertyToID("_ScreenTexture");
        // 	ApplicationRouter applicationRouter = ApplicationGraph.GetApplicationRouter();
        // 	applicationRouter.GetBlurObject().cameraBlur.onCameraBlurRenderImage += delegate(RenderTexture originalTex, RenderTexture blurTex, bool blurActive)
        // 	{
        // 		cellBackgroundBlurMaterial.SetTexture(screentTextureId, (!blurActive) ? originalTex : blurTex);
        // 	};
        // }
        // else
        // {
        chordsSelectorTableBackground.material = null;
        chordsFastAccessorBackground.material = null;
        chordsDrawerBackground.material = null;
        // }
    }

    public Color GetCellBackgroundColor()
    {
        return cellBackgroundColor;
    }

    public Color GetCellSelectedBackgroundColor()
    {
        return cellSelectedBackgroundColor;
    }

    public Color GetCellSelectedTextColor()
    {
        return cellSelectedTextColor;
    }

    public Color GetCellTextColor()
    {
        return cellTextColor;
    }

    public Color GetCellAddedTextColor()
    {
        return cellAddedTextColor;
    }

    [UsedImplicitly]
    public void OnBackButtonClick()
    {
        presenter.OnBackButtonClick();
    }
}