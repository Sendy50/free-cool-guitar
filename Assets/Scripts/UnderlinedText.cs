using UnityEngine;
using UnityEngine.UI;

public class UnderlinedText : MonoBehaviour
{
    [SerializeField] private Text _text;

    [SerializeField] private Image _underlineImage;

    private void Start()
    {
        FixUnderlineSize();
    }

    public void FixUnderlineSize()
    {
        var sizeDelta = _underlineImage.rectTransform.sizeDelta;
        sizeDelta.x = _text.preferredWidth;
        _underlineImage.rectTransform.sizeDelta = sizeDelta;
    }
}