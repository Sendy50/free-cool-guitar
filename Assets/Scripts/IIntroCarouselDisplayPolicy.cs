public interface IIntroCarouselDisplayPolicy
{
    bool HasCarouselBeenCompleted();

    void OnCarouselCompleted();

    bool HasFourthSlideBeenSeen();

    void OnFourthSlideSeen();
}