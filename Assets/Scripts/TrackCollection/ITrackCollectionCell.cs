using FirstUseIndicator;

namespace TrackCollection
{
    public interface ITrackCollectionCell : ITableViewCell
    {
        ITrackCollectionCellDelegate cellDelegate { get; set; }

        void SetTitle(string title);

        void SetArtist(string artist);

        void SetStoreText(string score);

        void SetNbStars(float nbStars);

        void SetUnlockButtonVisibility(bool visibility);

        void SetPlayButtonVisibility(bool visibility);

        void SetVIPButtonVisibility(bool visibility);

        void SetNewBadgeVisibility(bool visibility);

        IFirstUseIndicator GetFirstUseIndicator();
    }
}