using GuitarApplication;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.UI;

namespace TrackCollection
{
    public class TrackCollectionScreen : MonoBehaviour
    {
        public TableView tableView;

        public Image tableBackground;

        public Material backgroundBlurMaterial;

        public TrackCollectionScreenPresenter _presenter;

        public void Start()
        {
            var applicationRouter = ApplicationGraph.GetApplicationRouter();
            var inappManager = ApplicationGraph.GetInappManager();
            var scoreManager = ApplicationGraph.GetScoreManager();
            var songIdsPlayedStorage = ApplicationGraph.GetSongIdsPlayedStorage();
            var tracks = ApplicationGraph.GetSongRepository().GetSongs().ToArray();
            var additionalContentProvider = ApplicationGraph.GetAdditionalContentProvider();
            var cellDelegate =
                new TrackCollectionCellDelegate(tracks, applicationRouter, ApplicationGraph.GetSongIdsPlayedStorage());
            _presenter = new TrackCollectionScreenPresenter(tableView, tracks, inappManager, scoreManager,
                applicationRouter, cellDelegate, songIdsPlayedStorage, additionalContentProvider);
        }

        private void OnEnable()
        {
            var qualityManager = ApplicationGraph.GetQualityManager();
            var applicationRouter = ApplicationGraph.GetApplicationRouter();
            // if (qualityManager.GetQualityOption() != 0)
            // {
            // 	//tableBackground.material = backgroundBlurMaterial;
            // 	int screenTextureId = Shader.PropertyToID("_ScreenTexture");
            // 	applicationRouter.GetBlurObject().cameraBlur.onCameraBlurRenderImage += delegate(RenderTexture originalTex, RenderTexture blurTex, bool blurActive)
            // 	{
            // 		tableBackground.material.SetTexture(screenTextureId, (!blurActive) ? originalTex : blurTex);
            // 	};
            // }
            // else
            // {
            tableBackground.material = null;
            // }
            if (_presenter != null) _presenter.OnEnable();
        }

        [UsedImplicitly]
        public void OnBackButtonClicked()
        {
            _presenter.OnBackButtonClicked();
        }

        [UsedImplicitly]
        public void OnUnlockAllClicked()
        {
            _presenter.OnUnlockAllClicked();
        }
    }
}