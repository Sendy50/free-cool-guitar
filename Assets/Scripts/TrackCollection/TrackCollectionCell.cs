using FirstUseIndicator;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.UI;

namespace TrackCollection
{
    public class TrackCollectionCell : TableViewCell, ITrackCollectionCell, ITableViewCell
    {
        public Text trackTitleText;

        public Text trackSubtitleText;

        public Image firstStarImage;

        public Image secStarImage;

        public Image thirdStarImage;

        public Text trackScoreText;

        public Button playButton;

        public Button unlockButton;

        public Button vipButton;

        public GameObject newBadge;

        public FirstUseAnnimation firstUseAnnimation;

        public ITrackCollectionCellDelegate cellDelegate { get; set; }

        public void SetTitle(string title)
        {
            trackTitleText.text = title;
        }

        public void SetArtist(string artist)
        {
            trackSubtitleText.text = artist;
        }

        public void SetStoreText(string score)
        {
            trackScoreText.text = score;
        }

        public void SetNbStars(float nbStars)
        {
            firstStarImage.fillAmount = Mathf.Min(1f, nbStars);
            secStarImage.fillAmount = Mathf.Min(1f, Mathf.Max(0f, nbStars - 1f));
            thirdStarImage.fillAmount = Mathf.Min(1f, Mathf.Max(0f, nbStars - 2f));
        }

        public void SetUnlockButtonVisibility(bool visibility)
        {
            unlockButton.gameObject.SetActive(visibility);
        }

        public void SetPlayButtonVisibility(bool visibility)
        {
            playButton.gameObject.SetActive(visibility);
        }

        public void SetVIPButtonVisibility(bool visibility)
        {
            vipButton.gameObject.SetActive(visibility);
        }

        public void SetNewBadgeVisibility(bool visibility)
        {
            newBadge.SetActive(visibility);
        }

        public IFirstUseIndicator GetFirstUseIndicator()
        {
            return firstUseAnnimation;
        }

        [UsedImplicitly]
        public void onPlayButtonClicked()
        {
            if (cellDelegate != null) cellDelegate.OnPlayButtonClicked(this);
        }

        [UsedImplicitly]
        public void onUnlockButtonClicked()
        {
            if (cellDelegate != null) cellDelegate.OnUnlockButtonClicked(this);
        }

        [UsedImplicitly]
        public void onVipButtonClicked()
        {
            if (cellDelegate != null) cellDelegate.OnVipButtonClicked(this);
        }

        [UsedImplicitly]
        public void onCellButtonClicked()
        {
            if (cellDelegate != null) cellDelegate.OnCellButtonClicked(this);
        }
    }
}