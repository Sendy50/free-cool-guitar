using System.Collections.Generic;
using GuitarApplication;
using Song;
using SongCatalog;
using UnityEngine;
using Utils;

namespace TrackCollection
{
    public class TrackCollectionCellDelegate : ITrackCollectionCellDelegate
    {
        // private readonly InAppManager _inAppManager;

        private readonly ApplicationRouter _router;

        // private readonly ExternalAdsWrapper _externalAdsWrapper;

        private readonly SongIdsPlayedStorage _songIdsPlayedStorage;

        private readonly HashSet<string> _songsIdsPlayed;
        private readonly ISong[] _tracks;

        public TrackCollectionCellDelegate(ISong[] tracks, ApplicationRouter router,
            SongIdsPlayedStorage songIdsPlayedStorage)
        {
            Precondition.CheckNotNull(tracks);
            // Precondition.CheckNotNull(inAppManager);
            Precondition.CheckNotNull(router);
            // Precondition.CheckNotNull(externalAdsWrapper);
            Precondition.CheckNotNull(songIdsPlayedStorage);
            _tracks = tracks;
            // _inAppManager = inAppManager;
            _router = router;
            // _externalAdsWrapper = externalAdsWrapper;
            _songIdsPlayedStorage = songIdsPlayedStorage;
            _songsIdsPlayed = _songIdsPlayedStorage.ReadSongsIdsPlayed();
        }

        public void OnPlayButtonClicked(ITrackCollectionCell cell)
        {
            var song = _tracks[cell.GetCellIndex()];
            if (song.GetIsTutorialTrack())
            {
                var firstUseIndicator = cell.GetFirstUseIndicator();
                if (firstUseIndicator.GetIsNeverUsed()) firstUseIndicator.ValidateFirstUse();
            }

            ManagePlayedSongs(song.GetId());
            _router.RouteToInGameFromTrackList(song);
        }

        public void OnUnlockButtonClicked(ITrackCollectionCell cell)
        {
            var track = _tracks[cell.GetCellIndex()];
            DisplayRewardedAdsAndUnlockTrack(track);
        }

        public void OnVipButtonClicked(ITrackCollectionCell cell)
        {
            _router.RouteToStoreFromClickVipTrackListCell();
        }

        public void OnCellButtonClicked(ITrackCollectionCell cell)
        {
            // if (_inAppManager.IsSubscribe())
            // if (IAPManager.Instance.IsPrimeMember())
            // {
                OnPlayButtonClicked(cell);
                return;
            // }

            var song = _tracks[cell.GetCellIndex()];
            switch (song.GetSongType())
            {
                case SongType.Free:
                    OnPlayButtonClicked(cell);
                    break;
                case SongType.Unlockable:
                    OnUnlockButtonClicked(cell);
                    break;
                case SongType.Vip:
                    OnVipButtonClicked(cell);
                    break;
            }
        }

        private void ManagePlayedSongs(string songId)
        {
            if (_songsIdsPlayed.Add(songId)) _songIdsPlayedStorage.WriteSongsIdsPlayed(_songsIdsPlayed);
        }

        private void DisplayRewardedAdsAndUnlockTrack(ISong track)
        {
            //_router.ShowLoader();
            // _externalAdsWrapper.LoadContent(PlacementType.RewardVideo, "RewardedVideoPlacement", delegate(PlacementLoadingState loadingState)
            // {
            // 	switch (loadingState)
            // 	{
            // 	case PlacementLoadingState.Loading:
            // 		break;
            // 	case PlacementLoadingState.NotLoaded:
            // 	case PlacementLoadingState.Failure:
            // 	case PlacementLoadingState.Expired:
            // 		_router.HideLoader();
            // 		ShowLoadingRewardError();
            // 		break;
            // 	case PlacementLoadingState.Loaded:
            // 		_externalAdsWrapper.DisplayContent(PlacementType.RewardVideo, "RewardedVideoPlacement", delegate(PlacementShowHideResult result)
            // 		{
            // 			if (result == PlacementShowHideResult.ShowHideFailed)
            // 			{
            // 				_router.HideLoader();
            // 				ShowLoadingRewardError();
            // 			}
            // 		}, delegate(PlacementInteractionState interaction)
            // 		{
            // 			_router.HideLoader();
            // 			if (interaction == PlacementInteractionState.Rewarded)
            // 			{
            ManagePlayedSongs(track.GetId());
            _router.RouteToInGameFromTrackList(track);
            // 			}
            // 		});
            // 		break;
            // 	default:
            // 		throw new ArgumentOutOfRangeException("loadingState", loadingState, null);
            // 	}
            // });
            // _externalAdsWrapper.LoadContent(PlacementType.RewardVideo, "RewardedVideoPlacement", delegate(PlacementLoadingState loadingState)
            // {
            // 	switch (loadingState)
            // 	{
            // 	case PlacementLoadingState.Loading:
            // 		break;
            // 	case PlacementLoadingState.NotLoaded:
            // 	case PlacementLoadingState.Failure:
            // 	case PlacementLoadingState.Expired:
            // 		_router.HideLoader();
            // 		ShowLoadingRewardError();
            // 		break;
            // 	case PlacementLoadingState.Loaded:
            // 		_externalAdsWrapper.DisplayContent(PlacementType.RewardVideo, "RewardedVideoPlacement", delegate(PlacementShowHideResult result)
            // 		{
            // 			if (result == PlacementShowHideResult.ShowHideFailed)
            // 			{
            // 				_router.HideLoader();
            // 				ShowLoadingRewardError();
            // 			}
            // 		}, delegate(PlacementInteractionState interaction)
            // 		{
            // 			_router.HideLoader();
            // 			if (interaction == PlacementInteractionState.Rewarded)
            // 			{
            // 				ManagePlayedSongs(track.GetId());
            // 				_router.RouteToInGameFromTrackList(track);
            // 			}
            // 		});
            // 		break;
            // 	default:
            // 		throw new ArgumentOutOfRangeException("loadingState", loadingState, null);
            // 	}
            // });
        }

        private static void ShowLoadingRewardError()
        {
            var mNPopup = new MNPopup("Error", "An error occurred");
            mNPopup.AddAction("Ok", delegate { });
            mNPopup.AddDismissListener(delegate { Debug.Log("dismiss listener"); });
            mNPopup.Show();
        }
    }
}