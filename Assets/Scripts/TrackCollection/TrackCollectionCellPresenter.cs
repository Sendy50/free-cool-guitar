using System.Collections.Generic;
using Song;
using SongCatalog;
using UnityEngine;
using Utils;

namespace TrackCollection
{
    public class TrackCollectionCellPresenter
    {
        private readonly List<string> _newSongsIds;
        // private readonly InAppManager _inAppManager;

        private readonly IScoreManager _scoreManager;

        private readonly SongIdsPlayedStorage _songIdsPlayedStorage;

        public TrackCollectionCellPresenter(IScoreManager scoreManager, SongIdsPlayedStorage songIdsPlayedStorage,
            List<string> newSongsIds)
        {
            // Precondition.CheckNotNull(inAppManager);
            Precondition.CheckNotNull(scoreManager);
            Precondition.CheckNotNull(songIdsPlayedStorage);
            Precondition.CheckNotNull(newSongsIds);
            // _inAppManager = inAppManager;
            _scoreManager = scoreManager;
            _songIdsPlayedStorage = songIdsPlayedStorage;
            _newSongsIds = newSongsIds;
        }

        public void UpdateCell(ITrackCollectionCell cell, ISong song)
        {
            cell.SetTitle(song.GetName());
            cell.SetArtist(song.GetArtist());
            UpdateScore(cell, song, _scoreManager);
            // if (_inAppManager.IsSubscribe())
            // if (IAPManager.Instance.IsPrimeMember())
                SetUIForSubscribedUser(cell);
            // else
            //     SetUIForUnsubscribedUser(cell, song);
            
            PlayFirstUseIndicatorIfNeeded(cell, song);
            cell.SetNewBadgeVisibility(IsSongNew(song));
        }

        private bool IsSongNew(ISong song)
        {
            return _newSongsIds.Contains(song.GetId()) &&
                   !_songIdsPlayedStorage.ReadSongsIdsPlayed().Contains(song.GetId());
        }

        private void SetUIForSubscribedUser(ITrackCollectionCell cell)
        {
            cell.SetPlayButtonVisibility(true);
            cell.SetUnlockButtonVisibility(false);
            cell.SetVIPButtonVisibility(false);
        }

        private void SetUIForUnsubscribedUser(ITrackCollectionCell cell, ISong track)
        {
            switch (track.GetSongType())
            {
                case SongType.Free:
                    cell.SetPlayButtonVisibility(true);
                    cell.SetUnlockButtonVisibility(false);
                    cell.SetVIPButtonVisibility(false);
                    break;
                case SongType.Unlockable:
                    cell.SetPlayButtonVisibility(false);
                    cell.SetUnlockButtonVisibility(true);
                    cell.SetVIPButtonVisibility(false);
                    break;
                case SongType.Vip:
                    cell.SetPlayButtonVisibility(false);
                    cell.SetUnlockButtonVisibility(false);
                    cell.SetVIPButtonVisibility(true);
                    break;
            }
        }

        private void UpdateScore(ITrackCollectionCell cell, ISong track, IScoreManager scoreManager)
        {
            Debug.LogError("UpdateScore trackname : " + track.GetName());
            cell.SetStoreText(scoreManager.GetScoreForTrack(track).ToString());
            cell.SetNbStars(scoreManager.GetNumberOfStarsForTrack(track));
        }

        private void PlayFirstUseIndicatorIfNeeded(ITrackCollectionCell cell, ISong track)
        {
            var firstUseIndicator = cell.GetFirstUseIndicator();
            if (!track.GetIsTutorialTrack())
                firstUseIndicator.HideIndication();
            else if (!firstUseIndicator.GetIsNeverUsed())
                firstUseIndicator.HideIndication();
            else
                firstUseIndicator.PlayIndication();
        }
    }
}