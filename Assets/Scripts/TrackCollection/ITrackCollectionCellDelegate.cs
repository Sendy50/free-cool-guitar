namespace TrackCollection
{
    public interface ITrackCollectionCellDelegate
    {
        void OnPlayButtonClicked(ITrackCollectionCell cell);

        void OnUnlockButtonClicked(ITrackCollectionCell cell);

        void OnVipButtonClicked(ITrackCollectionCell cell);

        void OnCellButtonClicked(ITrackCollectionCell cell);
    }
}