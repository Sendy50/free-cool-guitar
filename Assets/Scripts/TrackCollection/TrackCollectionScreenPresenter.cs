using AdditionalContent;
using GuitarApplication;
using Song;
using SongCatalog;
using Store;
using Utils;

namespace TrackCollection
{
    public class TrackCollectionScreenPresenter : TableViewDelegate
    {
        private readonly ITrackCollectionCellDelegate _cellDelegate;

        private readonly TrackCollectionCellPresenter _cellPresenter;

        private readonly ApplicationRouter _router;

        // private readonly InAppManager _inAppManager;

        private readonly IScoreManager _scoreManager;
        public readonly ITableView _tableView;

        private readonly ISong[] _tracks;

        public TrackCollectionScreenPresenter(ITableView tableView, ISong[] tracks, InAppManager inAppManager,
            IScoreManager scoreManager, ApplicationRouter router, ITrackCollectionCellDelegate cellDelegate,
            SongIdsPlayedStorage songIdsPlayedStorage, AdditionalContentProvider additionalContentProvider)
        {
            Precondition.CheckNotNull(tableView);
            Precondition.CheckNotNull(tracks);
            // Precondition.CheckNotNull(inAppManager);
            Precondition.CheckNotNull(scoreManager);
            Precondition.CheckNotNull(router);
            Precondition.CheckNotNull(cellDelegate);
            Precondition.CheckNotNull(songIdsPlayedStorage);
            Precondition.CheckNotNull(additionalContentProvider);
            _tableView = tableView;
            _tracks = tracks;
            // _inAppManager = inAppManager;
            _scoreManager = scoreManager;
            _router = router;
            _cellDelegate = cellDelegate;
            _cellPresenter = new TrackCollectionCellPresenter(scoreManager, songIdsPlayedStorage,
                additionalContentProvider.GetNewSongs());
            tableView.tableViewDelegate = this;
            tableView.startTableView();
            RegisterObservations();
        }

        public int numberOfRows()
        {
            return _tracks.Length;
        }

        public void configureTableViewCellAtIndex(ITableViewCell cell, int index)
        {
            var trackCollectionCell = cell as ITrackCollectionCell;
            var song = _tracks[index];
            trackCollectionCell.cellDelegate = _cellDelegate;
            _cellPresenter.UpdateCell(trackCollectionCell, song);
        }

        public void topCellIndexChange(int topCellIndex)
        {
        }

        public void OnEnable()
        {
            if (_tableView != null) _tableView.reloadVisiblesCell();
        }

        private void RegisterObservations()
        {
            _scoreManager.OnMaxScoreChanged += delegate { _tableView.reloadVisiblesCell(); };
            // _inAppManager.OnInAppsUnlockedDelegate += delegate
            // {
            // 	_tableView.reloadVisiblesCell();
            // };
            // _inAppManager.OnInAppsLockedDelegate += delegate
            // {
            // 	_tableView.reloadVisiblesCell();
            // };
        }

        public void OnBackButtonClicked()
        {
            _router.RouteToFreeModeFromTrackList();
        }

        public void OnUnlockAllClicked()
        {
            _router.RouteToStoreFromTrackListNavBar();
        }
    }
}