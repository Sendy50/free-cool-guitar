public struct ChordNameCorrespondance
{
    public int chordId;

    public int[] MidiNumbers;

    public string key;

    public string suffix;

    public int[] Fingers;

    public bool IsSolo;

    public string Name => key + " " + suffix;
}