public struct ChordContainer : IChordContainer
{
    public MidiChord MidiChord;

    public Chord Chord;

    public IMidiChord GetMidiChord()
    {
        return MidiChord;
    }

    public IChord GetChord()
    {
        return Chord;
    }
}