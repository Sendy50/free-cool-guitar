namespace MWM.Audio.Sampler
{
    public class Constants
    {
        public const string EndCanvasApplauseAudioFileId = "1";

        public const string EndCanvasFirstStarsFilledAudioFileId = "2";

        public const string EndCanvasLastStarFilledAudioFileId = "3";

        public const string EndCanvasScoreIncrementAudioFileId = "4";

        public const string ClickAudioFileId = "5";
    }
}