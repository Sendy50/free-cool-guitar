using System.Collections.Generic;
using UnityEngine;

public class Smear : MonoBehaviour
{
    public int FramesBufferSize;

    public Renderer Renderer;

    private readonly Queue<Vector3> m_recentPositions = new Queue<Vector3>();

    private Material InstancedMaterial { get; set; }

    private void Start()
    {
        InstancedMaterial = Renderer.material;
    }

    private void LateUpdate()
    {
        if (m_recentPositions.Count > FramesBufferSize)
            InstancedMaterial.SetVector("_PrevPosition", m_recentPositions.Dequeue());
        InstancedMaterial.SetVector("_Position", transform.position);
        m_recentPositions.Enqueue(transform.position);
    }
}