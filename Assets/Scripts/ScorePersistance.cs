using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

[Serializable]
public class ScorePersistance
{
    public Dictionary<string, int> maxScore = new Dictionary<string, int>();

    public static ScorePersistance loadFromFile()
    {
        if (File.Exists(Application.persistentDataPath + "/score.dat"))
        {
            var binaryFormatter = new BinaryFormatter();
            var serializationStream = File.Open(Application.persistentDataPath + "/score.dat", FileMode.Open);
            return (ScorePersistance) binaryFormatter.Deserialize(serializationStream);
        }

        return new ScorePersistance();
    }

    public void saveToFile()
    {
        var binaryFormatter = new BinaryFormatter();
        var serializationStream = File.Create(Application.persistentDataPath + "/score.dat");
        binaryFormatter.Serialize(serializationStream, this);
    }
}