using System.Collections;
using UnityEngine;

public class StrokValidator : IUserActionValidator
{
    private Coroutine _timeoutCoroutine;

    public StrokValidator(MonoBehaviour monoBehaviour)
    {
        this.monoBehaviour = monoBehaviour;
        validationStatus = UserActionValidationStatus.unstarted;
    }

    public MonoBehaviour monoBehaviour { get; }

    public int stringIndexToDetect { get; private set; }

    public UserActionValidationStatus validationStatus { get; private set; }

    public GameTimeInterleave interleave { get; private set; }

    public void StartDetection()
    {
        if (interleave.IsTooLate())
        {
            validationStatus = UserActionValidationStatus.unvalidated;
        }
        else if (_timeoutCoroutine == null)
        {
            validationStatus = UserActionValidationStatus.detecting;
            _timeoutCoroutine = monoBehaviour.StartCoroutine(TimeoutRoutine(interleave));
        }
    }

    public UserActionValidationTouchStringResponse OnStringTouched(int stringIndex)
    {
        if (!interleave.IsOnInterleave() || validationStatus != UserActionValidationStatus.detecting)
            return UserActionValidationTouchStringResponse.notOnTiming;
        if (stringIndexToDetect != stringIndex) return UserActionValidationTouchStringResponse.stringIsNotValide;
        validationStatus = UserActionValidationStatus.validated;
        StopDetection();
        return UserActionValidationTouchStringResponse.stringIsValide;
    }

    public void SetupDetection(int stringIndexToDetect, GameTimeInterleave interleave)
    {
        this.stringIndexToDetect = stringIndexToDetect;
        this.interleave = interleave;
    }

    public void StopDetection()
    {
        if (_timeoutCoroutine != null)
        {
            monoBehaviour.StopCoroutine(_timeoutCoroutine);
            _timeoutCoroutine = null;
        }
    }

    private IEnumerator TimeoutRoutine(GameTimeInterleave interleave)
    {
        yield return new WaitUntil(() => interleave.IsOnInterleave());
        yield return new WaitUntil(() => !interleave.IsOnInterleave());
        validationStatus = UserActionValidationStatus.unvalidated;
        _timeoutCoroutine = null;
    }
}