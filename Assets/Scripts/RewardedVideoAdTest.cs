// using AudienceNetwork;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class RewardedVideoAdTest : MonoBehaviour
{
    public Text statusLabel;

    private bool isLoaded;
    // private RewardedVideoAd rewardedVideoAd;

    private void OnDestroy()
    {
        // if (rewardedVideoAd != null) rewardedVideoAd.Dispose();
        // Debug.Log("RewardedVideoAdTest was destroyed!");
    }

    public void LoadRewardedVideo()
    {
        statusLabel.text = "Loading rewardedVideo ad...";
        // var rewardedVideoAd = this.rewardedVideoAd = new RewardedVideoAd("YOUR_PLACEMENT_ID");
        // this.rewardedVideoAd.Register(gameObject);
        // this.rewardedVideoAd.RewardedVideoAdDidLoad = delegate
        // {
        //     Debug.Log("RewardedVideo ad loaded.");
        //     isLoaded = true;
        //     statusLabel.text = "Ad loaded. Click show to present!";
        // };
        // rewardedVideoAd.RewardedVideoAdDidFailWithError = delegate(string error)
        // {
        //     Debug.Log("RewardedVideo ad failed to load with error: " + error);
        //     statusLabel.text = "RewardedVideo ad failed to load. Check console for details.";
        // };
        // rewardedVideoAd.RewardedVideoAdWillLogImpression = delegate
        // {
        //     Debug.Log("RewardedVideo ad logged impression.");
        // };
        // rewardedVideoAd.RewardedVideoAdDidClick = delegate { Debug.Log("RewardedVideo ad clicked."); };
        // this.rewardedVideoAd.LoadAd();
    }

    public void ShowRewardedVideo()
    {
        if (isLoaded)
        {
            // rewardedVideoAd.Show();
            isLoaded = false;
            statusLabel.text = string.Empty;
        }
        else
        {
            statusLabel.text = "Ad not loaded. Click load to request an ad.";
        }
    }

    public void NextScene()
    {
        SceneManager.LoadScene("InterstitialAdScene");
    }
}