using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public class PersistableSO
{
    private readonly string filePath;

    public PersistableSO(string persisterName)
    {
        filePath = Application.persistentDataPath + $"/{persisterName}.pso";
    }

    public void Load(object objectOutput)
    {
        if (File.Exists(filePath))
        {
            var binaryFormatter = new BinaryFormatter();
            var fileStream = File.Open(filePath, FileMode.Open);
            JsonUtility.FromJsonOverwrite((string) binaryFormatter.Deserialize(fileStream), objectOutput);
            fileStream.Close();
        }
    }

    public void Save(object objectToSave)
    {
        var binaryFormatter = new BinaryFormatter();
        var fileStream = File.Create(filePath);
        var graph = JsonUtility.ToJson(objectToSave);
        binaryFormatter.Serialize(fileStream, graph);
        fileStream.Close();
    }
}