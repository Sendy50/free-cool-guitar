namespace Subscription
{
    public interface SubscriptionManager
    {
        void SetSku(string sku);

        string GetSku();

        SubscriptionDetails GetSubscriptionDetails();
    }
}