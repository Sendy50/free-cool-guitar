using JetBrains.Annotations;
using Utils;

namespace Subscription
{
    public class SubscriptionDetails
    {
        private readonly int _freeTrialPeriod;
        private readonly string _price;

        private readonly SubscriptionDuration _subscriptionDuration;

        public SubscriptionDetails(string price, int freeTrialPeriod, SubscriptionDuration subscriptionDuration)
        {
            Precondition.CheckNotNull(price);
            _price = price;
            _freeTrialPeriod = freeTrialPeriod;
            _subscriptionDuration = subscriptionDuration;
        }

        [NotNull]
        public string GetPrice()
        {
            return _price;
        }

        public int GetFreeTrialPeriod()
        {
            return _freeTrialPeriod;
        }

        public SubscriptionDuration GetSubscriptionDuration()
        {
            return _subscriptionDuration;
        }
    }
}