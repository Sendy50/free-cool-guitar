using System;
using Devices;
using JetBrains.Annotations;
using Store;
using Utils;

namespace Subscription
{
    public class SubscriptionManagerImpl : SubscriptionManager
    {
        private readonly DeviceManager _deviceManager;
        private readonly InAppManager _inAppManager;

        private string _sku;

        private SubscriptionDetails _subscriptionDetails;

        public SubscriptionManagerImpl(InAppManager inAppManager, DeviceManager deviceManager, string defaultSku)
        {
            Precondition.CheckNotNull(inAppManager);
            Precondition.CheckNotNull(deviceManager);
            Precondition.CheckNotNull(defaultSku);
            _inAppManager = inAppManager;
            _deviceManager = deviceManager;
            _sku = defaultSku;
            if (_inAppManager.GetStatus() != Status.Initialized)
                _inAppManager.OnInAppInitializedDelegate += OnInAppManagerInitialized;
            else
                OnInAppManagerInitialized();
        }

        public void SetSku(string sku)
        {
            Precondition.CheckNotNull(sku);
            _sku = sku;
            if (_inAppManager.GetStatus() == Status.Initialized) GenerateSkuDetails();
        }

        [NotNull]
        public string GetSku()
        {
            return _sku;
        }

        [NotNull]
        public SubscriptionDetails GetSubscriptionDetails()
        {
            return _subscriptionDetails;
        }

        private void OnInAppManagerInitialized()
        {
            GenerateSkuDetails();
        }

        private void GenerateSkuDetails()
        {
            var productDetails = _inAppManager.ProductDetailsForId(_sku);
            var deviceTarget = _deviceManager.GetDeviceTarget();
            var subscriptionAbTestSkuPrefix = InAppConst.GetSubscriptionAbTestSkuPrefix(deviceTarget);
            var price = productDetails != null
                ? productDetails.GetReadablePrice()
                : SkuUtils.GetPriceFromSku(_sku, deviceTarget, "unknownPrice");
            if (_sku.StartsWith(subscriptionAbTestSkuPrefix))
            {
                var text = _sku.Substring(subscriptionAbTestSkuPrefix.Length);
                var array = text.Split('.');
                var duration = array[1];
                var s = array[2];
                var subscriptionDuration = ParseSubscriptionDuration(duration);
                var freeTrialPeriod = int.Parse(s);
                _subscriptionDetails = new SubscriptionDetails(price, freeTrialPeriod, subscriptionDuration);
            }
            else
            {
                _subscriptionDetails = new SubscriptionDetails(price, 0, SubscriptionDuration.Unknown);
            }
        }

        private static SubscriptionDuration ParseSubscriptionDuration(string duration)
        {
            switch (duration)
            {
                case "weekly":
                    return SubscriptionDuration.Weekly;
                case "monthly":
                    return SubscriptionDuration.Monthly;
                case "annually":
                    return SubscriptionDuration.Annually;
                default:
                    throw new Exception("Unknown subscription duration : " + duration);
            }
        }
    }
}