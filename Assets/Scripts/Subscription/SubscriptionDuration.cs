namespace Subscription
{
    public enum SubscriptionDuration
    {
        Weekly,
        Monthly,
        Annually,
        Unknown
    }
}