using System;
using AssetBundles;

namespace IntroCarouselRessources
{
    public class ABTestRessourceApplyerBuilder : IRessourceApplyerBuilder
    {
        public IRessourceApplyer Build(IRessoucesProvider ressouceProvider, IntroCarouselSlide slide)
        {
            switch (ressouceProvider.GetRessourceKind())
            {
                case IntroCarouselRessourceKind.Video:
                    return new VidoRessouceApplyer(slide, ressouceProvider.GetVideoClipForSlide(slide.GetSlideNumber()),
                        ressouceProvider.GetVideoTextureForSlide(slide.GetSlideNumber()));
                case IntroCarouselRessourceKind.Image:
                    return new ImageRessouceApplyer(slide, ressouceProvider.GetSpriteForSlide(slide.GetSlideNumber()));
                default:
                    throw new Exception("Ressource kind undefined");
            }
        }
    }
}