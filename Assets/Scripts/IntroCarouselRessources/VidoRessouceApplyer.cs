using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;
using Utils;

namespace IntroCarouselRessources
{
    public class VidoRessouceApplyer : IRessourceApplyer
    {
        private readonly RenderTexture _renderTexture;

        private readonly IntroCarouselSlide _slide;

        private readonly VideoClip _video;
        private VideoPlayer _videoPlayer;

        public VidoRessouceApplyer(IntroCarouselSlide slide, VideoClip video, RenderTexture renderTexture)
        {
            Precondition.CheckNotNull(slide);
            Precondition.CheckNotNull(video);
            Precondition.CheckNotNull(renderTexture);
            _slide = slide;
            _video = video;
            _renderTexture = renderTexture;
            _videoPlayer = null;
            slide.OnSlideWillAppearEvent += OnSlideWillAppear;
            slide.OnSlideWillDisappearEvent += OnSlideWillDisappear;
        }

        public void ApplyRessouce()
        {
            AddVideoPlayer(_video, _renderTexture);
            AddRawImage(_renderTexture);
        }

        ~VidoRessouceApplyer()
        {
            if (_videoPlayer != null) _videoPlayer.loopPointReached -= VideoPlayerDidReachEnd;
        }

        private void AddVideoPlayer(VideoClip video, RenderTexture texture)
        {
            var videoPlayer = _slide.gameObject.AddComponent<VideoPlayer>();
            videoPlayer.source = VideoSource.VideoClip;
            videoPlayer.playOnAwake = false;
            videoPlayer.waitForFirstFrame = true;
            videoPlayer.isLooping = false;
            videoPlayer.renderMode = VideoRenderMode.RenderTexture;
            videoPlayer.targetTexture = texture;
            videoPlayer.aspectRatio = VideoAspectRatio.FitVertically;
            videoPlayer.audioOutputMode = VideoAudioOutputMode.Direct;
            videoPlayer.clip = video;
            _videoPlayer = videoPlayer;
            _videoPlayer.loopPointReached += VideoPlayerDidReachEnd;
            _videoPlayer.Prepare();
        }

        private void AddRawImage(RenderTexture texture)
        {
            var rawImage = _slide.gameObject.AddComponent<RawImage>();
            rawImage.texture = texture;
        }

        private void VideoPlayerDidReachEnd(VideoPlayer videoPlayer)
        {
            videoPlayer.Stop();
            videoPlayer.Play();
        }

        private void OnSlideWillAppear(IntroCarouselSlide sender)
        {
            if (_videoPlayer != null) _videoPlayer.Play();
        }

        private void OnSlideWillDisappear(IntroCarouselSlide sender)
        {
            if (_videoPlayer != null) _videoPlayer.Pause();
        }
    }
}