using UnityEngine;
using UnityEngine.UI;
using Utils;

namespace IntroCarouselRessources
{
    public class ImageRessouceApplyer : IRessourceApplyer
    {
        private readonly Sprite _image;
        private readonly IntroCarouselSlide _slide;

        public ImageRessouceApplyer(IntroCarouselSlide slide, Sprite image)
        {
            Precondition.CheckNotNull(slide);
            Precondition.CheckNotNull(image);
            _slide = slide;
            _image = image;
        }

        public void ApplyRessouce()
        {
            AddImage(_image);
        }

        private void AddImage(Sprite image)
        {
            var image2 = _slide.gameObject.AddComponent<Image>();
            image2.type = Image.Type.Simple;
            image2.sprite = image;
            image2.preserveAspect = true;
        }
    }
}