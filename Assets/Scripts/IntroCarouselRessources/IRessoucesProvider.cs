using AssetBundles;
using UnityEngine;
using UnityEngine.Video;

namespace IntroCarouselRessources
{
    public interface IRessoucesProvider
    {
        VideoClip GetVideoClipForSlide(IntroCarouselSlideNumber slideNumber);

        RenderTexture GetVideoTextureForSlide(IntroCarouselSlideNumber slideNumber);

        Sprite GetSpriteForSlide(IntroCarouselSlideNumber slideNumber);

        IntroCarouselRessourceKind GetRessourceKind();
    }
}