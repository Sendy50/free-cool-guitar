using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace IronSourceJSON
{
    public static class Json
    {
        public static object Deserialize(string json)
        {
            if (json == null) return null;
            return Parser.Parse(json);
        }

        public static string Serialize(object obj)
        {
            return Serializer.Serialize(obj);
        }

        private sealed class Parser : IDisposable
        {
            private const string WHITE_SPACE = " \t\n\r";

            private const string WORD_BREAK = " \t\n\r{}[],:\"";

            private StringReader json;

            private Parser(string jsonString)
            {
                json = new StringReader(jsonString);
            }

            private char PeekChar => Convert.ToChar(json.Peek());

            private char NextChar => Convert.ToChar(json.Read());

            private string NextWord
            {
                get
                {
                    var stringBuilder = new StringBuilder();
                    while (" \t\n\r{}[],:\"".IndexOf(PeekChar) == -1)
                    {
                        stringBuilder.Append(NextChar);
                        if (json.Peek() == -1) break;
                    }

                    return stringBuilder.ToString();
                }
            }

            private TOKEN NextToken
            {
                get
                {
                    EatWhitespace();
                    if (json.Peek() == -1) return TOKEN.NONE;
                    switch (PeekChar)
                    {
                        case '{':
                            return TOKEN.CURLY_OPEN;
                        case '}':
                            json.Read();
                            return TOKEN.CURLY_CLOSE;
                        case '[':
                            return TOKEN.SQUARED_OPEN;
                        case ']':
                            json.Read();
                            return TOKEN.SQUARED_CLOSE;
                        case ',':
                            json.Read();
                            return TOKEN.COMMA;
                        case '"':
                            return TOKEN.STRING;
                        case ':':
                            return TOKEN.COLON;
                        case '-':
                        case '0':
                        case '1':
                        case '2':
                        case '3':
                        case '4':
                        case '5':
                        case '6':
                        case '7':
                        case '8':
                        case '9':
                            return TOKEN.NUMBER;
                        default:
                            switch (NextWord)
                            {
                                case "false":
                                    return TOKEN.FALSE;
                                case "true":
                                    return TOKEN.TRUE;
                                case "null":
                                    return TOKEN.NULL;
                                default:
                                    return TOKEN.NONE;
                            }
                    }
                }
            }

            public void Dispose()
            {
                json.Dispose();
                json = null;
            }

            public static object Parse(string jsonString)
            {
                var parser = new Parser(jsonString);
                return parser.ParseValue();
            }

            private Dictionary<string, object> ParseObject()
            {
                var dictionary = new Dictionary<string, object>();
                json.Read();
                while (true)
                {
                    switch (NextToken)
                    {
                        case TOKEN.COMMA:
                            continue;
                        case TOKEN.NONE:
                            return null;
                        case TOKEN.CURLY_CLOSE:
                            return dictionary;
                    }

                    var text = ParseString();
                    if (text == null) return null;
                    if (NextToken != TOKEN.COLON) return null;
                    json.Read();
                    dictionary[text] = ParseValue();
                }
            }

            private List<object> ParseArray()
            {
                var list = new List<object>();
                json.Read();
                var flag = true;
                while (flag)
                {
                    var nextToken = NextToken;
                    switch (nextToken)
                    {
                        case TOKEN.NONE:
                            return null;
                        case TOKEN.SQUARED_CLOSE:
                            flag = false;
                            break;
                        default:
                        {
                            var item = ParseByToken(nextToken);
                            list.Add(item);
                            break;
                        }
                        case TOKEN.COMMA:
                            break;
                    }
                }

                return list;
            }

            private object ParseValue()
            {
                var nextToken = NextToken;
                return ParseByToken(nextToken);
            }

            private object ParseByToken(TOKEN token)
            {
                switch (token)
                {
                    case TOKEN.STRING:
                        return ParseString();
                    case TOKEN.NUMBER:
                        return ParseNumber();
                    case TOKEN.CURLY_OPEN:
                        return ParseObject();
                    case TOKEN.SQUARED_OPEN:
                        return ParseArray();
                    case TOKEN.TRUE:
                        return true;
                    case TOKEN.FALSE:
                        return false;
                    case TOKEN.NULL:
                    default:
                        return null;
                }
            }

            private string ParseString()
            {
                var stringBuilder = new StringBuilder();
                json.Read();
                var flag = true;
                while (flag)
                {
                    if (json.Peek() == -1)
                    {
                        flag = false;
                        break;
                    }

                    var nextChar = NextChar;
                    switch (nextChar)
                    {
                        case '"':
                            flag = false;
                            break;
                        case '\\':
                            if (json.Peek() == -1)
                            {
                                flag = false;
                                break;
                            }

                            nextChar = NextChar;
                            switch (nextChar)
                            {
                                case '"':
                                case '/':
                                case '\\':
                                    stringBuilder.Append(nextChar);
                                    break;
                                case 'b':
                                    stringBuilder.Append('\b');
                                    break;
                                case 'f':
                                    stringBuilder.Append('\f');
                                    break;
                                case 'n':
                                    stringBuilder.Append('\n');
                                    break;
                                case 'r':
                                    stringBuilder.Append('\r');
                                    break;
                                case 't':
                                    stringBuilder.Append('\t');
                                    break;
                                case 'u':
                                {
                                    var stringBuilder2 = new StringBuilder();
                                    for (var i = 0; i < 4; i++) stringBuilder2.Append(NextChar);
                                    stringBuilder.Append((char) Convert.ToInt32(stringBuilder2.ToString(), 16));
                                    break;
                                }
                            }

                            break;
                        default:
                            stringBuilder.Append(nextChar);
                            break;
                    }
                }

                return stringBuilder.ToString();
            }

            private object ParseNumber()
            {
                var nextWord = NextWord;
                if (nextWord.IndexOf('.') == -1)
                {
                    long.TryParse(nextWord, out var result);
                    return result;
                }

                double.TryParse(nextWord, out var result2);
                return result2;
            }

            private void EatWhitespace()
            {
                while (" \t\n\r".IndexOf(PeekChar) != -1)
                {
                    json.Read();
                    if (json.Peek() == -1) break;
                }
            }

            private enum TOKEN
            {
                NONE,
                CURLY_OPEN,
                CURLY_CLOSE,
                SQUARED_OPEN,
                SQUARED_CLOSE,
                COLON,
                COMMA,
                STRING,
                NUMBER,
                TRUE,
                FALSE,
                NULL
            }
        }

        private sealed class Serializer
        {
            private readonly StringBuilder builder;

            private Serializer()
            {
                builder = new StringBuilder();
            }

            public static string Serialize(object obj)
            {
                var serializer = new Serializer();
                serializer.SerializeValue(obj);
                return serializer.builder.ToString();
            }

            private void SerializeValue(object value)
            {
                string str;
                IList anArray;
                IDictionary obj;
                if (value == null)
                    builder.Append("null");
                else if ((str = value as string) != null)
                    SerializeString(str);
                else if (value is bool)
                    builder.Append(value.ToString().ToLower());
                else if ((anArray = value as IList) != null)
                    SerializeArray(anArray);
                else if ((obj = value as IDictionary) != null)
                    SerializeObject(obj);
                else if (value is char)
                    SerializeString(value.ToString());
                else
                    SerializeOther(value);
            }

            private void SerializeObject(IDictionary obj)
            {
                var flag = true;
                builder.Append('{');
                var enumerator = obj.Keys.GetEnumerator();
                try
                {
                    while (enumerator.MoveNext())
                    {
                        var current = enumerator.Current;
                        if (!flag) builder.Append(',');
                        SerializeString(current.ToString());
                        builder.Append(':');
                        SerializeValue(obj[current]);
                        flag = false;
                    }
                }
                finally
                {
                    IDisposable disposable;
                    if ((disposable = enumerator as IDisposable) != null) disposable.Dispose();
                }

                builder.Append('}');
            }

            private void SerializeArray(IList anArray)
            {
                builder.Append('[');
                var flag = true;
                var enumerator = anArray.GetEnumerator();
                try
                {
                    while (enumerator.MoveNext())
                    {
                        var current = enumerator.Current;
                        if (!flag) builder.Append(',');
                        SerializeValue(current);
                        flag = false;
                    }
                }
                finally
                {
                    IDisposable disposable;
                    if ((disposable = enumerator as IDisposable) != null) disposable.Dispose();
                }

                builder.Append(']');
            }

            private void SerializeString(string str)
            {
                builder.Append('"');
                var array = str.ToCharArray();
                var array2 = array;
                foreach (var c in array2)
                {
                    switch (c)
                    {
                        case '"':
                            builder.Append("\\\"");
                            continue;
                        case '\\':
                            builder.Append("\\\\");
                            continue;
                        case '\b':
                            builder.Append("\\b");
                            continue;
                        case '\f':
                            builder.Append("\\f");
                            continue;
                        case '\n':
                            builder.Append("\\n");
                            continue;
                        case '\r':
                            builder.Append("\\r");
                            continue;
                        case '\t':
                            builder.Append("\\t");
                            continue;
                    }

                    var num = Convert.ToInt32(c);
                    if (num >= 32 && num <= 126)
                        builder.Append(c);
                    else
                        builder.Append("\\u" + Convert.ToString(num, 16).PadLeft(4, '0'));
                }

                builder.Append('"');
            }

            private void SerializeOther(object value)
            {
                if (value is float || value is int || value is uint || value is long || value is double ||
                    value is sbyte || value is byte || value is short || value is ushort || value is ulong ||
                    value is decimal)
                    builder.Append(value);
                else
                    SerializeString(value.ToString());
            }
        }
    }
}