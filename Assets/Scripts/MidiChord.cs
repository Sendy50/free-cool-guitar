using System;
using System.Runtime.Serialization;

[Serializable]
public struct MidiChord : IMidiChord, ISerializable
{
    public int[] MidiNumbers;

    [NonSerialized] private int _hashCode;

    public MidiChord(int[] MidiNumbers)
    {
        this.MidiNumbers = MidiNumbers;
        _hashCode = ChordMap.GenerateMidiNumbersArrayHash(MidiNumbers).GetHashCode();
    }

    public MidiChord(SerializationInfo info, StreamingContext context)
    {
        MidiNumbers = (int[]) info.GetValue("MidiNumbers", typeof(int[]));
        _hashCode = ChordMap.GenerateMidiNumbersArrayHash(MidiNumbers).GetHashCode();
    }

    public int[] GetMidiNumbers()
    {
        return MidiNumbers;
    }

    public bool IsEqual(IMidiChord other)
    {
        return GetMidiNumbers()[0] == other.GetMidiNumbers()[0] && GetMidiNumbers()[1] == other.GetMidiNumbers()[1] &&
               GetMidiNumbers()[2] == other.GetMidiNumbers()[2] && GetMidiNumbers()[3] == other.GetMidiNumbers()[3] &&
               GetMidiNumbers()[4] == other.GetMidiNumbers()[4] && GetMidiNumbers()[5] == other.GetMidiNumbers()[5];
    }

    public void GetObjectData(SerializationInfo info, StreamingContext context)
    {
        info.AddValue("MidiNumbers", MidiNumbers, typeof(int[]));
    }

    public static bool operator ==(MidiChord m1, MidiChord m2)
    {
        return m1.IsEqual(m2);
    }

    public static bool operator !=(MidiChord m1, MidiChord m2)
    {
        return !(m1 == m2);
    }

    public override int GetHashCode()
    {
        return _hashCode;
    }

    public static MidiChord DefaultInvalideChord()
    {
        return new MidiChord(new int[6]
        {
            -1,
            -1,
            -1,
            -1,
            -1,
            -1
        });
    }

    public static MidiChord EmptyChord()
    {
        return new MidiChord(ChordMap.emptyChordMidiNumbers);
    }
}