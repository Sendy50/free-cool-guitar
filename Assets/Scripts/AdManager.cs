// using AudienceNetwork;
using UnityEngine;
using UnityEngine.UI;

public class AdManager : MonoBehaviour
{
    public GameObject targetAdObject;

    public Button targetButton;

    private bool adLoaded;
    // public NativeAd nativeAd;

    private void Start()
    {
        adLoaded = false;
        LoadAd();
    }

    private void OnDestroy()
    {
        // if ((bool) nativeAd)
        // {
        //     nativeAd.Dispose();
        // }

        Debug.Log("NativeAdTest was destroyed!");
    }

    public bool IsAdLoaded()
    {
        return adLoaded;
    }

    public void LoadAd()
    {
        // var nativeAd = new NativeAd("YOUR_PLACEMENT_ID");
        // this.nativeAd = nativeAd;
        if ((bool) targetAdObject)
        {
            if ((bool) targetButton)
            {
                // nativeAd.RegisterGameObjectForImpression(targetAdObject, new Button[1]
                // {
                //     targetButton
                // });
            }
            else
            {
                // nativeAd.RegisterGameObjectForImpression(targetAdObject, new Button[0]);
            }
        }
        else
        {
            // nativeAd.RegisterGameObjectForImpression(gameObject, new Button[0]);
        }

        // nativeAd.NativeAdDidLoad = delegate
        // {
        //     adLoaded = true;
        //     Debug.Log("Native ad loaded.");
        //     Debug.Log("Loading images...");
        //     StartCoroutine(nativeAd.LoadCoverImage(nativeAd.CoverImageURL));
        //     StartCoroutine(nativeAd.LoadIconImage(nativeAd.IconImageURL));
        //     Debug.Log("Images loaded.");
        // };
        // nativeAd.NativeAdDidFailWithError = delegate(string error)
        // {
        //     Debug.Log("Native ad failed to load with error: " + error);
        // };
        // nativeAd.NativeAdWillLogImpression = delegate { Debug.Log("Native ad logged impression."); };
        // nativeAd.NativeAdDidClick = delegate { Debug.Log("Native ad clicked."); };
        // nativeAd.LoadAd();
        Debug.Log("Native ad loading...");
    }
}