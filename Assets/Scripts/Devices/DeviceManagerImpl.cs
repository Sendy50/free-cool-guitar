using System;

namespace Devices
{
    public class DeviceManagerImpl : DeviceManager
    {
        public RuntimeType GetRuntimeType()
        {
            return RuntimeType.Android;
        }

        public DeviceTarget GetDeviceTarget()
        {
            return DeviceTarget.Android;
        }

        public string GetRuntimeTypeName()
        {
            switch (GetRuntimeType())
            {
                case RuntimeType.Editor:
                    return "editor";
                case RuntimeType.Android:
                    return "android";
                case RuntimeType.Ios:
                    return "ios";
                default:
                    throw new Exception("This RuntimeType Name isn't managed");
            }
        }

        public bool IsDevelopmentBuild()
        {
            return false;
        }
    }
}