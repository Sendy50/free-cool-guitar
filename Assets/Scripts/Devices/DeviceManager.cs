namespace Devices
{
    public interface DeviceManager
    {
        RuntimeType GetRuntimeType();

        DeviceTarget GetDeviceTarget();

        string GetRuntimeTypeName();

        bool IsDevelopmentBuild();
    }
}