namespace Devices
{
    public class DeviceModule
    {
        public DeviceManager CreateDeviceManager()
        {
            return new DeviceManagerImpl();
        }
    }
}