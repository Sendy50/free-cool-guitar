namespace Devices
{
    public enum RuntimeType
    {
        Android,
        Ios,
        Editor,
        NotSupported
    }
}