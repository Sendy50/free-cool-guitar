using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(RectTransform))]
[AddComponentMenu("UI/TableView")]
public class TableView : MonoBehaviour, ITableView
{
    public GameObject cellPrefab;

    public float heightForRow = 100f;

    public Sprite maskSprite;

    public Image.Type maskSpriteType;

    private float cellPercentHeight;

    private float contentHeight;

    private GameObject contentView;

    private RectTransform contentViewRectTransform;

    private GameObject maskView;

    private int nbReusableCell;

    private int numberOfRows;

    private readonly List<GameObject> reusableCells = new List<GameObject>();

    private ScrollRect scrollRect;

    private float tableViewHeight;

    private void Awake()
    {
        scrollRect = gameObject.AddComponent<ScrollRect>();
    }

    private void Update()
    {
        if (tableViewHeight <= 0f) return;
        var y = scrollRect.velocity.y;
        var verticalNormalizedPosition = scrollRect.verticalNormalizedPosition;
        var num = (1f - verticalNormalizedPosition) * (tableViewHeight / contentHeight);
        if (y < 0f)
        {
            var gameObject = reusableCells.Last();
            var component = gameObject.GetComponent<TableViewCell>();
            var num2 = 1f - (component.cellIndex - (nbReusableCell - 1.5f)) * cellPercentHeight;
            if (verticalNormalizedPosition > num2 - num && component.cellIndex - nbReusableCell >= 0)
            {
                reusableCells.RemoveAt(nbReusableCell - 1);
                component.cellIndex -= nbReusableCell;
                reusableCells.Insert(0, gameObject);
                var component2 = gameObject.GetComponent<RectTransform>();
                component2.offsetMin = new Vector2(0f, -(component.cellIndex + 1) * heightForRow);
                component2.offsetMax = new Vector2(0f, -component.cellIndex * heightForRow);
                component.prepareForReuse();
                tableViewDelegate.configureTableViewCellAtIndex(component, component.cellIndex);
                Update();
            }
        }
        else
        {
            var gameObject2 = reusableCells.First();
            var component3 = gameObject2.GetComponent<TableViewCell>();
            var num3 = 1f - (component3.cellIndex + 1.5f) * cellPercentHeight;
            if (verticalNormalizedPosition < num3 - num && component3.cellIndex + nbReusableCell < numberOfRows)
            {
                reusableCells.RemoveAt(0);
                component3.cellIndex += nbReusableCell;
                reusableCells.Add(gameObject2);
                var component4 = gameObject2.GetComponent<RectTransform>();
                component4.offsetMin = new Vector2(0f, -(component3.cellIndex + 1) * heightForRow);
                component4.offsetMax = new Vector2(0f, -component3.cellIndex * heightForRow);
                component3.prepareForReuse();
                tableViewDelegate.configureTableViewCellAtIndex(component3, component3.cellIndex);
                Update();
            }
        }

        var num4 = Mathf.Min(1f, Mathf.Max(0f, verticalNormalizedPosition));
        var num5 = (1f - num4) * (tableViewHeight / contentHeight);
        var topCellIndex = Mathf.CeilToInt((1f - num4 - num5) * numberOfRows);
        tableViewDelegate.topCellIndexChange(topCellIndex);
    }

    public TableViewDelegate tableViewDelegate { get; set; }

    public void startTableView()
    {
        StartCoroutine(startTableViewRoutine());
    }

    public void jumpToRow(int rowIndex)
    {
        var num = 1f - rowIndex * heightForRow / contentHeight;
        var num2 = (1f - num) * (tableViewHeight / contentHeight);
        scrollRect.verticalNormalizedPosition = num - num2;
        var num3 = rowIndex - 1;
        var num4 = num3 + nbReusableCell;
        if (num4 > numberOfRows) num3 -= num4 - numberOfRows;
        if (num3 <= 0) num3 = 0;
        for (var i = 0; i < Mathf.Min(numberOfRows, nbReusableCell); i++)
        {
            var num5 = num3 + i;
            var gameObject = reusableCells[i];
            var component = gameObject.GetComponent<TableViewCell>();
            component.cellIndex = num5;
            var component2 = gameObject.GetComponent<RectTransform>();
            component2.offsetMin = new Vector2(0f, -(num5 + 1) * heightForRow);
            component2.offsetMax = new Vector2(0f, -num5 * heightForRow);
            tableViewDelegate.configureTableViewCellAtIndex(component, num5);
        }
    }

    public void reloadVisiblesCell()
    {
        foreach (var reusableCell in reusableCells)
        {
            var component = reusableCell.GetComponent<TableViewCell>();
            tableViewDelegate.configureTableViewCellAtIndex(component, component.cellIndex);
        }
    }

    private IEnumerator startTableViewRoutine()
    {
        var rectT = gameObject.GetComponent<RectTransform>();
        yield return new WaitUntil(() => rectT.rect.height > 0f);
        tableViewHeight = rectT.rect.height;
        maskView = new GameObject("MaskView");
        maskView.transform.parent = transform;
        var maskRectTransform = maskView.AddComponent<RectTransform>();
        maskRectTransform.localScale = Vector3.one;
        maskRectTransform.localPosition = Vector3.zero;
        maskRectTransform.anchorMin = new Vector2(0f, 0f);
        maskRectTransform.anchorMax = new Vector2(1f, 1f);
        maskRectTransform.offsetMin = new Vector2(0f, 0f);
        maskRectTransform.offsetMax = new Vector2(0f, 0f);
        var maskViewImage = maskView.AddComponent<Image>();
        maskViewImage.sprite = maskSprite;
        maskViewImage.type = maskSpriteType;
        maskViewImage.color = new Color(0f, 0f, 0f, 1f);
        maskView.AddComponent<Mask>();
        contentView = new GameObject("ContentView");
        contentView.transform.parent = maskRectTransform;
        contentViewRectTransform = contentView.AddComponent<RectTransform>();
        contentViewRectTransform.localScale = Vector3.one;
        contentViewRectTransform.anchorMin = new Vector2(0f, 1f);
        contentViewRectTransform.anchorMax = new Vector2(1f, 1f);
        contentViewRectTransform.localPosition = new Vector3(0f, 0f, 0f);
        var contentViewImage = contentView.AddComponent<Image>();
        contentViewImage.color = Color.clear;
        scrollRect.content = contentViewRectTransform;
        scrollRect.horizontal = false;
        numberOfRows = tableViewDelegate.numberOfRows();
        contentHeight = numberOfRows * heightForRow;
        contentViewRectTransform.offsetMin = new Vector2(0f, 0f - contentHeight);
        contentViewRectTransform.offsetMax = new Vector2(0f, 0f);
        cellPercentHeight = 1f / numberOfRows;
        createReusablesCell();
        layoutFirstCell();
    }

    private void createReusablesCell()
    {
        nbReusableCell = Mathf.CeilToInt(tableViewHeight / heightForRow) + 2;
        for (var i = 0; i < nbReusableCell; i++)
        {
            Debug.LogError("createReusablesCell : @@ i :" + i);
            var gameObject = Instantiate(cellPrefab, Vector3.zero, Quaternion.identity);
            gameObject.transform.SetParent(contentViewRectTransform);
            var component = gameObject.GetComponent<RectTransform>();
            component.localScale = Vector3.one;
            component.anchorMin = new Vector2(0f, 1f);
            component.anchorMax = new Vector2(1f, 1f);
            component.localPosition = new Vector3(0f, 0f, 0f);
            reusableCells.Add(gameObject);
        }
    }

    private void layoutFirstCell()
    {
        scrollRect.vertical = numberOfRows > nbReusableCell;
        for (var i = 0; i < Mathf.Min(numberOfRows, nbReusableCell); i++)
        {
            var gameObject = reusableCells[i];
            var component = gameObject.GetComponent<TableViewCell>();
            component.cellIndex = i;
            var component2 = gameObject.GetComponent<RectTransform>();
            component2.offsetMin = new Vector2(0f, -(i + 1) * heightForRow);
            component2.offsetMax = new Vector2(0f, -i * heightForRow);
            tableViewDelegate.configureTableViewCellAtIndex(component, i);
        }
    }
}