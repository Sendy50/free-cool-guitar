using System;
using System.Collections.Generic;
using AdditionalContent;
using Patch;
using Utils;

namespace SamplerScripts.AdditionalContent
{
    public class AdditionalContentProviderImpl : AdditionalContentProvider
    {
        private readonly ExternalAccompanimentDownloader _accompanimentDownloader;

        private readonly List<string> _newSongsIds;

        private readonly SongRepository.SongRepository _songRepository;

        private readonly List<ExternalSong> _songsList;

        private readonly List<string> _songsOrder;
        private readonly AdditionalContentProviderStorage _storageManager;

        public AdditionalContentProviderImpl(AdditionalContentProviderStorage storageManager,
            ExternalAccompanimentDownloader accompanimentDownloader, SongRepository.SongRepository songRepository)
        {
            Precondition.CheckNotNull(storageManager);
            Precondition.CheckNotNull(accompanimentDownloader);
            Precondition.CheckNotNull(songRepository);
            _storageManager = storageManager;
            _accompanimentDownloader = accompanimentDownloader;
            _songRepository = songRepository;
            _songsOrder = _storageManager.ReadSongsOrder();
            _newSongsIds = _storageManager.ReadNewSongs();
            _songsList = new List<ExternalSong>();
        }

        public List<ExternalSong> GetAdditionalSongs()
        {
            return new List<ExternalSong>(_songsList);
        }

        public List<string> GetSongsOrder()
        {
            return new List<string>(_songsOrder);
        }

        public void SetSongsOrder(List<string> songsOrder)
        {
            _songsOrder.Clear();
            if (songsOrder != null && songsOrder.Count > 0) _songsOrder.AddRange(songsOrder);
            _storageManager.WriteSongsOrder(songsOrder);
        }

        public List<string> GetNewSongs()
        {
            return new List<string>(_newSongsIds);
        }

        public void SetNewSongs(List<string> newSongsIds)
        {
            _newSongsIds.Clear();
            if (newSongsIds != null && newSongsIds.Count > 0) _newSongsIds.AddRange(newSongsIds);
            _storageManager.WriteNewSongs(newSongsIds);
        }

        public void SetAdditionalPatchData(PatchDataList patchDataList, Action<bool> callback)
        {
            _accompanimentDownloader.GetExternalSongs(patchDataList.Songs,
                delegate(bool success, List<ExternalSong> externalSongs)
                {
                    _songsList.Clear();
                    _songsList.AddRange(externalSongs);
                    _songRepository.Refresh();
                    if (callback != null) callback(success);
                });
        }
    }
}