using GuitarConfiguration;
using TrackCollection;
using UnityEngine;

public class ScreenInstanceLocator : MonoBehaviour
{
    public GameObject _blur;
    public GameObject _trackCollectionScreen;
    public GameObject _guitarConfigurationScreen;
    public GameObject _chordsSelector;
    public GameObject _gameModeContainer;
    public GameObject _introCarouselPrefab;
    private Blur _blurInstance;

    private ChordsSelector _chordsSelectorInstance;

    private GameModeScreen _gameModeScreen;

    private GuitarConfigurationScreen _guitarConfigurationScreenInstance;

    private IntroCarousel _introCarousel;

    private ResourceRequest _runningTrackListRequest;

    private TrackCollectionScreen _trackCollectionInstance;

    public Blur GetBlure()
    {
        if (_blurInstance == null) _blurInstance = Instantiate(_blur).GetComponent<Blur>();
        return _blurInstance;
    }

    public TrackCollectionScreen GetTrackCollection()
    {
        if (_trackCollectionInstance == null)
            _trackCollectionInstance = Instantiate(_trackCollectionScreen).GetComponent<TrackCollectionScreen>();
        return _trackCollectionInstance;
    }

    public GuitarConfigurationScreen GetGuitarConfigurationScreen()
    {
        if (_guitarConfigurationScreenInstance == null)
            _guitarConfigurationScreenInstance =
                Instantiate(_guitarConfigurationScreen).GetComponent<GuitarConfigurationScreen>();
        return _guitarConfigurationScreenInstance;
    }

    public ChordsSelector GetChordsSelector()
    {
        if (_chordsSelectorInstance == null)
            _chordsSelectorInstance = Instantiate(_chordsSelector).GetComponent<ChordsSelector>();
        return _chordsSelectorInstance;
    }

    public GameModeScreen GetGameModeScreen()
    {
        if (_gameModeScreen == null) _gameModeScreen = Instantiate(_gameModeContainer).GetComponent<GameModeScreen>();
        return _gameModeScreen;
    }

    public IntroCarousel GetIntroCarousel()
    {
        if (_introCarousel == null) _introCarousel = Instantiate(_introCarouselPrefab).GetComponent<IntroCarousel>();
        return _introCarousel;
    }
}