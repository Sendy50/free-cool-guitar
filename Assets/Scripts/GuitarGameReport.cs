using System;
using MWM.GameEvent;

[Serializable]
public class GuitarGameReport : GameReport
{
    public string track_id;

    public string track_type;

    public string user_type;

    public int score;

    public int total_success_stroke;

    public int total_miss_stroke;

    public int total_fail_stroke;

    public int total_success_strum;

    public int total_miss_strum;

    public int track_action_count;
}