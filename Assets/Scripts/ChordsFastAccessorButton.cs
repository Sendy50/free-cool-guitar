using UnityEngine;
using UnityEngine.UI;

public class ChordsFastAccessorButton : MonoBehaviour
{
    public Text fastAccessorNameText;

    public Image backgroundImage;

    public Color backgroundColor;

    public Color textColor;

    public Color backgroundColorWhenHighlighted;

    public Color textColorWhenHighlighted;

    [HideInInspector] public FastAccessorDescriptor descriptor { get; private set; }

    public void updateWithDescriptor(FastAccessorDescriptor descriptor)
    {
        this.descriptor = descriptor;
        fastAccessorNameText.text = descriptor.fastAccessorName;
    }

    public void highlighted()
    {
        backgroundImage.color = backgroundColorWhenHighlighted;
        fastAccessorNameText.color = textColorWhenHighlighted;
    }

    public void unhighlighted()
    {
        backgroundImage.color = backgroundColor;
        fastAccessorNameText.color = textColor;
    }
}