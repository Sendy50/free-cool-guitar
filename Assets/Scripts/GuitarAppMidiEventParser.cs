using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GuitarAppMidiEventParser
{
    public const int nbStrings = 6;

    public const int notesTrackIndex = 0;

    public const int chordsTrackIndex = 1;

    public const int nbTrackIndex = 2;

    public const int muteMidiNumber = 0;

    public const int strumDownMidiNumber = 0;

    public const int strumUpMidiNumber = 1;

    public const float floatEpsilon = 1E-05f;

    public const int strumMinStrings = 3;

    public static ParsingResult ParseMidiFile(TextAsset jsonTextAsset)
    {
        var text = jsonTextAsset.text;
        Debug.LogError("ParseMidiFile JSON : " + text);
        var midiFile = JsonUtility.FromJson<MidiFile>(text);
        var tracks = midiFile.tracks;
        var notes = tracks[0].notes;
        var notes2 = tracks[1].notes;
        var list = new List<MidiNote>();
        var list2 = new List<MidiNote>();
        var list3 = new List<MidiChordEvent>();
        var actionList = new List<IUserAction>();
        var i = 0;
        for (var j = 1; j <= notes2.Length; j++)
        {
            var midiNote = notes2[j - 1];
            list.Add(midiNote);
            for (; j < notes2.Length && midiNote.time == notes2[j].time; j++) list.Add(notes2[j]);
            var currentChord = ChordEventFromList(list);
            list.Clear();
            var num = j < notes2.Length ? notes2[j].time : float.PositiveInfinity;
            var midiNote2 = notes[i];
            while (midiNote2.time < num)
            {
                list2.Add(midiNote2);
                for (i++; i < notes.Length && midiNote2.time == notes[i].time; i++) list2.Add(notes[i]);
                if (list2.Any(c => c.midi == 0))
                {
                    var notes3 = (from c in list2
                        where c.midi != 0
                        select MusicMidiEventFrom(c, currentChord)
                        into c
                        orderby c.StringIndex
                        select c).ToArray();
                    actionList.Add(new StrumUserAction
                    {
                        notes = notes3,
                        orientation = StrumUserAction.Orientation.Down
                    });
                }
                else if (list2.Any(c => c.midi == 1))
                {
                    var notes4 = (from c in list2
                        where c.midi != 1
                        select MusicMidiEventFrom(c, currentChord)
                        into c
                        orderby c.StringIndex descending
                        select c).ToArray();
                    actionList.Add(new StrumUserAction
                    {
                        notes = notes4,
                        orientation = StrumUserAction.Orientation.Up
                    });
                }
                else
                {
                    list2.ForEach(delegate(MidiNote c)
                    {
                        var item = new StrockUserAction
                        {
                            note = MusicMidiEventFrom(c, currentChord)
                        };
                        actionList.Add(item);
                    });
                }

                list2.Clear();
                if (i >= notes.Length) break;
                midiNote2 = notes[i];
            }

            list3.Add(currentChord);
        }

        ParsingResult result = default;
        result.UserActions = actionList.ToArray();
        result.MidiChordEvents = list3.ToArray();
        return result;
    }

    public static void AnalyseChords(List<MidiChordEvent> chords)
    {
        Debug.Log("----- starting chords parsing analyse ----");
        Debug.Log("number of detected chords: " + chords.Count);
        foreach (var chord in chords)
            if (!ChordMap.Map.ContainsKey(chord.MidiChord.GetHashCode()))
                Debug.Log("INVALIDE CHORD FOUND -- time: " + chord.time + " midi: " + chord.MidiChord.MidiNumbers[0] +
                          " " + chord.MidiChord.MidiNumbers[1] + " " + chord.MidiChord.MidiNumbers[2] + " " +
                          chord.MidiChord.MidiNumbers[3] + " " + chord.MidiChord.MidiNumbers[4] + " " +
                          chord.MidiChord.MidiNumbers[5]);
        Debug.Log("----- end chords parsing analyse ----");
    }

    public static ChordNameCorrespondance[] ParseChordMap(TextAsset jsonTextAsset)
    {
        var text = jsonTextAsset.text;
        var midiFile = JsonUtility.FromJson<MidiFile>(text);
        var tracks = midiFile.tracks;
        return tracks.Select(ChordCorrespondanceFromTrack).ToArray();
    }

    private static MidiChordEvent ChordEventFromList(List<MidiNote> list)
    {
        MidiChordEvent result = default;
        result.MidiChord = ChordFromList(list);
        result.time = list.First().time;
        return result;
    }

    private static MidiChord ChordFromList(IEnumerable<MidiNote> list)
    {
        var array = new int[6];
        foreach (var item in list)
        {
            var num = Mathf.RoundToInt(item.velocity * 127f) - 1;
            array[num] = item.midi;
        }

        return new MidiChord(array);
    }

    private static int StringIndexFromChord(int midiNumber, MidiChord chord)
    {
        var midiNumbers = chord.MidiNumbers;
        for (var i = 0; i < midiNumbers.Length; i++)
            if (midiNumber == midiNumbers[i])
                return i;
        return -1;
    }

    private static MusicNoteMidiEvent MusicMidiEventFrom(MidiNote midiNote, MidiChordEvent chord)
    {
        return new MusicNoteMidiEvent(midiNote, 0, StringIndexFromChord(midiNote.midi, chord.MidiChord));
    }

    private static ChordNameCorrespondance ChordCorrespondanceFromTrack(MidiTrack track)
    {
        var midiChord = ChordFromList(track.notes);
        ChordNameCorrespondance result = default;
        result.MidiNumbers = midiChord.MidiNumbers;
        return result;
    }

    public struct ParsingResult
    {
        public IUserAction[] UserActions;

        public MidiChordEvent[] MidiChordEvents;
    }
}