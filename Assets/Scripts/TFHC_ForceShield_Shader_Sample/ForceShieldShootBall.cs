using UnityEngine;

namespace TFHC_ForceShield_Shader_Sample
{
    public class ForceShieldShootBall : MonoBehaviour
    {
        public Rigidbody bullet;

        public Transform origshoot;

        public float speed = 1000f;

        private readonly float distance = 10f;

        private void Update()
        {
            if (Input.GetButtonDown("Fire1"))
            {
                var position = new Vector3(Input.mousePosition.x, Input.mousePosition.y, distance);
                position = Camera.main.ScreenToWorldPoint(position);
                var rigidbody = Instantiate(bullet, transform.position, Quaternion.identity);
                rigidbody.transform.LookAt(position);
                rigidbody.AddForce(rigidbody.transform.forward * speed);
            }
        }
    }
}