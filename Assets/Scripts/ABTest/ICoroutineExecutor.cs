using System.Collections;
using UnityEngine;

namespace ABTest
{
    public interface ICoroutineExecutor
    {
        Coroutine StartCoroutine(IEnumerator routine);

        void StopCoroutine(Coroutine routine);
    }
}