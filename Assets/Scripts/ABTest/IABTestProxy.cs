namespace ABTest
{
    public interface IABTestProxy
    {
        IABTest GetABTestForKey(string key);
    }
}