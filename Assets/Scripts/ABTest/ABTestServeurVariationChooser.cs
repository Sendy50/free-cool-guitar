using System;
using System.Linq;
using Utils;

namespace ABTest
{
    public class ABTestServeurVariationChooser : IABTestVariationChooser
    {
        private readonly IVariationChooserRequest _request;

        public ABTestServeurVariationChooser(IVariationChooserRequest request)
        {
            Precondition.CheckNotNull(request);
            _request = request;
        }

        public void ChooseVariation(IVariation[] possibility, Action<IVariation> callback)
        {
            _request.Run(delegate(string variationName)
            {
                var obj = possibility.FirstOrDefault(c => c.GetKey() == variationName);
                callback(obj);
            });
        }
    }
}