namespace ABTest
{
    public interface IABTestPresistanteData
    {
        string GetABTestKey();

        IVariation GetChosenVariation();
    }
}