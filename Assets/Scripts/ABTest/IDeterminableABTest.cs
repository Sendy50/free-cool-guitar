namespace ABTest
{
    public interface IDeterminableABTest : IABTest
    {
        new IVariation GetChosenVariation();

        bool GetIsVariationChoosed();
    }
}