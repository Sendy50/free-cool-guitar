using System.Runtime.Serialization;

namespace ABTest
{
    public interface IVariation : ISerializable
    {
        string GetKey();

        bool GetIsFailDefaultChoice();
    }
}