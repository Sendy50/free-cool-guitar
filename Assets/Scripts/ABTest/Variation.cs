using System;
using System.Runtime.Serialization;

namespace ABTest
{
    [Serializable]
    public class Variation : IVariation, ISerializable
    {
        public string key;

        public Variation(string key)
        {
            this.key = key;
        }

        public Variation(SerializationInfo info, StreamingContext context)
        {
            key = (string) info.GetValue("key", typeof(string));
        }

        public string GetKey()
        {
            return key;
        }

        public bool GetIsFailDefaultChoice()
        {
            return key == "failed_default_choice";
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("key", key, typeof(string));
        }

        public static bool operator ==(Variation v1, Variation v2)
        {
            return v1.key == v2.key;
        }

        public static bool operator !=(Variation v1, Variation v2)
        {
            return !(v1 == v2);
        }

        public override int GetHashCode()
        {
            return key.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType()) return false;
            var variation = (Variation) obj;
            return key == variation.key;
        }
    }
}