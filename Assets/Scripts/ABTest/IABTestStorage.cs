namespace ABTest
{
    public interface IABTestStorage
    {
        IABTestPresistanteData FetchABTestForKey(string key);

        void SaveABTest(IABTestPresistanteData abTest);
    }
}