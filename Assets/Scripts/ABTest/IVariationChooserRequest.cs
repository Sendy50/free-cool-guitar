using System;

namespace ABTest
{
    public interface IVariationChooserRequest
    {
        void Run(Action<string> callback);
    }
}