using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

namespace ABTest
{
    public class ABTestStorage : IABTestStorage
    {
        private static readonly string FilePath = Application.persistentDataPath + "/ab_test_storage.dat";

        private readonly Dictionary<string, IABTestPresistanteData> _data;

        public ABTestStorage()
        {
            _data = Load();
        }

        public IABTestPresistanteData FetchABTestForKey(string key)
        {
            return !_data.ContainsKey(key) ? null : _data[key];
        }

        public void SaveABTest(IABTestPresistanteData abTest)
        {
            _data[abTest.GetABTestKey()] = abTest;
            Save(_data);
        }

        private static Dictionary<string, IABTestPresistanteData> Load()
        {
            if (File.Exists(FilePath))
            {
                var binaryFormatter = new BinaryFormatter();
                var serializationStream = File.Open(FilePath, FileMode.Open);
                return (Dictionary<string, IABTestPresistanteData>) binaryFormatter.Deserialize(serializationStream);
            }

            return new Dictionary<string, IABTestPresistanteData>();
        }

        private static void Save(Dictionary<string, IABTestPresistanteData> data)
        {
            var binaryFormatter = new BinaryFormatter();
            var serializationStream = File.Create(FilePath);
            binaryFormatter.Serialize(serializationStream, data);
        }
    }
}