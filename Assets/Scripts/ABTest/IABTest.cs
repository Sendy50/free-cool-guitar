using System;

namespace ABTest
{
    public interface IABTest
    {
        string GetABTestKey();

        IVariation[] GetVariations();

        IVariation GetChosenVariation();

        void FetchVariation(Action<IABTest, IVariation> callback);
    }
}