using System;

namespace ABTest
{
    public interface IABTestVariationChooser
    {
        void ChooseVariation(IVariation[] possibility, Action<IVariation> callback);
    }
}