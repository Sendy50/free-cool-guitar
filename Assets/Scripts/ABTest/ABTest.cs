using System;
using UnityEngine;
using Utils;

namespace ABTest
{
    [Serializable]
    public class ABTest : IDeterminableABTest, IABTest
    {
        public string abTestKey;

        [NonSerialized] private IABTestStorage _storage;

        [NonSerialized] private IABTestVariationChooser _variationChooser;

        public IVariation chosenVariation;

        public IVariation[] variations;

        public ABTest(string abTestKey, IVariation[] variations, IABTestStorage storage,
            IABTestVariationChooser variationChooser)
        {
            Precondition.CheckNotNull(abTestKey);
            Precondition.CheckNotNull(variations);
            Precondition.CheckNotNull(storage);
            Precondition.CheckNotNull(variationChooser);
            this.abTestKey = abTestKey;
            this.variations = variations;
            _storage = storage;
            _variationChooser = variationChooser;
            var iABTestPresistanteData = storage.FetchABTestForKey(abTestKey);
            if (iABTestPresistanteData != null)
            {
                chosenVariation = iABTestPresistanteData.GetChosenVariation();
            }
        }

        public IVariation GetChosenVariation()
        {
            return chosenVariation;
        }

        public bool GetIsVariationChoosed()
        {
            return chosenVariation != null;
        }

        public string GetABTestKey()
        {
            return abTestKey;
        }

        public IVariation[] GetVariations()
        {
            return variations;
        }

        public void FetchVariation(Action<IABTest, IVariation> callback)
        {
            if (chosenVariation != null)
            {
                callback(this, chosenVariation);
                return;
            }

            _variationChooser.ChooseVariation(variations, delegate(IVariation choosen)
            {
                SetChosenVariation(choosen);
                callback(this, choosen);
            });
        }

        private void SetChosenVariation(IVariation variation)
        {
            chosenVariation = variation;
            Debug.Log("SetChosenVariation " + variation);
            _storage.SaveABTest(new ABTestPresistanteData(abTestKey, variation));
        }
    }
}