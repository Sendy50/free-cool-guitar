using System.Linq;
using MWM.GameEvent;
using Utils;

namespace ABTest
{
    public class ABTestProxy : IABTestProxy, IABTestEventDataBuilder
    {
        private readonly IDeterminableABTest[] _allABTest;

        private ICoroutineExecutor _coroutineExecutor;

        private IInstallationIdProvider _installationIdProvider;

        public ABTestProxy(IABTestStorage storage, IInstallationIdProvider installationIdProvider)
        {
            Precondition.CheckNotNull(storage);
            Precondition.CheckNotNull(installationIdProvider);
            // Precondition.CheckNotNull(coroutineExecutor);
            _installationIdProvider = installationIdProvider;
            // _coroutineExecutor = coroutineExecutor;
            _allABTest = new IDeterminableABTest[0];
        }

        public ABTestData[] buildEventData()
        {
            return (from c in _allABTest
                where c.GetIsVariationChoosed() && !c.GetChosenVariation().GetIsFailDefaultChoice()
                select new ABTestData(c.GetABTestKey(), c.GetChosenVariation().GetKey())).ToArray();
        }

        public IABTest GetABTestForKey(string key)
        {
            return _allABTest.FirstOrDefault(c => c.GetABTestKey() == key);
        }
    }
}