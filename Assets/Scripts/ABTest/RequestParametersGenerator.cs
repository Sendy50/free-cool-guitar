using MWM.GameEvent;
using UnityEngine;
using Utils;

namespace ABTest
{
    public class RequestParametersGenerator : IRequestParametersGenerator
    {
        private readonly string _abTestKey;

        private readonly IInstallationIdProvider _installationIdProvider;

        public RequestParametersGenerator(string abTestKey, IInstallationIdProvider installationIdProvider)
        {
            Precondition.CheckNotNull(abTestKey);
            Precondition.CheckNotNull(installationIdProvider);
            _abTestKey = abTestKey;
            _installationIdProvider = installationIdProvider;
        }

        public VariationChooserRequest.RequestParameters GenerateParameters()
        {
            return new VariationChooserRequest.RequestParameters(_installationIdProvider.GetInstallationId(),
                Application.identifier, Application.version, GetDeviceType(), _abTestKey);
        }

        private static string GetDeviceType()
        {
            return "android";
        }
    }
}