using System.Collections;
using UnityEngine;
using Utils;

namespace ABTest
{
    public class MonoBehaviourCoroutineExecutorAdapter : ICoroutineExecutor
    {
        private readonly MonoBehaviour _monoBehaviour;

        public MonoBehaviourCoroutineExecutorAdapter(MonoBehaviour monoBehaviour)
        {
            Precondition.CheckNotNull(monoBehaviour);
            _monoBehaviour = monoBehaviour;
        }

        public Coroutine StartCoroutine(IEnumerator routine)
        {
            return _monoBehaviour.StartCoroutine(routine);
        }

        public void StopCoroutine(Coroutine routine)
        {
            _monoBehaviour.StopCoroutine(routine);
        }
    }
}