using System;
using UnityEngine;
using Utils;

namespace ABTest
{
    [Serializable]
    public class ABTestPresistanteData : IABTestPresistanteData
    {
        [SerializeField] private string _abTestKey;

        [SerializeField] private IVariation _variation;

        public ABTestPresistanteData(string abTestKey, IVariation variation)
        {
            Precondition.CheckNotNull(abTestKey);
            Precondition.CheckNotNull(variation);
            _abTestKey = abTestKey;
            _variation = variation;
        }

        public string GetABTestKey()
        {
            return _abTestKey;
        }

        public IVariation GetChosenVariation()
        {
            return _variation;
        }
    }
}