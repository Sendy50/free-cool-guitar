namespace ABTest
{
    public interface IRequestParametersGenerator
    {
        VariationChooserRequest.RequestParameters GenerateParameters();
    }
}