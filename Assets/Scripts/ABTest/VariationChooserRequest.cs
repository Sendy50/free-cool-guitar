using System;
using System.Collections;
using System.Text;
using Newtonsoft.Json.Linq;
using UnityEngine;
using UnityEngine.Networking;
using Utils;

namespace ABTest
{
    public class VariationChooserRequest : IVariationChooserRequest
    {
        public const int DefaultTimeoutDuration = 3;

        private const string kABTestEndPoint = "https://api-dot-mwm-ab-testing.appspot.com/v1";

        private readonly ICoroutineExecutor _coroutineExecutor;

        private readonly string _onFailureVariationName;

        private readonly IRequestParametersGenerator _requestParametersGenerator;

        private readonly int _timeout;

        public VariationChooserRequest(ICoroutineExecutor coroutineExecutor,
            IRequestParametersGenerator requestParametersGenerator, string onFailureVariationName, int timeout = 3)
        {
            Precondition.CheckNotNull(coroutineExecutor);
            Precondition.CheckNotNull(requestParametersGenerator);
            Precondition.CheckNotNull(onFailureVariationName);
            _coroutineExecutor = coroutineExecutor;
            _requestParametersGenerator = requestParametersGenerator;
            _onFailureVariationName = onFailureVariationName;
            _timeout = timeout;
        }

        public void Run(Action<string> callback)
        {
            _coroutineExecutor.StartCoroutine(
                RequestVariationForABTest(_requestParametersGenerator.GenerateParameters(), callback));
        }

        private IEnumerator RequestVariationForABTest(RequestParameters parameters, Action<string> callback)
        {
            var jsonParameters = JsonUtility.ToJson(parameters);
            var request = new UnityWebRequest("https://api-dot-mwm-ab-testing.appspot.com/v1")
            {
                uploadHandler = new UploadHandlerRaw(new UTF8Encoding().GetBytes(jsonParameters)),
                downloadHandler = new DownloadHandlerBuffer(),
                method = "POST"
            };
            request.timeout = _timeout;
            var unityWebRequest = request;
            yield return request.SendWebRequest();
            if (request.isNetworkError || request.isHttpError)
            {
                callback(_onFailureVariationName);
                yield break;
            }

            var text = request.downloadHandler.text;
            var jObject = JObject.Parse(text);
            var obj = jObject["key"].ToObject<string>();
            callback(obj);
        }

        public class RequestParameters
        {
            public string abtest_key;

            public string app_id;

            public string app_version;

            public string device_type;
            public string installation_id;

            public RequestParameters(string installID, string appID, string appVersion, string deviceType,
                string abTestKey)
            {
                installation_id = installID;
                app_id = appID;
                app_version = appVersion;
                device_type = deviceType;
                abtest_key = abTestKey;
            }
        }
    }
}