using UnityEngine;

public class SimpleGPUInstancingExample : MonoBehaviour
{
    public Transform Prefab;

    public Material InstancedMaterial;

    private void Awake()
    {
        InstancedMaterial.enableInstancing = true;
        var num = 5;
        for (var i = 0; i < 1000; i++)
        {
            var transform = Instantiate(Prefab,
                new Vector3(Random.Range(-num, num), num + Random.Range(-num, num), Random.Range(-num, num)),
                Quaternion.identity);
            var materialPropertyBlock = new MaterialPropertyBlock();
            var value = new Color(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f));
            materialPropertyBlock.SetColor("_Color", value);
            transform.GetComponent<MeshRenderer>().SetPropertyBlock(materialPropertyBlock);
        }
    }
}