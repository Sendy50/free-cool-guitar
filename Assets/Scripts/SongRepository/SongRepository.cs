using System.Collections.Generic;
using Song;

namespace SongRepository
{
    public interface SongRepository
    {
        List<ISong> GetSongs();

        void Refresh();
    }
}