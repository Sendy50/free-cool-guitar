using System.Collections.Generic;
using GuitarApplication;
using Patch;
using Song;
using UnityEngine;

namespace SongRepository
{
    public class SongRepositoryBehavior : MonoBehaviour, SongRepository
    {
        [SerializeField] private PatchDataList _patchDataList;

        private List<ISong> _songs;

        private void Awake()
        {
            _songs = new List<ISong>();
            Refresh();
        }

        public List<ISong> GetSongs()
        {
            return new List<ISong>(_songs);
        }

        public void Refresh()
        {
            _songs.Clear();
            for (var i = 0; i < _patchDataList.Songs.Count; i++)
            {
                var item = _patchDataList.Songs[i];
                _songs.Add(item);
            }

            var additionalContentProvider = ApplicationGraph.GetAdditionalContentProvider();
            var additionalSongs = additionalContentProvider.GetAdditionalSongs();
            var songsOrder = additionalContentProvider.GetSongsOrder();
            foreach (var item2 in additionalSongs) _songs.Add(item2);
            OrderSongs(songsOrder, _songs);
        }

        private static void OrderSongs(List<string> songsOrder, List<ISong> notOrderedSongs)
        {
            if (songsOrder.Count != 0)
                notOrderedSongs.Sort(delegate(ISong song1, ISong song2)
                {
                    var num = songsOrder.IndexOf(song1.GetId());
                    var num2 = songsOrder.IndexOf(song2.GetId());
                    num = num == -1 ? int.MaxValue : num;
                    num2 = num2 == -1 ? int.MaxValue : num2;
                    return num.CompareTo(num2);
                });
        }
    }
}