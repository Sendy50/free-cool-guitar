using UnityEngine;

public class AdCamera : MonoBehaviour
{
    public SimpleTouchPad touchPad;

    public float rotationSpeed = 10f;

    public float speed = 10f;

    public GameObject target;

    private float currentTranslation;

    private Vector3 point;

    private void Start()
    {
        point = target.transform.position;
        transform.LookAt(point);
    }

    private void FixedUpdate()
    {
        var direction = touchPad.GetDirection();
        var num = direction.y * speed;
        if (currentTranslation != num)
        {
            currentTranslation = num;
            transform.RotateAround(point, new Vector3(0f, 1f, 0f), 5f * (!(direction.y >= 0f) ? -1f : 1f));
        }
    }
}