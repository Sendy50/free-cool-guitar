using UnityEngine;

public class UserActionMover : MonoBehaviour
{
    public GameProgression gameProgression { get; private set; }

    public float time { get; private set; }

    public void Setup(float time, GameProgression gameProgression)
    {
        this.gameProgression = gameProgression;
        this.time = time;
        OnProgressDidUpdate(gameProgression, gameProgression.CurrentTime);
    }

    public void StartMovement()
    {
        if (gameProgression != null)
        {
            gameProgression.OnProgressDidUpdateDelegate -= OnProgressDidUpdate;
            gameProgression.OnProgressDidUpdateDelegate += OnProgressDidUpdate;
        }
    }

    public void StopMovement()
    {
        if (gameProgression != null) gameProgression.OnProgressDidUpdateDelegate -= OnProgressDidUpdate;
    }

    private void OnProgressDidUpdate(GameProgression sender, float currentTime)
    {
        var position = transform.position;
        var time = this.time - currentTime;
        var num = position.x = sender.TimeToForward(time);
        transform.position = position;
    }
}