public interface IGuitarCellPresenter
{
    void SetGuitar(IGuitar guitar);

    void UpdateUI();
}