using MWM.GameEvent;
using Song;
using UnityEngine;

public class GuitarGameReportBuilder : GameReportBuilder<GuitarGameReport>
{
    public GuitarGameReportBuilder(IGameEventSender eventSender)
        : base(eventSender)
    {
    }

    public override void Reset()
    {
        _gameReport = new GuitarGameReport
        {
            duration = 0f,
            is_tuto = false,
            score = 0,
            total_success_stroke = 0,
            total_miss_stroke = 0,
            total_fail_stroke = 0,
            total_success_strum = 0,
            total_miss_strum = 0
        };
    }

    protected override string BuildReportValues()
    {
        _gameReport.duration = Time.time - _gameReport.duration;
        return base.BuildReportValues();
    }

    public void onGameStart()
    {
        _gameReport.duration = Time.time;
    }

    public void onPlayTrack(ISong track)
    {
        _gameReport.track_id = track.GetId();
        switch (track.GetSongType())
        {
            case SongType.Free:
                _gameReport.track_type = "free";
                break;
            case SongType.Unlockable:
                _gameReport.track_type = "unlockable";
                break;
            case SongType.Vip:
                _gameReport.track_type = "vip";
                break;
        }
    }

    public void onTrackFinishWithScore(int score)
    {
        _gameReport.score = score;
    }

    public void setCurrentTrackActionCount(int track_action_count)
    {
        _gameReport.track_action_count = track_action_count;
    }

    public void onSuccessStroke()
    {
        _gameReport.total_success_stroke++;
    }

    public void onMissStroke()
    {
        _gameReport.total_miss_stroke++;
    }

    public void onFailStroke()
    {
        _gameReport.total_fail_stroke++;
    }

    public void onSuccessStrum()
    {
        _gameReport.total_success_strum++;
    }

    public void onMissStrum()
    {
        _gameReport.total_miss_strum++;
    }

    public void setUserIsVip(bool isVip)
    {
        _gameReport.user_type = !isVip ? "no_vip" : "vip";
    }
}