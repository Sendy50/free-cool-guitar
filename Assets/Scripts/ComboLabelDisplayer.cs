using GuitarApplication;
using UnityEngine;
using UnityEngine.UI;

public class ComboLabelDisplayer : MonoBehaviour
{
    public Text test;

    private ScoreManager _scoreManager;

    private void Start()
    {
        _scoreManager = ApplicationGraph.GetScoreManager();
        _scoreManager.OnNewComboDelegate += OnNewCombo;
        _scoreManager.OnResteDelegate += OnScoreReste;
    }

    private void OnDestroy()
    {
        _scoreManager.OnNewComboDelegate -= OnNewCombo;
        _scoreManager.OnResteDelegate -= OnScoreReste;
    }

    private void OnNewCombo(ScoreManager sender, ScoreManager.Combo combo, int consecutiveCatch)
    {
        test.text = combo.scoreMultiplicateur.ToString();
    }

    private void OnScoreReste()
    {
        var text = test;
        var currentCombo = _scoreManager.currentCombo;
        text.text = currentCombo.scoreMultiplicateur.ToString();
    }
}