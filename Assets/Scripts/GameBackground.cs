using AssetBundles.GuitarSkin;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
public class GameBackground : MonoBehaviour
{
    public UserSettings userSettings;

    public SpriteRenderer backgroundRenderer;

    [SerializeField] private SpriteRenderer _soundboxRenderer;

    public SpriteRenderer[] frets;

    public GameObject fretboardsContainer;

    public GuitarHolder guitarHolder;

    public InterfaceIdiomEditor interfaceIdiomEditor;

    public float iphoneFretsXOffset;

    public float ipadFretsXOffset;

    private void Start()
    {
        UpdateOrientation();
        userSettings.GameOrientationChanged += OnOrientationChange;
        guitarHolder.OnSelectedGuitarChanged += OnSelectedGuitarChanged;
        OnSelectedGuitarChanged(guitarHolder.selectedGuitar);
        UpdateUIForInterfaceIdiom();
    }

    private void OnDestroy()
    {
        userSettings.GameOrientationChanged -= OnOrientationChange;
        guitarHolder.OnSelectedGuitarChanged -= OnSelectedGuitarChanged;
    }

    private void OnOrientationChange(UserSettings.GameOrientation gameOrientation)
    {
        UpdateOrientation();
    }

    private void UpdateOrientation()
    {
        var rotation = gameObject.transform.rotation;
        rotation.eulerAngles = new Vector3(0f,
            userSettings.GetGameOrientation() == UserSettings.GameOrientation.LeftHanded ? 180 : 0, 0f);
        gameObject.transform.rotation = rotation;
    }

    private void OnSelectedGuitarChanged(IGuitar guitar)
    {
        _soundboxRenderer.transform.localPosition = guitar.GetChordsSoundboxPosition();
        guitar.GetSkinProxy().GetSkin(delegate(IGuitarSkin skin, bool success)
        {
            backgroundRenderer.sprite = skin.GetGuitarBackground();
            _soundboxRenderer.sprite = skin.GetGuitarSoundbox();
            var fretboards = skin.GetFretboards();
            for (var i = 0; i < frets.Length; i++) frets[i].sprite = fretboards[i];
        });
    }

    private void UpdateUIForInterfaceIdiom()
    {
        var position = fretboardsContainer.transform.position;
        position.x = iphoneFretsXOffset;
        fretboardsContainer.transform.position = position;
    }
}