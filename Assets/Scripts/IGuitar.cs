using System;
using AssetBundles.GuitarSkin;
using UnityEngine;

public interface IGuitar
{
    string FilePath { get; }

    string GuitarName { get; }

    bool IsPremium { get; }

    SkinReference SkinReference { get; }

    Sprite GuitarSelectionCellImage { get; }
    
    Sprite GuitarBgImage{ get; }

    string PreviewName { get; }

    bool isSelected { get; }

    event Action<bool> OnGuitarSelected;

    void SetupSkinProxy(IGuitarSkinProxy proxy);

    IGuitarSkinProxy GetSkinProxy();

    Vector2 GetArpeggiosSoundboxPosition();

    Vector2 GetChordsSoundboxPosition();

    bool loadSprite();

    Sprite getAtlas(string atlasName);
}