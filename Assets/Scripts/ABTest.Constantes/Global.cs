using System.Runtime.InteropServices;

namespace ABTest.Constantes
{
    [StructLayout(LayoutKind.Sequential, Size = 1)]
    public struct Global
    {
        public const string FaildDefaultChoiceVariationKey = "failed_default_choice";
    }
}