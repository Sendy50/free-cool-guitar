using UnityEngine;
using Utils;

namespace MWM.GameEvent
{
    public class iOSNativeInappEventSender : MonoBehaviour
    {
        public IGameEventSender gameEventSender { get; private set; }

        // [DllImport("__Internal")]
        // private static extern void inapp_game_event_sender_start();

        private void Start()
        {
            Precondition.CheckNotNull(gameEventSender);
        }

        public void Initialize(IGameEventSender gameEventSender)
        {
            this.gameEventSender = gameEventSender;
            gameObject.SetActive(true);
        }

        public void onPaymentQueueUpdateTransaction(string message)
        {
            var allTransactionParam = JsonUtility.FromJson<AllTransactionParam>(message);
            var allTransactionParam2 = allTransactionParam.allTransactionParam;
            foreach (var sKPaymentTransactionParam in allTransactionParam2)
                gameEventSender.AddEvent(EventType.InappTransaction, sKPaymentTransactionParam.ToJSONString());
            gameEventSender.FlushAndSendPersistedEvents();
        }
    }
}