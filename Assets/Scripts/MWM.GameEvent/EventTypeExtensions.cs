using System;

namespace MWM.GameEvent
{
    public static class EventTypeExtensions
    {
        public static string StringValue(this EventType type)
        {
            switch (type)
            {
                case EventType.AdvertisingId:
                    return "advertising_id";
                case EventType.InappTransaction:
                    return "inapp_transaction";
                case EventType.InappReceipt:
                    return "inapp_receipt";
                case EventType.IntroCarouselDisplay:
                    return "intro_carousel_display";
                case EventType.IntroCarouselDisplaySecondSlide:
                    return "intro_carousel_display_second_slide";
                case EventType.IntroCarouselDisplayThirdSlide:
                    return "intro_carousel_display_third_slide";
                case EventType.IntroCarouselCloseFromNoSubscriptionSlide:
                    return "last_carousel_slide_close_to_main_interface";
                case EventType.IntroCarouselDisplaySubscriptionSlide:
                    return "subscription_slide_app_launch_display";
                case EventType.IntroCarouselCloseFromSubscriptionSlide:
                    return "subscription_slide_app_launch_dismiss";
                case EventType.IntroCarouselLastSlideStatusRequestTimeOut:
                    return "intro_carousel_last_slide_status_timeout";
                case EventType.InappPurchased:
                    return "inapp_purchased";
                case EventType.AdRequested:
                    return "ad_requested";
                case EventType.AdDisplayed:
                    return "ad_displayed";
                case EventType.ClickBuy:
                    return "user_click_buy";
                case EventType.CustomRatingPopupDisplayed:
                    return "custom_rating_popup_displayed";
                case EventType.CustomRatingPopupClickOk:
                    return "custom_rating_popup_click_ok";
                case EventType.CustomRatingPopupClickNoThanks:
                    return "custom_rating_popup_click_no_thanks";
                case EventType.ApplicationStart:
                    return "application_start";
                case EventType.StoreDisplay:
                    return "store_display";
                case EventType.StoreDismiss:
                    return "store_dismiss";
                case EventType.GameReport:
                    return "game_report";
                default:
                    throw new Exception("Unknown EventType");
            }
        }

        public static string StringValue(this IntroCarouselCloseFromSubscriptionSlideReason reason)
        {
            switch (reason)
            {
                case IntroCarouselCloseFromSubscriptionSlideReason.Buy:
                    return "BUY_COMPLETED";
                case IntroCarouselCloseFromSubscriptionSlideReason.CloseButton:
                    return "CLOSE_BUTTON";
                default:
                    throw new Exception("Unknown reason");
            }
        }

        public static string StringValue(this StoreDisplaySource source)
        {
            switch (source)
            {
                case StoreDisplaySource.FreeModeInterface:
                    return "FREE_GUITAR_MODE_NAV_BAR";
                case StoreDisplaySource.ClickPremiumTrack:
                    return "CLICK_PREMIUM_TRACK";
                case StoreDisplaySource.TrackListNavBar:
                    return "TRACK_LIST_NAV_BAR";
                case StoreDisplaySource.ClickLockedGuitar:
                    return "CLICK_LOCKED_GUITAR";
                case StoreDisplaySource.GuitarStoreNavBar:
                    return "GUITAR_STORE_NAV_BAR";
                case StoreDisplaySource.IngamePauseNavBar:
                    return "INGAME_PAUSE_NAV_BAR";
                case StoreDisplaySource.EndGame:
                    return "END_GAME";
                case StoreDisplaySource.PauseMenu:
                    return "PAUSE_MENU";
                default:
                    throw new Exception("Unknown source");
            }
        }

        public static string StringValue(this StoreDismissReason reason)
        {
            switch (reason)
            {
                case StoreDismissReason.BuyWeek:
                    return "BUY_COMPLETED_WEEK";
                case StoreDismissReason.BuyYear:
                    return "BUY_COMPLETED_YEAR";
                case StoreDismissReason.CloseButton:
                    return "CLOSE_BUTTON";
                case StoreDismissReason.Restore:
                    return "RESTORE_SUCCESS";
                default:
                    throw new Exception("Unknown reason");
            }
        }
    }
}