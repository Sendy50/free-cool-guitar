namespace MWM.GameEvent
{
    public enum StoreDismissReason
    {
        BuyWeek,
        BuyYear,
        CloseButton,
        Restore
    }
}