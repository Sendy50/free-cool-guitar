using UnityEngine;

namespace MWM.GameEvent
{
    public abstract class GameReportBuilder<ReportType> where ReportType : GameReport
    {
        protected readonly IGameEventSender _eventSender;

        public GameReportBuilder(IGameEventSender eventSender)
        {
            _eventSender = eventSender;
            Reset();
        }

        public virtual EventType GameReportEventType => EventType.GameReport;

        protected ReportType _gameReport { get; set; }

        public abstract void Reset();

        public void SetDuration(float duration)
        {
            _gameReport.duration = duration;
        }

        public void IncrementDuration(float deltaTime)
        {
            _gameReport.duration += deltaTime;
        }

        public void SetIsTutorial(bool isTutorial)
        {
            _gameReport.is_tuto = isTutorial;
        }

        protected virtual string BuildReportValues()
        {
            return JsonUtility.ToJson(_gameReport);
        }

        public void SendReport()
        {
            _eventSender.SendEvent(GameReportEventType, BuildReportValues());
        }
    }
}