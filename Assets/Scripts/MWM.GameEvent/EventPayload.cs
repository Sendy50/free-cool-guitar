using System;

namespace MWM.GameEvent
{
    [Serializable]
    public class EventPayload
    {
        public string installation_id;

        public string event_type;

        public string value;

        public long local_ts_millis;

        public bool online;

        public string vendor_id;

        public string apps_flyer_id;

        public string abtests;

        public string configs;

        public EventPayload(string installationId, string type, long localTsMillis, bool isOnline, string vendorId,
            string appsFlyerId, string ab_tests, string configs = "", string optional_value = "")
        {
            installation_id = installationId;
            event_type = type;
            value = optional_value;
            local_ts_millis = localTsMillis;
            online = isOnline;
            vendor_id = vendorId;
            apps_flyer_id = appsFlyerId;
            abtests = ab_tests;
            this.configs = configs;
        }
    }
}