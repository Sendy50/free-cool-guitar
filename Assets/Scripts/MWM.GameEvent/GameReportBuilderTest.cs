namespace MWM.GameEvent
{
    public class GameReportBuilderTest : GameReportBuilder<GameReport>
    {
        public GameReportBuilderTest(IGameEventSender eventSender)
            : base(eventSender)
        {
        }

        public override void Reset()
        {
            _gameReport = new GameReport
            {
                duration = 0f,
                is_tuto = false
            };
        }
    }
}