using System;

namespace MWM.GameEvent
{
    [Serializable]
    public struct ABTestData
    {
        public string abtest_key;

        public string variation;

        public ABTestData(string abTestKey, string variationName)
        {
            abtest_key = abTestKey;
            variation = variationName;
        }
    }
}