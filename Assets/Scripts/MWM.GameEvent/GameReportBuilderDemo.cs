namespace MWM.GameEvent
{
    public class GameReportBuilderDemo : GameReportBuilder<GameReportDemo>
    {
        public GameReportBuilderDemo(IGameEventSender eventSender)
            : base(eventSender)
        {
        }

        public override EventType GameReportEventType => EventType.GameReport;

        public void SetDemoValue(string value)
        {
            _gameReport.demoValue = value;
        }

        public override void Reset()
        {
            _gameReport = new GameReportDemo
            {
                duration = 0f,
                is_tuto = false,
                demoValue = string.Empty
            };
            _gameReport.is_tuto = true;
        }
    }
}