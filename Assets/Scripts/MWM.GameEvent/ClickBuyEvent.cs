using System;
using UnityEngine;

namespace MWM.GameEvent
{
    [Serializable]
    public class ClickBuyEvent
    {
        public enum Sources
        {
            IntroCarousel,
            Store,
            InHouseAdsStore
        }

        public string source;

        public string productId;

        public string currencyCode;

        public float price;

        public ClickBuyEvent(Sources source, string productId, string currencyCode, float price)
            : this(source.StringValue(), productId, currencyCode, price)
        {
        }

        private ClickBuyEvent(string source, string productId, string currencyCode, float price)
        {
            this.source = source;
            this.productId = productId;
            this.currencyCode = currencyCode;
            this.price = price;
        }

        public string ToJsonString()
        {
            return JsonUtility.ToJson(this);
        }
    }
}