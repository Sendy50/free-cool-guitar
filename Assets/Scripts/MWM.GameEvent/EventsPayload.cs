using System;

namespace MWM.GameEvent
{
    [Serializable]
    public class EventsPayload
    {
        public EventPayload[] events;

        public EventsPayload(EventPayload[] events)
        {
            this.events = events;
        }
    }
}