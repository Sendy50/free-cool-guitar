namespace MWM.GameEvent
{
    public enum ClickBuySource
    {
        SubscriptionSplash,
        IntroCarousel,
        Store,
        Unknown
    }
}