namespace MWM.GameEvent
{
    public class Result
    {
        public readonly string ErrorString;

        public readonly bool IsSuccess;
        public readonly string ResponseString;

        public Result(bool isSuccess, string resultString)
        {
            IsSuccess = isSuccess;
            if (IsSuccess)
                ResponseString = resultString;
            else
                ErrorString = resultString;
        }
    }
}