using System;

namespace MWM.GameEvent
{
    [Serializable]
    public class OriginalSKPaymentTransactionParam
    {
        public string transaction_identifier;

        public string product_identifier;

        public double transaction_date;

        public string transaction_state;
    }
}