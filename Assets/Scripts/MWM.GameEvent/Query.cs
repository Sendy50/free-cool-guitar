using System;
using System.Collections;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;

namespace MWM.GameEvent
{
    public class Query
    {
        public static IEnumerator GET(string uri, Action<Result> callback,
            Action<UnityWebRequest> customHeadersCallback = null)
        {
            var request = UnityWebRequest.Get(uri);
            AddCustomHeaders(request);
            var unityWebRequest = request;
            yield return request.SendWebRequest();
            if (request.isNetworkError || request.isHttpError)
                callback(new Result(false, request.error));
            else
                callback(new Result(true, request.downloadHandler.text));
        }

        public static IEnumerator POST(string uri, string parameters, Action<Result> callback = null,
            Action<UnityWebRequest> customHeadersCallback = null)
        {
            var request = new UnityWebRequest(uri)
            {
                uploadHandler = new UploadHandlerRaw(new UTF8Encoding().GetBytes(parameters)),
                downloadHandler = new DownloadHandlerBuffer(),
                method = "POST"
            };
            AddCustomHeaders(request);
            customHeadersCallback?.Invoke(request);
            var unityWebRequest = request;
            yield return request.SendWebRequest();
            if (request.isNetworkError || request.isHttpError)
                callback?.Invoke(new Result(false, request.error));
            else
                callback?.Invoke(new Result(true, request.downloadHandler.text));
        }

        private static void AddCustomHeaders(UnityWebRequest request)
        {
            request.SetRequestHeader("Content-Type", "application/json");
            request.SetRequestHeader("X-APP-ID", Application.identifier);
            request.SetRequestHeader("X-Device-Type", GetDeviceType());
            request.SetRequestHeader("X-App-Version", Application.version);
        }

        private static string GetDeviceType()
        {
            return "android";
        }
    }
}