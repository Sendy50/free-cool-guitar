using System;

namespace MWM.GameEvent
{
    [Serializable]
    public class AllTransactionParam
    {
        public SKPaymentTransactionParam[] allTransactionParam;
    }
}