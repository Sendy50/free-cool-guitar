namespace MWM.GameEvent
{
    public interface IABTestEventDataBuilder
    {
        ABTestData[] buildEventData();
    }
}