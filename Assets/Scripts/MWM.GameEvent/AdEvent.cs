using System;
using UnityEngine;

namespace MWM.GameEvent
{
    [Serializable]
    public class AdEvent
    {
        public string advertiser;

        public string metaplacement;

        public string placement;

        public string type;

        public AdEvent(string advertiser, string metaPlacementName, string placementId, string type)
        {
            this.advertiser = advertiser;
            metaplacement = metaPlacementName;
            placement = placementId;
            this.type = type;
        }

        public string ToJSONString()
        {
            return JsonUtility.ToJson(this);
        }
    }
}