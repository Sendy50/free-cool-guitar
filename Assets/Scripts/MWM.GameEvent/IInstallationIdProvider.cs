namespace MWM.GameEvent
{
    public interface IInstallationIdProvider
    {
        string GetInstallationId();
    }
}