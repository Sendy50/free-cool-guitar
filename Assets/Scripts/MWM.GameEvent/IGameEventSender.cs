namespace MWM.GameEvent
{
    public interface IGameEventSender
    {
        IInstallationIdProvider GetInstallationIdProvider();

        void SendEvent(EventType eventType, string value = "");

        void AddEvent(EventType eventType, string value = "");

        void FlushAndSendPersistedEvents();
    }
}