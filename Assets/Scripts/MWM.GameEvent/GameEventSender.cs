using System;
using System.Collections.Generic;
// using Assets.MWM.Core.Utils;
using Config;
using JetBrains.Annotations;
using MWM.GameEvent.Settings;
using Newtonsoft.Json;
using UnityEngine;

namespace MWM.GameEvent
{
    public class GameEventSender : MonoBehaviour, IGameEventSender
    {
        private const int MaximumNumberOfEventsPersisted = 50;

        private static GameEventSender _instance;

        public GameEventSettings InternalGameEventSettings;

        private readonly DateTime _epochDateTime = new DateTime(1970, 1, 1, 0, 0, 0);

        private IABTestEventDataBuilder _ABTestEventDataBuilder;

        private ConfigStorageContainer.ConfigStorage _configStorage;

        private GameEventStorage _eventStorage;

        private InstallationIdProvider _installationIdProvider;

        private List<EventPayload> _persistedEvents;

        public string InstallationId => _installationIdProvider.GetInstallationId();

        private void Awake()
        {
            if (_instance == null)
            {
                _instance = this;
                Initialize();
                DontDestroyOnLoad(this);
            }
            else
            {
                Destroy(this);
            }
        }

        public IInstallationIdProvider GetInstallationIdProvider()
        {
            return _installationIdProvider;
        }

        public void SendEvent(EventType eventType, string value = "")
        {
            //SendEvent(eventType.StringValue(), value);
        }

        public void AddEvent(EventType eventType, string value = "")
        {
            //EventPayload eventPayload = CreateEventPayload(eventType.StringValue(), value);
            //PersistEvent(eventPayload);
        }

        public void FlushAndSendPersistedEvents()
        {
            var obj = new EventsPayload(_persistedEvents.ToArray());
            var parameters = JsonUtility.ToJson(obj);
            _persistedEvents.Clear();
            _eventStorage.SaveEvents(_persistedEvents);
            StartCoroutine(Query.POST(InternalGameEventSettings.EndPoint, parameters));
        }

        public static GameEventSender Instance()
        {
            return _instance;
        }

        private void Initialize()
        {
            if (InternalGameEventSettings == null) throw new ArgumentException("A GameEventSettings must be provide");
            _installationIdProvider = new InstallationIdProvider();
            _eventStorage = new GameEventStorage();
            _persistedEvents = _eventStorage.GetSavedEvents();
            InitAppsFlyer();
            //SendAdvertisingIdentifier();
        }

        public void SetABTestEventDataBuilder(IABTestEventDataBuilder abTestEventDataBuilder)
        {
            _ABTestEventDataBuilder = abTestEventDataBuilder;
        }

        public void SetConfigStorage(ConfigStorageContainer.ConfigStorage configStorage)
        {
            _configStorage = configStorage;
        }

        // private void SendEvent(string eventType, string value = "")
        // {
        // 	EventPayload eventPayload = CreateEventPayload(eventType, value);
        // 	if (!IsNetworkReachable())
        // 	{
        // 		PersistEvent(eventPayload);
        // 		return;
        // 	}
        // 	if (_persistedEvents.Count > 0)
        // 	{
        // 		_persistedEvents.Add(eventPayload);
        // 		FlushAndSendPersistedEvents();
        // 		return;
        // 	}
        // 	EventPayload[] events = new EventPayload[1]
        // 	{
        // 		eventPayload
        // 	};
        // 	EventsPayload obj = new EventsPayload(events);
        // 	string parameters = JsonUtility.ToJson(obj);
        // 	StartCoroutine(Query.POST(InternalGameEventSettings.EndPoint, parameters));
        // }

        // private void SendAdvertisingIdentifier()
        // {
        //     AdvertisingIdUtils.GetAsyncAdvertisingId(delegate(string identifier)
        //     {
        //         SendEvent(EventType.AdvertisingId, identifier);
        //     });
        // }

        // private EventPayload CreateEventPayload(string eventType, string value)
        // {
        // 	string vendorId = "android-vendor-id";
        // 	string variation = GetVariation();
        // 	string configs = string.Empty;
        // 	if (_configStorage != null)
        // 	{
        // 		ConfigStorageContainer config = _configStorage.GetConfig();
        // 		if (config != null)
        // 		{
        // 			configs = config.GetConfigAsJsonString();
        // 		}
        // 	}
        // 	return new EventPayload(InstallationId, eventType, (long)DateTime.UtcNow.Subtract(_epochDateTime).TotalMilliseconds, IsNetworkReachable(), vendorId, AppsFlyer.getAppsFlyerId(), variation, configs, value);
        // }

        private string GetVariation()
        {
            var result = string.Empty;
            if (_ABTestEventDataBuilder != null)
            {
                var flag = false;
                if (_configStorage != null)
                {
                    var config = _configStorage.GetConfig();
                    flag = config.FourthSlideDisabledValue;
                }

                if (!flag)
                {
                    var array = _ABTestEventDataBuilder.buildEventData();
                    if (array.Length != 0) result = JsonConvert.SerializeObject(array);
                }
            }

            return result;
        }

        private void PersistEvent(EventPayload eventPayload)
        {
            if (_persistedEvents.Count >= 50) _persistedEvents.RemoveAt(0);
            _persistedEvents.Add(eventPayload);
            _eventStorage.SaveEvents(_persistedEvents);
        }

        private static bool IsNetworkReachable()
        {
            return Application.internetReachability != NetworkReachability.NotReachable;
        }

        private void InitAppsFlyer()
        {
            // AppsFlyer.setIsDebug(isDebug: false);
            InitAppsFlyerForAndroid();
        }

        [UsedImplicitly]
        private void InitAppsFlyerForAndroid()
        {
            var appsFlyerConfig = InternalGameEventSettings.AppsFlyerConfig;
            if (appsFlyerConfig.AppsFlyerDevKeyForAndroid == null)
                throw new Exception(
                    "AppsFlyer Dev Key For Android is missing, check value into AppsFlyerSettings (ScripatableObject)");
            // AppsFlyer.setAppsFlyerKey(appsFlyerConfig.AppsFlyerDevKeyForAndroid);
            // AppsFlyer.setCustomerUserID(InstallationId);
            // AppsFlyer.setAppID(Application.identifier);
            // AppsFlyer.init(appsFlyerConfig.AppsFlyerDevKeyForAndroid, "AppsFlyerTrackerCallbacks");
        }
    }
}