using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

namespace MWM.GameEvent
{
    public class InstallationIdProvider : IInstallationIdProvider
    {
        private static readonly string InstallationIdFilePath = Application.persistentDataPath + "/game_event_iid.dat";

        private readonly string _installationId;

        internal InstallationIdProvider()
        {
            _installationId = GetOrGenerateInstallationId();
        }

        public string GetInstallationId()
        {
            return _installationId;
        }

        private static string GetOrGenerateInstallationId()
        {
            if (File.Exists(InstallationIdFilePath))
                using (var serializationStream = File.OpenRead(InstallationIdFilePath))
                {
                    var binaryFormatter = new BinaryFormatter();
                    return (string) binaryFormatter.Deserialize(serializationStream);
                }

            var text = Guid.NewGuid().ToString();
            var serializationStream2 = File.Create(InstallationIdFilePath);
            var binaryFormatter2 = new BinaryFormatter();
            binaryFormatter2.Serialize(serializationStream2, text);
            return text;
        }
    }
}