using System;

namespace MWM.GameEvent
{
    public static class Extension
    {
        public static string StringValue(this ClickBuyEvent.Sources source)
        {
            switch (source)
            {
                case ClickBuyEvent.Sources.IntroCarousel:
                    return "INTRO_CAROUSEL_LAST_SLIDE";
                case ClickBuyEvent.Sources.Store:
                    return "STORE";
                case ClickBuyEvent.Sources.InHouseAdsStore:
                    return "STORE_SPLASH";
                default:
                    throw new ArgumentOutOfRangeException("source", source, null);
            }
        }
    }
}