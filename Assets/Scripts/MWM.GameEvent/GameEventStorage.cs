using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

namespace MWM.GameEvent
{
    public class GameEventStorage
    {
        private static readonly string EventStorageFilePath =
            Application.persistentDataPath + "/event-storage-path-2.dat";

        public List<EventPayload> GetSavedEvents()
        {
            if (!File.Exists(EventStorageFilePath)) return new List<EventPayload>();
            var serializationStream = File.OpenRead(EventStorageFilePath);
            var binaryFormatter = new BinaryFormatter();
            return (List<EventPayload>) binaryFormatter.Deserialize(serializationStream);
        }

        public void SaveEvents(List<EventPayload> events)
        {
            var serializationStream = File.Create(EventStorageFilePath);
            var binaryFormatter = new BinaryFormatter();
            binaryFormatter.Serialize(serializationStream, events);
        }
    }
}