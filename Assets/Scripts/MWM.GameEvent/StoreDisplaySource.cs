namespace MWM.GameEvent
{
    public enum StoreDisplaySource
    {
        FreeModeInterface,
        ClickPremiumTrack,
        TrackListNavBar,
        ClickLockedGuitar,
        GuitarStoreNavBar,
        IngamePauseNavBar,
        EndGame,
        PauseMenu
    }
}