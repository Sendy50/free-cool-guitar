using System;
using UnityEngine;

namespace MWM.GameEvent
{
    [Serializable]
    public class SKPaymentTransactionParam
    {
        public string transaction_identifier;

        public string product_identifier;

        public double transaction_date;

        public string transaction_state;

        public OriginalSKPaymentTransactionParam original_transaction;

        public string ToJSONString()
        {
            return JsonUtility.ToJson(this);
        }
    }
}