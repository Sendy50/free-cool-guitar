// using AudienceNetwork;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class InterstitialAdTest : MonoBehaviour
{
    public Text statusLabel;
    // private InterstitialAd interstitialAd;

    private bool isLoaded;

    private void OnDestroy()
    {
        // if (interstitialAd != null) interstitialAd.Dispose();
        // Debug.Log("InterstitialAdTest was destroyed!");
    }

    public void LoadInterstitial()
    {
        statusLabel.text = "Loading interstitial ad...";
        // var interstitialAd = this.interstitialAd =
        //     new InterstitialAd("VID_HD_16_9_15S_APP_INSTALL#404902449946116_405463959889965");
        // this.interstitialAd.Register(gameObject);
        // this.interstitialAd.InterstitialAdDidLoad = delegate
        // {
        //     Debug.Log("Interstitial ad loaded.");
        //     isLoaded = true;
        //     statusLabel.text = "Ad loaded. Click show to present!";
        // };
        // interstitialAd.InterstitialAdDidFailWithError = delegate(string error)
        // {
        //     Debug.Log("Interstitial ad failed to load with error: " + error);
        //     statusLabel.text = "Interstitial ad failed to load. Check console for details.";
        // };
        // interstitialAd.InterstitialAdWillLogImpression = delegate { Debug.Log("Interstitial ad logged impression."); };
        // interstitialAd.InterstitialAdDidClick = delegate { Debug.Log("Interstitial ad clicked."); };
        // this.interstitialAd.LoadAd();
    }

    public void ShowInterstitial()
    {
        if (isLoaded)
        {
            // interstitialAd.Show();
            isLoaded = false;
            statusLabel.text = string.Empty;
        }
        else
        {
            statusLabel.text = "Ad not loaded. Click load to request an ad.";
        }
    }

    public void NextScene()
    {
        SceneManager.LoadScene("AdViewScene");
    }
}