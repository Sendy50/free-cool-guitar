using GuitarApplication;
using MWM.Audio;
using UnityEngine;

public class GameProgression : MonoBehaviour
{
    public delegate void OnProgressDidUpdate(GameProgression sender, float currentTime);

    private const float ForwardBySecond = 10f;

    public GameSceneLayout sceneLayout;

    public UserSettings userSettings;

    public float StartTime = -1f;

    private Player _player;

    private float _timeScale = 1f;

    public float CurrentTime { get; private set; }

    public float ForwardProgression => TimeToForward(CurrentTime);

    public float PerfectForwardOffset => sceneLayout.perfectPosition;

    private void Awake()
    {
        gameObject.SetActive(false);
        Reset();
    }

    public void Reset()
    {
        CurrentTime = StartTime;
    }

    private void Update()
    {
        if (CurrentTime < 0f)
        {
            var deltaTime = Time.deltaTime;
            CurrentTime = Mathf.Min(0f, CurrentTime + deltaTime);
        }
        else
        {
            CurrentTime = _player.GetCurrentTime();
        }

        if (OnProgressDidUpdateDelegate != null) OnProgressDidUpdateDelegate(this, CurrentTime);
    }

    public event OnProgressDidUpdate OnProgressDidUpdateDelegate;

    public static float DurationToDimention(float duration)
    {
        return duration * 10f;
    }

    public static float DimentionToDuration(float dimention)
    {
        return dimention / 10f;
    }

    public float TimeToForward(float time)
    {
        var num = time * 10f + PerfectForwardOffset;
        return userSettings.GetGameOrientation() != 0 ? 0f - num : num;
    }

    public float ForwardToTime(float forward)
    {
        if (userSettings.GetGameOrientation() == UserSettings.GameOrientation.LeftHanded) forward = 0f - forward;
        return (forward - PerfectForwardOffset) / 10f;
    }

    public bool GetIsRunning()
    {
        return gameObject.activeSelf;
    }

    public void StartProgression()
    {
        if (_player == null) _player = ApplicationGraph.GetMusicGamingAudioEngine().GetMultiPlayerUnit().GetPlayer(0);
        Reset();
        gameObject.SetActive(true);
    }

    public void Resume()
    {
        if (_player == null) _player = ApplicationGraph.GetMusicGamingAudioEngine().GetMultiPlayerUnit().GetPlayer(0);
        gameObject.SetActive(true);
    }

    public void StopProgression()
    {
        gameObject.SetActive(false);
    }
}