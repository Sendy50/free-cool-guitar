using System;
using System.Collections.Generic;

public interface IGuitarHolder
{
    List<IGuitar> allGuitars { get; }

    IGuitar selectedGuitar { get; }

    event Action<IGuitar> OnSelectedGuitarChanged;

    void SetSelectedGuitar(IGuitar guitar);
}