using UnityEngine;
using UnityEngine.UI;

public class PageViewIndicator : MonoBehaviour
{
    [SerializeField] private Sprite _dot;

    [SerializeField] private float _spaceBetweenIndicators;

    private HorizontalLayoutGroup _horizontalLayout;

    private void Start()
    {
        _horizontalLayout = gameObject.AddComponent<HorizontalLayoutGroup>();
        _horizontalLayout.childAlignment = TextAnchor.MiddleCenter;
        _horizontalLayout.spacing = _spaceBetweenIndicators;
        _horizontalLayout.childForceExpandHeight = false;
        _horizontalLayout.childForceExpandWidth = false;
        _horizontalLayout.childControlHeight = true;
        _horizontalLayout.childControlWidth = false;
    }

    public void initializeWithNbPages(int nbPages)
    {
        for (var i = 0; i < nbPages; i++)
        {
            var gameObject = new GameObject("Indicator_" + i);
            var image = gameObject.AddComponent<Image>();
            image.color = new Color(1f, 1f, 1f, 0.3f);
            image.sprite = _dot;
            image.preserveAspect = true;
            gameObject.transform.SetParent(transform, false);
            gameObject.transform.localPosition = Vector3.zero;
            gameObject.transform.localScale = Vector3.one;
            var gameObject2 = new GameObject("Indicator_On_" + i);
            var image2 = gameObject2.AddComponent<Image>();
            image2.sprite = _dot;
            image2.preserveAspect = true;
            gameObject2.transform.SetParent(gameObject.transform, false);
            gameObject2.transform.localPosition = Vector3.zero;
            gameObject2.transform.localScale = Vector3.one;
            var component = gameObject2.GetComponent<RectTransform>();
            component.anchorMin = new Vector2(0f, 0f);
            component.anchorMax = new Vector2(1f, 1f);
            component.offsetMin = new Vector2(0f, 0f);
            component.offsetMax = new Vector2(0f, 0f);
            var toggle = gameObject.AddComponent<Toggle>();
            toggle.interactable = false;
            toggle.transition = Selectable.Transition.SpriteSwap;
            toggle.graphic = image2;
        }
    }
}