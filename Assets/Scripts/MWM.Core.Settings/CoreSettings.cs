using UnityEngine;

namespace MWM.Core.Settings
{
    [CreateAssetMenu(menuName = "MWM/Core/CoreSettings", fileName = "CoreSettings.asset")]
    public class CoreSettings : ScriptableObject
    {
        public BuildTarget CurrentBuildTarget;
    }
}