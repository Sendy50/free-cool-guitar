using System.Collections;
using UnityEngine;

[RequireComponent(typeof(ParticleSystem))]
public class ComboUI : MonoBehaviour
{
    public ParticleSystem particles;

    public void Play()
    {
        gameObject.SetActive(true);
        StartCoroutine(PlayRoutine());
    }

    private IEnumerator PlayRoutine()
    {
        particles.Play();
        yield return new WaitWhile(() => particles.isPlaying);
        gameObject.SetActive(false);
    }
}