public interface ITableView
{
    TableViewDelegate tableViewDelegate { get; set; }

    void reloadVisiblesCell();

    void startTableView();

    void jumpToRow(int rowIndex);
}