using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

public class MNAndroidAlert : MonoBehaviour
{
    private IEnumerable<string> actions;

    private string message = string.Empty;

    public Action<string> OnComplete = delegate { };

    private string title = string.Empty;

    public static MNAndroidAlert Create(string title, string message, IEnumerable<string> actions)
    {
        var mNAndroidAlert = new GameObject("AndroidPopUp").AddComponent<MNAndroidAlert>();
        mNAndroidAlert.title = title;
        mNAndroidAlert.message = message;
        mNAndroidAlert.actions = actions;
        return mNAndroidAlert;
    }

    public void Show()
    {
        var stringBuilder = new StringBuilder();
        var enumerator = actions.GetEnumerator();
        if (enumerator.MoveNext()) stringBuilder.Append(enumerator.Current);
        while (enumerator.MoveNext())
        {
            stringBuilder.Append("|");
            stringBuilder.Append(enumerator.Current);
        }

        MNAndroidNative.showMessage(title, message, stringBuilder.ToString(),
            MNP_PlatformSettings.Instance.AndroidDialogTheme);
    }

    public void onPopUpCallBack(string result)
    {
        OnComplete(result);
        Destroy(gameObject);
    }
}