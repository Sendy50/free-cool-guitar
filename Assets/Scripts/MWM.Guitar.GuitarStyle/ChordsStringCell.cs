using UnityEngine;
using UnityEngine.EventSystems;

namespace MWM.Guitar.GuitarStyle
{
    public class ChordsStringCell : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerDownHandler,
        IPointerUpHandler, IEventSystemHandler
    {
        public delegate void OnStringTouched(GuitarString touchedString, int stringIndex);

        [SerializeField] private GuitarString _guitarString;

        [SerializeField] private int _stringIndex;

        private bool _triggered;

        public void OnPointerDown(PointerEventData eventData)
        {
            OnPointerTriggered();
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            OnPointerTriggered();
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            OnPointerUntriggered();
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            OnPointerUntriggered();
        }

        public event OnStringTouched OnStringTouchedDelegate;

        private void OnPointerTriggered()
        {
            if (!_triggered)
            {
                _triggered = true;
                if (OnStringTouchedDelegate != null) OnStringTouchedDelegate(_guitarString, _stringIndex);
            }
        }

        private void OnPointerUntriggered()
        {
            _triggered = false;
        }
    }
}