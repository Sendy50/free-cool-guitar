using System;
using System.Collections;
using GuitarApplication;
using UnityEngine;
using UnityEngine.EventSystems;

namespace MWM.Guitar.GuitarStyle
{
    [RequireComponent(typeof(BoxCollider2D))]
    public class ArpeggiosNoteCell : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerDownHandler,
        IPointerUpHandler, IEventSystemHandler
    {
        private static readonly int HideHash = Animator.StringToHash("hide");

        [SerializeField] private GameObject _triggeredFeedbackContainer;

        [SerializeField] private SpriteRenderer _triggeredSprite;

        [SerializeField] private Transform _nameSpriteContainer;

        [SerializeField] private SpriteRenderer _nameSpriteChar1;

        [SerializeField] private SpriteRenderer _nameSpriteChar2;

        [SerializeField] private SpriteRenderer _nameSpriteChar3;

        [SerializeField] private float _nameSpriteCharSpacing;

        [SerializeField] private SpriteRenderer _nameSpriteBg;

        [SerializeField] private ParticleSystem _triggeredParticles;

        [SerializeField] private Animator _animator;

        private BoxCollider2D _collider;

        private int _stringIndex;

        private bool _triggered;

        private Coroutine _triggeredCoroutine;

        private UserSettings _userSettings;

        private void Awake()
        {
            _collider = GetComponent<BoxCollider2D>();
            _userSettings = ApplicationGraph.GetUserSettings();
        }

        private void OnEnable()
        {
            var gameOrientation = _userSettings.GetGameOrientation();
            SyncUiWithGameOrientation(gameOrientation);
            _userSettings.GameOrientationChanged += UserSettingsOnGameOrientationChanged;
        }

        private void OnDisable()
        {
            _userSettings.GameOrientationChanged -= UserSettingsOnGameOrientationChanged;
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            OnPointerTriggered();
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            OnPointerTriggered();
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            OnPointerUntriggered();
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            OnPointerUntriggered();
        }

        public event Action<int> NoteCellTriggered;

        private void UserSettingsOnGameOrientationChanged(UserSettings.GameOrientation gameOrientation)
        {
            SyncUiWithGameOrientation(gameOrientation);
        }

        private void SyncUiWithGameOrientation(UserSettings.GameOrientation gameOrientation)
        {
            if (gameOrientation == UserSettings.GameOrientation.RightHanded)
                _nameSpriteContainer.localScale = Vector3.one;
            else
                _nameSpriteContainer.localScale = new Vector3(-1f, 1f, 1f);
        }

        public void SetNoteName(Sprite char1, Sprite char2, Sprite char3)
        {
            // Debug.LogError("char1 - char2 - char3 : " + char1 + " - " + char2 + " - " + char3);
            _nameSpriteChar1.sprite = char1;
            _nameSpriteChar2.sprite = char2;
            _nameSpriteChar3.sprite = char3;
            if (char3 == null)
            {
                _nameSpriteChar1.transform.localPosition = new Vector3((0f - _nameSpriteCharSpacing) / 2f, 0f, 0f);
                _nameSpriteChar2.transform.localPosition = new Vector3(_nameSpriteCharSpacing / 2f, 0f, 0f);
                _nameSpriteChar3.enabled = false;
            }
            else
            {
                _nameSpriteChar1.transform.localPosition = new Vector3(0f - _nameSpriteCharSpacing, 0f, 0f);
                _nameSpriteChar2.transform.localPosition = Vector3.zero;
                _nameSpriteChar3.transform.localPosition = new Vector3(_nameSpriteCharSpacing, 0f, 0f);
            }
        }

        public void SetOrder(int order)
        {
            var localPosition = transform.localPosition;
            Debug.LogError("ZOrder : " + order);
            localPosition.z = order;
            transform.localPosition = localPosition;
        }

        public void SetSize(float width, float height)
        {
            _collider.size = new Vector2(width, height);
        }

        public void SetLocalPos(float x, float y)
        {
            var localPosition = transform.localPosition;
            localPosition.x = x;
            localPosition.y = y;
            transform.localPosition = localPosition;
        }

        public void SetTriggeredFeedbackContainerPos(float x, float y)
        {
            var transform = _triggeredFeedbackContainer.transform;
            var localPosition = transform.localPosition;
            localPosition.x = x;
            localPosition.y = y;
            transform.localPosition = localPosition;
        }

        public void SetStringIndex(int stringIndex)
        {
            _stringIndex = stringIndex;
        }

        public void SetMasked(bool masked)
        {
            var component = _triggeredParticles.GetComponent<ParticleSystemRenderer>();


            if (masked)
            {
                // Debug.LogError("SetMasked : "+masked);
                _triggeredSprite.maskInteraction = SpriteMaskInteraction.VisibleInsideMask;
                component.maskInteraction = SpriteMaskInteraction.VisibleInsideMask;
                _nameSpriteChar1.maskInteraction = SpriteMaskInteraction.VisibleInsideMask;
                _nameSpriteChar2.maskInteraction = SpriteMaskInteraction.VisibleInsideMask;
                _nameSpriteChar3.maskInteraction = SpriteMaskInteraction.VisibleInsideMask;
                _nameSpriteBg.maskInteraction = SpriteMaskInteraction.VisibleInsideMask;
            }
            else
            {
                _triggeredSprite.maskInteraction = SpriteMaskInteraction.None;
                component.maskInteraction = SpriteMaskInteraction.None;
                _nameSpriteChar1.maskInteraction = SpriteMaskInteraction.None;
                _nameSpriteChar2.maskInteraction = SpriteMaskInteraction.None;
                _nameSpriteChar3.maskInteraction = SpriteMaskInteraction.None;
                _nameSpriteBg.maskInteraction = SpriteMaskInteraction.None;
            }
        }

        private void OnPointerTriggered()
        {
            if (!_triggered)
            {
                _triggered = true;
                if (_triggeredCoroutine != null) StopCoroutine(_triggeredCoroutine);
                _triggeredCoroutine = StartCoroutine(TriggeredJob());
            }
        }

        private void OnPointerUntriggered()
        {
            _triggered = false;
        }

        private IEnumerator TriggeredJob()
        {
            // Debug.LogError("TriggeredJob : 1 this.NoteCellTriggered == null : "+(NoteCellTriggered == null));

            if (NoteCellTriggered != null) NoteCellTriggered(_stringIndex);

            // Debug.LogError("TriggeredJob : 2 this.NoteCellTriggered == null : "+(NoteCellTriggered == null));

            _triggeredSprite.enabled = true;
            _nameSpriteChar1.enabled = true;
            _nameSpriteChar2.enabled = true;
            _nameSpriteChar3.enabled = true;
            _nameSpriteBg.enabled = true;
            _triggeredParticles.Play();
            //_animator.Rebind();
            //_animator.SetTrigger(HideHash);
            yield return new WaitForSeconds(0.5f);
            _triggeredSprite.enabled = false;
            _nameSpriteChar1.enabled = false;
            _nameSpriteChar2.enabled = false;
            _nameSpriteChar3.enabled = false;
            _nameSpriteBg.enabled = false;
            _triggeredCoroutine = null;
        }
    }
}