using GuitarApplication;
using UnityEngine;

namespace MWM.Guitar.GuitarStyle
{
    public class ScreenAdapter : MonoBehaviour
    {
        [SerializeField] private GameObject _chordsScreen;

        [SerializeField] private GameObject _arpeggiosScreen;

        private UserSettings _userSettings;

        private void OnEnable()
        {
            _userSettings = ApplicationGraph.GetUserSettings();
            _userSettings.GuitarStyleChanged += UserSettingsOnGuitarStyleChanged;
            var guitarStyle = _userSettings.GetGuitarStyle();
            AdaptScreenToCurrent(guitarStyle);
        }

        private void OnDisable()
        {
            _userSettings.GuitarStyleChanged -= UserSettingsOnGuitarStyleChanged;
            if (_chordsScreen != null) _chordsScreen.SetActive(false);
            if (_arpeggiosScreen != null) _arpeggiosScreen.SetActive(false);
        }

        private void UserSettingsOnGuitarStyleChanged(UserSettings.GuitarStyle guitarStyle)
        {
            AdaptScreenToCurrent(guitarStyle);
        }

        private void AdaptScreenToCurrent(UserSettings.GuitarStyle guitarStyle)
        {
            if (guitarStyle == UserSettings.GuitarStyle.Chords)
            {
                _chordsScreen.SetActive(true);
                _arpeggiosScreen.SetActive(false);
            }
            else
            {
                _chordsScreen.SetActive(false);
                _arpeggiosScreen.SetActive(true);
            }
        }
    }
}