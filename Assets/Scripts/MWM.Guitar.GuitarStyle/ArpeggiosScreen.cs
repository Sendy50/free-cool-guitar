using AssetBundles.GuitarSkin;
using GuitarApplication;
using UnityEngine;

namespace MWM.Guitar.GuitarStyle
{
    public class ArpeggiosScreen : MonoBehaviour
    {
        [SerializeField] private GuitarHolder _guitarHolder;

        [SerializeField] private GameObject _backgroundContainer;

        [SerializeField] private SpriteRenderer _backgroundRenderer;

        [SerializeField] private SpriteRenderer _soundboxRenderer;

        [SerializeField] private Transform _neckTransform;

        [SerializeField] private float _neckStartPosX;

        [SerializeField] private float _neckMaxDistanceDefault;

        [SerializeField] private float _neckMaxDistance19By9;

        [SerializeField] private float _neckMaxDistance4By3;

        [SerializeField] private ArpeggiosFretboard[] _fretboards;

        [SerializeField] private GuitarString[] _guitarStrings;

        private ArpeggiosEngine _arpeggiosEngine;

        private UserSettings _userSettings;

        private void OnEnable()
        {
            _userSettings = ApplicationGraph.GetUserSettings();
            var samplingSynthUnit = ApplicationGraph.GetMusicGamingAudioEngine().GetSamplingSynthUnit();
            _arpeggiosEngine = new ArpeggiosEngine(samplingSynthUnit);

            for (var i = 0; i < _fretboards.Length; i++)
            {
                _fretboards[i].enabled = true;
                _fretboards[i].FretboardTriggered += OnFretboardCellTriggered;
            }


            // Debug.LogError("_fretboards size : "+_fretboards.Length);
            _fretboards[0].SetFretIndex(0);
            // Debug.LogError("_fretboards[3]._arpeggiosNoteCells == null : "+ (_fretboards[3]._arpeggiosNoteCells == null));
            // Debug.LogError("_fretboards[5]._arpeggiosNoteCells == null : "+ (_fretboards[5]._arpeggiosNoteCells == null));

            for (var j = 1; j < _fretboards.Length; j++)
                // Debug.LogError("SetFretIndex j : "+j);
                _fretboards[j].SetFretIndex(_fretboards.Length - j);

            _userSettings.GameOrientationChanged += UserSettingsOnGameOrientationChanged;
            var gameOrientation = _userSettings.GetGameOrientation();
            SyncUiWithGameOrientation(gameOrientation);
            _guitarHolder.OnSelectedGuitarChanged += GuitarHolderOnOnSelectedGuitarChanged;
            var selectedGuitar = _guitarHolder.selectedGuitar;
            SyncUiWithGuitar(selectedGuitar);
        }


        private void OnDisable()
        {
            for (var i = 0; i < _fretboards.Length; i++) _fretboards[i].FretboardTriggered -= OnFretboardCellTriggered;
            _userSettings.GameOrientationChanged -= UserSettingsOnGameOrientationChanged;
            _guitarHolder.OnSelectedGuitarChanged -= GuitarHolderOnOnSelectedGuitarChanged;
        }

        public void SetNeckPosition(float position)
        {
            var x = _neckStartPosX - GetNeckMaxDistance() * position;
            var localPosition = _neckTransform.localPosition;
            localPosition.x = x;
            _neckTransform.localPosition = localPosition;
        }

        private float GetNeckMaxDistance()
        {
            var num = Screen.width / (float) Screen.height;
            if (num >= 2f) return _neckMaxDistance19By9;
            if (num <= 1.44444442f) return _neckMaxDistance4By3;
            return _neckMaxDistanceDefault;
        }

        private void UserSettingsOnGameOrientationChanged(UserSettings.GameOrientation gameOrientation)
        {
            SyncUiWithGameOrientation(gameOrientation);
        }

        private void GuitarHolderOnOnSelectedGuitarChanged(IGuitar guitar)
        {
            SyncUiWithGuitar(guitar);
        }

        private void SyncUiWithGuitar(IGuitar guitar)
        {
            _soundboxRenderer.transform.localPosition = guitar.GetArpeggiosSoundboxPosition();
            guitar.GetSkinProxy().GetSkin(delegate(IGuitarSkin skin, bool success)
            {
                _backgroundRenderer.sprite = skin.GetGuitarBackground();
                _soundboxRenderer.sprite = skin.GetGuitarSoundbox();
                var fretboardIndicator = skin.GetFretboardIndicator();
                var arpeggiosStyleSkin = skin.GetArpeggiosStyleSkin();
                _fretboards[0].SetFretboardSprite(arpeggiosStyleSkin.FirstFretboardSprite);
                for (var i = 1; i < _fretboards.Length; i++)
                {
                    var arpeggiosFretboard = _fretboards[i];
                    arpeggiosFretboard.SetFretboardSprite(arpeggiosStyleSkin.RepeatableFretboardSprite);
                    switch (_fretboards.Length - i)
                    {
                        case 12:
                            arpeggiosFretboard.SetFretboardIndicators(fretboardIndicator, fretboardIndicator);
                            break;
                        case 3:
                        case 5:
                        case 7:
                        case 9:
                        case 15:
                        case 17:
                        case 19:
                            arpeggiosFretboard.SetFretboardIndicators(fretboardIndicator, null);
                            break;
                        default:
                            arpeggiosFretboard.SetFretboardIndicators(null, null);
                            break;
                    }
                }

                var strings = skin.GetStrings();
                var stringShadow = skin.GetStringShadow();
                Debug.LogError("strings.Length : " + strings.Length);

                for (var j = 0; j < _guitarStrings.Length; j++)
                {
                    _guitarStrings[j].ChangeStringAsset(strings[j]);
                    _guitarStrings[j].ChangeStringShadowAsset(stringShadow);
                }
            });
        }

        private void SyncUiWithGameOrientation(UserSettings.GameOrientation gameOrientation)
        {
            var rotation = _backgroundContainer.transform.rotation;
            if (gameOrientation == UserSettings.GameOrientation.LeftHanded)
            {
                rotation.eulerAngles = new Vector3(0f, 180f, 0f);
                _fretboards[0].SetZOrder(5);
            }
            else
            {
                rotation.eulerAngles = new Vector3(0f, 0f, 0f);
                _fretboards[0].SetZOrder(-5);
            }

            _backgroundContainer.transform.rotation = rotation;
        }

        private void OnFretboardCellTriggered(int stringIndex, int fretIndex)
        {
            _arpeggiosEngine.Play(stringIndex, fretIndex);
            _guitarStrings[stringIndex].StartOscillation();
        }
    }
}