using UnityEngine;

namespace MWM.Guitar.GuitarStyle
{
    public class ChordsScreen : MonoBehaviour
    {
        [SerializeField] private ChordsStrings _chordsStrings;

        private void OnEnable()
        {
            _chordsStrings.SetupChordProvider(SelectedChordsList.DefaultList());
        }
    }
}