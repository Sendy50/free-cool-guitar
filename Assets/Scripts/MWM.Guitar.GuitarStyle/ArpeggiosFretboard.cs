using System;
using GuitarApplication;
using UnityEngine;

namespace MWM.Guitar.GuitarStyle
{
    [RequireComponent(typeof(SpriteRenderer))]
    public class ArpeggiosFretboard : MonoBehaviour
    {
        private const int NumberOfStrings = 6;

        private const string FretIndicatorSortingLayer = "Backgroud";

        private const int FretIndicatorSortingOrder = 3;

        [SerializeField] private ArpeggiosNoteCell _arpeggiosNoteCellPrefab;

        [SerializeField] private float _cellWidth;

        [SerializeField] private float _cellHeight;

        [SerializeField] private float _cellXPos;

        [SerializeField] private Vector2 _cellFeedbackPos;

        [SerializeField] private bool _cellFeedbackMasked;

        [SerializeField] private Vector2 _soloFretIndicatorPos;

        [SerializeField] private Vector2 _duoFretIndicator1Pos;

        [SerializeField] private Vector2 _duoFretIndicator2Pos;

        private ArpeggiosNoteCell[] _arpeggiosNoteCells;

        private int _fretIndex;

        private SpriteRenderer _fretIndicator1;

        private SpriteRenderer _fretIndicator2;

        private bool _initialised;

        private SpriteRenderer _renderer;

        private void OnEnable()
        {
            if (!_initialised)
            {
                Initialised();
                _initialised = true;
            }
        }

        public event Action<int, int> FretboardTriggered;

        private void Initialised()
        {
            _renderer = GetComponent<SpriteRenderer>();
            _arpeggiosNoteCells = new ArpeggiosNoteCell[6];
            var transform = this.transform;
            var num = _cellHeight * 6f;
            var num2 = (num - _cellHeight) / 2f;

            for (var i = 0; i < 6; i++)
            {
                var arpeggiosNoteCell = Instantiate(_arpeggiosNoteCellPrefab, transform);
                // Debug.LogError(" arpeggiosNoteCell Instantiate : i : " +i+" arpeggiosNoteCell null : "+arpeggiosNoteCell == null);
                var y = num2 - i * _cellHeight;
                arpeggiosNoteCell.SetSize(_cellWidth, _cellHeight);
                arpeggiosNoteCell.SetLocalPos(_cellXPos, y);
                arpeggiosNoteCell.SetStringIndex(i);
                arpeggiosNoteCell.SetTriggeredFeedbackContainerPos(_cellFeedbackPos.x, _cellFeedbackPos.y);
                arpeggiosNoteCell.SetMasked(_cellFeedbackMasked);
                arpeggiosNoteCell.NoteCellTriggered += ArpeggiosNoteCellOnNoteCellTriggered;
                _arpeggiosNoteCells[i] = arpeggiosNoteCell;
            }

            // Debug.LogError(" @_arpeggiosNoteCells : length "+_arpeggiosNoteCells.Length);
            // Debug.LogError(" @_arpeggiosNoteCells : parent "+_arpeggiosNoteCells[0].transform.parent);
        }

        public void SetFretboardSprite(Sprite sprite)
        {
            _renderer.sprite = sprite;
        }

        public void SetFretboardIndicators(Sprite indicator1, Sprite indicator2)
        {
            if (indicator1 == null && _fretIndicator1 != null)
            {
                Destroy(_fretIndicator1.gameObject);
                _fretIndicator1 = null;
            }

            if (indicator1 != null && _fretIndicator1 == null) _fretIndicator1 = InstantiateFretIndicator();
            if (indicator2 == null && _fretIndicator2 != null)
            {
                Destroy(_fretIndicator2.gameObject);
                _fretIndicator2 = null;
            }

            if (indicator2 != null && _fretIndicator2 == null) _fretIndicator2 = InstantiateFretIndicator();
            if (!(_fretIndicator1 == null) || !(_fretIndicator2 == null))
            {
                if (_fretIndicator2 == null)
                {
                    _fretIndicator1.sprite = indicator1;
                    _fretIndicator1.gameObject.transform.localPosition = _soloFretIndicatorPos;
                    return;
                }

                _fretIndicator1.sprite = indicator1;
                _fretIndicator2.sprite = indicator2;
                _fretIndicator1.gameObject.transform.localPosition = _duoFretIndicator1Pos;
                _fretIndicator2.gameObject.transform.localPosition = _duoFretIndicator2Pos;
            }
        }


        private SpriteRenderer InstantiateFretIndicator()
        {
            var gameObject = new GameObject();
            gameObject.transform.parent = this.gameObject.transform;
            var spriteRenderer = gameObject.AddComponent<SpriteRenderer>();
            spriteRenderer.sortingLayerName = "Default";
            spriteRenderer.sortingOrder = 2;
            spriteRenderer.maskInteraction = SpriteMaskInteraction.VisibleInsideMask;
            return spriteRenderer;
        }


        public void SetFretIndex(int index)
        {
            _fretIndex = index;
            Debug.LogError("SetFretIndex : " + index);

            for (var i = 0; i < 6; i++)
            {
                var noteNumber = GetNoteNumber(i, _fretIndex);
                // Debug.LogError("1 noteNumber : " +  noteNumber);
                var nameChars = GetNameChars(noteNumber);
                // Debug.LogError("2 nameChars : " +  nameChars[0].name);
                // Debug.LogError("2 nameChars : " +  nameChars[1].name);
                // Debug.LogError("2 nameChars : " +  nameChars[2].name);
                // Debug.LogError("3 _arpeggiosNoteCells[i] lenth : " + _arpeggiosNoteCells.Length);
                // Debug.LogError("4 _arpeggiosNoteCells[i] null : " + (_arpeggiosNoteCells[i] == null));
                _arpeggiosNoteCells[i].SetNoteName(nameChars[0], nameChars[1], nameChars[2]);
                // Debug.LogError("5 nameChars : " +  nameChars[1].name);
            }
        }


        public void SetZOrder(int order)
        {
            for (var i = 0; i < 6; i++) _arpeggiosNoteCells[i].SetOrder(order);
        }

        private void ArpeggiosNoteCellOnNoteCellTriggered(int stringIndex)
        {
            if (FretboardTriggered != null) FretboardTriggered(stringIndex, _fretIndex);
        }

        private static int GetNoteNumber(int stringIndex, int fretIndex)
        {
            var num = ChordMap.emptyChordMidiNumbers[stringIndex];
            return num + fretIndex;
        }

        private Sprite[] GetNameChars(int midiNote)
        {
            var chars = GetChars(midiNote);
            var charSprites = ApplicationGraph.GetCharSprites();
            charSprites.TryGetValue(chars[0], out var value);
            charSprites.TryGetValue(chars[1], out var value2);
            charSprites.TryGetValue(chars[2], out var value3);
            return new Sprite[3]
            {
                value,
                value2,
                value3
            };
        }

        private char[] GetChars(int midiNote)
        {
            var num = midiNote - 36;
            var num2 = num % 12;
            var num3 = 2 + num / 12;
            var array = new char[3];
            int num4;
            switch (num2)
            {
                case 0:
                    array[0] = 'c';
                    num4 = 1;
                    break;
                case 1:
                    array[0] = 'c';
                    array[1] = '#';
                    num4 = 2;
                    break;
                case 2:
                    array[0] = 'd';
                    num4 = 1;
                    break;
                case 3:
                    array[0] = 'd';
                    array[1] = '#';
                    num4 = 2;
                    break;
                case 4:
                    array[0] = 'e';
                    num4 = 1;
                    break;
                case 5:
                    array[0] = 'f';
                    num4 = 1;
                    break;
                case 6:
                    array[0] = 'f';
                    array[1] = '#';
                    num4 = 2;
                    break;
                case 7:
                    array[0] = 'g';
                    num4 = 1;
                    break;
                case 8:
                    array[0] = 'g';
                    array[1] = '#';
                    num4 = 2;
                    break;
                case 9:
                    array[0] = 'a';
                    num4 = 1;
                    break;
                case 10:
                    array[0] = 'a';
                    array[1] = '#';
                    num4 = 2;
                    break;
                case 11:
                    array[0] = 'b';
                    num4 = 1;
                    break;
                default:
                    throw new ArgumentOutOfRangeException("unsupported semitone: " + midiNote);
            }

            array[num4] = (char) (48 + num3);
            return array;
        }
    }
}