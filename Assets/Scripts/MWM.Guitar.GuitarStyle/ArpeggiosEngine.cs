using System;
using MWM.Audio;

namespace MWM.Guitar.GuitarStyle
{
    public class ArpeggiosEngine
    {
        private readonly int[] _lastNotePerStrings;
        private readonly SamplingSynthUnit _samplingSynth;

        public ArpeggiosEngine(SamplingSynthUnit samplingSynth)
        {
            _samplingSynth = samplingSynth;
            _lastNotePerStrings = new int[6]
            {
                -1,
                -1,
                -1,
                -1,
                -1,
                -1
            };
        }

        public void Play(int stringIndex, int fretIndex)
        {
            if (stringIndex < 0 || stringIndex > 5) throw new ArgumentException("String index must be in range [0,5]");
            if (fretIndex < 0) throw new ArgumentException("Fret index must be positive");
            var num = _lastNotePerStrings[stringIndex];
            if (num != -1) _samplingSynth.NoteOff(0, num);
            var noteNumber = GetNoteNumber(stringIndex, fretIndex);
            _lastNotePerStrings[stringIndex] = noteNumber;
            _samplingSynth.NoteOn(0, noteNumber, 63);
        }

        private static int GetNoteNumber(int stringIndex, int fretIndex)
        {
            var num = ChordMap.emptyChordMidiNumbers[stringIndex];
            return num + fretIndex;
        }
    }
}