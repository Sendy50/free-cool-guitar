using MWM.Audio;
using Utils;

public class GuitarPreviewPlayer : IGuitarPreviewPlayer
{
    private readonly Player _player;

    public GuitarPreviewPlayer(Player player)
    {
        Precondition.CheckNotNull(player);
        _player = player;
    }

    public void PlayPreviewForGuitar(IGuitar guitar)
    {
        _player.LoadInternal("AllMusic/" + guitar.PreviewName, delegate(LoadingResult result)
        {
            if (result.IsSuccess()) _player.Play();
        });
    }

    public void StopPreview()
    {
        _player.Stop();
    }
}