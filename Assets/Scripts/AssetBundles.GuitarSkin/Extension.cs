using System;

namespace AssetBundles.GuitarSkin
{
    public static class Extension
    {
        public static string BundleName(this SkinReference skinRef)
        {
            switch (skinRef)
            {
                case SkinReference.folk:
                    return "skin-folk";
                case SkinReference.classic:
                    return "skin-classic";
                case SkinReference.rock:
                    return "skin-rock";
                case SkinReference.jazz:
                    return "skin-jazz";
                case SkinReference.twelveStrings:
                    return "skin-12-strings";
                default:
                    throw new Exception("SkinReference unknown");
            }
        }

        public static string SlicingBundleName(this SkinReference skinRef)
        {
            switch (skinRef)
            {
                case SkinReference.folk:
                    return "skin-folk-slicing";
                case SkinReference.classic:
                    return "skin-classic-slicing";
                case SkinReference.rock:
                    return "skin-rock-slicing";
                case SkinReference.jazz:
                    return "skin-jazz-slicing";
                case SkinReference.twelveStrings:
                    return "skin-12-strings-slicing";
                default:
                    throw new Exception("SkinReference unknown");
            }
        }

        public static string AppleODRTag(this SkinReference skinRef)
        {
            return skinRef.BundleName();
        }

        public static string SlicingAppleODRTag(this SkinReference skinRef)
        {
            return skinRef.SlicingBundleName();
        }

        public static bool IsAppleODR(this SkinReference skinRef)
        {
            return false;
        }
    }
}