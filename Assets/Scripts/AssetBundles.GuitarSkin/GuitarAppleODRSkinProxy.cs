using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utils;

namespace AssetBundles.GuitarSkin
{
    public class GuitarAppleODRSkinProxy : IGuitarSkinProxy
    {
        private string _appleODRTag;

        private string _bundleName;
        private readonly IGuitar _guitar;

        private readonly MonoBehaviour _monoBehaviour;

        private readonly List<Action<IGuitarSkin, bool>> _pendingCallback;

        private IGuitarSkin _skin;

        private string _slicingAppleODRTag;

        private string _slicingBundleName;

        public GuitarAppleODRSkinProxy(IGuitar guitar, string bundleName, string appleODRTag, string slicingBundleName,
            string slicingAppleODRTag, MonoBehaviour monoBehaviour)
        {
            Precondition.CheckNotNull(guitar);
            Precondition.CheckNotNull(bundleName);
            Precondition.CheckNotNull(appleODRTag);
            Precondition.CheckNotNull(slicingBundleName);
            Precondition.CheckNotNull(slicingAppleODRTag);
            Precondition.CheckNotNull(monoBehaviour);
            _guitar = guitar;
            _bundleName = bundleName;
            _appleODRTag = appleODRTag;
            _monoBehaviour = monoBehaviour;
            _slicingBundleName = slicingBundleName;
            _slicingAppleODRTag = slicingAppleODRTag;
            _pendingCallback = new List<Action<IGuitarSkin, bool>>();
        }

        public void GetSkin(Action<IGuitarSkin, bool> callback)
        {
            GetSkin(_guitar, callback);
        }

        private void GetSkin(IGuitar guitar, Action<IGuitarSkin, bool> callback)
        {
            if (_skin != null)
            {
                callback(_skin, true);
                return;
            }

            _pendingCallback.Add(callback);
            if (_pendingCallback.Count == 1) _monoBehaviour.StartCoroutine(LoadSkin(guitar));
        }

        private IEnumerator LoadSkin(IGuitar guitar)
        {
            yield break;
        }
    }
}