using System;

namespace AssetBundles.GuitarSkin
{
    public interface IGuitarSkinProxy
    {
        void GetSkin(Action<IGuitarSkin, bool> callback);
    }
}