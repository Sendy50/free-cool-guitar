using System.Collections;
using UnityEngine;

namespace AssetBundles.GuitarSkin
{
    public class GuitarSkinLocalSlicingAssetLoader : GuitarSkinLocalAssetLoader
    {
        public GuitarSkinLocalSlicingAssetLoader(string ressourcePath, GuitarSkin skinToFill, IGuitar guitar)
            : base(ressourcePath, skinToFill, guitar)
        {
            _guitar = guitar;
        }

        public override IEnumerator LoadSkin()
        {
            // yield return LoadBundle();
            skin.guitarBackground = _guitar.GuitarBgImage;
            // yield return AssetLoader<Sprite>.Load(_assetBundle, "GUITAR-BACKGROUND",
            //     delegate(Sprite r) { skin.guitarBackground = r; });
            // _assetBundle.Unload(false);
            // _assetBundle = null;
            yield return null;
        }

        protected override IEnumerator LoadBundle()
        {
            var request = AssetBundle.LoadFromFileAsync(ressourcePath);
            yield return request;
            _assetBundle = request.assetBundle;
        }
    }
}