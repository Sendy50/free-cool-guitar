using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using Utils;

namespace AssetBundles.GuitarSkin
{
    public class GuitarLocalSkinProxy : IGuitarSkinProxy
    {
        private readonly string _bundleName;
        private readonly IGuitar _guitar;

        private readonly MonoBehaviour _monoBehaviour;

        private readonly List<Action<IGuitarSkin, bool>> _pendingCallback;

        private IGuitarSkin _skin;

        private readonly string _slicingBundleName;

        public GuitarLocalSkinProxy(IGuitar guitar, string bundleName, string slicingBundleName,
            MonoBehaviour monoBehaviour)
        {
            Precondition.CheckNotNull(guitar);
            Precondition.CheckNotNull(bundleName);
            Precondition.CheckNotNull(slicingBundleName);
            Precondition.CheckNotNull(monoBehaviour);
            _guitar = guitar;
            _bundleName = bundleName;
            _slicingBundleName = slicingBundleName;
            _monoBehaviour = monoBehaviour;
            _pendingCallback = new List<Action<IGuitarSkin, bool>>();
        }

        public void GetSkin(Action<IGuitarSkin, bool> callback)
        {
            GetSkin(_guitar, callback);
        }

        private void GetSkin(IGuitar guitar, Action<IGuitarSkin, bool> callback)
        {
            if (_skin != null)
            {
                callback(_skin, true);
                return;
            }

            _pendingCallback.Add(callback);
            if (_pendingCallback.Count == 1) _monoBehaviour.StartCoroutine(LoadSkin(guitar));
        }

        private IEnumerator LoadSkin(IGuitar guitar)
        {
            Debug.LogError("LoadGuitar New Skin ...");
            Debug.LogError("AssetBundle Path ... : " + AssetBundles.Constantes.AssetBundlesPath);
            Debug.LogError("Bundle Name Path ... : " + _bundleName);
            Debug.LogError("All Path ... : " + Path.Combine(AssetBundles.Constantes.AssetBundlesPath, _bundleName));
            Debug.LogError("new GuitarSkin() : " + (new GuitarSkin() == null));
            var skin = new GuitarSkin();
            
            var assetLoader =
                new GuitarSkinLocalAssetLoader(Path.Combine(AssetBundles.Constantes.AssetBundlesPath, _bundleName),
                    skin, guitar);
            
            var slicingAssetLoader =
                new GuitarSkinLocalSlicingAssetLoader(
                    Path.Combine(AssetBundles.Constantes.AssetBundlesPath, _slicingBundleName), skin, guitar);
            
            var taskNormal = assetLoader.LoadSkin();
            var taskSlicing = slicingAssetLoader.LoadSkin();
            yield return taskNormal;
            yield return taskSlicing;
            _skin = skin;
            var observers = _pendingCallback.ToArray();
            var array = observers;
            foreach (var action in array) action(skin, true);
            _pendingCallback.Clear();
        }
    }
}