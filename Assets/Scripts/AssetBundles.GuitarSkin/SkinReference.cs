using System;

namespace AssetBundles.GuitarSkin
{
    [Serializable]
    public enum SkinReference
    {
        folk,
        classic,
        rock,
        jazz,
        twelveStrings
    }
}