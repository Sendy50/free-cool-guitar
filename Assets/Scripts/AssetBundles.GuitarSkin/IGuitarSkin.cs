using UnityEngine;

namespace AssetBundles.GuitarSkin
{
    public interface IGuitarSkin
    {
        Sprite GetChordButtonOn();

        Sprite GetChordButtonOff();

        Sprite GetInGameComboBackground();

        Sprite GetInGameScoreBackground();

        Sprite GetInGamePauseButton();

        Sprite[] GetFretboards();

        Sprite GetFretboardIndicator();

        Sprite[] GetStrings();

        Sprite GetStringShadow();

        Sprite GetFreeModeSettingsButton();

        Sprite GetFreeModeSettingsButtonHighlighted();

        Sprite GetGuitarMenuButton();

        Sprite GetGuitarMenuButtonHighlighted();

        Sprite GetGuitarBackground();

        Sprite GetGuitarSoundbox();

        ArpeggiosStyleSkin GetArpeggiosStyleSkin();
    }
}