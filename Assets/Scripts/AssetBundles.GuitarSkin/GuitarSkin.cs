using UnityEngine;

namespace AssetBundles.GuitarSkin
{
    public class GuitarSkin : IGuitarSkin
    {
        public ArpeggiosStyleSkin ArpeggiosStyleSkin;

        public Sprite chordButtonOff;
        public Sprite chordButtonOn;

        public Sprite freeModeSettingsButton;

        public Sprite freeModeSettingsButtonHighlighted;

        public Sprite fretboardIndicator;

        public Sprite[] fretboards;

        public Sprite guitarBackground;

        public Sprite guitarMenuButton;

        public Sprite guitarMenuButtonHighlighted;

        public Sprite guitarSoundbox;

        public Sprite inGameComboBackground;

        public Sprite inGamePauseButton;

        public Sprite inGameScoreBackground;

        public Sprite[] strings;

        public Sprite stringShadow;

        public Sprite GetChordButtonOn()
        {
            return chordButtonOn;
        }

        public Sprite GetChordButtonOff()
        {
            return chordButtonOff;
        }

        public Sprite GetInGameComboBackground()
        {
            return inGameComboBackground;
        }

        public Sprite GetInGameScoreBackground()
        {
            return inGameScoreBackground;
        }

        public Sprite GetInGamePauseButton()
        {
            return inGamePauseButton;
        }

        public Sprite[] GetFretboards()
        {
            return fretboards;
        }

        public Sprite GetFretboardIndicator()
        {
            return fretboardIndicator;
        }

        public Sprite[] GetStrings()
        {
            return strings;
        }

        public Sprite GetStringShadow()
        {
            return stringShadow;
        }

        public Sprite GetFreeModeSettingsButton()
        {
            return freeModeSettingsButton;
        }

        public Sprite GetFreeModeSettingsButtonHighlighted()
        {
            return freeModeSettingsButtonHighlighted;
        }

        public Sprite GetGuitarMenuButton()
        {
            return guitarMenuButton;
        }

        public Sprite GetGuitarMenuButtonHighlighted()
        {
            return guitarMenuButtonHighlighted;
        }

        public Sprite GetGuitarBackground()
        {
            return guitarBackground;
        }

        public Sprite GetGuitarSoundbox()
        {
            //Debug.LogError("Soundbox Name : " + guitarSoundbox.name);
            return guitarSoundbox;
        }

        public ArpeggiosStyleSkin GetArpeggiosStyleSkin()
        {
            return ArpeggiosStyleSkin;
        }
    }
}