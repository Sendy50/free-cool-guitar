using System.Collections;
using UnityEngine;
using Utils;

namespace AssetBundles.GuitarSkin
{
    public class GuitarSkinLocalAssetLoader : IGuitarSkinAssetLoader
    {
        protected AssetBundle _assetBundle;
        protected IGuitar _guitar;

        public GuitarSkinLocalAssetLoader(string ressourcePath, GuitarSkin skinToFill, IGuitar guitar)
        {
            Debug.LogError("ressourcePath : "+ressourcePath);
            // Debug.LogError("skinToFill.guitarBackground.name : "+skinToFill.guitarBackground.name);
            Precondition.CheckNotNull(ressourcePath);
            Precondition.CheckNotNull(skinToFill);
            Precondition.CheckNotNull(guitar);
            this.ressourcePath = ressourcePath;
            skin = skinToFill;
            _guitar = guitar;
        }

        public string ressourcePath { get; }

        public GuitarSkin skin { get; }

        public IGuitarSkin GetLoadedSkin()
        {
            return skin;
        }

        public virtual IEnumerator LoadSkin()
        {
            
            yield return _guitar.loadSprite();
            // skin.chordButtonOn = _guitar.getAtlas("CHORD_BTN_ON");
            // skin.chordButtonOff = _guitar.getAtlas("CHORD_BTN_OFF");
            // skin.inGameComboBackground = _guitar.getAtlas("INGAME_COMBO_BG");
            // skin.inGameScoreBackground = _guitar.getAtlas("INGAME_SCORE_BG");
            // skin.inGamePauseButton = _guitar.getAtlas("INGAME_PAUSE_BTN");
            // skin.inGamePauseButton = _guitar.getAtlas("INGAME_PAUSE_BTN");
            Sprite arpeggiosStyleSkinFirstFretboard = null;
            Sprite arpeggiosStyleSkinRepeatableFretboard = null;
            var fretboards = new Sprite[7];

            arpeggiosStyleSkinFirstFretboard = fretboards[0] = _guitar.getAtlas("FRETBOARD_1");

            arpeggiosStyleSkinRepeatableFretboard = fretboards[1] =
                fretboards[2] = fretboards[3] = fretboards[4] = fretboards[5] = fretboards[6] = _guitar.getAtlas("FRETBOARD_2");

            var strings = new Sprite[6];
            strings[0] = _guitar.getAtlas("STRING_1");
            strings[1] = _guitar.getAtlas("STRING_2");
            strings[2] = _guitar.getAtlas("STRING_3");
            strings[3] = _guitar.getAtlas("STRING_4");
            strings[4] = _guitar.getAtlas("STRING_5");
            strings[5] = _guitar.getAtlas("STRING_6");
            
            skin.stringShadow = _guitar.getAtlas("SHADOW");
            // skin.guitarMenuButton = _guitar.getAtlas("GUITAR_MENU_BUTTON");
            // skin.guitarMenuButtonHighlighted = _guitar.getAtlas("GUITAR_MENU_BUTTON_HIGHLIGHTED");
            // skin.freeModeSettingsButton = _guitar.getAtlas("FREE_MENU_SETTINGS_BTN");
            // skin.freeModeSettingsButtonHighlighted = _guitar.getAtlas("FREE_MENU_SETTINGS_BTN_HIGHLIGHTED");
            skin.guitarSoundbox = _guitar.getAtlas("GUITAR-SOUNDBOX");
            // skin.fretboardIndicator = _guitar.getAtlas("GUITAR-FRETBOARD_INDICATOR");
            skin.fretboardIndicator = _guitar.getAtlas("FRETBOARD_INDICATOR");


            // var task1 = AssetLoader<Sprite>.Load(_assetBundle, "CHORD_BTN_ON",
            //     delegate(Sprite r) { skin.chordButtonOn = r; });
            //
            //
            // var task12 = AssetLoader<Sprite>.Load(_assetBundle, "CHORD_BTN_OFF",
            //     delegate(Sprite r) { skin.chordButtonOff = r; });
            // var task16 = AssetLoader<Sprite>.Load(_assetBundle, "INGAME_COMBO_BG",
            //     delegate(Sprite r) { skin.inGameComboBackground = r; });
            // var task17 = AssetLoader<Sprite>.Load(_assetBundle, "INGAME_SCORE_BG",
            //     delegate(Sprite r) { skin.inGameScoreBackground = r; });
            // var task18 = AssetLoader<Sprite>.Load(_assetBundle, "INGAME_PAUSE_BTN",
            //     delegate(Sprite r) { skin.inGamePauseButton = r; });
            // Sprite arpeggiosStyleSkinFirstFretboard = null;
            // Sprite arpeggiosStyleSkinRepeatableFretboard = null;
            // var fretboards = new Sprite[7];
            // var task19 = AssetLoader<Sprite>.Load(_assetBundle, "FRETBOARD_1",
            //     delegate(Sprite r) { arpeggiosStyleSkinFirstFretboard = fretboards[0] = r; });
            // var task20 = AssetLoader<Sprite>.Load(_assetBundle, "FRETBOARD_2",
            //     delegate(Sprite r)
            //     {
            //         arpeggiosStyleSkinRepeatableFretboard = fretboards[1] =
            //             fretboards[2] = fretboards[3] = fretboards[4] = fretboards[5] = fretboards[6] = r;
            //     });
            // // var strings = new Sprite[6];
            // var task2 = AssetLoader<Sprite>.Load(_assetBundle, "STRING_1", delegate(Sprite r) { strings[0] = r; });
            // var task3 = AssetLoader<Sprite>.Load(_assetBundle, "STRING_2", delegate(Sprite r) { strings[1] = r; });
            // var task4 = AssetLoader<Sprite>.Load(_assetBundle, "STRING_3", delegate(Sprite r) { strings[2] = r; });
            // var task5 = AssetLoader<Sprite>.Load(_assetBundle, "STRING_4", delegate(Sprite r) { strings[3] = r; });
            // var task6 = AssetLoader<Sprite>.Load(_assetBundle, "STRING_5", delegate(Sprite r) { strings[4] = r; });
            // var task7 = AssetLoader<Sprite>.Load(_assetBundle, "STRING_6", delegate(Sprite r) { strings[5] = r; });
            // var task15 =
            //     AssetLoader<Sprite>.Load(_assetBundle, "SHADOW", delegate(Sprite r) { skin.stringShadow = r; });
            // var task8 = AssetLoader<Sprite>.Load(_assetBundle, "GUITAR_MENU_BUTTON",
            //     delegate(Sprite r) { skin.guitarMenuButton = r; });
            // var task9 = AssetLoader<Sprite>.Load(_assetBundle, "GUITAR_MENU_BUTTON_HIGHLIGHTED",
            //     delegate(Sprite r) { skin.guitarMenuButtonHighlighted = r; });
            // var task10 = AssetLoader<Sprite>.Load(_assetBundle, "FREE_MENU_SETTINGS_BTN",
            //     delegate(Sprite r) { skin.freeModeSettingsButton = r; });
            // var task11 = AssetLoader<Sprite>.Load(_assetBundle, "FREE_MENU_SETTINGS_BTN_HIGHLIGHTED",
            //     delegate(Sprite r) { skin.freeModeSettingsButtonHighlighted = r; });
            // var task13 = AssetLoader<Sprite>.Load(_assetBundle, "GUITAR-SOUNDBOX",
            //     delegate(Sprite r) { skin.guitarSoundbox = r; });
            // var task14 = AssetLoader<Sprite>.Load(_assetBundle, "FRETBOARD_INDICATOR",
            //     delegate(Sprite r) { skin.fretboardIndicator = r; });
            // yield return task1;
            // yield return task12;
            // yield return task16;
            // yield return task17;
            // yield return task18;
            // yield return task19;
            // yield return task20;
            // yield return task2;
            // yield return task3;
            // yield return task4;
            // yield return task5;
            // yield return task6;
            // yield return task7;
            // yield return task15;
            // yield return task8;
            // yield return task9;
            // yield return task10;
            // yield return task11;
            // yield return task13;
            // yield return task14;
            skin.fretboards = fretboards;
            skin.strings = strings;
            skin.ArpeggiosStyleSkin =
                new ArpeggiosStyleSkin(arpeggiosStyleSkinFirstFretboard, arpeggiosStyleSkinRepeatableFretboard);
            // _assetBundle.Unload(false);
            // _assetBundle = null;

            yield return null;
        }

        protected virtual IEnumerator LoadBundle()
        {
            Debug.LogError("LoadBundle : "+ressourcePath);
            var request = AssetBundle.LoadFromFileAsync(ressourcePath);
            yield return request;
            Debug.LogError(" request.assetBundle : "+request.assetBundle.name);
            _assetBundle = request.assetBundle;
            
            if(_assetBundle != null)
                Debug.LogError("_assetBundle not null: "+_assetBundle.name);
            else
            {
                Debug.LogError("_assetBundle : null "+_assetBundle.name);    
            }
            
        }
    }
}