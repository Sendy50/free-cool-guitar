using System.Runtime.InteropServices;

namespace AssetBundles.GuitarSkin
{
    [StructLayout(LayoutKind.Sequential, Size = 1)]
    public struct Constantes
    {
        [StructLayout(LayoutKind.Sequential, Size = 1)]
        public struct SkinAssetNames
        {
            public const string ChordButtonOn = "CHORD_BTN_ON";

            public const string ChordButtonOff = "CHORD_BTN_OFF";

            public const string InGameComboBackground = "INGAME_COMBO_BG";

            public const string InGameScoreBackground = "INGAME_SCORE_BG";

            public const string InGamePauseButton = "INGAME_PAUSE_BTN";

            public const string Fretboard1 = "FRETBOARD_1";

            public const string Fretboard2 = "FRETBOARD_2";

            public const string FretboardIndicator = "FRETBOARD_INDICATOR";

            public const string String1 = "STRING_1";

            public const string String2 = "STRING_2";

            public const string String3 = "STRING_3";

            public const string String4 = "STRING_4";

            public const string String5 = "STRING_5";

            public const string String6 = "STRING_6";

            public const string StringShadow = "SHADOW";

            public const string GuitarMenuButton = "GUITAR_MENU_BUTTON";

            public const string GuitarMenuButtonHighlighted = "GUITAR_MENU_BUTTON_HIGHLIGHTED";

            public const string FreeSettingsMenuButton = "FREE_MENU_SETTINGS_BTN";

            public const string FreeSettingsMenuButtonHighlighted = "FREE_MENU_SETTINGS_BTN_HIGHLIGHTED";

            public const string GuitarBackground = "GUITAR-BACKGROUND";

            public const string GuitarSoundbox = "GUITAR-SOUNDBOX";
        }
    }
}