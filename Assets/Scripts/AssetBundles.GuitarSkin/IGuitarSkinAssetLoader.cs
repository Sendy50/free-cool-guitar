using System.Collections;

namespace AssetBundles.GuitarSkin
{
    public interface IGuitarSkinAssetLoader
    {
        IGuitarSkin GetLoadedSkin();

        IEnumerator LoadSkin();
    }
}