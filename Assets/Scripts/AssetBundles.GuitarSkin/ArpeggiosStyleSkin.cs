using UnityEngine;

namespace AssetBundles.GuitarSkin
{
    public class ArpeggiosStyleSkin
    {
        public readonly Sprite FirstFretboardSprite;

        public readonly Sprite RepeatableFretboardSprite;

        public ArpeggiosStyleSkin(Sprite firstFretboardSprite, Sprite repeatableFretboardSprite)
        {
            FirstFretboardSprite = firstFretboardSprite;
            RepeatableFretboardSprite = repeatableFretboardSprite;
        }
    }
}