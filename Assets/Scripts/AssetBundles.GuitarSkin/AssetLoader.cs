using System;
using System.Collections;
using System.Runtime.InteropServices;
using UnityEngine;
using Object = UnityEngine.Object;

namespace AssetBundles.GuitarSkin
{
    [StructLayout(LayoutKind.Sequential, Size = 1)]
    public struct AssetLoader<T> where T : Object
    {
        public static IEnumerator Load(AssetBundle assetBundle, string assetName, Action<T> affectation)
        {
            
            var request = assetBundle.LoadAssetAsync<T>(assetName);
            yield return request;
            affectation(request.asset as T);
        }
    }
}