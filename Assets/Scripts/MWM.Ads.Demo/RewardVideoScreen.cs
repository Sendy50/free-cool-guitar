using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.UI;

namespace MWM.Ads.Demo
{
    [RequireComponent(typeof(Canvas))]
    public class RewardVideoScreen : MonoBehaviour
    {
        public Text InternalStatus;

        public MWMMoPubWrapper InternalMoPubWrapper;

        public Dropdown InternalDropdown;

        private Canvas _canvas;

        private void Awake()
        {
            _canvas = GetComponent<Canvas>();
        }

        public void Init()
        {
            InternalMoPubWrapper.coroutineLauncher = this;
        }

        public void Show()
        {
            Init();
            _canvas.enabled = true;
        }

        private void Hide()
        {
            _canvas.enabled = false;
        }

        [UsedImplicitly]
        public void OnShowRewardVideoClicked()
        {
            var placementName = GetCurrentSelectedPlacementName();
            InternalMoPubWrapper.LoadContent(PlacementType.RewardVideo, placementName,
                delegate(PlacementLoadingState result)
                {
                    switch (result)
                    {
                        case PlacementLoadingState.Loaded:
                            ReportStatus("LoadContent: SUCCESS");
                            InternalMoPubWrapper.DisplayContent(PlacementType.RewardVideo, placementName,
                                delegate(PlacementShowHideResult displayResult)
                                {
                                    switch (displayResult)
                                    {
                                        case PlacementShowHideResult.ShowHideSucceeded:
                                            ReportStatus("DisplayContent: SUCCESS");
                                            break;
                                        case PlacementShowHideResult.ShowHideFailed:
                                            ReportStatus("DisplayContent: FAILURE");
                                            break;
                                    }
                                }, delegate(PlacementInteractionState interactionResult)
                                {
                                    ReportStatus("DisplayContent: Video did close");
                                    if (interactionResult == PlacementInteractionState.Rewarded)
                                        ReportStatus("Video did reward");
                                });
                            break;
                        case PlacementLoadingState.Failure:
                            ReportStatus("LoadContent: FAILURE");
                            break;
                        case PlacementLoadingState.Loading:
                            ReportStatus("LoadContent: ALREADY LOADING");
                            break;
                    }
                });
            ReportStatus("Going to display reward video " + placementName);
        }

        [UsedImplicitly]
        public void OnBackClicked()
        {
            Hide();
        }

        private void ReportStatus(string status)
        {
            Debug.Log("RewardVideoScreen -> status: " + status);
            InternalStatus.text = status;
        }

        private string GetCurrentSelectedPlacementName()
        {
            return InternalDropdown.captionText.text;
        }
    }
}