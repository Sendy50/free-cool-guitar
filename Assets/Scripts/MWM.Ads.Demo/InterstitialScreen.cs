using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.UI;

namespace MWM.Ads.Demo
{
    [RequireComponent(typeof(Canvas))]
    public class InterstitialScreen : MonoBehaviour
    {
        public Text InternalStatus;

        public MWMMoPubWrapper InternalMoPubWrapper;

        public Dropdown InternalDropdown;

        private Canvas _canvas;

        private void Awake()
        {
            _canvas = GetComponent<Canvas>();
        }

        public void Show()
        {
            _canvas.enabled = true;
        }

        private void Hide()
        {
            _canvas.enabled = false;
        }

        [UsedImplicitly]
        public void OnLoadInterstitialClicked()
        {
            var currentSelectedPlacementName = GetCurrentSelectedPlacementName();
            ReportStatus("Preparing interstitial ad for placement name: " + currentSelectedPlacementName);
            InternalMoPubWrapper.LoadContent(PlacementType.Interstitial, currentSelectedPlacementName,
                delegate(PlacementLoadingState result)
                {
                    switch (result)
                    {
                        case PlacementLoadingState.Loaded:
                            ReportStatus("Load interstitial: SUCCESS");
                            break;
                        case PlacementLoadingState.Failure:
                            ReportStatus("Load interstitial: Failure");
                            break;
                        case PlacementLoadingState.Loading:
                            ReportStatus("Load interstitial: Already loading");
                            break;
                    }
                });
        }

        [UsedImplicitly]
        public void OnShowInterstitialClicked()
        {
            var placementName = GetCurrentSelectedPlacementName();
            InternalMoPubWrapper.DisplayContent(PlacementType.Interstitial, placementName,
                delegate(PlacementShowHideResult displayResult)
                {
                    switch (displayResult)
                    {
                        case PlacementShowHideResult.ShowHideFailed:
                            ReportStatus("Can't show interstitial. Not ready to display.");
                            break;
                        case PlacementShowHideResult.ShowHideSucceeded:
                            ReportStatus("Ready to display. Displaying now.");
                            break;
                    }
                }, delegate { ReportStatus("Interstitial \"" + placementName + "\" was closed."); });
        }

        [UsedImplicitly]
        public void OnBackClicked()
        {
            Hide();
        }

        private void ReportStatus(string status)
        {
            Debug.Log("InterstitialScreen -> status: " + status);
            InternalStatus.text = status;
        }

        private string GetCurrentSelectedPlacementName()
        {
            return InternalDropdown.captionText.text;
        }
    }
}