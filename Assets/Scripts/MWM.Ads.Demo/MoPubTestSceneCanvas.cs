using JetBrains.Annotations;
using MWM.Core.Demo;
using UnityEngine;

namespace MWM.Ads.Demo
{
    public class MoPubTestSceneCanvas : MonoBehaviour
    {
        public InterstitialScreen InternalInterstitialScreen;

        public BannerScreen InternalBannerScreen;

        public RewardVideoScreen InternalRewardVideoScreen;

        public ConsentGDPRCanvas InternalConsentGDPRScreen;

        [UsedImplicitly]
        public void OnInterstitialButtonClicked()
        {
            InternalInterstitialScreen.Show();
        }

        [UsedImplicitly]
        public void OnBannerButtonClicked()
        {
            InternalBannerScreen.Show();
        }

        [UsedImplicitly]
        public void OnRewardVideoButtonClicked()
        {
            InternalRewardVideoScreen.Show();
        }

        [UsedImplicitly]
        public void OnBackButtonClicked()
        {
            Router.GoToMainDemoScreen();
        }

        [UsedImplicitly]
        public void OnConsentGDPRClicked()
        {
            InternalConsentGDPRScreen.Show();
        }
    }
}