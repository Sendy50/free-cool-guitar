using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.UI;

namespace MWM.Ads.Demo
{
    [RequireComponent(typeof(Canvas))]
    public class BannerScreen : MonoBehaviour
    {
        public Text InternalStatus;

        public MWMMoPubWrapper InternalMoPubWrapper;

        public Dropdown InternalDropdown;

        private Canvas _canvas;

        private void Awake()
        {
            _canvas = GetComponent<Canvas>();
        }

        public void Show()
        {
            _canvas.enabled = true;
        }

        private void Hide()
        {
            _canvas.enabled = false;
        }

        [UsedImplicitly]
        public void OnLoadBannerClicked()
        {
            var currentSelectedPlacementName = GetCurrentSelectedPlacementName();
            ReportStatus("Preparing banner ad for placement name: " + currentSelectedPlacementName);
            InternalMoPubWrapper.LoadContent(PlacementType.Banner, currentSelectedPlacementName,
                delegate(PlacementLoadingState result)
                {
                    switch (result)
                    {
                        case PlacementLoadingState.Loaded:
                            ReportStatus("Load banner: SUCCESS");
                            break;
                        case PlacementLoadingState.Failure:
                            ReportStatus("Load banner: Failure");
                            break;
                        case PlacementLoadingState.Loading:
                            ReportStatus("Load banner: Already loading");
                            break;
                    }
                });
        }

        [UsedImplicitly]
        public void OnShowBannerClicked()
        {
            var placementName = GetCurrentSelectedPlacementName();
            InternalMoPubWrapper.DisplayContent(PlacementType.Banner, placementName,
                delegate(PlacementShowHideResult displayResult)
                {
                    switch (displayResult)
                    {
                        case PlacementShowHideResult.ShowHideFailed:
                            ReportStatus("Can't show banner. Not ready to display.");
                            break;
                        case PlacementShowHideResult.ShowHideSucceeded:
                            ReportStatus("Ready to display. Displaying banner now.");
                            break;
                    }
                }, delegate { ReportStatus("Banner \"" + placementName + "\" was closed."); });
        }

        [UsedImplicitly]
        public void OnHideBannerClicked()
        {
            var currentSelectedPlacementName = GetCurrentSelectedPlacementName();
            InternalMoPubWrapper.HideBanner(currentSelectedPlacementName);
            ReportStatus("Banner successfully hidden.");
        }

        [UsedImplicitly]
        public void OnReloadBannerClicked()
        {
            var currentSelectedPlacementName = GetCurrentSelectedPlacementName();
            InternalMoPubWrapper.ReloadBanner(currentSelectedPlacementName, delegate(PlacementLoadingState loadingState)
            {
                switch (loadingState)
                {
                    case PlacementLoadingState.Failure:
                        ReportStatus("Failed to reload banner.");
                        break;
                    case PlacementLoadingState.Loaded:
                        ReportStatus("Banner successfully reloaded.");
                        break;
                    case PlacementLoadingState.Loading:
                        ReportStatus("Banner placement is already being loaded.");
                        break;
                }
            });
        }

        [UsedImplicitly]
        public void OnBackClicked()
        {
            Hide();
        }

        private void ReportStatus(string status)
        {
            Debug.Log("BannerScreen -> status: " + status);
            InternalStatus.text = status;
        }

        private string GetCurrentSelectedPlacementName()
        {
            return InternalDropdown.captionText.text;
        }
    }
}