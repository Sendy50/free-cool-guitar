using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.UI;

namespace MWM.Ads.Demo
{
    [RequireComponent(typeof(Canvas))]
    public class ConsentGDPRCanvas : MonoBehaviour
    {
        public MWMMoPubWrapper InternalMoPubWrapper;

        public Text InternalStatus;

        public Button InternalDisplayConsentDefaultButton;

        public Button InternalDisplayConsentCustomButton;

        public MWMConsentDialog InternalMwmConsentDialog;

        private Canvas _canvas;

        private void Awake()
        {
            _canvas = GetComponent<Canvas>();
            InternalDisplayConsentDefaultButton.interactable = false;
            InternalDisplayConsentCustomButton.interactable = false;
        }

        public void Show()
        {
            _canvas.enabled = true;
        }

        private void Hide()
        {
            _canvas.enabled = false;
        }

        [UsedImplicitly]
        public void OnShowNeedToDisplayConsentClicked()
        {
            var flag = InternalMoPubWrapper.ShouldShowConsentDialog();
            InternalDisplayConsentDefaultButton.interactable = flag;
            InternalDisplayConsentCustomButton.interactable = flag;
            ReportStatus("Need To display Consent GDPR : " + flag);
        }

        [UsedImplicitly]
        public void OnDisplayDefaultConsentClicked()
        {
            // MoPubManager.OnConsentDialogLoadedEvent += OnConsentDialogLoaded;
            // MoPubAndroid.LoadConsentDialog();
        }

        [UsedImplicitly]
        public void OnDisplayCustomConsentClicked()
        {
            InternalMwmConsentDialog.Show(InternalMoPubWrapper);
        }

        [UsedImplicitly]
        public void OnBackClicked()
        {
            Hide();
        }

        private void ReportStatus(string status)
        {
            Debug.Log("ConsentGDPRCanvas -> status: " + status);
            InternalStatus.text = status;
        }

        private void OnConsentDialogLoaded()
        {
            // MoPubAndroid.ShowConsentDialog();
            // MoPubManager.OnConsentDialogLoadedEvent -= OnConsentDialogLoaded;
        }
    }
}