using UnityEngine;

public class GameModeScreen : MonoBehaviour
{
    public Canvas InGameScreen;

    public EndGameScreen EndGameScreen;

    public GameSceneManager GameSceneManager;

    public PauseMenuImpl PauseMenu;

    private void Start()
    {
        InGameScreen.gameObject.SetActive(true);
        EndGameScreen.gameObject.SetActive(false);
        PauseMenu.SetVisible(false);
        InGameScreen.worldCamera = Camera.main;
    }
}