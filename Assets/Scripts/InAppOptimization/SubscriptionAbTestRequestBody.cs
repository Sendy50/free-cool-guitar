namespace InAppOptimization
{
    public class SubscriptionAbTestRequestBody
    {
        public string app_id;

        public string app_version;

        public string device_type;
        public string installation_id;

        public string test_key;

        public SubscriptionAbTestRequestBody(string installationId, string appId, string appVersion, string deviceType,
            string inAppOptimizationTestKey)
        {
            installation_id = installationId;
            app_id = appId;
            app_version = appVersion;
            device_type = deviceType;
            test_key = inAppOptimizationTestKey;
        }
    }
}