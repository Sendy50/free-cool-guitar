using System;

namespace InAppOptimization
{
    public interface InAppOptimizationManager
    {
        void RequestVariation(Action<InAppOptimizationVariation> callback);

        InAppOptimizationVariation GetVariation();
    }
}