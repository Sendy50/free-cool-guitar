using System;
using Utils;

namespace InAppOptimization
{
    [Serializable]
    public class InAppOptimizationVariationData : InAppOptimizationVariation
    {
        private const string ErrorMessage =
            "Local variation does not contains this field. Checkout IsLocal() documentation.";

        private readonly string _endPointVersion;

        private readonly bool _local;

        private readonly string _testId;

        private readonly string _testKey;

        private readonly string _userGroupKey;

        private readonly string _variation;

        private InAppOptimizationVariationData(bool local, string variation, string testKey, string testId,
            string userGroupKey, string endPointVersion)
        {
            Precondition.CheckNotNull(variation);
            _variation = variation;
            _local = local;
            if (!local)
            {
                Precondition.CheckNotNull(testKey);
                Precondition.CheckNotNull(testId);
                Precondition.CheckNotNull(userGroupKey);
                Precondition.CheckNotNull(endPointVersion);
            }

            _testKey = testKey;
            _testId = testId;
            _userGroupKey = userGroupKey;
            _endPointVersion = endPointVersion;
        }

        public bool IsLocal()
        {
            return _local;
        }

        public string GetTestKey()
        {
            if (_local)
                throw new Exception("Local variation does not contains this field. Checkout IsLocal() documentation.");
            return _testKey;
        }

        public string GetTestId()
        {
            if (_local)
                throw new Exception("Local variation does not contains this field. Checkout IsLocal() documentation.");
            return _testId;
        }

        public string GetUserGroupKey()
        {
            if (_local)
                throw new Exception("Local variation does not contains this field. Checkout IsLocal() documentation.");
            return _userGroupKey;
        }

        public string GetVariation()
        {
            return _variation;
        }

        public string GetEndPointVersion()
        {
            if (_local)
                throw new Exception("Local variation does not contains this field. Checkout IsLocal() documentation.");
            return _endPointVersion;
        }

        public static InAppOptimizationVariationData CreateLocal(string variation)
        {
            return new InAppOptimizationVariationData(true, variation, null, null, null, null);
        }

        public static InAppOptimizationVariationData CreateNonLocal(string variation, string testKey, string testId,
            string userGroupKey, string endPointVersion)
        {
            Precondition.CheckNotNull(testKey);
            Precondition.CheckNotNull(testId);
            Precondition.CheckNotNull(userGroupKey);
            Precondition.CheckNotNull(endPointVersion);
            Precondition.CheckNotNull(variation);
            return new InAppOptimizationVariationData(false, variation, testKey, testId, userGroupKey, endPointVersion);
        }
    }
}