using System;
using System.Collections;
using System.Text;
using Async;
using Devices;
using JetBrains.Annotations;
using MWM.GameEvent;
using Newtonsoft.Json.Linq;
using UnityEngine;
using UnityEngine.Networking;
using Utils;

namespace InAppOptimization
{
    public class InAppOptimizationManagerImpl : InAppOptimizationManager
    {
        private const string Url =
            "https://inapp-optimization-dot-learning-guitar.appspot.com/v1/inapp-optimization/resolve-variation";

        private const string UrlDev =
            "https://dev-dot-inapp-optimization-dot-learning-guitar.appspot.com/v1/inapp-optimization/resolve-variation";

        private const string VariationDefault = "subscription.weekly.07.07.99";

        private const string AppId = "learning-guitar";

        private const string RequestHeaderAppKeyAndroid =
            "YrHXYHgAnvZkx96PMZsloXoxVpOMCDNvJypLW9914QvOsxVrFKexjobvmLphdszG";

        private const string RequestHeaderAppKeyIos =
            "VwddcmpvnCYgYYE5R0ovrsCgMVFNruJsKyEoc89DirvvfK5mhNZS9WB7MXz2aCV0";

        private const string EndPointVersion = "v1";

        private const string TestKey = "test_0001";

        private const int TimeoutSeconds = 3;

        private readonly AsyncJobExecutor _asyncJobExecutor;

        private readonly DeviceManager _deviceManager;

        private readonly InAppOptimizationStorage _inAppOptimizationStorage;

        [CanBeNull] private AsyncJob _asyncJob;

        private bool _doesNotNeedRequestAnymore;

        private InAppOptimizationVariationData _variation;

        public InAppOptimizationManagerImpl(DeviceManager deviceManager, AsyncJobExecutor asyncJobExecutor,
            InAppOptimizationStorage inAppOptimizationStorage)
        {
            Precondition.CheckNotNull(deviceManager);
            Precondition.CheckNotNull(asyncJobExecutor);
            Precondition.CheckNotNull(inAppOptimizationStorage);
            _deviceManager = deviceManager;
            _asyncJobExecutor = asyncJobExecutor;
            _inAppOptimizationStorage = inAppOptimizationStorage;
            _variation = _inAppOptimizationStorage.Load();
            if (_variation == null)
            {
                _doesNotNeedRequestAnymore = false;
                _variation = CreateDefaultInAppOptimizationVariation();
            }
            else
            {
                _doesNotNeedRequestAnymore = true;
            }
        }

        public void RequestVariation(Action<InAppOptimizationVariation> callback)
        {
            if (_doesNotNeedRequestAnymore)
            {
                callback(_variation);
                return;
            }

            _doesNotNeedRequestAnymore = true;
            var requestBody = CreateRequestBody();
            var tasks = CreateRequestEnumerator(requestBody, callback);
            if (_asyncJob != null) _asyncJobExecutor.Cancel(_asyncJob);
            _asyncJob = new AsyncJob(tasks, delegate(bool succeeded)
            {
                if (!succeeded)
                {
                    _inAppOptimizationStorage.Save(_variation);
                    callback(_variation);
                }
            });
            _asyncJobExecutor.Execute(_asyncJob);
        }

        public InAppOptimizationVariation GetVariation()
        {
            return _variation;
        }

        private void AddCustomHeaders(UnityWebRequest request)
        {
            var value = CreateDeviceType();
            request.SetRequestHeader("Content-Type", "application/json");
            request.SetRequestHeader("X-App-Id", "learning-guitar");
            request.SetRequestHeader("X-App-Key", CreateRequestHeaderAppKey());
            request.SetRequestHeader("X-Device-Type", value);
            request.SetRequestHeader("X-App-Version", Application.version);
        }

        private string CreateRequestHeaderAppKey()
        {
            switch (_deviceManager.GetDeviceTarget())
            {
                case DeviceTarget.Android:
                    return "YrHXYHgAnvZkx96PMZsloXoxVpOMCDNvJypLW9914QvOsxVrFKexjobvmLphdszG";
                case DeviceTarget.Ios:
                    return "VwddcmpvnCYgYYE5R0ovrsCgMVFNruJsKyEoc89DirvvfK5mhNZS9WB7MXz2aCV0";
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private string CreateEndPoint()
        {
            var flag = _deviceManager.GetRuntimeType() == RuntimeType.Editor;
            var flag2 = _deviceManager.IsDevelopmentBuild();
            if (flag || flag2)
                return
                    "https://dev-dot-inapp-optimization-dot-learning-guitar.appspot.com/v1/inapp-optimization/resolve-variation";
            return "https://inapp-optimization-dot-learning-guitar.appspot.com/v1/inapp-optimization/resolve-variation";
        }

        private string CreateDeviceType()
        {
            var deviceTarget = _deviceManager.GetDeviceTarget();
            switch (deviceTarget)
            {
                case DeviceTarget.Android:
                    return "android";
                case DeviceTarget.Ios:
                    return "ios";
                default:
                    throw new ArgumentOutOfRangeException("deviceTarget", deviceTarget, null);
            }
        }

        private SubscriptionAbTestRequestBody CreateRequestBody()
        {
            var version = Application.version;
            var installationId = GameEventSender.Instance().InstallationId;
            var deviceType = CreateDeviceType();
            return new SubscriptionAbTestRequestBody(installationId, "learning-guitar", version, deviceType,
                "test_0001");
        }

        private IEnumerator CreateRequestEnumerator(SubscriptionAbTestRequestBody requestBody,
            Action<InAppOptimizationVariation> callback)
        {
            var jsonParameters = JsonUtility.ToJson(requestBody);
            var request = new UnityWebRequest(CreateEndPoint())
            {
                uploadHandler = new UploadHandlerRaw(new UTF8Encoding().GetBytes(jsonParameters)),
                downloadHandler = new DownloadHandlerBuffer(),
                method = "POST",
                timeout = 3
            };
            AddCustomHeaders(request);
            var unityWebRequest = request;
            yield return request.SendWebRequest();
            if (request.isNetworkError || request.isHttpError)
            {
                _inAppOptimizationStorage.Save(_variation);
                callback(_variation);
                yield break;
            }

            var text = request.downloadHandler.text;
            var jObject = JObject.Parse(text);
            var variation = jObject["variation"].ToObject<string>();
            var userGroupKey = jObject["user_group_key"].ToObject<string>();
            var testId = jObject["test_id"].ToObject<string>();
            _variation =
                InAppOptimizationVariationData.CreateNonLocal(variation, "test_0001", testId, userGroupKey, "v1");
            _inAppOptimizationStorage.Save(_variation);
            callback(_variation);
        }

        private static InAppOptimizationVariationData CreateDefaultInAppOptimizationVariation()
        {
            return InAppOptimizationVariationData.CreateLocal("subscription.weekly.07.07.99");
        }
    }
}