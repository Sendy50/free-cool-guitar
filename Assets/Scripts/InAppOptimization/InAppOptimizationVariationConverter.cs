using Devices;
using Store;

namespace InAppOptimization
{
    public static class InAppOptimizationVariationConverter
    {
        public static string ExtractSku(InAppOptimizationVariation inAppOptimizationVariation,
            DeviceTarget deviceTarget)
        {
            var variation = inAppOptimizationVariation.GetVariation();
            return InAppConst.GetSubscriptionVariationSku(variation, deviceTarget);
        }
    }
}