using GuitarApplication;

namespace InAppOptimization
{
    public class InAppOptimizationModule
    {
        public InAppOptimizationManager CreateInAppOptimizationManager()
        {
            var deviceManager = ApplicationGraph.GetDeviceManager();
            var asyncJobExecutor = ApplicationGraph.GetAsyncJobExecutor();
            var inAppOptimizationStorage = CreateInappOptimizationStorage();
            return new InAppOptimizationManagerImpl(deviceManager, asyncJobExecutor, inAppOptimizationStorage);
        }

        private static InAppOptimizationStorage CreateInappOptimizationStorage()
        {
            return new InAppOptimizationStorageImpl();
        }
    }
}