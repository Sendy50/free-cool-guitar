namespace InAppOptimization
{
    public interface InAppOptimizationStorage
    {
        void Save(InAppOptimizationVariationData inAppOptimizationVariation);

        InAppOptimizationVariationData Load();
    }
}