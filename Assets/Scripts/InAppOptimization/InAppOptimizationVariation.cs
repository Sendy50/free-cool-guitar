namespace InAppOptimization
{
    public interface InAppOptimizationVariation
    {
        bool IsLocal();

        string GetTestKey();

        string GetTestId();

        string GetUserGroupKey();

        string GetVariation();

        string GetEndPointVersion();
    }
}