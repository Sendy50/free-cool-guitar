using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

namespace InAppOptimization
{
    public class InAppOptimizationStorageImpl : InAppOptimizationStorage
    {
        public static readonly string InAppOptimizationStorageDataPath =
            Application.persistentDataPath + "/in-app-optimization-storage";

        public void Save(InAppOptimizationVariationData inAppOptimizationVariation)
        {
            var serializationStream = File.Create(InAppOptimizationStorageDataPath);
            var binaryFormatter = new BinaryFormatter();
            binaryFormatter.Serialize(serializationStream, inAppOptimizationVariation);
        }

        public InAppOptimizationVariationData Load()
        {
            if (!File.Exists(InAppOptimizationStorageDataPath))
            {
                return null;
            }

            var serializationStream = File.OpenRead(InAppOptimizationStorageDataPath);
            var binaryFormatter = new BinaryFormatter();
            return (InAppOptimizationVariationData) binaryFormatter.Deserialize(serializationStream);
        }
    }
}