using System;
using JetBrains.Annotations;
using UnityEngine;

namespace MWM.Audio.Internal
{
    public class MusicGamingAudioEngineImpl : MusicGamingAudioEngine
    {
        private readonly MultiPlayerUnit _multiPlayerUnit;

        private readonly SamplerUnit _samplerUnit;

        private readonly SamplingSynthUnit _samplingSynthUnit;

        public MusicGamingAudioEngineImpl([CanBeNull] MultiPlayerUnit multiPlayerUnit,
            [CanBeNull] SamplerUnit samplerUnit, [CanBeNull] SamplingSynthUnit samplingSynthUnit)
        {
            _multiPlayerUnit = multiPlayerUnit;
            _samplerUnit = samplerUnit;
            _samplingSynthUnit = samplingSynthUnit;
        }

        public MultiPlayerUnit GetMultiPlayerUnit()
        {
            if (_multiPlayerUnit == null) throw new ApplicationException("Multi player unit not initialized.");
            return _multiPlayerUnit;
        }

        public SamplerUnit GetSamplerUnit()
        {
            if (_samplerUnit == null) throw new ApplicationException("Sampler unit not initialized.");
            return _samplerUnit;
        }

        public SamplingSynthUnit GetSamplingSynthUnit()
        {
            if (_samplingSynthUnit == null) throw new ApplicationException("Sampling synth unit not initialized.");
            return _samplingSynthUnit;
        }

        public virtual void SetAudioRendering(bool enable)
        {
            Debug.Log("MusicGamingAudioEngineImpl - SetAudioRendering - " + enable);
        }

        public virtual void Destroy()
        {
            Debug.Log("MusicGamingAudioEngineImpl - Destroy");
        }
    }
}