using System;
using IntroCarouselRessources;

public interface IIntroCarousel
{
    event Action<IIntroCarousel> ExitCarouselEvent;

    void Display(IRessoucesProvider ressoucesProvider);
}