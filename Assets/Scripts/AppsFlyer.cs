using System;
using System.Collections.Generic;
using UnityEngine;

public class AppsFlyer : MonoBehaviour
{
    private static readonly AndroidJavaClass obj = new AndroidJavaClass("com.appsflyer.AppsFlyerLib");

    private static readonly AndroidJavaObject cls_AppsFlyer = obj.CallStatic<AndroidJavaObject>("getInstance");

    private static readonly AndroidJavaClass
        propertiesClass = new AndroidJavaClass("com.appsflyer.AppsFlyerProperties");

    private static AndroidJavaObject
        afPropertiesInstance = propertiesClass.CallStatic<AndroidJavaObject>("getInstance");

    private static readonly AndroidJavaClass cls_AppsFlyerHelper =
        new AndroidJavaClass("com.appsflyer.AppsFlyerUnityHelper");

    private static string devKey;

    public static void trackEvent(string eventName, string eventValue)
    {
        print("AF.cs this is deprecated method. please use trackRichEvent instead. nothing is sent.");
    }

    public static void setCurrencyCode(string currencyCode)
    {
        cls_AppsFlyer.Call("setCurrencyCode", currencyCode);
    }

    public static void setCustomerUserID(string customerUserID)
    {
        cls_AppsFlyer.Call("setAppUserId", customerUserID);
    }

    public static void loadConversionData(string callbackObject)
    {
        var androidJavaClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        var androidJavaObject = androidJavaClass.GetStatic<AndroidJavaObject>("currentActivity");
        cls_AppsFlyerHelper.CallStatic("createConversionDataListener", androidJavaObject, callbackObject);
    }

    [Obsolete("Use loadConversionData(string callbackObject)")]
    public static void loadConversionData(string callbackObject, string callbackMethod, string callbackFailedMethod)
    {
        loadConversionData(callbackObject);
    }

    public static void setCollectIMEI(bool shouldCollect)
    {
        cls_AppsFlyer.Call("setCollectIMEI", shouldCollect);
    }

    public static void setCollectAndroidID(bool shouldCollect)
    {
        print("AF.cs setCollectAndroidID");
        cls_AppsFlyer.Call("setCollectAndroidID", shouldCollect);
    }

    public static void init(string key, string callbackObject)
    {
        init(key);
        if (callbackObject != null) loadConversionData(callbackObject);
    }

    public static void init(string key)
    {
        print("AF.cs init");
        devKey = key;
        var androidJavaClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        var androidJavaObject = androidJavaClass.GetStatic<AndroidJavaObject>("currentActivity");
        androidJavaObject.Call("runOnUiThread", new AndroidJavaRunnable(init_cb));
    }

    private static void init_cb()
    {
        print("AF.cs start tracking");
        trackAppLaunch();
    }

    public static void setAppsFlyerKey(string key)
    {
        print("AF.cs setAppsFlyerKey");
    }

    public static void trackAppLaunch()
    {
        print("AF.cs trackAppLaunch");
        var androidJavaClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        var androidJavaObject = androidJavaClass.GetStatic<AndroidJavaObject>("currentActivity");
        var androidJavaObject2 = androidJavaObject.Call<AndroidJavaObject>("getApplication");
        cls_AppsFlyer.Call("startTracking", androidJavaObject2, devKey);
        cls_AppsFlyer.Call("trackAppLaunch", androidJavaObject, devKey);
    }

    public static void setAppID(string packageName)
    {
        cls_AppsFlyer.Call("setAppId", packageName);
    }

    public static void createValidateInAppListener(string aObject, string callbackMethod, string callbackFailedMethod)
    {
        print("AF.cs createValidateInAppListener called");
        var androidJavaClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        var androidJavaObject = androidJavaClass.GetStatic<AndroidJavaObject>("currentActivity");
        cls_AppsFlyerHelper.CallStatic("createValidateInAppListener", androidJavaObject, aObject, callbackMethod,
            callbackFailedMethod);
    }

    public static void validateReceipt(string publicKey, string purchaseData, string signature, string price,
        string currency, Dictionary<string, string> extraParams)
    {
        print("AF.cs validateReceipt pk = " + publicKey + " data = " + purchaseData + "sig = " + signature);
        var androidJavaClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        var androidJavaObject2 = androidJavaClass.GetStatic<AndroidJavaObject>("currentActivity");
        AndroidJavaObject androidJavaObject = null;
        if (extraParams != null) androidJavaObject = ConvertHashMap(extraParams);
        print("inside cls_activity");
        cls_AppsFlyer.Call("validateAndTrackInAppPurchase", androidJavaObject2, publicKey, signature, purchaseData,
            price, currency, androidJavaObject);
    }

    public static void trackRichEvent(string eventName, Dictionary<string, string> eventValues)
    {
        var androidJavaClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        var androidJavaObject2 = androidJavaClass.GetStatic<AndroidJavaObject>("currentActivity");
        var androidJavaObject = ConvertHashMap(eventValues);
        cls_AppsFlyer.Call("trackEvent", androidJavaObject2, eventName, androidJavaObject);
    }

    private static AndroidJavaObject ConvertHashMap(Dictionary<string, string> dict)
    {
        var androidJavaObject = new AndroidJavaObject("java.util.HashMap");
        var methodID = AndroidJNIHelper.GetMethodID(androidJavaObject.GetRawClass(), "put",
            "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;");
        var array = new object[2];
        foreach (var item in dict)
        {
            var androidJavaObject2 = new AndroidJavaObject("java.lang.String", item.Key);
            var androidJavaObject3 = new AndroidJavaObject("java.lang.String", item.Value);
            array[0] = androidJavaObject2;
            array[1] = androidJavaObject3;
            AndroidJNI.CallObjectMethod(androidJavaObject.GetRawObject(), methodID,
                AndroidJNIHelper.CreateJNIArgArray(array));
        }

        return androidJavaObject;
    }

    public static void setImeiData(string imeiData)
    {
        print("AF.cs setImeiData");
        cls_AppsFlyer.Call("setImeiData", imeiData);
    }

    public static void setAndroidIdData(string androidIdData)
    {
        print("AF.cs setImeiData");
        cls_AppsFlyer.Call("setAndroidIdData", androidIdData);
    }

    public static void setIsDebug(bool isDebug)
    {
        print("AF.cs setDebugLog");
        cls_AppsFlyer.Call("setDebugLog", isDebug);
    }

    public static void setIsSandbox(bool isSandbox)
    {
    }

    public static void getConversionData()
    {
    }

    public static void handleOpenUrl(string url, string sourceApplication, string annotation)
    {
    }

    public static string getAppsFlyerId()
    {
        var androidJavaClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        var androidJavaObject = androidJavaClass.GetStatic<AndroidJavaObject>("currentActivity");
        return cls_AppsFlyer.Call<string>("getAppsFlyerUID", androidJavaObject);
    }

    public static void setGCMProjectNumber(string googleGCMNumber)
    {
        cls_AppsFlyer.Call("setGCMProjectNumber", googleGCMNumber);
    }

    public static void updateServerUninstallToken(string token)
    {
        var androidJavaClass = new AndroidJavaClass("com.appsflyer.AppsFlyerLib");
        var androidJavaObject = androidJavaClass.CallStatic<AndroidJavaObject>("getInstance");
        var androidJavaClass2 = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        var androidJavaObject2 = androidJavaClass2.GetStatic<AndroidJavaObject>("currentActivity");
        androidJavaObject.Call("updateServerUninstallToken", androidJavaObject2, token);
    }

    public static void enableUninstallTracking(string senderId)
    {
        var androidJavaClass = new AndroidJavaClass("com.appsflyer.AppsFlyerLib");
        var androidJavaObject = androidJavaClass.CallStatic<AndroidJavaObject>("getInstance");
        androidJavaObject.Call("enableUninstallTracking", senderId);
    }

    public static void setDeviceTrackingDisabled(bool state)
    {
        cls_AppsFlyer.Call("setDeviceTrackingDisabled", state);
    }
}