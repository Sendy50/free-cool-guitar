using JetBrains.Annotations;
using UnityEngine;

namespace MWM.Core.Demo
{
    public class MainDemoSceen : MonoBehaviour
    {
        [UsedImplicitly]
        public void OnAdsButtonClicked()
        {
            Router.GoToAdsDemoScreen();
        }

        [UsedImplicitly]
        public void OnAudioDemoClicked()
        {
            Router.GoToAudioDemoScreen();
        }

        [UsedImplicitly]
        public void OnGameEventDemoClicked()
        {
            Router.GoToGameEventDemoScreen();
        }

        [UsedImplicitly]
        public void OnInAppDemoClicked()
        {
            Router.GoToInAppDemoScreen();
        }

        [UsedImplicitly]
        public void OnSafeAreaDemoClicked()
        {
            Router.GoToSafeAreaDemoScreen();
        }
    }
}