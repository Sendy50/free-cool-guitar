using UnityEngine.SceneManagement;

namespace MWM.Core.Demo
{
    public static class Router
    {
        public static void GoToAudioDemoScreen()
        {
            SceneManager.LoadScene("MWM/Audio/Demo/AudioDemoScene");
        }

        public static void GoToMainDemoScreen()
        {
            SceneManager.LoadScene("MWM/Core/Demo/MainDemoScene");
        }

        public static void GoToAdsDemoScreen()
        {
            SceneManager.LoadScene("MWM/Ads/Demo/MoPubTestScene");
        }

        public static void GoToGameEventDemoScreen()
        {
            SceneManager.LoadScene("MWM/GameEvent/Demo/GameEventDemoScene");
        }

        public static void GoToInAppDemoScreen()
        {
            SceneManager.LoadScene("MWM/InApp/Demo/InAppDemoScene");
        }

        public static void GoToSafeAreaDemoScreen()
        {
            SceneManager.LoadScene("MWM/Ui/SafeArea/Demo/SafeAreaDemoScene");
        }
    }
}