using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Canvas))]
public class iOSCanvasScaler : MonoBehaviour
{
    public InterfaceIdiomEditor interfaceIdiomEditor;

    public CanvasScaler CanvasScaler => gameObject.GetComponent<CanvasScaler>();

    private void Awake()
    {
        UpdateUI();
    }

    private void UpdateUI()
    {
        var canvasScaler = gameObject.GetComponent<CanvasScaler>();
        if (canvasScaler == null) canvasScaler = gameObject.AddComponent<CanvasScaler>();
        canvasScaler.uiScaleMode = CanvasScaler.ScaleMode.ScaleWithScreenSize;
        canvasScaler.screenMatchMode = CanvasScaler.ScreenMatchMode.MatchWidthOrHeight;
        canvasScaler.referencePixelsPerUnit = 100f;
        canvasScaler.referenceResolution = new Vector2(2208f, 1242f);
    }
}