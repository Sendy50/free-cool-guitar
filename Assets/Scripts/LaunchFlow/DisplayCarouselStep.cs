using System;
using AssetBundles;
using Devices;
using GuitarApplication;
using IntroCarouselRessources;
using MWM.GameEvent;
using Utils;

namespace LaunchFlow
{
    public class DisplayCarouselStep
    {
        private readonly IIntroCarouselAssetProxy _assetProxy;

        private readonly DeviceManager _deviceManager;

        private readonly IIntroCarouselDisplayPolicy _displayPolicy;

        private readonly IGameEventSender _gameEventSender;
        private readonly IIntroCarouselInstanciator _instanciator;

        private readonly ApplicationRouter _router;

        private Action _callback;

        public DisplayCarouselStep(IIntroCarouselInstanciator instanciator, IIntroCarouselDisplayPolicy displayPolicy,
            ApplicationRouter router, IIntroCarouselAssetProxy assetProxy, IGameEventSender gameEventSender,
            DeviceManager deviceManager)
        {
            Precondition.CheckNotNull(instanciator);
            Precondition.CheckNotNull(displayPolicy);
            Precondition.CheckNotNull(router);
            Precondition.CheckNotNull(assetProxy);
            Precondition.CheckNotNull(gameEventSender);
            Precondition.CheckNotNull(deviceManager);
            _instanciator = instanciator;
            _displayPolicy = displayPolicy;
            _router = router;
            _assetProxy = assetProxy;
            _gameEventSender = gameEventSender;
            _deviceManager = deviceManager;
        }

        public void RunStep(Action callback = null)
        {
            var flag = !_displayPolicy.HasCarouselBeenCompleted();
            if (_displayPolicy.HasFourthSlideBeenSeen() && flag)
            {
                _displayPolicy.OnCarouselCompleted();
                _gameEventSender.SendEvent(EventType.IntroCarouselSkippedAtSecondAppLaunch, string.Empty);
                flag = false;
            }

            if (flag)
            {
                _callback = callback;
                var resourceKindFromDeviceTarget = GetResourceKindFromDeviceTarget(_deviceManager.GetDeviceTarget());
                _assetProxy.GetRessoucesProvider(resourceKindFromDeviceTarget,
                    delegate(IRessoucesProvider ressoucesProvider) { DisplayIntroCarousel(ressoucesProvider); });
            }
            else
            {
                _router.HideLaunchScreen();
                callback?.Invoke();
            }
        }

        private void DisplayIntroCarousel(IRessoucesProvider ressoucesProvider)
        {
            var introCarousel = _instanciator.Instantiate();
            introCarousel.ExitCarouselEvent += OnExiteCarousel;
            introCarousel.Display(ressoucesProvider);
            _router.HideLaunchScreen();
        }

        private void OnExiteCarousel(IIntroCarousel sender)
        {
            _instanciator.Destroy(sender);
            _assetProxy.Dispose();
            if (_callback != null) _callback();
        }

        private static IntroCarouselRessourceKind GetResourceKindFromDeviceTarget(DeviceTarget deviceTarget)
        {
            switch (deviceTarget)
            {
                case DeviceTarget.Android:
                    return IntroCarouselRessourceKind.Image;
                case DeviceTarget.Ios:
                    return IntroCarouselRessourceKind.Video;
                default:
                    throw new Exception("We didn't manage this deviceTarget current value : " + deviceTarget);
            }
        }
    }
}