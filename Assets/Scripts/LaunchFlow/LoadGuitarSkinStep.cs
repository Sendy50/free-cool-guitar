using System;
using System.Linq;
using AssetBundles.GuitarSkin;
using UnityEngine;
using Utils;

namespace LaunchFlow
{
    public class LoadGuitarSkinStep
    {
        private readonly IGuitarHolder _guitarHolder;

        private readonly MonoBehaviour _monoBehaviour;

        public LoadGuitarSkinStep(IGuitarHolder guitarHolder, MonoBehaviour monoBehaviour)
        {
            Precondition.CheckNotNull(guitarHolder);
            Precondition.CheckNotNull(monoBehaviour);
            _guitarHolder = guitarHolder;
            _monoBehaviour = monoBehaviour;
        }

        public void RunStep(Action callback)
        {
            Debug.LogError("RunStep");
            PrepareGuitarList();
            LoadGuitarSkin(_guitarHolder.selectedGuitar, callback);
        }

        private void LoadGuitarSkin(IGuitar guitar, Action callback)
        {
            guitar.GetSkinProxy().GetSkin(delegate(IGuitarSkin skin, bool success)
            {
                if (success)
                    callback();
                else
                    OnCurrentGuitarSkinLoadFailed(guitar, callback);
            });
        }

        private void OnCurrentGuitarSkinLoadFailed(IGuitar guitar, Action callback)
        {
            if (_guitarHolder.allGuitars.IndexOf(guitar) == 0)
                throw new Exception("The default guitar skin can't be loaded");
            var guitar2 = _guitarHolder.allGuitars[0];
            _guitarHolder.SetSelectedGuitar(guitar2);
            LoadGuitarSkin(guitar2, callback);
        }

        private void PrepareGuitarList()
        {
            foreach (var item in _guitarHolder.allGuitars.Where(c => c.SkinReference.IsAppleODR()))
                item.SetupSkinProxy(new GuitarAppleODRSkinProxy(item, item.SkinReference.BundleName(),
                    item.SkinReference.AppleODRTag(), item.SkinReference.SlicingBundleName(),
                    item.SkinReference.SlicingAppleODRTag(), _monoBehaviour));
            foreach (var item2 in _guitarHolder.allGuitars.Where(c => !c.SkinReference.IsAppleODR()))
                item2.SetupSkinProxy(new GuitarLocalSkinProxy(item2, item2.SkinReference.BundleName(),
                    item2.SkinReference.SlicingBundleName(), _monoBehaviour));
        }
    }
}