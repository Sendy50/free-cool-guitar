using System;
using System.Collections;
using AdditionalContent;
using Ads;
using Config;
using Devices;
using GuitarApplication;
using InAppOptimization;
using MWM.Audio;
using Notification;
using Store;
using Subscription;
using UnityEngine;
using Utils;

namespace LaunchFlow
{
    public class LaunchFlowController
    {
        private const int InAppManagerInitializationTimeoutSeconds = 5;

        private readonly AdditionalContentSynchronizer _additionalContentSynchronizer;

        // private readonly DisplayCarouselStep _displayCarouselStep;

        private readonly ApplicationRouter _applicationRouter;

        // private readonly InAppManager _inAppManager;

        private readonly ApplicationStatusHolder _applicationStatusHolder;

        private readonly ConfigStorageContainer.ConfigStorage _configStorageContainer;

        // private readonly SubscriptionManager _subscriptionManager;

        private readonly DeviceManager _deviceManager;

        // private readonly ExternalAdsWrapper _externalAdsWrapper;

        private readonly InAppOptimizationManager _inAppOptimizationManager;

        private readonly LoadGuitarSkinStep _loadGuitarSkinStep;

        private readonly LocalNotificationManager _localNotificationManager;

        private readonly MonoBehaviour _monoBehaviour;

        private readonly LoadSoundSystemStep _soundSystemLoader;

        private readonly IStatistics _statistics;

        private readonly WeeklyFeaturedTrackManager _weeklyFeaturedTrackManager;

        public LaunchFlowController(MonoBehaviour monoBehaviour, LoadSoundSystemStep soundSystemLoader,
            AdditionalContentSynchronizer additionalContentSynchronizer, ApplicationRouter applicationRouter,
            LoadGuitarSkinStep loadGuitarSkinStep, IStatistics statistics, InAppManager inAppManager,
            ApplicationStatusHolder applicationStatusHolder, LocalNotificationManager localNotificationManager,
            ExternalAdsWrapper externalAdsWrapper, InAppOptimizationManager inAppOptimizationManager,
            SubscriptionManager subscriptionManager, DeviceManager deviceManager,
            WeeklyFeaturedTrackManager weeklyFeaturedTrackManager,
            ConfigStorageContainer.ConfigStorage configStorageContainer)
        {
            Precondition.CheckNotNull(monoBehaviour);
            Precondition.CheckNotNull(soundSystemLoader);
            Precondition.CheckNotNull(additionalContentSynchronizer);
            // Precondition.CheckNotNull(displayCarouselStep);
            Precondition.CheckNotNull(applicationRouter);
            Precondition.CheckNotNull(loadGuitarSkinStep);
            Precondition.CheckNotNull(statistics);
            // Precondition.CheckNotNull(inAppManager);
            Precondition.CheckNotNull(applicationStatusHolder);
            Precondition.CheckNotNull(localNotificationManager);
            // Precondition.CheckNotNull(externalAdsWrapper);
            Precondition.CheckNotNull(inAppOptimizationManager);
            // Precondition.CheckNotNull(subscriptionManager);
            Precondition.CheckNotNull(deviceManager);
            Precondition.CheckNotNull(weeklyFeaturedTrackManager);
            Precondition.CheckNotNull(configStorageContainer);
            _monoBehaviour = monoBehaviour;
            _soundSystemLoader = soundSystemLoader;
            _additionalContentSynchronizer = additionalContentSynchronizer;
            // _displayCarouselStep = displayCarouselStep;
            _applicationRouter = applicationRouter;
            _loadGuitarSkinStep = loadGuitarSkinStep;
            _statistics = statistics;
            // _inAppManager = inAppManager;
            _applicationStatusHolder = applicationStatusHolder;
            _localNotificationManager = localNotificationManager;
            // _externalAdsWrapper = externalAdsWrapper;
            _inAppOptimizationManager = inAppOptimizationManager;
            // _subscriptionManager = subscriptionManager;
            _deviceManager = deviceManager;
            _weeklyFeaturedTrackManager = weeklyFeaturedTrackManager;
            _configStorageContainer = configStorageContainer;
        }

        public void Run(Action<MusicGamingAudioEngine> completion)
        {
            _monoBehaviour.StartCoroutine(RunRoutine(completion));
        }

        private IEnumerator RunRoutine(Action<MusicGamingAudioEngine> completion)
        {
            MusicGamingAudioEngine audioEngine = null;
            _soundSystemLoader.RunStep(delegate(MusicGamingAudioEngine engine) { audioEngine = engine; });
            var isLoadGuitarSkinFinish = false;
            _loadGuitarSkinStep.RunStep(delegate { isLoadGuitarSkinFinish = true; });
           // var isAdditionalContentDownloaded = false;

            //_additionalContentSynchronizer.Synchronize(delegate { isAdditionalContentDownloaded = true; });

            // if (Application.internetReachability != 0)
            // {
            // 	float start = Time.realtimeSinceStartup;
            // 	yield return new WaitUntil(() => _inAppManager.GetStatus() != 0 || Time.realtimeSinceStartup - start > 5f);
            // }
            // if (!_inAppManager.IsSubscribe())
            // {
            // 	bool isAppStatusRequestFinish = false;
            // 	_applicationStatusHolder.RunStep(delegate
            // 	{
            // 		isAppStatusRequestFinish = true;
            // 	});
            // 	if (_statistics.GetNumberAppLaunches() < 1)
            // 	{
            // 		bool isWeeklyFeatureTrackFinish = false;
            // 		_weeklyFeaturedTrackManager.RequestWeeklyFeaturedTrack(delegate
            // 		{
            // 			isWeeklyFeatureTrackFinish = true;
            // 		});
            // 		yield return new WaitUntil(() => isWeeklyFeatureTrackFinish);
            // 	}
            // 	if (!_configStorageContainer.GetConfig().HasWonSubscriptionValue)
            // 	{
            // 		bool requestInAppOptimizationCompleted = false;
            // 		RequestInAppOptimization(delegate
            // 		{
            // 			requestInAppOptimizationCompleted = true;
            // 		});
            // 		yield return new WaitUntil(() => requestInAppOptimizationCompleted);
            // 	}
            // 	yield return new WaitUntil(() => isAppStatusRequestFinish);
            // 	bool isShowCarouselFinish = false;
            // 	_displayCarouselStep.RunStep(delegate
            // 	{
            // 		isShowCarouselFinish = true;
            // 	});
            // 	yield return new WaitUntil(() => isShowCarouselFinish);
            // }
            // else
            // {
            _applicationRouter.HideLaunchScreen();
            // }
            _applicationRouter.ShowLoader();
            yield return new WaitUntil(() => _soundSystemLoader.isFinish && audioEngine != null);
            Debug.LogError("_soundSystemLoader.isFinish");
            yield return new WaitUntil(() => isLoadGuitarSkinFinish);
            Debug.LogError("isLoadGuitarSkinFinish");
            // yield return new WaitUntil(() => isAdditionalContentDownloaded);
            // Debug.LogError("isAdditionalContentDownloaded");
            // if (!_inAppManager.IsSubscribe())
            // {
            // 	if (_externalAdsWrapper.IsInitialized())
            // 	{
            // 		PreloadAds();
            // 	}
            // 	else
            // 	{
            // 		_externalAdsWrapper.RegisterToInitialization(OnAdsInit);
            // 	}
            // }
            _soundSystemLoader.ActivateAudioRendering();
            completion(audioEngine);
            _applicationRouter.HideLoader();
            _applicationRouter.RouteToFreeModeFromStartApplication();
            // if (_statistics.GetNumberAppLaunches() > 0 && !_inAppManager.IsSubscribe() && _applicationStatusHolder.IsDeployed())
            // {
            // 	_applicationRouter.RouteToSplashStoreFromStartAppWithDelay(0.5f);
            // }
            _statistics.IncrementNumAppLaunches();
            yield return new WaitForSeconds(0.25f);
            _localNotificationManager.RegisterForNotifications();
        }

        private void OnAdsInit()
        {
            // _externalAdsWrapper.UnregisterToInitialization(OnAdsInit);
            PreloadAds();
        }

        private void PreloadAds()
        {
            // _externalAdsWrapper.LoadContent(PlacementType.RewardVideo, "RewardedVideoPlacement", delegate
            // {
            // });
            // _externalAdsWrapper.LoadContent(PlacementType.Interstitial, "InterstitialPlacement", delegate
            // {
            // });
        }

        private void RequestInAppOptimization(Action callback)
        {
            _inAppOptimizationManager.RequestVariation(delegate(InAppOptimizationVariation inAppOptimizationVariation)
            {
                var sku = InAppOptimizationVariationConverter.ExtractSku(inAppOptimizationVariation,
                    _deviceManager.GetDeviceTarget());
                // _subscriptionManager.SetSku(sku);
                callback();
            });
        }
    }
}