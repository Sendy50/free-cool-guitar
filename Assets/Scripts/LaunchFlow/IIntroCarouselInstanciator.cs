namespace LaunchFlow
{
    public interface IIntroCarouselInstanciator
    {
        IIntroCarousel Instantiate();

        void Destroy(IIntroCarousel carrousel);
    }
}