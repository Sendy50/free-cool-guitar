using System;
using ABTest;
using AssetBundles;

namespace LaunchFlow
{
    public interface IIntroCarouselRemoteConfiguration
    {
        void Fetch(Action<IntroCarouselRessourceKind, IVariation> callback);
    }
}