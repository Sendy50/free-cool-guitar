using System;
using System.Collections;
using MWM.Audio;
using UnityEngine;
using Utils;

namespace LaunchFlow
{
    public class LoadSoundSystemStep
    {
        private readonly AudioEngineFactory _audioEngineFactory;

        private Action<MusicGamingAudioEngine> _callback;
        private readonly GuitarHolder _guitarHolder;

        private readonly MonoBehaviour _monoBehaviour;

        private MusicGamingAudioEngine _musicGamingAudioEngine;

        public LoadSoundSystemStep(MonoBehaviour monoBehaviour, AudioEngineFactory audioEngineFactory,
            GuitarHolder guitarHolder)
        {
            Precondition.CheckNotNull(guitarHolder);
            Precondition.CheckNotNull(audioEngineFactory);
            Precondition.CheckNotNull(monoBehaviour);
            _guitarHolder = guitarHolder;
            _monoBehaviour = monoBehaviour;
            _audioEngineFactory = audioEngineFactory;
            isFinish = false;
        }

        public bool isFinish { get; private set; }

        public void RunStep(Action<MusicGamingAudioEngine> callback = null)
        {
            isFinish = false;
            _callback = callback;
            _monoBehaviour.StartCoroutine(InitializeMusicPlayerRoutine());
        }

        public void ActivateAudioRendering()
        {
            _monoBehaviour.StartCoroutine(ActivateAudioRenderingRoutine());
        }

        private IEnumerator ActivateAudioRenderingRoutine()
        {
            yield return new WaitUntil(() => isFinish && _musicGamingAudioEngine != null);
            _musicGamingAudioEngine.SetAudioRendering(true);
        }

        private IEnumerator InitializeMusicPlayerRoutine()
        {
            MusicGamingAudioEngine audioEngine = null;
            _audioEngineFactory.CreateForApp(1, 5, 2,
                delegate(MusicGamingAudioEngine engine) { audioEngine = engine; });
            yield return new WaitUntil(() => audioEngine != null);
            var sampler = audioEngine.GetSamplerUnit();
            var samplerLoadConfiguration = new SamplerConfiguration();
            var samplingSynth = audioEngine.GetSamplingSynthUnit();
            samplerLoadConfiguration.AddInternal("1", "SamplesMWM/pianoLearn_applause");
            samplerLoadConfiguration.AddInternal("2", "SamplesMWM/pianoLearn_scoreStar1");
            samplerLoadConfiguration.AddInternal("3", "SamplesMWM/pianoLearn_scoreStar2");
            samplerLoadConfiguration.AddInternal("4", "SamplesMWM/pianoLearn_scoreUp");
            samplerLoadConfiguration.AddInternal("5", "SamplesMWM/pianoLearn_click");
            var isSamplingSynthLoaded = false;
            samplingSynth.LoadInternal(_guitarHolder.selectedGuitar.FilePath, delegate(LoadingResult result)
            {
                Debug.LogError("Loaded  : " + _guitarHolder.selectedGuitar.FilePath);
                isSamplingSynthLoaded = result.IsSuccess();
            });
            var isSamplerLoaded = false;
            sampler.Load(samplerLoadConfiguration,
                delegate(LoadingResult result) { isSamplerLoaded = result.IsSuccess(); });
            yield return new WaitUntil(() => isSamplingSynthLoaded && isSamplerLoaded);
            _musicGamingAudioEngine = audioEngine;
            isFinish = true;
            if (_callback != null) _callback(audioEngine);
        }
    }
}