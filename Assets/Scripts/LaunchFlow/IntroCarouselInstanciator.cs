using Config;
using GuitarApplication;
using MWM.GameEvent;
using Store;
using Utils;

namespace LaunchFlow
{
    public class IntroCarouselInstanciator : IIntroCarouselInstanciator
    {
        private ApplicationRouter _applicationRouter;

        private readonly ApplicationStatusHolder _appStatusHolder;

        private readonly ConfigStorageContainer.ConfigStorage _configStoragePersistent;

        private readonly IGameEventSender _eventSender;

        private readonly InAppManager _inappManager;

        private IntroCarouselAdapter _instanceRef;
        private readonly ScreenInstanceLocator _screenInstanceLocator;

        public IntroCarouselInstanciator(ScreenInstanceLocator screenInstanceLocator, InAppManager inappManager,
            ApplicationRouter applicationRouter, ApplicationStatusHolder appStatusHolder, IGameEventSender eventSender,
            ConfigStorageContainer.ConfigStorage configStoragePersistent)
        {
            Precondition.CheckNotNull(screenInstanceLocator);
            Precondition.CheckNotNull(inappManager);
            Precondition.CheckNotNull(applicationRouter);
            Precondition.CheckNotNull(appStatusHolder);
            Precondition.CheckNotNull(eventSender);
            Precondition.CheckNotNull(configStoragePersistent);
            _screenInstanceLocator = screenInstanceLocator;
            _inappManager = inappManager;
            _applicationRouter = applicationRouter;
            _appStatusHolder = appStatusHolder;
            _eventSender = eventSender;
            _configStoragePersistent = configStoragePersistent;
        }

        public IIntroCarousel Instantiate()
        {
            _instanceRef = new IntroCarouselAdapter(_screenInstanceLocator, _inappManager, _appStatusHolder,
                _eventSender, _configStoragePersistent);
            return _instanceRef;
        }

        public void Destroy(IIntroCarousel carrousel)
        {
            if (carrousel == _instanceRef)
            {
                _instanceRef.Dispose();
                _instanceRef = null;
            }
        }
    }
}