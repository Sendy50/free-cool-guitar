using System.Collections;
using System.IO;
using IntroCarouselRessources;
using UnityEngine;
using UnityEngine.Video;

namespace AssetBundles
{
    public abstract class IntroCarouselAssetLoader : IRessoucesProvider
    {
        protected AssetBundle _cachedAssetBundle;

        private readonly IntroCarouselRessourceKind _ressourceKind;

        public IntroCarouselAssetLoader(IntroCarouselRessourceKind ressourceKind)
        {
            _ressourceKind = ressourceKind;
        }

        public abstract VideoClip GetVideoClipForSlide(IntroCarouselSlideNumber slideNumber);

        public abstract RenderTexture GetVideoTextureForSlide(IntroCarouselSlideNumber slideNumber);

        public abstract Sprite GetSpriteForSlide(IntroCarouselSlideNumber slideNumber);

        public IntroCarouselRessourceKind GetRessourceKind()
        {
            return _ressourceKind;
        }

        public abstract bool IsAssetsLoaded();

        public abstract IEnumerator LoadAsset();

        public bool IsBundleLoaded()
        {
            return _cachedAssetBundle != null;
        }

        public void UnloadBundle(bool unloadAllLoadedObjects = false)
        {
            if (_cachedAssetBundle != null) _cachedAssetBundle.Unload(unloadAllLoadedObjects);
        }

        public IEnumerator LoadBundle()
        {
            var request = AssetBundle.LoadFromFileAsync(Path.Combine(Constantes.AssetBundlesPath, "intro-carrousel"));
            yield return request;
            _cachedAssetBundle = request.assetBundle;
        }
    }
}