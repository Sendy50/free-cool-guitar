using System;
using System.Collections;
using IntroCarouselRessources;
using UnityEngine;
using Utils;

namespace AssetBundles
{
    public class IntroCarouselAssetProxy : IIntroCarouselAssetProxy, IDisposable
    {
        private IntroCarouselAssetLoader _assetLoader;

        private readonly MonoBehaviour _monoBehaviour;

        public IntroCarouselAssetProxy(MonoBehaviour monoBehaviour)
        {
            Precondition.CheckNotNull(monoBehaviour);
            _monoBehaviour = monoBehaviour;
        }

        public void Dispose()
        {
            if (_assetLoader != null) _assetLoader.UnloadBundle();
        }

        public void GetRessoucesProvider(IntroCarouselRessourceKind ressourceKind, Action<IRessoucesProvider> callback)
        {
            _monoBehaviour.StartCoroutine(LoadVideoRoutine(ressourceKind, callback));
        }

        private IEnumerator LoadVideoRoutine(IntroCarouselRessourceKind ressourceKind,
            Action<IRessoucesProvider> callback)
        {
            switch (ressourceKind)
            {
                case IntroCarouselRessourceKind.Video:
                    _assetLoader = new IntroCarouselVideoAssetLoader(ressourceKind);
                    break;
                case IntroCarouselRessourceKind.Image:
                    _assetLoader = new IntroCarouselImageAssetLoader(ressourceKind);
                    break;
                default:
                    throw new Exception("IntroCarouselRessourceKind unknown");
            }

            yield return _assetLoader.LoadBundle();
            yield return _assetLoader.LoadAsset();
            callback(_assetLoader);
        }
    }
}