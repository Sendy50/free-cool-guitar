using System.IO;
using System.Runtime.InteropServices;
using UnityEngine;

namespace AssetBundles
{
    [StructLayout(LayoutKind.Sequential, Size = 1)]
    public struct Constantes
    {
        public static string AssetBundlesPath = Path.Combine(Application.streamingAssetsPath, "AndroidAssets");
        // public static string AssetBundlesPath = Path.Combine(Application.dataPath + "!assets/", "Android");

        public const string GuitarSkin12StringBundleName = "skin-12-strings";

        public const string GuitarSkinClassicBundleName = "skin-classic";

        public const string GuitarSkinFolkBundleName = "skin-folk";

        public const string GuitarSkinJazzBundleName = "skin-jazz";

        public const string GuitarSkinRockBundleName = "skin-rock";

        public const string SlicingGuitarSkin12StringBundleName = "skin-12-strings-slicing";

        public const string SlicingGuitarSkinClassicBundleName = "skin-classic-slicing";

        public const string SlicingGuitarSkinFolkBundleName = "skin-folk-slicing";

        public const string SlicingGuitarSkinJazzBundleName = "skin-jazz-slicing";

        public const string SlicingGuitarSkinRockBundleName = "skin-rock-slicing";

        public const string IntroCarouselBundleName = "intro-carrousel";
    }
}