using System;
using IntroCarouselRessources;

namespace AssetBundles
{
    public interface IIntroCarouselAssetProxy : IDisposable
    {
        void GetRessoucesProvider(IntroCarouselRessourceKind ressourceKind, Action<IRessoucesProvider> callback);
    }
}