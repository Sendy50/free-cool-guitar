using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Video;

namespace AssetBundles
{
    public class IntroCarouselVideoAssetLoader : IntroCarouselAssetLoader
    {
        private const string kVideoControl1AssetName = "1_1";

        private const string kVideoControl2AssetName = "2_1";

        private const string kVideoControl3AssetName = "3_1";

        private const string kVideoControl4AssetName = "4_1";

        private const string kVideoTexturRendererAssetName = "RenderTexture";

        private Dictionary<IntroCarouselSlideNumber, RenderTexture> _cachedRenderTextures;

        private Dictionary<IntroCarouselSlideNumber, VideoClip> _cachedVideos;

        private readonly string[] _videoAssetNames;

        public IntroCarouselVideoAssetLoader(IntroCarouselRessourceKind ressourceKind)
            : base(ressourceKind)
        {
            if (ressourceKind == IntroCarouselRessourceKind.Video)
            {
                _videoAssetNames = new string[4]
                {
                    "1_1",
                    "2_1",
                    "3_1",
                    "4_1"
                };
                return;
            }

            throw new Exception("The ressource kind is not expected in this context");
        }

        public override bool IsAssetsLoaded()
        {
            return _cachedVideos != null && _cachedRenderTextures != null;
        }

        public override VideoClip GetVideoClipForSlide(IntroCarouselSlideNumber slideNumber)
        {
            if (_cachedVideos == null)
                throw new Exception("Video are not loaded. Call LoadBundle then LoadVideosAsset before");
            return _cachedVideos[slideNumber];
        }

        public override RenderTexture GetVideoTextureForSlide(IntroCarouselSlideNumber slideNumber)
        {
            if (_cachedRenderTextures == null)
                throw new Exception("RenderTextures are not loaded. Call LoadBundle then LoadVideosAsset before");
            return _cachedRenderTextures[slideNumber];
        }

        public override Sprite GetSpriteForSlide(IntroCarouselSlideNumber slideNumber)
        {
            throw new Exception("IntroCarouselVideoAssetLoader can not load image assets");
        }

        public override IEnumerator LoadAsset()
        {
            if (IsBundleLoaded())
            {
                var requestVideos = (from c in _videoAssetNames
                    select _cachedAssetBundle.LoadAssetAsync<VideoClip>(c)).ToArray();
                var requestTextureVideo = _cachedAssetBundle.LoadAssetAsync<RenderTexture>("RenderTexture");
                var array = requestVideos;
                for (var i = 0; i < array.Length; i++) yield return array[i];
                yield return requestTextureVideo;
                _cachedVideos = new Dictionary<IntroCarouselSlideNumber, VideoClip>();
                _cachedRenderTextures = new Dictionary<IntroCarouselSlideNumber, RenderTexture>();
                _cachedVideos[IntroCarouselSlideNumber.one] = requestVideos[0].asset as VideoClip;
                _cachedVideos[IntroCarouselSlideNumber.two] = requestVideos[1].asset as VideoClip;
                _cachedVideos[IntroCarouselSlideNumber.three] = requestVideos[2].asset as VideoClip;
                _cachedVideos[IntroCarouselSlideNumber.four] = requestVideos[3].asset as VideoClip;
                var renderTexture = requestTextureVideo.asset as RenderTexture;
                var copy5 = new RenderTexture(renderTexture);
                copy5.Create();
                var copy4 = new RenderTexture(renderTexture);
                copy4.Create();
                var copy3 = new RenderTexture(renderTexture);
                copy3.Create();
                _cachedRenderTextures[IntroCarouselSlideNumber.one] = renderTexture;
                _cachedRenderTextures[IntroCarouselSlideNumber.two] = copy5;
                _cachedRenderTextures[IntroCarouselSlideNumber.three] = copy4;
                _cachedRenderTextures[IntroCarouselSlideNumber.four] = copy3;
            }
        }
    }
}