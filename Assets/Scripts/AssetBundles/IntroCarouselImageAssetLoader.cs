using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Video;

namespace AssetBundles
{
    public class IntroCarouselImageAssetLoader : IntroCarouselAssetLoader
    {
        private const string kImageControl1AssetName = "1_1";

        private const string kImageControl2AssetName = "2_1";

        private const string kImageControl3AssetName = "3_1";

        private const string kImageControl4AssetName = "4_1";

        private Dictionary<IntroCarouselSlideNumber, Sprite> _cachedImage;

        private readonly string[] _imageAssetNames;

        public IntroCarouselImageAssetLoader(IntroCarouselRessourceKind ressourceKind)
            : base(ressourceKind)
        {
            if (ressourceKind == IntroCarouselRessourceKind.Image)
            {
                _imageAssetNames = new string[4]
                {
                    "1_1",
                    "2_1",
                    "3_1",
                    "4_1"
                };
                return;
            }

            throw new Exception("The ressource kind is not expected in this context");
        }

        public override bool IsAssetsLoaded()
        {
            return _cachedImage != null;
        }

        public override VideoClip GetVideoClipForSlide(IntroCarouselSlideNumber slideNumber)
        {
            throw new Exception("IntroCarouselImageAssetLoader can not load video assets");
        }

        public override RenderTexture GetVideoTextureForSlide(IntroCarouselSlideNumber slideNumber)
        {
            throw new Exception("IntroCarouselImageAssetLoader can not load video rendre texture assets");
        }

        public override Sprite GetSpriteForSlide(IntroCarouselSlideNumber slideNumber)
        {
            return _cachedImage[slideNumber];
        }

        public override IEnumerator LoadAsset()
        {
            if (IsBundleLoaded())
            {
                var requestImages = (from c in _imageAssetNames
                    select _cachedAssetBundle.LoadAssetAsync<Sprite>(c)).ToArray();
                var array = requestImages;
                for (var i = 0; i < array.Length; i++) yield return array[i];
                _cachedImage = new Dictionary<IntroCarouselSlideNumber, Sprite>();
                _cachedImage[IntroCarouselSlideNumber.one] = requestImages[0].asset as Sprite;
                _cachedImage[IntroCarouselSlideNumber.two] = requestImages[1].asset as Sprite;
                _cachedImage[IntroCarouselSlideNumber.three] = requestImages[2].asset as Sprite;
                _cachedImage[IntroCarouselSlideNumber.four] = requestImages[3].asset as Sprite;
            }
        }
    }
}