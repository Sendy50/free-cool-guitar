using Song;

public interface INextButtonHandler
{
    void HandleNext(ISong currentSong);
}