using UnityEngine;

[CreateAssetMenu]
public class GameSceneLayout : ScriptableObject
{
    public float perfectPosition;

    public static float ScreenHeight => Camera.main.orthographicSize * 2f;

    public static float ScreenWidth => ScreenHeight * Camera.main.aspect;
}