using UnityEngine.UI.Extensions;
using Utils;

public class HorizontalScrollSnapDecorator : IHorizontalScrollSnapDecorator
{
    public HorizontalScrollSnapDecorator(HorizontalScrollSnap horizontalScrollSnap)
    {
        Precondition.CheckNotNull(horizontalScrollSnap);
        this.horizontalScrollSnap = horizontalScrollSnap;
    }

    public HorizontalScrollSnap horizontalScrollSnap { get; }

    public int GetCurrentPage()
    {
        return horizontalScrollSnap.CurrentPage;
    }

    public void GoToScreen(int screenIndex)
    {
        horizontalScrollSnap.GoToScreen(screenIndex);
    }

    public void UpdateLayout()
    {
        horizontalScrollSnap.UpdateLayout();
    }
}