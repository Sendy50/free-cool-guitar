using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

[Serializable]
public class IntroCarouselPersistence
{
    private const string PersistentFileName = "/introCarousel.dat";

    public bool DidCompleteCarousel;

    public bool HasUserSeenfourthSlide;

    public static IntroCarouselPersistence LoadFromFile()
    {
        if (File.Exists(Application.persistentDataPath + "/introCarousel.dat"))
        {
            var binaryFormatter = new BinaryFormatter();
            using (var serializationStream =
                File.Open(Application.persistentDataPath + "/introCarousel.dat", FileMode.Open))
            {
                return (IntroCarouselPersistence) binaryFormatter.Deserialize(serializationStream);
            }
        }

        return new IntroCarouselPersistence();
    }

    public void SaveToFile()
    {
        var binaryFormatter = new BinaryFormatter();
        using (var serializationStream = File.Create(Application.persistentDataPath + "/introCarousel.dat"))
        {
            binaryFormatter.Serialize(serializationStream, this);
        }
    }
}