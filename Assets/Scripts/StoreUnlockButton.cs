using System;
using UnityEngine;

public class StoreUnlockButton : MonoBehaviour
{
    private void Start()
    {
        // InAppManager inappManager = ApplicationGraph.GetInappManager();
        Action unlockAction = delegate { gameObject.SetActive(false); };
        Action lockAction = delegate { gameObject.SetActive(true); };
        // if (inappManager.IsSubscribe())
        // {
        // 	unlockAction();
        // }
        // else
        // {
        lockAction();
        // }
        // inappManager.OnInAppsLockedDelegate += delegate
        // {
        // 	if (!inappManager.IsSubscribe())
        // 	{
        // 		lockAction();
        // 	}
        // };
        // inappManager.OnInAppsUnlockedDelegate += delegate
        // {
        // 	if (inappManager.IsSubscribe())
        // 	{
        // 		unlockAction();
        // 	}
        // };
    }
}