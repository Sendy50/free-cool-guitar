using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

[Serializable]
public class SelectedChordsListPersistance
{
    public List<IMidiChord> _playableChords = new List<IMidiChord>();

    public int currentChordIndex;

    public static SelectedChordsListPersistance loadFromFile()
    {
        if (File.Exists(Application.persistentDataPath + "/SelectedChords.dat"))
        {
            var binaryFormatter = new BinaryFormatter();
            var serializationStream = File.Open(Application.persistentDataPath + "/SelectedChords.dat", FileMode.Open);
            var temp = (SelectedChordsListPersistance) binaryFormatter.Deserialize(serializationStream);
            serializationStream.Close();
            return temp;
        }

        return new SelectedChordsListPersistance();
    }

    public void saveToFile()
    {
        var binaryFormatter = new BinaryFormatter();
        var serializationStream = File.Create(Application.persistentDataPath + "/SelectedChords.dat");
        binaryFormatter.Serialize(serializationStream, this);
        serializationStream.Close();
    }
}