using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace MWM.Ui.SafeArea
{
    [RequireComponent(typeof(RectTransform))]
    public class SafeAreaContainer : UIBehaviour
    {
        [SerializeField] private CanvasScaler _canvasScaler;

        private bool _isUpdatingMargins;

        protected override void OnRectTransformDimensionsChange()
        {
            base.OnRectTransformDimensionsChange();
            UpdateMargins();
        }

        private void UpdateMargins()
        {
            if (_isUpdatingMargins) return;
            _isUpdatingMargins = true;
            var canvasScaler = _canvasScaler;
            var component = GetComponent<RectTransform>();
            var safeArea = Screen.safeArea;
            if (!(safeArea.width > Screen.width) && !(safeArea.height > Screen.height))
            {
                var xMin = safeArea.xMin;
                var num = Screen.width - safeArea.xMax;
                var yMin = safeArea.yMin;
                var num2 = Screen.height - safeArea.yMax;
                var num3 = canvasScaler.referenceResolution.x / Screen.width;
                var num4 = canvasScaler.referenceResolution.y / Screen.height;
                var x = xMin * num3;
                var num5 = num * num3;
                var num6 = num2 * num4;
                var y = yMin * num4;
                var vector = new Vector2(0f - num5, 0f - num6);
                if (component.offsetMax != vector) component.offsetMax = vector;
                var vector2 = new Vector2(x, y);
                if (component.offsetMin != vector2) component.offsetMin = vector2;
                _isUpdatingMargins = false;
            }
        }
    }
}