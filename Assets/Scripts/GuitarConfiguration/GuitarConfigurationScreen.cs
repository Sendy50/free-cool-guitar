using System;
using System.Collections.Generic;
using GuitarApplication;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.UI;

namespace GuitarConfiguration
{
    public class GuitarConfigurationScreen : MonoBehaviour
    {
        [SerializeField] private Sprite _defaultImageBackground;

        [SerializeField] private Sprite _selectedImageBackground;
        
        [SerializeField] private Sprite _defaultImageBackgroundHand;
  
        [SerializeField] private Sprite _selectedImageBackgroundHand;

        [SerializeField] private Canvas _canvas;

        [SerializeField] private GameObject _unlockAllButton;

        [SerializeField] private Image _leftHandPlayerOptionImage;

        [SerializeField] private Image _rightHandPlayerOptionImage;

        [SerializeField] private GuitarHolder _guitarHolder;

        [SerializeField] public List<GuitarCell> _guitarCells;

        [SerializeField] private Image _arpeggiosGuitarStyleOptionImage;

        [SerializeField] private Image _chordsGuitarStyleOptionImage;

        private ApplicationRouter _applicationRouter;

        private IGuitarSelectionHandler _guitarSelectionHandler;

        private IGuitarPreviewPlayer _previewPlayer;

        // private InAppManager _inAppMananger;

        private UserSettings _userSettings;
       

        public void Awake()
        {
            _applicationRouter = ApplicationGraph.GetApplicationRouter();
            _userSettings = ApplicationGraph.GetUserSettings();
            // _inAppMananger = ApplicationGraph.GetInappManager();
            var musicGamingAudioEngine = ApplicationGraph.GetMusicGamingAudioEngine();
            _previewPlayer = new GuitarPreviewPlayer(musicGamingAudioEngine.GetMultiPlayerUnit().GetPlayer(0));
            var samplingSynthUnit = musicGamingAudioEngine.GetSamplingSynthUnit();
            _guitarSelectionHandler = new GuitarSelectionHandler(_applicationRouter, samplingSynthUnit, _guitarHolder,
                new ErrorDisplayer());
            var preSelectedGuitar = _guitarSelectionHandler.GetPreSelectedGuitar();
            foreach (var guitarCell in _guitarCells)
            {
                var guitarModel = guitarCell.GetGuitarModel();
                guitarCell.SetSelectedGuitar(guitarModel.Equals(preSelectedGuitar));
            }
        }

        public void Show(Camera targetCamera = null)
        {
            if (targetCamera == null)
            {
                _canvas.renderMode = RenderMode.ScreenSpaceOverlay;
            }
            else
            {
                _canvas.renderMode = RenderMode.ScreenSpaceCamera;
                _canvas.worldCamera = targetCamera;
            }

            _canvas.enabled = true;
            SyncUiWithCurrentHandPlayerOption();
            SyncUiWithCurrentGuitarStyleOption();
            SyncUiWithInAppManager();
            // _inAppMananger.OnInAppsLockedDelegate += SyncUiWithInAppManager;
            // _inAppMananger.OnInAppsUnlockedDelegate += SyncUiWithInAppManager;
            foreach (var guitarCell in _guitarCells) guitarCell.OnGuitarCellClicked += OnGuitarCellClicked;
        }

        public void Hide()
        {
            _canvas.enabled = false;
            _previewPlayer.StopPreview();
            // _inAppMananger.OnInAppsLockedDelegate -= SyncUiWithInAppManager;
            // _inAppMananger.OnInAppsUnlockedDelegate -= SyncUiWithInAppManager;
            foreach (var guitarCell in _guitarCells) guitarCell.OnGuitarCellClicked -= OnGuitarCellClicked;
        }

        [UsedImplicitly]
        public void OnCloseClicked()
        {
            _guitarSelectionHandler.CloseAndLoadGuitarIfNeeded();
          
            // if (_applicationRouter.GetPremiumGuitarSelect())
            // {
            //     _applicationRouter.GetStoreManager()._instance.ShowStoreFromClickUnlockAllOnGuitarList();
            // }
            
            // else if()
            // {
            //     
            // }
        }

        [UsedImplicitly]
        public void OnUnlockAllClicked()
        {
            _applicationRouter.RouteToStoreFromClickUnlockAllOnGuitarConfiguration();
        }

        [UsedImplicitly]
        public void OnLeftHandPlayerOptionButtonClicked()
        {
            Debug.LogError("OnRightHandPlayerOptionButtonClicked : " + _userSettings.GetGameOrientation());
            if (_userSettings.GetGameOrientation() != UserSettings.GameOrientation.LeftHanded)
            {
                _userSettings.SetGameOrientation(UserSettings.GameOrientation.LeftHanded);
                SyncUiWithCurrentHandPlayerOption();
            }
        }

        [UsedImplicitly]
        public void OnRightHandPlayerOptionButtonClicked()
        {
            Debug.LogError("OnRightHandPlayerOptionButtonClicked : " + _userSettings.GetGameOrientation());
            if (_userSettings.GetGameOrientation() != UserSettings.GameOrientation.RightHanded)
            {
                _userSettings.SetGameOrientation(UserSettings.GameOrientation.RightHanded);
                SyncUiWithCurrentHandPlayerOption();
            }
        }

        [UsedImplicitly]
        public void OnArpeggiosStyleClicked()
        {
            Debug.LogError("OnArpeggiosStyleClicked : " + _userSettings.GetGuitarStyle());

            if (IAPManager.Instance.IsPrimeMember())
            {
                if (_userSettings.GetGuitarStyle() != UserSettings.GuitarStyle.Arpeggios)
                {
                    _userSettings.SetGuitarStyle(UserSettings.GuitarStyle.Arpeggios);
                    SyncUiWithCurrentGuitarStyleOption();
                }
            }
            else
            {
                _applicationRouter.SetPremiumGuitarSelect(true);
                _chordsGuitarStyleOptionImage.sprite = _defaultImageBackground;
                _arpeggiosGuitarStyleOptionImage.sprite = _selectedImageBackground;
                Debug.LogError("<== Show SOLO MODE Subcription Dialog ==>");
                //IntroManager.Instance.ShowOpenSubChordDialog();
            }
            
            
            
        }

        
        [UsedImplicitly]
        public void OnChordsStyleClicked()
        {
            Debug.LogError("OnChordsStyleClicked : " + _userSettings.GetGuitarStyle());
            if (_userSettings.GetGuitarStyle() != UserSettings.GuitarStyle.Chords)
            {
                _userSettings.SetGuitarStyle(UserSettings.GuitarStyle.Chords);
                SyncUiWithCurrentGuitarStyleOption();
            }
        }

        private void SyncUiWithCurrentHandPlayerOption()
        {
            switch (_userSettings.GetGameOrientation())
            {
                case UserSettings.GameOrientation.LeftHanded:
                    _rightHandPlayerOptionImage.sprite = _defaultImageBackgroundHand;
                    _leftHandPlayerOptionImage.sprite = _selectedImageBackgroundHand;
                    break;
                case UserSettings.GameOrientation.RightHanded:
                    _rightHandPlayerOptionImage.sprite = _selectedImageBackgroundHand;
                    _leftHandPlayerOptionImage.sprite = _defaultImageBackgroundHand;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void SyncUiWithCurrentGuitarStyleOption()
        {
            switch (_userSettings.GetGuitarStyle())
            {
                case UserSettings.GuitarStyle.Arpeggios:
                    _chordsGuitarStyleOptionImage.sprite = _defaultImageBackground;
                    _arpeggiosGuitarStyleOptionImage.sprite = _selectedImageBackground;
                    break;
                case UserSettings.GuitarStyle.Chords:
                    _chordsGuitarStyleOptionImage.sprite = _selectedImageBackground;
                    _arpeggiosGuitarStyleOptionImage.sprite = _defaultImageBackground;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void SyncUiWithInAppManager(string inappId = null)
        {
            _unlockAllButton.gameObject.SetActive(!IAPManager.Instance.IsPrimeMember());
            // _unlockAllButton.gameObject.SetActive(true);
        }

        
        
        private void OnGuitarCellClicked(IGuitar guitar)
        {
            _previewPlayer.PlayPreviewForGuitar(guitar);
            if (_guitarSelectionHandler.HandleGuitarSelection(guitar))
            {
                foreach (var guitarCell in _guitarCells)
                {
                    var guitarModel = guitarCell.GetGuitarModel();
                    guitarCell.SetSelectedGuitar(guitarModel.Equals(guitar));
                }
            }
            else
            {
                foreach (var guitarCell in _guitarCells)
                {
                    var guitarModel = guitarCell.GetGuitarModel();
                    guitarCell.SetSelectedGuitar(guitarModel.Equals(guitar));
                }
            }
        }
    }
}