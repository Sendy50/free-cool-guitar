using UnityEngine;

namespace MWM.Core.Utils
{
    public class WaitForSecondsRealtime : CustomYieldInstruction
    {
        private readonly float _wakeUpTime;

        public WaitForSecondsRealtime(float timeToWaitFor)
        {
            _wakeUpTime = Time.realtimeSinceStartup + timeToWaitFor;
        }

        public override bool keepWaiting => Time.realtimeSinceStartup < _wakeUpTime;
    }
}