using AppScreen;
using GuitarApplication;
using JetBrains.Annotations;
using TMPro;
using UnityEngine;

namespace Splashstore
{
    public class SplashstoreScreenBehavior : ScreenBehaviour, SplashstoreView
    {
        [SerializeField] private GameObject _androidView;

        [SerializeField] private GameObject _iOSView;

        [SerializeField] private TextMeshProUGUI _iOSPriceText;

        [SerializeField] private TextMeshProUGUI _iOSTosText;

        [SerializeField] private TextMeshProUGUI _iosNoThanksButtonText;

        [SerializeField] private TextMeshProUGUI _androidNoThanksButtonText;

        private SplashstorePresenter _presenter;

        private void Awake()
        {
            CreatePresenter();
        }

        private void OnDestroy()
        {
            _presenter.Destroy();
        }

        public void DisplayiOSPrice(string price)
        {
            _iOSPriceText.text = price;
        }

        public void DisplayiOSView(string tos, string noThanksText)
        {
            _iOSView.SetActive(true);
            _androidView.SetActive(false);
            _iOSTosText.text = tos;
            _iosNoThanksButtonText.text = noThanksText;
        }

        public void DisplayAndroidView(string noThanksText)
        {
            _androidView.SetActive(true);
            _iOSView.SetActive(false);
            _androidNoThanksButtonText.text = noThanksText;
        }

        public void HideView()
        {
            Hide();
        }

        public void ShowSplashStore(ScreenTransitionOrientation orientation)
        {
            _presenter.ShowScreen();
            base.Show(orientation);
        }

        [UsedImplicitly]
        public void OnBuyButtonClicked()
        {
            _presenter.OnBuyButtonClicked();
        }

        public void OnBackButtonClicked()
        {
            _presenter.OnBackButtonClicked();
        }

        private void CreatePresenter()
        {
            _presenter = new SplashstorePresenter(this, ApplicationGraph.GetInappManager(),
                ApplicationGraph.GetGameEventSender(), ApplicationGraph.GetDeviceManager(),
                ApplicationGraph.GetLocalizationManager(), ApplicationGraph.GetSubscriptionManager());
        }
    }
}