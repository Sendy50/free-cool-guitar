using System;
using System.Globalization;
using Devices;
using i18n;
using MWM.GameEvent;
using Store;
using Subscription;
using Utils;

namespace Splashstore
{
    public class SplashstorePresenter
    {
        private readonly DeviceManager _deviceManager;

        private readonly IGameEventSender _event;

        //private readonly InAppManager _inAppManager;

        private readonly LocalizationManager _localizationManager;

        private readonly SubscriptionManager _subscriptionManager;
        private readonly SplashstoreView _view;

        private string _inAppToBuy;

        public SplashstorePresenter(SplashstoreView view, InAppManager inAppManager, IGameEventSender eventSender,
            DeviceManager deviceManager, LocalizationManager localizationManager,
            SubscriptionManager subscriptionManager)
        {
            Precondition.CheckNotNull(view);
            // Precondition.CheckNotNull(inAppManager);
            Precondition.CheckNotNull(eventSender);
            Precondition.CheckNotNull(deviceManager);
            Precondition.CheckNotNull(localizationManager);
            // Precondition.CheckNotNull(subscriptionManager);
            _view = view;
            // _inAppManager = inAppManager;
            _event = eventSender;
            _deviceManager = deviceManager;
            _localizationManager = localizationManager;
            // _subscriptionManager = subscriptionManager;
        }

        public void Destroy()
        {
            //_inAppManager.OnInAppInitializedDelegate -= OnInAppManagerInitialized;
        }

        public void ShowScreen()
        {
            // _event.SendEvent(EventType.SplashStoreDisplay, string.Empty);
            // if (_inAppManager.GetStatus() == Status.Initialized)
            // {
            //     _view.DisplayiOSPrice("----");
            //     OnInAppManagerInitialized();
            // }
            // else
            // {
            //     _inAppManager.OnInAppInitializedDelegate += OnInAppManagerInitialized;
            // }

            if (_deviceManager.GetDeviceTarget() == DeviceTarget.Ios)
                _view.DisplayiOSView(_localizationManager.GetLocalizedValue("store_apple_terms_of_user"),
                    _localizationManager.GetLocalizedValue("splash_store_negative"));
            else
                _view.DisplayAndroidView(_localizationManager.GetLocalizedValue("splash_store_negative"));
        }

        public void OnBuyButtonClicked()
        {
            if (_inAppToBuy != null)
            {
                // var inAppData = _inAppManager.Buy(_inAppToBuy, BuyCallback);
                // if (inAppData != null)
                // {
                //     var clickBuyEvent = new ClickBuyEvent(ClickBuyEvent.Sources.InHouseAdsStore, inAppData.ProductId,
                //         inAppData.CurrencyCode, inAppData.Price);
                //     _event.SendEvent(EventType.ClickBuy, clickBuyEvent.ToJsonString());
                // }
            }
        }

        private void BuyCallback(Payment.Result paymentResult)
        {
            if (paymentResult.IsSuccess)
                _view.HideView();
            // _event.SendEvent(EventType.SplashStoreDismiss, _inAppToBuy);
        }

        public void OnBackButtonClicked()
        {
            // _event.SendEvent(EventType.SplashStoreDismiss, StoreDismissReason.CloseButton.ToString());
            _view.HideView();
        }

        private void OnInAppManagerInitialized()
        {
            _inAppToBuy = _subscriptionManager.GetSku();
            var subscriptionDetails = _subscriptionManager.GetSubscriptionDetails();
            // var productDetails = _inAppManager.ProductDetailsForId(_inAppToBuy);
            // var str = CreateSubscriptionRealInitialPriceText(productDetails.GetDecimalPrice(),
            //     productDetails.GetIsoCurrencyCode(), new decimal(0.5f));
            // var str2 = "<color=#FF0000><s><b>" + str + "</b></s></color>";
            // _view.DisplayiOSPrice(string.Format(
                // _localizationManager.GetLocalizedValue(
                //     GetPriceStringIdAccordingToSubscriptionDuration(subscriptionDetails.GetSubscriptionDuration())),
                // str2 + " " + productDetails.GetReadablePrice()));
        }

        public static string CreateSubscriptionRealInitialPriceText(decimal localizedPrice, string isoCurrencyCode,
            decimal promotionPercent)
        {
            var amount = localizedPrice / (1m - promotionPercent);
            return FormatCurrency(amount, isoCurrencyCode);
        }

        private static string FormatCurrency(decimal amount, string currencyCode)
        {
            var cultures = CultureInfo.GetCultures(CultureTypes.SpecificCultures);
            var array = cultures;
            foreach (var cultureInfo in array)
            {
                var regionInfo = new RegionInfo(cultureInfo.LCID);
                if (string.Equals(regionInfo.ISOCurrencySymbol, currencyCode, StringComparison.CurrentCultureIgnoreCase)
                ) return string.Format(cultureInfo, "{0:C2}", amount);
            }

            return amount.ToString("0.00");
        }

        private string GetPriceStringIdAccordingToSubscriptionDuration(SubscriptionDuration duration)
        {
            switch (duration)
            {
                case SubscriptionDuration.Weekly:
                    return "splash_store_price_weekly";
                case SubscriptionDuration.Monthly:
                    return "splash_store_monthly";
                case SubscriptionDuration.Annually:
                    return "splash_store_yearly";
                default:
                    throw new ArgumentOutOfRangeException("duration", duration, null);
            }
        }
    }
}