namespace Splashstore
{
    public interface SplashstoreView
    {
        void DisplayiOSPrice(string price);

        void DisplayiOSView(string tos, string noThanksTextButton);

        void DisplayAndroidView(string noThanksTextButton);

        void HideView();
    }
}