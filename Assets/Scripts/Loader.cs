using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class Loader : MonoBehaviour
{
    private const float rotationAngleCoeff = -30f;

    [SerializeField] private Sprite _loaderSprite;

    private Coroutine _rotationRoutine;

    private bool _isDisplayed => _rotationRoutine != null;

    private void Awake()
    {
        _rotationRoutine = null;
    }

    public void SetDisplay(bool display)
    {
        if (_isDisplayed != display)
        {
            if (display)
            {
                AddImageComponent(_loaderSprite);
                _rotationRoutine = StartCoroutine(RotationRoutine());
            }
            else
            {
                StopCoroutine(_rotationRoutine);
                _rotationRoutine = null;
                Destroy(GetImage());
            }
        }
    }

    private IEnumerator RotationRoutine()
    {
        var image = GetImage();
        if (image == null) yield break;
        var delay = new WaitForSeconds(0.05f);
        while (true)
        {
            image.transform.Rotate(0f, 0f, -30f, Space.Self);
            yield return delay;
        }
    }

    private Image GetImage()
    {
        return gameObject.GetComponent<Image>();
    }

    private void AddImageComponent(Sprite sprite)
    {
        gameObject.AddComponent<Image>();
        GetImage().sprite = _loaderSprite;
    }
    
    
}