using System;
using System.Collections.Generic;
using Config;
using GuitarApplication;
using IntroCarouselRessources;
using MWM.GameEvent;
using Store;
using Utils;
using Object = UnityEngine.Object;

public class IntroCarouselAdapter : IIntroCarousel
{
    private readonly IntroCarousel _introCarousel;

    private List<IRessourceApplyer> _ressouceApplyers;

    public IntroCarouselAdapter(ScreenInstanceLocator screenInstanceLocator, InAppManager inappManager,
        ApplicationStatusHolder appStatusHolder, IGameEventSender eventSender,
        ConfigStorageContainer.ConfigStorage configStorage)
    {
        Precondition.CheckNotNull(screenInstanceLocator);
        Precondition.CheckNotNull(inappManager);
        Precondition.CheckNotNull(appStatusHolder);
        Precondition.CheckNotNull(eventSender);
        Precondition.CheckNotNull(configStorage);
        _introCarousel = screenInstanceLocator.GetIntroCarousel();
        _introCarousel.inappManager = inappManager;
        _introCarousel.appStatusHolder = appStatusHolder;
        _introCarousel.gameEventSender = eventSender;
        _introCarousel.configStorage = configStorage;
        _introCarousel.gameObject.SetActive(false);
        _introCarousel.ExitCarouselDelegate += OnExitCarousel;
    }

    public event Action<IIntroCarousel> ExitCarouselEvent;

    public void Display(IRessoucesProvider ressoucesProvider)
    {
        _ressouceApplyers = new List<IRessourceApplyer>();
        for (var i = 0; i < IntroCarousel.MaxNumberOfPage; i++)
        {
            var aBTestRessourceApplyerBuilder = new ABTestRessourceApplyerBuilder();
            var ressourceApplyer =
                aBTestRessourceApplyerBuilder.Build(ressoucesProvider, _introCarousel.GetSlideAtIndex(i));
            ressourceApplyer.ApplyRessouce();
            _ressouceApplyers.Add(ressourceApplyer);
        }

        if (ApplicationGraph.GetConfigStorage().GetConfig().HasWonSubscriptionValue)
            _introCarousel.SwitchToPremiumVersionForFree();
        _introCarousel.Display();
    }

    public void Dispose()
    {
        if (_introCarousel != null)
        {
            _introCarousel.gameObject.SetActive(false);
            Object.Destroy(_introCarousel.gameObject);
        }

        if (_ressouceApplyers != null)
        {
            _ressouceApplyers.Clear();
            _ressouceApplyers = null;
        }
    }

    private void OnExitCarousel(IntroCarousel sender)
    {
        if (ExitCarouselEvent != null) ExitCarouselEvent(this);
    }
}