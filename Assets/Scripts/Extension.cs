using System;
using System.Linq;
using UnityEngine;
using Object = UnityEngine.Object;

public static class Extension
{
    public static bool IsSolo(this MidiChord chord)
    {
        return chord.ToChord().IsSolo;
    }

    public static bool IsSolo(this IMidiChord chord)
    {
        return chord.ToChord().GetIsSolo();
    }

    public static bool IsEmptyChord(this IMidiChord chord)
    {
        return chord.GetHashCode() == ChordMap.emptyChordMidiNumbersHash;
    }

    public static Chord ToChord(this MidiChord chord)
    {
        return ChordMap.ChordFromMidi(chord);
    }

    public static MidiChord ToMidiChord(this Chord chord)
    {
        return ChordMap.ChordToMidi(chord);
    }

    public static IChord ToChord(this IMidiChord chord)
    {
        return ChordMap.ChordFromMidi(chord);
    }

    public static IMidiChord ToMidiChord(this IChord chord)
    {
        return ChordMap.ChordToMidi(chord);
    }

    public static ChordUI UI(this IChord chord, ChordUI prefab, GameObject parent)
    {
        var chordUI = Object.Instantiate(prefab, parent.transform);
        chordUI.hash = chord.GetHash();
        var suffix = chord.GetSuffix();
        if (string.IsNullOrEmpty(suffix))
        {
            chordUI.chordName.gameObject.SetActive(false);
            chordUI.chordSubName.gameObject.SetActive(false);
            chordUI.chordName.text = string.Empty;
            chordUI.chordSubName.text = string.Empty;
            chordUI.withoutSuffixLetter.gameObject.SetActive(true);
            chordUI.withoutSuffixLetter.text = chord.GetKey();
        }
        else
        {
            chordUI.chordName.gameObject.SetActive(true);
            chordUI.chordSubName.gameObject.SetActive(true);
            chordUI.chordName.text = chord.GetKey();
            chordUI.chordSubName.text = chord.GetSuffix();
            chordUI.withoutSuffixLetter.text = string.Empty;
            chordUI.withoutSuffixLetter.gameObject.SetActive(false);
        }

        return chordUI;
    }

    public static IUserActionValidator Validator(this IUserAction action, MonoBehaviour monoBehaviour,
        GameplayParameters gameplayParameters, MusicSheetReader musicSheetReader)
    {
        if (action is StrockUserAction)
        {
            return (action as StrockUserAction).Validator(monoBehaviour, gameplayParameters, musicSheetReader);
        }

        if (action is StrumUserAction)
        {
            return (action as StrumUserAction).Validator(monoBehaviour, gameplayParameters, musicSheetReader);
        }

        return null;
    }

    public static IUserActionValidator Validator(this StrockUserAction action, MonoBehaviour monoBehaviour,
        GameplayParameters gameplayParameters, MusicSheetReader musicSheetReader)
    {
        var interleave =
            new GameTimeInterleave(
                new Vector2(action.time - gameplayParameters.catchTolerance * 0.5f,
                    action.time + gameplayParameters.catchTolerance * 0.5f), musicSheetReader.GameProgression);
        var strokValidator = new StrokValidator(monoBehaviour);
        strokValidator.SetupDetection(action.note.StringIndex, interleave);
        return strokValidator;
    }

    public static IUserActionValidator Validator(this StrumUserAction action, MonoBehaviour monoBehaviour,
        GameplayParameters gameplayParameters, MusicSheetReader musicSheetReader)
    {
        var interleave =
            new GameTimeInterleave(
                new Vector2(action.time - gameplayParameters.catchTolerance * 0.5f,
                    action.time + gameplayParameters.catchTolerance * 0.5f), musicSheetReader.GameProgression);
        var strumValidator = new StrumValidator(monoBehaviour);
        var array = action.notes.Select(c => c.StringIndex).ToArray();
        strumValidator.SetupDetection(array, interleave, Math.Max(2, array.Length >> 1));
        return strumValidator;
    }
}