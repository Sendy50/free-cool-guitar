using System;
using System.Collections;
using UnityEngine;

namespace MWM.Audio.Internal.UnityEngine
{
    public static class UnityEngineFactory
    {
        public static void Create(MonoBehaviour monoBehaviour, int nbPlayers, int nbSamples, int preferredNbTracks,
            Action<MusicGamingAudioEngine> completion)
        {
            monoBehaviour.StartCoroutine(FakeAsyncCreationJob(monoBehaviour, nbPlayers, nbSamples, preferredNbTracks,
                completion));
        }

        private static IEnumerator FakeAsyncCreationJob(MonoBehaviour monoBehaviour, int nbPlayers, int nbSamples,
            int preferredNbTracks, Action<MusicGamingAudioEngine> completion)
        {
            yield return new WaitForSeconds(1.5f);
            var multiPlayerUnit = nbPlayers <= 0 ? null : new UnityEngineMultiPlayer(monoBehaviour, nbPlayers);
            var samplerUnit = nbSamples <= 0 ? null : new UnityEngineSamplerUnit(monoBehaviour);
            var samplingSynthUnit = preferredNbTracks <= 0
                ? null
                : new UnityEngineSamplingSynthUnit(monoBehaviour, preferredNbTracks, 6);
            var musicGamingAudioEngineImpl =
                new MusicGamingAudioEngineImpl(multiPlayerUnit, samplerUnit, samplingSynthUnit);
            completion(musicGamingAudioEngineImpl);
        }
    }
}