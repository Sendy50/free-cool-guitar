using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

namespace MWM.Audio.Internal.UnityEngine
{
    public class UnityEngineSamplingSynthUnit : SamplingSynthUnit
    {
        private readonly Dictionary<int, AudioClip> _audioClipCache;

        private readonly UnityEngineSamplingSynthAudioSourcePool _audioSourcesPool;
        private readonly MonoBehaviour _monoBehaviour;

        private string _currentInstrumentPath;

        private bool _internalInstrument;

        public UnityEngineSamplingSynthUnit(MonoBehaviour monoBehaviour, int preferredNbTracks,
            int maxSimultaneousNotes)
        {
            _monoBehaviour = monoBehaviour;
            _audioClipCache = new Dictionary<int, AudioClip>();
            _audioSourcesPool =
                new UnityEngineSamplingSynthAudioSourcePool(_monoBehaviour, preferredNbTracks, maxSimultaneousNotes);
        }

        public void LoadExternal(string diskDirectoryPath, Action<LoadingResult> completion)
        {
            if (diskDirectoryPath == _currentInstrumentPath) completion(LoadingResult.Ok());
            UnloadCurrentInstrument();
            _currentInstrumentPath = diskDirectoryPath;
            _internalInstrument = false;
            var routine = FakeAsyncLoadingJob(completion);
            _monoBehaviour.StartCoroutine(routine);
        }

        public void LoadInternal(string assetDirectoryPath, Action<LoadingResult> completion)
        {
            if (assetDirectoryPath == _currentInstrumentPath) completion(LoadingResult.Ok());
            UnloadCurrentInstrument();
            _currentInstrumentPath = assetDirectoryPath;
            _internalInstrument = true;
            var routine = FakeAsyncLoadingJob(completion);
            _monoBehaviour.StartCoroutine(routine);
        }

        public void NoteOn(int trackIndex, int noteNumber, int velocity)
        {
            var audioSource = GetAudioSource(trackIndex, noteNumber);
            var audioClip = GetAudioClip(noteNumber);
            var volume = velocity / 127f;
            audioSource.clip = audioClip;
            audioSource.volume = volume;
            audioSource.Play();
        }

        public void NoteOff(int trackIndex, int noteNumber)
        {
            var audioSource = GetAudioSource(trackIndex, noteNumber);
            var audioClip = GetAudioClip(noteNumber);
            if (audioSource.clip != audioClip)
                throw new ApplicationException("Can't turn off note " + noteNumber + " for track " + trackIndex +
                                               ". Current note: " + audioClip.name);
            audioSource.Stop();
        }

        public void PedalOn(int trackIndex)
        {
        }

        public void PedalOff(int trackIndex)
        {
        }

        public void StopAllNotes(bool clearAudio)
        {
            _audioSourcesPool.StopAllAudioSources();
        }

        public void SetNumberTracks(int nbTracks)
        {
            _audioSourcesPool.SetNumberTracks(nbTracks);
        }

        private AudioClip GetAudioClip(int noteNumber)
        {
            if (_audioClipCache.ContainsKey(noteNumber)) return _audioClipCache[noteNumber];
            var audioClip = LoadAudioClip(noteNumber);
            _audioClipCache.Add(noteNumber, audioClip);
            return audioClip;
        }

        private AudioClip LoadAudioClip(int noteNumber)
        {
            if (_internalInstrument)
            {
                var path = _currentInstrumentPath + "/" + noteNumber.ToString("D3") + "_127";
                return Resources.Load(path) as AudioClip;
            }

            var text = _currentInstrumentPath + "/" + noteNumber.ToString("D3") + "_127.mp3";
            var wWW = new WWW(text);
            Thread.Sleep(50);
            var audioClip = wWW.GetAudioClip(false, true, AudioType.MPEG);
            audioClip.name = text;
            audioClip.LoadAudioData();
            wWW.Dispose();
            return audioClip;
        }

        private AudioSource GetAudioSource(int trackIndex, int noteNumber)
        {
            return _audioSourcesPool.GetAudioSource(trackIndex, noteNumber);
        }

        private void UnloadCurrentInstrument()
        {
            StopAllNotes(true);
            _audioClipCache.Clear();
            _audioSourcesPool.Reset();
        }

        private IEnumerator FakeAsyncLoadingJob(Action<LoadingResult> completion)
        {
            yield return new WaitForSeconds(1.5f);
            completion(LoadingResult.Ok());
        }
    }
}