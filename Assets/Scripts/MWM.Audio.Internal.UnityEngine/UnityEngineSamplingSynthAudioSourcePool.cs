using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Object = UnityEngine.Object;

namespace MWM.Audio.Internal.UnityEngine
{
    public class UnityEngineSamplingSynthAudioSourcePool
    {
        private readonly int _maxSimultaneousNotes;

        private readonly MonoBehaviour _monoBehaviour;

        private readonly Dictionary<int, List<AudioSourceNoteNumberPair>> _pool;

        private int _nbTracks;

        public UnityEngineSamplingSynthAudioSourcePool(MonoBehaviour monoBehaviour, int nbTracks,
            int maxSimultaneousNotes)
        {
            _pool = new Dictionary<int, List<AudioSourceNoteNumberPair>>();
            _monoBehaviour = monoBehaviour;
            _nbTracks = nbTracks;
            _maxSimultaneousNotes = maxSimultaneousNotes;
            Prepare();
        }

        public AudioSource GetAudioSource(int trackIndex, int noteNumber)
        {
            var audioSourceNoteNumberPair = _pool[trackIndex].Find(element => element.NoteNumber == noteNumber);
            if (audioSourceNoteNumberPair != null) return audioSourceNoteNumberPair.AudioSource;
            var anIdleAudioSource = GetAnIdleAudioSource(trackIndex);
            anIdleAudioSource.NoteNumber = noteNumber;
            return anIdleAudioSource.AudioSource;
        }

        public void StopAllAudioSources()
        {
            foreach (var value in _pool.Values)
            foreach (var item in value)
                if (item.AudioSource.isPlaying)
                    item.AudioSource.Stop();
        }

        public void SetNumberTracks(int nbTracks)
        {
            var i = 1;
            if (nbTracks > _nbTracks)
                for (var num = nbTracks - _nbTracks; i <= num; i++)
                {
                    var value = GenerateTrackAudioSourceList();
                    _pool.Add(_nbTracks + i, value);
                }
            else if (nbTracks < _nbTracks)
                for (var num2 = _nbTracks - nbTracks; i <= num2; i++)
                {
                    var list = _pool[_nbTracks - i];
                    foreach (var item in list) Object.Destroy(item.AudioSource);
                    list.Clear();
                    _pool.Remove(_nbTracks - i);
                }

            _nbTracks = nbTracks;
        }

        public void Reset()
        {
            foreach (var value in _pool.Values)
            {
                foreach (var item in value) Object.Destroy(item.AudioSource);
                value.Clear();
            }

            _pool.Clear();
            Prepare();
        }

        private AudioSourceNoteNumberPair GetAnIdleAudioSource(int trackIndex)
        {
            var audioSourceNoteNumberPair = _pool[trackIndex].Find(audioSource => !audioSource.AudioSource.isPlaying);
            if (audioSourceNoteNumberPair == null)
                throw new Exception(
                    "UnityEngineSamplingSynthAudioSourcePool: no more audio source available. Please increase `maxSimultaneousNotes` at init.");
            return audioSourceNoteNumberPair;
        }

        private void Prepare()
        {
            if (_pool.Keys.Count() <= 0)
                for (var i = 0; i < _nbTracks; i++)
                {
                    var value = GenerateTrackAudioSourceList();
                    _pool.Add(i, value);
                }
        }

        private List<AudioSourceNoteNumberPair> GenerateTrackAudioSourceList()
        {
            var list = new List<AudioSourceNoteNumberPair>();
            for (var i = 0; i < _maxSimultaneousNotes; i++)
            {
                var audioSource = _monoBehaviour.gameObject.AddComponent<AudioSource>();
                audioSource.playOnAwake = false;
                var item = new AudioSourceNoteNumberPair(-1, audioSource);
                list.Add(item);
            }

            return list;
        }

        private class AudioSourceNoteNumberPair
        {
            public readonly AudioSource AudioSource;
            public int NoteNumber;

            public AudioSourceNoteNumberPair(int noteNumber, AudioSource audioSource)
            {
                NoteNumber = noteNumber;
                AudioSource = audioSource;
            }
        }
    }
}