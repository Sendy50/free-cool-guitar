using System;
using UnityEngine;

namespace MWM.Audio.Internal.UnityEngine
{
    public class UnityEngineMultiPlayer : MultiPlayerUnit
    {
        private readonly Player[] _players;

        public UnityEngineMultiPlayer(MonoBehaviour monoBehaviour, int nbPlayers)
        {
            _players = new Player[nbPlayers];
            for (var i = 0; i < nbPlayers; i++)
            {
                var name = "unity-engine-player-" + i;
                var unityEnginePlayer = new UnityEnginePlayer(monoBehaviour, name);
                _players[i] = unityEnginePlayer;
            }
        }

        public Player GetPlayer(int index)
        {
            if (index < 0 || index >= _players.Length)
                throw new ApplicationException("Index must be in range [0;" + _players.Length + "] Found: " + index);
            return _players[index];
        }
    }
}