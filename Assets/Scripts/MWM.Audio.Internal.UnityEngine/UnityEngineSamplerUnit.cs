using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using Object = UnityEngine.Object;

namespace MWM.Audio.Internal.UnityEngine
{
    internal class UnityEngineSamplerUnit : SamplerUnit
    {
        private readonly Dictionary<string, AudioClip> _audioClipCache;

        private readonly Dictionary<string, AudioSource> _loadedAudioSources;
        private readonly MonoBehaviour _monoBehaviour;

        private readonly Dictionary<string, Coroutine> _pendingStopEventCoroutines;

        public UnityEngineSamplerUnit(MonoBehaviour monoBehaviour)
        {
            _monoBehaviour = monoBehaviour;
            _loadedAudioSources = new Dictionary<string, AudioSource>();
            _audioClipCache = new Dictionary<string, AudioClip>();
            _pendingStopEventCoroutines = new Dictionary<string, Coroutine>();
        }

        public event Action<string> SampleStarted;

        public event Action<string> SampleStopped;

        public void Load(SamplerConfiguration configuration, Action<LoadingResult> completion)
        {
            foreach (var loadedAudioSource in _loadedAudioSources)
            {
                var key = loadedAudioSource.Key;
                var value = loadedAudioSource.Value;
                StopSample(key, value);
                Object.Destroy(value);
            }

            _loadedAudioSources.Clear();
            var internalSamples = configuration.GetInternalSamples();
            LoadInternalSamples(internalSamples);
            var externalSamples = configuration.GetExternalSamples();
            LoadExternalSamples(externalSamples);
            var routine = FakeAsyncLoadingJob(completion);
            _monoBehaviour.StartCoroutine(routine);
        }

        public void Start(string sampleId)
        {
            var audioSource = GetAudioSource(sampleId);
            StopSample(sampleId, audioSource);
            StartSample(sampleId, audioSource);
        }

        public void Stop(string sampleId)
        {
            var audioSource = GetAudioSource(sampleId);
            StopSample(sampleId, audioSource);
        }

        public float GetTotalTime(string sampleId)
        {
            var audioSource = GetAudioSource(sampleId);
            return audioSource.clip.length;
        }

        public void StopAll()
        {
            foreach (var loadedAudioSource in _loadedAudioSources)
            {
                var key = loadedAudioSource.Key;
                var value = loadedAudioSource.Value;
                StopSample(key, value);
                Object.Destroy(value);
            }
        }

        private void StartSample(string sampleId, AudioSource audioSource)
        {
            CancelPendingStopEventCoroutine(sampleId);
            SchedulePendingStopEventCoroutine(sampleId, audioSource.clip.length);
            audioSource.Play();
            TriggerSampleStartedEvent(sampleId);
        }

        private void TriggerSampleStartedEvent(string sampleId)
        {
            if (SampleStarted != null) SampleStarted(sampleId);
        }

        private void StopSample(string sampleId, AudioSource audioSource)
        {
            if (audioSource.isPlaying)
            {
                CancelPendingStopEventCoroutine(sampleId);
                audioSource.Stop();
                TriggerSampleStoppedEvent(sampleId);
            }
        }

        private void TriggerSampleStoppedEvent(string sampleId)
        {
            if (SampleStopped != null) SampleStopped(sampleId);
        }

        private void SchedulePendingStopEventCoroutine(string sampleId, float duration)
        {
            var routine = PendingStopEventJob(sampleId, duration);
            var value = _monoBehaviour.StartCoroutine(routine);
            _pendingStopEventCoroutines[sampleId] = value;
        }

        private void CancelPendingStopEventCoroutine(string sampleId)
        {
            if (_pendingStopEventCoroutines.ContainsKey(sampleId))
            {
                var routine = _pendingStopEventCoroutines[sampleId];
                _monoBehaviour.StopCoroutine(routine);
                _pendingStopEventCoroutines.Remove(sampleId);
            }
        }

        private IEnumerator PendingStopEventJob(string sampleId, float duration)
        {
            yield return new WaitForSeconds(duration);
            _pendingStopEventCoroutines.Remove(sampleId);
            TriggerSampleStoppedEvent(sampleId);
        }

        private AudioSource GetAudioSource(string sampleId)
        {
            foreach (var loadedAudioSource in _loadedAudioSources)
            {
                var key = loadedAudioSource.Key;
                if (key != sampleId) continue;
                return loadedAudioSource.Value;
            }

            throw new ApplicationException("No sample loaded for id: " + sampleId);
        }

        private AudioSource CreateAudioSource()
        {
            var audioSource = _monoBehaviour.gameObject.AddComponent<AudioSource>();
            audioSource.playOnAwake = false;
            return audioSource;
        }

        private void LoadInternalSamples(IEnumerable<KeyValuePair<string, string>> internalSamples)
        {
            foreach (var internalSample in internalSamples)
            {
                var key = internalSample.Key;
                var value = internalSample.Value;
                var audioSource = CreateAudioSource();
                if (_audioClipCache.ContainsKey(key))
                {
                    audioSource.clip = _audioClipCache[key];
                }
                else
                {
                    var audioClip = Resources.Load(value) as AudioClip;
                    _audioClipCache[key] = audioClip;
                    audioSource.clip = audioClip;
                }

                _loadedAudioSources[key] = audioSource;
            }
        }

        private void LoadExternalSamples(IEnumerable<KeyValuePair<string, string>> externalSamples)
        {
            foreach (var externalSample in externalSamples)
            {
                var key = externalSample.Key;
                var value = externalSample.Value;
                var audioSource = CreateAudioSource();
                if (_audioClipCache.ContainsKey(key))
                {
                    audioSource.clip = _audioClipCache[key];
                }
                else
                {
                    var wWW = new WWW(value);
                    Thread.Sleep(50);
                    var audioClip = wWW.GetAudioClip(false, true, AudioType.MPEG);
                    audioClip.name = key;
                    audioClip.LoadAudioData();
                    wWW.Dispose();
                    _audioClipCache[key] = audioClip;
                    audioSource.clip = audioClip;
                }

                _loadedAudioSources[key] = audioSource;
            }
        }

        private IEnumerator FakeAsyncLoadingJob(Action<LoadingResult> completion)
        {
            yield return new WaitForSeconds(1.5f);
            completion(LoadingResult.Ok());
        }
    }
}