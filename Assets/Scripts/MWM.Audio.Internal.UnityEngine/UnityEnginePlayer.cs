using System;
using System.Collections;
using System.Threading;
using UnityEngine;

namespace MWM.Audio.Internal.UnityEngine
{
    public class UnityEnginePlayer : Player
    {
        private readonly AudioSource _audioSource;
        private readonly MonoBehaviour _monoBehaviour;

        private readonly string _name;

        private Coroutine _pendingReachedEndOfFileCoroutine;

        public UnityEnginePlayer(MonoBehaviour monoBehaviour, string name)
        {
            _monoBehaviour = monoBehaviour;
            _audioSource = _monoBehaviour.gameObject.AddComponent<AudioSource>();
            _audioSource.playOnAwake = false;
            _name = name;
        }

        public event Action<Player, bool> PlayingStateChanged;

        public event Action<Player> EndOfFileReached;

        public void LoadExternal(string diskPath, Action<LoadingResult> completion)
        {
            Stop();
            var wWW = new WWW(diskPath);
            Thread.Sleep(50);
            var audioClip = wWW.GetAudioClip(false, true, AudioType.MPEG);
            audioClip.name = _name + "(" + diskPath + ")";
            audioClip.LoadAudioData();
            wWW.Dispose();
            _audioSource.clip = audioClip;
            var routine = FakeAsyncLoadingJob(completion);
            _monoBehaviour.StartCoroutine(routine);
        }

        public void LoadInternal(string resourcePath, Action<LoadingResult> completion)
        {
            Stop();
            var audioClip = Resources.Load(resourcePath) as AudioClip;
            audioClip.name = _name + " (" + audioClip.name + ")";
            _audioSource.clip = audioClip;
            var routine = FakeAsyncLoadingJob(completion);
            _monoBehaviour.StartCoroutine(routine);
        }

        public void Play()
        {
            if (!_audioSource.isPlaying)
            {
                CancelPendingReachedEndOfFileCoroutine();
                _audioSource.Play();
                TriggerPlayingStateChanged(true);
                SchedulePendingReachedEndOfFileJob();
            }
        }

        public void Pause()
        {
            if (_audioSource.isPlaying)
            {
                CancelPendingReachedEndOfFileCoroutine();
                _audioSource.Pause();
                TriggerPlayingStateChanged(false);
            }
        }

        public void Stop()
        {
            var isPlaying = _audioSource.isPlaying;
            CancelPendingReachedEndOfFileCoroutine();
            _audioSource.Stop();
            if (isPlaying) TriggerPlayingStateChanged(false);
        }

        public bool IsPlaying()
        {
            return _audioSource.isPlaying;
        }

        public float GetTotalTime()
        {
            return _audioSource.clip.length;
        }

        public float GetCurrentTime()
        {
            return _audioSource.time;
        }

        public void Seek(float time)
        {
            _audioSource.time = time;
        }

        public void SetVolume(float newVolume)
        {
            _audioSource.volume = newVolume;
        }

        public float GetVolume()
        {
            return _audioSource.volume;
        }

        public void SetSpeed(float newSpeed)
        {
            _audioSource.pitch = newSpeed;
        }

        public float GetSpeed()
        {
            return _audioSource.pitch;
        }

        public void SetRepeat(bool enable)
        {
            _audioSource.loop = enable;
        }

        public bool IsRepeat()
        {
            return _audioSource.loop;
        }

        private void TriggerPlayingStateChanged(bool isPlaying)
        {
            if (PlayingStateChanged != null) PlayingStateChanged(this, isPlaying);
        }

        private void CancelPendingReachedEndOfFileCoroutine()
        {
            if (_pendingReachedEndOfFileCoroutine != null)
            {
                _monoBehaviour.StopCoroutine(_pendingReachedEndOfFileCoroutine);
                _pendingReachedEndOfFileCoroutine = null;
            }
        }

        private void SchedulePendingReachedEndOfFileJob()
        {
            var time = _audioSource.time;
            var length = _audioSource.clip.length;
            var duration = length - time;
            var routine = PendingReachedEndOfFileJob(duration);
            _pendingReachedEndOfFileCoroutine = _monoBehaviour.StartCoroutine(routine);
        }

        private IEnumerator PendingReachedEndOfFileJob(float duration)
        {
            yield return new WaitForSeconds(duration);
            _pendingReachedEndOfFileCoroutine = null;
            if (_audioSource.loop)
            {
                TriggerReachedEndOfFileEvent();
                SchedulePendingReachedEndOfFileJob();
            }
            else
            {
                TriggerPlayingStateChanged(false);
                TriggerReachedEndOfFileEvent();
            }
        }

        private void TriggerReachedEndOfFileEvent()
        {
            if (EndOfFileReached != null) EndOfFileReached(this);
        }

        private static IEnumerator FakeAsyncLoadingJob(Action<LoadingResult> completion)
        {
            yield return new WaitForSeconds(1.5f);
            completion(LoadingResult.Ok());
        }
    }
}