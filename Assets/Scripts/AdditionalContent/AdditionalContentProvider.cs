using System;
using System.Collections.Generic;
using Patch;

namespace AdditionalContent
{
    public interface AdditionalContentProvider
    {
        List<ExternalSong> GetAdditionalSongs();

        List<string> GetSongsOrder();

        void SetSongsOrder(List<string> songsOrder);

        List<string> GetNewSongs();

        void SetNewSongs(List<string> newSongs);

        void SetAdditionalPatchData(PatchDataList patchDataList, Action<bool> callback);
    }
}