using System;
using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;

namespace AdditionalContent
{
    [Serializable]
    public class RawConfig
    {
        [UsedImplicitly] [SerializeField] private string configVersion;

        [UsedImplicitly] [SerializeField] private List<string> songsOrder;

        [UsedImplicitly] [SerializeField] private List<string> newSongs;

        public string GetConfigVersion()
        {
            return configVersion;
        }

        public List<string> GetSongsOrder()
        {
            return songsOrder;
        }

        public List<string> GetNewSongs()
        {
            return newSongs;
        }

        public static RawConfig MapDataToRawConfig(string rawConfigStr)
        {
            return JsonUtility.FromJson<RawConfig>(rawConfigStr);
        }
    }
}