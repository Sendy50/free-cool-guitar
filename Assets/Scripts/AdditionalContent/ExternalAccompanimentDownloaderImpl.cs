using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Async;
using MWM.GameEvent;
using Song;
using UnityEngine;
using UnityEngine.Networking;
using Utils;
using EventType = MWM.GameEvent.EventType;

namespace AdditionalContent
{
    public class ExternalAccompanimentDownloaderImpl : ExternalAccompanimentDownloader
    {
        private const float WaitRequestTimeout = 9f;

        public const string AdditionalContentVersion = "1";

        private readonly AsyncJobExecutor _asyncJobExecutor;

        private readonly IGameEventSender _eventManager;

        private readonly string _songDirectoryPath;

        public ExternalAccompanimentDownloaderImpl(AsyncJobExecutor asyncJobExecutor, IGameEventSender eventManager)
        {
            Precondition.CheckNotNull(asyncJobExecutor);
            Precondition.CheckNotNull(eventManager);
            _asyncJobExecutor = asyncJobExecutor;
            _eventManager = eventManager;
            _songDirectoryPath = Application.persistentDataPath + "/Song/";
        }

        public event Action<string> InfoMessageChanged;

        public void GetExternalSongs(List<Song.Song> songs, Action<bool, List<ExternalSong>> callback)
        {
            var externalSongs = new List<ExternalSong>();
            var list = new List<ISong>();
            var count = songs.Count;
            for (var i = 0; i < count; i++)
            {
                var song = songs[i];
                var text = _songDirectoryPath + song.GetId();
                if (Directory.Exists(text))
                    externalSongs.Add(GenerateExternalSong(text, song));
                else
                    list.Add(song);
            }

            if (list.Count == 0)
            {
                if (callback != null) callback(true, externalSongs);
                return;
            }

            var asyncJob = new AsyncJob(DownloadAccompaniment(list, externalSongs, callback),
                delegate(bool isNormallyFinished)
                {
                    if (!isNormallyFinished && callback != null) callback(false, externalSongs);
                });
            _asyncJobExecutor.Execute(asyncJob);
        }

        private IEnumerator DownloadAccompaniment(List<ISong> songsToDownload, List<ExternalSong> songsAlreadyOnDisk,
            Action<bool, List<ExternalSong>> callback)
        {
            var downloadSucceed = true;
            var numberOfPacksToDownload = songsToDownload.Count;
            for (var i = 0; i < numberOfPacksToDownload; i++)
            {
                NotifyInfoMessageChanged("Loading songs " + (i + 1) + "/" + numberOfPacksToDownload + string.Empty);
                var song = songsToDownload[i];
                var contentName = GetContentName(song);
                var url = AdditionalContentEndPoint.GenerateUrl(AdditionalContentEndPoint.ContentGroup.Accompaniment,
                    "1", contentName);
                var request = UnityWebRequest.Get(url);
                request.SendWebRequest();
                var waitRequestInstruction = new WaitForSeconds(0.25f);
                var waitRequestTime = 0f;
                while (!request.isDone && waitRequestTime < 9f)
                {
                    yield return waitRequestInstruction;
                    waitRequestTime += 0.25f;
                }

                if (!request.isDone)
                {
                    LogError("Request timed out while downloading song " + song.GetId());
                    downloadSucceed = false;
                    request.Dispose();
                    continue;
                }

                if (request.isNetworkError || request.isHttpError)
                {
                    LogError("Error while downloading song " + song.GetId() + " error: " + request.error);
                    downloadSucceed = false;
                    continue;
                }

                var accompanimentDirectoryPath = _songDirectoryPath + song.GetId();
                if (SaveAccompanimentFile(request.downloadHandler.data, accompanimentDirectoryPath,
                    GetContentName(song)))
                    songsAlreadyOnDisk.Add(GenerateExternalSong(accompanimentDirectoryPath, song));
                else
                    downloadSucceed = false;
            }

            RemoveOldSongs(songsAlreadyOnDisk);
            callback?.Invoke(downloadSucceed, songsAlreadyOnDisk);
        }

        private void RemoveOldSongs(IEnumerable<ExternalSong> externalSongs)
        {
            if (!Directory.Exists(_songDirectoryPath)) return;
            var directories = Directory.GetDirectories(_songDirectoryPath);
            var list = externalSongs.Select(song => _songDirectoryPath + song.GetId()).ToList();
            var array = directories;
            foreach (var text in array)
                if (!list.Contains(text) && Directory.Exists(text))
                    Directory.Delete(text, true);
        }

        private static bool SaveAccompanimentFile(byte[] songData, string directoryPath, string fileName)
        {
            Directory.CreateDirectory(directoryPath);
            var path = directoryPath + "/" + fileName;
            var memoryStream = new MemoryStream(songData);
            try
            {
                var fileStream = File.Create(path);
                var array = new byte[8192];
                int count;
                while ((count = memoryStream.Read(array, 0, array.Length)) > 0) fileStream.Write(array, 0, count);
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }

        private void LogError(string message)
        {
            Debug.LogError(message);
            _eventManager.SendEvent(EventType.AccompanimentDownloaderError, message);
        }

        private void NotifyInfoMessageChanged(string infoMessage)
        {
            if (InfoMessageChanged != null) InfoMessageChanged(infoMessage);
        }

        private static ExternalSong GenerateExternalSong(string songPath, ISong song)
        {
            var accompaniment = string.Empty;
            if (!string.IsNullOrEmpty(song.GetAccompanimentName()))
                accompaniment = songPath + "/" + song.GetAccompanimentName() + ".mp3";
            return new ExternalSong(song.GetId(), song.GetJson(), accompaniment, song.GetName(), song.GetArtist(),
                song.GetIsTutorialTrack(), song.GetSongType(), song.GetCatalogVersion());
        }

        private static string GetContentName(ISong song)
        {
            return song.GetAccompanimentName() + ".mp3";
        }
    }
}