using System.Collections.Generic;

namespace AdditionalContent
{
    public interface AdditionalContentProviderStorage
    {
        List<string> ReadSongsOrder();

        void WriteSongsOrder(List<string> songsIds);

        List<string> ReadNewSongs();

        void WriteNewSongs(List<string> newSongsIds);

        void CleanStorage();
    }
}