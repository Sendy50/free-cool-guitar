using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

namespace AdditionalContent
{
    public class AdditionalContentSynchronizerStorageImpl : AdditionalContentSynchronizerStorage
    {
        private static readonly string CurrentconfigVersionDataPath =
            Application.persistentDataPath + "/current_config_version";

        private static readonly string LastSucessfulSyncTimeDataPath =
            Application.persistentDataPath + "/last_successful_sync_time";

        private static readonly string LastSucessfulSyncAppVersionDataPath =
            Application.persistentDataPath + "/last_successful_app_version";

        public void WriteCurrentConfigVersion(string version)
        {
            WriteConfigVersion(version);
        }

        public string ReadCurrentConfigVersion()
        {
            return GetConfigVersion();
        }

        public void WriteLastSuccessfulSyncUtcTime(DateTime lastSuccessfulSyncTime)
        {
            var serializationStream = File.Create(LastSucessfulSyncTimeDataPath);
            var binaryFormatter = new BinaryFormatter();
            binaryFormatter.Serialize(serializationStream, lastSuccessfulSyncTime);
        }

        public DateTime ReadLastSuccessfulSyncUtcTime()
        {
            if (!File.Exists(LastSucessfulSyncTimeDataPath)) return new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            var serializationStream = File.OpenRead(LastSucessfulSyncTimeDataPath);
            var binaryFormatter = new BinaryFormatter();
            return (DateTime) binaryFormatter.Deserialize(serializationStream);
        }

        public void WriteLastSuccessfulSyncAppVersion(string appVersion)
        {
            var serializationStream = File.Create(LastSucessfulSyncAppVersionDataPath);
            var binaryFormatter = new BinaryFormatter();
            binaryFormatter.Serialize(serializationStream, appVersion);
        }

        public string ReadLastSuccessfulSyncAppVersion()
        {
            if (!File.Exists(LastSucessfulSyncAppVersionDataPath)) return string.Empty;
            var serializationStream = File.OpenRead(LastSucessfulSyncAppVersionDataPath);
            var binaryFormatter = new BinaryFormatter();
            return (string) binaryFormatter.Deserialize(serializationStream);
        }

        private static string GetConfigVersion()
        {
            if (!File.Exists(CurrentconfigVersionDataPath)) return string.Empty;
            var serializationStream = File.OpenRead(CurrentconfigVersionDataPath);
            var binaryFormatter = new BinaryFormatter();
            return (string) binaryFormatter.Deserialize(serializationStream);
        }

        private static void WriteConfigVersion(string version)
        {
            var serializationStream = File.Create(CurrentconfigVersionDataPath);
            var binaryFormatter = new BinaryFormatter();
            binaryFormatter.Serialize(serializationStream, version);
        }
    }
}