using System;
using System.Collections;
using System.Security.Cryptography;
using System.Text;
using GuitarApplication;
using JetBrains.Annotations;
using Patch;
using UnityEngine;
using UnityEngine.Networking;
using Utils;

namespace AdditionalContent
{
    public class AdditionalContentSynchronizerImpl : MonoBehaviour, AdditionalContentSynchronizer
    {
        private const string PatchDataConfigContentName = "patch-data-config-android.json";

        private const string AssetBundleContentName = "asset-bundle-{0}-{1}";

        private const int WaitConfigTimeout = 5;

        private const float WaitAssetBundleTimeout = 6f;

        private AdditionalContentSynchronizerController _controller;

        private void Awake()
        {
            var storage = new AdditionalContentSynchronizerStorageImpl();
            var additionalContentProvider = ApplicationGraph.GetAdditionalContentProvider();
            _controller = new AdditionalContentSynchronizerController(storage, additionalContentProvider,
                DateTime.UtcNow, Application.version);
        }

        public void Synchronize(Action continuation)
        {
            var skipRawConfig = !_controller.ShouldUpdateRawConfig();
            StartCoroutine(SynchronizeCoroutine(skipRawConfig, continuation));
        }

        private IEnumerator SynchronizeCoroutine(bool skipRawConfig, Action continuation)
        {
            var currentConfigVersion = _controller.GetCurrentConfigVersion();
            bool rawConfigSuccess;
            if (skipRawConfig)
            {
                rawConfigSuccess = false;
            }
            else
            {
                var rawConfigWebRequest = GenerateRawConfigWebRequest();
                rawConfigWebRequest.SendWebRequest();
                var waitConfigInstruction = new WaitForSeconds(0.25f);
                var waitConfigTime = 0f;
                while (!rawConfigWebRequest.isDone && waitConfigTime < 5f)
                {
                    yield return waitConfigInstruction;
                    waitConfigTime += 0.25f;
                }

                var rawConfig = ExtractRawConfig(rawConfigWebRequest);
                rawConfigSuccess = rawConfig != null;
                Debug.LogError("rawConfig null : " + !rawConfigSuccess);
                if (rawConfigSuccess)
                {
                    var lastSyncAppVersion = _controller.GetLastSyncAppVersion();
                    var version = Application.version;
                    var flag = version != lastSyncAppVersion;
                    var flag2 = currentConfigVersion != rawConfig.GetConfigVersion();
                    if (flag || flag2)
                    {
                        _controller.SetCurrentRawConfig(rawConfig);
                        currentConfigVersion = rawConfig.GetConfigVersion();
                    }
                }
            }

            var additionalAssetBundleWww = GenerateAdditionalAssetBundleWww(currentConfigVersion);
            var waitAssetBundleInstruction = new WaitForSeconds(0.25f);
            var waitAssetBundleTime = 0f;
            while (!additionalAssetBundleWww.isDone && waitAssetBundleTime < 6f)
            {
                yield return waitAssetBundleInstruction;
                waitAssetBundleTime += 0.25f;
            }

            var patchDataList = ExtractPatchDataList(additionalAssetBundleWww);
            var patchDataListSuccess = patchDataList != null;
            var additionalPatchDataSuccess = false;
            Debug.LogError("patchDataListSuccess null :" + !patchDataListSuccess);
            if (patchDataListSuccess)
            {
                var waitContentProvider = true;
                _controller.SetAdditionalPatchData(patchDataList, delegate(bool success)
                {
                    waitContentProvider = false;
                    additionalPatchDataSuccess = success;
                });
                var waitForSeconds = new WaitForSeconds(0.25f);
                while (waitContentProvider) yield return waitForSeconds;
            }

            if (rawConfigSuccess && patchDataListSuccess && additionalPatchDataSuccess)
                _controller.MarkSuccessfulSync();
            continuation();
        }

        private static UnityWebRequest GenerateRawConfigWebRequest()
        {
            var version = Application.version;
            var uri = AdditionalContentEndPoint.GenerateUrl(AdditionalContentEndPoint.ContentGroup.Config, version,
                "patch-data-config-android.json");
            Debug.LogError("URI : " + uri);
            return UnityWebRequest.Get(uri);
        }

        [CanBeNull]
        private static RawConfig ExtractRawConfig(UnityWebRequest rawConfigWebRequest)
        {
            if (!rawConfigWebRequest.isDone)
            {
                LogError("Config file downloading is not done. Probably time out");
                rawConfigWebRequest.Dispose();
                return null;
            }

            if (rawConfigWebRequest.isNetworkError || rawConfigWebRequest.isHttpError)
            {
                LogError("Config file downloading has failed: " + rawConfigWebRequest.error);
                return null;
            }

            var text = rawConfigWebRequest.downloadHandler.text;
            return RawConfig.MapDataToRawConfig(text);
        }

        private static WWW GenerateAdditionalAssetBundleWww(string configVersion)
        {
            var version = Application.version;
            var url = AdditionalContentEndPoint.GenerateUrl(AdditionalContentEndPoint.ContentGroup.AssetBundle, version,
                GetAssetBundleContentName());
            Debug.LogError("GenerateAdditionalAssetBundleWww URL : " + url);
            var uTF8Encoding = new UTF8Encoding();
            var sHA256Managed = new SHA256Managed();
            var hashString = Convert.ToBase64String(sHA256Managed.ComputeHash(uTF8Encoding.GetBytes(configVersion)));
            var hash = Hash128.Parse(hashString);
            var cachedBundle = new CachedAssetBundle("additional-asset-bundle", hash);
            return WWW.LoadFromCacheOrDownload(url, cachedBundle);
        }

        [CanBeNull]
        private static PatchDataList ExtractPatchDataList(WWW additionalAssetBundleWww)
        {
            if (!additionalAssetBundleWww.isDone)
            {
                LogError("Asset bundle file is not done. Probably timedout.");
                return null;
            }

            if (!string.IsNullOrEmpty(additionalAssetBundleWww.error))
            {
                LogError("Asset bundle file downloading has failed: " + additionalAssetBundleWww.error);
                return null;
            }

            if (additionalAssetBundleWww.assetBundle == null)
            {
                LogError("Asset bundle file downloading has failed: no asset bundle found.");
                return null;
            }

            return additionalAssetBundleWww.assetBundle.LoadAsset<PatchDataList>("patch-data-list");
        }

        private static string GetAssetBundleContentName()
        {
            var platformName = GetPlatformName();
            var version = Application.version;
            return $"asset-bundle-{platformName}-{version}";
        }

        private static void LogError(string message)
        {
            Debug.LogError(message);
            //ApplicationGraph.GetGameEventSender().SendEvent(MWM.GameEvent.EventType.AccompanimentDownloaderError, message);
        }

        private static string GetPlatformName()
        {
            return "android";
        }

        public class AdditionalContentSynchronizerController
        {
            public const int MinNumberOfHoursBetweenTwoSync = 36;

            private readonly AdditionalContentProvider _additionalContentProvider;

            private readonly string _getAppVersion;

            private readonly DateTime _getUtcNow;

            private readonly AdditionalContentSynchronizerStorage _storage;

            public AdditionalContentSynchronizerController(AdditionalContentSynchronizerStorage storage,
                AdditionalContentProvider additionalContentProvider, DateTime getUtcNow, string getAppVersion)
            {
                Precondition.CheckNotNull(storage);
                Precondition.CheckNotNull(additionalContentProvider);
                Precondition.CheckNotNull(getUtcNow);
                Precondition.CheckNotNull(getAppVersion);
                _storage = storage;
                _additionalContentProvider = additionalContentProvider;
                _getUtcNow = getUtcNow;
                _getAppVersion = getAppVersion;
            }

            public string GetCurrentConfigVersion()
            {
                return _storage.ReadCurrentConfigVersion();
            }

            public string GetLastSyncAppVersion()
            {
                return _storage.ReadLastSuccessfulSyncAppVersion();
            }

            public void SetCurrentRawConfig(RawConfig newRawConfig)
            {
                _storage.WriteCurrentConfigVersion(newRawConfig.GetConfigVersion());
                _additionalContentProvider.SetSongsOrder(newRawConfig.GetSongsOrder());
                _additionalContentProvider.SetNewSongs(newRawConfig.GetNewSongs());
            }

            public void SetAdditionalPatchData(PatchDataList patchDataList, Action<bool> callback)
            {
                _additionalContentProvider.SetAdditionalPatchData(patchDataList, callback);
            }

            public bool ShouldUpdateRawConfig()
            {
                var text = _storage.ReadLastSuccessfulSyncAppVersion();
                var value = _getAppVersion;
                if (!text.Equals(value)) return true;
                var d = _storage.ReadLastSuccessfulSyncUtcTime();
                var d2 = _getUtcNow;
                var totalHours = (d2 - d).TotalHours;
                totalHours = Math.Max(0.0, totalHours);
                return totalHours > 36.0;
            }

            public void MarkSuccessfulSync()
            {
                var lastSuccessfulSyncTime = _getUtcNow;
                _storage.WriteLastSuccessfulSyncUtcTime(lastSuccessfulSyncTime);
                var appVersion = _getAppVersion;
                _storage.WriteLastSuccessfulSyncAppVersion(appVersion);
            }
        }
    }
}