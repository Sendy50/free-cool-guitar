using System;
using System.Collections.Generic;

namespace AdditionalContent
{
    public interface ExternalAccompanimentDownloader
    {
        event Action<string> InfoMessageChanged;

        void GetExternalSongs(List<Song.Song> songs, Action<bool, List<ExternalSong>> callback);
    }
}