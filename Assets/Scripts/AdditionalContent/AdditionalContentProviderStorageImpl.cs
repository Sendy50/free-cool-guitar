using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

namespace AdditionalContent
{
    public class AdditionalContentProviderStorageImpl : AdditionalContentProviderStorage
    {
        private static readonly string SongsOrderDataPath = Application.persistentDataPath + "/songs_order";

        private static readonly string NewSongsDataPath = Application.persistentDataPath + "/new_songs_ids";

        public List<string> ReadSongsOrder()
        {
            return GetOrder();
        }

        public void WriteSongsOrder(List<string> songsIds)
        {
            WriteOrder(songsIds);
        }

        public List<string> ReadNewSongs()
        {
            return ReadNewSongsIds();
        }

        public void WriteNewSongs(List<string> newSongsIds)
        {
            WriteNewSongsIds(newSongsIds);
        }

        public void CleanStorage()
        {
            if (File.Exists(SongsOrderDataPath)) File.Delete(SongsOrderDataPath);
            if (File.Exists(NewSongsDataPath)) File.Delete(NewSongsDataPath);
        }

        private static List<string> ReadNewSongsIds()
        {
            if (!File.Exists(NewSongsDataPath)) return new List<string>();
            var serializationStream = File.OpenRead(NewSongsDataPath);
            var binaryFormatter = new BinaryFormatter();
            return (List<string>) binaryFormatter.Deserialize(serializationStream);
        }

        private static void WriteNewSongsIds(List<string> songsId)
        {
            var serializationStream = File.Create(NewSongsDataPath);
            var binaryFormatter = new BinaryFormatter();
            binaryFormatter.Serialize(serializationStream, songsId);
        }

        private static List<string> GetOrder()
        {
            if (!File.Exists(SongsOrderDataPath)) return new List<string>();
            var serializationStream = File.OpenRead(SongsOrderDataPath);
            var binaryFormatter = new BinaryFormatter();
            return (List<string>) binaryFormatter.Deserialize(serializationStream);
        }

        private static void WriteOrder(List<string> songsId)
        {
            var serializationStream = File.Create(SongsOrderDataPath);
            var binaryFormatter = new BinaryFormatter();
            binaryFormatter.Serialize(serializationStream, songsId);
        }
    }
}