using System;

namespace AdditionalContent
{
    public interface AdditionalContentSynchronizer
    {
        void Synchronize(Action continuation);
    }
}