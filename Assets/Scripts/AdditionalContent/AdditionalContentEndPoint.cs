using System;
using System.Security.Cryptography;
using System.Text;
using UnityEngine;
using Random = System.Random;

namespace AdditionalContent
{
    public static class AdditionalContentEndPoint
    {
        public enum ContentGroup
        {
            Config,
            AssetBundle,
            Accompaniment
        }

        private const string KeyPart1 = "539f9263-9cb2-49";

        private const string KeyPart2 = "e7-9aa8-47b60d5fb95e";

        private const string EndPointContent =
            "https://content-dot-learning-guitar.appspot.com/content?g={0}&v={1}&cn={2}&t={3}&s={4}";

        private static readonly SaltGenerator StaticRandomSaltGenerator = new RandomSaltGenerator();

        public static string GenerateUrl(ContentGroup contentGroup, string contentVersion, string contentName)
        {
            return GenerateUrl(contentGroup, contentVersion, contentName, StaticRandomSaltGenerator);
        }

        public static string GenerateUrl(ContentGroup contentGroup, string contentVersion, string contentName,
            SaltGenerator saltGenerator)
        {
            var text = ConvertContentGroupToString(contentGroup);
            var tokenHolder = GenerateTokenHolder("539f9263-9cb2-49e7-9aa8-47b60d5fb95e", contentVersion, contentName,
                saltGenerator);
            return
                $"https://content-dot-learning-guitar.appspot.com/content?g={text}&v={contentVersion}&cn={contentName}&t={WWW.EscapeURL(tokenHolder.Value)}&s={WWW.EscapeURL(tokenHolder.Salt)}";
        }

        public static TokenHolder GenerateTokenHolder(string key, string contentVersion, string contentName,
            SaltGenerator saltGenerator)
        {
            var ue = new UTF8Encoding();
            var sha = new SHA256Managed();
            var text = saltGenerator.GenerateSalt();
            var value = B64EncodedSha256Hash(
                B64EncodedSha256Hash(
                    B64EncodedSha256Hash(text, sha, ue) + B64EncodedSha256Hash(key, sha, ue) +
                    B64EncodedSha256Hash(contentName, sha, ue), sha, ue) +
                B64EncodedSha256Hash(contentVersion, sha, ue), sha, ue);
            return new TokenHolder(value, text);
        }

        private static string B64EncodedSha256Hash(string message, SHA256 sha256, UTF8Encoding ue)
        {
            return Convert.ToBase64String(sha256.ComputeHash(ue.GetBytes(message)));
        }

        private static string ConvertContentGroupToString(ContentGroup contentGroup)
        {
            switch (contentGroup)
            {
                case ContentGroup.Config:
                    return "config";
                case ContentGroup.AssetBundle:
                    return "asset-bundle";
                case ContentGroup.Accompaniment:
                    return "accompaniment";
                default:
                    throw new ArgumentException("Unknown content group : " + contentGroup);
            }
        }

        public class TokenHolder
        {
            public readonly string Salt;
            public readonly string Value;

            public TokenHolder(string value, string salt)
            {
                Value = value;
                Salt = salt;
            }
        }

        public interface SaltGenerator
        {
            string GenerateSalt();
        }

        private class RandomSaltGenerator : SaltGenerator
        {
            private readonly SHA256 _sha256;

            private readonly UTF8Encoding _utf8;

            public RandomSaltGenerator()
            {
                _sha256 = new SHA256Managed();
                _utf8 = new UTF8Encoding();
            }

            public string GenerateSalt()
            {
                var num = new Random().NextDouble();
                return B64EncodedSha256Hash(string.Concat(arg1: Guid.NewGuid().ToString(), arg0: num), _sha256, _utf8);
            }
        }
    }
}