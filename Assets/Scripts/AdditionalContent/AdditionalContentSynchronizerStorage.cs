using System;

namespace AdditionalContent
{
    public interface AdditionalContentSynchronizerStorage
    {
        string ReadCurrentConfigVersion();

        void WriteCurrentConfigVersion(string version);

        void WriteLastSuccessfulSyncUtcTime(DateTime lastSuccessfulSyncTime);

        DateTime ReadLastSuccessfulSyncUtcTime();

        void WriteLastSuccessfulSyncAppVersion(string appVersion);

        string ReadLastSuccessfulSyncAppVersion();
    }
}