using Song;
using UnityEngine;
using Utils;

namespace AdditionalContent
{
    public class ExternalSong : ISong
    {
        private readonly string _accompaniment;

        private readonly string _artist;

        private readonly int _catalogVersion;
        private readonly string _id;

        private readonly bool _isTutorialTrack;

        private readonly string _name;

        private readonly TextAsset _textAsset;

        private readonly SongType _type;

        private GuitarAppMidiEventParser.ParsingResult? _parsedJson;

        public ExternalSong(string id, TextAsset textAsset, string accompaniment, string name, string artist,
            bool isTutorialTrack, SongType type, int catalogVersion)
        {
            Precondition.CheckNotNull(id);
            Precondition.CheckNotNull(accompaniment);
            Precondition.CheckNotNull(name);
            _id = id;
            _textAsset = textAsset;
            _accompaniment = accompaniment;
            _name = name;
            _artist = artist;
            _isTutorialTrack = isTutorialTrack;
            _type = type;
            _catalogVersion = catalogVersion;
        }

        public string GetId()
        {
            return _id;
        }

        public TextAsset GetJson()
        {
            return _textAsset;
        }

        public string GetAccompanimentName()
        {
            return _accompaniment;
        }

        public GuitarAppMidiEventParser.ParsingResult GetParsedJson()
        {
            var parsedJson = _parsedJson;
            if (parsedJson.HasValue) return _parsedJson.Value;
            _parsedJson = GuitarAppMidiEventParser.ParseMidiFile(_textAsset);
            return _parsedJson.Value;
        }

        public string GetName()
        {
            return _name;
        }

        public string GetArtist()
        {
            return _artist;
        }

        public bool GetIsTutorialTrack()
        {
            return _isTutorialTrack;
        }

        public SongType GetSongType()
        {
            return _type;
        }

        public int GetCatalogVersion()
        {
            return _catalogVersion;
        }
    }
}