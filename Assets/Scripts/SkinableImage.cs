using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class SkinableImage : MonoBehaviour
{
    [SerializeField] public ImageSkinTarget target;

    [SerializeField] public GuitarHolder guitarHolder;

    private Image _image;

    private SkinImageUpdator imageUpdator;

    private void Start()
    {
        _image = GetComponent<Image>();
        imageUpdator = new SkinImageUpdator();
        imageUpdator.target = target;
        imageUpdator.SkinableSetup(guitarHolder, UpdateImage);
    }

    private void UpdateImage(Sprite sprite)
    {
        //_image.sprite = sprite;
    }
}