using System.Collections;
using GuitarApplication;
using MWM.Audio;
using UnityEngine;
using UnityEngine.UI;

public class ProgressBar : MonoBehaviour
{
    public Slider Slider;

    private StoppableCoroutine _coroutine;

    public void Reset()
    {
        Slider.value = 0f;
    }

    private void Start()
    {
        Slider.value = 0f;
    }

    public void StartProgression()
    {
        _coroutine.StartRoutine(this,
            UpdateRoutine(ApplicationGraph.GetMusicGamingAudioEngine().GetMultiPlayerUnit().GetPlayer(0)));
    }

    public void StopProgression()
    {
        _coroutine.StopRoutine(this);
        Slider.value = 0f;
    }

    public IEnumerator UpdateRoutine(Player player)
    {
        var wait = new WaitForSeconds(0.1f);
        while (true)
        {
            var duration = player.GetTotalTime();
            if (duration <= 0f)
                Slider.value = 0f;
            else
                Slider.value = player.GetCurrentTime() / duration;
            yield return wait;
        }
    }
}