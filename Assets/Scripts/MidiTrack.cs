using System;

[Serializable]
public class MidiTrack
{
    public float startTime;

    public float duration;

    public float length;

    public string name;

    public MidiNote[] notes;
}