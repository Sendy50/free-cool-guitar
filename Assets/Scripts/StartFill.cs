using UnityEngine;
using UnityEngine.UI;

public class StartFill : MonoBehaviour
{
    public Image StartFillImage;

    public ParticleSystem StartFilledParticles;
}