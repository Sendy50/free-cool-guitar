using System;
using System.Collections.Generic;
using System.Linq;
using Utils;

public class SelectedChordsList : IChordsList, ISelectedChord, ICurrentChordProvider
{
    public const int minimumChords = 1;

    private static SelectedChordsList _default;

    private readonly int maxChordListSize;

    private readonly SelectedChordsListPersistance persistance;

    private SelectedChordsList(int maxChordListSize)
    {
        this.maxChordListSize = maxChordListSize;
        persistance = SelectedChordsListPersistance.loadFromFile();
        if (persistance._playableChords.Count == 0)
        {
            persistance._playableChords = new List<IMidiChord>
            {
                new MidiChord(ChordMap.emptyChordMidiNumbers),
                new MidiChord(new int[6]
                {
                    0,
                    48,
                    52,
                    55,
                    60,
                    64
                }),
                new MidiChord(new int[6]
                {
                    0,
                    45,
                    52,
                    57,
                    60,
                    64
                }),
                new MidiChord(new int[6]
                {
                    41,
                    48,
                    53,
                    57,
                    60,
                    65
                }),
                new MidiChord(new int[6]
                {
                    43,
                    47,
                    50,
                    55,
                    62,
                    67
                })
            };
            persistance.saveToFile();
        }

        SafeChecking();
    }

    public event Action<IChordsList> OnSelectedChodsListChanged;

    public List<IMidiChord> GetPlayableChords()
    {
        return persistance._playableChords;
    }

    public void AddChord(IMidiChord chord)
    {
        if (persistance._playableChords.Count < maxChordListSize)
        {
            persistance._playableChords.Add(chord);
            persistance.saveToFile();
            if (OnSelectedChodsListChanged != null) OnSelectedChodsListChanged(this);
        }
    }

    public bool RemoveChord(IMidiChord chord)
    {
        var count = persistance._playableChords.Count;
        var flag = false;
        if (count <= 1) return false;
        var currentMidiChord = GetCurrentMidiChord();
        Predicate<IMidiChord> predicate = c => c.IsEqual(chord);
        var list = persistance._playableChords.IndexesWhere(predicate);
        persistance._playableChords.RemoveAll(predicate);
        persistance.saveToFile();
        if (list.Contains(persistance.currentChordIndex))
        {
            persistance.currentChordIndex = 0;
            flag = true;
        }
        else
        {
            persistance.currentChordIndex = persistance._playableChords.IndexOf(currentMidiChord);
        }

        persistance.saveToFile();
        if (OnSelectedChodsListChanged != null) OnSelectedChodsListChanged(this);
        if (flag)
        {
            if (OnSelectedChodChanged != null) OnSelectedChodChanged(this, GetCurrentMidiChord());
            if (OnCurrentChodeDidChange != null) OnCurrentChodeDidChange(this, GetCurrentMidiChord());
        }

        return true;
    }

    public event Action<ICurrentChordProvider, IMidiChord> OnCurrentChodeDidChange;

    public bool GetIsMuteRepresentationEnabled()
    {
        return true;
    }

    public event Action<ISelectedChord, IMidiChord> OnSelectedChodChanged;

    public void SelectChord(IMidiChord chord)
    {
        var flag = false;
        for (var i = 0; i < persistance._playableChords.Count; i++)
            if (chord.IsEqual(persistance._playableChords[i]))
            {
                persistance.currentChordIndex = i;
                persistance.saveToFile();
                flag = true;
                if (OnSelectedChodChanged != null) OnSelectedChodChanged(this, chord);
                if (OnCurrentChodeDidChange != null) OnCurrentChodeDidChange(this, chord);
                break;
            }

        if (!flag && chord.IsEmptyChord()) AddChord(MidiChord.EmptyChord());
    }

    public IMidiChord GetCurrentMidiChord()
    {
        return persistance._playableChords[persistance.currentChordIndex];
    }

    public int GetCurrentMidiChordIndex()
    {
        return persistance.currentChordIndex;
    }

    public static SelectedChordsList DefaultList(int maxChordListSize = 6)
    {
        if (_default == null) _default = new SelectedChordsList(maxChordListSize);
        return _default;
    }

    private void SafeChecking()
    {
        if (persistance.currentChordIndex >= persistance._playableChords.Count())
        {
            persistance.currentChordIndex = 0;
            persistance.saveToFile();
        }

        var midiChord = new MidiChord(ChordMap.wrongOldEmpltyChordMidiNumbers);
        for (var i = 0; i < persistance._playableChords.Count(); i++)
            if (midiChord.IsEqual(persistance._playableChords[i]))
                persistance._playableChords[i] = new MidiChord(ChordMap.emptyChordMidiNumbers);
    }
}