namespace FirstUseIndicator
{
    public interface IFirstUseIndicator
    {
        void PlayIndication();

        void HideIndication();

        void ValidateFirstUse();

        bool GetIsNeverUsed();
    }
}