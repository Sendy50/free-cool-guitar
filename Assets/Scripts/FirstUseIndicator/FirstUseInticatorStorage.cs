using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

namespace FirstUseIndicator
{
    public class FirstUseInticatorStorage
    {
        public readonly string identifier;

        public FirstUseInticatorStorage(string identifier)
        {
            this.identifier = identifier;
        }

        private string GetDataPath()
        {
            return Application.persistentDataPath + "/" + identifier;
        }

        public bool ReadIsValidated()
        {
            return ReadIsValidated(GetDataPath());
        }

        public void WriteIsValidated(bool validated)
        {
            WriteIsValidated(GetDataPath(), validated);
        }

        private static bool ReadIsValidated(string dataPath)
        {
            if (!File.Exists(dataPath)) return false;
            var serializationStream = File.OpenRead(dataPath);
            var binaryFormatter = new BinaryFormatter();
            bool temp = (bool) binaryFormatter.Deserialize(serializationStream);
            serializationStream.Close();
            return temp;
        }

        private static void WriteIsValidated(string dataPath, bool validated)
        {
            var serializationStream = File.Create(dataPath);
            var binaryFormatter = new BinaryFormatter();
            binaryFormatter.Serialize(serializationStream, validated);
            serializationStream.Close();
        }
    }
}