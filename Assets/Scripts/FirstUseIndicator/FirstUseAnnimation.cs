using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.UI;

namespace FirstUseIndicator
{
    public class FirstUseAnnimation : MonoBehaviour, IFirstUseIndicator
    {
        private const string animationParameterName = "AnimatedState";

        [UsedImplicitly] public string identifier;

        [UsedImplicitly] public Animator animator;

        [UsedImplicitly] public Button button;

        [UsedImplicitly] public bool autoManaged = true;

        private FirstUseInticatorStorage _storage;

        private void Awake()
        {
            _storage = new FirstUseInticatorStorage(identifier);
        }

        private void Start()
        {
            if (autoManaged && GetIsNeverUsed()) button.onClick.AddListener(OnClick);
        }

        private void OnEnable()
        {
            if (autoManaged && GetIsNeverUsed()) PlayIndication();
        }

        public void PlayIndication()
        {
            if (animator.gameObject.activeSelf) animator.SetBool("AnimatedState", true);
        }

        public void HideIndication()
        {
            if (animator.gameObject.activeSelf) animator.SetBool("AnimatedState", false);
        }

        public void ValidateFirstUse()
        {
            if (autoManaged) button.onClick.RemoveListener(OnClick);
            HideIndication();
            _storage.WriteIsValidated(true);
        }

        public bool GetIsNeverUsed()
        {
            return !_storage.ReadIsValidated();
        }

        [UsedImplicitly]
        public void OnClick()
        {
            ValidateFirstUse();
        }
    }
}