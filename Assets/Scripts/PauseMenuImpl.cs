using System.Collections;
using GuitarApplication;
using JetBrains.Annotations;
using Song;
using UnityEngine;
using UnityEngine.UI;

public class PauseMenuImpl : MonoBehaviour, PauseMenu
{
    public Text songTitle;

    public Text artistName;

    public Image popupBackground;

    public Material backgroundBlurMaterial;

    private ApplicationRouter _applicationRouter;

    private void Start()
    {
        _applicationRouter = ApplicationGraph.GetApplicationRouter();
    }

    private void OnEnable()
    {
        var qualityManager = ApplicationGraph.GetQualityManager();
        if (qualityManager.GetQualityOption() != 0)
        {
            popupBackground.material = backgroundBlurMaterial;
            var screentTextureId = Shader.PropertyToID("_ScreenTexture");
            var applicationRouter = ApplicationGraph.GetApplicationRouter();
            applicationRouter.GetBlurObject().cameraBlur.onCameraBlurRenderImage +=
                delegate(RenderTexture originalTex, RenderTexture blurTex, bool blurActive)
                {
                    popupBackground.material.SetTexture(screentTextureId, !blurActive ? originalTex : blurTex);
                };
        }
        else
        {
            popupBackground.material = null;
        }
    }

    public void SetVisible(bool visible)
    {
        gameObject.SetActive(visible);
    }

    public void DisplayWithSong(ISong currentSong)
    {
        SetVisible(true);
        songTitle.text = currentSong.GetName();
        artistName.text = currentSong.GetArtist();
        songTitle.enabled = false;
        artistName.enabled = false;
        StartCoroutine(DirtyLayoutFix());
    }

    [UsedImplicitly]
    public void OnUnlockAllButtonClick()
    {
        _applicationRouter.RouteToStoreFromPauseMenu();
    }

    [UsedImplicitly]
    public void OnRetryButtonClick()
    {
        _applicationRouter.CancelAndRestartGame();
    }

    [UsedImplicitly]
    public void OnHomeButtonClick()
    {
        _applicationRouter.RouteToFreeModeFromPauseMenu();
    }

    [UsedImplicitly]
    public void OnTrackListButtonClick()
    {
        _applicationRouter.RouteToTrackListFromPauseMenu();
    }

    [UsedImplicitly]
    public void OnResumButtonClick()
    {
        _applicationRouter.ResumeGame();
    }

    private IEnumerator DirtyLayoutFix()
    {
        yield return null;
        songTitle.enabled = true;
        artistName.enabled = true;
    }
}