using UnityEngine;

public interface IChordsSelectorTableViewCell : ITableViewCell
{
    event ChordsSelectorTableViewCell.OnSelectEvent OnSelect;

    event ChordsSelectorTableViewCell.OnAddEvent OnAdd;

    event ChordsSelectorTableViewCell.OnRemoveEvent OnRemove;

    void SetChordName(string name);

    void SetChordNameColor(Color color);

    void SetBackgroundImageColor(Color color);

    void SetAddButtonVisibility(bool visible);

    void SetRemoveButtonVisibility(bool visible);
}