using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

[CreateAssetMenu]
public class GuitarHolder : RuntimeSet<Guitar>, IGuitarHolder
{
    public const int defaultGuitarIndex = 0;

    public int selectedIndex;

    private List<IGuitar> _allGuitars;

    private Storage _storage;

    private void Awake()
    {
        if (_storage == null)
        {
            _storage = new Storage();
            _storage.Load(this);
        }
    }

    private void OnEnable()
    {
        if (_storage == null)
        {
            _storage = new Storage();
            _storage.Load(this);
        }
    }

    public List<IGuitar> allGuitars
    {
        get
        {
            if (_allGuitars != null) return _allGuitars;
            _allGuitars = Items.Select((Func<Guitar, IGuitar>) (c => c)).ToList();
            return _allGuitars;
        }
    }

    public IGuitar selectedGuitar => Items[selectedIndex];

    public event Action<IGuitar> OnSelectedGuitarChanged;

    public void SetSelectedGuitar(IGuitar guitar)
    {
        if (!(guitar is Guitar)) return;
        var guitar2 = guitar as Guitar;
        var num = 0;
        for (var i = 0; i < Items.Count; i++)
        {
            var guitar3 = Items[i];
            if (guitar3 != guitar2)
                guitar3.isSelected = false;
            else
                num = i;
        }

        guitar2.isSelected = true;
        if (num != selectedIndex)
        {
            selectedIndex = num;
            Save();
            if (OnSelectedGuitarChanged != null) OnSelectedGuitarChanged(guitar2);
        }
    }

    public void Save()
    {
        _storage.Save(this);
    }

    private class Storage
    {
        public void Load(GuitarHolder guitarHolder)
        {
            if (File.Exists(Application.persistentDataPath + "/GuitarHolder.dat"))
            {
                var binaryFormatter = new BinaryFormatter();
                var serializationStream =
                    File.Open(Application.persistentDataPath + "/GuitarHolder.dat", FileMode.Open);
                var num = (int) binaryFormatter.Deserialize(serializationStream);
                if (num >= 0 && num < guitarHolder.Items.Count)
                {
                    guitarHolder.selectedIndex = num;
                    serializationStream.Close();
                    return;
                }

                serializationStream.Close();
            }

            guitarHolder.selectedIndex = 0;
        }

        public void Save(GuitarHolder guitarHolder)
        {
            var binaryFormatter = new BinaryFormatter();
            var serializationStream = File.Create(Application.persistentDataPath + "/GuitarHolder.dat");
            binaryFormatter.Serialize(serializationStream, guitarHolder.selectedIndex);
            serializationStream.Close();
        }
    }
}