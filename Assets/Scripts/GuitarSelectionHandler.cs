using AssetBundles.GuitarSkin;
using GuitarApplication;
using MWM.Audio;
using UnityEngine;
using Utils;

public class GuitarSelectionHandler : IGuitarSelectionHandler
{
    private readonly ApplicationRouter _applicationRouter;

    private readonly IErrorDisplayer _errorDisplayer;

    private readonly IGuitarHolder _guitarHolder;

    //private readonly InAppManager _inAppManager;

    private readonly SamplingSynthUnit _samplingSynch;

    private IGuitar _preSelectedGuitar;

    public GuitarSelectionHandler(ApplicationRouter applicationRouter, SamplingSynthUnit samplingSynch,
        IGuitarHolder guitarHolder, IErrorDisplayer errorDisplayer)
    {
        Precondition.CheckNotNull(applicationRouter);
        //Precondition.CheckNotNull(inAppManager);
        Precondition.CheckNotNull(samplingSynch);
        Precondition.CheckNotNull(guitarHolder);
        Precondition.CheckNotNull(errorDisplayer);
        _applicationRouter = applicationRouter;
        //_inAppManager = inAppManager;
        _samplingSynch = samplingSynch;
        _guitarHolder = guitarHolder;
        _errorDisplayer = errorDisplayer;
        _preSelectedGuitar = guitarHolder.selectedGuitar;
    }

    public bool HandleGuitarSelection(IGuitar selectedGuitar)
    {
        if (_preSelectedGuitar.Equals(selectedGuitar))
        {
            _applicationRouter.SetPremiumGuitarSelect(false);
            _preSelectedGuitar = selectedGuitar;
            return false;
        }
        
        // if (selectedGuitar.IsPremium) //&& !_inAppManager.IsSubscribe())
        // if (selectedGuitar.IsPremium && !IAPManager.Instance.IsPrimeMember())
        // {
        //     //_applicationRouter.RouteToStoreFromClickToLockedGuitar();
        //     // _applicationRouter._premiumGuitarSelect = true;
        //     _applicationRouter.SetPremiumGuitarSelect(true);
        //     return false;
        // }

        _applicationRouter.SetPremiumGuitarSelect(false);
        _preSelectedGuitar = selectedGuitar;
        return true;
    }

    public void CloseAndLoadGuitarIfNeeded()
    {
        
        if (_guitarHolder.selectedGuitar.Equals(_preSelectedGuitar))
            _applicationRouter.RouteToFreeModeFromGuitarConfiguration();
        else
        {
            //if(IAPManager.Instance.IsPrimeMember())
                LoadGuitar(_preSelectedGuitar);
        }

    }

    public IGuitar GetPreSelectedGuitar()
    {
        return _preSelectedGuitar;
    }

    private void LoadGuitar(IGuitar selectedGuitar)
    {
        Debug.LogError("LoadGuitar... : " + selectedGuitar.GuitarName);
        // _errorDisplayer.Display("Error", "An error occurred. " + "result.GetDescription() ABCDEFGHIJKLMNOPQRSTUVWXYZ");
        _applicationRouter.ShowLoader();
        selectedGuitar.GetSkinProxy().GetSkin(delegate(IGuitarSkin skin, bool isLoadSkinSuccess)
        {
            Debug.LogError("GetSkin isLoadSkinSuccess... : " + isLoadSkinSuccess);

            if (!isLoadSkinSuccess)
            {
                //if(IAPManager.Instance.IsPrimeMember())
                    _applicationRouter.HideLoader();
                
                _preSelectedGuitar = _guitarHolder.selectedGuitar;
                
                if (Application.internetReachability == NetworkReachability.NotReachable)
                    _errorDisplayer.Display("No internet connection", "Connect to internet and try again.");
                else
                    _errorDisplayer.Display("Error", "An error occurred");
            }
            else
            {
                _samplingSynch.LoadInternal(selectedGuitar.FilePath, delegate(LoadingResult result)
                {
                    Debug.LogError("LoadInternal ... result : " + result);
                    if (result.IsSuccess())
                    {
                        _guitarHolder.SetSelectedGuitar(selectedGuitar);
                        _applicationRouter.RouteToFreeModeFromGuitarConfiguration();
                        _preSelectedGuitar = selectedGuitar;
                    }
                    else
                    {
                       // _errorDisplayer.Display("Error", "An error occurred. " + result.GetDescription());
                        _preSelectedGuitar = _guitarHolder.selectedGuitar;
                    }

                    _applicationRouter.HideLoader();
                });
            }
        });
    }
}