public static class IntroCarouselSlideNumberExtension
{
    public static int IntValue(this IntroCarouselSlideNumber number)
    {
        return (int) number;
    }
}