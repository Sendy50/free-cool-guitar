using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using GlobalInput;
using GuitarApplication;
using Song;
using Tuto;
using UnityEngine;
using UnityEngine.UI;

public class GameSceneManager : MonoBehaviour, OnBackPressedListener
{
    public GameProgression gameProgression;

    public UserActionSpawner userActionSpawner;

    public MusicSheetReader musicSheetReader;

    public UserActionCatcher userActionCatcher;

    public NotesPlayer notesPlayer;

    public GuitarHolder guitarHolder;

    public ChordsUIDisplayer chordsUIDisplayer;

    public ProgressBar progressBar;

    public GodMode godMode;

    public Button pauseButton;

    public Animator BeReady;

    private ApplicationRouter _applicationRouter;

    private ChordsStrings _chordsStrings;

    private ISong _currentSong;

    private GuitarGameReportBuilder _gameReportBuilder;

    private GlobalInputManager _globalInputManager;

    private UserSettings.GuitarStyle _guitarStyleToRestore;

    private bool _resumeGameMustResumeProgression;

    private ScoreManager _scoreManager;

    private HashSet<string> _songsIdsPlayed;

    private ITutoManager _tutoManager;

    private UserSettings _userSettings;

    private void Awake()
    {
        _globalInputManager = ApplicationGraph.GetGlobalInputManager();
        _applicationRouter = ApplicationGraph.GetApplicationRouter();
        _userSettings = ApplicationGraph.GetUserSettings();
    }

    private void Start()
    {
        pauseButton.enabled = false;
        _chordsStrings = _applicationRouter.GetChordsStrings();
        _scoreManager = ApplicationGraph.GetScoreManager();
        _tutoManager = new TutoManager(this, userActionCatcher.gameplayParameters, _chordsStrings,
            ApplicationGraph.GetMusicGamingAudioEngine().GetMultiPlayerUnit().GetPlayer(0), new TutoStorage(),
            userActionSpawner);
        _gameReportBuilder = new GuitarGameReportBuilder(ApplicationGraph.GetGameEventSender());
        userActionCatcher.OnActionCatchedDelegate += delegate(IUserAction action)
        {
            if (action is StrumUserAction) _gameReportBuilder.onSuccessStrum();
            if (action is StrockUserAction) _gameReportBuilder.onSuccessStroke();
        };
        userActionCatcher.OnActionMissedDelegate += delegate(IUserAction action)
        {
            if (action is StrumUserAction) _gameReportBuilder.onMissStrum();
            if (action is StrockUserAction) _gameReportBuilder.onMissStroke();
        };
        userActionCatcher.OnTouchStringResponseDelegate +=
            delegate(UserActionValidationTouchStringResponse response, int stringIndex)
            {
                if (response != 0) _gameReportBuilder.onFailStroke();
            };
    }

    private void OnEnable()
    {
        _globalInputManager.AddOnBackPressedListeners(this, GlobalInputManagerImpl.Priority.Pause);
        _guitarStyleToRestore = _userSettings.GetGuitarStyle();
        _userSettings.SetGuitarStyle(UserSettings.GuitarStyle.Chords);
    }

    private void OnDisable()
    {
        _globalInputManager.RemoveOnBackPressedListeners(this);
        _userSettings.SetGuitarStyle(_guitarStyleToRestore);
    }

    public bool OnBackPressed()
    {
        if (BeReady.isActiveAndEnabled) return true;
        if (pauseButton.isActiveAndEnabled)
        {
            OnPauseClicked();
            return true;
        }

        return false;
    }

    public void StartGameWithNewSong(ISong song)
    {
        StartCoroutine(StartGameWithNewSongRoutine(song));
    }

    private IEnumerator StartGameWithNewSongRoutine(ISong song)
    {
        yield return new WaitUntil(() => ApplicationGraph.GetMusicGamingAudioEngine() != null);
        yield return InternalStartGameWithNewSong(song);
    }

    private IEnumerator InternalStartGameWithNewSong(ISong song)
    {
        _currentSong = song;
        var parsingResult = song.GetParsedJson();
        var musicSheet = new MusicSheet(parsingResult.UserActions, parsingResult.MidiChordEvents);
        yield return StartGameWithNewSongRoutine(musicSheet);
    }

    public void RestartGame()
    {
        StartCoroutine(RestartGameRoutine(musicSheetReader.MusicSheet));
    }

    public void ResumeGame()
    {
        StartCoroutine(ResumeGameRoutine());
    }

    public void CancelAndRestartGame()
    {
        StartCoroutine(CancelAndRestartGameRoutine());
    }

    public void CancelGame(Action cancelGameFinished)
    {
        StartCoroutine(CancelGameRoutine(cancelGameFinished));
    }

    private void OnPauseClicked()
    {
        if (pauseButton.isActiveAndEnabled)
        {
            _resumeGameMustResumeProgression = gameProgression.GetIsRunning();
            gameProgression.StopProgression();
            notesPlayer.Pause();
            _applicationRouter.RouteToPauseMenu(_currentSong);
            pauseButton.enabled = false;
        }
    }

    private IEnumerator StartGameWithNewSongRoutine(MusicSheet musicSheet)
    {
        yield return PreparGameRoutine(musicSheet);
        BeReady.gameObject.SetActive(true);
        BeReady.SetTrigger("StartBeReady");
        yield return new WaitUntil(() =>
            BeReady.gameObject.activeSelf && !BeReady.GetCurrentAnimatorStateInfo(0).IsName("Idle"));
        yield return new WaitUntil(() =>
            BeReady.gameObject.activeSelf && BeReady.GetCurrentAnimatorStateInfo(0).IsName("Idle"));
        BeReady.gameObject.SetActive(false);
        yield return StartGameRoutine(musicSheet, false);
    }

    private IEnumerator RestartGameRoutine(MusicSheet musicSheet)
    {
        yield return PreparGameRoutine(musicSheet);
        BeReady.gameObject.SetActive(true);
        BeReady.SetTrigger("StartBeReady");
        yield return new WaitUntil(() =>
            BeReady.gameObject.activeSelf && !BeReady.GetCurrentAnimatorStateInfo(0).IsName("Idle"));
        yield return new WaitUntil(() =>
            BeReady.gameObject.activeSelf && BeReady.GetCurrentAnimatorStateInfo(0).IsName("Idle"));
        BeReady.gameObject.SetActive(false);
        yield return StartGameRoutine(musicSheet, true);
    }

    private IEnumerator PreparGameRoutine(MusicSheet musicSheet)
    {
        _gameReportBuilder.Reset();
        _gameReportBuilder.onPlayTrack(_currentSong);
        var gameReportBuilder = _gameReportBuilder;
        var parsedJson = _currentSong.GetParsedJson();
        gameReportBuilder.setCurrentTrackActionCount(parsedJson.UserActions.Count());
        // InAppManager inappManager = ApplicationGraph.GetInappManager();
        // _gameReportBuilder.setUserIsVip(inappManager.IsSubscribe());
        _gameReportBuilder.setUserIsVip(true);
        _gameReportBuilder.onGameStart();
        _resumeGameMustResumeProgression = true;
        gameProgression.Reset();
        musicSheetReader.PreparRead(musicSheet, userActionCatcher);
        userActionSpawner.PrepareForNewRun();
        chordsUIDisplayer.SetChordsToDisplay(musicSheet.Chords.Where(c => !c.MidiChord.IsSolo())
            .Select((Func<MidiChordEvent, IChord>) (c => c.MidiChord.ToChord())).Distinct()
            .ToArray());

        chordsUIDisplayer.SetTouchDetectionEnable(false);
        chordsUIDisplayer.StartCurrentlyDisplayedChordRoutine(musicSheetReader);
        _chordsStrings.SetupChordProvider(musicSheet);
        yield break;
    }

    private IEnumerator ResumeGameRoutine()
    {
        BeReady.gameObject.SetActive(true);
        BeReady.SetTrigger("StartBeReady");
        yield return new WaitUntil(() =>
            BeReady.gameObject.activeSelf && !BeReady.GetCurrentAnimatorStateInfo(0).IsName("Idle"));
        yield return new WaitUntil(() =>
            BeReady.gameObject.activeSelf && BeReady.GetCurrentAnimatorStateInfo(0).IsName("Idle"));
        BeReady.gameObject.SetActive(false);
        if (_resumeGameMustResumeProgression)
        {
            gameProgression.Resume();
            notesPlayer.Resume();
        }

        pauseButton.enabled = true;
    }

    private IEnumerator CancelGameRoutine(Action cancelGameFinished)
    {
        yield return StopGameRoutine(false);
        _scoreManager.ResetScore();
        userActionSpawner.PrepareForNewRun();
        cancelGameFinished?.Invoke();
    }

    private IEnumerator CancelAndRestartGameRoutine()
    {
        yield return CancelGameRoutine(null);
        yield return RestartGameRoutine(musicSheetReader.MusicSheet);
    }

    private IEnumerator StartGameRoutine(MusicSheet musicSheet, bool skipLoadMusic)
    {
        Statistics.Instance.IncrementTotalNumberOfGames();
        userActionSpawner.StartSpwaning(musicSheetReader, _chordsStrings);
        yield return notesPlayer.StartPlayingRoutine(musicSheetReader, _currentSong, skipLoadMusic);
        notesPlayer.OnMusicPlayToEndDelegate -= OnMusicPlayToEnd;
        notesPlayer.OnMusicPlayToEndDelegate += OnMusicPlayToEnd;
        musicSheetReader.StartRead();
        _scoreManager.ResetScore();
        _scoreManager.StartScoreHandle(userActionCatcher);
        if (_tutoManager.IsTutoRequiredForTrack(_currentSong)) _tutoManager.StartTuto(musicSheetReader);
        userActionCatcher.StartCatch(musicSheetReader, _chordsStrings);
        gameProgression.StartProgression();
        progressBar.StartProgression();
        pauseButton.enabled = true;
        if (godMode.gameObject.activeSelf) godMode.StartGodMode(musicSheetReader);
    }

    private IEnumerator StopGameRoutine(bool routeToEndScreen = true)
    {
        notesPlayer.OnMusicPlayToEndDelegate -= OnMusicPlayToEnd;
        pauseButton.enabled = false;
        _chordsStrings.SetupChordProvider(SelectedChordsList.DefaultList());
        gameProgression.StopProgression();
        chordsUIDisplayer.StopCurrentlyDisplayedChordRoutine();
        progressBar.StopProgression();
        musicSheetReader.StopRead();
        userActionSpawner.StopSpwaning();
        userActionCatcher.StopCatch();
        ApplicationGraph.GetMusicGamingAudioEngine().GetMultiPlayerUnit().GetPlayer(0)
            .Stop();
        if (routeToEndScreen)
        {
            if (_tutoManager.GetIsRunning()) _tutoManager.HandleEndOfTrack(_currentSong);

            _scoreManager.SaveScoreForTrackID(_currentSong);
            _applicationRouter.RouteToEndGame(_currentSong);
        }
        else if (_tutoManager.GetIsRunning())
        {
            _tutoManager.CancelTuto();
        }

        _gameReportBuilder.onTrackFinishWithScore(_scoreManager.currentScore);
        _gameReportBuilder.SendReport();
        _scoreManager.ResetScore();
        yield break;
    }

    private void OnMusicPlayToEnd()
    {
        StartCoroutine(StopGameRoutine());
    }
}