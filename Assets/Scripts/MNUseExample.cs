using UnityEngine;

public class MNUseExample : MNFeaturePreview
{
    public string appleId = "itms-apps://itunes.apple.com/id375380948?mt=8";

    public string androidAppUrl = "market://details?id=com.google.earth";

    private void Awake()
    {
    }

    private void OnGUI()
    {
        UpdateToStartPos();
        GUI.Label(new Rect(StartX, StartY, Screen.width, 40f), "Native Pop Ups", style);
        StartY += YLableStep;
        if (GUI.Button(new Rect(StartX, StartY, buttonWidth, buttonHeight), "Rate PopUp with events"))
        {
            var mNRateUsPopup = new MNRateUsPopup("rate us", "rate us, please", "Rate Us", "No, Thanks", "Later");
            mNRateUsPopup.SetAppleId(appleId);
            mNRateUsPopup.SetAndroidAppUrl(androidAppUrl);
            mNRateUsPopup.AddDeclineListener(delegate { Debug.Log("rate us declined"); });
            mNRateUsPopup.AddRemindListener(delegate { Debug.Log("remind me later"); });
            mNRateUsPopup.AddRateUsListener(delegate { Debug.Log("rate us!!!"); });
            mNRateUsPopup.AddDismissListener(delegate { Debug.Log("rate us dialog dismissed :("); });
            mNRateUsPopup.Show();
        }

        StartX += XButtonStep;
        if (GUI.Button(new Rect(StartX, StartY, buttonWidth, buttonHeight), "Dialog PopUp"))
        {
            var mNPopup = new MNPopup("title", "dialog message");
            mNPopup.AddAction("action1", delegate { Debug.Log("action 1 action callback"); });
            mNPopup.AddAction("action2", delegate { Debug.Log("action 2 action callback"); });
            mNPopup.AddDismissListener(delegate { Debug.Log("dismiss listener"); });
            mNPopup.Show();
        }

        StartX += XButtonStep;
        if (GUI.Button(new Rect(StartX, StartY, buttonWidth, buttonHeight), "Message PopUp"))
        {
            var mNPopup2 = new MNPopup("title", "dialog message");
            mNPopup2.AddAction("Ok", delegate { Debug.Log("Ok action callback"); });
            mNPopup2.AddDismissListener(delegate { Debug.Log("dismiss listener"); });
            mNPopup2.Show();
        }

        StartY += YButtonStep;
        StartX = XStartPos;
        if (GUI.Button(new Rect(StartX, StartY, buttonWidth, buttonHeight), "Show Prealoder"))
        {
            MNP.ShowPreloader("Title", "Message");
            Invoke("OnPreloaderTimeOut", 3f);
        }

        StartX += XButtonStep;
        if (GUI.Button(new Rect(StartX, StartY, buttonWidth, buttonHeight), "Hide Prealoder")) MNP.HidePreloader();
    }

    private void OnPreloaderTimeOut()
    {
        MNP.HidePreloader();
    }
}