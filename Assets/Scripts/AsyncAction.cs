using System.Threading;
using UnityEngine;

public class AsyncAction : CustomYieldInstruction
{
    private bool _isPerformingAction;

    public AsyncAction(SynchronousJob job, string parameter)
    {
        var asyncAction = this;
        _isPerformingAction = true;
        var thread = new Thread((ThreadStart) delegate
        {
            job(parameter);
            asyncAction._isPerformingAction = false;
        });
        thread.Start();
    }

    public override bool keepWaiting => _isPerformingAction;
}