using System.Collections.Generic;
using UnityEngine;

namespace Patch
{
    [CreateAssetMenu(fileName = "patch-data-list", menuName = "MWMGuitar/PatchDataList")]
    public class PatchDataList : ScriptableObject
    {
        public List<Song.Song> Songs;
    }
}