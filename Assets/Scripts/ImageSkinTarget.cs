using System;

[Serializable]
public enum ImageSkinTarget
{
    FreeModeSelectGuitarButton = 0,
    FreeModeSelectGuitarButtonHighlighted = 1,
    IngamePauseButton = 4,
    IngameCurrentScore = 5,
    IngameCurrentCombo = 6,
    ChordDisplayUI = 7,
    ChordDisplayUIHighlighted = 8,
    FreeModeSettingsButton = 9,
    FreeModeSettingsButtonHighlighted = 10
}