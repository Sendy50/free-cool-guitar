﻿public interface IErrorDisplayer
{
    void Display(string title, string message);
}