public interface IChordContainer
{
    IMidiChord GetMidiChord();

    IChord GetChord();
}