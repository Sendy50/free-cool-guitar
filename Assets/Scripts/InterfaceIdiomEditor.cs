using System;
using UnityEngine;

[CreateAssetMenu]
public class InterfaceIdiomEditor : ScriptableObject
{
    public delegate void OnInterfaceIdiomDidChange(InterfaceIdiom idiom);

    [Serializable]
    public enum InterfaceIdiom
    {
        iphone,
        ipad
    }

    [Tooltip("Used this to run iphone or ipad display on editor. This value is not use on device")]
    public InterfaceIdiom idiom;

    private void OnValidate()
    {
        if (interfaceIdiomDidChangeEvent != null) interfaceIdiomDidChangeEvent(idiom);
    }

    public event OnInterfaceIdiomDidChange interfaceIdiomDidChangeEvent;
}