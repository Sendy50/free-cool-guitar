using System;
using AssetBundles.GuitarSkin;
using UnityEngine;

public class SkinImageUpdator
{
    public ImageSkinTarget target;

    public event Action<Sprite> changeImage;

    public void SkinableSetup(IGuitarHolder guitarHolder, Action<Sprite> changeImage = null)
    {
        Debug.LogError("SkinableSetup ");
        this.changeImage = changeImage;
        OnSelectedGuitarChanged(guitarHolder.selectedGuitar);
        guitarHolder.OnSelectedGuitarChanged += OnSelectedGuitarChanged;
    }

    public static void SpriteForGuitar(IGuitar guitar, ImageSkinTarget target, Action<Sprite> callback)
    {
        Debug.LogError("SpriteForGuitar : " + (guitar.GetSkinProxy() == null));
        guitar.GetSkinProxy().GetSkin(delegate(IGuitarSkin skin, bool success)
        {
            Debug.LogError("SpriteForGuitar : success : " + success);
            // Debug.LogError("SpriteForGuitar : target : "+target);
            // Debug.LogError("SpriteForGuitar : skin : "+(skin.GetInGamePauseButton() == null));
            if (!success)
                callback(null);
            else
                switch (target)
                {
                    case ImageSkinTarget.FreeModeSelectGuitarButton:
                        callback(skin.GetGuitarMenuButton());
                        break;
                    case ImageSkinTarget.FreeModeSelectGuitarButtonHighlighted:
                        callback(skin.GetGuitarMenuButtonHighlighted());
                        break;
                    case ImageSkinTarget.FreeModeSettingsButton:
                        callback(skin.GetFreeModeSettingsButton());
                        break;
                    case ImageSkinTarget.FreeModeSettingsButtonHighlighted:
                        callback(skin.GetFreeModeSettingsButtonHighlighted());
                        break;
                    case ImageSkinTarget.IngamePauseButton:
                        callback(skin.GetInGamePauseButton());
                        break;
                    case ImageSkinTarget.IngameCurrentScore:
                        callback(skin.GetInGameScoreBackground());
                        break;
                    case ImageSkinTarget.IngameCurrentCombo:
                        callback(skin.GetInGameComboBackground());
                        break;
                    case ImageSkinTarget.ChordDisplayUI:
                        callback(skin.GetChordButtonOff());
                        break;
                    case ImageSkinTarget.ChordDisplayUIHighlighted:
                        callback(skin.GetChordButtonOn());
                        break;
                    default:
                        throw new Exception("guitar skin unknown");
                }
        });
    }

    public void SpriteForGuitar(IGuitar guitar, Action<Sprite> callback)
    {
        SpriteForGuitar(guitar, target, callback);
    }

    public void OnSelectedGuitarChanged(IGuitar guitar)
    {
        SpriteForGuitar(guitar, delegate(Sprite sprite)
        {
            if (changeImage != null) changeImage(sprite);
        });
    }
}