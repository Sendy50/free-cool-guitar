using System;
using System.Collections;
using Utils;

namespace Async
{
    public class AsyncJob
    {
        private readonly Action<bool> _callback;
        private readonly IEnumerator _tasks;

        public AsyncJob(IEnumerator tasks, Action<bool> callback)
        {
            Precondition.CheckNotNull(tasks);
            Precondition.CheckNotNull(callback);
            _tasks = tasks;
            _callback = callback;
        }

        public IEnumerator GetTasks()
        {
            return _tasks;
        }

        public void NotifyCallback(bool success)
        {
            _callback(success);
        }
    }
}