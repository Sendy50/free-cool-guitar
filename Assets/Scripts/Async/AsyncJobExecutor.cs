namespace Async
{
    public interface AsyncJobExecutor
    {
        void Execute(AsyncJob asyncJob);

        void Cancel(AsyncJob asyncJob);
    }
}