using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Async
{
    public class AsyncJobExecutorBehaviour : MonoBehaviour, AsyncJobExecutor
    {
        private Dictionary<AsyncJob, Coroutine> _startedCoroutine;

        private void Awake()
        {
            _startedCoroutine = new Dictionary<AsyncJob, Coroutine>();
        }

        public void Execute(AsyncJob asyncJob)
        {
            if (!_startedCoroutine.ContainsKey(asyncJob))
            {
                var value = StartCoroutine(WrappedAsyncJob(asyncJob));
                _startedCoroutine[asyncJob] = value;
            }
        }

        public void Cancel(AsyncJob asyncJob)
        {
            if (_startedCoroutine.ContainsKey(asyncJob))
            {
                var routine = _startedCoroutine[asyncJob];
                StopCoroutine(routine);
                _startedCoroutine.Remove(asyncJob);
                asyncJob.NotifyCallback(false);
            }
        }

        private IEnumerator WrappedAsyncJob(AsyncJob asyncJob)
        {
            yield return asyncJob.GetTasks();
            _startedCoroutine.Remove(asyncJob);
            asyncJob.NotifyCallback(true);
        }
    }
}