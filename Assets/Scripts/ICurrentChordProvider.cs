using System;

public interface ICurrentChordProvider
{
    event Action<ICurrentChordProvider, IMidiChord> OnCurrentChodeDidChange;

    IMidiChord GetCurrentMidiChord();

    bool GetIsMuteRepresentationEnabled();
}