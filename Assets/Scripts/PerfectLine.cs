using System;
using System.Collections;
using GuitarApplication;
using UnityEngine;

public class PerfectLine : MonoBehaviour
{
    public GameSceneLayout gameSceneLayout;

    public UserSettings userSettings;

    public GameProgression gameProgression;

    public GameplayParameters gameplayParameters;

    public SpriteRenderer perfectLineSprite;

    public StrokSpawnObject strok;

    public UserActionCatcher userActionCatcher;

    public SpriteRenderer[] strumsGoodDown;

    public SpriteRenderer[] strumsGoodUp;

    public SpriteRenderer[] stroksGood;

    public SpriteRenderer[] stroksMiss;

    public ParticleSystem[] comboParticles;

    private ScoreManager _scoreManager;

    private void Start()
    {
        _scoreManager = ApplicationGraph.GetScoreManager();
        var num = GameProgression.DurationToDimention(gameplayParameters.catchTolerance);
        var size = strok.sprite.bounds.size;
        var x = size.x;
        var num2 = GameProgression.DimentionToDuration(x);
        var num3 = Mathf.Clamp(num - x * 0.5f, 0.0001f, num);
        var size2 = perfectLineSprite.bounds.size;
        var x2 = size2.x;
        var x3 = num3 / x2;
        var localScale = gameObject.transform.localScale;
        localScale.x = x3;
        gameObject.transform.localScale = localScale;
        UpdateOrientation();
        userActionCatcher.OnActionCatchedDelegate += OnActionCatched;
        userActionCatcher.OnTouchStringResponseDelegate += OnTouchStringResponse;
        _scoreManager.OnNewComboDelegate += OnNewCombo;
        _scoreManager.OnResteDelegate += OnScoreReste;
        userSettings.GameOrientationChanged += OnOrientationChange;
    }

    private void OnDestroy()
    {
        userActionCatcher.OnActionCatchedDelegate -= OnActionCatched;
        userActionCatcher.OnTouchStringResponseDelegate -= OnTouchStringResponse;
        _scoreManager.OnNewComboDelegate -= OnNewCombo;
        _scoreManager.OnResteDelegate -= OnScoreReste;
        userSettings.GameOrientationChanged -= OnOrientationChange;
    }

    private void UpdateOrientation()
    {
        var position = transform.position;
        var screenWidth = GameSceneLayout.ScreenWidth;
        position.x = userSettings.GetGameOrientation() != 0
            ? 0f - gameProgression.PerfectForwardOffset
            : gameProgression.PerfectForwardOffset;
        transform.position = position;
    }

    private void OnOrientationChange(UserSettings.GameOrientation gameOrientation)
    {
        UpdateOrientation();
    }

    private void OnActionCatched(IUserAction action)
    {
        if (action is StrumUserAction)
        {
            var strumUserAction = action as StrumUserAction;
            if (strumUserAction.orientation == StrumUserAction.Orientation.Down)
                StartCoroutine(CatchStrumRoutine(strumsGoodDown[action.SpawningStringIndex()]));
            else
                StartCoroutine(CatchStrumRoutine(strumsGoodUp[action.SpawningStringIndex() - 3]));
        }
        else
        {
            StartCoroutine(CatchStrokRoutine(stroksGood[action.SpawningStringIndex()]));
        }
    }

    private void OnTouchStringResponse(UserActionValidationTouchStringResponse response, int stringIndex)
    {
        if (response != 0) StartCoroutine(MissStrokRoutine(stroksMiss[stringIndex]));
    }

    private IEnumerator CatchStrumRoutine(SpriteRenderer spriteRenderer)
    {
        spriteRenderer.gameObject.SetActive(true);
        yield return Fad(spriteRenderer, 1f, 355f / (678f * (float) Math.PI));
        yield return Fad(spriteRenderer, 0f, 0.333333343f);
        spriteRenderer.gameObject.SetActive(false);
    }

    private IEnumerator MissStrumRoutine(SpriteRenderer spriteRenderer)
    {
        spriteRenderer.gameObject.SetActive(true);
        yield return Fad(spriteRenderer, 1f, 355f / (678f * (float) Math.PI));
        yield return Fad(spriteRenderer, 0f, 0.333333343f);
        spriteRenderer.gameObject.SetActive(false);
    }

    private IEnumerator CatchStrokRoutine(SpriteRenderer spriteRenderer)
    {
        spriteRenderer.gameObject.SetActive(true);
        yield return Fad(spriteRenderer, 1f, 71f / (678f * (float) Math.PI));
        yield return Fad(spriteRenderer, 0f, 7f / 15f);
        spriteRenderer.gameObject.SetActive(false);
    }

    private IEnumerator MissStrokRoutine(SpriteRenderer spriteRenderer)
    {
        spriteRenderer.gameObject.SetActive(true);
        yield return Fad(spriteRenderer, 1f, 71f / (678f * (float) Math.PI));
        yield return Fad(spriteRenderer, 0f, 7f / 15f);
        spriteRenderer.gameObject.SetActive(false);
    }

    private IEnumerator Fad(SpriteRenderer spriteRenderer, float finalAlpha, float duration)
    {
        var startColor = spriteRenderer.color;
        var endColor = new Color(startColor.r, startColor.g, startColor.b, finalAlpha);
        var time = 0f;
        while (time < duration)
        {
            yield return null;
            time += Time.deltaTime;
            spriteRenderer.color = Color.Lerp(startColor, endColor, time / duration);
        }

        spriteRenderer.color = endColor;
    }

    private void OnNewCombo(ScoreManager sender, ScoreManager.Combo combo, int consecutiveCatch)
    {
        var num = (int) combo.scoreMultiplicateur - 2;
        if (num < 0)
        {
            OnScoreReste();
            return;
        }

        for (var i = 0; i < comboParticles.Length; i++)
        {
            var particleSystem = comboParticles[i];
            if (i == num)
            {
                particleSystem.gameObject.SetActive(true);
                particleSystem.Play();
            }
            else
            {
                particleSystem.Stop();
                particleSystem.gameObject.SetActive(false);
            }
        }
    }

    private void OnScoreReste()
    {
        var array = comboParticles;
        foreach (var particleSystem in array)
        {
            particleSystem.Stop();
            particleSystem.gameObject.SetActive(false);
        }
    }
}