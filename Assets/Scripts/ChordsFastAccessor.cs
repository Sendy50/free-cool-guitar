using System.Collections.Generic;
using UnityEngine;

public class ChordsFastAccessor : MonoBehaviour, IChordsFastAccessor
{
    public RectTransform rectTransform;

    public Camera canvasCamera;

    public List<FastAccessorDescriptor> allFastAccessor;

    public GameObject fastAccessorButtonPrefab;

    private readonly List<ChordsFastAccessorButton> allFastAccessorButton = new List<ChordsFastAccessorButton>();

    private void Start()
    {
        foreach (var item in allFastAccessor)
        {
            Debug.LogError("allFastAccessorButton add size : " + allFastAccessorButton.Count);
            Debug.LogError("allFastAccessor add size : " + allFastAccessor.Count);
            Debug.LogError("item fastAccessorName : " + item.ChordId);
            var gameObject = Instantiate(fastAccessorButtonPrefab, transform);
            Debug.LogError("item fastAccessorName : " + item.fastAccessorName);
            var component = gameObject.GetComponent<ChordsFastAccessorButton>();
            Debug.LogError("item fastAccessorName : " + item.ChordId);
            component.updateWithDescriptor(item);
            Debug.LogError("item fastAccessorName : " + item.fastAccessorName);
            allFastAccessorButton.Add(component);
            Debug.LogError("item fastAccessorName : " + item.ChordId);
        }
    }

    private void Update()
    {
        if (Input.touchCount > 0)
        {
            var position = Input.GetTouch(0).position;
            if (RectTransformUtility.RectangleContainsScreenPoint(rectTransform, position, canvasCamera))
            {
                RectTransformUtility.ScreenPointToLocalPointInRectangle(rectTransform, position, canvasCamera,
                    out var localPoint);
                var rect = rectTransform.rect;
                var x = rect.size.x;
                var y = rect.size.y;
                var normalTouchPos = new Vector2(Mathf.InverseLerp((0f - x) / 2f, x / 2f, localPoint.x),
                    1f - Mathf.InverseLerp((0f - y) / 2f, y / 2f, localPoint.y));
                sendEventForNormalTouchPos(normalTouchPos);
            }
        }
    }

    public event OnFastAccessorSelectChord onFastAccessorSelect;

    public void updateHighlightedButton(int topDisplayChordId)
    {
        var flag = false;
        for (var num = allFastAccessorButton.Count - 1; num >= 0; num--)
        {
            var chordsFastAccessorButton = allFastAccessorButton[num];
            if (topDisplayChordId >= chordsFastAccessorButton.descriptor.ChordId && !flag)
            {
                chordsFastAccessorButton.highlighted();
                flag = true;
            }
            else
            {
                chordsFastAccessorButton.unhighlighted();
            }
        }
    }

    private void sendEventForNormalTouchPos(Vector2 normalTouchPos)
    {
        var num = 1f / allFastAccessorButton.Count;
        var num2 = Mathf.FloorToInt(normalTouchPos.y / num);
        var num3 = -1f;
        foreach (var item in allFastAccessorButton)
        {
            num3 += 1f;
            if (num3 == num2)
            {
                item.highlighted();
                if (onFastAccessorSelect != null) onFastAccessorSelect(item.descriptor.ChordId);
            }
            else
            {
                item.unhighlighted();
            }
        }
    }
}