using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

public class MNIOSAlert : MonoBehaviour
{
    private IEnumerable<string> actions;

    private string message = string.Empty;

    public Action<string> OnComplete = delegate { };

    private string title = string.Empty;

    public static MNIOSAlert Create(string title, string message, IEnumerable<string> actions)
    {
        var mNIOSAlert = new GameObject("IOSPopUp").AddComponent<MNIOSAlert>();
        mNIOSAlert.title = title;
        mNIOSAlert.message = message;
        mNIOSAlert.actions = actions;
        return mNIOSAlert;
    }

    public void Show()
    {
        var stringBuilder = new StringBuilder();
        var enumerator = actions.GetEnumerator();
        if (enumerator.MoveNext()) stringBuilder.Append(enumerator.Current);
        while (enumerator.MoveNext())
        {
            stringBuilder.Append("|");
            stringBuilder.Append(enumerator.Current);
        }

        MNIOSNative.showMessage(title, message, stringBuilder.ToString());
    }

    public void onPopUpCallBack(string result)
    {
        OnComplete(result);
        Destroy(gameObject);
    }
}