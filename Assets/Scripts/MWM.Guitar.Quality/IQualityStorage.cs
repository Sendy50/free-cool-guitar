namespace MWM.Guitar.Quality
{
    public interface IQualityStorage
    {
        QualityOption? ReadOverridenQualityOption();

        void WriteOverridenQualityOption(QualityOption qualityOption);
    }
}