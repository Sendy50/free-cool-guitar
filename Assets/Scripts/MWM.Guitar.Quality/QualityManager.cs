using System;
using JetBrains.Annotations;
using UnityEngine;

namespace MWM.Guitar.Quality
{
    public class QualityManager : IQualityManager
    {
        private const int UnityQualityIndexVeryLow = 0;

        private const int UnityQualityIndexLow = 1;

        private const int UnityQualityIndexMedium = 2;

        private readonly IQualityStorage _qualityStorage;

        private QualityOption _qualityOption;

        public QualityManager(IQualityStorage qualityStorage)
        {
            _qualityStorage = qualityStorage;
            //qualityStorage.WriteOverridenQualityOption(QualityOption.Low);
            // QualityOption? qualityOption = qualityStorage.ReadOverridenQualityOption();
            // if (qualityOption.HasValue)
            // {
            // 	_qualityOption = qualityOption.Value;
            // }
            // else
            // {
            // 	_qualityOption = ComputeDefaultQualityOption();
            // }

            _qualityOption = QualityOption.Low;
            ApplyQuality(_qualityOption);
        }

        public QualityOption GetQualityOption()
        {
            return _qualityOption;
        }

        public void SetQualityOption(QualityOption qualityOption)
        {
            if (_qualityOption != qualityOption)
            {
                _qualityOption = qualityOption;
                _qualityStorage.WriteOverridenQualityOption(_qualityOption);
                ApplyQuality(_qualityOption);
            }
        }

        private static QualityOption ComputeDefaultQualityOption()
        {
            return ComputeDefaultQualityOptionForAndroid();
        }

        [UsedImplicitly]
        private static QualityOption ComputeDefaultQualityOptionForIos()
        {
            return QualityOption.Normal;
        }

        [UsedImplicitly]
        private static QualityOption ComputeDefaultQualityOptionForAndroid()
        {
            if (SystemInfo.systemMemorySize < 1000 || SystemInfo.graphicsMemorySize <= 256) return QualityOption.Low;
            return QualityOption.Normal;
        }

        private static void ApplyQuality(QualityOption qualityOption)
        {
            var unityQualityIndex = GetUnityQualityIndex(qualityOption);
            QualitySettings.SetQualityLevel(unityQualityIndex);
            switch (qualityOption)
            {
                case QualityOption.Low:
                    Application.targetFrameRate = 50;
                    break;
                case QualityOption.Normal:
                    Application.targetFrameRate = 60;
                    break;
                default:
                    throw new ArgumentOutOfRangeException("qualityOption", qualityOption, null);
            }
        }

        private static int GetUnityQualityIndex(QualityOption qualityOption)
        {
            return GetUnityQualityIndexForAndroid(qualityOption);
        }

        [UsedImplicitly]
        private static int GetUnityQualityIndexForIos()
        {
            return 2;
        }

        [UsedImplicitly]
        private static int GetUnityQualityIndexForAndroid(QualityOption qualityOption)
        {
            switch (qualityOption)
            {
                case QualityOption.Low:
                    return 0;
                case QualityOption.Normal:
                    return 1;
                default:
                    throw new ArgumentOutOfRangeException("qualityOption", qualityOption, null);
            }
        }
    }
}