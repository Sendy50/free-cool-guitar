using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

namespace MWM.Guitar.Quality
{
    public class QualityStorageFile : IQualityStorage
    {
        private static readonly string OverridenQualityOptionPath =
            Application.persistentDataPath + "/overriden-quality-option";

        public QualityOption? ReadOverridenQualityOption()
        {
            if (!File.Exists(OverridenQualityOptionPath)) return null;
            var serializationStream = File.OpenRead(OverridenQualityOptionPath);
            var binaryFormatter = new BinaryFormatter();
            return (QualityOption) binaryFormatter.Deserialize(serializationStream);
        }

        public void WriteOverridenQualityOption(QualityOption qualityOption)
        {
            var serializationStream = File.Create(OverridenQualityOptionPath);
            var binaryFormatter = new BinaryFormatter();
            binaryFormatter.Serialize(serializationStream, qualityOption);
        }
    }
}