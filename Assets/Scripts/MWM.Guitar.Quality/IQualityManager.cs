namespace MWM.Guitar.Quality
{
    public interface IQualityManager
    {
        QualityOption GetQualityOption();

        void SetQualityOption(QualityOption qualityOption);
    }
}