using GuitarConfiguration;
using Song;
using TrackCollection;

namespace GuitarApplication
{
    public interface ApplicationRouter
    {
        
        bool  GetPremiumGuitarSelect();
        void  SetPremiumGuitarSelect(bool select);

        GuitarConfigurationScreen GetGuitarConfigurationScreen();

        TrackCollectionScreen GetTrackCollectionScreen();

        StoreManager GetStoreManager();


        void HideLaunchScreen();

        void ShowLoader();

        void HideLoader();

        void RouteToFreeModeFromStartApplication();

        void RouteToFreeModeFromGuitarConfiguration();

        void RouteToFreeModeFromTrackList();

        void RouteToFreeModeFromChordSelectior();

        void RouteToFreeModeFromPauseMenu();

        void RouteToStoreFromClickToLockedGuitar();

        void RouteToStoreFromClickVipTrackListCell();

        void RouteToStoreFromTrackListNavBar();

        void RouteToStoreFromFreeMode(bool isOpeningAppDisplay);

        void RouteToStoreFromPauseMenu();

        void RouteToStoreFromClickUnlockAllOnGuitarConfiguration();

        void RouteToSplashStoreFromStartAppWithDelay(float delay);

        void RouteToInGameFromTrackList(ISong song);

        void RouteToInGameFromEndScreen(ISong track);

        void RouteToInGameFromEndScreen();

        void CancelAndRestartGame();

        void ResumeGame();

        void RouteToPauseMenu(ISong currentSong);

        void RouteToEndGame(ISong song);

        void RouteToTrackListFromPauseMenu();

        void RouteToTrackListFromEndGame();

        void RouteToCustomRatingPopupFromEndGame();

        ChordsStrings GetChordsStrings();

        Blur GetBlurObject();

        void SetActiveBlurCamera(bool active);
    }
}