namespace GuitarApplication
{
    public interface IStatistics
    {
        void IncrementNumAppLaunches();

        int GetNumberAppLaunches();

        void IncrementTotalNumberOfGames();

        int GetTotalNumberOfGames();

        void IncrementNumRatingRequestDisplay();

        int GetNumRatingRequestDisplayForCurrentAppVersion();

        bool GetDidDisplayRatingRequestThisSession();

        void SetDidDisplayRatingRequestThisSession(bool didDisplay);

        void IncrementNumRatingRequestAccepted();

        int GetNumRatingRequestAcceptedForCurrentAppVersion();
    }
}