using System;
using System.Collections;
using System.Runtime.CompilerServices;
using AppScreen;
using GuitarConfiguration;
using InHouseAds;
using JetBrains.Annotations;
using MWM.Guitar.FreeMode;
using MWM.Guitar.Quality;
using MWM.Guitar.Settings;
using Rating;
using Song;
using Splashstore;
using Store;
using TrackCollection;
using UnityEngine;

namespace GuitarApplication
{
    public class ApplicationRouterBehaviour : MonoBehaviour, ApplicationRouter
    {
        // private InHouseAdsDisplayHandler _inHouseAdsDisplayHandler;

        [CompilerGenerated] private static Func<float> _003C_003Ef__mg_0024cache0;

        [SerializeField] private GameObject _loader;

        [SerializeField] private GameObject _launchScreen;

        [SerializeField] private FreeModeSceneImpl _freeModeScreen;

        [SerializeField] private StoreScreen _storeScreen;

        [SerializeField] private CustomRatingPopup _ratingPopup;

        [SerializeField] private ChordsStrings _chordsStrings;

        [SerializeField] private GameObject _guitarStyleScreenAdapter;

        [SerializeField] private SettingsScreen _settingsScreen;

        [SerializeField] private GameObject _blackFilter;

        [SerializeField] private Camera _mainCamera;

        [SerializeField] public ScreenInstanceLocator _screenInstanceLocator;

        [SerializeField] private SplashstoreScreenBehavior _splashstoreScreen;
       
        [SerializeField] private StoreManager _storeManagerScreen;

        private InHouseAdsManager _inHouseAdsManager;

        private IQualityManager _qualityManager;

        private ScoreManager _scoreManager;

        private StoreRouter _storeRouter;
        private bool _premiumGuitarSelect = false;

        private GameModeScreen GameModeScreen => _screenInstanceLocator.GetGameModeScreen();

        private ChordsSelector ChordsSelector => _screenInstanceLocator.GetChordsSelector();

        public GuitarConfigurationScreen GuitarConfigurationScreen =>
            _screenInstanceLocator.GetGuitarConfigurationScreen();

        private TrackCollectionScreen TrackListScreen => _screenInstanceLocator.GetTrackCollection();

       

        public bool GetPremiumGuitarSelect()
        {
            return _premiumGuitarSelect;
        }

        public GuitarConfigurationScreen GetGuitarConfigurationScreen()
        {
            return GuitarConfigurationScreen;
        }
        
        public TrackCollectionScreen GetTrackCollectionScreen()
        {
            return TrackListScreen;
        }

        public StoreManager GetStoreManager()
        {
            return _storeManagerScreen;
        }

        public void SetPremiumGuitarSelect(bool @select)
        {
            _premiumGuitarSelect = select;
        }

        public void HideLaunchScreen()
        {
            _launchScreen.SetActive(false);
        }

     
        
        public void ShowLoader()
        {
            _loader.SetActive(true);
        }

        
        
        public void HideLoader()
        {
            _loader.SetActive(false);
        }

        public void RouteToFreeModeFromStartApplication()
        {
            _freeModeScreen.SetVisible(true);
            _guitarStyleScreenAdapter.SetActive(true);
        }

        public void RouteToFreeModeFromGuitarConfiguration()
        {
            GuitarConfigurationScreen.Hide();
            _blackFilter.SetActive(false);
            SetActiveBlurCamera(false);
            _freeModeScreen.SetVisible(true);
        }

        public void RouteToFreeModeFromTrackList()
        {
            _blackFilter.SetActive(false);
            SetActiveBlurCamera(false);
            TrackListScreen.gameObject.SetActive(false);
            _freeModeScreen.SetVisible(true);
        }

        public void RouteToFreeModeFromChordSelectior()
        {
            ChordsSelector.gameObject.SetActive(false);
            _blackFilter.SetActive(false);
            SetActiveBlurCamera(false);
            _freeModeScreen.SetVisible(true);
        }

        public void RouteToFreeModeFromPauseMenu()
        {
            GameModeScreen.PauseMenu.SetVisible(false);
            GameModeScreen.InGameScreen.gameObject.SetActive(false);
            GameModeScreen.GameSceneManager.CancelGame(DiableGameModeContainer);
            _blackFilter.SetActive(true);
            SetActiveBlurCamera(true);
            // Action<InHouseAdsDisplayHandler> closeAdsCompletion = null;
            // closeAdsCompletion = delegate
            // {
            // 	_inHouseAdsDisplayHandler.CloseDelegate -= closeAdsCompletion;
            _blackFilter.SetActive(false);
            SetActiveBlurCamera(false);
            _freeModeScreen.SetVisible(true);
            _guitarStyleScreenAdapter.SetActive(true);
            // };
            // if (!_inHouseAdsManager.ShowInHouseAdsIfAppropriate(delegate(InHouseAdsPlacement placement)
            // {
            // 	_inHouseAdsDisplayHandler.CloseDelegate += closeAdsCompletion;
            // 	_inHouseAdsDisplayHandler.Show(placement, InHouseDisplaySource.PauseMenu);
            // 	return true;
            // }))
            // {
            // 	closeAdsCompletion(_inHouseAdsDisplayHandler);
            // }
        }

        public void RouteToStoreFromClickToLockedGuitar()
        {
            GuitarConfigurationScreen.Hide();
            _blackFilter.SetActive(true);
            SetActiveBlurCamera(true);
            _storeRouter.ShowStoreFromSelectLockedGuitar();
        }

        public void RouteToStoreFromClickVipTrackListCell()
        {
            TrackListScreen.gameObject.SetActive(false);
            _blackFilter.SetActive(true);
            SetActiveBlurCamera(true);
            _storeManagerScreen._instance.ShowStoreFromClickVIPTrack();
        }

        public void RouteToStoreFromTrackListNavBar()
        {
            TrackListScreen.gameObject.SetActive(false);
            _blackFilter.SetActive(true);
            SetActiveBlurCamera(true);
            _storeRouter.ShowStoreFromTrackListNavBar();
        }

        public void RouteToStoreFromFreeMode(bool isOpeningAppDisplay)
        {
            if (isOpeningAppDisplay)
            {
                StartCoroutine(DelayShowStoreFromOpeningApp());
                return;
            }

            _freeModeScreen.SetVisible(false);
            _blackFilter.SetActive(true);
            SetActiveBlurCamera(true);
            _storeRouter.ShowStoreFromFreeModeInterface();
        }

        public void RouteToStoreFromPauseMenu()
        {
            GameModeScreen.PauseMenu.SetVisible(false);
            _blackFilter.SetActive(true);
            SetActiveBlurCamera(true);
            _storeRouter.ShowStoreFromPauseInGameInterface();
        }

        public void RouteToStoreFromClickUnlockAllOnGuitarConfiguration()
        {
            GuitarConfigurationScreen.Hide();
            _blackFilter.SetActive(true);
            SetActiveBlurCamera(true);
            _storeManagerScreen._instance.ShowStoreFromClickUnlockAllOnGuitarList();
        }

        public void RouteToInGameFromTrackList(ISong song)
        {
            GameModeScreen.gameObject.SetActive(true);
            _blackFilter.SetActive(false);
            SetActiveBlurCamera(false);
            TrackListScreen.gameObject.SetActive(false);
            GameModeScreen.InGameScreen.gameObject.SetActive(true);
            GameModeScreen.GameSceneManager.StartGameWithNewSong(song);
        }

        public void RouteToInGameFromEndScreen(ISong track)
        {
            _blackFilter.SetActive(false);
            SetActiveBlurCamera(false);
            GameModeScreen.EndGameScreen.gameObject.SetActive(false);
            GameModeScreen.GameSceneManager.StartGameWithNewSong(track);
        }

        public void RouteToInGameFromEndScreen()
        {
            GameModeScreen.EndGameScreen.gameObject.SetActive(false);
            // Action<InHouseAdsDisplayHandler> closeAdsCompletion = null;
            // closeAdsCompletion = delegate
            // {
            _blackFilter.SetActive(false);
            SetActiveBlurCamera(false);
            // _inHouseAdsDisplayHandler.CloseDelegate -= closeAdsCompletion;
            GameModeScreen.GameSceneManager.RestartGame();
            // };
            // if (!_inHouseAdsManager.ShowInHouseAdsIfAppropriate(delegate(InHouseAdsPlacement placement)
            // {
            // 	_inHouseAdsDisplayHandler.CloseDelegate += closeAdsCompletion;
            // 	_inHouseAdsDisplayHandler.Show(placement, InHouseDisplaySource.EndGame);
            // 	return true;
            // }))
            // {
            // closeAdsCompletion(_inHouseAdsDisplayHandler);
            // }
        }

        public void CancelAndRestartGame()
        {
            GameModeScreen.PauseMenu.SetVisible(false);
            _blackFilter.SetActive(true);
            SetActiveBlurCamera(true);
            // Action<InHouseAdsDisplayHandler> closeAdsCompletion = null;
            // closeAdsCompletion = delegate
            // {
            // 	_inHouseAdsDisplayHandler.CloseDelegate -= closeAdsCompletion;
            _blackFilter.SetActive(false);
            SetActiveBlurCamera(false);
            GameModeScreen.GameSceneManager.CancelAndRestartGame();
            // };
            // if (!_inHouseAdsManager.ShowInHouseAdsIfAppropriate(delegate(InHouseAdsPlacement placement)
            // {
            // 	_inHouseAdsDisplayHandler.CloseDelegate += closeAdsCompletion;
            // 	_inHouseAdsDisplayHandler.Show(placement, InHouseDisplaySource.PauseMenu);
            // 	return true;
            // }))
            // {
            // 	closeAdsCompletion(_inHouseAdsDisplayHandler);
            // }
        }

        public void ResumeGame()
        {
            GameModeScreen.PauseMenu.SetVisible(false);
            _blackFilter.SetActive(false);
            SetActiveBlurCamera(false);
            GameModeScreen.GameSceneManager.ResumeGame();
        }

        public void RouteToPauseMenu(ISong currentSong)
        {
            GameModeScreen.PauseMenu.DisplayWithSong(currentSong);
            _blackFilter.SetActive(true);
            SetActiveBlurCamera(true);
        }

        public void RouteToEndGame(ISong song)
        {
            _blackFilter.SetActive(true);
            SetActiveBlurCamera(true);
            GameModeScreen.EndGameScreen.gameObject.SetActive(true);
            GameModeScreen.EndGameScreen.UpdateForTrack(song, _scoreManager);
        }

        public void RouteToTrackListFromPauseMenu()
        {
            GameModeScreen.PauseMenu.SetVisible(false);
            GameModeScreen.InGameScreen.gameObject.SetActive(false);
            GameModeScreen.GameSceneManager.CancelGame(DiableGameModeContainer);
            _blackFilter.SetActive(true);
            SetActiveBlurCamera(true);
            // Action<InHouseAdsDisplayHandler> closeAdsCompletion = null;
            // closeAdsCompletion = delegate
            // {
            // 	_inHouseAdsDisplayHandler.CloseDelegate -= closeAdsCompletion;
            TrackListScreen.gameObject.SetActive(true);
            // };
            // if (!_inHouseAdsManager.ShowInHouseAdsIfAppropriate(delegate(InHouseAdsPlacement placement)
            // {
            // 	_inHouseAdsDisplayHandler.CloseDelegate += closeAdsCompletion;
            // 	_inHouseAdsDisplayHandler.Show(placement, InHouseDisplaySource.PauseMenu);
            // 	return true;
            // }))
            // {
            // 	closeAdsCompletion(_inHouseAdsDisplayHandler);
            // }
        }

        public void RouteToTrackListFromEndGame()
        {
            GameModeScreen.InGameScreen.gameObject.SetActive(false);
            GameModeScreen.EndGameScreen.gameObject.SetActive(false);
            DiableGameModeContainer();
            // Action<InHouseAdsDisplayHandler> closeAdsCompletion = null;
            // closeAdsCompletion = delegate
            // {
            // 	_inHouseAdsDisplayHandler.CloseDelegate -= closeAdsCompletion;
            _blackFilter.SetActive(true);
            SetActiveBlurCamera(true);
            TrackListScreen.gameObject.SetActive(true);
            // };
            // if (!_inHouseAdsManager.ShowInHouseAdsIfAppropriate(delegate(InHouseAdsPlacement placement)
            // {
            // 	_inHouseAdsDisplayHandler.CloseDelegate += closeAdsCompletion;
            // 	_inHouseAdsDisplayHandler.Show(placement, InHouseDisplaySource.EndGame);
            // 	return true;
            // }))
            // {
            // 	closeAdsCompletion(_inHouseAdsDisplayHandler);
            // }
        }

        public void RouteToCustomRatingPopupFromEndGame()
        {
            _ratingPopup.Show();
        }

        public ChordsStrings GetChordsStrings()
        {
            return _chordsStrings;
        }

        public void RouteToSplashStoreFromStartAppWithDelay(float delay)
        {
            StartCoroutine(DisplaySplashStoreWithDelayEnumerator(delay));
        }

        public Blur GetBlurObject()
        {
            if (_qualityManager.GetQualityOption() == QualityOption.Low) return null;
            return _screenInstanceLocator.GetBlure();
        }

        public void SetActiveBlurCamera(bool active)
        {
            if (_qualityManager.GetQualityOption() != 0) GetBlurObject().gameObject.SetActive(active);
        }

        public void Initialize()
        {
            RegisterScreens();
            SetInitialStatus();
        }

        private void RegisterScreens()
        {
            _qualityManager = ApplicationGraph.GetQualityManager();
            _scoreManager = ApplicationGraph.GetScoreManager();
            var gameEventSender = ApplicationGraph.GetGameEventSender();
            // var inappManager = ApplicationGraph.GetInappManager();
            // var subscriptionManager = ApplicationGraph.GetSubscriptionManager();
            var localizationManager = ApplicationGraph.GetLocalizationManager();
            // StorePresenterImpl storePresenterImpl = new StorePresenterImpl(_storeScreen, inappManager, gameEventSender, subscriptionManager, localizationManager);
            // _storeScreen.Initialize(storePresenterImpl);
            // StoreRouterImpl storeDisplayer = (StoreRouterImpl)(_storeRouter = new StoreRouterImpl(storePresenterImpl, gameEventSender));
            // _storeRouter.ShowFreeModeInterface += delegate
            // {
            // 	_blackFilter.SetActive(value: false);
            // 	SetActiveBlurCamera(active: false);
            // 	_freeModeScreen.SetVisible(visible: true);
            // };
            // _storeRouter.ShowPauseInGameInterface += delegate
            // {
            // 	GameModeScreen.PauseMenu.SetVisible(visible: true);
            // 	_blackFilter.SetActive(value: true);
            // 	SetActiveBlurCamera(active: true);
            // };
           
            // _storeManagerScreen._instance.ShowGuitarSelectionMenuInterface += delegate
            // {
            // 	GuitarConfigurationScreen.Show(_mainCamera);
            // };
            // _storeManagerScreen._instance.ShowTrackList += delegate
            // {
            // 	TrackListScreen.gameObject.SetActive(value: true);
            // };
            // _inHouseAdsDisplayHandler = new InHouseAdsDisplayHandler(storeDisplayer, ApplicationGraph.GetExternalAdsWrapper(), gameEventSender, inappManager, this, "InterstitialPlacement");
            // _inHouseAdsDisplayHandler.PreloadExternalAds();
            // _inHouseAdsManager = new InHouseAdsManagerPersistent((InHouseAdsStorage)new InHouseAdsStorageFile(), Time.realtimeSinceStartup, inappManager, ApplicationGraph.GetApplicationStatusHolder());
        }

        private void SetInitialStatus()
        {
            //_launchScreen.SetActive(true);
            _freeModeScreen.SetVisible(false);
            _loader.SetActive(false);
            _storeScreen.SetVisible(false);
            _guitarStyleScreenAdapter.SetActive(false);
            _ratingPopup.gameObject.SetActive(false);
        }

        [UsedImplicitly]
        public void RouteToTrackListFromFreeMode()
        {
            _freeModeScreen.SetVisible(false);
            _blackFilter.SetActive(true);
            SetActiveBlurCamera(true);
            TrackListScreen.gameObject.SetActive(true);
        }

        [UsedImplicitly]
        public void RouteToGuitarListFromFreeMode()
        {
            _freeModeScreen.SetVisible(false);
            _blackFilter.SetActive(true);
            SetActiveBlurCamera(true);
            GuitarConfigurationScreen.Show(_mainCamera);
        }

        [UsedImplicitly]
        public void RouteToSettingsFromFreeMode()
        {
            _freeModeScreen.SetVisible(false);
            _blackFilter.SetActive(true);
            SetActiveBlurCamera(true);
            _settingsScreen.Show();
        }

        [UsedImplicitly]
        public void RouteFreeModeFromSettings()
        {
            _settingsScreen.Hide();
            _blackFilter.SetActive(false);
            SetActiveBlurCamera(false);
            _freeModeScreen.SetVisible(true);
        }

        [UsedImplicitly]
        public void RouteToChordsSelectorFromFreeMode()
        {
            _freeModeScreen.SetVisible(false);
            _blackFilter.SetActive(true);
            SetActiveBlurCamera(true);
            ChordsSelector.gameObject.SetActive(true);
        }

        [UsedImplicitly]
        public void RouteToStoreFromFreeMode()
        {
            RouteToStoreFromFreeMode(false);
        }

        private IEnumerator DelayShowStoreFromOpeningApp()
        {
            yield return new WaitForSeconds(0.5f);
            _freeModeScreen.SetVisible(false);
            _blackFilter.SetActive(true);
            SetActiveBlurCamera(true);
            _storeRouter.ShowStoreFromOpeningApp();
        }

        private void DiableGameModeContainer()
        {
            GameModeScreen.gameObject.SetActive(false);
        }

        private IEnumerator DisplaySplashStoreWithDelayEnumerator(float delay)
        {
            yield return new WaitForSeconds(delay);
            _splashstoreScreen.ShowSplashStore(ScreenTransitionOrientation.Bottom);
        }
    }
}