using System.Collections;
using MWM.GameEvent;
using UnityEngine;
using UnityEngine.SceneManagement;
using EventType = MWM.GameEvent.EventType;

namespace GuitarApplication
{
    public class ApplicationLauncher : MonoBehaviour
    {
        public GameEventSender gameEventSender;

        public Loader loader;

        private void Start()
        {
            // gameEventSender.SendEvent(EventType.ApplicationStart, string.Empty);
            loader.SetDisplay(true);
            StartCoroutine(LaunchRoutine());
        }

        private IEnumerator LaunchRoutine()
        {
            yield return new WaitForSeconds(2f);
            yield return SceneManager.LoadSceneAsync("MainScene");
        }
    }
}