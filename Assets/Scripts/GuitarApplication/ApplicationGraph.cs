using System;
using System.Collections.Generic;
using ABTest;
using AdditionalContent;
using Ads;
using Async;
using Config;
using Devices;
using GlobalInput;
using i18n;
using InAppOptimization;
using LaunchFlow;
using MWM.Ads;
using MWM.Audio;
using MWM.GameEvent;
using MWM.Guitar.Quality;
using Notification;
using QuitDialog;
using SamplerScripts.AdditionalContent;
using SongCatalog;
using SongRepository;
using Store;
using Subscription;
using UnityEngine;
using Utils;

namespace GuitarApplication
{
    public class ApplicationGraph : MonoBehaviour
    {
        private static ApplicationGraph _instance;

        [SerializeField] private ScreenInstanceLocator _screenInstanceLocator;

        [SerializeField] private ApplicationRouterBehaviour _router;

        [SerializeField] private iOSNativeInappEventSender _iOSNativeInappEventSender;

        [SerializeField] private AsyncJobExecutorBehaviour _internalAsyncJobExecutorBehaviour;

        [SerializeField] private SongRepositoryBehavior InternalSongRepository;

        [SerializeField] private AdditionalContentSynchronizerImpl _additionalContentSynchronizer;

        [SerializeField] private MWMMoPubWrapper _MWMMoPubWrapper;

        [SerializeField] private FakeEditorAdView _fakeEditorAdView;

        [SerializeField] private GuitarHolder _guitarHolder;

        [SerializeField] private AudioEngineFactory _audioEngineFactory;

        [SerializeField] private GlobalInputManagerImpl InternalGlobalInputManager;

        [SerializeField] private UserSettings _userSettings;

        [SerializeField] private List<Sprite> _charSprites;

        private AdditionalContentProvider _additionalContentProvider;

        private ApplicationStatusHolder _applicationStatusHolder;

        private Dictionary<char, Sprite> _charSpritesDictionary;

        private ConfigStorageContainer.ConfigStorage _configStorage;

        private DeviceManager _deviceManager;

        private ExternalAccompanimentDownloader _externalAccompanimentDownloader;

        private ExternalAdsWrapper _externalAdsWrapper;

        private GameEventSender _gameEventSender;

        private InAppManager _inappManager;

        private InAppOptimizationManager _inAppOptimizationManager;

        [SerializeField] private LocalizationManager _localizationManager;

        private LocalNotificationManager _localNotificationManager;

        private LocalNotificationScheduler _localNotificationScheduler;

        private MusicGamingAudioEngine _musicGamingAudioEngine;

        private QualityManager _qualityManager;

        private QuitDialogManager _quitDialogManager;

        private ScoreManager _scoreManager;

        private SongIdsPlayedStorage _songIdsPlayedStorage;

        private SubscriptionManager _subscriptionManager;

        private WeeklyFeaturedTrackManager _weeklyFeaturedTrackManager;

        private void Awake()
        {
            if (_instance == null)
            {
                _instance = this;
            }
            else if (_instance != this)
            {
                Destroy(this);
                return;
            }

            DontDestroyOnLoad(this);
            Init();
        }

        private void OnDestroy()
        {
            _quitDialogManager.Destroy();
            if (_musicGamingAudioEngine != null) _musicGamingAudioEngine.Destroy();
        }

        private void OnApplicationFocus(bool hasFocus)
        {
            if (_localNotificationManager != null && _localNotificationScheduler != null)
            {
                if (hasFocus)
                    _localNotificationManager.CheckReceivedLocalNotification();
                else
                    _localNotificationScheduler.ScheduleRetentionNotifications();
            }
        }

        public static ApplicationGraph Get()
        {
            if (_instance == null)
                throw new Exception(
                    "Application graph not instantiated. The application graph behaviour must be attached to a game object of the very first scene of the app.");
            return _instance;
        }

        public static ApplicationRouter GetApplicationRouter()
        {
            return Get()._router;
        }
        
       

        public static DeviceManager GetDeviceManager()
        {
            return Get()._deviceManager;
        }

        public static InAppOptimizationManager GetInAppOptimizationManager()
        {
            return Get()._inAppOptimizationManager;
        }

        public static AsyncJobExecutor GetAsyncJobExecutor()
        {
            return Get()._internalAsyncJobExecutorBehaviour;
        }

        public static InAppManager GetInappManager()
        {
            return Get()._inappManager;
        }

        public static IGameEventSender GetGameEventSender()
        {
            return Get()._gameEventSender;
        }

        public static ExternalAdsWrapper GetExternalAdsWrapper()
        {
            return Get()._externalAdsWrapper;
        }

        public static GlobalInputManager GetGlobalInputManager()
        {
            return Get().InternalGlobalInputManager;
        }

        public static ApplicationStatusHolder GetApplicationStatusHolder()
        {
            return Get()._applicationStatusHolder;
        }

        public static QualityManager GetQualityManager()
        {
            return Get()._qualityManager;
        }

        public static ConfigStorageContainer.ConfigStorage GetConfigStorage()
        {
            return Get()._configStorage;
        }

        public static WeeklyFeaturedTrackManager GetWeeklyFeaturedTrackManager()
        {
            return Get()._weeklyFeaturedTrackManager;
        }

        public static LocalizationManager GetLocalizationManager()
        {
            return Get()._localizationManager;
        }

        public static AdditionalContentProvider GetAdditionalContentProvider()
        {
            return Get()._additionalContentProvider;
        }

        public static SongRepository.SongRepository GetSongRepository()
        {
            return Get().InternalSongRepository;
        }

        public static ExternalAccompanimentDownloader GetExternalAccompanimentDownloader()
        {
            return Get()._externalAccompanimentDownloader;
        }

        public static MusicGamingAudioEngine GetMusicGamingAudioEngine()
        {
            return Get()._musicGamingAudioEngine;
        }

        public static SongIdsPlayedStorage GetSongIdsPlayedStorage()
        {
            return Get()._songIdsPlayedStorage;
        }

        public static ScoreManager GetScoreManager()
        {
            return Get()._scoreManager;
        }

        public static UserSettings GetUserSettings()
        {
            return Get()._userSettings;
        }

        public static Dictionary<char, Sprite> GetCharSprites()
        {
            return Get()._charSpritesDictionary;
        }

        public static SubscriptionManager GetSubscriptionManager()
        {
            return Get()._subscriptionManager;
        }

        private void Init()
        {
            _deviceManager = new DeviceModule().CreateDeviceManager();
            _gameEventSender = GameEventSender.Instance();
            _inAppOptimizationManager = new InAppOptimizationModule().CreateInAppOptimizationManager();
            // _gameEventSender.SetInAppOptimizationManager(_inAppOptimizationManager);
            _charSpritesDictionary = new Dictionary<char, Sprite>
            {
                {
                    '#',
                    _charSprites[0]
                },
                {
                    '1',
                    _charSprites[1]
                },
                {
                    '2',
                    _charSprites[2]
                },
                {
                    '3',
                    _charSprites[3]
                },
                {
                    '4',
                    _charSprites[4]
                },
                {
                    '5',
                    _charSprites[5]
                },
                {
                    '6',
                    _charSprites[6]
                },
                {
                    '7',
                    _charSprites[7]
                },
                {
                    '8',
                    _charSprites[8]
                },
                {
                    '9',
                    _charSprites[9]
                },
                {
                    'a',
                    _charSprites[10]
                },
                {
                    'b',
                    _charSprites[11]
                },
                {
                    'c',
                    _charSprites[12]
                },
                {
                    'd',
                    _charSprites[13]
                },
                {
                    'e',
                    _charSprites[14]
                },
                {
                    'f',
                    _charSprites[15]
                },
                {
                    'g',
                    _charSprites[16]
                }
            };

            var storageManager = new AdditionalContentProviderStorageImpl();
            _externalAccompanimentDownloader =
                new ExternalAccompanimentDownloaderImpl(_internalAsyncJobExecutorBehaviour, _gameEventSender);
            _additionalContentProvider = new AdditionalContentProviderImpl(storageManager,
                _externalAccompanimentDownloader, InternalSongRepository);

            if (_router == null)
                throw new ArgumentException("A Application Router Behaviour must be provided to the application graph");

            _router.gameObject.SetActive(true);
            if (_screenInstanceLocator == null)
                throw new ArgumentException("A Screen Instance Locator must be provided to the application graph");

            _screenInstanceLocator.gameObject.SetActive(true);
            if (InternalSongRepository == null)
                throw new ArgumentException("A Song Repository must be provided to the application graph");
            InternalSongRepository.gameObject.SetActive(true);
            if (_additionalContentSynchronizer == null)
                throw new ArgumentException(
                    "An AdditionalContentSynchronizer must be attached to the PatchLoadingSceneGraph");
            _additionalContentSynchronizer.gameObject.SetActive(true);
            if (InternalGlobalInputManager == null)
                throw new ArgumentException("A InternalGlobalInputManager must be provided to the application graph");
            InternalGlobalInputManager.gameObject.SetActive(true);
            if (_internalAsyncJobExecutorBehaviour == null)
                throw new ArgumentException("An async job executor must be provided to the ApplicationGraph.");
            _internalAsyncJobExecutorBehaviour.gameObject.SetActive(true);
            var qualityStorage = new QualityStorageFile();
            _qualityManager = new QualityManager(qualityStorage);
            var musicGamingAudioEngineRef = new ExternalAdsWrapperImpl.MusicGamingAudioEngineRef();
            // _MWMMoPubWrapper.gameObject.SetActive(value: true);
            // _MWMMoPubWrapper.coroutineLauncher = this;
            // _externalAdsWrapper = new ExternalAdsWrapperImpl(_MWMMoPubWrapper, musicGamingAudioEngineRef);
            _localizationManager = new LocalizationManager();
            _songIdsPlayedStorage = new SongIdsPlayedStorageImpl();
            _scoreManager = new ScoreManager();
            _configStorage = new ConfigStoragePersistent();
            _weeklyFeaturedTrackManager = new WeeklyFeaturedTrackManagerWeb(this, _configStorage, Device.Default());
            _applicationStatusHolder = new ApplicationStatusHolderWeb(this);
            _iOSNativeInappEventSender.Initialize(_gameEventSender);
            // _inappManager = CreateInAppManager();
            // _inappManager.VerifySubscription();
            //_subscriptionManager = new SubscriptionManagerImpl(_inappManager, _deviceManager, InAppConst.GetSubscriptionVariationSku("subscription.weekly.07.07.99", _deviceManager.GetDeviceTarget()));
            _router.Initialize();
            var applicationStatusHolder = GetApplicationStatusHolder();
            _audioEngineFactory.gameObject.SetActive(true);
            var soundSystemLoader = new LoadSoundSystemStep(this, _audioEngineFactory, _guitarHolder);
            // IntroCarouselInstanciator instanciator = new IntroCarouselInstanciator(_screenInstanceLocator, _inappManager, _router, applicationStatusHolder, _gameEventSender, _configStorage);
            var storage = new ABTestStorage();
            // MonoBehaviourCoroutineExecutorAdapter coroutineExecutor = new MonoBehaviourCoroutineExecutorAdapter(this);
            var aBTestEventDataBuilder = new ABTestProxy(storage, _gameEventSender.GetInstallationIdProvider());
            // IntroCarouselAssetProxy assetProxy = new IntroCarouselAssetProxy(this);
            _gameEventSender.SetABTestEventDataBuilder(aBTestEventDataBuilder);
            _gameEventSender.SetConfigStorage(_configStorage);
            // IntroCarouselPersistenceWrapper displayPolicy = new IntroCarouselPersistenceWrapper();
            // DisplayCarouselStep displayCarouselStep = new DisplayCarouselStep(instanciator, displayPolicy, _router, assetProxy, _gameEventSender, _deviceManager);
            var loadGuitarSkinStep = new LoadGuitarSkinStep(_guitarHolder, this);
            _quitDialogManager = new QuitDialogManagerImpl(InternalGlobalInputManager);
            _localNotificationManager = CreateLocalNotificationManager();
            _localNotificationScheduler =
                new LocalNotificationSchedulerImpl(_localNotificationManager, _localizationManager);
            var launchFlowController = new LaunchFlowController(this, soundSystemLoader, _additionalContentSynchronizer,
                _router, loadGuitarSkinStep, Statistics.Instance, _inappManager, applicationStatusHolder,
                _localNotificationManager, _externalAdsWrapper, _inAppOptimizationManager, _subscriptionManager,
                _deviceManager, _weeklyFeaturedTrackManager, _configStorage);
            launchFlowController.Run(delegate(MusicGamingAudioEngine audioEngine)
            {
                musicGamingAudioEngineRef.engine = audioEngine;
                _musicGamingAudioEngine = audioEngine;
            });
        }

        // private InAppManager CreateInAppManager()
        // {
        // 	InAppManager inAppManager = new InAppManagerImpl(this, _configStorage, _deviceManager);
        // 	return new InAppManagerWrapperWithLoader(inAppManager, ManagerLoader);
        // }

        private LocalNotificationManager CreateLocalNotificationManager()
        {
            return new LocalNotificationManagerAndroid(_gameEventSender);
        }

        private void ManagerLoader(bool visible)
        {
            if (visible)
                _router.ShowLoader();
            else
                _router.HideLoader();
        }
    }
}