using Song;

namespace GuitarApplication
{
    public interface IApplicationRouter : IApplicatoinLoaderDisplayer
    {
        void RouteToStoreFromClickToLockedGuitar();

        void RouteFreeModeFromGuitarList();

        void RouteToInGameFromTrackList(ISong song);

        void RouteToFreeModeFromTrackList();

        void RouteToStoreFromClickVIPTrackListCell();

        void RouteToStoreFromTrackListNavBar();

        void RouteToFreeModeFromChordSelectior();

        void RouteToInGameFromEndScreenWithNextTrack(ISong track);

        void RouteToFreeMode();

        void HideLaunchScreen();
    }
}