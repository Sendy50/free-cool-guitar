using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

namespace GuitarApplication
{
    [Serializable]
    public class BaseStatisticPersistence
    {
        private const string PersistentFileName = "/baseStatistics.dat";

        public int AppLaunches;

        public int TotalNumberOfGames;

        public string LastLaunchAppVersion;

        public static BaseStatisticPersistence loadFromFile()
        {
            if (File.Exists(Application.persistentDataPath + "/baseStatistics.dat"))
            {
                var binaryFormatter = new BinaryFormatter();
                var serializationStream =
                    File.Open(Application.persistentDataPath + "/baseStatistics.dat", FileMode.Open);
                BaseStatisticPersistence temp = (BaseStatisticPersistence) binaryFormatter.Deserialize(serializationStream);
                serializationStream.Close();
                return temp;
            }

            return new BaseStatisticPersistence();
        }

        public void saveToFile()
        {
            var binaryFormatter = new BinaryFormatter();
            var serializationStream = File.Create(Application.persistentDataPath + "/baseStatistics.dat");
            binaryFormatter.Serialize(serializationStream, this);
            serializationStream.Close();
        }
    }
}