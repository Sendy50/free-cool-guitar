namespace GuitarApplication
{
    public class ApplicationConstant
    {
        public static readonly string ApplicationLoadingSceneName = "ApplicationLoadingScene";

        public static readonly string MainSceneName = "MainScene";
    }
}