using Rating;
using UnityEngine;

namespace GuitarApplication
{
    public class Statistics : IStatistics
    {
        private static Statistics instance;

        private readonly BaseStatisticPersistence _baseStats;

        private bool _didDisplayRatingRequestThisSession;

        private readonly RatingStatisticPersistence _ratingStats;

        public Statistics()
        {
            _didDisplayRatingRequestThisSession = false;
            _baseStats = BaseStatisticPersistence.loadFromFile();
            _ratingStats = RatingStatisticPersistence.loadFromFile();
        }

        public static Statistics Instance
        {
            get
            {
                if (instance == null) instance = new Statistics();
                return instance;
            }
        }

        public void IncrementNumAppLaunches()
        {
            _baseStats.AppLaunches++;
            var version = Application.version;
            if (_baseStats.LastLaunchAppVersion == null)
            {
                _baseStats.LastLaunchAppVersion = version;
            }
            else if (!version.Equals(_baseStats.LastLaunchAppVersion))
            {
                OnNewAppVersionDetected();
                _baseStats.LastLaunchAppVersion = version;
            }

            _baseStats.saveToFile();
        }

        public int GetNumberAppLaunches()
        {
            return _baseStats.AppLaunches;
        }

        public void IncrementTotalNumberOfGames()
        {
            _baseStats.TotalNumberOfGames++;
            _baseStats.saveToFile();
        }

        public int GetTotalNumberOfGames()
        {
            return _baseStats.TotalNumberOfGames;
        }

        public void IncrementNumRatingRequestDisplay()
        {
            _ratingStats.NumRatingRequestForThisVersion++;
            _ratingStats.saveToFile();
        }

        public int GetNumRatingRequestDisplayForCurrentAppVersion()
        {
            return _ratingStats.NumRatingRequestForThisVersion;
        }

        public bool GetDidDisplayRatingRequestThisSession()
        {
            return _didDisplayRatingRequestThisSession;
        }

        public void SetDidDisplayRatingRequestThisSession(bool didDisplay)
        {
            _didDisplayRatingRequestThisSession = didDisplay;
        }

        public void IncrementNumRatingRequestAccepted()
        {
            _ratingStats.NumRatingAcceptedForThisVersion++;
            _ratingStats.saveToFile();
        }

        public int GetNumRatingRequestAcceptedForCurrentAppVersion()
        {
            return _ratingStats.NumRatingAcceptedForThisVersion;
        }

        private void OnNewAppVersionDetected()
        {
            _ratingStats.NumRatingRequestForThisVersion = 0;
            _ratingStats.NumRatingAcceptedForThisVersion = 0;
            _ratingStats.saveToFile();
        }
    }
}