using System;
using System.Collections;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using UnityEngine;
using UnityEngine.Networking;
using Utils;

namespace GuitarApplication
{
    public class ApplicationStatusHolderWeb : ApplicationStatusHolder
    {
        private const string EndPoint = "https://us-central1-djit-sas.cloudfunctions.net/remote-config-learning-guitar";

        private const string VersionParamKey = "version";

        private const string PlatformParamKey = "platform";

        private const string ResponseStatusKey = "status";

        private const string DevAppStatusValue = "dev";

        private const string DeployedAppStatusValue = "deployed";

        private const int DefaultTimeoutDuration = 3;

        private readonly MonoBehaviour _coroutineExecutor;

        private readonly int _timeout;

        private bool _didReceiveServerResponse;

        private AppStatus _internalAppStatus = AppStatus.NoDeployed;

        public ApplicationStatusHolderWeb(MonoBehaviour coroutineExecutor, int timeout = 3)
        {
            Precondition.CheckNotNull(coroutineExecutor);
            _coroutineExecutor = coroutineExecutor;
            _timeout = timeout;
            _didReceiveServerResponse = false;
        }

        public bool IsDeployed()
        {
            return _internalAppStatus != AppStatus.NoDeployed;
        }

        public bool DidReceiveStatusFromServer()
        {
            return _didReceiveServerResponse;
        }

        public void RunStep(Action callback)
        {
            if (Application.internetReachability == NetworkReachability.NotReachable)
            {
                _internalAppStatus = AppStatus.NoDeployed;
                callback?.Invoke();
            }
            else
            {
                var runtimeTypeName = ApplicationGraph.GetDeviceManager().GetRuntimeTypeName();
                _coroutineExecutor.StartCoroutine(AppStatusRetrievingRequest(runtimeTypeName, callback));
            }
        }

        private IEnumerator AppStatusRetrievingRequest(string devicePlatformName, Action callback)
        {
            var request =
                UnityWebRequest.Get(
                    "https://us-central1-djit-sas.cloudfunctions.net/remote-config-learning-guitar?version=" +
                    Application.version + "&platform=" + devicePlatformName);
            request.timeout = _timeout;
            var unityWebRequest = request;
            yield return request.SendWebRequest();
            if (request.isNetworkError || request.isHttpError)
            {
                _internalAppStatus = AppStatus.NoDeployed;
                callback?.Invoke();
                yield break;
            }

            JObject responseDictionary;
            try
            {
                var text = request.downloadHandler.text;
                responseDictionary = JObject.Parse(text);
            }
            catch (JsonReaderException arg)
            {
                Debug.LogWarning("[ApplicationStatusHolder] Server response parsing fail: " + arg);
                _internalAppStatus = AppStatus.NoDeployed;
                callback?.Invoke();
                yield break;
            }

            _didReceiveServerResponse = true;
            switch (responseDictionary["status"].ToObject<string>())
            {
                case "dev":
                    _internalAppStatus = AppStatus.Dev;
                    break;
                case "deployed":
                    _internalAppStatus = AppStatus.Deployed;
                    break;
                default:
                    _internalAppStatus = AppStatus.NoDeployed;
                    break;
            }

            callback?.Invoke();
        }

        private enum AppStatus
        {
            Dev,
            NoDeployed,
            Deployed
        }
    }
}