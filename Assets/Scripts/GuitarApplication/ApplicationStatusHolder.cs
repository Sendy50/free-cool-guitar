using System;

namespace GuitarApplication
{
    public interface ApplicationStatusHolder
    {
        bool IsDeployed();

        bool DidReceiveStatusFromServer();

        void RunStep(Action callback);
    }
}