namespace GuitarApplication
{
    public interface IApplicatoinLoaderDisplayer
    {
        void ShowLoader();

        void HideLoader();
    }
}