using System;
using UnityEngine;

public interface IGuitarCell
{
    event Action onCellClick;

    void SetGuitarName(string text);

    void SetGuitarSprite(Sprite sprite);

    void SetStatText(string text);

    void SetVisibleIcone(GuitarCellIcon icon);
}