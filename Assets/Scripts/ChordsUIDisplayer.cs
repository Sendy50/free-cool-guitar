using System.Collections;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class ChordsUIDisplayer : MonoBehaviour
{
    public ChordUI chordUIPrefab;

    public UserSettings userSettings;

    public HorizontalLayoutGroup horizontalLayoutGroup;

    private Coroutine _currentChordDisplayRefreshCoroutine;

    public ChordUI[] displayedChords { get; private set; }

    private void Start()
    {
        UpdateOrientation();
        userSettings.GameOrientationChanged += OnOrientationChange;
    }

    private void OnDestroy()
    {
        userSettings.GameOrientationChanged -= OnOrientationChange;
    }

    private void OnOrientationChange(UserSettings.GameOrientation gameOrientation)
    {
        UpdateOrientation();
    }

    private void UpdateOrientation()
    {
        var component = GetComponent<HorizontalLayoutGroup>();
        if (userSettings.GetGameOrientation() == UserSettings.GameOrientation.RightHanded)
            component.childAlignment = TextAnchor.LowerRight;
        else
            component.childAlignment = TextAnchor.LowerLeft;
    }

    public void SetChordsToDisplay(IChord[] chords)
    {
        if (this.displayedChords != null)
        {
            var displayedChords = this.displayedChords;
            foreach (var chordUI in displayedChords) Destroy(chordUI.gameObject);
        }

        this.displayedChords = (from c in chords
            select c.UI(chordUIPrefab, gameObject)).ToArray();
        
        int i = 0;
        foreach (ChordUI chordUI in displayedChords)
        {
            chordUI.index = i;
            i++;
        }
    }

    public void SetTouchDetectionEnable(bool enable)
    {
        var displayedChords = this.displayedChords;
        foreach (var chordUI in displayedChords) chordUI.SetTouchDetectionEnable(enable);
    }

    public void StartCurrentlyDisplayedChordRoutine(ICurrentChordProvider currentChordProvider)
    {
        if (_currentChordDisplayRefreshCoroutine == null)
            _currentChordDisplayRefreshCoroutine =
                StartCoroutine(CurrentlyDisplayedChordUpdateRoutine(currentChordProvider));
    }

    public void StopCurrentlyDisplayedChordRoutine()
    {
        if (_currentChordDisplayRefreshCoroutine != null)
        {
            StopCoroutine(_currentChordDisplayRefreshCoroutine);
            _currentChordDisplayRefreshCoroutine = null;
        }
    }

    private IEnumerator CurrentlyDisplayedChordUpdateRoutine(ICurrentChordProvider currentChordProvider)
    {
        while (true)
        {
            var currentChordHashCode = currentChordProvider.GetCurrentMidiChord().GetHashCode();
            UpdateDisplayedChord(currentChordHashCode);
            yield return new WaitUntil(() =>
                currentChordHashCode != currentChordProvider.GetCurrentMidiChord().GetHashCode());
        }
    }

    private void UpdateDisplayedChord(int displayedChordHash)
    {
        var displayedChords = this.displayedChords;
        var num = displayedChords.Length;
        for (var i = 0; i < num; i++)
        {
            var chordUI = displayedChords[i];
            chordUI.SetSelected(chordUI.hash == displayedChordHash);
        }
    }
}