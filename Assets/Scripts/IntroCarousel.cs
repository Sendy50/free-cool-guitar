using System;
using System.Collections;
using System.Collections.Generic;
using Config;
using Devices;
using GuitarApplication;
using i18n;
using MWM.GameEvent;
using Store;
using Subscription;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using EventType = MWM.GameEvent.EventType;

public class IntroCarousel : MonoBehaviour
{
    public delegate void OnExitCarousel(IntroCarousel sender);

    public static readonly int MaxNumberOfPage = 4;

    private static readonly Color PlatformAndroidBlueColor = new Color(0.152f, 0.87f, 1f);

    public IntroCarouselScrollSnap ScrollSnap;

    public IntroCarouselSwipeRecognizer SwipeRecognizer;

    public IntroCarouselPager Pager;

    [SerializeField] private IntroCarouselSlide _lastSlide;

    public Button NextButton;

    public Button PurchaseButton;

    [SerializeField] private Button _accessPremiumButton;

    public GameObject ContinueButton;

    [SerializeField] private TextMeshProUGUI _iosLegalText;

    [SerializeField] private TextMeshProUGUI _androidLegalText;

    public CanvasGroup[] DescriptionContainersCanvasGroup;

    [SerializeField] private CanvasGroup _freePremiumDescriptionCanvasGroup;

    [SerializeField] private CanvasGroup _iosLegalTextCanvasGroup;

    [SerializeField] private CanvasGroup _androidLegalTextCanvasGroup;

    [SerializeField] private GameObject _cancelAtAnyTimeHint;

    [SerializeField] private List<Text> _titlesSlide;

    [SerializeField] private List<Image> _iconsSlide;

    [SerializeField] private List<Image> _btnBackgrounds;

    [SerializeField] private List<Text> _btnTexts;

    [SerializeField] private Sprite _spriteBtnBackgroundBlue;

    [SerializeField] private Text _purchaseSlideFreeTrialPeriod;

    [SerializeField] private Text _purchaseSlidePrice;

    private CanvasGroup _currentlyDisplayedDescriptionCanvasGroup;

    private DeviceTarget _deviceTarget;

    private bool _didAppendLastSlide;

    private bool _didPerformLastSlideInsertionCheck;

    private bool _didSendLastPageDisplayStat;

    private bool _didSendSecondSlideDisplayStat;

    private bool _didSendThirdSlideDisplayStat;

    private GameObject _legalText;

    private CanvasGroup _legalTextCanvasGroup;

    [HideInInspector] public LocalizationManager _localizationManager;

    private bool _offeredPremiumVersion;

    private IIntroCarouselDisplayPolicy _persistence;

    [HideInInspector] public SubscriptionManager _subscriptionManager;

    [HideInInspector] public ApplicationStatusHolder appStatusHolder;

    [HideInInspector] public ConfigStorageContainer.ConfigStorage configStorage;

    [HideInInspector] public IGameEventSender gameEventSender;

    [HideInInspector] public InAppManager inappManager;

    private bool _isLeavingPurchasePage => _didAppendLastSlide && IsLastSlide(ScrollSnap.PreviousPageIndex);

    private void Awake()
    {
        _subscriptionManager = ApplicationGraph.GetSubscriptionManager();
        _localizationManager = ApplicationGraph.GetLocalizationManager();
        var deviceManager = ApplicationGraph.GetDeviceManager();
        _deviceTarget = deviceManager.GetDeviceTarget();
        _didAppendLastSlide = false;
        _didSendLastPageDisplayStat = false;
        _offeredPremiumVersion = false;
        _didPerformLastSlideInsertionCheck = false;
        _currentlyDisplayedDescriptionCanvasGroup = DescriptionContainersCanvasGroup[0];
        _persistence = new IntroCarouselPersistenceWrapper();
        switch (_deviceTarget)
        {
            case DeviceTarget.Android:
                _iosLegalText.text = ApplicationGraph.GetLocalizationManager()
                    .GetLocalizedValue("store_android_terms_of_user");
                _legalText = _androidLegalText.gameObject;
                _legalTextCanvasGroup = _androidLegalTextCanvasGroup;
                break;
            case DeviceTarget.Ios:
                _iosLegalText.text = ApplicationGraph.GetLocalizationManager()
                    .GetLocalizedValue("store_apple_terms_of_user");
                _legalText = _iosLegalText.gameObject;
                _legalTextCanvasGroup = _iosLegalTextCanvasGroup;
                break;
            default:
                throw new Exception("IntroCarousel: Platform not supported for legal text");
        }

        NextButton.GetComponentInChildren<Text>().text = GetNextButtonTextForPageIndex(0);
    }

    public event OnExitCarousel ExitCarouselDelegate;

    public void Display()
    {
        if (_deviceTarget == DeviceTarget.Android)
        {
            foreach (var item in _titlesSlide) item.color = PlatformAndroidBlueColor;
            foreach (var item2 in _iconsSlide) item2.color = PlatformAndroidBlueColor;
            foreach (var btnBackground in _btnBackgrounds) btnBackground.sprite = _spriteBtnBackgroundBlue;
            foreach (var btnText in _btnTexts) btnText.color = PlatformAndroidBlueColor;
        }

        gameObject.SetActive(true);
        SwipeRecognizer.StartInputCoroutine();
        SwipeRecognizer.SwipeDetected += OnSwipeDetected;
        gameEventSender.SendEvent(EventType.IntroCarouselDisplay, string.Empty);
        GetSlideAtIndex(ScrollSnap.CurrentPageIndex).OnSlideWillAppear();
        if (inappManager.GetStatus() == Status.Initialized)
        {
            DisplayInApp();
            return;
        }

        _purchaseSlideFreeTrialPeriod.text = "----";
        _purchaseSlidePrice.text = "----";
        inappManager.OnInAppInitializedDelegate += OnInAppManagerInitialized;
    }

    public IntroCarouselSlide GetSlideAtIndex(int index)
    {
        if (PageIndexToNumber(index) == IntroCarouselSlideNumber.four) return _lastSlide;
        return ScrollSnap.GetChildObjectAtIndex(index).GetComponent<IntroCarouselSlide>();
    }

    private IntroCarouselSlideNumber PageIndexToNumber(int pageIndex)
    {
        switch (pageIndex)
        {
            case 0:
                return IntroCarouselSlideNumber.one;
            case 1:
                return IntroCarouselSlideNumber.two;
            case 2:
                return IntroCarouselSlideNumber.three;
            case 3:
                return IntroCarouselSlideNumber.four;
            default:
                throw new ArgumentException("Invalid page index");
        }
    }

    public void OnNextButtonClicked()
    {
        if (IsLastSlide(ScrollSnap.CurrentPageIndex) && !_didAppendLastSlide)
        {
            gameEventSender.SendEvent(EventType.IntroCarouselCloseFromNoSubscriptionSlide, string.Empty);
            ExitCarousel();
        }
        else
        {
            ScrollSnap.TransitionToNextScreen();
        }
    }

    public void OnContinueWithLimitedVersionClicked()
    {
        gameEventSender.SendEvent(EventType.IntroCarouselCloseFromSubscriptionSlide,
            IntroCarouselCloseFromSubscriptionSlideReason.CloseButton.StringValue());
        ExitCarousel();
    }

    public void OnPurchaseButtonClicked()
    {
        PurchaseWeeklyPremium();
    }

    public void OnAccessPremiumVersionClicked()
    {
        gameEventSender.SendEvent(EventType.IntroCarouselCloseFromSubscriptionSlide,
            IntroCarouselCloseFromSubscriptionSlideReason.CloseButton.StringValue());
        ExitCarousel();
    }

    private void PurchaseWeeklyPremium()
    {
        var inAppData = inappManager.Buy(_subscriptionManager.GetSku(), delegate(Payment.Result success)
        {
            if (success.IsSuccess)
            {
                gameEventSender.SendEvent(EventType.IntroCarouselCloseFromSubscriptionSlide,
                    IntroCarouselCloseFromSubscriptionSlideReason.Buy.StringValue());
                ExitCarousel();
            }
            else
            {
                var mNPopup = new MNPopup("Error", "Could not complete transaction.");
                mNPopup.AddAction("OK", delegate { });
                mNPopup.Show();
            }
        });
        if (inAppData != null)
        {
            var clickBuyEvent = new ClickBuyEvent(ClickBuyEvent.Sources.IntroCarousel, inAppData.ProductId,
                inAppData.CurrencyCode, inAppData.Price);
            gameEventSender.SendEvent(EventType.ClickBuy, clickBuyEvent.ToJsonString());
        }
    }

    public void OnCarouselPageChangeStart()
    {
        NextButton.interactable = false;
        PurchaseButton.interactable = false;
        if (ScrollSnap.PreviousPageIndex != ScrollSnap.CurrentPageIndex)
        {
            if (_offeredPremiumVersion)
                StartCoroutine(FreePremiumDescriptionTransitionRoutine(ScrollSnap.CurrentPageIndex));
            else
                StartCoroutine(DescriptionTransitionRoutine(ScrollSnap.CurrentPageIndex));
        }
    }

    public void OnCarouselPageChangeEnd()
    {
        if (ScrollSnap.CurrentPageIndex == 1 && !_didAppendLastSlide && !_didPerformLastSlideInsertionCheck)
        {
            _didPerformLastSlideInsertionCheck = true;
            if (appStatusHolder.IsDeployed() && !configStorage.GetConfig().FourthSlideDisabledValue)
            {
                _didAppendLastSlide = true;
                ScrollSnap.AddChild(_lastSlide.gameObject, true);
            }

            if (!appStatusHolder.DidReceiveStatusFromServer())
                gameEventSender.SendEvent(EventType.IntroCarouselLastSlideStatusRequestTimeOut, string.Empty);
        }

        if (IsLastSlide(ScrollSnap.CurrentPageIndex) && _didAppendLastSlide && !_didSendLastPageDisplayStat)
        {
            _persistence.OnFourthSlideSeen();
            _didSendLastPageDisplayStat = true;
            gameEventSender.SendEvent(EventType.IntroCarouselDisplaySubscriptionSlide, string.Empty);
        }
        else if (ScrollSnap.CurrentPageIndex == 1 && !_didSendSecondSlideDisplayStat)
        {
            _didSendSecondSlideDisplayStat = true;
            gameEventSender.SendEvent(EventType.IntroCarouselDisplaySecondSlide, string.Empty);
        }
        else if (ScrollSnap.CurrentPageIndex == 2 && !_didSendThirdSlideDisplayStat)
        {
            _didSendThirdSlideDisplayStat = true;
            gameEventSender.SendEvent(EventType.IntroCarouselDisplayThirdSlide, string.Empty);
        }

        if (ScrollSnap.PreviousPageIndex != ScrollSnap.CurrentPageIndex)
        {
            OnNewSlideWillAppear();
            var currentPageIndex = ScrollSnap.CurrentPageIndex;
            GetSlideAtIndex(currentPageIndex).OnSlideWillAppear();
            Pager.OnPageIndexChanged(ScrollSnap.CurrentPageIndex);
            UpdateSlideActiveStateForPageIndex(currentPageIndex);
        }

        NextButton.interactable = true;
        PurchaseButton.interactable = true;
    }

    public void SwitchToPremiumVersionForFree()
    {
        _offeredPremiumVersion = true;
        DescriptionContainersCanvasGroup[DescriptionContainersCanvasGroup.Length - 1] =
            _freePremiumDescriptionCanvasGroup;
    }

    private void OnSwipeDetected(bool leftDirection)
    {
        if (!ScrollSnap.IsAnimating())
        {
            ScrollSnap.CancelDrag();
            if (leftDirection)
                ScrollSnap.TransitionToNextScreen();
            else
                ScrollSnap.TransitionToPreviousScreen();
        }
    }

    private IEnumerator DescriptionTransitionRoutine(int newPageIndex)
    {
        var duration = 0.1f;
        var time2 = 0f;
        var fadeOutValue = 1f;
        var fadeInValue = 0f;
        var newDescriptionCanvasGroup = DescriptionContainersCanvasGroup[newPageIndex];
        var isGoingToPurchaseSlide = _didAppendLastSlide && IsLastSlide(newPageIndex);
        if (isGoingToPurchaseSlide)
        {
            NextButton.gameObject.SetActive(false);
            _legalText.gameObject.SetActive(true);
            _legalTextCanvasGroup.gameObject.SetActive(true);
            PurchaseButton.gameObject.SetActive(true);
            ContinueButton.gameObject.SetActive(true);
        }
        else
        {
            NextButton.gameObject.SetActive(true);
            NextButton.GetComponentInChildren<Text>().text = GetNextButtonTextForPageIndex(newPageIndex);
            PurchaseButton.gameObject.SetActive(false);
        }

        while (fadeOutValue > 0f)
        {
            fadeOutValue = Mathf.Max(Mathf.Lerp(1f, 0f, time2 / duration), 0f);
            _currentlyDisplayedDescriptionCanvasGroup.alpha = fadeOutValue;
            if (_isLeavingPurchasePage)
            {
                _legalTextCanvasGroup.alpha = fadeOutValue;
                ContinueButton.gameObject.GetComponent<CanvasGroup>().alpha = fadeOutValue;
            }

            time2 += Time.deltaTime;
            yield return null;
        }

        time2 = 0f;
        while (fadeInValue < 1f)
        {
            fadeInValue = newDescriptionCanvasGroup.alpha = Mathf.Min(Mathf.Lerp(0f, 1f, time2 / duration), 1f);
            if (isGoingToPurchaseSlide)
            {
                _legalTextCanvasGroup.alpha = fadeInValue;
                ContinueButton.gameObject.GetComponent<CanvasGroup>().alpha = fadeInValue;
            }

            time2 += Time.deltaTime;
            yield return null;
        }

        if (!isGoingToPurchaseSlide)
        {
            _legalText.gameObject.SetActive(false);
            ContinueButton.gameObject.SetActive(false);
        }

        _currentlyDisplayedDescriptionCanvasGroup = newDescriptionCanvasGroup;
    }

    private IEnumerator FreePremiumDescriptionTransitionRoutine(int newPageIndex)
    {
        var duration = 0.1f;
        var time2 = 0f;
        var fadeOutValue = 1f;
        var fadeInValue = 0f;
        var newDescriptionCanvasGroup = DescriptionContainersCanvasGroup[newPageIndex];
        if (_didAppendLastSlide && IsLastSlide(newPageIndex))
        {
            NextButton.gameObject.SetActive(false);
            _accessPremiumButton.gameObject.SetActive(true);
        }
        else
        {
            NextButton.gameObject.SetActive(true);
            NextButton.GetComponentInChildren<Text>().text = GetNextButtonTextForPageIndex(newPageIndex);
            _accessPremiumButton.gameObject.SetActive(false);
        }

        while (fadeOutValue > 0f)
        {
            fadeOutValue = Mathf.Max(Mathf.Lerp(1f, 0f, time2 / duration), 0f);
            _currentlyDisplayedDescriptionCanvasGroup.alpha = fadeOutValue;
            time2 += Time.deltaTime;
            yield return null;
        }

        time2 = 0f;
        while (fadeInValue < 1f)
        {
            fadeInValue = newDescriptionCanvasGroup.alpha = Mathf.Min(Mathf.Lerp(0f, 1f, time2 / duration), 1f);
            time2 += Time.deltaTime;
            yield return null;
        }

        _currentlyDisplayedDescriptionCanvasGroup = newDescriptionCanvasGroup;
    }

    private void OnNewSlideWillAppear()
    {
        for (var i = 0; i < ScrollSnap.ChildObjects.Length; i++) GetSlideAtIndex(i).OnSlideWillDisappear();
    }

    private bool IsLastSlide(int pageIndex)
    {
        return pageIndex == ScrollSnap.ChildObjects.Length - 1;
    }

    private string GetNextButtonTextForPageIndex(int pageIndex)
    {
        var localizationManager = ApplicationGraph.GetLocalizationManager();
        switch (pageIndex)
        {
            case 0:
                return localizationManager.GetLocalizedValue("carousel_slide_1_button");
            case 1:
                return localizationManager.GetLocalizedValue("carousel_slide_2_button");
            case 2:
                return localizationManager.GetLocalizedValue("carousel_slide_3_button");
            default:
                return string.Empty;
        }
    }

    private void UpdateSlideActiveStateForPageIndex(int currentPageIndex)
    {
        var num = Mathf.Max(0, currentPageIndex - 1);
        var num2 = Mathf.Min(currentPageIndex + 1, ScrollSnap.ChildObjects.Length - 1);
        for (var i = 0; i < ScrollSnap.ChildObjects.Length; i++)
            if (i == currentPageIndex || i == num || i == num2)
                ScrollSnap.GetChildObjectAtIndex(i).gameObject.SetActive(true);
            else
                ScrollSnap.GetChildObjectAtIndex(i).gameObject.SetActive(false);
    }

    private void ExitCarousel()
    {
        SwipeRecognizer.StopInputCoroutine();
        SwipeRecognizer.SwipeDetected -= OnSwipeDetected;
        _persistence.OnCarouselCompleted();
        if (ExitCarouselDelegate != null) ExitCarouselDelegate(this);
    }

    private void OnInAppManagerInitialized()
    {
        DisplayInApp();
    }

    private void DisplayInApp()
    {
        var subscriptionDetails = _subscriptionManager.GetSubscriptionDetails();
        var flag = subscriptionDetails.GetFreeTrialPeriod() > 0;
        var purchaseSlideFreeTrialTextIdFromFreeTrial = GetPurchaseSlideFreeTrialTextIdFromFreeTrial(flag);
        var key = !flag
            ? _localizationManager.GetLocalizedValue(purchaseSlideFreeTrialTextIdFromFreeTrial)
            : string.Format(_localizationManager.GetLocalizedValue(purchaseSlideFreeTrialTextIdFromFreeTrial),
                subscriptionDetails.GetFreeTrialPeriod());
        var purchaseSlidePriceTextIdAccordingToSubscriptionDuration =
            GetPurchaseSlidePriceTextIdAccordingToSubscriptionDuration(subscriptionDetails.GetSubscriptionDuration(),
                flag);
        var key2 = string.Format(
            _localizationManager.GetLocalizedValue(purchaseSlidePriceTextIdAccordingToSubscriptionDuration),
            subscriptionDetails.GetPrice());
        _purchaseSlideFreeTrialPeriod.text = _localizationManager.GetLocalizedValue(key);
        _purchaseSlidePrice.text = _localizationManager.GetLocalizedValue(key2);
    }

    private static string GetPurchaseSlideFreeTrialTextIdFromFreeTrial(bool hasFreeTrial)
    {
        return !hasFreeTrial ? "carousel_title_no_free_trial" : "carousel_title_free_trial";
    }

    private static string GetPurchaseSlidePriceTextIdAccordingToSubscriptionDuration(SubscriptionDuration subTime,
        bool hasFreeTrial)
    {
        switch (subTime)
        {
            case SubscriptionDuration.Weekly:
                return !hasFreeTrial ? "carousel_subtitle_price_per_week_2" : "carousel_subtitle_price_per_week";
            case SubscriptionDuration.Monthly:
                return !hasFreeTrial ? "carousel_subtitle_price_per_month_2" : "carousel_subtitle_price_per_month";
            case SubscriptionDuration.Annually:
                return !hasFreeTrial ? "carousel_subtitle_price_per_year_2" : "carousel_subtitle_price_per_year";
            default:
                throw new ArgumentOutOfRangeException("subTime", subTime, null);
        }
    }
}