public interface IGuitarSelectionHandler
{
    bool HandleGuitarSelection(IGuitar selectedGuitar);

    void CloseAndLoadGuitarIfNeeded();

    IGuitar GetPreSelectedGuitar();
}