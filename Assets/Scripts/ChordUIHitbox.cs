using UnityEngine;
using UnityEngine.EventSystems;

public class ChordUIHitbox : MonoBehaviour, IPointerDownHandler, IEventSystemHandler
{
    public delegate void OnChordUITouched();

    public void OnPointerDown(PointerEventData eventData)
    {
        Debug.LogError("ChordSelectBtnClick");
        if (OnChordUITouchedDelegate != null) OnChordUITouchedDelegate();
    }

    public event OnChordUITouched OnChordUITouchedDelegate;
}