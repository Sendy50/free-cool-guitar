using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Networking;
using Utils;

namespace Config
{
    public class WeeklyFeaturedTrackManagerWeb : WeeklyFeaturedTrackManager
    {
        public const int DefaultTimeoutDuration = 3;

        private const string WeeklyFeaturedTrackEndPoint =
            "https://us-central1-djit-sas.cloudfunctions.net/weekly-featured-track-learning-guitar?platform=";

        private readonly ConfigStorageContainer.ConfigStorage _configStorage;

        private readonly MonoBehaviour _coroutineExecutor;

        private readonly IDeviceInfoProvider _deviceManager;

        private readonly int _timeout;

        public WeeklyFeaturedTrackManagerWeb(MonoBehaviour coroutineExecutor,
            ConfigStorageContainer.ConfigStorage configStorage, IDeviceInfoProvider deviceManager, int timeout = 3)
        {
            Precondition.CheckNotNull(coroutineExecutor);
            Precondition.CheckNotNull(configStorage);
            Precondition.CheckNotNull(configStorage);
            _coroutineExecutor = coroutineExecutor;
            _configStorage = configStorage;
            _deviceManager = deviceManager;
            _timeout = timeout;
        }

        public void RequestWeeklyFeaturedTrack(Action<WeeklyFeaturedTrack> callback)
        {
            _coroutineExecutor.StartCoroutine(
                CreateRequestWeeklyFeaturedTrackCoroutine(_deviceManager.GetDevicePlatformName(), callback));
        }

        private IEnumerator CreateRequestWeeklyFeaturedTrackCoroutine(string deviceType,
            Action<WeeklyFeaturedTrack> callback)
        {
            var request =
                UnityWebRequest.Get(
                    "https://us-central1-djit-sas.cloudfunctions.net/weekly-featured-track-learning-guitar?platform=" +
                    deviceType);
            request.SetRequestHeader("x-env", "prod");
            request.timeout = _timeout;
            yield return request.SendWebRequest();
            if (request.isNetworkError || request.isHttpError)
            {
                _configStorage.SaveConfig(new ConfigStorageContainer());
                callback(null);
            }
            else
            {
                var text = request.downloadHandler.text;
                var weeklyFeaturedTrack = JsonUtility.FromJson<WeeklyFeaturedTrack>(text);
                _configStorage.SaveConfig(new ConfigStorageContainer(weeklyFeaturedTrack.muted,
                    weeklyFeaturedTrack.preview));
                callback(weeklyFeaturedTrack);
            }
        }
    }
}