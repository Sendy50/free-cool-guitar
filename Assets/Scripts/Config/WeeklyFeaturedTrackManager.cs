using System;

namespace Config
{
    public interface WeeklyFeaturedTrackManager
    {
        void RequestWeeklyFeaturedTrack(Action<WeeklyFeaturedTrack> callback);
    }
}