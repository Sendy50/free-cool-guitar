using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

namespace Config
{
    public class ConfigStoragePersistent : ConfigStorageContainer.ConfigStorage
    {
        private static readonly string ConfigStorageDataPath = Application.persistentDataPath + "/config-storage";

        private ConfigStorageContainer _isConfigActivated;

        public ConfigStoragePersistent()
        {
            if (!File.Exists(ConfigStorageDataPath))
            {
                _isConfigActivated = new ConfigStorageContainer();
                return;
            }

            var serializationStream = File.OpenRead(ConfigStorageDataPath);
            var binaryFormatter = new BinaryFormatter();
            _isConfigActivated = (ConfigStorageContainer) binaryFormatter.Deserialize(serializationStream);
        }

        public void SaveConfig(ConfigStorageContainer config)
        {
            _isConfigActivated = config;
            var serializationStream = File.Create(ConfigStorageDataPath);
            var binaryFormatter = new BinaryFormatter();
            binaryFormatter.Serialize(serializationStream, config);
        }

        public ConfigStorageContainer GetConfig()
        {
            return _isConfigActivated;
        }
    }
}