using UnityEngine.Serialization;

namespace Config
{
    public class WeeklyFeaturedTrack
    {
        [FormerlySerializedAs("display_name")] public string displayName;

        public int duration;
        public string id;

        public string key;

        public bool muted;

        [FormerlySerializedAs("preferred_skin")]
        public string preferredSkin;

        public bool preview;

        public WeeklyFeaturedTrack(string id, string key, string displayName, int duration, bool muted, bool preview,
            string preferredSkin)
        {
            this.id = id;
            this.key = key;
            this.displayName = displayName;
            this.duration = duration;
            this.muted = muted;
            this.preview = preview;
            this.preferredSkin = preferredSkin;
        }
    }
}