using System;
using JetBrains.Annotations;

namespace Config
{
    [Serializable]
    public class ConfigStorageContainer
    {
        private readonly string FourthSlideDisabledKey = "fourth_slide_disabled";

        public readonly bool FourthSlideDisabledValue;

        public ConfigStorageContainer(bool hasWonSubscriptionValue = false, bool fourthSlideDisabled = false)
        {
            HasWonSubscriptionValue = hasWonSubscriptionValue;
            FourthSlideDisabledValue = fourthSlideDisabled;
        }

        private string HasWonSubscriptionKey { get; } = "free_user";

        public bool HasWonSubscriptionValue { get; }

        [CanBeNull]
        public string GetConfigAsJsonString()
        {
            var obj = new string[9]
            {
                "[{\"key\":\"",
                HasWonSubscriptionKey,
                "\", \"value\":\"",
                HasWonSubscriptionValue.ToString().ToLower(),
                "\"}, {\"key\":\"",
                FourthSlideDisabledKey,
                "\", \"value\":\"",
                null,
                null
            };
            var fourthSlideDisabledValue = FourthSlideDisabledValue;
            obj[7] = fourthSlideDisabledValue.ToString().ToLower();
            obj[8] = "\"}]";
            return string.Concat(obj);
        }

        public interface ConfigStorage
        {
            void SaveConfig(ConfigStorageContainer config);

            ConfigStorageContainer GetConfig();
        }
    }
}