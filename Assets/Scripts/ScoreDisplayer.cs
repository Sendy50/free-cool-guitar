using System.Collections;
using System.Collections.Generic;
using System.Linq;
using GuitarApplication;
using UnityEngine;
using UnityEngine.UI;

public class ScoreDisplayer : MonoBehaviour
{
    public Text scoreText;

    public int incrementationByFrame = 5;

    private List<IEnumerator> _scheduledIncrementation;

    private ScoreManager _scoreManager;

    private void Awake()
    {
        _scheduledIncrementation = new List<IEnumerator>();
    }

    private void Start()
    {
        _scoreManager = ApplicationGraph.GetScoreManager();
        scoreText.text = "0";
        _scoreManager.OnScoreIncrementationDelegate += OnScoreIncrementation;
        _scoreManager.OnResteDelegate += OnScoreReste;
    }

    private void OnDestroy()
    {
        _scoreManager.OnScoreIncrementationDelegate -= OnScoreIncrementation;
        _scoreManager.OnResteDelegate -= OnScoreReste;
    }

    private void OnScoreIncrementation(ScoreManager sender, int newScore, int incrementation)
    {
        var enumerator = IncrementationDisplayRoutine(newScore - incrementation, incrementation);
        _scheduledIncrementation.Add(enumerator);
        if (_scheduledIncrementation.Count <= 1) StartCoroutine(enumerator);
    }

    private void OnScoreReste()
    {
        scoreText.text = "0";
    }

    private IEnumerator IncrementationDisplayRoutine(int startScore, int incrementation)
    {
        var add = 0;
        while (incrementation > add)
        {
            add = Mathf.Min(add + incrementationByFrame, incrementation);
            scoreText.text = (startScore + add).ToString();
            yield return null;
        }

        scoreText.text = (startScore + incrementation).ToString();
        _scheduledIncrementation.RemoveAt(0);
        if (_scheduledIncrementation.Count > 0)
        {
            var routine = _scheduledIncrementation.First();
            StartCoroutine(routine);
        }
    }
}