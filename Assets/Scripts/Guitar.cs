using System;
using System.Collections.Generic;
using AssetBundles.GuitarSkin;
using UnityEngine;
using UnityEngine.U2D;

[CreateAssetMenu]
public class Guitar : ScriptableObject, IGuitar
{
    [SerializeField] private Vector2 _arpeggiosSoundboxXOffset;

    [SerializeField] private Vector2 _chordsSoundboxXOffset;

    [SerializeField] private string filePath;

    [SerializeField] private string guitarName;

    [SerializeField] private bool isPremium;

    [SerializeField] private SkinReference skinReference;

    [SerializeField] private Sprite guitarSelectionCellImage;
    
    [SerializeField] private Sprite guitarBgImage;

    [SerializeField] private string previewName;
    
    [SerializeField] private SpriteAtlas SpriteAtlas;

    private bool _isSelected;
    
    private IGuitarSkinProxy _skinProxy;

    public string FilePath => filePath;

    public string GuitarName => guitarName;

    public bool IsPremium => isPremium;

    public SkinReference SkinReference => skinReference;

    public Sprite GuitarSelectionCellImage => guitarSelectionCellImage;
    public Sprite GuitarBgImage => guitarBgImage;

    public string PreviewName => previewName;

    public Dictionary<string, Sprite> spriteDic = new Dictionary<string, Sprite>();
    
    [HideInInspector]
    public bool isSelected
    {
        get => _isSelected;
        set
        {
            if (_isSelected != value)
            {
                _isSelected = value;
                if (OnGuitarSelected != null) OnGuitarSelected(_isSelected);
            }
        }
    }

    public event Action<bool> OnGuitarSelected;

    public void SetupSkinProxy(IGuitarSkinProxy proxy)
    {
        _skinProxy = proxy;
    }

    public IGuitarSkinProxy GetSkinProxy()
    {
        return _skinProxy;
    }

    public Vector2 GetArpeggiosSoundboxPosition()
    {
        return _arpeggiosSoundboxXOffset;
    }

    public Vector2 GetChordsSoundboxPosition()
    {
        return _chordsSoundboxXOffset;
    }
    public bool loadSprite()
    {
        Debug.LogError("LoadSprite : SpriteAtlas.spriteCount : "+SpriteAtlas.spriteCount);
        Sprite[] allSprites = new Sprite[SpriteAtlas.spriteCount];// = Resources.LoadAll<Sprite>(SpriteAtlas.name);
        SpriteAtlas.GetSprites(allSprites);
        if (allSprites == null || allSprites.Length <= 0)
        {
            Debug.LogError("The Provided Base-Atlas Sprite `" + SpriteAtlas.name + "` does not exist!");
            return false;
        }

        for (int i = 0; i < allSprites.Length; i++)
        {
            Debug.LogError("Atlas Name : " +SpriteAtlas.name+" spriteName : "+allSprites[i].name);
            spriteDic.Add(allSprites[i].name, allSprites[i]);
        }

        return true;
    }
    
    public Sprite getAtlas(string atlasName)
    {
        Sprite tempSprite;

        if (!spriteDic.TryGetValue(atlasName+"(Clone)", out tempSprite))
        {
            Debug.LogError("The Provided atlas `" + atlasName + "` does not exist!");
            return null;
        }
        else
        {
            Debug.LogError("The Provided atlas `" + atlasName + "` Loaded Success!");
        }
        return tempSprite;
    }
}