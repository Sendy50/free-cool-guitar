public interface IChordsFastAccessor
{
    event OnFastAccessorSelectChord onFastAccessorSelect;

    void updateHighlightedButton(int topDisplayChordId);
}