using Store;
using Utils;

public class GuitarCellPresenter : IGuitarCellPresenter
{
    public const string freeStatusText = "FREE";

    public const string lockStatusText = "PREMIUM";

    public const string unlockStatusText = "UNLOCKED";

    public GuitarCellPresenter(IGuitarCell cell, IGuitarSelectionHandler cellClickHandler, InAppManager inAppManager)
    {
        Precondition.CheckNotNull(cell);
        Precondition.CheckNotNull(cellClickHandler);
        // Precondition.CheckNotNull(inAppManager);
        this.cell = cell;
        this.cellClickHandler = cellClickHandler;
        // this.inAppManager = inAppManager;
        cell.onCellClick += OnCellClick;
    }

    public IGuitarCell cell { get; }

    public IGuitar guitar { get; private set; }

    public IGuitarSelectionHandler cellClickHandler { get; }

    public InAppManager inAppManager { get; private set; }

    public void SetGuitar(IGuitar guitar)
    {
        Precondition.CheckNotNull(guitar);
        this.guitar = guitar;
        UpdateUI();
    }

    public void UpdateUI()
    {
        if (guitar == null) return;
        cell.SetGuitarName(guitar.GuitarName);
        cell.SetGuitarSprite(guitar.GuitarSelectionCellImage);
        if (guitar.IsPremium)
        {
            // if (IAPManager.Instance.IsPrimeMember())
            // {
            	cell.SetStatText("UNLOCKED");
            	cell.SetVisibleIcone(GuitarCellIcon.Unlocked);
            // }
            // else
            // {
            // cell.SetStatText("PREMIUM");
            // cell.SetVisibleIcone(GuitarCellIcon.Premium);
            // }
        }
        else
        {
            cell.SetStatText("FREE");
            cell.SetVisibleIcone(GuitarCellIcon.Unlocked);
        }
    }

    private void OnCellClick()
    {
        if (guitar != null) cellClickHandler.HandleGuitarSelection(guitar);
    }
}