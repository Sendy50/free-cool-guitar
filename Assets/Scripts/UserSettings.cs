using System;
using UnityEngine;

[CreateAssetMenu]
public class UserSettings : ScriptableObject
{
    public enum GameOrientation
    {
        RightHanded,
        LeftHanded
    }

    public enum GuitarStyle
    {
        Chords,
        Arpeggios
    }

    private DataHolder _dataHolder;

    private PersistableSO _persistableSo;

    private void Awake()
    {
        if (_persistableSo == null)
        {
            _dataHolder = new DataHolder();
            _persistableSo = new PersistableSO("UserSettings_v2");
            _persistableSo.Load(_dataHolder);
        }
    }

    private void OnEnable()
    {
        if (_persistableSo == null)
        {
            _dataHolder = new DataHolder();
            _persistableSo = new PersistableSO("UserSettings_v2");
            _persistableSo.Load(_dataHolder);
        }
    }

    public event Action<GameOrientation> GameOrientationChanged;

    public event Action<GuitarStyle> GuitarStyleChanged;

    public GameOrientation GetGameOrientation()
    {
        return _dataHolder.GameOrientation;
    }

    public void SetGameOrientation(GameOrientation gameOrientation)
    {
        if (gameOrientation != _dataHolder.GameOrientation)
        {
            _dataHolder.GameOrientation = gameOrientation;
            _persistableSo.Save(_dataHolder);
            NotifyGameOrientationChanged();
        }
    }

    private void NotifyGameOrientationChanged()
    {
        if (GameOrientationChanged != null) GameOrientationChanged(_dataHolder.GameOrientation);
    }

    public GuitarStyle GetGuitarStyle()
    {
        return _dataHolder.GuitarStyle;
    }

    public void SetGuitarStyle(GuitarStyle guitarStyle)
    {
        if (guitarStyle != _dataHolder.GuitarStyle)
        {
            _dataHolder.GuitarStyle = guitarStyle;
            _persistableSo.Save(_dataHolder);
            NotifyGuitarStyleChanged();
        }
    }

    private void NotifyGuitarStyleChanged()
    {
        if (GuitarStyleChanged != null) GuitarStyleChanged(_dataHolder.GuitarStyle);
    }

    private class DataHolder
    {
        public GameOrientation GameOrientation;

        public GuitarStyle GuitarStyle;
    }
}