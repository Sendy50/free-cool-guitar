using System;
using Song;

public interface IScoreManager
{
    event Action<ISong, int> OnMaxScoreChanged;

    int GetScoreForTrack(ISong track);

    float GetNumberOfStarsForTrack(ISong track);
}