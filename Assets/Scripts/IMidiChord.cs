using System.Runtime.Serialization;

public interface IMidiChord : ISerializable
{
    int[] GetMidiNumbers();

    bool IsEqual(IMidiChord other);
}