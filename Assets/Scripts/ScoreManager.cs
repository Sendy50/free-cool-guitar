using System;
using System.Linq;
using Song;
using UnityEngine;

public class ScoreManager : IScoreManager
{
    public delegate void OnNewCombo(ScoreManager sender, Combo combo, int consecutiveCatch);

    public delegate void OnReste();

    public delegate void OnScoreIncrementation(ScoreManager sender, int newScore, int incrementation);

    private static readonly Combo[] _combos = new Combo[5]
    {
        Combo.Default(),
        new Combo
        {
            countTarget = 5,
            scoreMultiplicateur = 2f
        },
        new Combo
        {
            countTarget = 10,
            scoreMultiplicateur = 3f
        },
        new Combo
        {
            countTarget = 15,
            scoreMultiplicateur = 4f
        },
        new Combo
        {
            countTarget = 20,
            scoreMultiplicateur = 5f
        }
    };

    private readonly ScorePersistance _persistance;

    private UserActionCatcher _userActionCatcher;

    public int scoreIncrementation = 10;

    public float[] StarsLevels = new float[3]
    {
        0.3f,
        0.6f,
        0.9f
    };

    public ScoreManager()
    {
        ResetScore();
        _persistance = ScorePersistance.loadFromFile();
    }

    public int currentScore { get; private set; }

    public Combo currentCombo { get; private set; }

    public int consecutiveCatch { get; private set; }

    public event Action<ISong, int> OnMaxScoreChanged;

    public int GetScoreForTrack(ISong track)
    {
        return _persistance.maxScore.ContainsKey(track.GetId()) ? _persistance.maxScore[track.GetId()] : 0;
    }

    public float GetNumberOfStarsForTrack(ISong track)
    {
        var scoreForTrack = GetScoreForTrack(track);
        return GetNumberOfStarsForScoreOnTrack(scoreForTrack, track);
    }

    public event OnNewCombo OnNewComboDelegate;

    public event OnScoreIncrementation OnScoreIncrementationDelegate;

    public event OnReste OnResteDelegate;

    public void ResetScore()
    {
        consecutiveCatch = 0;
        currentScore = 0;
        currentCombo = _combos.First();
        if (OnResteDelegate != null) OnResteDelegate();
    }

    public void StartScoreHandle(UserActionCatcher userActionCatcher)
    {
        _userActionCatcher = userActionCatcher;
        userActionCatcher.OnActionCatchedDelegate -= OnActionCatchedDelegate;
        userActionCatcher.OnActionMissedDelegate -= OnActionMissed;
        userActionCatcher.OnTouchStringResponseDelegate -= OnTouchStringResponse;
        userActionCatcher.OnActionCatchedDelegate += OnActionCatchedDelegate;
        userActionCatcher.OnActionMissedDelegate += OnActionMissed;
        userActionCatcher.OnTouchStringResponseDelegate += OnTouchStringResponse;
    }

    public void StopScoreHandle()
    {
        if (!(_userActionCatcher == null))
        {
            _userActionCatcher.OnActionCatchedDelegate -= OnActionCatchedDelegate;
            _userActionCatcher.OnActionMissedDelegate -= OnActionMissed;
            _userActionCatcher.OnTouchStringResponseDelegate -= OnTouchStringResponse;
        }
    }

    public void SaveScoreForTrackID(ISong track)
    {
        if (!_persistance.maxScore.ContainsKey(track.GetId()) || _persistance.maxScore[track.GetId()] < currentScore)
        {
            _persistance.maxScore[track.GetId()] = currentScore;
            _persistance.saveToFile();
            if (OnMaxScoreChanged != null) OnMaxScoreChanged(track, currentScore);
        }
    }

    public float GetScoreRatio(ISong track, int score)
    {
        var maxPossibleScore = GetMaxPossibleScore(track);
        return score / maxPossibleScore;
    }

    public float GetNumberOfStarsForScoreOnTrack(int score, ISong track)
    {
        var num = 0f;
        var a = 0f;
        var scoreRatio = GetScoreRatio(track, score);
        var starsLevels = StarsLevels;
        foreach (var num2 in starsLevels)
            if (scoreRatio > num2)
            {
                num += 1f;
                a = num2;
            }
            else
            {
                num += Mathf.InverseLerp(a, num2, scoreRatio);
            }

        return num;
    }

    public float GetMaxPossibleScore(GuitarAppMidiEventParser.ParsingResult parsingResult)
    {
        return GetMaxPossibleScore(parsingResult.UserActions.Length);
    }

    public float GetMaxPossibleScore(ISong track)
    {
        Debug.LogError("GetMaxPossibleScore : track Name : " + track.GetName());
        Debug.LogError("GetMaxPossibleScore : track.GetParsedJSON() : " + track.GetParsedJson());
        return GetMaxPossibleScore(track.GetParsedJson());
    }

    public float GetMaxPossibleScore(int totalNbAction)
    {
        var num = 0f;
        var num2 = totalNbAction;
        for (var i = 0; i < _combos.Length - 1; i++)
        {
            var combo = _combos[i];
            var combo2 = _combos[i + 1];
            var num3 = combo2.countTarget - combo.countTarget;
            var num4 = num2 <= num3 ? num2 : num3;
            num2 -= num4;
            num += combo.scoreMultiplicateur * num4 * scoreIncrementation;
            if (num2 <= 0) break;
        }

        if (num2 > 0)
        {
            var num5 = num;
            var combo3 = _combos.Last();
            num = num5 + combo3.scoreMultiplicateur * num2 * scoreIncrementation;
        }

        return num;
    }

    private void OnActionCatchedDelegate(IUserAction action)
    {
        OnCatchSuccess();
    }

    private void OnActionMissed(IUserAction action)
    {
        OnCatchMiss();
    }

    private void OnTouchStringResponse(UserActionValidationTouchStringResponse response, int stringIndex)
    {
        if (response != 0) OnCatchMiss();
    }

    private void OnCatchSuccess()
    {
        consecutiveCatch++;
        float num = scoreIncrementation;
        var currentCombo = this.currentCombo;
        IncrementScore((int) (num * currentCombo.scoreMultiplicateur));
        UpdateCombo();
    }

    private void OnCatchMiss()
    {
        consecutiveCatch = 0;
        UpdateCombo();
    }

    private void UpdateCombo()
    {
        var currentCombo = this.currentCombo;
        this.currentCombo = ComboForConsecutiveCatch(consecutiveCatch);
        var scoreMultiplicateur = currentCombo.scoreMultiplicateur;
        var currentCombo2 = this.currentCombo;
        if (scoreMultiplicateur != currentCombo2.scoreMultiplicateur && OnNewComboDelegate != null)
            OnNewComboDelegate(this, this.currentCombo, consecutiveCatch);
    }

    private Combo ComboForConsecutiveCatch(int consecutiveCatch)
    {
        var result = _combos.First();
        var combos = _combos;
        for (var i = 0; i < combos.Length; i++)
        {
            var combo = combos[i];
            if (consecutiveCatch >= combo.countTarget) result = combo;
        }

        return result;
    }

    private void IncrementScore(int scoreIncrementation)
    {
        currentScore += scoreIncrementation;
        if (OnScoreIncrementationDelegate != null)
            OnScoreIncrementationDelegate(this, currentScore, scoreIncrementation);
    }

    public struct Combo
    {
        public int countTarget;

        public float scoreMultiplicateur;

        public static Combo Default()
        {
            Combo result = default;
            result.countTarget = 0;
            result.scoreMultiplicateur = 1f;
            return result;
        }
    }
}