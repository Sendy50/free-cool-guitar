public interface IUserActionValidator
{
    UserActionValidationStatus validationStatus { get; }

    GameTimeInterleave interleave { get; }

    void StartDetection();

    UserActionValidationTouchStringResponse OnStringTouched(int stringIndex);
}