using System;
using UnityEngine;

public abstract class SpawnObject : MonoBehaviour
{
    public WeakReference pool;

    public int actionIndex;

    public UserActionMover mover;

    public abstract void OnCatched();

    public abstract void OnMissed();

    public abstract void PlayTutoAnimation();

    public abstract void StopTutoAnimation();
}