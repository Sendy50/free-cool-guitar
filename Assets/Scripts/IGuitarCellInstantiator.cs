public interface IGuitarCellInstantiator
{
    IGuitarCell InstantiateCell();
}