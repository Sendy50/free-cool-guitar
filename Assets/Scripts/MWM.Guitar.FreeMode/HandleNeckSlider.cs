using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace MWM.Guitar.FreeMode
{
    public class HandleNeckSlider : MonoBehaviour, IDragHandler, IEventSystemHandler
    {
        public void OnDrag(PointerEventData eventData)
        {
            if (OnDragHandle != null) OnDragHandle(eventData);
        }

        public event Action<PointerEventData> OnDragHandle;
    }
}