using System;

namespace MWM.Guitar.FreeMode
{
    public interface ISelectCordsControl
    {
        event Action<IMidiChord> OnSelectedUITouched;

        void RefreshDetectableChord();

        void StopDetectingTouches();
    }
}