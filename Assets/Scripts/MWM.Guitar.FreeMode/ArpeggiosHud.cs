using GuitarApplication;
using MWM.Guitar.GuitarStyle;
using UnityEngine;
using UnityEngine.UI;

namespace MWM.Guitar.FreeMode
{
    public class ArpeggiosHud : MonoBehaviour
    {
        [SerializeField] private NeckSlider _neckSlider;

        [SerializeField] private ArpeggiosScreen _arpeggiosScreen;

        [SerializeField] private HorizontalLayoutGroup _bottomBarLayout;

        private UserSettings _userSettings;

        private void Awake()
        {
            _userSettings = ApplicationGraph.GetUserSettings();
            _userSettings.GameOrientationChanged += UserSettingsGameOrientationChanged;
            _neckSlider.ValueChanged += NeckSliderOnValueChanged;
            var gameOrientation = _userSettings.GetGameOrientation();
            SyncGuitarHudOrientation(gameOrientation);
        }

        private void OnDestroy()
        {
            _neckSlider.ValueChanged -= NeckSliderOnValueChanged;
            _userSettings.GameOrientationChanged -= UserSettingsGameOrientationChanged;
        }

        private void NeckSliderOnValueChanged(float newValue)
        {
            _arpeggiosScreen.SetNeckPosition(newValue);
        }

        private void UserSettingsGameOrientationChanged(UserSettings.GameOrientation orientation)
        {
            SyncGuitarHudOrientation(orientation);
        }

        private void SyncGuitarHudOrientation(UserSettings.GameOrientation orientation)
        {
            if (orientation == UserSettings.GameOrientation.LeftHanded)
                _bottomBarLayout.childAlignment = TextAnchor.MiddleLeft;
            else
                _bottomBarLayout.childAlignment = TextAnchor.MiddleRight;
        }
    }
}