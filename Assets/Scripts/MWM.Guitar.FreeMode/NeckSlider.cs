using System;
using GuitarApplication;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace MWM.Guitar.FreeMode
{
    public class NeckSlider : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IEventSystemHandler
    {
        [SerializeField] private RectTransform _internalBackgroundTransform;

        [SerializeField] private RectTransform _internalHandleTransform;

        [SerializeField] private HandleNeckSlider _internalHandleNeckSlider;

        [SerializeField] private CanvasScaler _canvasScaler;

        private bool _enabledDrag;

        private float _leftLimit;

        private bool _reversedValue;

        private float _rightLimit;

        private float _scaleMultiplier;

        private float _sliderValue;

        private UserSettings _userSettings;

        private void Awake()
        {
            _userSettings = ApplicationGraph.GetUserSettings();
            _userSettings.GameOrientationChanged += UserSettingsGameOrientationChanged;
            var rect = _internalBackgroundTransform.rect;
            var rect2 = _internalHandleTransform.rect;
            _internalHandleNeckSlider.OnDragHandle += OnDragHandle;
            _leftLimit = rect.xMin - rect2.xMin;
            _rightLimit = rect.xMax - rect2.xMax;
            _scaleMultiplier = _canvasScaler.referenceResolution.x / Screen.width;
            var gameOrientation = _userSettings.GetGameOrientation();
            SyncGuitarHudOrientation(gameOrientation);
        }

        private void OnDestroy()
        {
            _internalHandleNeckSlider.OnDragHandle -= OnDragHandle;
            _userSettings.GameOrientationChanged -= UserSettingsGameOrientationChanged;
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            _enabledDrag = true;
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            _enabledDrag = false;
        }

        public event Action<float> ValueChanged;

        private void OnDragHandle(PointerEventData eventData)
        {
            var localPosition = _internalHandleTransform.localPosition;
            var num = localPosition.x + eventData.delta.x * _scaleMultiplier;
            if (num <= _leftLimit)
            {
                localPosition.x = _leftLimit;
            }
            else if (num >= _rightLimit)
            {
                localPosition.x = _rightLimit;
            }
            else
            {
                if (!_enabledDrag) return;
                localPosition.x = num;
            }

            _internalHandleTransform.localPosition = localPosition;
            var num2 = (localPosition.x - _leftLimit) / (_rightLimit - _leftLimit);
            _sliderValue = !_reversedValue ? num2 : 1f - num2;
            if (ValueChanged != null) ValueChanged(_sliderValue);
        }

        private void SetThumbPosition(float value)
        {
            if (value < 0f || value > 1f) throw new ArgumentException("the value must be between 0 and 1");
            var x = _leftLimit + value * (_rightLimit - _leftLimit);
            var localPosition = _internalHandleTransform.localPosition;
            localPosition.x = x;
            _internalHandleTransform.localPosition = localPosition;
        }

        private void UserSettingsGameOrientationChanged(UserSettings.GameOrientation orientation)
        {
            SyncGuitarHudOrientation(orientation);
        }

        private void SyncGuitarHudOrientation(UserSettings.GameOrientation orientation)
        {
            var localScale = _internalBackgroundTransform.localScale;
            float thumbPosition;
            if (orientation == UserSettings.GameOrientation.LeftHanded)
            {
                _reversedValue = true;
                localScale.x = -1f;
                thumbPosition = 1f - _sliderValue;
            }
            else
            {
                _reversedValue = false;
                localScale.x = 1f;
                thumbPosition = _sliderValue;
            }

            SetThumbPosition(thumbPosition);
            _internalBackgroundTransform.localScale = localScale;
        }
    }
}