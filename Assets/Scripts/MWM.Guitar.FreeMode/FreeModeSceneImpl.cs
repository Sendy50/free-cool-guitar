using GuitarApplication;
using UnityEngine;

namespace MWM.Guitar.FreeMode
{
    public class FreeModeSceneImpl : MonoBehaviour
    {
        [SerializeField] private GameObject _guitarStyleChordsHud;

        [SerializeField] private GameObject _guitarStyleArpeggiosHud;

        private UserSettings _userSettings;

        private void Awake()
        {
            _userSettings = ApplicationGraph.GetUserSettings();
            _userSettings.GuitarStyleChanged += UserSettingsOnGuitarStyleChanged;
            var guitarStyle = _userSettings.GetGuitarStyle();
            SyncGuitarStyleHud(guitarStyle);
        }

        private void OnDestroy()
        {
            _userSettings.GuitarStyleChanged -= UserSettingsOnGuitarStyleChanged;
        }

        public void SetVisible(bool visible)
        {
            gameObject.SetActive(visible);
        }

        private void UserSettingsOnGuitarStyleChanged(UserSettings.GuitarStyle guitarStyle)
        {
            SyncGuitarStyleHud(guitarStyle);
        }

        private void SyncGuitarStyleHud(UserSettings.GuitarStyle guitarStyle)
        {
            if (guitarStyle == UserSettings.GuitarStyle.Chords)
            {
                _guitarStyleChordsHud.SetActive(true);
                _guitarStyleArpeggiosHud.SetActive(false);
            }
            else
            {
                _guitarStyleChordsHud.SetActive(false);
                _guitarStyleArpeggiosHud.SetActive(true);
            }
        }
    }
}