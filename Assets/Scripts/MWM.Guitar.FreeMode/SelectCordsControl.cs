using System;
using System.Linq;
using UnityEngine;

namespace MWM.Guitar.FreeMode
{
    [RequireComponent(typeof(ChordsUIDisplayer))]
    public class SelectCordsControl : MonoBehaviour, ISelectCordsControl
    {
        public bool removeButtonsEnabled;

        private ChordsUIDisplayer _chordDisplayer;

        private ChordUI[] observedChordUIs;

        public SelectedChordsList selectedChords { get; private set; }

        private void Start()
        {
            selectedChords = SelectedChordsList.DefaultList();
            _chordDisplayer = GetComponent<ChordsUIDisplayer>();
            RefreshDetectableChord();
            SetSelectedChordUI(selectedChords.GetCurrentMidiChord());
            selectedChords.OnSelectedChodChanged += OnSelectedChodChanged;
            selectedChords.OnSelectedChodsListChanged += OnSelectedChodsListChanged;
        }

        public event Action<IMidiChord> OnSelectedUITouched;

        public void RefreshDetectableChord()
        {
            _chordDisplayer.SetChordsToDisplay((from c in selectedChords.GetPlayableChords()
                where !c.IsEmptyChord()
                select c).Select(Extension.ToChord).ToArray());
            StopDetectingTouches();
            observedChordUIs = _chordDisplayer.displayedChords.Select(delegate(ChordUI c)
            {
                c.OnChordUITouchedDelegate += OnChordUITouched;
                c.OnRemoveButtonTouchedDelegate += OnRemoveButtonTouched;
                c.removeButton.gameObject.SetActive(removeButtonsEnabled);
                return c;
            }).ToArray();
        }

        public void StopDetectingTouches()
        {
            if (observedChordUIs != null)
            {
                var array = observedChordUIs;
                foreach (var chordUI in array)
                {
                    chordUI.OnChordUITouchedDelegate -= OnChordUITouched;
                    chordUI.OnRemoveButtonTouchedDelegate -= OnRemoveButtonTouched;
                }
            }
        }

        private void OnChordUITouched(ChordUI sender)
        {
            var midiChord = ChordMap.Map[sender.hash].MidiChord;
            if (midiChord.IsEqual(selectedChords.GetCurrentMidiChord()))
                selectedChords.SelectChord(MidiChord.EmptyChord());
            else
                selectedChords.SelectChord(midiChord);
            if (OnSelectedUITouched != null) OnSelectedUITouched(midiChord);
        }

        private void OnRemoveButtonTouched(ChordUI sender)
        {
            var midiChord = ChordMap.Map[sender.hash].MidiChord;
            selectedChords.RemoveChord(midiChord);
        }

        private void SetSelectedChordUI(IMidiChord midiChord)
        {
            var hashCode = midiChord.GetHashCode();
            var displayedChords = _chordDisplayer.displayedChords;
            foreach (var chordUI in displayedChords) chordUI.SetSelected(hashCode == chordUI.hash);
        }

        private void OnSelectedChodChanged(ISelectedChord sender, IMidiChord chord)
        {
            SetSelectedChordUI(chord);
        }

        private void OnSelectedChodsListChanged(IChordsList list)
        {
            RefreshDetectableChord();
            SetSelectedChordUI(selectedChords.GetCurrentMidiChord());
        }
    }
}