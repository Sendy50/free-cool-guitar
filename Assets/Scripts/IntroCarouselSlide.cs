using System;
using UnityEngine;

public class IntroCarouselSlide : MonoBehaviour
{
    [SerializeField] private IntroCarouselSlideNumber _slideNumber;

    [SerializeField] private IntroCarouselSlideMode _slideMode;

    public event Action<IntroCarouselSlide> OnSlideWillAppearEvent;

    public event Action<IntroCarouselSlide> OnSlideWillDisappearEvent;

    public IntroCarouselSlideNumber GetSlideNumber()
    {
        return _slideNumber;
    }

    public void OnSlideWillAppear()
    {
        if (OnSlideWillAppearEvent != null) OnSlideWillAppearEvent(this);
    }

    public void OnSlideWillDisappear()
    {
        if (OnSlideWillDisappearEvent != null) OnSlideWillDisappearEvent(this);
    }

    private enum IntroCarouselSlideMode
    {
        video,
        image
    }
}