public interface IGuitarCellPresenterInstantiator
{
    IGuitarCellPresenter InstantiatePresenter(IGuitar guitar);
}