using System.Collections;
using UnityEngine;

[RequireComponent(typeof(RectTransform))]
public class GuitarString : MonoBehaviour
{
    [HideInInspector] public RectTransform rectTransform;

    public float amplitude = 0.05f;

    public float durationFactor = 0.5f;

    public SpriteRenderer spriteRenderer;

    public SpriteRenderer spriteShadowRenderer;

    public GameObject muteIndicator;

    public UserSettings userSettings;

    public InterfaceIdiomEditor interfaceIdiomEditor;

    public float iphoneMuteIndicatorXOffset;

    public float ipadMuteIndicatorXOffset;

    private Coroutine _badActionCoroutine;

    private Color _colorOrigine;

    private Coroutine _oscillationCoroutine;

    private float _yOrigine;

    private void Awake()
    {
        rectTransform = GetComponent<RectTransform>();
        var position = rectTransform.position;
        _yOrigine = position.y;
        _colorOrigine = GetComponent<SpriteRenderer>().color;
        HideMuteIndicator();
    }

    private void Start()
    {
        UpdateOrientation();
        UpdateUIForInterfaceIdiom();
        userSettings.GameOrientationChanged += OnOrientationChange;
    }

    private void OnDestroy()
    {
        userSettings.GameOrientationChanged -= OnOrientationChange;
    }

    public void StartOscillation()
    {
        StopOscillation();
        if (_oscillationCoroutine == null) _oscillationCoroutine = StartCoroutine(OscillationRoutine());
    }

    public void StopOscillation()
    {
        var position = rectTransform.position;
        position.y = _yOrigine;
        rectTransform.position = position;
        if (_oscillationCoroutine != null)
        {
            StopCoroutine(_oscillationCoroutine);
            _oscillationCoroutine = null;
        }
    }

    public void ShowMuteIndicator()
    {
        muteIndicator.SetActive(true);
    }

    public void HideMuteIndicator()
    {
        muteIndicator.SetActive(false);
    }

    private void OnOrientationChange(UserSettings.GameOrientation gameOrientation)
    {
        UpdateOrientation();
    }

    private void UpdateOrientation()
    {
        var rotation = gameObject.transform.rotation;
        rotation.eulerAngles = new Vector3(0f,
            userSettings.GetGameOrientation() == UserSettings.GameOrientation.LeftHanded ? 180 : 0, 0f);
        gameObject.transform.rotation = rotation;
    }

    public void DisplayBadAction()
    {
        if (_badActionCoroutine != null)
        {
            StopCoroutine(_badActionCoroutine);
            _badActionCoroutine = null;
        }

        GetComponent<SpriteRenderer>().color = _colorOrigine;
        _badActionCoroutine = StartCoroutine(BadActionRoutine());
    }

    private IEnumerator OscillationRoutine()
    {
        var position = rectTransform.position;
        var upDown = true;
        var time = 1f;
        var a = amplitude;
        while (a > 0.001)
        {
            position.y = _yOrigine + (!upDown ? 0f - a : a);
            upDown = !upDown;
            a /= time * time;
            rectTransform.position = position;
            yield return null;
            time += Time.deltaTime * durationFactor;
        }

        position.y = _yOrigine;
        rectTransform.position = position;
        _oscillationCoroutine = null;
    }

    private IEnumerator BadActionRoutine()
    {
        var duration = 0.1f;
        var time2 = 0f;
        var badColor = Color.red;
        var spriteRenderer = GetComponent<SpriteRenderer>();
        while (time2 < duration)
        {
            time2 += Time.deltaTime;
            spriteRenderer.color = Color.Lerp(_colorOrigine, badColor, time2 / duration);
            yield return null;
        }

        time2 = 0f;
        while (time2 < duration)
        {
            time2 += Time.deltaTime;
            spriteRenderer.color = Color.Lerp(badColor, _colorOrigine, duration);
            yield return null;
        }

        spriteRenderer.color = _colorOrigine;
        _badActionCoroutine = null;
    }

    private void UpdateUIForInterfaceIdiom()
    {
        var anchoredPosition = muteIndicator.GetComponent<RectTransform>().anchoredPosition;
        anchoredPosition.x = iphoneMuteIndicatorXOffset;
        muteIndicator.GetComponent<RectTransform>().anchoredPosition = anchoredPosition;
    }

    public void ChangeStringAsset(Sprite sprite)
    {
        Debug.LogError("ChangeStringAsset : sprite == null "+(sprite == null));
        spriteRenderer.sprite = sprite;
        var size = spriteRenderer.size;
        size.x = 30f;
        size.y = (sprite.textureRect.height / sprite.pixelsPerUnit) == 0?(size.y/10):(sprite.textureRect.height / sprite.pixelsPerUnit);
        Debug.LogError("@@@@ "+transform.gameObject.name+" Size.y : "+size.y);
        spriteRenderer.size = size;
        var position = spriteRenderer.transform.position;
        position.x = 0f;
        spriteRenderer.transform.position = position;
    }

    public void ChangeStringShadowAsset(Sprite sprite)
    {
        spriteShadowRenderer.sprite = sprite;
        var size = spriteShadowRenderer.size;
        size.x = 30f;
        size.y = (sprite.textureRect.height / sprite.pixelsPerUnit) == 0?size.y:sprite.textureRect.height / sprite.pixelsPerUnit;
        spriteShadowRenderer.size = size;
        var position = spriteRenderer.transform.position;
        position.x = 0f;
        spriteRenderer.transform.position = position;
    }
}