using UnityEngine;
using UnityEngine.UI;

public class StringFingerIndicator : MonoBehaviour
{
    public RectTransform rectTransform;

    public Text numberText;

    public Image crossImage;
}