using System.Collections;
using GuitarApplication;
using JetBrains.Annotations;
using MWM.Audio;
using Rating;
using Song;
using UnityEngine;
using UnityEngine.UI;
using Utils;

public class EndGameScreen : MonoBehaviour
{
    private const float StarFillAnimationDuration = 0.3f;

    private const float StarFillAnimationPauseTime = 0.3f;

    public StartFill[] stars;

    public ParticleSystem AllStarsFilledParticles;

    public Text songName;

    public Text songArtist;

    public Text score;

    public Text maxScore;

    public Image popupBackground;

    private ISong _currentTrack;

    // public Material backgroundBlurMaterial;

    private INextButtonHandler _nextButtonHandler;

    private SamplerUnit _sampler;

    private void Awake()
    {
        _sampler = ApplicationGraph.GetMusicGamingAudioEngine().GetSamplerUnit();
    }

    private void Start()
    {
        _nextButtonHandler = new NextButtonHandler(ApplicationGraph.GetApplicationRouter(),
            ApplicationGraph.GetSongRepository().GetSongs());
    }

    private void OnEnable()
    {
        var qualityManager = ApplicationGraph.GetQualityManager();
        // if (qualityManager.GetQualityOption() != 0)
        // {
        // 	popupBackground.material = backgroundBlurMaterial;
        // 	int screentTextureId = Shader.PropertyToID("_ScreenTexture");
        // 	ApplicationRouter applicationRouter = ApplicationGraph.GetApplicationRouter();
        // 	applicationRouter.GetBlurObject().cameraBlur.onCameraBlurRenderImage += delegate(RenderTexture originalTex, RenderTexture blurTex, bool blurActive)
        // 	{
        // 		popupBackground.material.SetTexture(screentTextureId, (!blurActive) ? originalTex : blurTex);
        // 	};
        // }
        // else
        // {
        popupBackground.material = null;
        // }
    }

    [UsedImplicitly]
    public void OnNextButtonClick()
    {
        if (_currentTrack != null) _nextButtonHandler.HandleNext(_currentTrack);
    }

    [UsedImplicitly]
    public void OnRestartButtonClick()
    {
        ApplicationGraph.GetApplicationRouter().RouteToInGameFromEndScreen();
    }

    [UsedImplicitly]
    public void OnTrackListButtonClick()
    {
        ApplicationGraph.GetApplicationRouter().RouteToTrackListFromEndGame();
    }

    public void UpdateForTrack(ISong track, ScoreManager scoreManager)
    {
        var currentScore = scoreManager.currentScore;
        var scoreForTrack = scoreManager.GetScoreForTrack(track);
        _currentTrack = track;
        songName.text = track.GetName();
        songArtist.text = track.GetArtist();
        score.text = "0";
        maxScore.text = scoreForTrack.ToString();
        gameObject.SetActive(true);
        var numberOfStarsForScoreOnTrack = scoreManager.GetNumberOfStarsForScoreOnTrack(currentScore, track);
        StartCoroutine(ScoreIncrementAnimation(scoreManager.currentScore,
            ComputeScoreIncrementAnimationDuration(numberOfStarsForScoreOnTrack), currentScore >= scoreForTrack));
        StartCoroutine(StarAnimation(numberOfStarsForScoreOnTrack, 0.3f));
        _sampler.Start("1");
        // if (RatingRules.ShouldDisplayRatingRequest(Statistics.Instance, ApplicationGraph.GetConfigStorage(),
            // Device.Default(), ApplicationGraph.GetInappManager().IsSubscribe()))
            // StartCoroutine(DisplayRatingPopupWithDelay());
    }

    private IEnumerator DisplayRatingPopupWithDelay()
    {
        yield return new WaitForSeconds(1.5f);
        ApplicationGraph.GetApplicationRouter().RouteToCustomRatingPopupFromEndGame();
    }

    private IEnumerator StarAnimation(float starsValue, float fillTime)
    {
        var array = stars;
        foreach (var startFill in array) startFill.StartFillImage.fillAmount = 0f;
        for (var i = 0; i < stars.Length; i++)
        {
            if (starsValue <= 0f) break;
            var timer = 0f;
            while (timer < fillTime * Mathf.Min(1f, starsValue))
            {
                stars[i].StartFillImage.fillAmount = Mathf.Lerp(0f, 1f, timer / fillTime);
                timer += Time.deltaTime;
                yield return null;
            }

            stars[i].StartFillImage.fillAmount = Mathf.Min(1f, starsValue);
            starsValue -= 1f;
            if (stars[i].StartFillImage.fillAmount == 1f) OnStartFilledAtIndex(i);
            yield return new WaitForSeconds(0.3f);
        }
    }

    private IEnumerator ScoreIncrementAnimation(int newScore, float duration, bool isNewBestScore)
    {
        _sampler.Start("4");
        var time = 0f;
        while (time < duration)
        {
            var scoreToDisplay = Mathf.Lerp(0f, newScore, time / duration);
            time += Time.deltaTime;
            score.text = ((int) scoreToDisplay).ToString();
            yield return null;
        }

        var text = score;
        var num = newScore;
        text.text = num.ToString();
        _sampler.Stop("4");
        if (isNewBestScore) AllStarsFilledParticles.Play();
    }

    private void OnStartFilledAtIndex(int index)
    {
        stars[index].StartFilledParticles.Play();
        switch (index)
        {
            case 0:
                _sampler.Start("2");
                break;
            case 1:
                _sampler.Start("2");
                break;
            case 2:
                _sampler.Start("3");
                break;
        }
    }

    private float ComputeScoreIncrementAnimationDuration(float starsValue)
    {
        var num = 0.3f * starsValue;
        return num + 0.3f * (starsValue / 1.0001f);
    }
}