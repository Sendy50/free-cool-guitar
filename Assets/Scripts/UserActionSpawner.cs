using System;
using System.Collections;
using System.Linq;
using GuitarApplication;
using UnityEngine;

public class UserActionSpawner : MonoBehaviour
{
    public UserSettings userSettings;

    public GameplayParameters gameplayParameters;

    public StrokSpawnObject strokPrefab;

    public StrumSpawnObject strumPrefab;

    private ChordsStrings _chordsStrings;

    private ScoreManager _scoreManager;

    private Coroutine _spawningRoutine;

    public Pool<StrokSpawnObject> strokPool { get; private set; }

    public Pool<StrumSpawnObject> strumPool { get; private set; }

    private void Awake()
    {
        strokPool = new Pool<StrokSpawnObject>(30, delegate
        {
            var strokSpawnObject = Instantiate(strokPrefab, Vector3.zero, Quaternion.identity);
            strokSpawnObject.Reuse();
            return strokSpawnObject;
        });
        strokPool.All().ForEach(delegate(StrokSpawnObject c) { c.pool = new WeakReference(strokPool); });
        strumPool = new Pool<StrumSpawnObject>(20, delegate
        {
            var strumSpawnObject = Instantiate(strumPrefab, Vector3.zero, Quaternion.identity);
            strumSpawnObject.Reuse();
            return strumSpawnObject;
        });
        strumPool.All().ForEach(delegate(StrumSpawnObject c) { c.pool = new WeakReference(strumPool); });
        _scoreManager = ApplicationGraph.GetScoreManager();
    }

    public void StartSpwaning(MusicSheetReader sheetReader, ChordsStrings chordsStrings)
    {
        _chordsStrings = chordsStrings;
        if (_spawningRoutine == null) _spawningRoutine = StartCoroutine(SpawningRoutine(sheetReader));
    }

    public void StopSpwaning()
    {
        if (_spawningRoutine != null)
        {
            StopCoroutine(_spawningRoutine);
            _spawningRoutine = null;
        }
    }

    public void PrepareForNewRun()
    {
        strokPool.InUse().ToList().ForEach(delegate(StrokSpawnObject c) { c.Reuse(); });
        strumPool.InUse().ToList().ForEach(delegate(StrumSpawnObject c) { c.Reuse(); });
    }

    private IEnumerator SpawningRoutine(MusicSheetReader sheetReader)
    {
        var sheet = sheetReader.MusicSheet;
        var progression = sheetReader.GameProgression;
        var forwardTime = gameplayParameters.actionSpawningTimeHorizon;
        var actions = sheet.UserActions;
        for (var i = 0; i < actions.Length; i++)
        {
            var nextAction = actions[i];
            yield return new WaitUntil(() => nextAction.time < progression.CurrentTime + forwardTime);
            SpawnAction(nextAction, progression, i);
        }

        _spawningRoutine = null;
    }

    private void SpawnAction(IUserAction action, GameProgression progression, int actionIndex)
    {
        var xCoodinate = progression.TimeToForward(action.time);
        SpawnObject spawnObject = null;
        if (action is StrockUserAction) spawnObject = SpawnStrok(action as StrockUserAction, progression, xCoodinate);
        if (action is StrumUserAction) spawnObject = SpawnStrum(action as StrumUserAction, progression, xCoodinate);
        spawnObject.actionIndex = actionIndex;
        spawnObject.mover.Setup(action.time, progression);
        spawnObject.mover.StartMovement();
    }

    private SpawnObject SpawnStrok(StrockUserAction action, GameProgression progression, float xCoodinate)
    {
        var num = action.SpawningStringIndex();
        var @object = strokPool.GetObject();
        @object.gameObject.SetActive(true);
        var position = @object.transform.position;
        position.x = xCoodinate;
        var position2 = _chordsStrings.guitarStrings[num].gameObject.transform.position;
        position.y = position2.y;
        @object.transform.position = position;
        @object.OnSpawned(_scoreManager);
        return @object;
    }

    private SpawnObject SpawnStrum(StrumUserAction action, GameProgression progression, float xCoodinate)
    {
        var @object = strumPool.GetObject();
        var num = action.SpawningStringIndex();
        @object.gameObject.SetActive(true);
        var position = @object.transform.position;
        position.x = xCoodinate;
        var position2 = _chordsStrings.guitarStrings[num].gameObject.transform.position;
        position.y = position2.y;
        @object.transform.position = position;
        @object.SetNbStrings((from c in action.notes
            where c.MidiNote.midi != 0
            select c).Count());
        @object.SetOrientation(action.orientation);
        @object.OnSpawned(_scoreManager);
        return @object;
    }
}