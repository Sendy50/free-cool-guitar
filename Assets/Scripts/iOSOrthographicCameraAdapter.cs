using UnityEngine;

[RequireComponent(typeof(Camera))]
public class iOSOrthographicCameraAdapter : MonoBehaviour
{
    public InterfaceIdiomEditor interfaceIdiomEditor;

    public float iphoneCameraSize;

    public float ipadCameraSize;

    private void Awake()
    {
        UpdateUI();
    }

    private void UpdateUI()
    {
        var camera = gameObject.GetComponent<Camera>();
        if (camera == null) camera = gameObject.AddComponent<Camera>();
        camera.orthographic = true;
        camera.orthographicSize = iphoneCameraSize;
    }
}