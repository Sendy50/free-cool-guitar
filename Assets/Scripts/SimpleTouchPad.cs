using UnityEngine;
using UnityEngine.EventSystems;

public class SimpleTouchPad : MonoBehaviour, IPointerDownHandler, IDragHandler, IPointerUpHandler, IEventSystemHandler
{
    public float smoothing;

    private Vector2 direction;

    private Vector2 origin;

    private int pointerId;

    private Vector2 smoothDirection;

    private bool touched;

    private void Awake()
    {
        direction = Vector2.zero;
        touched = false;
    }

    public void OnDrag(PointerEventData data)
    {
        if (pointerId == data.pointerId)
        {
            var position = data.position;
            direction = (position - origin).normalized;
            Debug.Log(direction);
        }
    }

    public void OnPointerDown(PointerEventData data)
    {
        if (!touched)
        {
            touched = true;
            pointerId = data.pointerId;
            origin = data.position;
        }
    }

    public void OnPointerUp(PointerEventData data)
    {
        if (pointerId == data.pointerId)
        {
            direction = Vector2.zero;
            touched = false;
        }
    }

    public Vector2 GetDirection()
    {
        smoothDirection = Vector2.MoveTowards(smoothDirection, direction, smoothing);
        return smoothDirection;
    }
}