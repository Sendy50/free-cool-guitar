public interface IHorizontalScrollSnapDecorator
{
    int GetCurrentPage();

    void GoToScreen(int screenIndex);

    void UpdateLayout();
}