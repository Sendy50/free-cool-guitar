using JetBrains.Annotations;
using MWM.Core.Demo;
using UnityEngine;

namespace MWM.GameEvent.Demo
{
    public class GameEventDemoScreen : MonoBehaviour
    {
        private GameReportBuilderDemo _gameReportBuilder;

        private void Start()
        {
            _gameReportBuilder = new GameReportBuilderDemo(GameEventSender.Instance());
        }

        [UsedImplicitly]
        public void OnBackButtonClicked()
        {
            Router.GoToMainDemoScreen();
        }

        [UsedImplicitly]
        public void OnSendInAppPurchasedEventButtonClicked()
        {
            GameEventSender.Instance().SendEvent(EventType.InappPurchased, "test_in_app_purchased");
        }

        [UsedImplicitly]
        public void OnSendGameReportEventButtonClicked()
        {
            _gameReportBuilder.Reset();
            _gameReportBuilder.SetIsTutorial(false);
            _gameReportBuilder.IncrementDuration(5f);
            _gameReportBuilder.IncrementDuration(10f);
            _gameReportBuilder.SetDemoValue("demo value");
            _gameReportBuilder.SendReport();
        }
    }
}