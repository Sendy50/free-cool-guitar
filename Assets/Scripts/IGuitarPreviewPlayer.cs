public interface IGuitarPreviewPlayer
{
    void PlayPreviewForGuitar(IGuitar guitar);

    void StopPreview();
}