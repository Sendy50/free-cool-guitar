using UnityEngine;

public class TableViewCell : MonoBehaviour, ITableViewCell
{
    [HideInInspector] public int cellIndex;

    public int GetCellIndex()
    {
        return cellIndex;
    }

    public void prepareForReuse()
    {
    }
}