using System.Collections;
using UnityEngine;

public class StrokSpawnObject : SpawnObject
{
    public SpriteRenderer sprite;

    public SpriteRenderer goodSprite;

    public SpriteRenderer missSprite;

    public ParticleSystem validateParticle;

    // public Animator tutoAnimator;

    public float goodFadOutDuration = 0.5f;

    public ParticleSystem[] comboParticles;

    [HideInInspector] public ScoreManager scoreManager;

    public void Reuse()
    {
        gameObject.SetActive(false);
        actionIndex = -1;
        goodSprite.color = Color.white;
        goodSprite.gameObject.SetActive(false);
        missSprite.gameObject.SetActive(false);
        sprite.gameObject.SetActive(false);
        if (scoreManager != null)
        {
            scoreManager.OnNewComboDelegate -= OnNewCombo;
            scoreManager.OnResteDelegate -= OnScoreReste;
        }

        UpdateComboParticle(ScoreManager.Combo.Default());
        StopTutoAnimation();
        if (this.pool != null)
        {
            var pool = this.pool.Target as Pool<StrokSpawnObject>;
            if (pool != null)
            {
                pool.ReleaseObject(this);
                sprite.gameObject.SetActive(true);
            }
        }
    }

    public void OnSpawned(ScoreManager scoreManager)
    {
        this.scoreManager = scoreManager;
        sprite.gameObject.SetActive(true);
        UpdateComboParticle(scoreManager.currentCombo);
        scoreManager.OnNewComboDelegate += OnNewCombo;
        scoreManager.OnResteDelegate += OnScoreReste;
    }

    public override void OnCatched()
    {
        StartCoroutine(OnCatchedRoutine());
    }

    public override void OnMissed()
    {
        StartCoroutine(OnMissRoutine());
    }

    public override void PlayTutoAnimation()
    {
        // if (tutoAnimator.gameObject.activeSelf)
        // {
        // 	tutoAnimator.SetBool("StrokTutoAnim", value: true);
        // }
    }

    public override void StopTutoAnimation()
    {
        // if (tutoAnimator.gameObject.activeSelf)
        // {
        // 	tutoAnimator.SetBool("StrokTutoAnim", value: false);
        // }
    }

    private IEnumerator OnMissRoutine()
    {
        sprite.gameObject.SetActive(false);
        missSprite.gameObject.SetActive(true);
        yield return new WaitForSeconds(0.5f);
        Reuse();
    }

    private IEnumerator OnCatchedRoutine()
    {
        sprite.gameObject.SetActive(false);
        goodSprite.gameObject.SetActive(true);
        mover.StopMovement();
        validateParticle.Play();
        var fadoutCoroutine = StartCoroutine(FadeOut(goodSprite, goodFadOutDuration));
        yield return new WaitUntil(() => !validateParticle.isPlaying);
        yield return fadoutCoroutine;
        Reuse();
    }

    private IEnumerator FadeOut(SpriteRenderer sprite, float duration)
    {
        var time = 0f;
        var startColor = sprite.color;
        var endColor = new Color(startColor.r, startColor.g, startColor.b, 0f);
        while (time < duration)
        {
            yield return null;
            time += Time.deltaTime;
            sprite.color = Color.Lerp(startColor, endColor, time / duration);
        }

        sprite.color = endColor;
    }

    private void OnNewCombo(ScoreManager sender, ScoreManager.Combo combo, int consecutiveCatch)
    {
        UpdateComboParticle(combo);
    }

    private void OnScoreReste()
    {
        UpdateComboParticle(scoreManager.currentCombo);
    }

    private void UpdateComboParticle(ScoreManager.Combo combo)
    {
        var num = (int) combo.scoreMultiplicateur - 2;
        if (num < 0)
        {
            var array = comboParticles;
            foreach (var particleSystem in array)
            {
                particleSystem.Stop();
                particleSystem.gameObject.SetActive(false);
            }

            return;
        }

        for (var j = 0; j < comboParticles.Length; j++)
        {
            var particleSystem2 = comboParticles[j];
            if (j == num)
            {
                particleSystem2.gameObject.SetActive(true);
                particleSystem2.Play();
            }
            else
            {
                particleSystem2.Stop();
                particleSystem2.gameObject.SetActive(false);
            }
        }
    }
}