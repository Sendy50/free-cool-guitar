using GuitarApplication;
using UnityEngine;

public class ComboDisplayer : MonoBehaviour
{
    public ComboUI[] comboUIs;

    public UserSettings userSettings;

    public float leftHandedXPosition;

    private ScoreManager.Combo _lastCombo;

    private ScoreManager _scoreManager;

    private void Start()
    {
        _scoreManager = ApplicationGraph.GetScoreManager();
        _scoreManager.OnNewComboDelegate += OnNewCombo;
        _scoreManager.OnResteDelegate += OnScoreReste;
        _lastCombo = _scoreManager.currentCombo;
        UpdateOrientation();
        userSettings.GameOrientationChanged += OnOrientationChange;
    }

    private void OnDestroy()
    {
        _scoreManager.OnNewComboDelegate -= OnNewCombo;
        _scoreManager.OnResteDelegate -= OnScoreReste;
        userSettings.GameOrientationChanged -= OnOrientationChange;
    }

    private void OnOrientationChange(UserSettings.GameOrientation gameOrientation)
    {
        UpdateOrientation();
    }

    private void UpdateOrientation()
    {
        var position = gameObject.transform.position;
        position.x = userSettings.GetGameOrientation() != UserSettings.GameOrientation.LeftHanded
            ? 0f - leftHandedXPosition
            : leftHandedXPosition;
        gameObject.transform.position = position;
    }

    private void OnNewCombo(ScoreManager sender, ScoreManager.Combo combo, int consecutiveCatch)
    {
        if (combo.scoreMultiplicateur > _lastCombo.scoreMultiplicateur)
        {
            var num = (int) combo.scoreMultiplicateur - 2;
            if (num < 0 || num >= comboUIs.Length) return;
            comboUIs[num].Play();
        }

        _lastCombo = combo;
    }

    private void OnScoreReste()
    {
        _lastCombo = _scoreManager.currentCombo;
    }
}