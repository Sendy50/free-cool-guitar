using UnityEngine.Events;

namespace UnityEngine.UI.Extensions.Tweens
{
    public struct FloatTween : ITweenValue
    {
        public class FloatTweenCallback : UnityEvent<float>
        {
        }

        public class FloatFinishCallback : UnityEvent
        {
        }

        private FloatTweenCallback m_Target;

        private FloatFinishCallback m_Finish;

        public float startFloat { get; set; }

        public float targetFloat { get; set; }

        public float duration { get; set; }

        public bool ignoreTimeScale { get; set; }

        public void TweenValue(float floatPercentage)
        {
            if (ValidTarget()) m_Target.Invoke(Mathf.Lerp(startFloat, targetFloat, floatPercentage));
        }

        public void AddOnChangedCallback(UnityAction<float> callback)
        {
            if (m_Target == null) m_Target = new FloatTweenCallback();
            m_Target.AddListener(callback);
        }

        public void AddOnFinishCallback(UnityAction callback)
        {
            if (m_Finish == null) m_Finish = new FloatFinishCallback();
            m_Finish.AddListener(callback);
        }

        public bool GetIgnoreTimescale()
        {
            return ignoreTimeScale;
        }

        public float GetDuration()
        {
            return duration;
        }

        public bool ValidTarget()
        {
            return m_Target != null;
        }

        public void Finished()
        {
            if (m_Finish != null) m_Finish.Invoke();
        }
    }
}