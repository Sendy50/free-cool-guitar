using UnityEngine.EventSystems;

namespace UnityEngine.UI
{
    [RequireComponent(typeof(InputField))]
    [AddComponentMenu("UI/Extensions/Return Key Trigger")]
    public class ReturnKeyTriggersButton : MonoBehaviour, ISubmitHandler, IEventSystemHandler
    {
        public Button button;

        public float highlightDuration = 0.2f;
        private EventSystem _system;

        private readonly bool highlight = true;

        private void Start()
        {
            _system = EventSystem.current;
        }

        public void OnSubmit(BaseEventData eventData)
        {
            if (highlight) button.OnPointerEnter(new PointerEventData(_system));
            button.OnPointerClick(new PointerEventData(_system));
            if (highlight) Invoke("RemoveHighlight", highlightDuration);
        }

        private void RemoveHighlight()
        {
            button.OnPointerExit(new PointerEventData(_system));
        }
    }
}