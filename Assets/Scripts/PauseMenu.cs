using Song;

public interface PauseMenu
{
    void SetVisible(bool visible);

    void DisplayWithSong(ISong currentSong);
}