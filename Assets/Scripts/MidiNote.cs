using System;

[Serializable]
public class MidiNote
{
    public int midi;

    public float time;

    public float velocity;

    public float duration;
}