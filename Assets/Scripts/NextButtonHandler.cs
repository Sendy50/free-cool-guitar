using System.Collections.Generic;
using GuitarApplication;
using Song;
using UnityEngine;
using Utils;

public class NextButtonHandler : INextButtonHandler
{
    private readonly List<ISong> _allSongs;
    private readonly ApplicationRouter _router;

    // private ExternalAdsWrapper _externalAdsWrapper;

    // private InAppManager _inappManager;

    public NextButtonHandler(ApplicationRouter router, List<ISong> allSongs)
    {
        Precondition.CheckNotNull(router);
        Precondition.CheckNotNull(allSongs);
        // Precondition.CheckNotNull(externalAdsWrapper);
        // Precondition.CheckNotNull(inappManager);
        _router = router;
        _allSongs = allSongs;
        // _externalAdsWrapper = externalAdsWrapper;
        // _inappManager = inappManager;
    }

    public void HandleNext(ISong currentSong)
    {
        var num = _allSongs.FindIndex(c => c.GetId() == currentSong.GetId());
        var num2 = num + 1;
        if (num2 >= _allSongs.Count) num2 = 0;
        var song = _allSongs[num2];
        // if (_inappManager.IsSubscribe())
        // if (IAPManager.Instance.IsPrimeMember())
        // {
            _router.RouteToInGameFromEndScreen(song);
            return;
        // }

        switch (song.GetSongType())
        {
            case SongType.Vip:
                HandleNext(song);
                break;
            case SongType.Unlockable:
                _router.RouteToInGameFromEndScreen(song);
                break;
            case SongType.Free:
                _router.RouteToInGameFromEndScreen(song);
                break;
        }
    }

    private void displayRewardedAdsAndUnlockTrack(ISong track)
    {
        _router.ShowLoader();
        // _externalAdsWrapper.LoadContent(PlacementType.RewardVideo, "RewardedVideoPlacement", delegate(PlacementLoadingState loadingState)
        // {
        // switch (PlacementLoadingState.Loaded)
        // {
        // case PlacementLoadingState.Loading:
        // 	break;
        // case PlacementLoadingState.NotLoaded:
        // case PlacementLoadingState.Failure:
        // case PlacementLoadingState.Expired:
        // 	_router.HideLoader();
        // 	// ShowLoadingRewardError();
        // 	break;
        // case PlacementLoadingState.Loaded:
        // _externalAdsWrapper.DisplayContent(PlacementType.RewardVideo, "RewardedVideoPlacement", delegate(PlacementShowHideResult result)
        // {
        // 	if (result == PlacementShowHideResult.ShowHideFailed)
        // 	{
        // 		_router.HideLoader();
        // 		ShowLoadingRewardError();
        // 	}
        // }, delegate
        // {
        //ShowLoadingRewardError();
        _router.HideLoader();
        _router.RouteToInGameFromEndScreen(track);
        // });
        // 	break;
        // default:
        // 	throw new ArgumentOutOfRangeException("loadingState", PlacementLoadingState.Loaded, null);
        // }
        // });
    }

    private void ShowLoadingRewardError()
    {
        var mNPopup = new MNPopup("Error", "An error occurred");
        mNPopup.AddAction("Ok", delegate { });
        mNPopup.AddDismissListener(delegate { Debug.Log("dismiss listener"); });
        mNPopup.Show();
    }
}