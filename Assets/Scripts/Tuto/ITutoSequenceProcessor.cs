using System.Collections;

namespace Tuto
{
    public interface ITutoSequenceProcessor
    {
        IEnumerator Run();

        void Dispose();
    }
}