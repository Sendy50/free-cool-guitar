using Utils;

namespace Tuto
{
    public class TutoSequence : ITutoSequence
    {
        private int currentIndex;
        private readonly ITutoStep[] steps;

        public TutoSequence(ITutoStep[] steps)
        {
            Precondition.CheckNotNull(steps);
            this.steps = steps;
            currentIndex = 0;
        }

        public ITutoStep GetCurrentStep()
        {
            return steps[currentIndex];
        }

        public bool MoveNext()
        {
            if (currentIndex + 1 >= steps.Length) return false;
            currentIndex++;
            return true;
        }

        public void Reset()
        {
            currentIndex = 0;
            var array = steps;
            foreach (var tutoStep in array) tutoStep.Cancel();
        }
    }
}