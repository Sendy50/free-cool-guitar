using System.Collections;
using UnityEngine;
using Utils;

namespace Tuto
{
    public class TutoSequenceProcessor : ITutoSequenceProcessor
    {
        public readonly ITutoSequence sequence;

        public TutoSequenceProcessor(ITutoSequence sequence)
        {
            Precondition.CheckNotNull(sequence);
            this.sequence = sequence;
        }

        public IEnumerator Run()
        {
            if (sequence.GetCurrentStep() == null) yield break;
            do
            {
                var currentTutoStep = sequence.GetCurrentStep();
                currentTutoStep.StartStep();
                yield return new WaitUntil(() => currentTutoStep.GetIsStepValidated());
                currentTutoStep.Finish();
            } while (sequence.MoveNext());

            sequence.Reset();
        }

        public void Dispose()
        {
            sequence.Reset();
        }

        ~TutoSequenceProcessor()
        {
            Dispose();
        }
    }
}