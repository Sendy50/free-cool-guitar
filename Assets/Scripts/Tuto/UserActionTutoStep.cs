using System.Collections;
using MWM.Audio;
using UnityEngine;
using Utils;

namespace Tuto
{
    public class UserActionTutoStep : ITutoStep
    {
        private readonly ChordsStrings _chordsStrings;
        private IUserAction action;

        private readonly int actionIndex;

        private readonly IUserActionValidator actionValidator;

        private GameplayParameters gameplayParameters;

        private readonly GameProgression gameProgression;

        private bool hasStopped;

        private readonly MonoBehaviour monoBehaviour;

        private MusicSheetReader musicSheetReader;

        private readonly Player player;

        private readonly UserActionSpawner spawner;

        private StoppableCoroutine tutoRoutine;

        public UserActionTutoStep(IUserAction action, int actionIndex, MonoBehaviour monoBehaviour,
            GameplayParameters gameplayParameters, MusicSheetReader musicSheetReader, GameProgression gameProgression,
            ChordsStrings chordsStrings, Player player, UserActionSpawner spawner)
        {
            Precondition.CheckNotNull(action);
            Precondition.CheckNotNull(monoBehaviour);
            Precondition.CheckNotNull(gameplayParameters);
            Precondition.CheckNotNull(musicSheetReader);
            Precondition.CheckNotNull(gameProgression);
            Precondition.CheckNotNull(chordsStrings);
            Precondition.CheckNotNull(player);
            Precondition.CheckNotNull(spawner);
            this.action = action;
            this.actionIndex = actionIndex;
            this.monoBehaviour = monoBehaviour;
            this.gameplayParameters = gameplayParameters;
            this.musicSheetReader = musicSheetReader;
            this.gameProgression = gameProgression;
            _chordsStrings = chordsStrings;
            this.player = player;
            this.spawner = spawner;
            actionValidator = action.Validator(monoBehaviour, gameplayParameters, musicSheetReader);
            hasStopped = false;
        }

        public void StartStep()
        {
            hasStopped = false;
            tutoRoutine.StartRoutine(monoBehaviour, RunStep());
            _chordsStrings.OnStringTouchedDelegate -= OnStringTouched;
            _chordsStrings.OnStringTouchedDelegate += OnStringTouched;
        }

        public void Finish()
        {
            _chordsStrings.OnStringTouchedDelegate -= OnStringTouched;
            tutoRoutine.StopRoutine(monoBehaviour);
            if (hasStopped && !gameProgression.GetIsRunning()) gameProgression.Resume();
            if (hasStopped && !player.IsPlaying()) player.Play();
            hasStopped = false;
        }

        public void Cancel()
        {
            _chordsStrings.OnStringTouchedDelegate -= OnStringTouched;
            tutoRoutine.StopRoutine(monoBehaviour);
        }

        public bool GetIsStepValidated()
        {
            return actionValidator.validationStatus == UserActionValidationStatus.validated;
        }

        private IEnumerator RunStep()
        {
            var interleave = actionValidator.interleave;
            actionValidator.StartDetection();
            yield return new WaitUntil(() => interleave.NormalizeDistanceToMiddle() > 0f);
            gameProgression.StopProgression();
            player.Pause();
            hasStopped = true;
            var spawnObject = UserActionCatcher.FindSpawnObjectForAction(actionValidator, actionIndex, spawner);
            spawnObject.PlayTutoAnimation();
            yield return new WaitUntil(() => GetIsStepValidated());
            spawnObject.StopTutoAnimation();
        }

        private void OnStringTouched(GuitarString sender, int stringIndex)
        {
            actionValidator.OnStringTouched(stringIndex);
        }
    }
}