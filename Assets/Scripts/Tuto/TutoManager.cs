using MWM.Audio;
using Song;
using UnityEngine;
using Utils;

namespace Tuto
{
    public class TutoManager : ITutoManager
    {
        private readonly ChordsStrings _chordsStrings;

        private readonly GameplayParameters _gameplayParameters;
        private readonly MonoBehaviour _monoBehaviour;

        private readonly Player _player;

        private readonly UserActionSpawner _spawner;

        private readonly ITutoStorage _storage;

        private StoppableCoroutine _tutoCoroutine;

        private TutoSequenceProcessor _tutoProcessor;

        public TutoManager(MonoBehaviour monoBehaviour, GameplayParameters gameplayParameters,
            ChordsStrings chordsStrings, Player player, ITutoStorage storage, UserActionSpawner spawner)
        {
            Precondition.CheckNotNull(monoBehaviour);
            Precondition.CheckNotNull(gameplayParameters);
            Precondition.CheckNotNull(chordsStrings);
            Precondition.CheckNotNull(player);
            Precondition.CheckNotNull(storage);
            Precondition.CheckNotNull(spawner);
            _monoBehaviour = monoBehaviour;
            _gameplayParameters = gameplayParameters;
            _chordsStrings = chordsStrings;
            _player = player;
            _storage = storage;
            _spawner = spawner;
        }

        public bool IsTutoRequiredForTrack(ISong track)
        {
            if (!track.GetIsTutorialTrack()) return false;
            return !_storage.GetIsTutoCompletedForTrackId(track.GetId());
        }

        public bool GetIsRunning()
        {
            return _tutoCoroutine.GetIsRunning();
        }

        public void CancelTuto()
        {
            _tutoCoroutine.StopRoutine(_monoBehaviour);
            if (_tutoProcessor != null) _tutoProcessor.Dispose();
        }

        public void HandleEndOfTrack(ISong track)
        {
            _storage.SetTutoCompletedForTrackId(track.GetId());
            _tutoCoroutine.StopRoutine(_monoBehaviour);
            if (_tutoProcessor != null) _tutoProcessor.Dispose();
        }

        public void StartTuto(MusicSheetReader musicSheetReader)
        {
            var musicSheet = musicSheetReader.MusicSheet;
            var num = 0;
            var tutoStep = BuildSetpForAction(musicSheet.UserActions[num], num, musicSheetReader);
            num = 1;
            var tutoStep2 = BuildSetpForAction(musicSheet.UserActions[num], num, musicSheetReader);
            num = 2;
            var tutoStep3 = BuildSetpForAction(musicSheet.UserActions[num], num, musicSheetReader);
            num = 3;
            var tutoStep4 = BuildSetpForAction(musicSheet.UserActions[num], num, musicSheetReader);
            var steps = new ITutoStep[4]
            {
                tutoStep,
                tutoStep2,
                tutoStep3,
                tutoStep4
            };
            var sequence = new TutoSequence(steps);
            _tutoProcessor = new TutoSequenceProcessor(sequence);
            _tutoCoroutine.StartRoutine(_monoBehaviour, _tutoProcessor.Run());
        }

        public ITutoStep BuildSetpForAction(IUserAction action, int actionIndex, MusicSheetReader musicSheetReader)
        {
            return new UserActionTutoStep(action, actionIndex, _monoBehaviour, _gameplayParameters, musicSheetReader,
                musicSheetReader.GameProgression, _chordsStrings, _player, _spawner);
        }
    }
}