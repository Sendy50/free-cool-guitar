namespace Tuto
{
    public interface ITutoSequence
    {
        ITutoStep GetCurrentStep();

        bool MoveNext();

        void Reset();
    }
}