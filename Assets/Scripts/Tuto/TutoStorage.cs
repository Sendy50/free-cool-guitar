using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

namespace Tuto
{
    public class TutoStorage : ITutoStorage
    {
        private static readonly string TutoDataPath = Application.persistentDataPath + "/tutorial_datas";


        private readonly Dictionary<string, bool> _data;

        public TutoStorage()
        {
            _data = loadFromFile();
            saveToFile(_data);
        }

        public void SetTutoCompletedForTrackId(string trackId)
        {
            _data[trackId] = true;
            saveToFile(_data);
        }

        public bool GetIsTutoCompletedForTrackId(string trackId)
        {
            if (!_data.ContainsKey(trackId)) return false;
            return _data[trackId];
        }

        private static Dictionary<string, bool> loadFromFile()
        {
            Debug.LogError("TutoDataPath : " + TutoDataPath);
            if (File.Exists(TutoDataPath))
            {
                var binaryFormatter = new BinaryFormatter();
                var serializationStream = File.Open(TutoDataPath, FileMode.Open);
                Dictionary<string, bool> temp;
                if (serializationStream.Length > 0)
                    temp = (Dictionary<string, bool>) binaryFormatter.Deserialize(serializationStream);
                else
                    temp = new Dictionary<string, bool>();
                serializationStream.Close();
                return temp;
            }

            return new Dictionary<string, bool>();
        }

        private static void saveToFile(Dictionary<string, bool> data)
        {
            var binaryFormatter = new BinaryFormatter();
            var serializationStream = File.Create(TutoDataPath);
            binaryFormatter.Serialize(serializationStream, data);
            serializationStream.Close();
        }
    }
}