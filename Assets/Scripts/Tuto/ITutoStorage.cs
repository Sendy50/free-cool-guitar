namespace Tuto
{
    public interface ITutoStorage
    {
        void SetTutoCompletedForTrackId(string trackId);

        bool GetIsTutoCompletedForTrackId(string trackId);
    }
}