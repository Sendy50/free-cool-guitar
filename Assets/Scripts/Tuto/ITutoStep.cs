namespace Tuto
{
    public interface ITutoStep
    {
        void StartStep();

        void Finish();

        void Cancel();

        bool GetIsStepValidated();
    }
}