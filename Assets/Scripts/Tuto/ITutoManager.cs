using Song;

namespace Tuto
{
    public interface ITutoManager
    {
        bool IsTutoRequiredForTrack(ISong track);

        bool GetIsRunning();

        void CancelTuto();

        void HandleEndOfTrack(ISong track);

        void StartTuto(MusicSheetReader musicSheetReader);
    }
}