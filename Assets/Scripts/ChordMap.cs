using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ChordMap
{
	private static Dictionary<int, ChordContainer> _internalMap = null;

	public static readonly int[] wrongOldEmpltyChordMidiNumbers = new int[6]
	{
		40,
		45,
		50,
		55,
		59,
		67
	};

	public static readonly int[] emptyChordMidiNumbers = new int[6]
	{
		40,
		45,
		50,
		55,
		59,
		64
	};

	public static readonly int emptyChordMidiNumbersHash = GenerateMidiNumbersArrayHash(emptyChordMidiNumbers);

	public static ChordNameCorrespondance[] NameMap = new ChordNameCorrespondance[2139]
	{
		new ChordNameCorrespondance
		{
			chordId = 0,
			MidiNumbers = new int[6]
			{
				0,
				48,
				52,
				55,
				60,
				64
			},
			key = "C",
			suffix = string.Empty,
			Fingers = new int[6]
			{
				0,
				3,
				2,
				0,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1,
			MidiNumbers = new int[6]
			{
				0,
				48,
				55,
				60,
				64,
				67
			},
			key = "C",
			suffix = string.Empty,
			Fingers = new int[6]
			{
				0,
				1,
				3,
				3,
				3,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 2,
			MidiNumbers = new int[6]
			{
				48,
				55,
				60,
				64,
				67,
				72
			},
			key = "C",
			suffix = string.Empty,
			Fingers = new int[6]
			{
				1,
				3,
				4,
				2,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 3,
			MidiNumbers = new int[6]
			{
				48,
				52,
				55,
				60,
				64,
				72
			},
			key = "C",
			suffix = string.Empty,
			Fingers = new int[6]
			{
				3,
				2,
				1,
				1,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 4,
			MidiNumbers = new int[6]
			{
				0,
				0,
				60,
				67,
				72,
				76
			},
			key = "C",
			suffix = string.Empty,
			Fingers = new int[6]
			{
				0,
				0,
				1,
				2,
				4,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 5,
			MidiNumbers = new int[6]
			{
				0,
				48,
				55,
				60,
				63,
				67
			},
			key = "C",
			suffix = "m",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				4,
				2,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 6,
			MidiNumbers = new int[6]
			{
				48,
				55,
				60,
				63,
				67,
				72
			},
			key = "C",
			suffix = "m",
			Fingers = new int[6]
			{
				1,
				3,
				4,
				1,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 7,
			MidiNumbers = new int[6]
			{
				0,
				0,
				60,
				63,
				67,
				72
			},
			key = "C",
			suffix = "m",
			Fingers = new int[6]
			{
				0,
				0,
				3,
				1,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 8,
			MidiNumbers = new int[6]
			{
				0,
				0,
				60,
				67,
				72,
				75
			},
			key = "C",
			suffix = "m",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				4,
				2
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 9,
			MidiNumbers = new int[6]
			{
				0,
				48,
				52,
				58,
				60,
				64
			},
			key = "C",
			suffix = "7",
			Fingers = new int[6]
			{
				0,
				3,
				2,
				4,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 10,
			MidiNumbers = new int[6]
			{
				0,
				48,
				55,
				58,
				64,
				67
			},
			key = "C",
			suffix = "7",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				1,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 11,
			MidiNumbers = new int[6]
			{
				48,
				55,
				58,
				64,
				67,
				72
			},
			key = "C",
			suffix = "7",
			Fingers = new int[6]
			{
				1,
				3,
				1,
				2,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 12,
			MidiNumbers = new int[6]
			{
				0,
				0,
				60,
				67,
				70,
				76
			},
			key = "C",
			suffix = "7",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				2,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 13,
			MidiNumbers = new int[6]
			{
				0,
				48,
				52,
				58,
				60,
				0
			},
			key = "C",
			suffix = "7",
			Fingers = new int[6]
			{
				0,
				3,
				2,
				4,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 14,
			MidiNumbers = new int[6]
			{
				48,
				0,
				58,
				64,
				67,
				0
			},
			key = "C",
			suffix = "7",
			Fingers = new int[6]
			{
				1,
				0,
				2,
				4,
				3,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 15,
			MidiNumbers = new int[6]
			{
				0,
				48,
				50,
				55,
				60,
				67
			},
			key = "C",
			suffix = "sus2",
			Fingers = new int[6]
			{
				0,
				3,
				0,
				0,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 16,
			MidiNumbers = new int[6]
			{
				0,
				48,
				55,
				60,
				62,
				67
			},
			key = "C",
			suffix = "sus2",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				4,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 17,
			MidiNumbers = new int[6]
			{
				0,
				48,
				55,
				62,
				0,
				0
			},
			key = "C",
			suffix = "sus2",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				4,
				0,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 18,
			MidiNumbers = new int[6]
			{
				0,
				0,
				60,
				62,
				67,
				74
			},
			key = "C",
			suffix = "sus2",
			Fingers = new int[6]
			{
				0,
				0,
				3,
				1,
				2,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 19,
			MidiNumbers = new int[6]
			{
				0,
				0,
				60,
				67,
				72,
				74
			},
			key = "C",
			suffix = "sus2",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 20,
			MidiNumbers = new int[6]
			{
				0,
				48,
				53,
				55,
				60,
				65
			},
			key = "C",
			suffix = "sus4",
			Fingers = new int[6]
			{
				0,
				3,
				4,
				0,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 21,
			MidiNumbers = new int[6]
			{
				0,
				48,
				55,
				60,
				65,
				67
			},
			key = "C",
			suffix = "sus4",
			Fingers = new int[6]
			{
				0,
				1,
				2,
				3,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 22,
			MidiNumbers = new int[6]
			{
				48,
				55,
				60,
				65,
				67,
				72
			},
			key = "C",
			suffix = "sus4",
			Fingers = new int[6]
			{
				1,
				2,
				3,
				4,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 23,
			MidiNumbers = new int[6]
			{
				0,
				0,
				60,
				67,
				72,
				77
			},
			key = "C",
			suffix = "sus4",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				4,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 24,
			MidiNumbers = new int[6]
			{
				0,
				0,
				60,
				65,
				67,
				72
			},
			key = "C",
			suffix = "sus4",
			Fingers = new int[6]
			{
				0,
				0,
				3,
				4,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 25,
			MidiNumbers = new int[6]
			{
				0,
				48,
				55,
				58,
				63,
				67
			},
			key = "C",
			suffix = "m7",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				1,
				2,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 26,
			MidiNumbers = new int[6]
			{
				48,
				55,
				60,
				63,
				70,
				72
			},
			key = "C",
			suffix = "m7",
			Fingers = new int[6]
			{
				1,
				2,
				3,
				1,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 27,
			MidiNumbers = new int[6]
			{
				48,
				0,
				58,
				63,
				67,
				0
			},
			key = "C",
			suffix = "m7",
			Fingers = new int[6]
			{
				1,
				0,
				2,
				3,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 28,
			MidiNumbers = new int[6]
			{
				0,
				48,
				51,
				58,
				63,
				0
			},
			key = "C",
			suffix = "m7",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 29,
			MidiNumbers = new int[6]
			{
				48,
				55,
				58,
				63,
				67,
				72
			},
			key = "C",
			suffix = "m7",
			Fingers = new int[6]
			{
				1,
				3,
				1,
				1,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 30,
			MidiNumbers = new int[6]
			{
				0,
				48,
				51,
				58,
				60,
				0
			},
			key = "C",
			suffix = "m7",
			Fingers = new int[6]
			{
				0,
				3,
				1,
				4,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 31,
			MidiNumbers = new int[6]
			{
				0,
				0,
				60,
				67,
				70,
				75
			},
			key = "C",
			suffix = "m7",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				4,
				2,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 32,
			MidiNumbers = new int[6]
			{
				0,
				48,
				52,
				55,
				59,
				64
			},
			key = "C",
			suffix = "Maj7",
			Fingers = new int[6]
			{
				0,
				3,
				2,
				0,
				0,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 33,
			MidiNumbers = new int[6]
			{
				0,
				0,
				60,
				64,
				67,
				71
			},
			key = "C",
			suffix = "Maj7",
			Fingers = new int[6]
			{
				0,
				0,
				4,
				3,
				2,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 34,
			MidiNumbers = new int[6]
			{
				0,
				48,
				55,
				59,
				64,
				67
			},
			key = "C",
			suffix = "Maj7",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				2,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 35,
			MidiNumbers = new int[6]
			{
				48,
				0,
				59,
				64,
				67,
				0
			},
			key = "C",
			suffix = "Maj7",
			Fingers = new int[6]
			{
				1,
				0,
				3,
				4,
				2,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 36,
			MidiNumbers = new int[6]
			{
				0,
				0,
				60,
				67,
				71,
				76
			},
			key = "C",
			suffix = "Maj7",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				3,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 37,
			MidiNumbers = new int[6]
			{
				48,
				55,
				59,
				64,
				67,
				72
			},
			key = "C",
			suffix = "Maj7",
			Fingers = new int[6]
			{
				1,
				4,
				2,
				3,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 38,
			MidiNumbers = new int[6]
			{
				48,
				55,
				60,
				0,
				0,
				0
			},
			key = "C",
			suffix = "5",
			Fingers = new int[6]
			{
				1,
				3,
				4,
				0,
				0,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 39,
			MidiNumbers = new int[6]
			{
				0,
				48,
				55,
				60,
				0,
				0
			},
			key = "C",
			suffix = "5",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				4,
				0,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 40,
			MidiNumbers = new int[6]
			{
				0,
				0,
				0,
				60,
				67,
				72
			},
			key = "C",
			suffix = "5",
			Fingers = new int[6]
			{
				0,
				0,
				0,
				1,
				4,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 41,
			MidiNumbers = new int[6]
			{
				0,
				0,
				60,
				67,
				72,
				0
			},
			key = "C",
			suffix = "5",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 42,
			MidiNumbers = new int[6]
			{
				0,
				48,
				52,
				56,
				60,
				0
			},
			key = "C",
			suffix = "+",
			Fingers = new int[6]
			{
				0,
				4,
				3,
				1,
				2,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 43,
			MidiNumbers = new int[6]
			{
				0,
				0,
				60,
				64,
				68,
				72
			},
			key = "C",
			suffix = "+",
			Fingers = new int[6]
			{
				0,
				0,
				4,
				2,
				3,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 44,
			MidiNumbers = new int[6]
			{
				48,
				52,
				56,
				60,
				0,
				0
			},
			key = "C",
			suffix = "+",
			Fingers = new int[6]
			{
				4,
				3,
				2,
				1,
				0,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 45,
			MidiNumbers = new int[6]
			{
				48,
				56,
				60,
				64,
				0,
				0
			},
			key = "C",
			suffix = "+",
			Fingers = new int[6]
			{
				1,
				4,
				3,
				2,
				0,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 46,
			MidiNumbers = new int[6]
			{
				0,
				48,
				0,
				60,
				64,
				68
			},
			key = "C",
			suffix = "+",
			Fingers = new int[6]
			{
				0,
				1,
				0,
				3,
				4,
				2
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 47,
			MidiNumbers = new int[6]
			{
				0,
				48,
				54,
				60,
				63,
				0
			},
			key = "C",
			suffix = "°",
			Fingers = new int[6]
			{
				0,
				1,
				2,
				4,
				3,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 48,
			MidiNumbers = new int[6]
			{
				48,
				0,
				60,
				63,
				66,
				0
			},
			key = "C",
			suffix = "°",
			Fingers = new int[6]
			{
				2,
				0,
				4,
				3,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 49,
			MidiNumbers = new int[6]
			{
				48,
				54,
				60,
				63,
				0,
				0
			},
			key = "C",
			suffix = "°",
			Fingers = new int[6]
			{
				1,
				3,
				4,
				2,
				0,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 50,
			MidiNumbers = new int[6]
			{
				0,
				48,
				55,
				60,
				64,
				69
			},
			key = "C",
			suffix = "6",
			Fingers = new int[6]
			{
				0,
				1,
				2,
				3,
				4,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 51,
			MidiNumbers = new int[6]
			{
				0,
				0,
				60,
				67,
				69,
				76
			},
			key = "C",
			suffix = "6",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 52,
			MidiNumbers = new int[6]
			{
				48,
				0,
				57,
				64,
				67,
				0
			},
			key = "C",
			suffix = "6",
			Fingers = new int[6]
			{
				2,
				0,
				1,
				4,
				3,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 53,
			MidiNumbers = new int[6]
			{
				0,
				48,
				52,
				58,
				62,
				64
			},
			key = "C",
			suffix = "9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 54,
			MidiNumbers = new int[6]
			{
				48,
				55,
				58,
				64,
				67,
				74
			},
			key = "C",
			suffix = "9",
			Fingers = new int[6]
			{
				1,
				3,
				1,
				2,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 55,
			MidiNumbers = new int[6]
			{
				0,
				48,
				52,
				58,
				62,
				67
			},
			key = "C",
			suffix = "9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				3,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 56,
			MidiNumbers = new int[6]
			{
				48,
				0,
				58,
				62,
				64,
				0
			},
			key = "C",
			suffix = "9",
			Fingers = new int[6]
			{
				3,
				0,
				4,
				2,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 57,
			MidiNumbers = new int[6]
			{
				0,
				0,
				60,
				64,
				70,
				74
			},
			key = "C",
			suffix = "9",
			Fingers = new int[6]
			{
				0,
				0,
				2,
				1,
				4,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 58,
			MidiNumbers = new int[6]
			{
				48,
				52,
				58,
				62,
				67,
				0
			},
			key = "C",
			suffix = "9",
			Fingers = new int[6]
			{
				2,
				1,
				3,
				1,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 59,
			MidiNumbers = new int[6]
			{
				0,
				48,
				52,
				58,
				60,
				65
			},
			key = "C",
			suffix = "11",
			Fingers = new int[6]
			{
				0,
				3,
				2,
				4,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 60,
			MidiNumbers = new int[6]
			{
				0,
				48,
				53,
				58,
				64,
				67
			},
			key = "C",
			suffix = "11",
			Fingers = new int[6]
			{
				0,
				1,
				1,
				1,
				3,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 61,
			MidiNumbers = new int[6]
			{
				0,
				0,
				60,
				65,
				70,
				76
			},
			key = "C",
			suffix = "11",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				1,
				2,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 62,
			MidiNumbers = new int[6]
			{
				0,
				48,
				52,
				58,
				62,
				69
			},
			key = "C",
			suffix = "13",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 63,
			MidiNumbers = new int[6]
			{
				48,
				0,
				58,
				62,
				64,
				69
			},
			key = "C",
			suffix = "13",
			Fingers = new int[6]
			{
				3,
				0,
				4,
				2,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 64,
			MidiNumbers = new int[6]
			{
				48,
				0,
				58,
				64,
				69,
				74
			},
			key = "C",
			suffix = "13",
			Fingers = new int[6]
			{
				1,
				0,
				1,
				1,
				2,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 65,
			MidiNumbers = new int[6]
			{
				48,
				55,
				58,
				64,
				69,
				72
			},
			key = "C",
			suffix = "13",
			Fingers = new int[6]
			{
				1,
				3,
				1,
				2,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 66,
			MidiNumbers = new int[6]
			{
				0,
				48,
				53,
				58,
				60,
				65
			},
			key = "C",
			suffix = "7sus4",
			Fingers = new int[6]
			{
				0,
				2,
				3,
				4,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 67,
			MidiNumbers = new int[6]
			{
				0,
				48,
				55,
				58,
				65,
				67
			},
			key = "C",
			suffix = "7sus4",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				1,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 68,
			MidiNumbers = new int[6]
			{
				48,
				55,
				58,
				65,
				67,
				72
			},
			key = "C",
			suffix = "7sus4",
			Fingers = new int[6]
			{
				1,
				2,
				1,
				3,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 69,
			MidiNumbers = new int[6]
			{
				0,
				0,
				60,
				67,
				70,
				77
			},
			key = "C",
			suffix = "7sus4",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				2,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 70,
			MidiNumbers = new int[6]
			{
				0,
				48,
				50,
				58,
				62,
				65
			},
			key = "C",
			suffix = "9sus4",
			Fingers = new int[6]
			{
				0,
				2,
				0,
				3,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 71,
			MidiNumbers = new int[6]
			{
				0,
				48,
				53,
				58,
				62,
				67
			},
			key = "C",
			suffix = "9sus4",
			Fingers = new int[6]
			{
				0,
				1,
				1,
				1,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 72,
			MidiNumbers = new int[6]
			{
				48,
				0,
				58,
				62,
				65,
				70
			},
			key = "C",
			suffix = "9sus4",
			Fingers = new int[6]
			{
				3,
				0,
				4,
				2,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 73,
			MidiNumbers = new int[6]
			{
				48,
				0,
				58,
				62,
				65,
				0
			},
			key = "C",
			suffix = "9sus4",
			Fingers = new int[6]
			{
				3,
				0,
				4,
				2,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 74,
			MidiNumbers = new int[6]
			{
				0,
				48,
				53,
				58,
				62,
				69
			},
			key = "C",
			suffix = "13sus4",
			Fingers = new int[6]
			{
				0,
				1,
				1,
				1,
				1,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 75,
			MidiNumbers = new int[6]
			{
				48,
				0,
				58,
				65,
				69,
				74
			},
			key = "C",
			suffix = "13sus4",
			Fingers = new int[6]
			{
				1,
				0,
				2,
				4,
				4,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 76,
			MidiNumbers = new int[6]
			{
				0,
				48,
				54,
				57,
				63,
				0
			},
			key = "C",
			suffix = "°7",
			Fingers = new int[6]
			{
				0,
				2,
				3,
				1,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 77,
			MidiNumbers = new int[6]
			{
				48,
				0,
				57,
				63,
				66,
				0
			},
			key = "C",
			suffix = "°7",
			Fingers = new int[6]
			{
				3,
				0,
				1,
				4,
				2,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 78,
			MidiNumbers = new int[6]
			{
				0,
				0,
				60,
				66,
				69,
				75
			},
			key = "C",
			suffix = "°7",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				2,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 79,
			MidiNumbers = new int[6]
			{
				0,
				48,
				56,
				58,
				64,
				68
			},
			key = "C",
			suffix = "+7",
			Fingers = new int[6]
			{
				0,
				1,
				4,
				1,
				3,
				2
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 80,
			MidiNumbers = new int[6]
			{
				48,
				0,
				58,
				64,
				68,
				0
			},
			key = "C",
			suffix = "+7",
			Fingers = new int[6]
			{
				1,
				0,
				2,
				3,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 81,
			MidiNumbers = new int[6]
			{
				0,
				0,
				60,
				68,
				70,
				76
			},
			key = "C",
			suffix = "+7",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				4,
				2,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 82,
			MidiNumbers = new int[6]
			{
				0,
				48,
				0,
				58,
				64,
				68
			},
			key = "C",
			suffix = "+7",
			Fingers = new int[6]
			{
				0,
				1,
				0,
				2,
				4,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 83,
			MidiNumbers = new int[6]
			{
				0,
				48,
				54,
				58,
				64,
				0
			},
			key = "C",
			suffix = "7♭5",
			Fingers = new int[6]
			{
				0,
				1,
				2,
				1,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 84,
			MidiNumbers = new int[6]
			{
				48,
				0,
				58,
				64,
				66,
				0
			},
			key = "C",
			suffix = "7♭5",
			Fingers = new int[6]
			{
				2,
				0,
				3,
				4,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 85,
			MidiNumbers = new int[6]
			{
				0,
				0,
				60,
				66,
				70,
				76
			},
			key = "C",
			suffix = "7♭5",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				2,
				2,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 86,
			MidiNumbers = new int[6]
			{
				0,
				48,
				54,
				58,
				64,
				70
			},
			key = "C",
			suffix = "7♭5",
			Fingers = new int[6]
			{
				0,
				1,
				2,
				1,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 87,
			MidiNumbers = new int[6]
			{
				0,
				48,
				52,
				58,
				63,
				0
			},
			key = "C",
			suffix = "7♯9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 88,
			MidiNumbers = new int[6]
			{
				48,
				55,
				58,
				64,
				67,
				75
			},
			key = "C",
			suffix = "7♯9",
			Fingers = new int[6]
			{
				1,
				3,
				1,
				2,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 89,
			MidiNumbers = new int[6]
			{
				0,
				0,
				60,
				64,
				70,
				75
			},
			key = "C",
			suffix = "7♯9",
			Fingers = new int[6]
			{
				0,
				0,
				2,
				1,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 90,
			MidiNumbers = new int[6]
			{
				0,
				48,
				52,
				58,
				63,
				66
			},
			key = "C",
			suffix = "7♭5♯9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 91,
			MidiNumbers = new int[6]
			{
				48,
				54,
				58,
				64,
				67,
				72
			},
			key = "C",
			suffix = "7♯11",
			Fingers = new int[6]
			{
				1,
				2,
				1,
				3,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 92,
			MidiNumbers = new int[6]
			{
				0,
				48,
				52,
				58,
				61,
				0
			},
			key = "C",
			suffix = "7♭9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 93,
			MidiNumbers = new int[6]
			{
				48,
				55,
				58,
				64,
				67,
				73
			},
			key = "C",
			suffix = "7♭9",
			Fingers = new int[6]
			{
				1,
				4,
				1,
				2,
				1,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 94,
			MidiNumbers = new int[6]
			{
				0,
				48,
				52,
				58,
				61,
				67
			},
			key = "C",
			suffix = "7♭9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 95,
			MidiNumbers = new int[6]
			{
				0,
				0,
				60,
				64,
				70,
				73
			},
			key = "C",
			suffix = "7♭9",
			Fingers = new int[6]
			{
				0,
				0,
				2,
				1,
				3,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 96,
			MidiNumbers = new int[6]
			{
				0,
				48,
				52,
				58,
				62,
				68
			},
			key = "C",
			suffix = "+9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 97,
			MidiNumbers = new int[6]
			{
				48,
				0,
				58,
				64,
				68,
				74
			},
			key = "C",
			suffix = "+9",
			Fingers = new int[6]
			{
				1,
				0,
				2,
				3,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 98,
			MidiNumbers = new int[6]
			{
				0,
				48,
				51,
				55,
				59,
				67
			},
			key = "C",
			suffix = "mMaj7",
			Fingers = new int[6]
			{
				0,
				3,
				1,
				0,
				0,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 99,
			MidiNumbers = new int[6]
			{
				0,
				48,
				55,
				59,
				63,
				67
			},
			key = "C",
			suffix = "mMaj7",
			Fingers = new int[6]
			{
				0,
				1,
				4,
				2,
				3,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 100,
			MidiNumbers = new int[6]
			{
				48,
				0,
				59,
				63,
				67,
				0
			},
			key = "C",
			suffix = "mMaj7",
			Fingers = new int[6]
			{
				1,
				0,
				4,
				2,
				3,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 101,
			MidiNumbers = new int[6]
			{
				48,
				55,
				59,
				63,
				67,
				72
			},
			key = "C",
			suffix = "mMaj7",
			Fingers = new int[6]
			{
				1,
				3,
				2,
				1,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 102,
			MidiNumbers = new int[6]
			{
				0,
				0,
				60,
				67,
				71,
				75
			},
			key = "C",
			suffix = "mMaj7",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				4,
				2
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 103,
			MidiNumbers = new int[6]
			{
				0,
				0,
				60,
				63,
				67,
				71
			},
			key = "C",
			suffix = "mMaj7",
			Fingers = new int[6]
			{
				0,
				0,
				4,
				2,
				3,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 104,
			MidiNumbers = new int[6]
			{
				0,
				48,
				52,
				55,
				62,
				64
			},
			key = "C",
			suffix = "add9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				0,
				3,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 105,
			MidiNumbers = new int[6]
			{
				0,
				0,
				60,
				64,
				67,
				74
			},
			key = "C",
			suffix = "add9",
			Fingers = new int[6]
			{
				0,
				0,
				3,
				2,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 106,
			MidiNumbers = new int[6]
			{
				0,
				48,
				52,
				55,
				62,
				67
			},
			key = "C",
			suffix = "add9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				0,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 107,
			MidiNumbers = new int[6]
			{
				0,
				48,
				55,
				62,
				64,
				67
			},
			key = "C",
			suffix = "add9",
			Fingers = new int[6]
			{
				0,
				1,
				2,
				4,
				3,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 108,
			MidiNumbers = new int[6]
			{
				48,
				0,
				0,
				64,
				67,
				74
			},
			key = "C",
			suffix = "add9",
			Fingers = new int[6]
			{
				1,
				0,
				0,
				3,
				2,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 109,
			MidiNumbers = new int[6]
			{
				0,
				48,
				51,
				55,
				62,
				67
			},
			key = "C",
			suffix = "madd9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				0,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 110,
			MidiNumbers = new int[6]
			{
				0,
				0,
				60,
				63,
				67,
				74
			},
			key = "C",
			suffix = "madd9",
			Fingers = new int[6]
			{
				0,
				0,
				3,
				1,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 111,
			MidiNumbers = new int[6]
			{
				0,
				48,
				51,
				57,
				60,
				0
			},
			key = "C",
			suffix = "m6",
			Fingers = new int[6]
			{
				0,
				4,
				1,
				3,
				2,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 112,
			MidiNumbers = new int[6]
			{
				48,
				55,
				60,
				63,
				69,
				72
			},
			key = "C",
			suffix = "m6",
			Fingers = new int[6]
			{
				1,
				2,
				3,
				1,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 113,
			MidiNumbers = new int[6]
			{
				0,
				0,
				60,
				67,
				69,
				75
			},
			key = "C",
			suffix = "m6",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				1,
				2
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 114,
			MidiNumbers = new int[6]
			{
				0,
				48,
				55,
				57,
				63,
				0
			},
			key = "C",
			suffix = "m6",
			Fingers = new int[6]
			{
				0,
				2,
				4,
				1,
				3,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 115,
			MidiNumbers = new int[6]
			{
				0,
				48,
				51,
				57,
				60,
				67
			},
			key = "C",
			suffix = "m6",
			Fingers = new int[6]
			{
				0,
				3,
				1,
				2,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 116,
			MidiNumbers = new int[6]
			{
				48,
				0,
				57,
				63,
				67,
				0
			},
			key = "C",
			suffix = "m6",
			Fingers = new int[6]
			{
				2,
				0,
				1,
				3,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 117,
			MidiNumbers = new int[6]
			{
				0,
				48,
				0,
				57,
				63,
				67
			},
			key = "C",
			suffix = "m6",
			Fingers = new int[6]
			{
				0,
				2,
				0,
				1,
				4,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 118,
			MidiNumbers = new int[6]
			{
				0,
				48,
				51,
				56,
				60,
				67
			},
			key = "C",
			suffix = "m♭6",
			Fingers = new int[6]
			{
				0,
				3,
				1,
				1,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 119,
			MidiNumbers = new int[6]
			{
				48,
				0,
				56,
				63,
				67,
				0
			},
			key = "C",
			suffix = "m♭6",
			Fingers = new int[6]
			{
				2,
				0,
				1,
				3,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 120,
			MidiNumbers = new int[6]
			{
				0,
				48,
				54,
				58,
				63,
				0
			},
			key = "C",
			suffix = "m7♭5",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				2,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 121,
			MidiNumbers = new int[6]
			{
				48,
				0,
				58,
				63,
				66,
				0
			},
			key = "C",
			suffix = "m7♭5",
			Fingers = new int[6]
			{
				2,
				0,
				3,
				4,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 122,
			MidiNumbers = new int[6]
			{
				0,
				0,
				60,
				66,
				70,
				75
			},
			key = "C",
			suffix = "m7♭5",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				2,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 123,
			MidiNumbers = new int[6]
			{
				48,
				54,
				60,
				66,
				70,
				75
			},
			key = "C",
			suffix = "m7♭5",
			Fingers = new int[6]
			{
				1,
				2,
				3,
				4,
				4,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 124,
			MidiNumbers = new int[6]
			{
				0,
				48,
				0,
				58,
				63,
				66
			},
			key = "C",
			suffix = "m7♭5",
			Fingers = new int[6]
			{
				0,
				2,
				0,
				3,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 125,
			MidiNumbers = new int[6]
			{
				48,
				54,
				58,
				63,
				70,
				72
			},
			key = "C",
			suffix = "m7♭5",
			Fingers = new int[6]
			{
				1,
				2,
				1,
				1,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 126,
			MidiNumbers = new int[6]
			{
				48,
				55,
				58,
				63,
				67,
				74
			},
			key = "C",
			suffix = "m9",
			Fingers = new int[6]
			{
				1,
				2,
				1,
				1,
				1,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 127,
			MidiNumbers = new int[6]
			{
				0,
				48,
				51,
				58,
				62,
				0
			},
			key = "C",
			suffix = "m9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 128,
			MidiNumbers = new int[6]
			{
				0,
				48,
				51,
				58,
				62,
				67
			},
			key = "C",
			suffix = "m9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				3,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 129,
			MidiNumbers = new int[6]
			{
				0,
				0,
				60,
				63,
				70,
				74
			},
			key = "C",
			suffix = "m9",
			Fingers = new int[6]
			{
				0,
				0,
				2,
				1,
				4,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 130,
			MidiNumbers = new int[6]
			{
				48,
				0,
				58,
				63,
				67,
				74
			},
			key = "C",
			suffix = "m9",
			Fingers = new int[6]
			{
				2,
				0,
				3,
				3,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 131,
			MidiNumbers = new int[6]
			{
				0,
				48,
				51,
				58,
				60,
				65
			},
			key = "C",
			suffix = "m11",
			Fingers = new int[6]
			{
				0,
				3,
				1,
				4,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 132,
			MidiNumbers = new int[6]
			{
				0,
				48,
				53,
				58,
				63,
				67
			},
			key = "C",
			suffix = "m11",
			Fingers = new int[6]
			{
				0,
				1,
				1,
				1,
				2,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 133,
			MidiNumbers = new int[6]
			{
				48,
				0,
				58,
				63,
				65,
				0
			},
			key = "C",
			suffix = "m11",
			Fingers = new int[6]
			{
				2,
				0,
				3,
				4,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 134,
			MidiNumbers = new int[6]
			{
				0,
				0,
				60,
				65,
				70,
				75
			},
			key = "C",
			suffix = "m11",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				1,
				2,
				2
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 135,
			MidiNumbers = new int[6]
			{
				0,
				48,
				0,
				58,
				63,
				65
			},
			key = "C",
			suffix = "m11",
			Fingers = new int[6]
			{
				0,
				2,
				0,
				3,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 136,
			MidiNumbers = new int[6]
			{
				0,
				48,
				0,
				58,
				63,
				69
			},
			key = "C",
			suffix = "m13",
			Fingers = new int[6]
			{
				0,
				1,
				0,
				2,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 137,
			MidiNumbers = new int[6]
			{
				48,
				55,
				58,
				63,
				69,
				72
			},
			key = "C",
			suffix = "m13",
			Fingers = new int[6]
			{
				1,
				3,
				1,
				1,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 138,
			MidiNumbers = new int[6]
			{
				48,
				0,
				58,
				63,
				69,
				74
			},
			key = "C",
			suffix = "m13",
			Fingers = new int[6]
			{
				2,
				0,
				3,
				3,
				4,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 139,
			MidiNumbers = new int[6]
			{
				0,
				48,
				51,
				58,
				62,
				69
			},
			key = "C",
			suffix = "m13",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 140,
			MidiNumbers = new int[6]
			{
				0,
				48,
				55,
				58,
				63,
				69
			},
			key = "C",
			suffix = "m13",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				1,
				2,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 141,
			MidiNumbers = new int[6]
			{
				0,
				48,
				51,
				59,
				62,
				0
			},
			key = "C",
			suffix = "mMaj9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				4,
				3,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 142,
			MidiNumbers = new int[6]
			{
				48,
				55,
				59,
				63,
				67,
				74
			},
			key = "C",
			suffix = "mMaj9",
			Fingers = new int[6]
			{
				1,
				3,
				2,
				1,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 143,
			MidiNumbers = new int[6]
			{
				0,
				48,
				52,
				59,
				62,
				0
			},
			key = "C",
			suffix = "Maj9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				4,
				3,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 144,
			MidiNumbers = new int[6]
			{
				0,
				0,
				60,
				64,
				71,
				74
			},
			key = "C",
			suffix = "Maj9",
			Fingers = new int[6]
			{
				0,
				0,
				2,
				1,
				4,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 145,
			MidiNumbers = new int[6]
			{
				0,
				60,
				62,
				67,
				71,
				76
			},
			key = "C",
			suffix = "Maj9",
			Fingers = new int[6]
			{
				0,
				3,
				1,
				1,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 146,
			MidiNumbers = new int[6]
			{
				48,
				52,
				59,
				62,
				67,
				71
			},
			key = "C",
			suffix = "Maj9",
			Fingers = new int[6]
			{
				2,
				1,
				4,
				1,
				3,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 147,
			MidiNumbers = new int[6]
			{
				48,
				0,
				59,
				62,
				67,
				0
			},
			key = "C",
			suffix = "Maj9",
			Fingers = new int[6]
			{
				2,
				0,
				4,
				1,
				3,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 148,
			MidiNumbers = new int[6]
			{
				0,
				48,
				0,
				59,
				64,
				69
			},
			key = "C",
			suffix = "Maj13",
			Fingers = new int[6]
			{
				0,
				1,
				0,
				2,
				4,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 149,
			MidiNumbers = new int[6]
			{
				48,
				52,
				57,
				62,
				67,
				71
			},
			key = "C",
			suffix = "Maj13",
			Fingers = new int[6]
			{
				2,
				1,
				1,
				1,
				3,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 150,
			MidiNumbers = new int[6]
			{
				48,
				0,
				59,
				64,
				69,
				74
			},
			key = "C",
			suffix = "Maj13",
			Fingers = new int[6]
			{
				1,
				0,
				2,
				3,
				4,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 151,
			MidiNumbers = new int[6]
			{
				0,
				48,
				52,
				57,
				59,
				64
			},
			key = "C",
			suffix = "Maj13",
			Fingers = new int[6]
			{
				0,
				3,
				1,
				2,
				0,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 152,
			MidiNumbers = new int[6]
			{
				0,
				48,
				52,
				55,
				59,
				66
			},
			key = "C",
			suffix = "Maj7♯11",
			Fingers = new int[6]
			{
				0,
				3,
				1,
				0,
				0,
				2
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 153,
			MidiNumbers = new int[6]
			{
				0,
				48,
				54,
				59,
				64,
				67
			},
			key = "C",
			suffix = "Maj7♯11",
			Fingers = new int[6]
			{
				0,
				1,
				2,
				3,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 154,
			MidiNumbers = new int[6]
			{
				48,
				0,
				59,
				64,
				66,
				0
			},
			key = "C",
			suffix = "Maj7♯11",
			Fingers = new int[6]
			{
				2,
				0,
				3,
				4,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 155,
			MidiNumbers = new int[6]
			{
				0,
				0,
				60,
				66,
				71,
				76
			},
			key = "C",
			suffix = "Maj7♯11",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				2,
				3,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 156,
			MidiNumbers = new int[6]
			{
				0,
				48,
				52,
				58,
				62,
				66
			},
			key = "C",
			suffix = "9♭5",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 157,
			MidiNumbers = new int[6]
			{
				48,
				52,
				58,
				62,
				66,
				0
			},
			key = "C",
			suffix = "9♭5",
			Fingers = new int[6]
			{
				2,
				1,
				3,
				1,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 158,
			MidiNumbers = new int[6]
			{
				48,
				54,
				58,
				63,
				70,
				74
			},
			key = "C",
			suffix = "m9♭5",
			Fingers = new int[6]
			{
				1,
				2,
				1,
				1,
				4,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 159,
			MidiNumbers = new int[6]
			{
				0,
				48,
				52,
				58,
				61,
				68
			},
			key = "C",
			suffix = "+7♭9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 160,
			MidiNumbers = new int[6]
			{
				48,
				0,
				58,
				64,
				68,
				73
			},
			key = "C",
			suffix = "+7♭9",
			Fingers = new int[6]
			{
				1,
				0,
				2,
				3,
				3,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 161,
			MidiNumbers = new int[6]
			{
				0,
				48,
				52,
				58,
				63,
				68
			},
			key = "C",
			suffix = "+7♯9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				4,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 162,
			MidiNumbers = new int[6]
			{
				48,
				0,
				58,
				64,
				68,
				75
			},
			key = "C",
			suffix = "+7♯9",
			Fingers = new int[6]
			{
				1,
				0,
				2,
				3,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 163,
			MidiNumbers = new int[6]
			{
				0,
				49,
				56,
				61,
				65,
				68
			},
			key = "C♯",
			suffix = string.Empty,
			Fingers = new int[6]
			{
				0,
				1,
				3,
				3,
				3,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 164,
			MidiNumbers = new int[6]
			{
				49,
				56,
				61,
				65,
				68,
				73
			},
			key = "C♯",
			suffix = string.Empty,
			Fingers = new int[6]
			{
				1,
				3,
				4,
				2,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 165,
			MidiNumbers = new int[6]
			{
				0,
				49,
				53,
				56,
				61,
				65
			},
			key = "C♯",
			suffix = string.Empty,
			Fingers = new int[6]
			{
				0,
				4,
				3,
				1,
				2,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 166,
			MidiNumbers = new int[6]
			{
				49,
				53,
				56,
				61,
				65,
				73
			},
			key = "C♯",
			suffix = string.Empty,
			Fingers = new int[6]
			{
				3,
				2,
				1,
				1,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 167,
			MidiNumbers = new int[6]
			{
				0,
				0,
				61,
				68,
				73,
				77
			},
			key = "C♯",
			suffix = string.Empty,
			Fingers = new int[6]
			{
				0,
				0,
				1,
				2,
				4,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 168,
			MidiNumbers = new int[6]
			{
				0,
				49,
				56,
				61,
				64,
				68
			},
			key = "C♯",
			suffix = "m",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				4,
				2,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 169,
			MidiNumbers = new int[6]
			{
				49,
				56,
				61,
				64,
				68,
				73
			},
			key = "C♯",
			suffix = "m",
			Fingers = new int[6]
			{
				1,
				3,
				4,
				1,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 170,
			MidiNumbers = new int[6]
			{
				0,
				0,
				61,
				64,
				68,
				73
			},
			key = "C♯",
			suffix = "m",
			Fingers = new int[6]
			{
				0,
				0,
				3,
				1,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 171,
			MidiNumbers = new int[6]
			{
				0,
				0,
				61,
				68,
				73,
				76
			},
			key = "C♯",
			suffix = "m",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				4,
				2
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 172,
			MidiNumbers = new int[6]
			{
				0,
				49,
				56,
				59,
				65,
				68
			},
			key = "C♯",
			suffix = "7",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				1,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 173,
			MidiNumbers = new int[6]
			{
				49,
				56,
				59,
				65,
				68,
				73
			},
			key = "C♯",
			suffix = "7",
			Fingers = new int[6]
			{
				1,
				3,
				1,
				2,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 174,
			MidiNumbers = new int[6]
			{
				49,
				0,
				59,
				65,
				68,
				0
			},
			key = "C♯",
			suffix = "7",
			Fingers = new int[6]
			{
				1,
				0,
				2,
				4,
				3,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 175,
			MidiNumbers = new int[6]
			{
				0,
				49,
				53,
				59,
				61,
				0
			},
			key = "C♯",
			suffix = "7",
			Fingers = new int[6]
			{
				0,
				3,
				2,
				4,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 176,
			MidiNumbers = new int[6]
			{
				0,
				0,
				61,
				68,
				71,
				77
			},
			key = "C♯",
			suffix = "7",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				2,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 177,
			MidiNumbers = new int[6]
			{
				0,
				49,
				56,
				61,
				63,
				68
			},
			key = "C♯",
			suffix = "sus2",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				4,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 178,
			MidiNumbers = new int[6]
			{
				0,
				49,
				51,
				56,
				61,
				68
			},
			key = "C♯",
			suffix = "sus2",
			Fingers = new int[6]
			{
				0,
				3,
				1,
				1,
				2,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 179,
			MidiNumbers = new int[6]
			{
				0,
				49,
				56,
				63,
				0,
				0
			},
			key = "C♯",
			suffix = "sus2",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				4,
				0,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 180,
			MidiNumbers = new int[6]
			{
				0,
				0,
				61,
				63,
				68,
				75
			},
			key = "C♯",
			suffix = "sus2",
			Fingers = new int[6]
			{
				0,
				0,
				3,
				1,
				2,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 181,
			MidiNumbers = new int[6]
			{
				0,
				0,
				61,
				68,
				73,
				75
			},
			key = "C♯",
			suffix = "sus2",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 182,
			MidiNumbers = new int[6]
			{
				0,
				49,
				56,
				61,
				66,
				68
			},
			key = "C♯",
			suffix = "sus4",
			Fingers = new int[6]
			{
				0,
				1,
				2,
				3,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 183,
			MidiNumbers = new int[6]
			{
				49,
				56,
				61,
				66,
				68,
				73
			},
			key = "C♯",
			suffix = "sus4",
			Fingers = new int[6]
			{
				1,
				2,
				3,
				4,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 184,
			MidiNumbers = new int[6]
			{
				0,
				49,
				54,
				56,
				61,
				0
			},
			key = "C♯",
			suffix = "sus4",
			Fingers = new int[6]
			{
				0,
				3,
				4,
				1,
				2,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 185,
			MidiNumbers = new int[6]
			{
				0,
				0,
				61,
				68,
				73,
				78
			},
			key = "C♯",
			suffix = "sus4",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				4,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 186,
			MidiNumbers = new int[6]
			{
				0,
				0,
				61,
				66,
				68,
				73
			},
			key = "C♯",
			suffix = "sus4",
			Fingers = new int[6]
			{
				0,
				0,
				3,
				4,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 187,
			MidiNumbers = new int[6]
			{
				0,
				49,
				56,
				59,
				64,
				68
			},
			key = "C♯",
			suffix = "m7",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				1,
				2,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 188,
			MidiNumbers = new int[6]
			{
				49,
				56,
				61,
				64,
				71,
				73
			},
			key = "C♯",
			suffix = "m7",
			Fingers = new int[6]
			{
				1,
				2,
				3,
				1,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 189,
			MidiNumbers = new int[6]
			{
				49,
				0,
				59,
				64,
				68,
				0
			},
			key = "C♯",
			suffix = "m7",
			Fingers = new int[6]
			{
				1,
				0,
				2,
				3,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 190,
			MidiNumbers = new int[6]
			{
				0,
				49,
				52,
				59,
				64,
				0
			},
			key = "C♯",
			suffix = "m7",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 191,
			MidiNumbers = new int[6]
			{
				49,
				56,
				59,
				64,
				68,
				73
			},
			key = "C♯",
			suffix = "m7",
			Fingers = new int[6]
			{
				1,
				3,
				1,
				1,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 192,
			MidiNumbers = new int[6]
			{
				0,
				49,
				52,
				59,
				61,
				0
			},
			key = "C♯",
			suffix = "m7",
			Fingers = new int[6]
			{
				0,
				3,
				1,
				4,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 193,
			MidiNumbers = new int[6]
			{
				0,
				0,
				61,
				68,
				71,
				76
			},
			key = "C♯",
			suffix = "m7",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				4,
				2,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 194,
			MidiNumbers = new int[6]
			{
				0,
				49,
				56,
				60,
				65,
				68
			},
			key = "C♯",
			suffix = "Maj7",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				2,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 195,
			MidiNumbers = new int[6]
			{
				0,
				0,
				61,
				65,
				68,
				72
			},
			key = "C♯",
			suffix = "Maj7",
			Fingers = new int[6]
			{
				0,
				0,
				4,
				3,
				2,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 196,
			MidiNumbers = new int[6]
			{
				0,
				0,
				61,
				68,
				72,
				77
			},
			key = "C♯",
			suffix = "Maj7",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				3,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 197,
			MidiNumbers = new int[6]
			{
				0,
				49,
				53,
				56,
				60,
				0
			},
			key = "C♯",
			suffix = "Maj7",
			Fingers = new int[6]
			{
				0,
				4,
				3,
				1,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 198,
			MidiNumbers = new int[6]
			{
				49,
				0,
				60,
				65,
				68,
				0
			},
			key = "C♯",
			suffix = "Maj7",
			Fingers = new int[6]
			{
				1,
				0,
				3,
				4,
				2,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 199,
			MidiNumbers = new int[6]
			{
				49,
				56,
				60,
				65,
				68,
				73
			},
			key = "C♯",
			suffix = "Maj7",
			Fingers = new int[6]
			{
				1,
				4,
				2,
				3,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 200,
			MidiNumbers = new int[6]
			{
				0,
				49,
				56,
				61,
				0,
				0
			},
			key = "C♯",
			suffix = "5",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				4,
				0,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 201,
			MidiNumbers = new int[6]
			{
				49,
				56,
				61,
				0,
				0,
				0
			},
			key = "C♯",
			suffix = "5",
			Fingers = new int[6]
			{
				1,
				3,
				4,
				0,
				0,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 202,
			MidiNumbers = new int[6]
			{
				0,
				0,
				0,
				61,
				68,
				73
			},
			key = "C♯",
			suffix = "5",
			Fingers = new int[6]
			{
				0,
				0,
				0,
				1,
				4,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 203,
			MidiNumbers = new int[6]
			{
				0,
				0,
				61,
				68,
				73,
				0
			},
			key = "C♯",
			suffix = "5",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 204,
			MidiNumbers = new int[6]
			{
				0,
				49,
				53,
				57,
				61,
				0
			},
			key = "C♯",
			suffix = "+",
			Fingers = new int[6]
			{
				0,
				4,
				3,
				1,
				2,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 205,
			MidiNumbers = new int[6]
			{
				0,
				0,
				61,
				65,
				69,
				73
			},
			key = "C♯",
			suffix = "+",
			Fingers = new int[6]
			{
				0,
				0,
				4,
				2,
				3,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 206,
			MidiNumbers = new int[6]
			{
				49,
				53,
				57,
				61,
				0,
				0
			},
			key = "C♯",
			suffix = "+",
			Fingers = new int[6]
			{
				4,
				3,
				2,
				1,
				0,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 207,
			MidiNumbers = new int[6]
			{
				49,
				57,
				61,
				65,
				0,
				0
			},
			key = "C♯",
			suffix = "+",
			Fingers = new int[6]
			{
				1,
				4,
				3,
				2,
				0,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 208,
			MidiNumbers = new int[6]
			{
				0,
				49,
				0,
				61,
				65,
				69
			},
			key = "C♯",
			suffix = "+",
			Fingers = new int[6]
			{
				0,
				1,
				0,
				3,
				4,
				2
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 209,
			MidiNumbers = new int[6]
			{
				0,
				49,
				55,
				61,
				64,
				0
			},
			key = "C♯",
			suffix = "°",
			Fingers = new int[6]
			{
				0,
				1,
				2,
				4,
				3,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 210,
			MidiNumbers = new int[6]
			{
				49,
				0,
				61,
				64,
				67,
				0
			},
			key = "C♯",
			suffix = "°",
			Fingers = new int[6]
			{
				2,
				0,
				4,
				3,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 211,
			MidiNumbers = new int[6]
			{
				49,
				55,
				61,
				64,
				0,
				0
			},
			key = "C♯",
			suffix = "°",
			Fingers = new int[6]
			{
				1,
				3,
				4,
				2,
				0,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 212,
			MidiNumbers = new int[6]
			{
				0,
				49,
				56,
				61,
				65,
				70
			},
			key = "C♯",
			suffix = "6",
			Fingers = new int[6]
			{
				0,
				1,
				2,
				3,
				4,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 213,
			MidiNumbers = new int[6]
			{
				0,
				0,
				61,
				65,
				70,
				73
			},
			key = "C♯",
			suffix = "6",
			Fingers = new int[6]
			{
				0,
				0,
				3,
				2,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 214,
			MidiNumbers = new int[6]
			{
				0,
				0,
				61,
				68,
				70,
				77
			},
			key = "C♯",
			suffix = "6",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 215,
			MidiNumbers = new int[6]
			{
				49,
				0,
				58,
				65,
				68,
				0
			},
			key = "C♯",
			suffix = "6",
			Fingers = new int[6]
			{
				2,
				0,
				1,
				4,
				3,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 216,
			MidiNumbers = new int[6]
			{
				0,
				49,
				53,
				59,
				63,
				0
			},
			key = "C♯",
			suffix = "9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 217,
			MidiNumbers = new int[6]
			{
				49,
				56,
				59,
				65,
				68,
				75
			},
			key = "C♯",
			suffix = "9",
			Fingers = new int[6]
			{
				1,
				3,
				1,
				2,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 218,
			MidiNumbers = new int[6]
			{
				49,
				0,
				59,
				63,
				68,
				73
			},
			key = "C♯",
			suffix = "9",
			Fingers = new int[6]
			{
				2,
				0,
				3,
				1,
				4,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 219,
			MidiNumbers = new int[6]
			{
				0,
				49,
				53,
				59,
				63,
				68
			},
			key = "C♯",
			suffix = "9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				3,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 220,
			MidiNumbers = new int[6]
			{
				49,
				0,
				59,
				63,
				65,
				0
			},
			key = "C♯",
			suffix = "9",
			Fingers = new int[6]
			{
				3,
				0,
				4,
				2,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 221,
			MidiNumbers = new int[6]
			{
				0,
				0,
				61,
				65,
				71,
				75
			},
			key = "C♯",
			suffix = "9",
			Fingers = new int[6]
			{
				0,
				0,
				2,
				1,
				4,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 222,
			MidiNumbers = new int[6]
			{
				49,
				53,
				59,
				63,
				68,
				0
			},
			key = "C♯",
			suffix = "9",
			Fingers = new int[6]
			{
				2,
				1,
				3,
				1,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 223,
			MidiNumbers = new int[6]
			{
				0,
				49,
				53,
				59,
				61,
				66
			},
			key = "C♯",
			suffix = "11",
			Fingers = new int[6]
			{
				0,
				3,
				2,
				4,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 224,
			MidiNumbers = new int[6]
			{
				0,
				49,
				54,
				59,
				65,
				68
			},
			key = "C♯",
			suffix = "11",
			Fingers = new int[6]
			{
				0,
				1,
				1,
				1,
				3,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 225,
			MidiNumbers = new int[6]
			{
				0,
				0,
				61,
				66,
				71,
				77
			},
			key = "C♯",
			suffix = "11",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				1,
				2,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 226,
			MidiNumbers = new int[6]
			{
				0,
				49,
				53,
				59,
				63,
				70
			},
			key = "C♯",
			suffix = "13",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 227,
			MidiNumbers = new int[6]
			{
				49,
				0,
				59,
				63,
				65,
				70
			},
			key = "C♯",
			suffix = "13",
			Fingers = new int[6]
			{
				3,
				0,
				4,
				2,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 228,
			MidiNumbers = new int[6]
			{
				49,
				0,
				59,
				65,
				70,
				75
			},
			key = "C♯",
			suffix = "13",
			Fingers = new int[6]
			{
				1,
				0,
				1,
				1,
				2,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 229,
			MidiNumbers = new int[6]
			{
				49,
				56,
				59,
				65,
				70,
				73
			},
			key = "C♯",
			suffix = "13",
			Fingers = new int[6]
			{
				1,
				3,
				1,
				2,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 230,
			MidiNumbers = new int[6]
			{
				0,
				49,
				54,
				59,
				61,
				66
			},
			key = "C♯",
			suffix = "7sus4",
			Fingers = new int[6]
			{
				0,
				2,
				3,
				4,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 231,
			MidiNumbers = new int[6]
			{
				49,
				56,
				59,
				66,
				68,
				73
			},
			key = "C♯",
			suffix = "7sus4",
			Fingers = new int[6]
			{
				1,
				2,
				1,
				3,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 232,
			MidiNumbers = new int[6]
			{
				0,
				49,
				56,
				59,
				66,
				68
			},
			key = "C♯",
			suffix = "7sus4",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				1,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 233,
			MidiNumbers = new int[6]
			{
				0,
				0,
				61,
				68,
				71,
				78
			},
			key = "C♯",
			suffix = "7sus4",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				2,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 234,
			MidiNumbers = new int[6]
			{
				0,
				49,
				54,
				59,
				63,
				68
			},
			key = "C♯",
			suffix = "9sus4",
			Fingers = new int[6]
			{
				0,
				1,
				1,
				1,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 235,
			MidiNumbers = new int[6]
			{
				49,
				0,
				59,
				63,
				66,
				0
			},
			key = "C♯",
			suffix = "9sus4",
			Fingers = new int[6]
			{
				3,
				0,
				4,
				2,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 236,
			MidiNumbers = new int[6]
			{
				49,
				0,
				59,
				63,
				66,
				71
			},
			key = "C♯",
			suffix = "9sus4",
			Fingers = new int[6]
			{
				3,
				0,
				4,
				2,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 237,
			MidiNumbers = new int[6]
			{
				0,
				49,
				54,
				59,
				63,
				70
			},
			key = "C♯",
			suffix = "13sus4",
			Fingers = new int[6]
			{
				0,
				1,
				1,
				1,
				1,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 238,
			MidiNumbers = new int[6]
			{
				49,
				0,
				59,
				66,
				70,
				75
			},
			key = "C♯",
			suffix = "13sus4",
			Fingers = new int[6]
			{
				1,
				0,
				2,
				4,
				4,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 239,
			MidiNumbers = new int[6]
			{
				0,
				49,
				55,
				58,
				64,
				0
			},
			key = "C♯",
			suffix = "°7",
			Fingers = new int[6]
			{
				0,
				2,
				3,
				1,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 240,
			MidiNumbers = new int[6]
			{
				49,
				55,
				58,
				64,
				0,
				0
			},
			key = "C♯",
			suffix = "°7",
			Fingers = new int[6]
			{
				2,
				4,
				1,
				3,
				0,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 241,
			MidiNumbers = new int[6]
			{
				49,
				0,
				58,
				64,
				67,
				0
			},
			key = "C♯",
			suffix = "°7",
			Fingers = new int[6]
			{
				3,
				0,
				1,
				4,
				2,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 242,
			MidiNumbers = new int[6]
			{
				0,
				0,
				61,
				67,
				70,
				76
			},
			key = "C♯",
			suffix = "°7",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				2,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 243,
			MidiNumbers = new int[6]
			{
				0,
				49,
				57,
				59,
				65,
				69
			},
			key = "C♯",
			suffix = "+7",
			Fingers = new int[6]
			{
				0,
				1,
				4,
				1,
				3,
				2
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 244,
			MidiNumbers = new int[6]
			{
				49,
				0,
				59,
				65,
				69,
				0
			},
			key = "C♯",
			suffix = "+7",
			Fingers = new int[6]
			{
				1,
				0,
				2,
				3,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 245,
			MidiNumbers = new int[6]
			{
				0,
				0,
				61,
				69,
				71,
				77
			},
			key = "C♯",
			suffix = "+7",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				4,
				2,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 246,
			MidiNumbers = new int[6]
			{
				0,
				49,
				0,
				59,
				65,
				69
			},
			key = "C♯",
			suffix = "+7",
			Fingers = new int[6]
			{
				0,
				1,
				0,
				2,
				4,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 247,
			MidiNumbers = new int[6]
			{
				0,
				49,
				55,
				59,
				65,
				0
			},
			key = "C♯",
			suffix = "7♭5",
			Fingers = new int[6]
			{
				0,
				1,
				2,
				1,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 248,
			MidiNumbers = new int[6]
			{
				49,
				0,
				59,
				65,
				67,
				0
			},
			key = "C♯",
			suffix = "7♭5",
			Fingers = new int[6]
			{
				2,
				0,
				3,
				4,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 249,
			MidiNumbers = new int[6]
			{
				0,
				0,
				61,
				67,
				71,
				77
			},
			key = "C♯",
			suffix = "7♭5",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				2,
				2,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 250,
			MidiNumbers = new int[6]
			{
				0,
				49,
				55,
				59,
				65,
				71
			},
			key = "C♯",
			suffix = "7♭5",
			Fingers = new int[6]
			{
				0,
				1,
				2,
				1,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 251,
			MidiNumbers = new int[6]
			{
				0,
				49,
				53,
				59,
				64,
				0
			},
			key = "C♯",
			suffix = "7♯9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 252,
			MidiNumbers = new int[6]
			{
				49,
				56,
				59,
				65,
				68,
				76
			},
			key = "C♯",
			suffix = "7♯9",
			Fingers = new int[6]
			{
				1,
				3,
				1,
				2,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 253,
			MidiNumbers = new int[6]
			{
				0,
				0,
				61,
				65,
				71,
				76
			},
			key = "C♯",
			suffix = "7♯9",
			Fingers = new int[6]
			{
				0,
				0,
				2,
				1,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 254,
			MidiNumbers = new int[6]
			{
				0,
				49,
				53,
				59,
				64,
				67
			},
			key = "C♯",
			suffix = "7♭5♯9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 255,
			MidiNumbers = new int[6]
			{
				49,
				55,
				59,
				65,
				68,
				73
			},
			key = "C♯",
			suffix = "7♯11",
			Fingers = new int[6]
			{
				1,
				2,
				1,
				3,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 256,
			MidiNumbers = new int[6]
			{
				0,
				49,
				53,
				59,
				62,
				0
			},
			key = "C♯",
			suffix = "7♭9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 257,
			MidiNumbers = new int[6]
			{
				49,
				56,
				59,
				65,
				68,
				74
			},
			key = "C♯",
			suffix = "7♭9",
			Fingers = new int[6]
			{
				1,
				4,
				1,
				2,
				1,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 258,
			MidiNumbers = new int[6]
			{
				0,
				49,
				53,
				59,
				62,
				68
			},
			key = "C♯",
			suffix = "7♭9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 259,
			MidiNumbers = new int[6]
			{
				0,
				0,
				61,
				65,
				71,
				74
			},
			key = "C♯",
			suffix = "7♭9",
			Fingers = new int[6]
			{
				0,
				0,
				2,
				1,
				3,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 260,
			MidiNumbers = new int[6]
			{
				0,
				49,
				53,
				59,
				63,
				69
			},
			key = "C♯",
			suffix = "+9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 261,
			MidiNumbers = new int[6]
			{
				49,
				0,
				59,
				65,
				69,
				75
			},
			key = "C♯",
			suffix = "+9",
			Fingers = new int[6]
			{
				1,
				0,
				2,
				3,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 262,
			MidiNumbers = new int[6]
			{
				0,
				49,
				56,
				60,
				64,
				68
			},
			key = "C♯",
			suffix = "mMaj7",
			Fingers = new int[6]
			{
				0,
				1,
				4,
				2,
				3,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 263,
			MidiNumbers = new int[6]
			{
				49,
				0,
				60,
				64,
				68,
				0
			},
			key = "C♯",
			suffix = "mMaj7",
			Fingers = new int[6]
			{
				1,
				0,
				4,
				2,
				3,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 264,
			MidiNumbers = new int[6]
			{
				49,
				56,
				60,
				64,
				68,
				73
			},
			key = "C♯",
			suffix = "mMaj7",
			Fingers = new int[6]
			{
				1,
				3,
				2,
				1,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 265,
			MidiNumbers = new int[6]
			{
				0,
				0,
				61,
				68,
				72,
				76
			},
			key = "C♯",
			suffix = "mMaj7",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				4,
				2
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 266,
			MidiNumbers = new int[6]
			{
				0,
				0,
				61,
				64,
				68,
				72
			},
			key = "C♯",
			suffix = "mMaj7",
			Fingers = new int[6]
			{
				0,
				0,
				4,
				2,
				3,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 267,
			MidiNumbers = new int[6]
			{
				0,
				0,
				61,
				65,
				68,
				75
			},
			key = "C♯",
			suffix = "add9",
			Fingers = new int[6]
			{
				0,
				0,
				3,
				2,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 268,
			MidiNumbers = new int[6]
			{
				0,
				49,
				51,
				56,
				61,
				65
			},
			key = "C♯",
			suffix = "add9",
			Fingers = new int[6]
			{
				0,
				4,
				1,
				1,
				2,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 269,
			MidiNumbers = new int[6]
			{
				0,
				49,
				56,
				63,
				65,
				68
			},
			key = "C♯",
			suffix = "add9",
			Fingers = new int[6]
			{
				0,
				1,
				2,
				4,
				3,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 270,
			MidiNumbers = new int[6]
			{
				49,
				0,
				0,
				65,
				68,
				75
			},
			key = "C♯",
			suffix = "add9",
			Fingers = new int[6]
			{
				1,
				0,
				0,
				3,
				2,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 271,
			MidiNumbers = new int[6]
			{
				0,
				0,
				61,
				64,
				68,
				75
			},
			key = "C♯",
			suffix = "madd9",
			Fingers = new int[6]
			{
				0,
				0,
				3,
				1,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 272,
			MidiNumbers = new int[6]
			{
				0,
				49,
				52,
				58,
				61,
				0
			},
			key = "C♯",
			suffix = "m6",
			Fingers = new int[6]
			{
				0,
				4,
				1,
				3,
				2,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 273,
			MidiNumbers = new int[6]
			{
				49,
				56,
				61,
				64,
				70,
				73
			},
			key = "C♯",
			suffix = "m6",
			Fingers = new int[6]
			{
				1,
				2,
				3,
				1,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 274,
			MidiNumbers = new int[6]
			{
				0,
				0,
				61,
				68,
				70,
				76
			},
			key = "C♯",
			suffix = "m6",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				1,
				2
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 275,
			MidiNumbers = new int[6]
			{
				0,
				49,
				56,
				58,
				64,
				0
			},
			key = "C♯",
			suffix = "m6",
			Fingers = new int[6]
			{
				0,
				2,
				4,
				1,
				3,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 276,
			MidiNumbers = new int[6]
			{
				0,
				49,
				52,
				58,
				61,
				68
			},
			key = "C♯",
			suffix = "m6",
			Fingers = new int[6]
			{
				0,
				3,
				1,
				2,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 277,
			MidiNumbers = new int[6]
			{
				49,
				0,
				58,
				64,
				68,
				0
			},
			key = "C♯",
			suffix = "m6",
			Fingers = new int[6]
			{
				2,
				0,
				1,
				3,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 278,
			MidiNumbers = new int[6]
			{
				0,
				49,
				0,
				58,
				64,
				68
			},
			key = "C♯",
			suffix = "m6",
			Fingers = new int[6]
			{
				0,
				2,
				0,
				1,
				4,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 279,
			MidiNumbers = new int[6]
			{
				0,
				49,
				52,
				57,
				61,
				68
			},
			key = "C♯",
			suffix = "m♭6",
			Fingers = new int[6]
			{
				0,
				3,
				1,
				1,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 280,
			MidiNumbers = new int[6]
			{
				49,
				0,
				57,
				64,
				68,
				0
			},
			key = "C♯",
			suffix = "m♭6",
			Fingers = new int[6]
			{
				2,
				0,
				1,
				3,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 281,
			MidiNumbers = new int[6]
			{
				0,
				49,
				55,
				59,
				64,
				0
			},
			key = "C♯",
			suffix = "m7♭5",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				2,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 282,
			MidiNumbers = new int[6]
			{
				49,
				0,
				59,
				64,
				67,
				0
			},
			key = "C♯",
			suffix = "m7♭5",
			Fingers = new int[6]
			{
				2,
				0,
				3,
				4,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 283,
			MidiNumbers = new int[6]
			{
				49,
				55,
				61,
				67,
				71,
				76
			},
			key = "C♯",
			suffix = "m7♭5",
			Fingers = new int[6]
			{
				1,
				2,
				3,
				4,
				4,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 284,
			MidiNumbers = new int[6]
			{
				0,
				0,
				61,
				67,
				71,
				76
			},
			key = "C♯",
			suffix = "m7♭5",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				2,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 285,
			MidiNumbers = new int[6]
			{
				0,
				49,
				0,
				59,
				64,
				67
			},
			key = "C♯",
			suffix = "m7♭5",
			Fingers = new int[6]
			{
				0,
				2,
				0,
				3,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 286,
			MidiNumbers = new int[6]
			{
				49,
				55,
				59,
				64,
				71,
				73
			},
			key = "C♯",
			suffix = "m7♭5",
			Fingers = new int[6]
			{
				1,
				2,
				1,
				1,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 287,
			MidiNumbers = new int[6]
			{
				0,
				49,
				52,
				59,
				63,
				0
			},
			key = "C♯",
			suffix = "m9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 288,
			MidiNumbers = new int[6]
			{
				49,
				56,
				59,
				64,
				68,
				75
			},
			key = "C♯",
			suffix = "m9",
			Fingers = new int[6]
			{
				1,
				2,
				1,
				1,
				1,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 289,
			MidiNumbers = new int[6]
			{
				0,
				49,
				52,
				59,
				63,
				68
			},
			key = "C♯",
			suffix = "m9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				3,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 290,
			MidiNumbers = new int[6]
			{
				0,
				0,
				61,
				64,
				71,
				75
			},
			key = "C♯",
			suffix = "m9",
			Fingers = new int[6]
			{
				0,
				0,
				2,
				1,
				4,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 291,
			MidiNumbers = new int[6]
			{
				49,
				0,
				59,
				64,
				68,
				75
			},
			key = "C♯",
			suffix = "m9",
			Fingers = new int[6]
			{
				2,
				0,
				3,
				3,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 292,
			MidiNumbers = new int[6]
			{
				0,
				49,
				52,
				59,
				61,
				66
			},
			key = "C♯",
			suffix = "m11",
			Fingers = new int[6]
			{
				0,
				3,
				1,
				4,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 293,
			MidiNumbers = new int[6]
			{
				0,
				49,
				54,
				59,
				64,
				68
			},
			key = "C♯",
			suffix = "m11",
			Fingers = new int[6]
			{
				0,
				1,
				1,
				1,
				2,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 294,
			MidiNumbers = new int[6]
			{
				49,
				0,
				59,
				64,
				66,
				0
			},
			key = "C♯",
			suffix = "m11",
			Fingers = new int[6]
			{
				2,
				0,
				3,
				4,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 295,
			MidiNumbers = new int[6]
			{
				0,
				0,
				61,
				66,
				71,
				76
			},
			key = "C♯",
			suffix = "m11",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				1,
				2,
				2
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 296,
			MidiNumbers = new int[6]
			{
				0,
				49,
				0,
				59,
				64,
				66
			},
			key = "C♯",
			suffix = "m11",
			Fingers = new int[6]
			{
				0,
				2,
				0,
				3,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 297,
			MidiNumbers = new int[6]
			{
				0,
				49,
				0,
				59,
				64,
				70
			},
			key = "C♯",
			suffix = "m13",
			Fingers = new int[6]
			{
				0,
				1,
				0,
				2,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 298,
			MidiNumbers = new int[6]
			{
				49,
				56,
				59,
				64,
				70,
				73
			},
			key = "C♯",
			suffix = "m13",
			Fingers = new int[6]
			{
				1,
				3,
				1,
				1,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 299,
			MidiNumbers = new int[6]
			{
				49,
				0,
				59,
				64,
				70,
				75
			},
			key = "C♯",
			suffix = "m13",
			Fingers = new int[6]
			{
				2,
				0,
				3,
				3,
				4,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 300,
			MidiNumbers = new int[6]
			{
				0,
				49,
				56,
				59,
				64,
				70
			},
			key = "C♯",
			suffix = "m13",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				1,
				2,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 301,
			MidiNumbers = new int[6]
			{
				0,
				49,
				52,
				59,
				63,
				70
			},
			key = "C♯",
			suffix = "m13",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 302,
			MidiNumbers = new int[6]
			{
				0,
				49,
				52,
				60,
				63,
				0
			},
			key = "C♯",
			suffix = "mMaj9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				4,
				3,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 303,
			MidiNumbers = new int[6]
			{
				49,
				56,
				60,
				64,
				68,
				75
			},
			key = "C♯",
			suffix = "mMaj9",
			Fingers = new int[6]
			{
				1,
				3,
				2,
				1,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 304,
			MidiNumbers = new int[6]
			{
				0,
				49,
				53,
				60,
				63,
				0
			},
			key = "C♯",
			suffix = "Maj9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				4,
				3,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 305,
			MidiNumbers = new int[6]
			{
				0,
				0,
				61,
				65,
				72,
				75
			},
			key = "C♯",
			suffix = "Maj9",
			Fingers = new int[6]
			{
				0,
				0,
				2,
				1,
				4,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 306,
			MidiNumbers = new int[6]
			{
				0,
				49,
				51,
				56,
				60,
				65
			},
			key = "C♯",
			suffix = "Maj9",
			Fingers = new int[6]
			{
				0,
				3,
				1,
				1,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 307,
			MidiNumbers = new int[6]
			{
				49,
				53,
				60,
				63,
				68,
				72
			},
			key = "C♯",
			suffix = "Maj9",
			Fingers = new int[6]
			{
				2,
				1,
				4,
				1,
				3,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 308,
			MidiNumbers = new int[6]
			{
				49,
				0,
				60,
				63,
				68,
				0
			},
			key = "C♯",
			suffix = "Maj9",
			Fingers = new int[6]
			{
				2,
				0,
				4,
				1,
				3,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 309,
			MidiNumbers = new int[6]
			{
				0,
				49,
				0,
				60,
				65,
				70
			},
			key = "C♯",
			suffix = "Maj13",
			Fingers = new int[6]
			{
				0,
				1,
				0,
				2,
				4,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 310,
			MidiNumbers = new int[6]
			{
				49,
				53,
				58,
				63,
				68,
				72
			},
			key = "C♯",
			suffix = "Maj13",
			Fingers = new int[6]
			{
				2,
				1,
				1,
				1,
				3,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 311,
			MidiNumbers = new int[6]
			{
				49,
				0,
				60,
				65,
				70,
				75
			},
			key = "C♯",
			suffix = "Maj13",
			Fingers = new int[6]
			{
				1,
				0,
				2,
				3,
				4,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 312,
			MidiNumbers = new int[6]
			{
				0,
				49,
				55,
				60,
				65,
				68
			},
			key = "C♯",
			suffix = "Maj7♯11",
			Fingers = new int[6]
			{
				0,
				1,
				2,
				3,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 313,
			MidiNumbers = new int[6]
			{
				49,
				0,
				60,
				65,
				67,
				0
			},
			key = "C♯",
			suffix = "Maj7♯11",
			Fingers = new int[6]
			{
				2,
				0,
				3,
				4,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 314,
			MidiNumbers = new int[6]
			{
				0,
				0,
				61,
				67,
				72,
				77
			},
			key = "C♯",
			suffix = "Maj7♯11",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				2,
				3,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 315,
			MidiNumbers = new int[6]
			{
				0,
				49,
				53,
				59,
				63,
				67
			},
			key = "C♯",
			suffix = "9♭5",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 316,
			MidiNumbers = new int[6]
			{
				49,
				53,
				59,
				63,
				67,
				0
			},
			key = "C♯",
			suffix = "9♭5",
			Fingers = new int[6]
			{
				2,
				1,
				3,
				1,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 317,
			MidiNumbers = new int[6]
			{
				49,
				55,
				59,
				64,
				71,
				75
			},
			key = "C♯",
			suffix = "m9♭5",
			Fingers = new int[6]
			{
				1,
				2,
				1,
				1,
				4,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 318,
			MidiNumbers = new int[6]
			{
				0,
				49,
				53,
				59,
				62,
				69
			},
			key = "C♯",
			suffix = "+7♭9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 319,
			MidiNumbers = new int[6]
			{
				49,
				0,
				59,
				65,
				69,
				74
			},
			key = "C♯",
			suffix = "+7♭9",
			Fingers = new int[6]
			{
				1,
				0,
				2,
				3,
				3,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 320,
			MidiNumbers = new int[6]
			{
				0,
				49,
				53,
				59,
				64,
				69
			},
			key = "C♯",
			suffix = "+7♯9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				4,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 321,
			MidiNumbers = new int[6]
			{
				49,
				0,
				59,
				65,
				69,
				76
			},
			key = "C♯",
			suffix = "+7♯9",
			Fingers = new int[6]
			{
				1,
				0,
				2,
				3,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 322,
			MidiNumbers = new int[6]
			{
				0,
				0,
				50,
				57,
				62,
				66
			},
			key = "D",
			suffix = string.Empty,
			Fingers = new int[6]
			{
				0,
				0,
				0,
				1,
				3,
				2
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 323,
			MidiNumbers = new int[6]
			{
				0,
				50,
				57,
				62,
				66,
				69
			},
			key = "D",
			suffix = string.Empty,
			Fingers = new int[6]
			{
				0,
				1,
				3,
				3,
				3,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 324,
			MidiNumbers = new int[6]
			{
				50,
				57,
				62,
				66,
				69,
				74
			},
			key = "D",
			suffix = string.Empty,
			Fingers = new int[6]
			{
				1,
				3,
				4,
				2,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 325,
			MidiNumbers = new int[6]
			{
				0,
				50,
				54,
				57,
				62,
				66
			},
			key = "D",
			suffix = string.Empty,
			Fingers = new int[6]
			{
				0,
				4,
				3,
				1,
				2,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 326,
			MidiNumbers = new int[6]
			{
				50,
				54,
				57,
				62,
				66,
				74
			},
			key = "D",
			suffix = string.Empty,
			Fingers = new int[6]
			{
				3,
				2,
				1,
				1,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 327,
			MidiNumbers = new int[6]
			{
				0,
				0,
				62,
				69,
				74,
				78
			},
			key = "D",
			suffix = string.Empty,
			Fingers = new int[6]
			{
				0,
				0,
				1,
				2,
				4,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 328,
			MidiNumbers = new int[6]
			{
				0,
				0,
				50,
				57,
				62,
				65
			},
			key = "D",
			suffix = "m",
			Fingers = new int[6]
			{
				0,
				0,
				0,
				2,
				3,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 329,
			MidiNumbers = new int[6]
			{
				0,
				50,
				57,
				62,
				65,
				69
			},
			key = "D",
			suffix = "m",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				4,
				2,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 330,
			MidiNumbers = new int[6]
			{
				50,
				57,
				62,
				65,
				69,
				74
			},
			key = "D",
			suffix = "m",
			Fingers = new int[6]
			{
				1,
				2,
				3,
				1,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 331,
			MidiNumbers = new int[6]
			{
				0,
				0,
				62,
				65,
				69,
				74
			},
			key = "D",
			suffix = "m",
			Fingers = new int[6]
			{
				0,
				0,
				3,
				1,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 332,
			MidiNumbers = new int[6]
			{
				0,
				0,
				62,
				69,
				74,
				77
			},
			key = "D",
			suffix = "m",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				4,
				2
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 333,
			MidiNumbers = new int[6]
			{
				0,
				0,
				50,
				57,
				60,
				66
			},
			key = "D",
			suffix = "7",
			Fingers = new int[6]
			{
				0,
				0,
				0,
				2,
				1,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 334,
			MidiNumbers = new int[6]
			{
				0,
				50,
				57,
				60,
				66,
				69
			},
			key = "D",
			suffix = "7",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				1,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 335,
			MidiNumbers = new int[6]
			{
				50,
				57,
				60,
				66,
				69,
				74
			},
			key = "D",
			suffix = "7",
			Fingers = new int[6]
			{
				1,
				3,
				1,
				2,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 336,
			MidiNumbers = new int[6]
			{
				50,
				0,
				60,
				66,
				69,
				0
			},
			key = "D",
			suffix = "7",
			Fingers = new int[6]
			{
				1,
				0,
				2,
				4,
				3,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 337,
			MidiNumbers = new int[6]
			{
				0,
				50,
				54,
				60,
				62,
				0
			},
			key = "D",
			suffix = "7",
			Fingers = new int[6]
			{
				0,
				3,
				2,
				4,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 338,
			MidiNumbers = new int[6]
			{
				0,
				0,
				62,
				69,
				72,
				78
			},
			key = "D",
			suffix = "7",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				2,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 339,
			MidiNumbers = new int[6]
			{
				0,
				0,
				50,
				57,
				62,
				64
			},
			key = "D",
			suffix = "sus2",
			Fingers = new int[6]
			{
				0,
				0,
				0,
				2,
				3,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 340,
			MidiNumbers = new int[6]
			{
				0,
				50,
				57,
				62,
				64,
				69
			},
			key = "D",
			suffix = "sus2",
			Fingers = new int[6]
			{
				0,
				1,
				2,
				3,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 341,
			MidiNumbers = new int[6]
			{
				0,
				50,
				57,
				64,
				0,
				0
			},
			key = "D",
			suffix = "sus2",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				4,
				0,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 342,
			MidiNumbers = new int[6]
			{
				0,
				0,
				62,
				64,
				69,
				76
			},
			key = "D",
			suffix = "sus2",
			Fingers = new int[6]
			{
				0,
				0,
				3,
				1,
				2,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 343,
			MidiNumbers = new int[6]
			{
				0,
				0,
				62,
				69,
				74,
				76
			},
			key = "D",
			suffix = "sus2",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 344,
			MidiNumbers = new int[6]
			{
				0,
				0,
				50,
				57,
				62,
				67
			},
			key = "D",
			suffix = "sus4",
			Fingers = new int[6]
			{
				0,
				0,
				0,
				2,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 345,
			MidiNumbers = new int[6]
			{
				0,
				50,
				57,
				62,
				67,
				69
			},
			key = "D",
			suffix = "sus4",
			Fingers = new int[6]
			{
				0,
				1,
				2,
				3,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 346,
			MidiNumbers = new int[6]
			{
				50,
				57,
				62,
				67,
				69,
				74
			},
			key = "D",
			suffix = "sus4",
			Fingers = new int[6]
			{
				1,
				2,
				3,
				4,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 347,
			MidiNumbers = new int[6]
			{
				0,
				50,
				55,
				57,
				62,
				0
			},
			key = "D",
			suffix = "sus4",
			Fingers = new int[6]
			{
				0,
				3,
				4,
				1,
				2,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 348,
			MidiNumbers = new int[6]
			{
				0,
				0,
				62,
				69,
				74,
				79
			},
			key = "D",
			suffix = "sus4",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				4,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 349,
			MidiNumbers = new int[6]
			{
				0,
				0,
				62,
				67,
				69,
				74
			},
			key = "D",
			suffix = "sus4",
			Fingers = new int[6]
			{
				0,
				0,
				3,
				4,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 350,
			MidiNumbers = new int[6]
			{
				0,
				0,
				50,
				57,
				60,
				65
			},
			key = "D",
			suffix = "m7",
			Fingers = new int[6]
			{
				0,
				0,
				0,
				2,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 351,
			MidiNumbers = new int[6]
			{
				0,
				50,
				57,
				60,
				65,
				69
			},
			key = "D",
			suffix = "m7",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				1,
				2,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 352,
			MidiNumbers = new int[6]
			{
				50,
				57,
				60,
				65,
				69,
				74
			},
			key = "D",
			suffix = "m7",
			Fingers = new int[6]
			{
				1,
				2,
				1,
				1,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 353,
			MidiNumbers = new int[6]
			{
				50,
				0,
				60,
				65,
				69,
				0
			},
			key = "D",
			suffix = "m7",
			Fingers = new int[6]
			{
				1,
				0,
				2,
				3,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 354,
			MidiNumbers = new int[6]
			{
				50,
				57,
				62,
				65,
				72,
				74
			},
			key = "D",
			suffix = "m7",
			Fingers = new int[6]
			{
				1,
				2,
				3,
				1,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 355,
			MidiNumbers = new int[6]
			{
				0,
				50,
				53,
				60,
				65,
				0
			},
			key = "D",
			suffix = "m7",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 356,
			MidiNumbers = new int[6]
			{
				0,
				50,
				53,
				60,
				62,
				0
			},
			key = "D",
			suffix = "m7",
			Fingers = new int[6]
			{
				0,
				3,
				1,
				4,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 357,
			MidiNumbers = new int[6]
			{
				0,
				0,
				62,
				69,
				72,
				77
			},
			key = "D",
			suffix = "m7",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				4,
				2,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 358,
			MidiNumbers = new int[6]
			{
				0,
				0,
				50,
				57,
				61,
				66
			},
			key = "D",
			suffix = "Maj7",
			Fingers = new int[6]
			{
				0,
				0,
				0,
				1,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 359,
			MidiNumbers = new int[6]
			{
				0,
				50,
				57,
				61,
				66,
				69
			},
			key = "D",
			suffix = "Maj7",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				2,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 360,
			MidiNumbers = new int[6]
			{
				0,
				0,
				62,
				66,
				69,
				73
			},
			key = "D",
			suffix = "Maj7",
			Fingers = new int[6]
			{
				0,
				0,
				4,
				3,
				2,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 361,
			MidiNumbers = new int[6]
			{
				0,
				0,
				62,
				69,
				73,
				78
			},
			key = "D",
			suffix = "Maj7",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				3,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 362,
			MidiNumbers = new int[6]
			{
				0,
				50,
				54,
				57,
				61,
				0
			},
			key = "D",
			suffix = "Maj7",
			Fingers = new int[6]
			{
				0,
				3,
				2,
				1,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 363,
			MidiNumbers = new int[6]
			{
				50,
				0,
				61,
				66,
				69,
				0
			},
			key = "D",
			suffix = "Maj7",
			Fingers = new int[6]
			{
				1,
				0,
				3,
				4,
				2,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 364,
			MidiNumbers = new int[6]
			{
				50,
				57,
				61,
				66,
				69,
				74
			},
			key = "D",
			suffix = "Maj7",
			Fingers = new int[6]
			{
				1,
				4,
				2,
				3,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 365,
			MidiNumbers = new int[6]
			{
				0,
				50,
				57,
				62,
				0,
				0
			},
			key = "D",
			suffix = "5",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				4,
				0,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 366,
			MidiNumbers = new int[6]
			{
				0,
				0,
				50,
				57,
				62,
				0
			},
			key = "D",
			suffix = "5",
			Fingers = new int[6]
			{
				0,
				0,
				0,
				1,
				2,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 367,
			MidiNumbers = new int[6]
			{
				50,
				57,
				62,
				0,
				0,
				0
			},
			key = "D",
			suffix = "5",
			Fingers = new int[6]
			{
				1,
				3,
				4,
				0,
				0,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 368,
			MidiNumbers = new int[6]
			{
				0,
				0,
				0,
				62,
				69,
				74
			},
			key = "D",
			suffix = "5",
			Fingers = new int[6]
			{
				0,
				0,
				0,
				1,
				4,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 369,
			MidiNumbers = new int[6]
			{
				0,
				0,
				62,
				69,
				74,
				0
			},
			key = "D",
			suffix = "5",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 370,
			MidiNumbers = new int[6]
			{
				0,
				50,
				54,
				58,
				62,
				0
			},
			key = "D",
			suffix = "+",
			Fingers = new int[6]
			{
				0,
				4,
				3,
				1,
				2,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 371,
			MidiNumbers = new int[6]
			{
				0,
				0,
				62,
				66,
				70,
				74
			},
			key = "D",
			suffix = "+",
			Fingers = new int[6]
			{
				0,
				0,
				4,
				2,
				3,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 372,
			MidiNumbers = new int[6]
			{
				50,
				54,
				58,
				62,
				0,
				0
			},
			key = "D",
			suffix = "+",
			Fingers = new int[6]
			{
				4,
				3,
				2,
				1,
				0,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 373,
			MidiNumbers = new int[6]
			{
				50,
				58,
				62,
				66,
				0,
				0
			},
			key = "D",
			suffix = "+",
			Fingers = new int[6]
			{
				1,
				4,
				3,
				2,
				0,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 374,
			MidiNumbers = new int[6]
			{
				0,
				50,
				0,
				62,
				66,
				70
			},
			key = "D",
			suffix = "+",
			Fingers = new int[6]
			{
				0,
				1,
				0,
				3,
				4,
				2
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 375,
			MidiNumbers = new int[6]
			{
				0,
				50,
				56,
				62,
				65,
				0
			},
			key = "D",
			suffix = "°",
			Fingers = new int[6]
			{
				0,
				1,
				2,
				4,
				3,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 376,
			MidiNumbers = new int[6]
			{
				50,
				0,
				62,
				65,
				68,
				0
			},
			key = "D",
			suffix = "°",
			Fingers = new int[6]
			{
				2,
				0,
				4,
				3,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 377,
			MidiNumbers = new int[6]
			{
				50,
				56,
				62,
				65,
				0,
				0
			},
			key = "D",
			suffix = "°",
			Fingers = new int[6]
			{
				1,
				3,
				4,
				2,
				0,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 378,
			MidiNumbers = new int[6]
			{
				0,
				0,
				50,
				57,
				59,
				66
			},
			key = "D",
			suffix = "6",
			Fingers = new int[6]
			{
				0,
				0,
				0,
				2,
				0,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 379,
			MidiNumbers = new int[6]
			{
				0,
				50,
				57,
				62,
				66,
				71
			},
			key = "D",
			suffix = "6",
			Fingers = new int[6]
			{
				0,
				1,
				2,
				3,
				4,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 380,
			MidiNumbers = new int[6]
			{
				40,
				45,
				62,
				66,
				71,
				74
			},
			key = "D",
			suffix = "6",
			Fingers = new int[6]
			{
				0,
				0,
				3,
				2,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 381,
			MidiNumbers = new int[6]
			{
				0,
				0,
				62,
				69,
				71,
				78
			},
			key = "D",
			suffix = "6",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 382,
			MidiNumbers = new int[6]
			{
				50,
				0,
				59,
				66,
				69,
				0
			},
			key = "D",
			suffix = "6",
			Fingers = new int[6]
			{
				2,
				0,
				1,
				4,
				3,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 383,
			MidiNumbers = new int[6]
			{
				0,
				0,
				50,
				57,
				60,
				64
			},
			key = "D",
			suffix = "9",
			Fingers = new int[6]
			{
				0,
				0,
				0,
				2,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 384,
			MidiNumbers = new int[6]
			{
				0,
				50,
				54,
				60,
				64,
				0
			},
			key = "D",
			suffix = "9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 385,
			MidiNumbers = new int[6]
			{
				50,
				57,
				60,
				66,
				69,
				76
			},
			key = "D",
			suffix = "9",
			Fingers = new int[6]
			{
				1,
				3,
				1,
				2,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 386,
			MidiNumbers = new int[6]
			{
				50,
				0,
				60,
				64,
				69,
				74
			},
			key = "D",
			suffix = "9",
			Fingers = new int[6]
			{
				2,
				0,
				3,
				1,
				4,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 387,
			MidiNumbers = new int[6]
			{
				0,
				50,
				54,
				60,
				64,
				69
			},
			key = "D",
			suffix = "9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				3,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 388,
			MidiNumbers = new int[6]
			{
				50,
				0,
				60,
				64,
				66,
				0
			},
			key = "D",
			suffix = "9",
			Fingers = new int[6]
			{
				3,
				0,
				4,
				2,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 389,
			MidiNumbers = new int[6]
			{
				0,
				0,
				62,
				66,
				72,
				76
			},
			key = "D",
			suffix = "9",
			Fingers = new int[6]
			{
				0,
				0,
				2,
				1,
				4,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 390,
			MidiNumbers = new int[6]
			{
				50,
				54,
				60,
				64,
				69,
				0
			},
			key = "D",
			suffix = "9",
			Fingers = new int[6]
			{
				2,
				1,
				3,
				1,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 391,
			MidiNumbers = new int[6]
			{
				0,
				0,
				50,
				60,
				64,
				67
			},
			key = "D",
			suffix = "11",
			Fingers = new int[6]
			{
				0,
				0,
				0,
				3,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 392,
			MidiNumbers = new int[6]
			{
				0,
				50,
				54,
				60,
				62,
				67
			},
			key = "D",
			suffix = "11",
			Fingers = new int[6]
			{
				0,
				3,
				2,
				4,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 393,
			MidiNumbers = new int[6]
			{
				0,
				50,
				55,
				60,
				66,
				69
			},
			key = "D",
			suffix = "11",
			Fingers = new int[6]
			{
				0,
				1,
				1,
				1,
				3,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 394,
			MidiNumbers = new int[6]
			{
				0,
				0,
				62,
				67,
				72,
				78
			},
			key = "D",
			suffix = "11",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				1,
				2,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 395,
			MidiNumbers = new int[6]
			{
				50,
				0,
				60,
				64,
				66,
				71
			},
			key = "D",
			suffix = "13",
			Fingers = new int[6]
			{
				3,
				0,
				4,
				2,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 396,
			MidiNumbers = new int[6]
			{
				0,
				50,
				54,
				60,
				64,
				71
			},
			key = "D",
			suffix = "13",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 397,
			MidiNumbers = new int[6]
			{
				50,
				0,
				60,
				66,
				71,
				76
			},
			key = "D",
			suffix = "13",
			Fingers = new int[6]
			{
				1,
				0,
				1,
				1,
				2,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 398,
			MidiNumbers = new int[6]
			{
				50,
				57,
				60,
				66,
				71,
				74
			},
			key = "D",
			suffix = "13",
			Fingers = new int[6]
			{
				1,
				3,
				1,
				2,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 399,
			MidiNumbers = new int[6]
			{
				0,
				0,
				50,
				57,
				60,
				67
			},
			key = "D",
			suffix = "7sus4",
			Fingers = new int[6]
			{
				0,
				0,
				0,
				2,
				1,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 400,
			MidiNumbers = new int[6]
			{
				0,
				50,
				55,
				60,
				62,
				67
			},
			key = "D",
			suffix = "7sus4",
			Fingers = new int[6]
			{
				0,
				2,
				3,
				4,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 401,
			MidiNumbers = new int[6]
			{
				0,
				50,
				57,
				60,
				67,
				69
			},
			key = "D",
			suffix = "7sus4",
			Fingers = new int[6]
			{
				0,
				1,
				2,
				1,
				3,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 402,
			MidiNumbers = new int[6]
			{
				50,
				57,
				60,
				67,
				69,
				74
			},
			key = "D",
			suffix = "7sus4",
			Fingers = new int[6]
			{
				1,
				2,
				1,
				3,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 403,
			MidiNumbers = new int[6]
			{
				0,
				0,
				62,
				69,
				72,
				79
			},
			key = "D",
			suffix = "7sus4",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				2,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 404,
			MidiNumbers = new int[6]
			{
				0,
				50,
				55,
				60,
				64,
				69
			},
			key = "D",
			suffix = "9sus4",
			Fingers = new int[6]
			{
				0,
				1,
				1,
				1,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 405,
			MidiNumbers = new int[6]
			{
				50,
				0,
				60,
				64,
				67,
				0
			},
			key = "D",
			suffix = "9sus4",
			Fingers = new int[6]
			{
				3,
				0,
				4,
				2,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 406,
			MidiNumbers = new int[6]
			{
				50,
				0,
				60,
				64,
				67,
				72
			},
			key = "D",
			suffix = "9sus4",
			Fingers = new int[6]
			{
				3,
				0,
				4,
				2,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 407,
			MidiNumbers = new int[6]
			{
				0,
				50,
				55,
				60,
				64,
				71
			},
			key = "D",
			suffix = "13sus4",
			Fingers = new int[6]
			{
				0,
				1,
				1,
				1,
				1,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 408,
			MidiNumbers = new int[6]
			{
				50,
				0,
				60,
				67,
				71,
				76
			},
			key = "D",
			suffix = "13sus4",
			Fingers = new int[6]
			{
				1,
				0,
				2,
				4,
				4,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 409,
			MidiNumbers = new int[6]
			{
				0,
				0,
				50,
				56,
				59,
				65
			},
			key = "D",
			suffix = "°7",
			Fingers = new int[6]
			{
				0,
				0,
				0,
				1,
				0,
				2
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 410,
			MidiNumbers = new int[6]
			{
				0,
				50,
				56,
				59,
				65,
				0
			},
			key = "D",
			suffix = "°7",
			Fingers = new int[6]
			{
				0,
				2,
				3,
				1,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 411,
			MidiNumbers = new int[6]
			{
				50,
				0,
				59,
				65,
				68,
				0
			},
			key = "D",
			suffix = "°7",
			Fingers = new int[6]
			{
				3,
				0,
				1,
				4,
				2,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 412,
			MidiNumbers = new int[6]
			{
				0,
				0,
				62,
				68,
				71,
				77
			},
			key = "D",
			suffix = "°7",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				2,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 413,
			MidiNumbers = new int[6]
			{
				0,
				50,
				58,
				60,
				66,
				70
			},
			key = "D",
			suffix = "+7",
			Fingers = new int[6]
			{
				0,
				1,
				4,
				1,
				3,
				2
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 414,
			MidiNumbers = new int[6]
			{
				50,
				0,
				60,
				66,
				70,
				0
			},
			key = "D",
			suffix = "+7",
			Fingers = new int[6]
			{
				1,
				0,
				2,
				3,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 415,
			MidiNumbers = new int[6]
			{
				0,
				0,
				62,
				70,
				72,
				78
			},
			key = "D",
			suffix = "+7",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				4,
				2,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 416,
			MidiNumbers = new int[6]
			{
				0,
				50,
				0,
				60,
				66,
				70
			},
			key = "D",
			suffix = "+7",
			Fingers = new int[6]
			{
				0,
				1,
				0,
				2,
				4,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 417,
			MidiNumbers = new int[6]
			{
				0,
				50,
				56,
				60,
				66,
				0
			},
			key = "D",
			suffix = "7♭5",
			Fingers = new int[6]
			{
				0,
				1,
				2,
				1,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 418,
			MidiNumbers = new int[6]
			{
				50,
				0,
				60,
				66,
				68,
				0
			},
			key = "D",
			suffix = "7♭5",
			Fingers = new int[6]
			{
				2,
				0,
				3,
				4,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 419,
			MidiNumbers = new int[6]
			{
				0,
				0,
				62,
				68,
				72,
				78
			},
			key = "D",
			suffix = "7♭5",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				2,
				2,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 420,
			MidiNumbers = new int[6]
			{
				0,
				50,
				56,
				60,
				66,
				72
			},
			key = "D",
			suffix = "7♭5",
			Fingers = new int[6]
			{
				0,
				1,
				2,
				1,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 421,
			MidiNumbers = new int[6]
			{
				0,
				50,
				54,
				60,
				65,
				0
			},
			key = "D",
			suffix = "7♯9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 422,
			MidiNumbers = new int[6]
			{
				50,
				57,
				60,
				66,
				69,
				77
			},
			key = "D",
			suffix = "7♯9",
			Fingers = new int[6]
			{
				1,
				3,
				1,
				2,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 423,
			MidiNumbers = new int[6]
			{
				0,
				0,
				62,
				66,
				72,
				77
			},
			key = "D",
			suffix = "7♯9",
			Fingers = new int[6]
			{
				0,
				0,
				2,
				1,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 424,
			MidiNumbers = new int[6]
			{
				0,
				50,
				54,
				60,
				65,
				68
			},
			key = "D",
			suffix = "7♭5♯9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 425,
			MidiNumbers = new int[6]
			{
				50,
				56,
				60,
				66,
				69,
				74
			},
			key = "D",
			suffix = "7♯11",
			Fingers = new int[6]
			{
				1,
				2,
				1,
				3,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 426,
			MidiNumbers = new int[6]
			{
				0,
				50,
				54,
				60,
				63,
				0
			},
			key = "D",
			suffix = "7♭9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 427,
			MidiNumbers = new int[6]
			{
				50,
				57,
				60,
				66,
				69,
				75
			},
			key = "D",
			suffix = "7♭9",
			Fingers = new int[6]
			{
				1,
				4,
				1,
				2,
				1,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 428,
			MidiNumbers = new int[6]
			{
				0,
				50,
				54,
				60,
				63,
				69
			},
			key = "D",
			suffix = "7♭9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 429,
			MidiNumbers = new int[6]
			{
				0,
				0,
				62,
				66,
				72,
				75
			},
			key = "D",
			suffix = "7♭9",
			Fingers = new int[6]
			{
				0,
				0,
				2,
				1,
				3,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 430,
			MidiNumbers = new int[6]
			{
				0,
				50,
				54,
				60,
				64,
				70
			},
			key = "D",
			suffix = "+9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 431,
			MidiNumbers = new int[6]
			{
				50,
				0,
				60,
				66,
				70,
				76
			},
			key = "D",
			suffix = "+9",
			Fingers = new int[6]
			{
				1,
				0,
				2,
				3,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 432,
			MidiNumbers = new int[6]
			{
				0,
				50,
				57,
				61,
				65,
				69
			},
			key = "D",
			suffix = "mMaj7",
			Fingers = new int[6]
			{
				0,
				1,
				4,
				2,
				3,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 433,
			MidiNumbers = new int[6]
			{
				50,
				0,
				61,
				65,
				69,
				0
			},
			key = "D",
			suffix = "mMaj7",
			Fingers = new int[6]
			{
				1,
				0,
				4,
				2,
				3,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 434,
			MidiNumbers = new int[6]
			{
				50,
				57,
				61,
				65,
				69,
				74
			},
			key = "D",
			suffix = "mMaj7",
			Fingers = new int[6]
			{
				1,
				3,
				2,
				1,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 435,
			MidiNumbers = new int[6]
			{
				0,
				0,
				62,
				69,
				73,
				77
			},
			key = "D",
			suffix = "mMaj7",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				4,
				2
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 436,
			MidiNumbers = new int[6]
			{
				0,
				0,
				62,
				65,
				69,
				73
			},
			key = "D",
			suffix = "mMaj7",
			Fingers = new int[6]
			{
				0,
				0,
				4,
				2,
				3,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 437,
			MidiNumbers = new int[6]
			{
				0,
				50,
				54,
				57,
				62,
				64
			},
			key = "D",
			suffix = "add9",
			Fingers = new int[6]
			{
				0,
				4,
				3,
				1,
				2,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 438,
			MidiNumbers = new int[6]
			{
				0,
				0,
				60,
				66,
				69,
				76
			},
			key = "D",
			suffix = "add9",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				2,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 439,
			MidiNumbers = new int[6]
			{
				0,
				0,
				62,
				66,
				69,
				76
			},
			key = "D",
			suffix = "add9",
			Fingers = new int[6]
			{
				0,
				0,
				3,
				2,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 440,
			MidiNumbers = new int[6]
			{
				0,
				50,
				57,
				64,
				66,
				69
			},
			key = "D",
			suffix = "add9",
			Fingers = new int[6]
			{
				0,
				1,
				2,
				4,
				3,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 441,
			MidiNumbers = new int[6]
			{
				50,
				0,
				0,
				66,
				69,
				76
			},
			key = "D",
			suffix = "add9",
			Fingers = new int[6]
			{
				1,
				0,
				0,
				3,
				2,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 442,
			MidiNumbers = new int[6]
			{
				0,
				0,
				62,
				65,
				69,
				76
			},
			key = "D",
			suffix = "madd9",
			Fingers = new int[6]
			{
				0,
				0,
				3,
				1,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 443,
			MidiNumbers = new int[6]
			{
				0,
				0,
				50,
				57,
				59,
				65
			},
			key = "D",
			suffix = "m6",
			Fingers = new int[6]
			{
				0,
				0,
				0,
				2,
				0,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 444,
			MidiNumbers = new int[6]
			{
				50,
				57,
				62,
				65,
				71,
				74
			},
			key = "D",
			suffix = "m6",
			Fingers = new int[6]
			{
				1,
				2,
				3,
				1,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 445,
			MidiNumbers = new int[6]
			{
				0,
				50,
				53,
				59,
				62,
				0
			},
			key = "D",
			suffix = "m6",
			Fingers = new int[6]
			{
				0,
				4,
				1,
				2,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 446,
			MidiNumbers = new int[6]
			{
				0,
				0,
				62,
				69,
				71,
				77
			},
			key = "D",
			suffix = "m6",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				1,
				2
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 447,
			MidiNumbers = new int[6]
			{
				0,
				50,
				57,
				59,
				65,
				0
			},
			key = "D",
			suffix = "m6",
			Fingers = new int[6]
			{
				0,
				2,
				4,
				1,
				3,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 448,
			MidiNumbers = new int[6]
			{
				0,
				50,
				53,
				59,
				62,
				69
			},
			key = "D",
			suffix = "m6",
			Fingers = new int[6]
			{
				0,
				3,
				1,
				2,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 449,
			MidiNumbers = new int[6]
			{
				50,
				0,
				59,
				65,
				69,
				0
			},
			key = "D",
			suffix = "m6",
			Fingers = new int[6]
			{
				2,
				0,
				1,
				3,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 450,
			MidiNumbers = new int[6]
			{
				0,
				50,
				0,
				59,
				65,
				69
			},
			key = "D",
			suffix = "m6",
			Fingers = new int[6]
			{
				0,
				2,
				0,
				1,
				4,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 451,
			MidiNumbers = new int[6]
			{
				0,
				50,
				53,
				58,
				62,
				69
			},
			key = "D",
			suffix = "m♭6",
			Fingers = new int[6]
			{
				0,
				3,
				1,
				1,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 452,
			MidiNumbers = new int[6]
			{
				50,
				0,
				58,
				65,
				69,
				0
			},
			key = "D",
			suffix = "m♭6",
			Fingers = new int[6]
			{
				2,
				0,
				1,
				3,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 453,
			MidiNumbers = new int[6]
			{
				0,
				0,
				50,
				56,
				60,
				65
			},
			key = "D",
			suffix = "m7♭5",
			Fingers = new int[6]
			{
				0,
				0,
				0,
				1,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 454,
			MidiNumbers = new int[6]
			{
				0,
				50,
				56,
				60,
				65,
				0
			},
			key = "D",
			suffix = "m7♭5",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				2,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 455,
			MidiNumbers = new int[6]
			{
				50,
				0,
				60,
				65,
				68,
				0
			},
			key = "D",
			suffix = "m7♭5",
			Fingers = new int[6]
			{
				2,
				0,
				3,
				4,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 456,
			MidiNumbers = new int[6]
			{
				50,
				56,
				62,
				68,
				72,
				77
			},
			key = "D",
			suffix = "m7♭5",
			Fingers = new int[6]
			{
				1,
				2,
				3,
				4,
				4,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 457,
			MidiNumbers = new int[6]
			{
				0,
				0,
				62,
				68,
				72,
				77
			},
			key = "D",
			suffix = "m7♭5",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				2,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 458,
			MidiNumbers = new int[6]
			{
				0,
				50,
				0,
				60,
				65,
				68
			},
			key = "D",
			suffix = "m7♭5",
			Fingers = new int[6]
			{
				0,
				2,
				0,
				3,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 459,
			MidiNumbers = new int[6]
			{
				50,
				56,
				60,
				65,
				72,
				74
			},
			key = "D",
			suffix = "m7♭5",
			Fingers = new int[6]
			{
				1,
				2,
				1,
				1,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 460,
			MidiNumbers = new int[6]
			{
				50,
				57,
				60,
				65,
				69,
				76
			},
			key = "D",
			suffix = "m9",
			Fingers = new int[6]
			{
				1,
				2,
				1,
				1,
				1,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 461,
			MidiNumbers = new int[6]
			{
				0,
				50,
				53,
				60,
				64,
				0
			},
			key = "D",
			suffix = "m9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 462,
			MidiNumbers = new int[6]
			{
				0,
				50,
				53,
				60,
				64,
				69
			},
			key = "D",
			suffix = "m9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				3,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 463,
			MidiNumbers = new int[6]
			{
				0,
				0,
				62,
				65,
				72,
				76
			},
			key = "D",
			suffix = "m9",
			Fingers = new int[6]
			{
				0,
				0,
				2,
				1,
				4,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 464,
			MidiNumbers = new int[6]
			{
				50,
				0,
				60,
				65,
				69,
				76
			},
			key = "D",
			suffix = "m9",
			Fingers = new int[6]
			{
				2,
				0,
				3,
				3,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 465,
			MidiNumbers = new int[6]
			{
				0,
				50,
				53,
				60,
				62,
				67
			},
			key = "D",
			suffix = "m11",
			Fingers = new int[6]
			{
				0,
				3,
				1,
				4,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 466,
			MidiNumbers = new int[6]
			{
				0,
				50,
				55,
				60,
				65,
				69
			},
			key = "D",
			suffix = "m11",
			Fingers = new int[6]
			{
				0,
				1,
				1,
				1,
				2,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 467,
			MidiNumbers = new int[6]
			{
				50,
				0,
				60,
				65,
				67,
				0
			},
			key = "D",
			suffix = "m11",
			Fingers = new int[6]
			{
				2,
				0,
				3,
				4,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 468,
			MidiNumbers = new int[6]
			{
				0,
				0,
				62,
				67,
				72,
				77
			},
			key = "D",
			suffix = "m11",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				1,
				2,
				2
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 469,
			MidiNumbers = new int[6]
			{
				0,
				50,
				0,
				60,
				65,
				67
			},
			key = "D",
			suffix = "m11",
			Fingers = new int[6]
			{
				0,
				2,
				0,
				3,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 470,
			MidiNumbers = new int[6]
			{
				0,
				50,
				0,
				60,
				65,
				71
			},
			key = "D",
			suffix = "m13",
			Fingers = new int[6]
			{
				0,
				1,
				0,
				2,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 471,
			MidiNumbers = new int[6]
			{
				50,
				57,
				60,
				65,
				71,
				74
			},
			key = "D",
			suffix = "m13",
			Fingers = new int[6]
			{
				1,
				3,
				1,
				1,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 472,
			MidiNumbers = new int[6]
			{
				50,
				0,
				60,
				65,
				71,
				76
			},
			key = "D",
			suffix = "m13",
			Fingers = new int[6]
			{
				2,
				0,
				3,
				3,
				4,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 473,
			MidiNumbers = new int[6]
			{
				0,
				50,
				57,
				60,
				65,
				71
			},
			key = "D",
			suffix = "m13",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				1,
				2,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 474,
			MidiNumbers = new int[6]
			{
				0,
				50,
				53,
				60,
				64,
				71
			},
			key = "D",
			suffix = "m13",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 475,
			MidiNumbers = new int[6]
			{
				0,
				50,
				53,
				61,
				64,
				0
			},
			key = "D",
			suffix = "mMaj9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				4,
				3,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 476,
			MidiNumbers = new int[6]
			{
				50,
				57,
				61,
				65,
				69,
				76
			},
			key = "D",
			suffix = "mMaj9",
			Fingers = new int[6]
			{
				1,
				3,
				2,
				1,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 477,
			MidiNumbers = new int[6]
			{
				0,
				50,
				54,
				61,
				64,
				0
			},
			key = "D",
			suffix = "Maj9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				4,
				3,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 478,
			MidiNumbers = new int[6]
			{
				0,
				0,
				62,
				66,
				73,
				76
			},
			key = "D",
			suffix = "Maj9",
			Fingers = new int[6]
			{
				0,
				0,
				2,
				1,
				4,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 479,
			MidiNumbers = new int[6]
			{
				0,
				50,
				52,
				57,
				61,
				66
			},
			key = "D",
			suffix = "Maj9",
			Fingers = new int[6]
			{
				0,
				3,
				1,
				1,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 480,
			MidiNumbers = new int[6]
			{
				50,
				54,
				61,
				64,
				69,
				73
			},
			key = "D",
			suffix = "Maj9",
			Fingers = new int[6]
			{
				2,
				1,
				4,
				1,
				3,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 481,
			MidiNumbers = new int[6]
			{
				50,
				0,
				61,
				64,
				69,
				0
			},
			key = "D",
			suffix = "Maj9",
			Fingers = new int[6]
			{
				2,
				0,
				4,
				1,
				3,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 482,
			MidiNumbers = new int[6]
			{
				0,
				50,
				0,
				61,
				66,
				71
			},
			key = "D",
			suffix = "Maj13",
			Fingers = new int[6]
			{
				0,
				1,
				0,
				2,
				4,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 483,
			MidiNumbers = new int[6]
			{
				50,
				54,
				59,
				64,
				69,
				73
			},
			key = "D",
			suffix = "Maj13",
			Fingers = new int[6]
			{
				2,
				1,
				1,
				1,
				3,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 484,
			MidiNumbers = new int[6]
			{
				50,
				0,
				61,
				66,
				71,
				76
			},
			key = "D",
			suffix = "Maj13",
			Fingers = new int[6]
			{
				1,
				0,
				2,
				3,
				4,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 485,
			MidiNumbers = new int[6]
			{
				0,
				50,
				56,
				61,
				66,
				69
			},
			key = "D",
			suffix = "Maj7♯11",
			Fingers = new int[6]
			{
				0,
				1,
				2,
				3,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 486,
			MidiNumbers = new int[6]
			{
				50,
				0,
				61,
				66,
				68,
				0
			},
			key = "D",
			suffix = "Maj7♯11",
			Fingers = new int[6]
			{
				2,
				0,
				3,
				4,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 487,
			MidiNumbers = new int[6]
			{
				0,
				0,
				62,
				68,
				73,
				78
			},
			key = "D",
			suffix = "Maj7♯11",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				2,
				3,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 488,
			MidiNumbers = new int[6]
			{
				0,
				50,
				54,
				60,
				64,
				68
			},
			key = "D",
			suffix = "9♭5",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 489,
			MidiNumbers = new int[6]
			{
				50,
				54,
				60,
				64,
				68,
				0
			},
			key = "D",
			suffix = "9♭5",
			Fingers = new int[6]
			{
				2,
				1,
				3,
				1,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 490,
			MidiNumbers = new int[6]
			{
				50,
				56,
				60,
				65,
				72,
				76
			},
			key = "D",
			suffix = "m9♭5",
			Fingers = new int[6]
			{
				1,
				2,
				1,
				1,
				4,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 491,
			MidiNumbers = new int[6]
			{
				0,
				50,
				54,
				60,
				63,
				70
			},
			key = "D",
			suffix = "+7♭9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 492,
			MidiNumbers = new int[6]
			{
				50,
				0,
				60,
				66,
				70,
				75
			},
			key = "D",
			suffix = "+7♭9",
			Fingers = new int[6]
			{
				1,
				0,
				2,
				3,
				3,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 493,
			MidiNumbers = new int[6]
			{
				0,
				50,
				54,
				60,
				65,
				70
			},
			key = "D",
			suffix = "+7♯9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				4,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 494,
			MidiNumbers = new int[6]
			{
				50,
				0,
				60,
				66,
				70,
				77
			},
			key = "D",
			suffix = "+7♯9",
			Fingers = new int[6]
			{
				1,
				0,
				2,
				3,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 495,
			MidiNumbers = new int[6]
			{
				0,
				51,
				58,
				63,
				67,
				70
			},
			key = "E♭",
			suffix = string.Empty,
			Fingers = new int[6]
			{
				0,
				1,
				3,
				3,
				3,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 496,
			MidiNumbers = new int[6]
			{
				0,
				51,
				55,
				58,
				63,
				67
			},
			key = "E♭",
			suffix = string.Empty,
			Fingers = new int[6]
			{
				0,
				4,
				3,
				1,
				2,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 497,
			MidiNumbers = new int[6]
			{
				0,
				0,
				51,
				58,
				63,
				67
			},
			key = "E♭",
			suffix = string.Empty,
			Fingers = new int[6]
			{
				0,
				0,
				1,
				2,
				4,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 498,
			MidiNumbers = new int[6]
			{
				51,
				58,
				63,
				67,
				70,
				75
			},
			key = "E♭",
			suffix = string.Empty,
			Fingers = new int[6]
			{
				1,
				3,
				4,
				2,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 499,
			MidiNumbers = new int[6]
			{
				51,
				55,
				58,
				63,
				67,
				75
			},
			key = "E♭",
			suffix = string.Empty,
			Fingers = new int[6]
			{
				3,
				2,
				1,
				1,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 500,
			MidiNumbers = new int[6]
			{
				0,
				51,
				58,
				63,
				66,
				70
			},
			key = "E♭",
			suffix = "m",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				4,
				2,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 501,
			MidiNumbers = new int[6]
			{
				0,
				0,
				51,
				58,
				63,
				66
			},
			key = "E♭",
			suffix = "m",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				4,
				2
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 502,
			MidiNumbers = new int[6]
			{
				51,
				58,
				63,
				66,
				70,
				75
			},
			key = "E♭",
			suffix = "m",
			Fingers = new int[6]
			{
				1,
				3,
				4,
				1,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 503,
			MidiNumbers = new int[6]
			{
				0,
				0,
				63,
				66,
				70,
				75
			},
			key = "E♭",
			suffix = "m",
			Fingers = new int[6]
			{
				0,
				0,
				3,
				1,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 504,
			MidiNumbers = new int[6]
			{
				0,
				0,
				51,
				58,
				61,
				67
			},
			key = "E♭",
			suffix = "7",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				2,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 505,
			MidiNumbers = new int[6]
			{
				0,
				51,
				58,
				61,
				67,
				70
			},
			key = "E♭",
			suffix = "7",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				1,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 506,
			MidiNumbers = new int[6]
			{
				51,
				0,
				61,
				67,
				70,
				0
			},
			key = "E♭",
			suffix = "7",
			Fingers = new int[6]
			{
				1,
				0,
				2,
				4,
				3,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 507,
			MidiNumbers = new int[6]
			{
				0,
				51,
				55,
				61,
				63,
				0
			},
			key = "E♭",
			suffix = "7",
			Fingers = new int[6]
			{
				0,
				3,
				2,
				4,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 508,
			MidiNumbers = new int[6]
			{
				51,
				58,
				61,
				67,
				70,
				75
			},
			key = "E♭",
			suffix = "7",
			Fingers = new int[6]
			{
				1,
				3,
				1,
				2,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 509,
			MidiNumbers = new int[6]
			{
				0,
				0,
				51,
				58,
				63,
				65
			},
			key = "E♭",
			suffix = "sus2",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 510,
			MidiNumbers = new int[6]
			{
				0,
				51,
				58,
				63,
				65,
				70
			},
			key = "E♭",
			suffix = "sus2",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				4,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 511,
			MidiNumbers = new int[6]
			{
				0,
				51,
				58,
				65,
				0,
				0
			},
			key = "E♭",
			suffix = "sus2",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				4,
				0,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 512,
			MidiNumbers = new int[6]
			{
				0,
				0,
				63,
				65,
				70,
				77
			},
			key = "E♭",
			suffix = "sus2",
			Fingers = new int[6]
			{
				0,
				0,
				3,
				1,
				2,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 513,
			MidiNumbers = new int[6]
			{
				0,
				51,
				56,
				58,
				63,
				0
			},
			key = "E♭",
			suffix = "sus4",
			Fingers = new int[6]
			{
				0,
				3,
				4,
				1,
				2,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 514,
			MidiNumbers = new int[6]
			{
				0,
				51,
				58,
				63,
				68,
				70
			},
			key = "E♭",
			suffix = "sus4",
			Fingers = new int[6]
			{
				0,
				1,
				2,
				3,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 515,
			MidiNumbers = new int[6]
			{
				51,
				58,
				63,
				68,
				70,
				75
			},
			key = "E♭",
			suffix = "sus4",
			Fingers = new int[6]
			{
				1,
				2,
				3,
				4,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 516,
			MidiNumbers = new int[6]
			{
				0,
				0,
				51,
				58,
				63,
				68
			},
			key = "E♭",
			suffix = "sus4",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				4,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 517,
			MidiNumbers = new int[6]
			{
				0,
				0,
				63,
				68,
				70,
				75
			},
			key = "E♭",
			suffix = "sus4",
			Fingers = new int[6]
			{
				0,
				0,
				3,
				4,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 518,
			MidiNumbers = new int[6]
			{
				0,
				0,
				51,
				58,
				61,
				66
			},
			key = "E♭",
			suffix = "m7",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				4,
				2,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 519,
			MidiNumbers = new int[6]
			{
				0,
				51,
				58,
				61,
				66,
				70
			},
			key = "E♭",
			suffix = "m7",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				1,
				2,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 520,
			MidiNumbers = new int[6]
			{
				51,
				0,
				61,
				66,
				70,
				0
			},
			key = "E♭",
			suffix = "m7",
			Fingers = new int[6]
			{
				1,
				0,
				2,
				3,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 521,
			MidiNumbers = new int[6]
			{
				51,
				58,
				61,
				66,
				73,
				75
			},
			key = "E♭",
			suffix = "m7",
			Fingers = new int[6]
			{
				1,
				3,
				1,
				1,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 522,
			MidiNumbers = new int[6]
			{
				0,
				51,
				54,
				61,
				66,
				0
			},
			key = "E♭",
			suffix = "m7",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 523,
			MidiNumbers = new int[6]
			{
				51,
				58,
				63,
				66,
				73,
				75
			},
			key = "E♭",
			suffix = "m7",
			Fingers = new int[6]
			{
				1,
				2,
				3,
				1,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 524,
			MidiNumbers = new int[6]
			{
				51,
				58,
				61,
				66,
				70,
				75
			},
			key = "E♭",
			suffix = "m7",
			Fingers = new int[6]
			{
				1,
				3,
				1,
				1,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 525,
			MidiNumbers = new int[6]
			{
				0,
				51,
				54,
				61,
				63,
				0
			},
			key = "E♭",
			suffix = "m7",
			Fingers = new int[6]
			{
				0,
				3,
				1,
				4,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 526,
			MidiNumbers = new int[6]
			{
				0,
				0,
				51,
				58,
				62,
				67
			},
			key = "E♭",
			suffix = "Maj7",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				3,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 527,
			MidiNumbers = new int[6]
			{
				0,
				51,
				58,
				62,
				67,
				70
			},
			key = "E♭",
			suffix = "Maj7",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				2,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 528,
			MidiNumbers = new int[6]
			{
				0,
				0,
				63,
				70,
				74,
				79
			},
			key = "E♭",
			suffix = "Maj7",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				3,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 529,
			MidiNumbers = new int[6]
			{
				0,
				51,
				55,
				58,
				62,
				0
			},
			key = "E♭",
			suffix = "Maj7",
			Fingers = new int[6]
			{
				0,
				4,
				3,
				1,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 530,
			MidiNumbers = new int[6]
			{
				0,
				0,
				63,
				67,
				70,
				74
			},
			key = "E♭",
			suffix = "Maj7",
			Fingers = new int[6]
			{
				0,
				0,
				4,
				3,
				2,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 531,
			MidiNumbers = new int[6]
			{
				51,
				0,
				62,
				67,
				70,
				0
			},
			key = "E♭",
			suffix = "Maj7",
			Fingers = new int[6]
			{
				1,
				0,
				3,
				4,
				2,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 532,
			MidiNumbers = new int[6]
			{
				51,
				58,
				62,
				67,
				70,
				75
			},
			key = "E♭",
			suffix = "Maj7",
			Fingers = new int[6]
			{
				1,
				4,
				2,
				3,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 533,
			MidiNumbers = new int[6]
			{
				0,
				51,
				58,
				63,
				0,
				0
			},
			key = "E♭",
			suffix = "5",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				4,
				0,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 534,
			MidiNumbers = new int[6]
			{
				51,
				58,
				63,
				0,
				0,
				0
			},
			key = "E♭",
			suffix = "5",
			Fingers = new int[6]
			{
				1,
				3,
				4,
				0,
				0,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 535,
			MidiNumbers = new int[6]
			{
				0,
				0,
				0,
				63,
				70,
				75
			},
			key = "E♭",
			suffix = "5",
			Fingers = new int[6]
			{
				0,
				0,
				0,
				1,
				4,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 536,
			MidiNumbers = new int[6]
			{
				0,
				0,
				51,
				58,
				63,
				0
			},
			key = "E♭",
			suffix = "5",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 537,
			MidiNumbers = new int[6]
			{
				0,
				51,
				55,
				59,
				63,
				0
			},
			key = "E♭",
			suffix = "+",
			Fingers = new int[6]
			{
				0,
				4,
				3,
				1,
				2,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 538,
			MidiNumbers = new int[6]
			{
				0,
				0,
				63,
				67,
				71,
				75
			},
			key = "E♭",
			suffix = "+",
			Fingers = new int[6]
			{
				0,
				0,
				4,
				2,
				3,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 539,
			MidiNumbers = new int[6]
			{
				51,
				55,
				59,
				63,
				0,
				0
			},
			key = "E♭",
			suffix = "+",
			Fingers = new int[6]
			{
				4,
				3,
				2,
				1,
				0,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 540,
			MidiNumbers = new int[6]
			{
				51,
				59,
				63,
				67,
				0,
				0
			},
			key = "E♭",
			suffix = "+",
			Fingers = new int[6]
			{
				1,
				4,
				3,
				2,
				0,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 541,
			MidiNumbers = new int[6]
			{
				0,
				51,
				0,
				63,
				67,
				71
			},
			key = "E♭",
			suffix = "+",
			Fingers = new int[6]
			{
				0,
				1,
				0,
				3,
				4,
				2
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 542,
			MidiNumbers = new int[6]
			{
				0,
				51,
				57,
				63,
				66,
				0
			},
			key = "E♭",
			suffix = "°",
			Fingers = new int[6]
			{
				0,
				1,
				2,
				4,
				3,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 543,
			MidiNumbers = new int[6]
			{
				51,
				0,
				63,
				66,
				69,
				0
			},
			key = "E♭",
			suffix = "°",
			Fingers = new int[6]
			{
				2,
				0,
				4,
				3,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 544,
			MidiNumbers = new int[6]
			{
				51,
				57,
				63,
				66,
				0,
				0
			},
			key = "E♭",
			suffix = "°",
			Fingers = new int[6]
			{
				1,
				3,
				4,
				2,
				0,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 545,
			MidiNumbers = new int[6]
			{
				0,
				0,
				51,
				58,
				60,
				67
			},
			key = "E♭",
			suffix = "6",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 546,
			MidiNumbers = new int[6]
			{
				0,
				51,
				58,
				63,
				67,
				72
			},
			key = "E♭",
			suffix = "6",
			Fingers = new int[6]
			{
				0,
				1,
				2,
				3,
				4,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 547,
			MidiNumbers = new int[6]
			{
				0,
				0,
				63,
				70,
				72,
				79
			},
			key = "E♭",
			suffix = "6",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 548,
			MidiNumbers = new int[6]
			{
				51,
				0,
				60,
				67,
				70,
				0
			},
			key = "E♭",
			suffix = "6",
			Fingers = new int[6]
			{
				2,
				0,
				1,
				4,
				3,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 549,
			MidiNumbers = new int[6]
			{
				0,
				51,
				55,
				61,
				65,
				0
			},
			key = "E♭",
			suffix = "9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 550,
			MidiNumbers = new int[6]
			{
				51,
				0,
				61,
				65,
				70,
				75
			},
			key = "E♭",
			suffix = "9",
			Fingers = new int[6]
			{
				2,
				0,
				3,
				1,
				4,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 551,
			MidiNumbers = new int[6]
			{
				51,
				58,
				61,
				67,
				70,
				77
			},
			key = "E♭",
			suffix = "9",
			Fingers = new int[6]
			{
				1,
				3,
				1,
				2,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 552,
			MidiNumbers = new int[6]
			{
				0,
				51,
				55,
				61,
				65,
				70
			},
			key = "E♭",
			suffix = "9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				3,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 553,
			MidiNumbers = new int[6]
			{
				51,
				0,
				61,
				65,
				67,
				0
			},
			key = "E♭",
			suffix = "9",
			Fingers = new int[6]
			{
				3,
				0,
				4,
				2,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 554,
			MidiNumbers = new int[6]
			{
				0,
				0,
				63,
				67,
				73,
				77
			},
			key = "E♭",
			suffix = "9",
			Fingers = new int[6]
			{
				0,
				0,
				2,
				1,
				4,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 555,
			MidiNumbers = new int[6]
			{
				51,
				55,
				61,
				65,
				70,
				0
			},
			key = "E♭",
			suffix = "9",
			Fingers = new int[6]
			{
				2,
				1,
				3,
				1,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 556,
			MidiNumbers = new int[6]
			{
				0,
				51,
				0,
				61,
				65,
				68
			},
			key = "E♭",
			suffix = "11",
			Fingers = new int[6]
			{
				0,
				2,
				0,
				3,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 557,
			MidiNumbers = new int[6]
			{
				0,
				51,
				55,
				61,
				63,
				68
			},
			key = "E♭",
			suffix = "11",
			Fingers = new int[6]
			{
				0,
				3,
				2,
				4,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 558,
			MidiNumbers = new int[6]
			{
				0,
				51,
				56,
				61,
				67,
				70
			},
			key = "E♭",
			suffix = "11",
			Fingers = new int[6]
			{
				0,
				1,
				1,
				1,
				3,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 559,
			MidiNumbers = new int[6]
			{
				0,
				0,
				51,
				56,
				61,
				67
			},
			key = "E♭",
			suffix = "11",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				1,
				2,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 560,
			MidiNumbers = new int[6]
			{
				0,
				51,
				55,
				61,
				65,
				72
			},
			key = "E♭",
			suffix = "13",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 561,
			MidiNumbers = new int[6]
			{
				51,
				0,
				61,
				65,
				67,
				72
			},
			key = "E♭",
			suffix = "13",
			Fingers = new int[6]
			{
				3,
				0,
				4,
				2,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 562,
			MidiNumbers = new int[6]
			{
				51,
				0,
				61,
				67,
				72,
				77
			},
			key = "E♭",
			suffix = "13",
			Fingers = new int[6]
			{
				1,
				0,
				1,
				1,
				2,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 563,
			MidiNumbers = new int[6]
			{
				51,
				58,
				61,
				67,
				72,
				75
			},
			key = "E♭",
			suffix = "13",
			Fingers = new int[6]
			{
				1,
				3,
				1,
				2,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 564,
			MidiNumbers = new int[6]
			{
				0,
				51,
				56,
				61,
				63,
				68
			},
			key = "E♭",
			suffix = "7sus4",
			Fingers = new int[6]
			{
				0,
				2,
				3,
				4,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 565,
			MidiNumbers = new int[6]
			{
				0,
				51,
				58,
				61,
				68,
				70
			},
			key = "E♭",
			suffix = "7sus4",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				1,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 566,
			MidiNumbers = new int[6]
			{
				51,
				58,
				61,
				68,
				70,
				75
			},
			key = "E♭",
			suffix = "7sus4",
			Fingers = new int[6]
			{
				1,
				2,
				1,
				3,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 567,
			MidiNumbers = new int[6]
			{
				0,
				0,
				51,
				58,
				61,
				68
			},
			key = "E♭",
			suffix = "7sus4",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				2,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 568,
			MidiNumbers = new int[6]
			{
				0,
				51,
				56,
				61,
				65,
				70
			},
			key = "E♭",
			suffix = "9sus4",
			Fingers = new int[6]
			{
				0,
				1,
				1,
				1,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 569,
			MidiNumbers = new int[6]
			{
				51,
				0,
				61,
				65,
				68,
				0
			},
			key = "E♭",
			suffix = "9sus4",
			Fingers = new int[6]
			{
				3,
				0,
				4,
				2,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 570,
			MidiNumbers = new int[6]
			{
				51,
				0,
				61,
				65,
				68,
				73
			},
			key = "E♭",
			suffix = "9sus4",
			Fingers = new int[6]
			{
				3,
				0,
				4,
				2,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 571,
			MidiNumbers = new int[6]
			{
				0,
				51,
				56,
				61,
				65,
				72
			},
			key = "E♭",
			suffix = "13sus4",
			Fingers = new int[6]
			{
				0,
				1,
				1,
				1,
				1,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 572,
			MidiNumbers = new int[6]
			{
				51,
				0,
				61,
				68,
				72,
				77
			},
			key = "E♭",
			suffix = "13sus4",
			Fingers = new int[6]
			{
				1,
				0,
				2,
				4,
				4,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 573,
			MidiNumbers = new int[6]
			{
				0,
				51,
				57,
				60,
				66,
				0
			},
			key = "E♭",
			suffix = "°7",
			Fingers = new int[6]
			{
				0,
				2,
				3,
				1,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 574,
			MidiNumbers = new int[6]
			{
				0,
				0,
				51,
				57,
				60,
				66
			},
			key = "E♭",
			suffix = "°7",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				2,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 575,
			MidiNumbers = new int[6]
			{
				51,
				0,
				60,
				66,
				69,
				0
			},
			key = "E♭",
			suffix = "°7",
			Fingers = new int[6]
			{
				3,
				0,
				1,
				4,
				2,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 576,
			MidiNumbers = new int[6]
			{
				0,
				51,
				59,
				61,
				67,
				71
			},
			key = "E♭",
			suffix = "+7",
			Fingers = new int[6]
			{
				0,
				1,
				4,
				1,
				3,
				2
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 577,
			MidiNumbers = new int[6]
			{
				51,
				0,
				61,
				67,
				71,
				0
			},
			key = "E♭",
			suffix = "+7",
			Fingers = new int[6]
			{
				1,
				0,
				2,
				3,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 578,
			MidiNumbers = new int[6]
			{
				0,
				0,
				51,
				59,
				61,
				67
			},
			key = "E♭",
			suffix = "+7",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				4,
				2,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 579,
			MidiNumbers = new int[6]
			{
				0,
				51,
				0,
				61,
				67,
				71
			},
			key = "E♭",
			suffix = "+7",
			Fingers = new int[6]
			{
				0,
				1,
				0,
				2,
				4,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 580,
			MidiNumbers = new int[6]
			{
				0,
				51,
				57,
				61,
				67,
				0
			},
			key = "E♭",
			suffix = "7♭5",
			Fingers = new int[6]
			{
				0,
				1,
				2,
				1,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 581,
			MidiNumbers = new int[6]
			{
				51,
				0,
				61,
				67,
				69,
				0
			},
			key = "E♭",
			suffix = "7♭5",
			Fingers = new int[6]
			{
				2,
				0,
				3,
				4,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 582,
			MidiNumbers = new int[6]
			{
				0,
				0,
				51,
				57,
				61,
				67
			},
			key = "E♭",
			suffix = "7♭5",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				2,
				2,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 583,
			MidiNumbers = new int[6]
			{
				0,
				51,
				57,
				61,
				67,
				73
			},
			key = "E♭",
			suffix = "7♭5",
			Fingers = new int[6]
			{
				0,
				1,
				2,
				1,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 584,
			MidiNumbers = new int[6]
			{
				0,
				51,
				55,
				61,
				66,
				0
			},
			key = "E♭",
			suffix = "7♯9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 585,
			MidiNumbers = new int[6]
			{
				51,
				58,
				61,
				67,
				70,
				78
			},
			key = "E♭",
			suffix = "7♯9",
			Fingers = new int[6]
			{
				1,
				3,
				1,
				2,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 586,
			MidiNumbers = new int[6]
			{
				0,
				0,
				63,
				67,
				73,
				78
			},
			key = "E♭",
			suffix = "7♯9",
			Fingers = new int[6]
			{
				0,
				0,
				2,
				1,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 587,
			MidiNumbers = new int[6]
			{
				0,
				51,
				55,
				61,
				66,
				69
			},
			key = "E♭",
			suffix = "7♭5♯9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 588,
			MidiNumbers = new int[6]
			{
				51,
				57,
				61,
				67,
				70,
				75
			},
			key = "E♭",
			suffix = "7♯11",
			Fingers = new int[6]
			{
				1,
				2,
				1,
				3,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 589,
			MidiNumbers = new int[6]
			{
				0,
				51,
				55,
				61,
				64,
				0
			},
			key = "E♭",
			suffix = "7♭9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 590,
			MidiNumbers = new int[6]
			{
				51,
				58,
				61,
				67,
				70,
				76
			},
			key = "E♭",
			suffix = "7♭9",
			Fingers = new int[6]
			{
				1,
				4,
				1,
				2,
				1,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 591,
			MidiNumbers = new int[6]
			{
				0,
				51,
				55,
				61,
				64,
				70
			},
			key = "E♭",
			suffix = "7♭9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 592,
			MidiNumbers = new int[6]
			{
				0,
				0,
				63,
				67,
				73,
				76
			},
			key = "E♭",
			suffix = "7♭9",
			Fingers = new int[6]
			{
				0,
				0,
				2,
				1,
				3,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 593,
			MidiNumbers = new int[6]
			{
				0,
				51,
				55,
				61,
				65,
				71
			},
			key = "E♭",
			suffix = "+9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 594,
			MidiNumbers = new int[6]
			{
				51,
				0,
				61,
				67,
				71,
				77
			},
			key = "E♭",
			suffix = "+9",
			Fingers = new int[6]
			{
				1,
				0,
				2,
				3,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 595,
			MidiNumbers = new int[6]
			{
				0,
				51,
				58,
				62,
				66,
				70
			},
			key = "E♭",
			suffix = "mMaj7",
			Fingers = new int[6]
			{
				0,
				1,
				4,
				2,
				3,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 596,
			MidiNumbers = new int[6]
			{
				51,
				0,
				62,
				66,
				70,
				0
			},
			key = "E♭",
			suffix = "mMaj7",
			Fingers = new int[6]
			{
				1,
				0,
				4,
				2,
				3,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 597,
			MidiNumbers = new int[6]
			{
				51,
				58,
				62,
				66,
				70,
				75
			},
			key = "E♭",
			suffix = "mMaj7",
			Fingers = new int[6]
			{
				1,
				3,
				2,
				1,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 598,
			MidiNumbers = new int[6]
			{
				0,
				0,
				51,
				58,
				62,
				66
			},
			key = "E♭",
			suffix = "mMaj7",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				4,
				2
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 599,
			MidiNumbers = new int[6]
			{
				0,
				0,
				63,
				66,
				70,
				74
			},
			key = "E♭",
			suffix = "mMaj7",
			Fingers = new int[6]
			{
				0,
				0,
				4,
				2,
				3,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 600,
			MidiNumbers = new int[6]
			{
				0,
				0,
				63,
				67,
				70,
				77
			},
			key = "E♭",
			suffix = "add9",
			Fingers = new int[6]
			{
				0,
				0,
				3,
				2,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 601,
			MidiNumbers = new int[6]
			{
				0,
				51,
				58,
				65,
				67,
				70
			},
			key = "E♭",
			suffix = "add9",
			Fingers = new int[6]
			{
				0,
				1,
				2,
				4,
				3,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 602,
			MidiNumbers = new int[6]
			{
				51,
				0,
				0,
				67,
				70,
				77
			},
			key = "E♭",
			suffix = "add9",
			Fingers = new int[6]
			{
				1,
				0,
				0,
				3,
				2,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 603,
			MidiNumbers = new int[6]
			{
				0,
				0,
				63,
				66,
				70,
				77
			},
			key = "E♭",
			suffix = "madd9",
			Fingers = new int[6]
			{
				0,
				0,
				3,
				1,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 604,
			MidiNumbers = new int[6]
			{
				0,
				0,
				51,
				58,
				60,
				66
			},
			key = "E♭",
			suffix = "m6",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				1,
				2
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 605,
			MidiNumbers = new int[6]
			{
				0,
				51,
				54,
				60,
				63,
				0
			},
			key = "E♭",
			suffix = "m6",
			Fingers = new int[6]
			{
				0,
				4,
				1,
				3,
				2,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 606,
			MidiNumbers = new int[6]
			{
				0,
				0,
				63,
				70,
				72,
				78
			},
			key = "E♭",
			suffix = "m6",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				1,
				2
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 607,
			MidiNumbers = new int[6]
			{
				0,
				51,
				58,
				60,
				66,
				0
			},
			key = "E♭",
			suffix = "m6",
			Fingers = new int[6]
			{
				0,
				2,
				4,
				1,
				3,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 608,
			MidiNumbers = new int[6]
			{
				51,
				58,
				63,
				66,
				72,
				75
			},
			key = "E♭",
			suffix = "m6",
			Fingers = new int[6]
			{
				1,
				2,
				3,
				1,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 609,
			MidiNumbers = new int[6]
			{
				0,
				51,
				54,
				60,
				63,
				70
			},
			key = "E♭",
			suffix = "m6",
			Fingers = new int[6]
			{
				0,
				3,
				1,
				2,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 610,
			MidiNumbers = new int[6]
			{
				51,
				0,
				60,
				66,
				70,
				0
			},
			key = "E♭",
			suffix = "m6",
			Fingers = new int[6]
			{
				2,
				0,
				1,
				3,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 611,
			MidiNumbers = new int[6]
			{
				0,
				51,
				0,
				60,
				66,
				70
			},
			key = "E♭",
			suffix = "m6",
			Fingers = new int[6]
			{
				0,
				2,
				0,
				1,
				4,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 612,
			MidiNumbers = new int[6]
			{
				0,
				51,
				54,
				59,
				63,
				70
			},
			key = "E♭",
			suffix = "m♭6",
			Fingers = new int[6]
			{
				0,
				3,
				1,
				1,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 613,
			MidiNumbers = new int[6]
			{
				51,
				0,
				59,
				66,
				70,
				0
			},
			key = "E♭",
			suffix = "m♭6",
			Fingers = new int[6]
			{
				2,
				0,
				1,
				3,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 614,
			MidiNumbers = new int[6]
			{
				0,
				51,
				57,
				61,
				66,
				0
			},
			key = "E♭",
			suffix = "m7♭5",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				2,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 615,
			MidiNumbers = new int[6]
			{
				0,
				0,
				51,
				57,
				61,
				66
			},
			key = "E♭",
			suffix = "m7♭5",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				2,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 616,
			MidiNumbers = new int[6]
			{
				51,
				0,
				61,
				66,
				69,
				0
			},
			key = "E♭",
			suffix = "m7♭5",
			Fingers = new int[6]
			{
				2,
				0,
				3,
				4,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 617,
			MidiNumbers = new int[6]
			{
				51,
				57,
				63,
				69,
				73,
				78
			},
			key = "E♭",
			suffix = "m7♭5",
			Fingers = new int[6]
			{
				1,
				2,
				3,
				4,
				4,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 618,
			MidiNumbers = new int[6]
			{
				0,
				51,
				0,
				61,
				66,
				69
			},
			key = "E♭",
			suffix = "m7♭5",
			Fingers = new int[6]
			{
				0,
				2,
				0,
				3,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 619,
			MidiNumbers = new int[6]
			{
				51,
				57,
				61,
				66,
				73,
				75
			},
			key = "E♭",
			suffix = "m7♭5",
			Fingers = new int[6]
			{
				1,
				2,
				1,
				1,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 620,
			MidiNumbers = new int[6]
			{
				0,
				51,
				54,
				61,
				65,
				0
			},
			key = "E♭",
			suffix = "m9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 621,
			MidiNumbers = new int[6]
			{
				51,
				58,
				61,
				66,
				70,
				77
			},
			key = "E♭",
			suffix = "m9",
			Fingers = new int[6]
			{
				1,
				2,
				1,
				1,
				1,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 622,
			MidiNumbers = new int[6]
			{
				0,
				51,
				54,
				61,
				65,
				70
			},
			key = "E♭",
			suffix = "m9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				3,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 623,
			MidiNumbers = new int[6]
			{
				0,
				0,
				63,
				66,
				73,
				77
			},
			key = "E♭",
			suffix = "m9",
			Fingers = new int[6]
			{
				0,
				0,
				2,
				1,
				4,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 624,
			MidiNumbers = new int[6]
			{
				51,
				0,
				61,
				66,
				70,
				77
			},
			key = "E♭",
			suffix = "m9",
			Fingers = new int[6]
			{
				2,
				0,
				3,
				3,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 625,
			MidiNumbers = new int[6]
			{
				0,
				51,
				54,
				61,
				63,
				68
			},
			key = "E♭",
			suffix = "m11",
			Fingers = new int[6]
			{
				0,
				3,
				1,
				4,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 626,
			MidiNumbers = new int[6]
			{
				0,
				51,
				56,
				61,
				66,
				70
			},
			key = "E♭",
			suffix = "m11",
			Fingers = new int[6]
			{
				0,
				1,
				1,
				1,
				2,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 627,
			MidiNumbers = new int[6]
			{
				51,
				0,
				61,
				66,
				68,
				0
			},
			key = "E♭",
			suffix = "m11",
			Fingers = new int[6]
			{
				2,
				0,
				3,
				4,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 628,
			MidiNumbers = new int[6]
			{
				0,
				0,
				51,
				56,
				61,
				66
			},
			key = "E♭",
			suffix = "m11",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				1,
				2,
				2
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 629,
			MidiNumbers = new int[6]
			{
				0,
				51,
				0,
				61,
				66,
				68
			},
			key = "E♭",
			suffix = "m11",
			Fingers = new int[6]
			{
				0,
				2,
				0,
				3,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 630,
			MidiNumbers = new int[6]
			{
				0,
				51,
				0,
				61,
				66,
				72
			},
			key = "E♭",
			suffix = "m13",
			Fingers = new int[6]
			{
				0,
				1,
				0,
				2,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 631,
			MidiNumbers = new int[6]
			{
				51,
				58,
				61,
				66,
				72,
				75
			},
			key = "E♭",
			suffix = "m13",
			Fingers = new int[6]
			{
				1,
				3,
				1,
				1,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 632,
			MidiNumbers = new int[6]
			{
				51,
				0,
				61,
				66,
				72,
				77
			},
			key = "E♭",
			suffix = "m13",
			Fingers = new int[6]
			{
				2,
				0,
				3,
				3,
				4,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 633,
			MidiNumbers = new int[6]
			{
				0,
				51,
				58,
				61,
				66,
				72
			},
			key = "E♭",
			suffix = "m13",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				1,
				2,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 634,
			MidiNumbers = new int[6]
			{
				0,
				51,
				54,
				61,
				65,
				72
			},
			key = "E♭",
			suffix = "m13",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 635,
			MidiNumbers = new int[6]
			{
				0,
				51,
				54,
				62,
				65,
				0
			},
			key = "E♭",
			suffix = "mMaj9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				4,
				3,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 636,
			MidiNumbers = new int[6]
			{
				51,
				58,
				62,
				66,
				70,
				77
			},
			key = "E♭",
			suffix = "mMaj9",
			Fingers = new int[6]
			{
				1,
				3,
				2,
				1,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 637,
			MidiNumbers = new int[6]
			{
				0,
				51,
				55,
				62,
				65,
				0
			},
			key = "E♭",
			suffix = "Maj9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				4,
				3,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 638,
			MidiNumbers = new int[6]
			{
				0,
				0,
				63,
				67,
				74,
				77
			},
			key = "E♭",
			suffix = "Maj9",
			Fingers = new int[6]
			{
				0,
				0,
				2,
				1,
				4,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 639,
			MidiNumbers = new int[6]
			{
				0,
				51,
				53,
				58,
				62,
				67
			},
			key = "E♭",
			suffix = "Maj9",
			Fingers = new int[6]
			{
				0,
				3,
				1,
				1,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 640,
			MidiNumbers = new int[6]
			{
				51,
				55,
				62,
				65,
				70,
				74
			},
			key = "E♭",
			suffix = "Maj9",
			Fingers = new int[6]
			{
				2,
				1,
				4,
				1,
				3,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 641,
			MidiNumbers = new int[6]
			{
				51,
				0,
				62,
				65,
				70,
				0
			},
			key = "E♭",
			suffix = "Maj9",
			Fingers = new int[6]
			{
				2,
				0,
				4,
				1,
				3,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 642,
			MidiNumbers = new int[6]
			{
				0,
				51,
				0,
				62,
				67,
				72
			},
			key = "E♭",
			suffix = "Maj13",
			Fingers = new int[6]
			{
				0,
				1,
				0,
				2,
				4,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 643,
			MidiNumbers = new int[6]
			{
				51,
				55,
				60,
				65,
				70,
				74
			},
			key = "E♭",
			suffix = "Maj13",
			Fingers = new int[6]
			{
				2,
				1,
				1,
				1,
				3,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 644,
			MidiNumbers = new int[6]
			{
				51,
				0,
				62,
				67,
				72,
				77
			},
			key = "E♭",
			suffix = "Maj13",
			Fingers = new int[6]
			{
				1,
				0,
				2,
				3,
				4,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 645,
			MidiNumbers = new int[6]
			{
				0,
				51,
				57,
				62,
				67,
				70
			},
			key = "E♭",
			suffix = "Maj7♯11",
			Fingers = new int[6]
			{
				0,
				1,
				2,
				3,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 646,
			MidiNumbers = new int[6]
			{
				51,
				0,
				62,
				67,
				69,
				0
			},
			key = "E♭",
			suffix = "Maj7♯11",
			Fingers = new int[6]
			{
				2,
				0,
				3,
				4,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 647,
			MidiNumbers = new int[6]
			{
				0,
				0,
				51,
				57,
				62,
				67
			},
			key = "E♭",
			suffix = "Maj7♯11",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				2,
				3,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 648,
			MidiNumbers = new int[6]
			{
				0,
				51,
				55,
				61,
				65,
				69
			},
			key = "E♭",
			suffix = "9♭5",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 649,
			MidiNumbers = new int[6]
			{
				51,
				55,
				61,
				65,
				69,
				0
			},
			key = "E♭",
			suffix = "9♭5",
			Fingers = new int[6]
			{
				2,
				1,
				3,
				1,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 650,
			MidiNumbers = new int[6]
			{
				51,
				57,
				61,
				66,
				73,
				77
			},
			key = "E♭",
			suffix = "m9♭5",
			Fingers = new int[6]
			{
				1,
				2,
				1,
				1,
				4,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 651,
			MidiNumbers = new int[6]
			{
				0,
				51,
				55,
				61,
				64,
				71
			},
			key = "E♭",
			suffix = "+7♭9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 652,
			MidiNumbers = new int[6]
			{
				51,
				0,
				61,
				67,
				71,
				76
			},
			key = "E♭",
			suffix = "+7♭9",
			Fingers = new int[6]
			{
				1,
				0,
				2,
				3,
				3,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 653,
			MidiNumbers = new int[6]
			{
				0,
				51,
				55,
				61,
				66,
				71
			},
			key = "E♭",
			suffix = "+7♯9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				4,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 654,
			MidiNumbers = new int[6]
			{
				51,
				0,
				61,
				67,
				71,
				78
			},
			key = "E♭",
			suffix = "+7♯9",
			Fingers = new int[6]
			{
				1,
				0,
				2,
				3,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 655,
			MidiNumbers = new int[6]
			{
				40,
				47,
				52,
				56,
				59,
				64
			},
			key = "E",
			suffix = string.Empty,
			Fingers = new int[6]
			{
				0,
				2,
				3,
				1,
				0,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 656,
			MidiNumbers = new int[6]
			{
				0,
				52,
				59,
				64,
				68,
				71
			},
			key = "E",
			suffix = string.Empty,
			Fingers = new int[6]
			{
				0,
				1,
				2,
				3,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 657,
			MidiNumbers = new int[6]
			{
				0,
				52,
				56,
				59,
				64,
				68
			},
			key = "E",
			suffix = string.Empty,
			Fingers = new int[6]
			{
				0,
				4,
				3,
				1,
				2,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 658,
			MidiNumbers = new int[6]
			{
				40,
				0,
				52,
				59,
				64,
				68
			},
			key = "E",
			suffix = string.Empty,
			Fingers = new int[6]
			{
				0,
				0,
				1,
				2,
				4,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 659,
			MidiNumbers = new int[6]
			{
				52,
				59,
				64,
				68,
				71,
				76
			},
			key = "E",
			suffix = string.Empty,
			Fingers = new int[6]
			{
				1,
				3,
				4,
				2,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 660,
			MidiNumbers = new int[6]
			{
				52,
				56,
				59,
				64,
				68,
				76
			},
			key = "E",
			suffix = string.Empty,
			Fingers = new int[6]
			{
				3,
				2,
				1,
				1,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 661,
			MidiNumbers = new int[6]
			{
				0,
				0,
				52,
				59,
				64,
				68
			},
			key = "E",
			suffix = string.Empty,
			Fingers = new int[6]
			{
				0,
				0,
				1,
				2,
				4,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 662,
			MidiNumbers = new int[6]
			{
				40,
				47,
				52,
				55,
				59,
				64
			},
			key = "E",
			suffix = "m",
			Fingers = new int[6]
			{
				0,
				2,
				3,
				0,
				0,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 663,
			MidiNumbers = new int[6]
			{
				0,
				52,
				59,
				64,
				67,
				71
			},
			key = "E",
			suffix = "m",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				4,
				2,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 664,
			MidiNumbers = new int[6]
			{
				40,
				55,
				59,
				55,
				67,
				64
			},
			key = "E",
			suffix = "m",
			Fingers = new int[6]
			{
				0,
				3,
				2,
				0,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 665,
			MidiNumbers = new int[6]
			{
				40,
				0,
				52,
				59,
				64,
				67
			},
			key = "E",
			suffix = "m",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				4,
				2
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 666,
			MidiNumbers = new int[6]
			{
				52,
				59,
				64,
				67,
				71,
				76
			},
			key = "E",
			suffix = "m",
			Fingers = new int[6]
			{
				1,
				3,
				4,
				1,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 667,
			MidiNumbers = new int[6]
			{
				0,
				0,
				64,
				67,
				71,
				76
			},
			key = "E",
			suffix = "m",
			Fingers = new int[6]
			{
				0,
				0,
				3,
				1,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 668,
			MidiNumbers = new int[6]
			{
				0,
				0,
				52,
				59,
				64,
				67
			},
			key = "E",
			suffix = "m",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				4,
				2
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 669,
			MidiNumbers = new int[6]
			{
				40,
				47,
				50,
				56,
				59,
				64
			},
			key = "E",
			suffix = "7",
			Fingers = new int[6]
			{
				0,
				2,
				0,
				1,
				0,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 670,
			MidiNumbers = new int[6]
			{
				40,
				47,
				52,
				56,
				62,
				64
			},
			key = "E",
			suffix = "7",
			Fingers = new int[6]
			{
				0,
				2,
				3,
				1,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 671,
			MidiNumbers = new int[6]
			{
				0,
				52,
				59,
				62,
				68,
				71
			},
			key = "E",
			suffix = "7",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				1,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 672,
			MidiNumbers = new int[6]
			{
				40,
				47,
				50,
				56,
				62,
				64
			},
			key = "E",
			suffix = "7",
			Fingers = new int[6]
			{
				0,
				2,
				0,
				1,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 673,
			MidiNumbers = new int[6]
			{
				0,
				0,
				52,
				59,
				62,
				68
			},
			key = "E",
			suffix = "7",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				2,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 674,
			MidiNumbers = new int[6]
			{
				0,
				52,
				56,
				62,
				64,
				0
			},
			key = "E",
			suffix = "7",
			Fingers = new int[6]
			{
				0,
				3,
				2,
				4,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 675,
			MidiNumbers = new int[6]
			{
				52,
				59,
				62,
				68,
				71,
				76
			},
			key = "E",
			suffix = "7",
			Fingers = new int[6]
			{
				1,
				3,
				1,
				2,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 676,
			MidiNumbers = new int[6]
			{
				52,
				0,
				62,
				68,
				71,
				0
			},
			key = "E",
			suffix = "7",
			Fingers = new int[6]
			{
				1,
				0,
				2,
				4,
				3,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 677,
			MidiNumbers = new int[6]
			{
				40,
				47,
				52,
				59,
				64,
				66
			},
			key = "E",
			suffix = "sus2",
			Fingers = new int[6]
			{
				0,
				1,
				1,
				3,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 678,
			MidiNumbers = new int[6]
			{
				40,
				52,
				54,
				59,
				66,
				64
			},
			key = "E",
			suffix = "sus2",
			Fingers = new int[6]
			{
				0,
				3,
				1,
				2,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 679,
			MidiNumbers = new int[6]
			{
				40,
				52,
				59,
				64,
				66,
				71
			},
			key = "E",
			suffix = "sus2",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				4,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 680,
			MidiNumbers = new int[6]
			{
				0,
				52,
				59,
				64,
				66,
				71
			},
			key = "E",
			suffix = "sus2",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				4,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 681,
			MidiNumbers = new int[6]
			{
				0,
				52,
				59,
				66,
				0,
				0
			},
			key = "E",
			suffix = "sus2",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				4,
				0,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 682,
			MidiNumbers = new int[6]
			{
				0,
				0,
				64,
				66,
				71,
				78
			},
			key = "E",
			suffix = "sus2",
			Fingers = new int[6]
			{
				0,
				0,
				3,
				1,
				2,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 683,
			MidiNumbers = new int[6]
			{
				0,
				0,
				52,
				59,
				64,
				66
			},
			key = "E",
			suffix = "sus2",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 684,
			MidiNumbers = new int[6]
			{
				40,
				47,
				52,
				57,
				59,
				64
			},
			key = "E",
			suffix = "sus4",
			Fingers = new int[6]
			{
				0,
				2,
				3,
				4,
				0,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 685,
			MidiNumbers = new int[6]
			{
				0,
				52,
				59,
				64,
				69,
				71
			},
			key = "E",
			suffix = "sus4",
			Fingers = new int[6]
			{
				0,
				1,
				2,
				3,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 686,
			MidiNumbers = new int[6]
			{
				40,
				45,
				52,
				57,
				59,
				64
			},
			key = "E",
			suffix = "sus4",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				2,
				0,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 687,
			MidiNumbers = new int[6]
			{
				40,
				45,
				52,
				59,
				59,
				64
			},
			key = "E",
			suffix = "sus4",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				2,
				0,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 688,
			MidiNumbers = new int[6]
			{
				0,
				0,
				52,
				59,
				64,
				69
			},
			key = "E",
			suffix = "sus4",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				2,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 689,
			MidiNumbers = new int[6]
			{
				52,
				59,
				64,
				69,
				71,
				76
			},
			key = "E",
			suffix = "sus4",
			Fingers = new int[6]
			{
				1,
				2,
				3,
				4,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 690,
			MidiNumbers = new int[6]
			{
				0,
				0,
				64,
				69,
				71,
				76
			},
			key = "E",
			suffix = "sus4",
			Fingers = new int[6]
			{
				0,
				0,
				3,
				4,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 691,
			MidiNumbers = new int[6]
			{
				40,
				47,
				52,
				55,
				62,
				64
			},
			key = "E",
			suffix = "m7",
			Fingers = new int[6]
			{
				0,
				1,
				2,
				0,
				3,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 692,
			MidiNumbers = new int[6]
			{
				0,
				52,
				59,
				62,
				67,
				71
			},
			key = "E",
			suffix = "m7",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				1,
				2,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 693,
			MidiNumbers = new int[6]
			{
				40,
				47,
				52,
				55,
				62,
				67
			},
			key = "E",
			suffix = "m7",
			Fingers = new int[6]
			{
				0,
				1,
				2,
				0,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 694,
			MidiNumbers = new int[6]
			{
				52,
				0,
				62,
				67,
				71,
				0
			},
			key = "E",
			suffix = "m7",
			Fingers = new int[6]
			{
				1,
				0,
				2,
				3,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 695,
			MidiNumbers = new int[6]
			{
				0,
				52,
				55,
				62,
				67,
				0
			},
			key = "E",
			suffix = "m7",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 696,
			MidiNumbers = new int[6]
			{
				52,
				59,
				64,
				67,
				74,
				76
			},
			key = "E",
			suffix = "m7",
			Fingers = new int[6]
			{
				1,
				2,
				3,
				1,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 697,
			MidiNumbers = new int[6]
			{
				52,
				59,
				62,
				67,
				71,
				76
			},
			key = "E",
			suffix = "m7",
			Fingers = new int[6]
			{
				1,
				3,
				1,
				1,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 698,
			MidiNumbers = new int[6]
			{
				0,
				52,
				55,
				62,
				64,
				0
			},
			key = "E",
			suffix = "m7",
			Fingers = new int[6]
			{
				0,
				3,
				1,
				4,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 699,
			MidiNumbers = new int[6]
			{
				0,
				0,
				52,
				59,
				62,
				67
			},
			key = "E",
			suffix = "m7",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				4,
				2,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 700,
			MidiNumbers = new int[6]
			{
				40,
				47,
				51,
				56,
				59,
				0
			},
			key = "E",
			suffix = "Maj7",
			Fingers = new int[6]
			{
				0,
				3,
				1,
				2,
				0,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 701,
			MidiNumbers = new int[6]
			{
				0,
				0,
				52,
				59,
				63,
				68
			},
			key = "E",
			suffix = "Maj7",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				3,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 702,
			MidiNumbers = new int[6]
			{
				0,
				52,
				59,
				63,
				68,
				71
			},
			key = "E",
			suffix = "Maj7",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				2,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 703,
			MidiNumbers = new int[6]
			{
				0,
				52,
				56,
				59,
				63,
				0
			},
			key = "E",
			suffix = "Maj7",
			Fingers = new int[6]
			{
				0,
				4,
				3,
				1,
				2,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 704,
			MidiNumbers = new int[6]
			{
				0,
				0,
				64,
				68,
				71,
				75
			},
			key = "E",
			suffix = "Maj7",
			Fingers = new int[6]
			{
				0,
				0,
				4,
				3,
				2,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 705,
			MidiNumbers = new int[6]
			{
				52,
				0,
				63,
				68,
				71,
				0
			},
			key = "E",
			suffix = "Maj7",
			Fingers = new int[6]
			{
				1,
				0,
				3,
				4,
				2,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 706,
			MidiNumbers = new int[6]
			{
				52,
				59,
				63,
				68,
				71,
				76
			},
			key = "E",
			suffix = "Maj7",
			Fingers = new int[6]
			{
				1,
				4,
				2,
				3,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 707,
			MidiNumbers = new int[6]
			{
				40,
				47,
				52,
				0,
				0,
				0
			},
			key = "E",
			suffix = "5",
			Fingers = new int[6]
			{
				0,
				1,
				2,
				0,
				0,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 708,
			MidiNumbers = new int[6]
			{
				0,
				52,
				59,
				64,
				0,
				0
			},
			key = "E",
			suffix = "5",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				4,
				0,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 709,
			MidiNumbers = new int[6]
			{
				52,
				59,
				64,
				0,
				0,
				0
			},
			key = "E",
			suffix = "5",
			Fingers = new int[6]
			{
				1,
				3,
				4,
				0,
				0,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 710,
			MidiNumbers = new int[6]
			{
				0,
				0,
				0,
				64,
				71,
				76
			},
			key = "E",
			suffix = "5",
			Fingers = new int[6]
			{
				0,
				0,
				0,
				1,
				4,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 711,
			MidiNumbers = new int[6]
			{
				0,
				0,
				52,
				59,
				64,
				0
			},
			key = "E",
			suffix = "5",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 712,
			MidiNumbers = new int[6]
			{
				0,
				52,
				56,
				60,
				64,
				0
			},
			key = "E",
			suffix = "+",
			Fingers = new int[6]
			{
				0,
				4,
				3,
				1,
				2,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 713,
			MidiNumbers = new int[6]
			{
				0,
				0,
				64,
				68,
				72,
				76
			},
			key = "E",
			suffix = "+",
			Fingers = new int[6]
			{
				0,
				0,
				4,
				2,
				3,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 714,
			MidiNumbers = new int[6]
			{
				52,
				56,
				60,
				64,
				0,
				0
			},
			key = "E",
			suffix = "+",
			Fingers = new int[6]
			{
				4,
				3,
				2,
				1,
				0,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 715,
			MidiNumbers = new int[6]
			{
				52,
				60,
				64,
				68,
				0,
				0
			},
			key = "E",
			suffix = "+",
			Fingers = new int[6]
			{
				1,
				4,
				3,
				2,
				0,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 716,
			MidiNumbers = new int[6]
			{
				0,
				52,
				0,
				64,
				68,
				72
			},
			key = "E",
			suffix = "+",
			Fingers = new int[6]
			{
				0,
				1,
				0,
				3,
				4,
				2
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 717,
			MidiNumbers = new int[6]
			{
				0,
				52,
				58,
				64,
				67,
				0
			},
			key = "E",
			suffix = "°",
			Fingers = new int[6]
			{
				0,
				1,
				2,
				4,
				3,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 718,
			MidiNumbers = new int[6]
			{
				52,
				0,
				64,
				67,
				70,
				0
			},
			key = "E",
			suffix = "°",
			Fingers = new int[6]
			{
				2,
				0,
				4,
				3,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 719,
			MidiNumbers = new int[6]
			{
				52,
				58,
				64,
				67,
				0,
				0
			},
			key = "E",
			suffix = "°",
			Fingers = new int[6]
			{
				1,
				3,
				4,
				2,
				0,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 720,
			MidiNumbers = new int[6]
			{
				40,
				47,
				52,
				56,
				61,
				64
			},
			key = "E",
			suffix = "6",
			Fingers = new int[6]
			{
				0,
				2,
				3,
				1,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 721,
			MidiNumbers = new int[6]
			{
				0,
				52,
				59,
				64,
				68,
				73
			},
			key = "E",
			suffix = "6",
			Fingers = new int[6]
			{
				0,
				1,
				2,
				3,
				4,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 722,
			MidiNumbers = new int[6]
			{
				0,
				0,
				52,
				59,
				61,
				68
			},
			key = "E",
			suffix = "6",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 723,
			MidiNumbers = new int[6]
			{
				0,
				0,
				52,
				56,
				61,
				64
			},
			key = "E",
			suffix = "6",
			Fingers = new int[6]
			{
				0,
				0,
				2,
				1,
				3,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 724,
			MidiNumbers = new int[6]
			{
				52,
				0,
				61,
				68,
				71,
				0
			},
			key = "E",
			suffix = "6",
			Fingers = new int[6]
			{
				2,
				0,
				1,
				4,
				3,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 725,
			MidiNumbers = new int[6]
			{
				40,
				47,
				50,
				56,
				59,
				66
			},
			key = "E",
			suffix = "9",
			Fingers = new int[6]
			{
				0,
				2,
				0,
				1,
				0,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 726,
			MidiNumbers = new int[6]
			{
				40,
				52,
				56,
				62,
				66,
				64
			},
			key = "E",
			suffix = "9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 727,
			MidiNumbers = new int[6]
			{
				52,
				0,
				62,
				66,
				71,
				76
			},
			key = "E",
			suffix = "9",
			Fingers = new int[6]
			{
				2,
				0,
				3,
				1,
				4,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 728,
			MidiNumbers = new int[6]
			{
				52,
				59,
				62,
				68,
				71,
				78
			},
			key = "E",
			suffix = "9",
			Fingers = new int[6]
			{
				1,
				3,
				1,
				2,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 729,
			MidiNumbers = new int[6]
			{
				0,
				52,
				56,
				62,
				66,
				71
			},
			key = "E",
			suffix = "9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				3,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 730,
			MidiNumbers = new int[6]
			{
				52,
				0,
				62,
				66,
				68,
				0
			},
			key = "E",
			suffix = "9",
			Fingers = new int[6]
			{
				3,
				0,
				4,
				2,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 731,
			MidiNumbers = new int[6]
			{
				0,
				0,
				52,
				56,
				62,
				66
			},
			key = "E",
			suffix = "9",
			Fingers = new int[6]
			{
				0,
				0,
				2,
				1,
				4,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 732,
			MidiNumbers = new int[6]
			{
				52,
				56,
				62,
				66,
				71,
				0
			},
			key = "E",
			suffix = "9",
			Fingers = new int[6]
			{
				2,
				1,
				3,
				1,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 733,
			MidiNumbers = new int[6]
			{
				40,
				47,
				50,
				57,
				59,
				66
			},
			key = "E",
			suffix = "11",
			Fingers = new int[6]
			{
				0,
				1,
				0,
				2,
				0,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 734,
			MidiNumbers = new int[6]
			{
				0,
				52,
				56,
				62,
				64,
				69
			},
			key = "E",
			suffix = "11",
			Fingers = new int[6]
			{
				0,
				3,
				2,
				4,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 735,
			MidiNumbers = new int[6]
			{
				0,
				52,
				57,
				62,
				68,
				71
			},
			key = "E",
			suffix = "11",
			Fingers = new int[6]
			{
				0,
				1,
				1,
				1,
				3,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 736,
			MidiNumbers = new int[6]
			{
				0,
				0,
				52,
				57,
				62,
				68
			},
			key = "E",
			suffix = "11",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				1,
				2,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 737,
			MidiNumbers = new int[6]
			{
				40,
				0,
				50,
				56,
				61,
				66
			},
			key = "E",
			suffix = "13",
			Fingers = new int[6]
			{
				0,
				0,
				0,
				1,
				2,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 738,
			MidiNumbers = new int[6]
			{
				0,
				52,
				56,
				62,
				66,
				73
			},
			key = "E",
			suffix = "13",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 739,
			MidiNumbers = new int[6]
			{
				52,
				0,
				62,
				66,
				68,
				73
			},
			key = "E",
			suffix = "13",
			Fingers = new int[6]
			{
				3,
				0,
				4,
				2,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 740,
			MidiNumbers = new int[6]
			{
				52,
				0,
				62,
				68,
				73,
				78
			},
			key = "E",
			suffix = "13",
			Fingers = new int[6]
			{
				1,
				0,
				1,
				1,
				2,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 741,
			MidiNumbers = new int[6]
			{
				52,
				59,
				62,
				68,
				73,
				76
			},
			key = "E",
			suffix = "13",
			Fingers = new int[6]
			{
				1,
				3,
				1,
				2,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 742,
			MidiNumbers = new int[6]
			{
				40,
				47,
				50,
				57,
				59,
				64
			},
			key = "E",
			suffix = "7sus4",
			Fingers = new int[6]
			{
				0,
				1,
				0,
				2,
				0,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 743,
			MidiNumbers = new int[6]
			{
				0,
				52,
				59,
				62,
				69,
				71
			},
			key = "E",
			suffix = "7sus4",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				1,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 744,
			MidiNumbers = new int[6]
			{
				0,
				52,
				57,
				62,
				64,
				69
			},
			key = "E",
			suffix = "7sus4",
			Fingers = new int[6]
			{
				0,
				2,
				3,
				4,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 745,
			MidiNumbers = new int[6]
			{
				52,
				59,
				62,
				69,
				71,
				76
			},
			key = "E",
			suffix = "7sus4",
			Fingers = new int[6]
			{
				1,
				3,
				1,
				4,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 746,
			MidiNumbers = new int[6]
			{
				0,
				0,
				52,
				59,
				62,
				69
			},
			key = "E",
			suffix = "7sus4",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				2,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 747,
			MidiNumbers = new int[6]
			{
				0,
				52,
				57,
				62,
				66,
				71
			},
			key = "E",
			suffix = "9sus4",
			Fingers = new int[6]
			{
				0,
				1,
				1,
				1,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 748,
			MidiNumbers = new int[6]
			{
				52,
				0,
				62,
				66,
				69,
				0
			},
			key = "E",
			suffix = "9sus4",
			Fingers = new int[6]
			{
				3,
				0,
				4,
				2,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 749,
			MidiNumbers = new int[6]
			{
				52,
				0,
				62,
				66,
				69,
				74
			},
			key = "E",
			suffix = "9sus4",
			Fingers = new int[6]
			{
				3,
				0,
				4,
				2,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 750,
			MidiNumbers = new int[6]
			{
				0,
				52,
				57,
				62,
				66,
				73
			},
			key = "E",
			suffix = "13sus4",
			Fingers = new int[6]
			{
				0,
				1,
				1,
				1,
				1,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 751,
			MidiNumbers = new int[6]
			{
				52,
				0,
				62,
				69,
				73,
				78
			},
			key = "E",
			suffix = "13sus4",
			Fingers = new int[6]
			{
				1,
				0,
				2,
				4,
				4,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 752,
			MidiNumbers = new int[6]
			{
				40,
				46,
				52,
				55,
				61,
				64
			},
			key = "E",
			suffix = "°7",
			Fingers = new int[6]
			{
				0,
				1,
				2,
				0,
				3,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 753,
			MidiNumbers = new int[6]
			{
				40,
				52,
				58,
				61,
				67,
				64
			},
			key = "E",
			suffix = "°7",
			Fingers = new int[6]
			{
				0,
				2,
				3,
				1,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 754,
			MidiNumbers = new int[6]
			{
				0,
				0,
				52,
				58,
				61,
				67
			},
			key = "E",
			suffix = "°7",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				2,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 755,
			MidiNumbers = new int[6]
			{
				0,
				52,
				58,
				61,
				67,
				0
			},
			key = "E",
			suffix = "°7",
			Fingers = new int[6]
			{
				0,
				2,
				3,
				1,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 756,
			MidiNumbers = new int[6]
			{
				52,
				0,
				61,
				67,
				70,
				0
			},
			key = "E",
			suffix = "°7",
			Fingers = new int[6]
			{
				3,
				0,
				1,
				4,
				2,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 757,
			MidiNumbers = new int[6]
			{
				0,
				52,
				60,
				62,
				68,
				72
			},
			key = "E",
			suffix = "+7",
			Fingers = new int[6]
			{
				0,
				1,
				4,
				1,
				3,
				2
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 758,
			MidiNumbers = new int[6]
			{
				52,
				0,
				62,
				68,
				72,
				0
			},
			key = "E",
			suffix = "+7",
			Fingers = new int[6]
			{
				1,
				0,
				2,
				3,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 759,
			MidiNumbers = new int[6]
			{
				0,
				0,
				52,
				60,
				62,
				68
			},
			key = "E",
			suffix = "+7",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				4,
				2,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 760,
			MidiNumbers = new int[6]
			{
				0,
				52,
				0,
				62,
				68,
				72
			},
			key = "E",
			suffix = "+7",
			Fingers = new int[6]
			{
				0,
				1,
				0,
				2,
				4,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 761,
			MidiNumbers = new int[6]
			{
				0,
				52,
				58,
				62,
				68,
				0
			},
			key = "E",
			suffix = "7♭5",
			Fingers = new int[6]
			{
				0,
				1,
				2,
				1,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 762,
			MidiNumbers = new int[6]
			{
				52,
				0,
				62,
				68,
				70,
				0
			},
			key = "E",
			suffix = "7♭5",
			Fingers = new int[6]
			{
				2,
				0,
				3,
				4,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 763,
			MidiNumbers = new int[6]
			{
				0,
				0,
				52,
				58,
				62,
				68
			},
			key = "E",
			suffix = "7♭5",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				2,
				2,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 764,
			MidiNumbers = new int[6]
			{
				0,
				52,
				58,
				62,
				68,
				74
			},
			key = "E",
			suffix = "7♭5",
			Fingers = new int[6]
			{
				0,
				1,
				2,
				1,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 765,
			MidiNumbers = new int[6]
			{
				0,
				52,
				56,
				62,
				67,
				0
			},
			key = "E",
			suffix = "7♯9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 766,
			MidiNumbers = new int[6]
			{
				52,
				59,
				62,
				68,
				71,
				79
			},
			key = "E",
			suffix = "7♯9",
			Fingers = new int[6]
			{
				1,
				3,
				1,
				2,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 767,
			MidiNumbers = new int[6]
			{
				0,
				0,
				52,
				56,
				62,
				67
			},
			key = "E",
			suffix = "7♯9",
			Fingers = new int[6]
			{
				0,
				0,
				2,
				1,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 768,
			MidiNumbers = new int[6]
			{
				0,
				52,
				56,
				62,
				67,
				70
			},
			key = "E",
			suffix = "7♭5♯9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 769,
			MidiNumbers = new int[6]
			{
				52,
				58,
				62,
				68,
				71,
				76
			},
			key = "E",
			suffix = "7♯11",
			Fingers = new int[6]
			{
				1,
				2,
				1,
				3,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 770,
			MidiNumbers = new int[6]
			{
				0,
				52,
				56,
				62,
				65,
				0
			},
			key = "E",
			suffix = "7♭9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 771,
			MidiNumbers = new int[6]
			{
				52,
				59,
				62,
				68,
				71,
				77
			},
			key = "E",
			suffix = "7♭9",
			Fingers = new int[6]
			{
				1,
				4,
				1,
				2,
				1,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 772,
			MidiNumbers = new int[6]
			{
				0,
				52,
				56,
				62,
				65,
				71
			},
			key = "E",
			suffix = "7♭9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 773,
			MidiNumbers = new int[6]
			{
				0,
				0,
				52,
				56,
				62,
				65
			},
			key = "E",
			suffix = "7♭9",
			Fingers = new int[6]
			{
				0,
				0,
				2,
				1,
				3,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 774,
			MidiNumbers = new int[6]
			{
				0,
				52,
				56,
				62,
				66,
				72
			},
			key = "E",
			suffix = "+9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 775,
			MidiNumbers = new int[6]
			{
				52,
				0,
				62,
				68,
				72,
				78
			},
			key = "E",
			suffix = "+9",
			Fingers = new int[6]
			{
				1,
				0,
				2,
				3,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 776,
			MidiNumbers = new int[6]
			{
				0,
				52,
				59,
				63,
				67,
				71
			},
			key = "E",
			suffix = "mMaj7",
			Fingers = new int[6]
			{
				0,
				1,
				4,
				2,
				3,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 777,
			MidiNumbers = new int[6]
			{
				52,
				0,
				63,
				67,
				71,
				0
			},
			key = "E",
			suffix = "mMaj7",
			Fingers = new int[6]
			{
				1,
				0,
				4,
				2,
				3,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 778,
			MidiNumbers = new int[6]
			{
				52,
				59,
				63,
				67,
				71,
				76
			},
			key = "E",
			suffix = "mMaj7",
			Fingers = new int[6]
			{
				1,
				3,
				2,
				1,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 779,
			MidiNumbers = new int[6]
			{
				0,
				0,
				52,
				59,
				63,
				67
			},
			key = "E",
			suffix = "mMaj7",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				4,
				2
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 780,
			MidiNumbers = new int[6]
			{
				0,
				0,
				64,
				67,
				71,
				75
			},
			key = "E",
			suffix = "mMaj7",
			Fingers = new int[6]
			{
				0,
				0,
				4,
				2,
				3,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 781,
			MidiNumbers = new int[6]
			{
				40,
				47,
				54,
				56,
				59,
				64
			},
			key = "E",
			suffix = "add9",
			Fingers = new int[6]
			{
				0,
				2,
				4,
				1,
				0,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 782,
			MidiNumbers = new int[6]
			{
				40,
				47,
				52,
				56,
				59,
				66
			},
			key = "E",
			suffix = "add9",
			Fingers = new int[6]
			{
				0,
				2,
				3,
				1,
				0,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 783,
			MidiNumbers = new int[6]
			{
				0,
				0,
				64,
				68,
				71,
				78
			},
			key = "E",
			suffix = "add9",
			Fingers = new int[6]
			{
				0,
				0,
				3,
				2,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 784,
			MidiNumbers = new int[6]
			{
				0,
				52,
				59,
				66,
				68,
				71
			},
			key = "E",
			suffix = "add9",
			Fingers = new int[6]
			{
				0,
				1,
				2,
				4,
				3,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 785,
			MidiNumbers = new int[6]
			{
				52,
				0,
				0,
				68,
				71,
				78
			},
			key = "E",
			suffix = "add9",
			Fingers = new int[6]
			{
				1,
				0,
				0,
				3,
				2,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 786,
			MidiNumbers = new int[6]
			{
				0,
				0,
				64,
				67,
				71,
				78
			},
			key = "E",
			suffix = "madd9",
			Fingers = new int[6]
			{
				0,
				0,
				3,
				1,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 787,
			MidiNumbers = new int[6]
			{
				40,
				47,
				52,
				55,
				61,
				64
			},
			key = "E",
			suffix = "m6",
			Fingers = new int[6]
			{
				0,
				1,
				2,
				0,
				3,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 788,
			MidiNumbers = new int[6]
			{
				0,
				0,
				52,
				59,
				61,
				67
			},
			key = "E",
			suffix = "m6",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				1,
				2
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 789,
			MidiNumbers = new int[6]
			{
				0,
				52,
				55,
				61,
				64,
				0
			},
			key = "E",
			suffix = "m6",
			Fingers = new int[6]
			{
				0,
				3,
				1,
				2,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 790,
			MidiNumbers = new int[6]
			{
				0,
				52,
				59,
				61,
				67,
				0
			},
			key = "E",
			suffix = "m6",
			Fingers = new int[6]
			{
				0,
				2,
				4,
				1,
				3,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 791,
			MidiNumbers = new int[6]
			{
				52,
				59,
				64,
				67,
				73,
				76
			},
			key = "E",
			suffix = "m6",
			Fingers = new int[6]
			{
				1,
				2,
				3,
				1,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 792,
			MidiNumbers = new int[6]
			{
				0,
				52,
				55,
				61,
				64,
				71
			},
			key = "E",
			suffix = "m6",
			Fingers = new int[6]
			{
				0,
				3,
				1,
				2,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 793,
			MidiNumbers = new int[6]
			{
				52,
				0,
				61,
				67,
				71,
				0
			},
			key = "E",
			suffix = "m6",
			Fingers = new int[6]
			{
				2,
				0,
				1,
				3,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 794,
			MidiNumbers = new int[6]
			{
				0,
				52,
				0,
				61,
				67,
				71
			},
			key = "E",
			suffix = "m6",
			Fingers = new int[6]
			{
				0,
				2,
				0,
				1,
				4,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 795,
			MidiNumbers = new int[6]
			{
				0,
				52,
				55,
				60,
				64,
				71
			},
			key = "E",
			suffix = "m♭6",
			Fingers = new int[6]
			{
				0,
				3,
				1,
				1,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 796,
			MidiNumbers = new int[6]
			{
				52,
				0,
				60,
				67,
				71,
				0
			},
			key = "E",
			suffix = "m♭6",
			Fingers = new int[6]
			{
				2,
				0,
				1,
				3,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 797,
			MidiNumbers = new int[6]
			{
				0,
				52,
				58,
				62,
				67,
				0
			},
			key = "E",
			suffix = "m7♭5",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				2,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 798,
			MidiNumbers = new int[6]
			{
				0,
				0,
				52,
				58,
				62,
				67
			},
			key = "E",
			suffix = "m7♭5",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				2,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 799,
			MidiNumbers = new int[6]
			{
				40,
				46,
				52,
				58,
				62,
				67
			},
			key = "E",
			suffix = "m7♭5",
			Fingers = new int[6]
			{
				0,
				1,
				2,
				3,
				4,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 800,
			MidiNumbers = new int[6]
			{
				52,
				0,
				62,
				67,
				70,
				0
			},
			key = "E",
			suffix = "m7♭5",
			Fingers = new int[6]
			{
				2,
				0,
				3,
				4,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 801,
			MidiNumbers = new int[6]
			{
				52,
				58,
				64,
				70,
				74,
				79
			},
			key = "E",
			suffix = "m7♭5",
			Fingers = new int[6]
			{
				1,
				2,
				3,
				4,
				4,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 802,
			MidiNumbers = new int[6]
			{
				0,
				52,
				0,
				62,
				67,
				70
			},
			key = "E",
			suffix = "m7♭5",
			Fingers = new int[6]
			{
				0,
				2,
				0,
				3,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 803,
			MidiNumbers = new int[6]
			{
				52,
				58,
				62,
				67,
				74,
				76
			},
			key = "E",
			suffix = "m7♭5",
			Fingers = new int[6]
			{
				1,
				2,
				1,
				1,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 804,
			MidiNumbers = new int[6]
			{
				0,
				52,
				55,
				62,
				66,
				0
			},
			key = "E",
			suffix = "m9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 805,
			MidiNumbers = new int[6]
			{
				40,
				47,
				52,
				55,
				62,
				66
			},
			key = "E",
			suffix = "m9",
			Fingers = new int[6]
			{
				0,
				1,
				2,
				0,
				4,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 806,
			MidiNumbers = new int[6]
			{
				52,
				59,
				62,
				67,
				71,
				78
			},
			key = "E",
			suffix = "m9",
			Fingers = new int[6]
			{
				1,
				2,
				1,
				1,
				1,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 807,
			MidiNumbers = new int[6]
			{
				0,
				52,
				55,
				62,
				66,
				71
			},
			key = "E",
			suffix = "m9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				3,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 808,
			MidiNumbers = new int[6]
			{
				0,
				0,
				64,
				67,
				74,
				78
			},
			key = "E",
			suffix = "m9",
			Fingers = new int[6]
			{
				0,
				0,
				2,
				1,
				4,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 809,
			MidiNumbers = new int[6]
			{
				52,
				0,
				62,
				67,
				71,
				78
			},
			key = "E",
			suffix = "m9",
			Fingers = new int[6]
			{
				2,
				0,
				3,
				3,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 810,
			MidiNumbers = new int[6]
			{
				0,
				52,
				55,
				62,
				64,
				69
			},
			key = "E",
			suffix = "m11",
			Fingers = new int[6]
			{
				0,
				3,
				1,
				4,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 811,
			MidiNumbers = new int[6]
			{
				0,
				52,
				57,
				62,
				67,
				71
			},
			key = "E",
			suffix = "m11",
			Fingers = new int[6]
			{
				0,
				1,
				1,
				1,
				2,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 812,
			MidiNumbers = new int[6]
			{
				52,
				0,
				62,
				67,
				69,
				0
			},
			key = "E",
			suffix = "m11",
			Fingers = new int[6]
			{
				2,
				0,
				3,
				4,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 813,
			MidiNumbers = new int[6]
			{
				0,
				0,
				52,
				57,
				62,
				67
			},
			key = "E",
			suffix = "m11",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				1,
				2,
				2
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 814,
			MidiNumbers = new int[6]
			{
				0,
				52,
				0,
				62,
				67,
				69
			},
			key = "E",
			suffix = "m11",
			Fingers = new int[6]
			{
				0,
				2,
				0,
				3,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 815,
			MidiNumbers = new int[6]
			{
				0,
				52,
				0,
				62,
				67,
				73
			},
			key = "E",
			suffix = "m13",
			Fingers = new int[6]
			{
				0,
				1,
				0,
				2,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 816,
			MidiNumbers = new int[6]
			{
				52,
				59,
				62,
				67,
				73,
				76
			},
			key = "E",
			suffix = "m13",
			Fingers = new int[6]
			{
				1,
				3,
				1,
				1,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 817,
			MidiNumbers = new int[6]
			{
				52,
				0,
				62,
				67,
				73,
				78
			},
			key = "E",
			suffix = "m13",
			Fingers = new int[6]
			{
				2,
				0,
				3,
				3,
				4,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 818,
			MidiNumbers = new int[6]
			{
				0,
				52,
				59,
				62,
				67,
				73
			},
			key = "E",
			suffix = "m13",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				1,
				2,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 819,
			MidiNumbers = new int[6]
			{
				0,
				52,
				55,
				62,
				66,
				73
			},
			key = "E",
			suffix = "m13",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 820,
			MidiNumbers = new int[6]
			{
				0,
				52,
				55,
				63,
				66,
				0
			},
			key = "E",
			suffix = "mMaj9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				4,
				3,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 821,
			MidiNumbers = new int[6]
			{
				52,
				59,
				63,
				67,
				71,
				78
			},
			key = "E",
			suffix = "mMaj9",
			Fingers = new int[6]
			{
				1,
				3,
				2,
				1,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 822,
			MidiNumbers = new int[6]
			{
				0,
				52,
				56,
				63,
				66,
				0
			},
			key = "E",
			suffix = "Maj9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				4,
				3,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 823,
			MidiNumbers = new int[6]
			{
				0,
				0,
				52,
				56,
				63,
				66
			},
			key = "E",
			suffix = "Maj9",
			Fingers = new int[6]
			{
				0,
				0,
				2,
				1,
				4,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 824,
			MidiNumbers = new int[6]
			{
				0,
				52,
				54,
				59,
				63,
				68
			},
			key = "E",
			suffix = "Maj9",
			Fingers = new int[6]
			{
				0,
				3,
				1,
				1,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 825,
			MidiNumbers = new int[6]
			{
				52,
				56,
				63,
				66,
				71,
				75
			},
			key = "E",
			suffix = "Maj9",
			Fingers = new int[6]
			{
				2,
				1,
				4,
				1,
				3,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 826,
			MidiNumbers = new int[6]
			{
				52,
				0,
				63,
				66,
				71,
				0
			},
			key = "E",
			suffix = "Maj9",
			Fingers = new int[6]
			{
				2,
				0,
				4,
				1,
				3,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 827,
			MidiNumbers = new int[6]
			{
				0,
				52,
				0,
				63,
				68,
				73
			},
			key = "E",
			suffix = "Maj13",
			Fingers = new int[6]
			{
				0,
				1,
				0,
				2,
				4,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 828,
			MidiNumbers = new int[6]
			{
				52,
				56,
				61,
				66,
				71,
				75
			},
			key = "E",
			suffix = "Maj13",
			Fingers = new int[6]
			{
				2,
				1,
				1,
				1,
				3,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 829,
			MidiNumbers = new int[6]
			{
				52,
				0,
				63,
				68,
				73,
				78
			},
			key = "E",
			suffix = "Maj13",
			Fingers = new int[6]
			{
				1,
				0,
				2,
				3,
				4,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 830,
			MidiNumbers = new int[6]
			{
				0,
				52,
				58,
				63,
				68,
				71
			},
			key = "E",
			suffix = "Maj7♯11",
			Fingers = new int[6]
			{
				0,
				1,
				2,
				3,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 831,
			MidiNumbers = new int[6]
			{
				52,
				0,
				63,
				68,
				70,
				0
			},
			key = "E",
			suffix = "Maj7♯11",
			Fingers = new int[6]
			{
				2,
				0,
				3,
				4,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 832,
			MidiNumbers = new int[6]
			{
				0,
				0,
				52,
				58,
				63,
				68
			},
			key = "E",
			suffix = "Maj7♯11",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				2,
				3,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 833,
			MidiNumbers = new int[6]
			{
				0,
				52,
				56,
				62,
				66,
				70
			},
			key = "E",
			suffix = "9♭5",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 834,
			MidiNumbers = new int[6]
			{
				52,
				56,
				62,
				66,
				70,
				0
			},
			key = "E",
			suffix = "9♭5",
			Fingers = new int[6]
			{
				2,
				1,
				3,
				1,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 835,
			MidiNumbers = new int[6]
			{
				52,
				58,
				62,
				67,
				74,
				78
			},
			key = "E",
			suffix = "m9♭5",
			Fingers = new int[6]
			{
				1,
				2,
				1,
				1,
				4,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 836,
			MidiNumbers = new int[6]
			{
				0,
				52,
				56,
				62,
				65,
				72
			},
			key = "E",
			suffix = "+7♭9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 837,
			MidiNumbers = new int[6]
			{
				52,
				0,
				62,
				68,
				72,
				77
			},
			key = "E",
			suffix = "+7♭9",
			Fingers = new int[6]
			{
				1,
				0,
				2,
				3,
				3,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 838,
			MidiNumbers = new int[6]
			{
				0,
				52,
				56,
				62,
				67,
				72
			},
			key = "E",
			suffix = "+7♯9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				4,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 839,
			MidiNumbers = new int[6]
			{
				52,
				0,
				62,
				68,
				72,
				79
			},
			key = "E",
			suffix = "+7♯9",
			Fingers = new int[6]
			{
				1,
				0,
				2,
				3,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 840,
			MidiNumbers = new int[6]
			{
				41,
				48,
				53,
				57,
				60,
				65
			},
			key = "F",
			suffix = string.Empty,
			Fingers = new int[6]
			{
				1,
				3,
				4,
				2,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 841,
			MidiNumbers = new int[6]
			{
				0,
				53,
				60,
				65,
				69,
				72
			},
			key = "F",
			suffix = string.Empty,
			Fingers = new int[6]
			{
				0,
				1,
				3,
				3,
				3,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 842,
			MidiNumbers = new int[6]
			{
				0,
				53,
				57,
				60,
				65,
				69
			},
			key = "F",
			suffix = string.Empty,
			Fingers = new int[6]
			{
				0,
				4,
				3,
				1,
				2,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 843,
			MidiNumbers = new int[6]
			{
				0,
				0,
				53,
				57,
				60,
				65
			},
			key = "F",
			suffix = string.Empty,
			Fingers = new int[6]
			{
				0,
				0,
				3,
				2,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 844,
			MidiNumbers = new int[6]
			{
				53,
				57,
				60,
				65,
				69,
				77
			},
			key = "F",
			suffix = string.Empty,
			Fingers = new int[6]
			{
				3,
				2,
				1,
				1,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 845,
			MidiNumbers = new int[6]
			{
				0,
				0,
				53,
				60,
				65,
				69
			},
			key = "F",
			suffix = string.Empty,
			Fingers = new int[6]
			{
				0,
				0,
				1,
				2,
				4,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 846,
			MidiNumbers = new int[6]
			{
				41,
				48,
				53,
				56,
				60,
				65
			},
			key = "F",
			suffix = "m",
			Fingers = new int[6]
			{
				1,
				3,
				4,
				1,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 847,
			MidiNumbers = new int[6]
			{
				0,
				53,
				60,
				65,
				68,
				72
			},
			key = "F",
			suffix = "m",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				4,
				2,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 848,
			MidiNumbers = new int[6]
			{
				0,
				0,
				53,
				60,
				65,
				68
			},
			key = "F",
			suffix = "m",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				4,
				2
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 849,
			MidiNumbers = new int[6]
			{
				0,
				0,
				53,
				56,
				60,
				65
			},
			key = "F",
			suffix = "m",
			Fingers = new int[6]
			{
				0,
				0,
				3,
				1,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 850,
			MidiNumbers = new int[6]
			{
				41,
				48,
				51,
				57,
				60,
				65
			},
			key = "F",
			suffix = "7",
			Fingers = new int[6]
			{
				1,
				3,
				1,
				2,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 851,
			MidiNumbers = new int[6]
			{
				0,
				53,
				60,
				63,
				69,
				72
			},
			key = "F",
			suffix = "7",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				1,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 852,
			MidiNumbers = new int[6]
			{
				41,
				0,
				51,
				57,
				60,
				0
			},
			key = "F",
			suffix = "7",
			Fingers = new int[6]
			{
				1,
				0,
				2,
				4,
				3,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 853,
			MidiNumbers = new int[6]
			{
				0,
				0,
				53,
				60,
				63,
				69
			},
			key = "F",
			suffix = "7",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				2,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 854,
			MidiNumbers = new int[6]
			{
				0,
				53,
				57,
				63,
				65,
				0
			},
			key = "F",
			suffix = "7",
			Fingers = new int[6]
			{
				0,
				3,
				2,
				4,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 855,
			MidiNumbers = new int[6]
			{
				0,
				53,
				60,
				65,
				67,
				72
			},
			key = "F",
			suffix = "sus2",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				4,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 856,
			MidiNumbers = new int[6]
			{
				0,
				0,
				53,
				60,
				65,
				67
			},
			key = "F",
			suffix = "sus2",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				4,
				2
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 857,
			MidiNumbers = new int[6]
			{
				0,
				53,
				60,
				67,
				0,
				0
			},
			key = "F",
			suffix = "sus2",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				4,
				0,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 858,
			MidiNumbers = new int[6]
			{
				0,
				0,
				65,
				67,
				72,
				79
			},
			key = "F",
			suffix = "sus2",
			Fingers = new int[6]
			{
				0,
				0,
				3,
				1,
				2,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 859,
			MidiNumbers = new int[6]
			{
				41,
				48,
				53,
				58,
				60,
				65
			},
			key = "F",
			suffix = "sus4",
			Fingers = new int[6]
			{
				1,
				3,
				3,
				3,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 860,
			MidiNumbers = new int[6]
			{
				0,
				53,
				60,
				65,
				70,
				72
			},
			key = "F",
			suffix = "sus4",
			Fingers = new int[6]
			{
				0,
				1,
				2,
				3,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 861,
			MidiNumbers = new int[6]
			{
				0,
				53,
				58,
				60,
				65,
				0
			},
			key = "F",
			suffix = "sus4",
			Fingers = new int[6]
			{
				0,
				3,
				4,
				1,
				2,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 862,
			MidiNumbers = new int[6]
			{
				0,
				0,
				53,
				60,
				65,
				70
			},
			key = "F",
			suffix = "sus4",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				2,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 863,
			MidiNumbers = new int[6]
			{
				0,
				0,
				53,
				58,
				60,
				65
			},
			key = "F",
			suffix = "sus4",
			Fingers = new int[6]
			{
				0,
				0,
				3,
				4,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 864,
			MidiNumbers = new int[6]
			{
				41,
				48,
				51,
				56,
				60,
				65
			},
			key = "F",
			suffix = "m7",
			Fingers = new int[6]
			{
				1,
				3,
				1,
				1,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 865,
			MidiNumbers = new int[6]
			{
				0,
				53,
				60,
				63,
				68,
				72
			},
			key = "F",
			suffix = "m7",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				1,
				2,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 866,
			MidiNumbers = new int[6]
			{
				41,
				0,
				51,
				56,
				60,
				0
			},
			key = "F",
			suffix = "m7",
			Fingers = new int[6]
			{
				1,
				0,
				2,
				3,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 867,
			MidiNumbers = new int[6]
			{
				0,
				53,
				56,
				63,
				68,
				0
			},
			key = "F",
			suffix = "m7",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 868,
			MidiNumbers = new int[6]
			{
				41,
				48,
				53,
				56,
				63,
				65
			},
			key = "F",
			suffix = "m7",
			Fingers = new int[6]
			{
				1,
				2,
				3,
				1,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 869,
			MidiNumbers = new int[6]
			{
				0,
				53,
				56,
				63,
				65,
				0
			},
			key = "F",
			suffix = "m7",
			Fingers = new int[6]
			{
				0,
				3,
				1,
				4,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 870,
			MidiNumbers = new int[6]
			{
				0,
				0,
				53,
				60,
				63,
				68
			},
			key = "F",
			suffix = "m7",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				4,
				2,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 871,
			MidiNumbers = new int[6]
			{
				0,
				0,
				53,
				57,
				60,
				64
			},
			key = "F",
			suffix = "Maj7",
			Fingers = new int[6]
			{
				0,
				0,
				3,
				2,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 872,
			MidiNumbers = new int[6]
			{
				0,
				53,
				60,
				64,
				69,
				72
			},
			key = "F",
			suffix = "Maj7",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				2,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 873,
			MidiNumbers = new int[6]
			{
				0,
				0,
				53,
				60,
				64,
				69
			},
			key = "F",
			suffix = "Maj7",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				2,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 874,
			MidiNumbers = new int[6]
			{
				41,
				0,
				52,
				57,
				60,
				64
			},
			key = "F",
			suffix = "Maj7",
			Fingers = new int[6]
			{
				1,
				0,
				3,
				4,
				2,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 875,
			MidiNumbers = new int[6]
			{
				0,
				53,
				57,
				60,
				64,
				0
			},
			key = "F",
			suffix = "Maj7",
			Fingers = new int[6]
			{
				0,
				4,
				3,
				1,
				2,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 876,
			MidiNumbers = new int[6]
			{
				0,
				0,
				65,
				69,
				72,
				76
			},
			key = "F",
			suffix = "Maj7",
			Fingers = new int[6]
			{
				0,
				0,
				4,
				3,
				2,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 877,
			MidiNumbers = new int[6]
			{
				41,
				0,
				52,
				57,
				60,
				0
			},
			key = "F",
			suffix = "Maj7",
			Fingers = new int[6]
			{
				1,
				0,
				3,
				4,
				2,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 878,
			MidiNumbers = new int[6]
			{
				41,
				48,
				52,
				57,
				60,
				65
			},
			key = "F",
			suffix = "Maj7",
			Fingers = new int[6]
			{
				1,
				4,
				2,
				3,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 879,
			MidiNumbers = new int[6]
			{
				41,
				48,
				53,
				0,
				0,
				0
			},
			key = "F",
			suffix = "5",
			Fingers = new int[6]
			{
				1,
				3,
				4,
				0,
				0,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 880,
			MidiNumbers = new int[6]
			{
				0,
				53,
				60,
				65,
				0,
				0
			},
			key = "F",
			suffix = "5",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				4,
				0,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 881,
			MidiNumbers = new int[6]
			{
				0,
				0,
				0,
				65,
				72,
				77
			},
			key = "F",
			suffix = "5",
			Fingers = new int[6]
			{
				0,
				0,
				0,
				1,
				4,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 882,
			MidiNumbers = new int[6]
			{
				0,
				0,
				53,
				60,
				65,
				0
			},
			key = "F",
			suffix = "5",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 883,
			MidiNumbers = new int[6]
			{
				0,
				53,
				57,
				61,
				65,
				0
			},
			key = "F",
			suffix = "+",
			Fingers = new int[6]
			{
				0,
				4,
				3,
				1,
				2,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 884,
			MidiNumbers = new int[6]
			{
				0,
				0,
				53,
				57,
				61,
				65
			},
			key = "F",
			suffix = "+",
			Fingers = new int[6]
			{
				0,
				0,
				4,
				2,
				3,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 885,
			MidiNumbers = new int[6]
			{
				53,
				57,
				61,
				65,
				0,
				0
			},
			key = "F",
			suffix = "+",
			Fingers = new int[6]
			{
				4,
				3,
				2,
				1,
				0,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 886,
			MidiNumbers = new int[6]
			{
				41,
				49,
				53,
				57,
				0,
				0
			},
			key = "F",
			suffix = "+",
			Fingers = new int[6]
			{
				1,
				4,
				3,
				2,
				0,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 887,
			MidiNumbers = new int[6]
			{
				0,
				53,
				0,
				65,
				69,
				73
			},
			key = "F",
			suffix = "+",
			Fingers = new int[6]
			{
				0,
				1,
				0,
				3,
				4,
				2
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 888,
			MidiNumbers = new int[6]
			{
				0,
				53,
				59,
				65,
				68,
				0
			},
			key = "F",
			suffix = "°",
			Fingers = new int[6]
			{
				0,
				1,
				2,
				4,
				3,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 889,
			MidiNumbers = new int[6]
			{
				53,
				0,
				65,
				68,
				71,
				0
			},
			key = "F",
			suffix = "°",
			Fingers = new int[6]
			{
				2,
				0,
				4,
				3,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 890,
			MidiNumbers = new int[6]
			{
				41,
				47,
				53,
				56,
				0,
				0
			},
			key = "F",
			suffix = "°",
			Fingers = new int[6]
			{
				1,
				3,
				4,
				2,
				0,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 891,
			MidiNumbers = new int[6]
			{
				0,
				0,
				53,
				57,
				62,
				65
			},
			key = "F",
			suffix = "6",
			Fingers = new int[6]
			{
				0,
				0,
				3,
				2,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 892,
			MidiNumbers = new int[6]
			{
				0,
				53,
				60,
				65,
				69,
				74
			},
			key = "F",
			suffix = "6",
			Fingers = new int[6]
			{
				0,
				1,
				2,
				3,
				4,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 893,
			MidiNumbers = new int[6]
			{
				0,
				0,
				53,
				60,
				62,
				69
			},
			key = "F",
			suffix = "6",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 894,
			MidiNumbers = new int[6]
			{
				53,
				0,
				62,
				69,
				72,
				0
			},
			key = "F",
			suffix = "6",
			Fingers = new int[6]
			{
				2,
				0,
				1,
				4,
				3,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 895,
			MidiNumbers = new int[6]
			{
				41,
				48,
				51,
				57,
				60,
				67
			},
			key = "F",
			suffix = "9",
			Fingers = new int[6]
			{
				1,
				3,
				1,
				2,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 896,
			MidiNumbers = new int[6]
			{
				41,
				0,
				51,
				55,
				60,
				65
			},
			key = "F",
			suffix = "9",
			Fingers = new int[6]
			{
				1,
				0,
				2,
				0,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 897,
			MidiNumbers = new int[6]
			{
				0,
				53,
				57,
				63,
				67,
				0
			},
			key = "F",
			suffix = "9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 898,
			MidiNumbers = new int[6]
			{
				0,
				53,
				57,
				63,
				67,
				72
			},
			key = "F",
			suffix = "9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				3,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 899,
			MidiNumbers = new int[6]
			{
				53,
				0,
				63,
				67,
				69,
				0
			},
			key = "F",
			suffix = "9",
			Fingers = new int[6]
			{
				3,
				0,
				4,
				2,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 900,
			MidiNumbers = new int[6]
			{
				0,
				0,
				53,
				57,
				63,
				67
			},
			key = "F",
			suffix = "9",
			Fingers = new int[6]
			{
				0,
				0,
				2,
				1,
				4,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 901,
			MidiNumbers = new int[6]
			{
				53,
				57,
				63,
				67,
				72,
				0
			},
			key = "F",
			suffix = "9",
			Fingers = new int[6]
			{
				2,
				1,
				3,
				1,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 902,
			MidiNumbers = new int[6]
			{
				41,
				48,
				51,
				58,
				60,
				67
			},
			key = "F",
			suffix = "11",
			Fingers = new int[6]
			{
				1,
				2,
				1,
				3,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 903,
			MidiNumbers = new int[6]
			{
				0,
				53,
				57,
				63,
				65,
				70
			},
			key = "F",
			suffix = "11",
			Fingers = new int[6]
			{
				0,
				3,
				2,
				4,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 904,
			MidiNumbers = new int[6]
			{
				0,
				53,
				58,
				63,
				69,
				72
			},
			key = "F",
			suffix = "11",
			Fingers = new int[6]
			{
				0,
				1,
				1,
				1,
				3,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 905,
			MidiNumbers = new int[6]
			{
				0,
				0,
				53,
				58,
				63,
				69
			},
			key = "F",
			suffix = "11",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				1,
				2,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 906,
			MidiNumbers = new int[6]
			{
				41,
				45,
				51,
				57,
				62,
				67
			},
			key = "F",
			suffix = "13",
			Fingers = new int[6]
			{
				1,
				0,
				2,
				3,
				4,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 907,
			MidiNumbers = new int[6]
			{
				0,
				53,
				57,
				63,
				67,
				74
			},
			key = "F",
			suffix = "13",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 908,
			MidiNumbers = new int[6]
			{
				53,
				0,
				63,
				67,
				69,
				74
			},
			key = "F",
			suffix = "13",
			Fingers = new int[6]
			{
				3,
				0,
				4,
				2,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 909,
			MidiNumbers = new int[6]
			{
				41,
				0,
				51,
				57,
				62,
				67
			},
			key = "F",
			suffix = "13",
			Fingers = new int[6]
			{
				1,
				0,
				1,
				1,
				2,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 910,
			MidiNumbers = new int[6]
			{
				41,
				48,
				51,
				57,
				62,
				65
			},
			key = "F",
			suffix = "13",
			Fingers = new int[6]
			{
				1,
				3,
				1,
				2,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 911,
			MidiNumbers = new int[6]
			{
				41,
				48,
				51,
				58,
				60,
				65
			},
			key = "F",
			suffix = "7sus4",
			Fingers = new int[6]
			{
				1,
				3,
				1,
				4,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 912,
			MidiNumbers = new int[6]
			{
				0,
				53,
				60,
				63,
				70,
				72
			},
			key = "F",
			suffix = "7sus4",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				1,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 913,
			MidiNumbers = new int[6]
			{
				53,
				60,
				63,
				70,
				72,
				77
			},
			key = "F",
			suffix = "7sus4",
			Fingers = new int[6]
			{
				1,
				3,
				1,
				4,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 914,
			MidiNumbers = new int[6]
			{
				0,
				53,
				58,
				63,
				65,
				70
			},
			key = "F",
			suffix = "7sus4",
			Fingers = new int[6]
			{
				0,
				2,
				3,
				4,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 915,
			MidiNumbers = new int[6]
			{
				0,
				0,
				53,
				60,
				63,
				70
			},
			key = "F",
			suffix = "7sus4",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				2,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 916,
			MidiNumbers = new int[6]
			{
				0,
				53,
				58,
				63,
				67,
				72
			},
			key = "F",
			suffix = "9sus4",
			Fingers = new int[6]
			{
				0,
				1,
				1,
				1,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 917,
			MidiNumbers = new int[6]
			{
				53,
				0,
				63,
				67,
				70,
				0
			},
			key = "F",
			suffix = "9sus4",
			Fingers = new int[6]
			{
				3,
				0,
				4,
				2,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 918,
			MidiNumbers = new int[6]
			{
				53,
				0,
				63,
				67,
				70,
				75
			},
			key = "F",
			suffix = "9sus4",
			Fingers = new int[6]
			{
				3,
				0,
				4,
				2,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 919,
			MidiNumbers = new int[6]
			{
				0,
				53,
				58,
				63,
				67,
				74
			},
			key = "F",
			suffix = "13sus4",
			Fingers = new int[6]
			{
				0,
				1,
				1,
				1,
				1,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 920,
			MidiNumbers = new int[6]
			{
				41,
				0,
				51,
				58,
				62,
				67
			},
			key = "F",
			suffix = "13sus4",
			Fingers = new int[6]
			{
				1,
				0,
				2,
				4,
				4,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 921,
			MidiNumbers = new int[6]
			{
				41,
				47,
				50,
				56,
				0,
				0
			},
			key = "F",
			suffix = "°7",
			Fingers = new int[6]
			{
				1,
				3,
				0,
				2,
				0,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 922,
			MidiNumbers = new int[6]
			{
				0,
				53,
				59,
				62,
				68,
				0
			},
			key = "F",
			suffix = "°7",
			Fingers = new int[6]
			{
				0,
				2,
				3,
				1,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 923,
			MidiNumbers = new int[6]
			{
				0,
				0,
				53,
				59,
				62,
				68
			},
			key = "F",
			suffix = "°7",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				2,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 924,
			MidiNumbers = new int[6]
			{
				41,
				0,
				50,
				56,
				59,
				0
			},
			key = "F",
			suffix = "°7",
			Fingers = new int[6]
			{
				1,
				0,
				0,
				3,
				0,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 925,
			MidiNumbers = new int[6]
			{
				41,
				47,
				53,
				56,
				62,
				65
			},
			key = "F",
			suffix = "°7",
			Fingers = new int[6]
			{
				1,
				2,
				3,
				1,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 926,
			MidiNumbers = new int[6]
			{
				53,
				0,
				62,
				68,
				71,
				0
			},
			key = "F",
			suffix = "°7",
			Fingers = new int[6]
			{
				3,
				0,
				1,
				4,
				2,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 927,
			MidiNumbers = new int[6]
			{
				0,
				53,
				61,
				63,
				69,
				73
			},
			key = "F",
			suffix = "+7",
			Fingers = new int[6]
			{
				0,
				1,
				4,
				1,
				3,
				2
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 928,
			MidiNumbers = new int[6]
			{
				41,
				0,
				51,
				57,
				61,
				0
			},
			key = "F",
			suffix = "+7",
			Fingers = new int[6]
			{
				1,
				0,
				2,
				3,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 929,
			MidiNumbers = new int[6]
			{
				0,
				0,
				53,
				61,
				63,
				69
			},
			key = "F",
			suffix = "+7",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				4,
				2,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 930,
			MidiNumbers = new int[6]
			{
				0,
				53,
				0,
				63,
				69,
				73
			},
			key = "F",
			suffix = "+7",
			Fingers = new int[6]
			{
				0,
				1,
				0,
				2,
				4,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 931,
			MidiNumbers = new int[6]
			{
				0,
				53,
				59,
				63,
				69,
				0
			},
			key = "F",
			suffix = "7♭5",
			Fingers = new int[6]
			{
				0,
				1,
				2,
				1,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 932,
			MidiNumbers = new int[6]
			{
				53,
				0,
				63,
				69,
				71,
				0
			},
			key = "F",
			suffix = "7♭5",
			Fingers = new int[6]
			{
				2,
				0,
				3,
				4,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 933,
			MidiNumbers = new int[6]
			{
				0,
				0,
				53,
				59,
				63,
				69
			},
			key = "F",
			suffix = "7♭5",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				2,
				2,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 934,
			MidiNumbers = new int[6]
			{
				0,
				53,
				59,
				63,
				69,
				75
			},
			key = "F",
			suffix = "7♭5",
			Fingers = new int[6]
			{
				0,
				1,
				2,
				1,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 935,
			MidiNumbers = new int[6]
			{
				0,
				53,
				57,
				63,
				68,
				0
			},
			key = "F",
			suffix = "7♯9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 936,
			MidiNumbers = new int[6]
			{
				41,
				48,
				51,
				57,
				60,
				68
			},
			key = "F",
			suffix = "7♯9",
			Fingers = new int[6]
			{
				1,
				3,
				1,
				2,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 937,
			MidiNumbers = new int[6]
			{
				0,
				0,
				53,
				57,
				63,
				68
			},
			key = "F",
			suffix = "7♯9",
			Fingers = new int[6]
			{
				0,
				0,
				2,
				1,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 938,
			MidiNumbers = new int[6]
			{
				0,
				53,
				57,
				63,
				68,
				71
			},
			key = "F",
			suffix = "7♭5♯9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 939,
			MidiNumbers = new int[6]
			{
				41,
				47,
				51,
				57,
				60,
				65
			},
			key = "F",
			suffix = "7♯11",
			Fingers = new int[6]
			{
				1,
				2,
				1,
				3,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 940,
			MidiNumbers = new int[6]
			{
				0,
				53,
				57,
				63,
				66,
				0
			},
			key = "F",
			suffix = "7♭9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 941,
			MidiNumbers = new int[6]
			{
				41,
				48,
				51,
				57,
				60,
				66
			},
			key = "F",
			suffix = "7♭9",
			Fingers = new int[6]
			{
				1,
				4,
				1,
				2,
				1,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 942,
			MidiNumbers = new int[6]
			{
				0,
				53,
				57,
				63,
				66,
				72
			},
			key = "F",
			suffix = "7♭9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 943,
			MidiNumbers = new int[6]
			{
				0,
				0,
				53,
				57,
				63,
				66
			},
			key = "F",
			suffix = "7♭9",
			Fingers = new int[6]
			{
				0,
				0,
				2,
				1,
				3,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 944,
			MidiNumbers = new int[6]
			{
				0,
				53,
				57,
				63,
				67,
				73
			},
			key = "F",
			suffix = "+9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 945,
			MidiNumbers = new int[6]
			{
				41,
				0,
				51,
				57,
				61,
				67
			},
			key = "F",
			suffix = "+9",
			Fingers = new int[6]
			{
				1,
				0,
				2,
				3,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 946,
			MidiNumbers = new int[6]
			{
				0,
				53,
				60,
				64,
				68,
				72
			},
			key = "F",
			suffix = "mMaj7",
			Fingers = new int[6]
			{
				0,
				1,
				4,
				2,
				3,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 947,
			MidiNumbers = new int[6]
			{
				41,
				0,
				52,
				56,
				60,
				0
			},
			key = "F",
			suffix = "mMaj7",
			Fingers = new int[6]
			{
				1,
				0,
				4,
				2,
				3,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 948,
			MidiNumbers = new int[6]
			{
				41,
				48,
				52,
				56,
				60,
				65
			},
			key = "F",
			suffix = "mMaj7",
			Fingers = new int[6]
			{
				1,
				3,
				2,
				1,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 949,
			MidiNumbers = new int[6]
			{
				0,
				0,
				53,
				60,
				64,
				68
			},
			key = "F",
			suffix = "mMaj7",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				4,
				2
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 950,
			MidiNumbers = new int[6]
			{
				0,
				0,
				65,
				68,
				72,
				76
			},
			key = "F",
			suffix = "mMaj7",
			Fingers = new int[6]
			{
				0,
				0,
				4,
				2,
				3,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 951,
			MidiNumbers = new int[6]
			{
				0,
				0,
				53,
				57,
				60,
				67
			},
			key = "F",
			suffix = "add9",
			Fingers = new int[6]
			{
				0,
				0,
				3,
				2,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 952,
			MidiNumbers = new int[6]
			{
				0,
				53,
				60,
				67,
				69,
				72
			},
			key = "F",
			suffix = "add9",
			Fingers = new int[6]
			{
				0,
				1,
				2,
				4,
				3,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 953,
			MidiNumbers = new int[6]
			{
				41,
				0,
				0,
				57,
				60,
				67
			},
			key = "F",
			suffix = "add9",
			Fingers = new int[6]
			{
				1,
				0,
				0,
				3,
				2,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 954,
			MidiNumbers = new int[6]
			{
				0,
				0,
				53,
				56,
				60,
				67
			},
			key = "F",
			suffix = "madd9",
			Fingers = new int[6]
			{
				0,
				0,
				3,
				1,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 955,
			MidiNumbers = new int[6]
			{
				41,
				48,
				53,
				56,
				62,
				65
			},
			key = "F",
			suffix = "m6",
			Fingers = new int[6]
			{
				1,
				2,
				3,
				1,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 956,
			MidiNumbers = new int[6]
			{
				0,
				0,
				53,
				60,
				62,
				68
			},
			key = "F",
			suffix = "m6",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				1,
				2
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 957,
			MidiNumbers = new int[6]
			{
				0,
				53,
				56,
				62,
				65,
				0
			},
			key = "F",
			suffix = "m6",
			Fingers = new int[6]
			{
				0,
				4,
				1,
				3,
				2,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 958,
			MidiNumbers = new int[6]
			{
				41,
				0,
				50,
				56,
				60,
				0
			},
			key = "F",
			suffix = "m6",
			Fingers = new int[6]
			{
				1,
				0,
				0,
				2,
				3,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 959,
			MidiNumbers = new int[6]
			{
				0,
				53,
				60,
				62,
				68,
				0
			},
			key = "F",
			suffix = "m6",
			Fingers = new int[6]
			{
				0,
				2,
				4,
				1,
				3,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 960,
			MidiNumbers = new int[6]
			{
				0,
				53,
				56,
				62,
				65,
				72
			},
			key = "F",
			suffix = "m6",
			Fingers = new int[6]
			{
				0,
				3,
				1,
				2,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 961,
			MidiNumbers = new int[6]
			{
				53,
				0,
				62,
				68,
				72,
				0
			},
			key = "F",
			suffix = "m6",
			Fingers = new int[6]
			{
				2,
				0,
				1,
				3,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 962,
			MidiNumbers = new int[6]
			{
				0,
				53,
				0,
				62,
				68,
				72
			},
			key = "F",
			suffix = "m6",
			Fingers = new int[6]
			{
				0,
				2,
				0,
				1,
				4,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 963,
			MidiNumbers = new int[6]
			{
				0,
				53,
				56,
				61,
				65,
				72
			},
			key = "F",
			suffix = "m♭6",
			Fingers = new int[6]
			{
				0,
				3,
				1,
				1,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 964,
			MidiNumbers = new int[6]
			{
				53,
				0,
				61,
				68,
				72,
				0
			},
			key = "F",
			suffix = "m♭6",
			Fingers = new int[6]
			{
				2,
				0,
				1,
				3,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 965,
			MidiNumbers = new int[6]
			{
				0,
				53,
				59,
				63,
				68,
				0
			},
			key = "F",
			suffix = "m7♭5",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				2,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 966,
			MidiNumbers = new int[6]
			{
				0,
				0,
				53,
				59,
				63,
				68
			},
			key = "F",
			suffix = "m7♭5",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				2,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 967,
			MidiNumbers = new int[6]
			{
				41,
				0,
				51,
				56,
				59,
				0
			},
			key = "F",
			suffix = "m7♭5",
			Fingers = new int[6]
			{
				1,
				0,
				2,
				3,
				0,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 968,
			MidiNumbers = new int[6]
			{
				41,
				47,
				53,
				59,
				63,
				68
			},
			key = "F",
			suffix = "m7♭5",
			Fingers = new int[6]
			{
				1,
				2,
				3,
				4,
				4,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 969,
			MidiNumbers = new int[6]
			{
				53,
				0,
				63,
				68,
				71,
				0
			},
			key = "F",
			suffix = "m7♭5",
			Fingers = new int[6]
			{
				2,
				0,
				3,
				4,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 970,
			MidiNumbers = new int[6]
			{
				0,
				53,
				0,
				63,
				68,
				71
			},
			key = "F",
			suffix = "m7♭5",
			Fingers = new int[6]
			{
				0,
				2,
				0,
				3,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 971,
			MidiNumbers = new int[6]
			{
				41,
				47,
				51,
				56,
				63,
				65
			},
			key = "F",
			suffix = "m7♭5",
			Fingers = new int[6]
			{
				1,
				2,
				1,
				1,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 972,
			MidiNumbers = new int[6]
			{
				41,
				48,
				51,
				56,
				60,
				67
			},
			key = "F",
			suffix = "m9",
			Fingers = new int[6]
			{
				1,
				2,
				1,
				1,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 973,
			MidiNumbers = new int[6]
			{
				0,
				53,
				56,
				63,
				67,
				0
			},
			key = "F",
			suffix = "m9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 974,
			MidiNumbers = new int[6]
			{
				0,
				53,
				56,
				63,
				67,
				72
			},
			key = "F",
			suffix = "m9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				3,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 975,
			MidiNumbers = new int[6]
			{
				0,
				0,
				53,
				56,
				63,
				67
			},
			key = "F",
			suffix = "m9",
			Fingers = new int[6]
			{
				0,
				0,
				2,
				1,
				4,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 976,
			MidiNumbers = new int[6]
			{
				41,
				0,
				51,
				56,
				60,
				67
			},
			key = "F",
			suffix = "m9",
			Fingers = new int[6]
			{
				2,
				0,
				3,
				3,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 977,
			MidiNumbers = new int[6]
			{
				0,
				53,
				56,
				63,
				65,
				70
			},
			key = "F",
			suffix = "m11",
			Fingers = new int[6]
			{
				0,
				3,
				1,
				4,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 978,
			MidiNumbers = new int[6]
			{
				0,
				53,
				58,
				63,
				68,
				72
			},
			key = "F",
			suffix = "m11",
			Fingers = new int[6]
			{
				0,
				1,
				1,
				1,
				2,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 979,
			MidiNumbers = new int[6]
			{
				53,
				0,
				63,
				68,
				70,
				0
			},
			key = "F",
			suffix = "m11",
			Fingers = new int[6]
			{
				2,
				0,
				3,
				4,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 980,
			MidiNumbers = new int[6]
			{
				0,
				0,
				53,
				58,
				63,
				68
			},
			key = "F",
			suffix = "m11",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				1,
				2,
				2
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 981,
			MidiNumbers = new int[6]
			{
				0,
				53,
				0,
				63,
				68,
				70
			},
			key = "F",
			suffix = "m11",
			Fingers = new int[6]
			{
				0,
				2,
				0,
				3,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 982,
			MidiNumbers = new int[6]
			{
				0,
				53,
				0,
				63,
				68,
				74
			},
			key = "F",
			suffix = "m13",
			Fingers = new int[6]
			{
				0,
				1,
				0,
				2,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 983,
			MidiNumbers = new int[6]
			{
				41,
				48,
				51,
				56,
				62,
				65
			},
			key = "F",
			suffix = "m13",
			Fingers = new int[6]
			{
				1,
				3,
				1,
				1,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 984,
			MidiNumbers = new int[6]
			{
				41,
				0,
				51,
				56,
				62,
				67
			},
			key = "F",
			suffix = "m13",
			Fingers = new int[6]
			{
				2,
				0,
				3,
				3,
				4,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 985,
			MidiNumbers = new int[6]
			{
				0,
				53,
				60,
				63,
				68,
				74
			},
			key = "F",
			suffix = "m13",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				1,
				2,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 986,
			MidiNumbers = new int[6]
			{
				0,
				53,
				56,
				63,
				67,
				74
			},
			key = "F",
			suffix = "m13",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 987,
			MidiNumbers = new int[6]
			{
				0,
				53,
				56,
				64,
				67,
				0
			},
			key = "F",
			suffix = "mMaj9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				4,
				3,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 988,
			MidiNumbers = new int[6]
			{
				41,
				48,
				52,
				56,
				60,
				67
			},
			key = "F",
			suffix = "mMaj9",
			Fingers = new int[6]
			{
				1,
				3,
				2,
				1,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 989,
			MidiNumbers = new int[6]
			{
				0,
				53,
				57,
				64,
				67,
				0
			},
			key = "F",
			suffix = "Maj9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				4,
				3,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 990,
			MidiNumbers = new int[6]
			{
				0,
				0,
				53,
				57,
				64,
				67
			},
			key = "F",
			suffix = "Maj9",
			Fingers = new int[6]
			{
				0,
				0,
				2,
				1,
				4,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 991,
			MidiNumbers = new int[6]
			{
				0,
				53,
				55,
				60,
				64,
				69
			},
			key = "F",
			suffix = "Maj9",
			Fingers = new int[6]
			{
				0,
				3,
				1,
				1,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 992,
			MidiNumbers = new int[6]
			{
				53,
				57,
				64,
				67,
				72,
				76
			},
			key = "F",
			suffix = "Maj9",
			Fingers = new int[6]
			{
				2,
				1,
				4,
				1,
				3,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 993,
			MidiNumbers = new int[6]
			{
				53,
				0,
				64,
				67,
				72,
				0
			},
			key = "F",
			suffix = "Maj9",
			Fingers = new int[6]
			{
				2,
				0,
				4,
				1,
				3,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 994,
			MidiNumbers = new int[6]
			{
				0,
				53,
				0,
				64,
				69,
				74
			},
			key = "F",
			suffix = "Maj13",
			Fingers = new int[6]
			{
				0,
				1,
				0,
				2,
				4,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 995,
			MidiNumbers = new int[6]
			{
				53,
				57,
				62,
				67,
				72,
				76
			},
			key = "F",
			suffix = "Maj13",
			Fingers = new int[6]
			{
				2,
				1,
				1,
				1,
				3,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 996,
			MidiNumbers = new int[6]
			{
				41,
				0,
				52,
				57,
				62,
				67
			},
			key = "F",
			suffix = "Maj13",
			Fingers = new int[6]
			{
				1,
				0,
				2,
				3,
				4,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 997,
			MidiNumbers = new int[6]
			{
				0,
				53,
				59,
				64,
				69,
				72
			},
			key = "F",
			suffix = "Maj7♯11",
			Fingers = new int[6]
			{
				0,
				1,
				2,
				3,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 998,
			MidiNumbers = new int[6]
			{
				53,
				0,
				64,
				69,
				71,
				0
			},
			key = "F",
			suffix = "Maj7♯11",
			Fingers = new int[6]
			{
				2,
				0,
				3,
				4,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 999,
			MidiNumbers = new int[6]
			{
				0,
				0,
				53,
				59,
				64,
				69
			},
			key = "F",
			suffix = "Maj7♯11",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				2,
				3,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1000,
			MidiNumbers = new int[6]
			{
				0,
				53,
				57,
				63,
				67,
				71
			},
			key = "F",
			suffix = "9♭5",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1001,
			MidiNumbers = new int[6]
			{
				53,
				57,
				63,
				67,
				71,
				0
			},
			key = "F",
			suffix = "9♭5",
			Fingers = new int[6]
			{
				2,
				1,
				3,
				1,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1002,
			MidiNumbers = new int[6]
			{
				41,
				47,
				51,
				56,
				63,
				67
			},
			key = "F",
			suffix = "m9♭5",
			Fingers = new int[6]
			{
				1,
				2,
				1,
				1,
				4,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1003,
			MidiNumbers = new int[6]
			{
				0,
				53,
				57,
				63,
				66,
				73
			},
			key = "F",
			suffix = "+7♭9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1004,
			MidiNumbers = new int[6]
			{
				41,
				0,
				51,
				57,
				61,
				66
			},
			key = "F",
			suffix = "+7♭9",
			Fingers = new int[6]
			{
				1,
				0,
				2,
				3,
				3,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1005,
			MidiNumbers = new int[6]
			{
				0,
				53,
				57,
				63,
				68,
				73
			},
			key = "F",
			suffix = "+7♯9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				4,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1006,
			MidiNumbers = new int[6]
			{
				41,
				0,
				51,
				57,
				61,
				68
			},
			key = "F",
			suffix = "+7♯9",
			Fingers = new int[6]
			{
				1,
				0,
				2,
				3,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1007,
			MidiNumbers = new int[6]
			{
				42,
				49,
				54,
				58,
				61,
				66
			},
			key = "F♯",
			suffix = string.Empty,
			Fingers = new int[6]
			{
				1,
				3,
				4,
				2,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1008,
			MidiNumbers = new int[6]
			{
				0,
				54,
				61,
				66,
				70,
				73
			},
			key = "F♯",
			suffix = string.Empty,
			Fingers = new int[6]
			{
				0,
				1,
				3,
				3,
				3,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1009,
			MidiNumbers = new int[6]
			{
				0,
				54,
				58,
				61,
				66,
				70
			},
			key = "F♯",
			suffix = string.Empty,
			Fingers = new int[6]
			{
				0,
				4,
				3,
				1,
				2,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1010,
			MidiNumbers = new int[6]
			{
				0,
				0,
				54,
				58,
				61,
				66
			},
			key = "F♯",
			suffix = string.Empty,
			Fingers = new int[6]
			{
				0,
				0,
				3,
				2,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1011,
			MidiNumbers = new int[6]
			{
				54,
				58,
				61,
				66,
				70,
				78
			},
			key = "F♯",
			suffix = string.Empty,
			Fingers = new int[6]
			{
				3,
				2,
				1,
				1,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1012,
			MidiNumbers = new int[6]
			{
				0,
				0,
				54,
				61,
				66,
				70
			},
			key = "F♯",
			suffix = string.Empty,
			Fingers = new int[6]
			{
				0,
				0,
				1,
				2,
				4,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1013,
			MidiNumbers = new int[6]
			{
				42,
				49,
				54,
				57,
				61,
				66
			},
			key = "F♯",
			suffix = "m",
			Fingers = new int[6]
			{
				1,
				3,
				4,
				1,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1014,
			MidiNumbers = new int[6]
			{
				0,
				54,
				61,
				66,
				69,
				73
			},
			key = "F♯",
			suffix = "m",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				4,
				2,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1015,
			MidiNumbers = new int[6]
			{
				0,
				0,
				54,
				61,
				66,
				69
			},
			key = "F♯",
			suffix = "m",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				4,
				2
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1016,
			MidiNumbers = new int[6]
			{
				0,
				0,
				54,
				57,
				61,
				66
			},
			key = "F♯",
			suffix = "m",
			Fingers = new int[6]
			{
				0,
				0,
				3,
				1,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1017,
			MidiNumbers = new int[6]
			{
				42,
				49,
				52,
				58,
				61,
				66
			},
			key = "F♯",
			suffix = "7",
			Fingers = new int[6]
			{
				1,
				3,
				1,
				2,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1018,
			MidiNumbers = new int[6]
			{
				0,
				54,
				61,
				64,
				70,
				73
			},
			key = "F♯",
			suffix = "7",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				1,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1019,
			MidiNumbers = new int[6]
			{
				42,
				0,
				52,
				58,
				61,
				0
			},
			key = "F♯",
			suffix = "7",
			Fingers = new int[6]
			{
				1,
				0,
				2,
				4,
				3,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1020,
			MidiNumbers = new int[6]
			{
				0,
				0,
				54,
				61,
				64,
				70
			},
			key = "F♯",
			suffix = "7",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				2,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1021,
			MidiNumbers = new int[6]
			{
				0,
				54,
				58,
				64,
				66,
				0
			},
			key = "F♯",
			suffix = "7",
			Fingers = new int[6]
			{
				0,
				3,
				2,
				4,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1022,
			MidiNumbers = new int[6]
			{
				0,
				54,
				61,
				64,
				70,
				73
			},
			key = "F♯",
			suffix = "7",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				1,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1023,
			MidiNumbers = new int[6]
			{
				42,
				49,
				54,
				56,
				0,
				0
			},
			key = "F♯",
			suffix = "sus2",
			Fingers = new int[6]
			{
				2,
				3,
				4,
				1,
				0,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1024,
			MidiNumbers = new int[6]
			{
				0,
				0,
				54,
				56,
				61,
				68
			},
			key = "F♯",
			suffix = "sus2",
			Fingers = new int[6]
			{
				0,
				0,
				3,
				1,
				2,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1025,
			MidiNumbers = new int[6]
			{
				42,
				0,
				52,
				56,
				61,
				66
			},
			key = "F♯",
			suffix = "sus2",
			Fingers = new int[6]
			{
				2,
				0,
				3,
				1,
				4,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1026,
			MidiNumbers = new int[6]
			{
				0,
				54,
				61,
				66,
				68,
				73
			},
			key = "F♯",
			suffix = "sus2",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				4,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1027,
			MidiNumbers = new int[6]
			{
				0,
				0,
				54,
				61,
				66,
				68
			},
			key = "F♯",
			suffix = "sus2",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1028,
			MidiNumbers = new int[6]
			{
				0,
				54,
				61,
				68,
				0,
				0
			},
			key = "F♯",
			suffix = "sus2",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				4,
				0,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1029,
			MidiNumbers = new int[6]
			{
				42,
				49,
				54,
				59,
				61,
				66
			},
			key = "F♯",
			suffix = "sus4",
			Fingers = new int[6]
			{
				1,
				2,
				3,
				4,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1030,
			MidiNumbers = new int[6]
			{
				0,
				54,
				61,
				66,
				71,
				73
			},
			key = "F♯",
			suffix = "sus4",
			Fingers = new int[6]
			{
				0,
				1,
				2,
				3,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1031,
			MidiNumbers = new int[6]
			{
				0,
				54,
				59,
				61,
				66,
				0
			},
			key = "F♯",
			suffix = "sus4",
			Fingers = new int[6]
			{
				0,
				3,
				4,
				1,
				2,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1032,
			MidiNumbers = new int[6]
			{
				0,
				0,
				54,
				61,
				66,
				71
			},
			key = "F♯",
			suffix = "sus4",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				4,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1033,
			MidiNumbers = new int[6]
			{
				0,
				0,
				54,
				59,
				61,
				66
			},
			key = "F♯",
			suffix = "sus4",
			Fingers = new int[6]
			{
				0,
				0,
				3,
				4,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1034,
			MidiNumbers = new int[6]
			{
				42,
				49,
				52,
				57,
				61,
				66
			},
			key = "F♯",
			suffix = "m7",
			Fingers = new int[6]
			{
				1,
				3,
				1,
				1,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1035,
			MidiNumbers = new int[6]
			{
				42,
				0,
				52,
				57,
				61,
				0
			},
			key = "F♯",
			suffix = "m7",
			Fingers = new int[6]
			{
				1,
				0,
				2,
				3,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1036,
			MidiNumbers = new int[6]
			{
				0,
				54,
				61,
				64,
				69,
				73
			},
			key = "F♯",
			suffix = "m7",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				1,
				2,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1037,
			MidiNumbers = new int[6]
			{
				0,
				54,
				57,
				64,
				69,
				0
			},
			key = "F♯",
			suffix = "m7",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1038,
			MidiNumbers = new int[6]
			{
				42,
				49,
				54,
				57,
				64,
				66
			},
			key = "F♯",
			suffix = "m7",
			Fingers = new int[6]
			{
				1,
				2,
				3,
				1,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1039,
			MidiNumbers = new int[6]
			{
				0,
				54,
				57,
				64,
				66,
				0
			},
			key = "F♯",
			suffix = "m7",
			Fingers = new int[6]
			{
				0,
				3,
				1,
				4,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1040,
			MidiNumbers = new int[6]
			{
				0,
				0,
				54,
				61,
				64,
				69
			},
			key = "F♯",
			suffix = "m7",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				4,
				2,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1041,
			MidiNumbers = new int[6]
			{
				0,
				0,
				54,
				58,
				61,
				65
			},
			key = "F♯",
			suffix = "Maj7",
			Fingers = new int[6]
			{
				0,
				0,
				4,
				3,
				2,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1042,
			MidiNumbers = new int[6]
			{
				0,
				54,
				61,
				65,
				70,
				73
			},
			key = "F♯",
			suffix = "Maj7",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				2,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1043,
			MidiNumbers = new int[6]
			{
				0,
				0,
				54,
				61,
				65,
				70
			},
			key = "F♯",
			suffix = "Maj7",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				3,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1044,
			MidiNumbers = new int[6]
			{
				0,
				54,
				58,
				61,
				65,
				0
			},
			key = "F♯",
			suffix = "Maj7",
			Fingers = new int[6]
			{
				0,
				4,
				3,
				1,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1045,
			MidiNumbers = new int[6]
			{
				42,
				0,
				53,
				58,
				61,
				0
			},
			key = "F♯",
			suffix = "Maj7",
			Fingers = new int[6]
			{
				1,
				0,
				3,
				4,
				2,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1046,
			MidiNumbers = new int[6]
			{
				42,
				49,
				53,
				58,
				61,
				66
			},
			key = "F♯",
			suffix = "Maj7",
			Fingers = new int[6]
			{
				1,
				4,
				2,
				3,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1047,
			MidiNumbers = new int[6]
			{
				42,
				49,
				54,
				0,
				0,
				0
			},
			key = "F♯",
			suffix = "5",
			Fingers = new int[6]
			{
				1,
				3,
				4,
				0,
				0,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1048,
			MidiNumbers = new int[6]
			{
				0,
				54,
				61,
				66,
				0,
				0
			},
			key = "F♯",
			suffix = "5",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				4,
				0,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1049,
			MidiNumbers = new int[6]
			{
				0,
				0,
				0,
				66,
				73,
				78
			},
			key = "F♯",
			suffix = "5",
			Fingers = new int[6]
			{
				0,
				0,
				0,
				1,
				4,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1050,
			MidiNumbers = new int[6]
			{
				0,
				0,
				54,
				61,
				66,
				0
			},
			key = "F♯",
			suffix = "5",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1051,
			MidiNumbers = new int[6]
			{
				0,
				54,
				58,
				62,
				66,
				0
			},
			key = "F♯",
			suffix = "+",
			Fingers = new int[6]
			{
				0,
				4,
				3,
				1,
				2,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1052,
			MidiNumbers = new int[6]
			{
				0,
				0,
				54,
				58,
				62,
				66
			},
			key = "F♯",
			suffix = "+",
			Fingers = new int[6]
			{
				0,
				0,
				4,
				2,
				3,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1053,
			MidiNumbers = new int[6]
			{
				54,
				58,
				62,
				66,
				0,
				0
			},
			key = "F♯",
			suffix = "+",
			Fingers = new int[6]
			{
				4,
				3,
				2,
				1,
				0,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1054,
			MidiNumbers = new int[6]
			{
				42,
				50,
				54,
				58,
				0,
				0
			},
			key = "F♯",
			suffix = "+",
			Fingers = new int[6]
			{
				1,
				4,
				3,
				2,
				0,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1055,
			MidiNumbers = new int[6]
			{
				0,
				54,
				0,
				66,
				70,
				74
			},
			key = "F♯",
			suffix = "+",
			Fingers = new int[6]
			{
				0,
				1,
				0,
				3,
				4,
				2
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1056,
			MidiNumbers = new int[6]
			{
				0,
				54,
				60,
				66,
				69,
				0
			},
			key = "F♯",
			suffix = "°",
			Fingers = new int[6]
			{
				0,
				1,
				2,
				4,
				3,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1057,
			MidiNumbers = new int[6]
			{
				42,
				0,
				54,
				57,
				60,
				0
			},
			key = "F♯",
			suffix = "°",
			Fingers = new int[6]
			{
				2,
				0,
				4,
				3,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1058,
			MidiNumbers = new int[6]
			{
				42,
				48,
				54,
				57,
				0,
				0
			},
			key = "F♯",
			suffix = "°",
			Fingers = new int[6]
			{
				1,
				3,
				4,
				2,
				0,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1059,
			MidiNumbers = new int[6]
			{
				0,
				0,
				54,
				58,
				63,
				66
			},
			key = "F♯",
			suffix = "6",
			Fingers = new int[6]
			{
				0,
				0,
				3,
				2,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1060,
			MidiNumbers = new int[6]
			{
				0,
				54,
				61,
				66,
				70,
				75
			},
			key = "F♯",
			suffix = "6",
			Fingers = new int[6]
			{
				0,
				1,
				2,
				3,
				4,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1061,
			MidiNumbers = new int[6]
			{
				0,
				0,
				54,
				61,
				63,
				70
			},
			key = "F♯",
			suffix = "6",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1062,
			MidiNumbers = new int[6]
			{
				42,
				0,
				51,
				58,
				61,
				0
			},
			key = "F♯",
			suffix = "6",
			Fingers = new int[6]
			{
				2,
				0,
				1,
				4,
				3,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1063,
			MidiNumbers = new int[6]
			{
				42,
				49,
				52,
				58,
				61,
				68
			},
			key = "F♯",
			suffix = "9",
			Fingers = new int[6]
			{
				1,
				3,
				1,
				2,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1064,
			MidiNumbers = new int[6]
			{
				0,
				54,
				58,
				64,
				68,
				0
			},
			key = "F♯",
			suffix = "9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1065,
			MidiNumbers = new int[6]
			{
				0,
				54,
				58,
				64,
				68,
				73
			},
			key = "F♯",
			suffix = "9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				3,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1066,
			MidiNumbers = new int[6]
			{
				54,
				0,
				64,
				68,
				70,
				0
			},
			key = "F♯",
			suffix = "9",
			Fingers = new int[6]
			{
				3,
				0,
				4,
				2,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1067,
			MidiNumbers = new int[6]
			{
				0,
				0,
				54,
				58,
				64,
				68
			},
			key = "F♯",
			suffix = "9",
			Fingers = new int[6]
			{
				0,
				0,
				2,
				1,
				4,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1068,
			MidiNumbers = new int[6]
			{
				42,
				46,
				52,
				56,
				61,
				0
			},
			key = "F♯",
			suffix = "9",
			Fingers = new int[6]
			{
				2,
				1,
				3,
				1,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1069,
			MidiNumbers = new int[6]
			{
				42,
				49,
				52,
				59,
				61,
				68
			},
			key = "F♯",
			suffix = "11",
			Fingers = new int[6]
			{
				1,
				2,
				1,
				3,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1070,
			MidiNumbers = new int[6]
			{
				42,
				0,
				52,
				56,
				59,
				64
			},
			key = "F♯",
			suffix = "11",
			Fingers = new int[6]
			{
				2,
				0,
				3,
				1,
				0,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1071,
			MidiNumbers = new int[6]
			{
				0,
				54,
				58,
				64,
				66,
				71
			},
			key = "F♯",
			suffix = "11",
			Fingers = new int[6]
			{
				0,
				3,
				2,
				4,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1072,
			MidiNumbers = new int[6]
			{
				0,
				54,
				59,
				64,
				70,
				73
			},
			key = "F♯",
			suffix = "11",
			Fingers = new int[6]
			{
				0,
				1,
				1,
				1,
				3,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1073,
			MidiNumbers = new int[6]
			{
				0,
				0,
				54,
				59,
				64,
				70
			},
			key = "F♯",
			suffix = "11",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				1,
				2,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1074,
			MidiNumbers = new int[6]
			{
				42,
				0,
				52,
				58,
				63,
				68
			},
			key = "F♯",
			suffix = "13",
			Fingers = new int[6]
			{
				1,
				0,
				1,
				1,
				2,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1075,
			MidiNumbers = new int[6]
			{
				0,
				54,
				58,
				64,
				68,
				75
			},
			key = "F♯",
			suffix = "13",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1076,
			MidiNumbers = new int[6]
			{
				54,
				0,
				64,
				68,
				70,
				75
			},
			key = "F♯",
			suffix = "13",
			Fingers = new int[6]
			{
				3,
				0,
				4,
				2,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1077,
			MidiNumbers = new int[6]
			{
				42,
				49,
				52,
				58,
				63,
				66
			},
			key = "F♯",
			suffix = "13",
			Fingers = new int[6]
			{
				1,
				3,
				1,
				2,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1078,
			MidiNumbers = new int[6]
			{
				42,
				49,
				52,
				59,
				61,
				66
			},
			key = "F♯",
			suffix = "7sus4",
			Fingers = new int[6]
			{
				1,
				2,
				1,
				3,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1079,
			MidiNumbers = new int[6]
			{
				0,
				54,
				59,
				64,
				66,
				71
			},
			key = "F♯",
			suffix = "7sus4",
			Fingers = new int[6]
			{
				0,
				2,
				3,
				4,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1080,
			MidiNumbers = new int[6]
			{
				0,
				54,
				61,
				64,
				71,
				73
			},
			key = "F♯",
			suffix = "7sus4",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				1,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1081,
			MidiNumbers = new int[6]
			{
				0,
				0,
				54,
				61,
				64,
				71
			},
			key = "F♯",
			suffix = "7sus4",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				2,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1082,
			MidiNumbers = new int[6]
			{
				0,
				54,
				59,
				64,
				68,
				73
			},
			key = "F♯",
			suffix = "9sus4",
			Fingers = new int[6]
			{
				0,
				1,
				1,
				1,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1083,
			MidiNumbers = new int[6]
			{
				54,
				0,
				64,
				68,
				71,
				0
			},
			key = "F♯",
			suffix = "9sus4",
			Fingers = new int[6]
			{
				3,
				0,
				4,
				2,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1084,
			MidiNumbers = new int[6]
			{
				54,
				0,
				64,
				68,
				71,
				76
			},
			key = "F♯",
			suffix = "9sus4",
			Fingers = new int[6]
			{
				3,
				0,
				4,
				2,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1085,
			MidiNumbers = new int[6]
			{
				0,
				54,
				59,
				64,
				68,
				75
			},
			key = "F♯",
			suffix = "13sus4",
			Fingers = new int[6]
			{
				0,
				1,
				1,
				1,
				1,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1086,
			MidiNumbers = new int[6]
			{
				42,
				0,
				52,
				59,
				63,
				68
			},
			key = "F♯",
			suffix = "13sus4",
			Fingers = new int[6]
			{
				1,
				0,
				2,
				4,
				4,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1087,
			MidiNumbers = new int[6]
			{
				42,
				48,
				51,
				57,
				0,
				0
			},
			key = "F♯",
			suffix = "°7",
			Fingers = new int[6]
			{
				2,
				4,
				1,
				3,
				0,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1088,
			MidiNumbers = new int[6]
			{
				0,
				54,
				60,
				63,
				69,
				0
			},
			key = "F♯",
			suffix = "°7",
			Fingers = new int[6]
			{
				0,
				2,
				3,
				1,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1089,
			MidiNumbers = new int[6]
			{
				0,
				0,
				54,
				60,
				63,
				69
			},
			key = "F♯",
			suffix = "°7",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				2,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1090,
			MidiNumbers = new int[6]
			{
				42,
				0,
				51,
				57,
				60,
				0
			},
			key = "F♯",
			suffix = "°7",
			Fingers = new int[6]
			{
				3,
				0,
				1,
				4,
				2,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1091,
			MidiNumbers = new int[6]
			{
				42,
				48,
				54,
				57,
				63,
				66
			},
			key = "F♯",
			suffix = "°7",
			Fingers = new int[6]
			{
				1,
				2,
				3,
				1,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1092,
			MidiNumbers = new int[6]
			{
				0,
				54,
				62,
				64,
				70,
				74
			},
			key = "F♯",
			suffix = "+7",
			Fingers = new int[6]
			{
				0,
				1,
				4,
				1,
				3,
				2
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1093,
			MidiNumbers = new int[6]
			{
				42,
				0,
				52,
				58,
				62,
				0
			},
			key = "F♯",
			suffix = "+7",
			Fingers = new int[6]
			{
				1,
				0,
				2,
				3,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1094,
			MidiNumbers = new int[6]
			{
				0,
				0,
				54,
				62,
				64,
				70
			},
			key = "F♯",
			suffix = "+7",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				4,
				2,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1095,
			MidiNumbers = new int[6]
			{
				0,
				54,
				0,
				64,
				70,
				74
			},
			key = "F♯",
			suffix = "+7",
			Fingers = new int[6]
			{
				0,
				1,
				0,
				2,
				4,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1096,
			MidiNumbers = new int[6]
			{
				0,
				54,
				60,
				64,
				70,
				0
			},
			key = "F♯",
			suffix = "7♭5",
			Fingers = new int[6]
			{
				0,
				1,
				2,
				1,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1097,
			MidiNumbers = new int[6]
			{
				42,
				0,
				52,
				58,
				60,
				0
			},
			key = "F♯",
			suffix = "7♭5",
			Fingers = new int[6]
			{
				2,
				0,
				3,
				4,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1098,
			MidiNumbers = new int[6]
			{
				0,
				0,
				54,
				60,
				64,
				70
			},
			key = "F♯",
			suffix = "7♭5",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				2,
				2,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1099,
			MidiNumbers = new int[6]
			{
				0,
				54,
				60,
				64,
				70,
				76
			},
			key = "F♯",
			suffix = "7♭5",
			Fingers = new int[6]
			{
				0,
				1,
				2,
				1,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1100,
			MidiNumbers = new int[6]
			{
				0,
				54,
				58,
				64,
				69,
				0
			},
			key = "F♯",
			suffix = "7♯9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1101,
			MidiNumbers = new int[6]
			{
				42,
				49,
				52,
				58,
				61,
				69
			},
			key = "F♯",
			suffix = "7♯9",
			Fingers = new int[6]
			{
				1,
				3,
				1,
				2,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1102,
			MidiNumbers = new int[6]
			{
				0,
				0,
				54,
				58,
				64,
				69
			},
			key = "F♯",
			suffix = "7♯9",
			Fingers = new int[6]
			{
				0,
				0,
				2,
				1,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1103,
			MidiNumbers = new int[6]
			{
				0,
				54,
				58,
				64,
				69,
				72
			},
			key = "F♯",
			suffix = "7♭5♯9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1104,
			MidiNumbers = new int[6]
			{
				42,
				48,
				52,
				58,
				61,
				66
			},
			key = "F♯",
			suffix = "7♯11",
			Fingers = new int[6]
			{
				1,
				2,
				1,
				3,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1105,
			MidiNumbers = new int[6]
			{
				0,
				54,
				58,
				64,
				67,
				0
			},
			key = "F♯",
			suffix = "7♭9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1106,
			MidiNumbers = new int[6]
			{
				42,
				49,
				52,
				58,
				61,
				67
			},
			key = "F♯",
			suffix = "7♭9",
			Fingers = new int[6]
			{
				1,
				4,
				1,
				2,
				1,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1107,
			MidiNumbers = new int[6]
			{
				0,
				54,
				58,
				64,
				67,
				73
			},
			key = "F♯",
			suffix = "7♭9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1108,
			MidiNumbers = new int[6]
			{
				0,
				0,
				54,
				58,
				64,
				67
			},
			key = "F♯",
			suffix = "7♭9",
			Fingers = new int[6]
			{
				0,
				0,
				2,
				1,
				3,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1109,
			MidiNumbers = new int[6]
			{
				0,
				54,
				58,
				64,
				68,
				74
			},
			key = "F♯",
			suffix = "+9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1110,
			MidiNumbers = new int[6]
			{
				42,
				0,
				52,
				58,
				62,
				68
			},
			key = "F♯",
			suffix = "+9",
			Fingers = new int[6]
			{
				1,
				0,
				2,
				3,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1111,
			MidiNumbers = new int[6]
			{
				0,
				54,
				61,
				65,
				69,
				73
			},
			key = "F♯",
			suffix = "mMaj7",
			Fingers = new int[6]
			{
				0,
				1,
				4,
				2,
				3,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1112,
			MidiNumbers = new int[6]
			{
				42,
				0,
				53,
				57,
				61,
				0
			},
			key = "F♯",
			suffix = "mMaj7",
			Fingers = new int[6]
			{
				1,
				0,
				4,
				2,
				3,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1113,
			MidiNumbers = new int[6]
			{
				42,
				49,
				53,
				57,
				61,
				66
			},
			key = "F♯",
			suffix = "mMaj7",
			Fingers = new int[6]
			{
				1,
				3,
				2,
				1,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1114,
			MidiNumbers = new int[6]
			{
				0,
				0,
				54,
				61,
				65,
				69
			},
			key = "F♯",
			suffix = "mMaj7",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				4,
				2
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1115,
			MidiNumbers = new int[6]
			{
				0,
				0,
				54,
				57,
				61,
				65
			},
			key = "F♯",
			suffix = "mMaj7",
			Fingers = new int[6]
			{
				0,
				0,
				4,
				2,
				3,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1116,
			MidiNumbers = new int[6]
			{
				0,
				0,
				54,
				58,
				61,
				68
			},
			key = "F♯",
			suffix = "add9",
			Fingers = new int[6]
			{
				0,
				0,
				3,
				2,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1117,
			MidiNumbers = new int[6]
			{
				0,
				54,
				61,
				68,
				70,
				73
			},
			key = "F♯",
			suffix = "add9",
			Fingers = new int[6]
			{
				0,
				1,
				2,
				4,
				3,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1118,
			MidiNumbers = new int[6]
			{
				42,
				0,
				0,
				58,
				61,
				68
			},
			key = "F♯",
			suffix = "add9",
			Fingers = new int[6]
			{
				1,
				0,
				0,
				3,
				2,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1119,
			MidiNumbers = new int[6]
			{
				0,
				0,
				54,
				57,
				61,
				68
			},
			key = "F♯",
			suffix = "madd9",
			Fingers = new int[6]
			{
				0,
				0,
				3,
				1,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1120,
			MidiNumbers = new int[6]
			{
				42,
				49,
				54,
				57,
				63,
				66
			},
			key = "F♯",
			suffix = "m6",
			Fingers = new int[6]
			{
				1,
				2,
				3,
				1,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1121,
			MidiNumbers = new int[6]
			{
				0,
				0,
				54,
				61,
				63,
				69
			},
			key = "F♯",
			suffix = "m6",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				1,
				2
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1122,
			MidiNumbers = new int[6]
			{
				0,
				54,
				57,
				63,
				66,
				0
			},
			key = "F♯",
			suffix = "m6",
			Fingers = new int[6]
			{
				0,
				4,
				1,
				3,
				2,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1123,
			MidiNumbers = new int[6]
			{
				42,
				0,
				51,
				57,
				61,
				0
			},
			key = "F♯",
			suffix = "m6",
			Fingers = new int[6]
			{
				2,
				0,
				1,
				3,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1124,
			MidiNumbers = new int[6]
			{
				0,
				54,
				61,
				63,
				69,
				0
			},
			key = "F♯",
			suffix = "m6",
			Fingers = new int[6]
			{
				0,
				2,
				4,
				1,
				3,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1125,
			MidiNumbers = new int[6]
			{
				0,
				54,
				57,
				63,
				66,
				73
			},
			key = "F♯",
			suffix = "m6",
			Fingers = new int[6]
			{
				0,
				3,
				1,
				2,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1126,
			MidiNumbers = new int[6]
			{
				0,
				54,
				0,
				63,
				69,
				73
			},
			key = "F♯",
			suffix = "m6",
			Fingers = new int[6]
			{
				0,
				2,
				0,
				1,
				4,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1127,
			MidiNumbers = new int[6]
			{
				0,
				54,
				57,
				62,
				66,
				73
			},
			key = "F♯",
			suffix = "m♭6",
			Fingers = new int[6]
			{
				0,
				3,
				1,
				1,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1128,
			MidiNumbers = new int[6]
			{
				54,
				0,
				62,
				69,
				73,
				0
			},
			key = "F♯",
			suffix = "m♭6",
			Fingers = new int[6]
			{
				2,
				0,
				1,
				3,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1129,
			MidiNumbers = new int[6]
			{
				42,
				0,
				52,
				57,
				60,
				0
			},
			key = "F♯",
			suffix = "m7♭5",
			Fingers = new int[6]
			{
				2,
				0,
				3,
				4,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1130,
			MidiNumbers = new int[6]
			{
				0,
				54,
				60,
				64,
				69,
				0
			},
			key = "F♯",
			suffix = "m7♭5",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				2,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1131,
			MidiNumbers = new int[6]
			{
				0,
				0,
				54,
				60,
				64,
				69
			},
			key = "F♯",
			suffix = "m7♭5",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				2,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1132,
			MidiNumbers = new int[6]
			{
				42,
				48,
				54,
				60,
				64,
				69
			},
			key = "F♯",
			suffix = "m7♭5",
			Fingers = new int[6]
			{
				1,
				2,
				3,
				4,
				4,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1133,
			MidiNumbers = new int[6]
			{
				0,
				54,
				0,
				64,
				69,
				72
			},
			key = "F♯",
			suffix = "m7♭5",
			Fingers = new int[6]
			{
				0,
				2,
				0,
				3,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1134,
			MidiNumbers = new int[6]
			{
				42,
				48,
				52,
				57,
				64,
				66
			},
			key = "F♯",
			suffix = "m7♭5",
			Fingers = new int[6]
			{
				1,
				2,
				1,
				1,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1135,
			MidiNumbers = new int[6]
			{
				42,
				49,
				52,
				57,
				61,
				68
			},
			key = "F♯",
			suffix = "m9",
			Fingers = new int[6]
			{
				1,
				2,
				1,
				1,
				1,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1136,
			MidiNumbers = new int[6]
			{
				0,
				54,
				57,
				64,
				68,
				0
			},
			key = "F♯",
			suffix = "m9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1137,
			MidiNumbers = new int[6]
			{
				0,
				54,
				57,
				64,
				68,
				73
			},
			key = "F♯",
			suffix = "m9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				3,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1138,
			MidiNumbers = new int[6]
			{
				0,
				0,
				54,
				57,
				64,
				68
			},
			key = "F♯",
			suffix = "m9",
			Fingers = new int[6]
			{
				0,
				0,
				2,
				1,
				4,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1139,
			MidiNumbers = new int[6]
			{
				42,
				0,
				52,
				57,
				61,
				68
			},
			key = "F♯",
			suffix = "m9",
			Fingers = new int[6]
			{
				2,
				0,
				3,
				3,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1140,
			MidiNumbers = new int[6]
			{
				0,
				54,
				57,
				64,
				66,
				71
			},
			key = "F♯",
			suffix = "m11",
			Fingers = new int[6]
			{
				0,
				3,
				1,
				4,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1141,
			MidiNumbers = new int[6]
			{
				0,
				54,
				59,
				64,
				69,
				73
			},
			key = "F♯",
			suffix = "m11",
			Fingers = new int[6]
			{
				0,
				1,
				1,
				1,
				2,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1142,
			MidiNumbers = new int[6]
			{
				54,
				0,
				64,
				69,
				71,
				0
			},
			key = "F♯",
			suffix = "m11",
			Fingers = new int[6]
			{
				2,
				0,
				3,
				4,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1143,
			MidiNumbers = new int[6]
			{
				0,
				0,
				54,
				59,
				64,
				69
			},
			key = "F♯",
			suffix = "m11",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				1,
				2,
				2
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1144,
			MidiNumbers = new int[6]
			{
				0,
				54,
				0,
				64,
				69,
				71
			},
			key = "F♯",
			suffix = "m11",
			Fingers = new int[6]
			{
				0,
				2,
				0,
				3,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1145,
			MidiNumbers = new int[6]
			{
				0,
				54,
				0,
				64,
				69,
				75
			},
			key = "F♯",
			suffix = "m13",
			Fingers = new int[6]
			{
				0,
				1,
				0,
				2,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1146,
			MidiNumbers = new int[6]
			{
				42,
				49,
				52,
				57,
				63,
				66
			},
			key = "F♯",
			suffix = "m13",
			Fingers = new int[6]
			{
				1,
				3,
				1,
				1,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1147,
			MidiNumbers = new int[6]
			{
				42,
				0,
				52,
				57,
				63,
				68
			},
			key = "F♯",
			suffix = "m13",
			Fingers = new int[6]
			{
				2,
				0,
				3,
				3,
				4,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1148,
			MidiNumbers = new int[6]
			{
				0,
				54,
				61,
				64,
				69,
				75
			},
			key = "F♯",
			suffix = "m13",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				1,
				2,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1149,
			MidiNumbers = new int[6]
			{
				0,
				54,
				57,
				64,
				68,
				75
			},
			key = "F♯",
			suffix = "m13",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1150,
			MidiNumbers = new int[6]
			{
				0,
				54,
				57,
				65,
				68,
				0
			},
			key = "F♯",
			suffix = "mMaj9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				4,
				3,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1151,
			MidiNumbers = new int[6]
			{
				42,
				49,
				53,
				57,
				61,
				68
			},
			key = "F♯",
			suffix = "mMaj9",
			Fingers = new int[6]
			{
				1,
				3,
				2,
				1,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1152,
			MidiNumbers = new int[6]
			{
				0,
				54,
				58,
				65,
				68,
				0
			},
			key = "F♯",
			suffix = "Maj9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				4,
				3,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1153,
			MidiNumbers = new int[6]
			{
				0,
				0,
				54,
				58,
				65,
				68
			},
			key = "F♯",
			suffix = "Maj9",
			Fingers = new int[6]
			{
				0,
				0,
				2,
				1,
				4,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1154,
			MidiNumbers = new int[6]
			{
				0,
				54,
				56,
				61,
				65,
				70
			},
			key = "F♯",
			suffix = "Maj9",
			Fingers = new int[6]
			{
				0,
				3,
				1,
				1,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1155,
			MidiNumbers = new int[6]
			{
				42,
				46,
				53,
				56,
				61,
				65
			},
			key = "F♯",
			suffix = "Maj9",
			Fingers = new int[6]
			{
				2,
				1,
				4,
				1,
				3,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1156,
			MidiNumbers = new int[6]
			{
				42,
				0,
				53,
				56,
				61,
				0
			},
			key = "F♯",
			suffix = "Maj9",
			Fingers = new int[6]
			{
				2,
				0,
				4,
				1,
				3,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1157,
			MidiNumbers = new int[6]
			{
				0,
				54,
				0,
				65,
				70,
				75
			},
			key = "F♯",
			suffix = "Maj13",
			Fingers = new int[6]
			{
				0,
				1,
				0,
				2,
				4,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1158,
			MidiNumbers = new int[6]
			{
				42,
				46,
				51,
				56,
				61,
				65
			},
			key = "F♯",
			suffix = "Maj13",
			Fingers = new int[6]
			{
				2,
				1,
				1,
				1,
				3,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1159,
			MidiNumbers = new int[6]
			{
				42,
				0,
				53,
				58,
				63,
				68
			},
			key = "F♯",
			suffix = "Maj13",
			Fingers = new int[6]
			{
				1,
				0,
				2,
				3,
				4,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1160,
			MidiNumbers = new int[6]
			{
				0,
				54,
				60,
				65,
				70,
				73
			},
			key = "F♯",
			suffix = "Maj7♯11",
			Fingers = new int[6]
			{
				0,
				1,
				2,
				3,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1161,
			MidiNumbers = new int[6]
			{
				42,
				0,
				53,
				58,
				60,
				0
			},
			key = "F♯",
			suffix = "Maj7♯11",
			Fingers = new int[6]
			{
				2,
				0,
				3,
				4,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1162,
			MidiNumbers = new int[6]
			{
				0,
				0,
				54,
				60,
				65,
				70
			},
			key = "F♯",
			suffix = "Maj7♯11",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				2,
				3,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1163,
			MidiNumbers = new int[6]
			{
				0,
				54,
				58,
				64,
				68,
				72
			},
			key = "F♯",
			suffix = "9♭5",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1164,
			MidiNumbers = new int[6]
			{
				42,
				46,
				52,
				56,
				60,
				0
			},
			key = "F♯",
			suffix = "9♭5",
			Fingers = new int[6]
			{
				2,
				1,
				3,
				1,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1165,
			MidiNumbers = new int[6]
			{
				42,
				48,
				52,
				57,
				64,
				68
			},
			key = "F♯",
			suffix = "m9♭5",
			Fingers = new int[6]
			{
				1,
				2,
				1,
				1,
				4,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1166,
			MidiNumbers = new int[6]
			{
				0,
				54,
				58,
				64,
				67,
				74
			},
			key = "F♯",
			suffix = "+7♭9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1167,
			MidiNumbers = new int[6]
			{
				42,
				0,
				52,
				58,
				62,
				67
			},
			key = "F♯",
			suffix = "+7♭9",
			Fingers = new int[6]
			{
				1,
				0,
				2,
				3,
				3,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1168,
			MidiNumbers = new int[6]
			{
				0,
				54,
				58,
				64,
				69,
				74
			},
			key = "F♯",
			suffix = "+7♯9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				4,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1169,
			MidiNumbers = new int[6]
			{
				42,
				0,
				52,
				58,
				62,
				69
			},
			key = "F♯",
			suffix = "+7♯9",
			Fingers = new int[6]
			{
				1,
				0,
				2,
				3,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1170,
			MidiNumbers = new int[6]
			{
				43,
				47,
				50,
				55,
				62,
				67
			},
			key = "G",
			suffix = string.Empty,
			Fingers = new int[6]
			{
				2,
				1,
				0,
				0,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1171,
			MidiNumbers = new int[6]
			{
				43,
				47,
				50,
				55,
				59,
				67
			},
			key = "G",
			suffix = string.Empty,
			Fingers = new int[6]
			{
				2,
				1,
				0,
				0,
				0,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1172,
			MidiNumbers = new int[6]
			{
				43,
				50,
				55,
				59,
				62,
				67
			},
			key = "G",
			suffix = string.Empty,
			Fingers = new int[6]
			{
				1,
				3,
				4,
				2,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1173,
			MidiNumbers = new int[6]
			{
				43,
				47,
				50,
				0,
				0,
				0
			},
			key = "G",
			suffix = string.Empty,
			Fingers = new int[6]
			{
				2,
				1,
				0,
				0,
				0,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1174,
			MidiNumbers = new int[6]
			{
				0,
				55,
				62,
				67,
				71,
				74
			},
			key = "G",
			suffix = string.Empty,
			Fingers = new int[6]
			{
				0,
				1,
				2,
				3,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1175,
			MidiNumbers = new int[6]
			{
				0,
				55,
				59,
				62,
				67,
				71
			},
			key = "G",
			suffix = string.Empty,
			Fingers = new int[6]
			{
				0,
				4,
				3,
				1,
				2,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1176,
			MidiNumbers = new int[6]
			{
				55,
				59,
				62,
				67,
				71,
				79
			},
			key = "G",
			suffix = string.Empty,
			Fingers = new int[6]
			{
				3,
				2,
				1,
				1,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1177,
			MidiNumbers = new int[6]
			{
				0,
				0,
				55,
				62,
				67,
				71
			},
			key = "G",
			suffix = string.Empty,
			Fingers = new int[6]
			{
				0,
				0,
				1,
				2,
				4,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1178,
			MidiNumbers = new int[6]
			{
				43,
				50,
				55,
				58,
				62,
				67
			},
			key = "G",
			suffix = "m",
			Fingers = new int[6]
			{
				1,
				3,
				4,
				1,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1179,
			MidiNumbers = new int[6]
			{
				0,
				55,
				62,
				67,
				70,
				74
			},
			key = "G",
			suffix = "m",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				4,
				2,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1180,
			MidiNumbers = new int[6]
			{
				0,
				0,
				55,
				62,
				67,
				70
			},
			key = "G",
			suffix = "m",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				4,
				2
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1181,
			MidiNumbers = new int[6]
			{
				0,
				0,
				55,
				58,
				62,
				67
			},
			key = "G",
			suffix = "m",
			Fingers = new int[6]
			{
				0,
				0,
				3,
				1,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1182,
			MidiNumbers = new int[6]
			{
				43,
				47,
				50,
				55,
				59,
				65
			},
			key = "G",
			suffix = "7",
			Fingers = new int[6]
			{
				3,
				2,
				0,
				0,
				0,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1183,
			MidiNumbers = new int[6]
			{
				43,
				50,
				53,
				59,
				62,
				67
			},
			key = "G",
			suffix = "7",
			Fingers = new int[6]
			{
				1,
				3,
				1,
				2,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1184,
			MidiNumbers = new int[6]
			{
				0,
				55,
				62,
				65,
				71,
				74
			},
			key = "G",
			suffix = "7",
			Fingers = new int[6]
			{
				0,
				1,
				2,
				1,
				3,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1185,
			MidiNumbers = new int[6]
			{
				0,
				0,
				55,
				62,
				65,
				71
			},
			key = "G",
			suffix = "7",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				2,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1186,
			MidiNumbers = new int[6]
			{
				0,
				55,
				59,
				65,
				67,
				0
			},
			key = "G",
			suffix = "7",
			Fingers = new int[6]
			{
				0,
				3,
				2,
				4,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1187,
			MidiNumbers = new int[6]
			{
				43,
				0,
				53,
				59,
				62,
				0
			},
			key = "G",
			suffix = "7",
			Fingers = new int[6]
			{
				1,
				0,
				2,
				4,
				3,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1188,
			MidiNumbers = new int[6]
			{
				43,
				45,
				50,
				55,
				62,
				67
			},
			key = "G",
			suffix = "sus2",
			Fingers = new int[6]
			{
				2,
				0,
				0,
				0,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1189,
			MidiNumbers = new int[6]
			{
				43,
				50,
				55,
				57,
				0,
				0
			},
			key = "G",
			suffix = "sus2",
			Fingers = new int[6]
			{
				2,
				3,
				4,
				1,
				0,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1190,
			MidiNumbers = new int[6]
			{
				43,
				0,
				50,
				57,
				62,
				67
			},
			key = "G",
			suffix = "sus2",
			Fingers = new int[6]
			{
				2,
				0,
				0,
				1,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1191,
			MidiNumbers = new int[6]
			{
				0,
				55,
				62,
				67,
				69,
				74
			},
			key = "G",
			suffix = "sus2",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				4,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1192,
			MidiNumbers = new int[6]
			{
				0,
				0,
				55,
				62,
				67,
				69
			},
			key = "G",
			suffix = "sus2",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1193,
			MidiNumbers = new int[6]
			{
				0,
				55,
				62,
				69,
				0,
				0
			},
			key = "G",
			suffix = "sus2",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				4,
				0,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1194,
			MidiNumbers = new int[6]
			{
				0,
				0,
				55,
				57,
				62,
				69
			},
			key = "G",
			suffix = "sus2",
			Fingers = new int[6]
			{
				0,
				0,
				3,
				1,
				2,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1195,
			MidiNumbers = new int[6]
			{
				43,
				48,
				50,
				55,
				60,
				67
			},
			key = "G",
			suffix = "sus4",
			Fingers = new int[6]
			{
				2,
				3,
				0,
				0,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1196,
			MidiNumbers = new int[6]
			{
				43,
				50,
				55,
				60,
				62,
				67
			},
			key = "G",
			suffix = "sus4",
			Fingers = new int[6]
			{
				1,
				2,
				3,
				4,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1197,
			MidiNumbers = new int[6]
			{
				0,
				55,
				60,
				62,
				67,
				0
			},
			key = "G",
			suffix = "sus4",
			Fingers = new int[6]
			{
				0,
				3,
				4,
				1,
				2,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1198,
			MidiNumbers = new int[6]
			{
				0,
				0,
				55,
				62,
				67,
				72
			},
			key = "G",
			suffix = "sus4",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				2,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1199,
			MidiNumbers = new int[6]
			{
				0,
				55,
				62,
				67,
				72,
				74
			},
			key = "G",
			suffix = "sus4",
			Fingers = new int[6]
			{
				0,
				1,
				2,
				3,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1200,
			MidiNumbers = new int[6]
			{
				0,
				0,
				55,
				60,
				62,
				67
			},
			key = "G",
			suffix = "sus4",
			Fingers = new int[6]
			{
				0,
				0,
				3,
				4,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1201,
			MidiNumbers = new int[6]
			{
				43,
				50,
				53,
				58,
				62,
				67
			},
			key = "G",
			suffix = "m7",
			Fingers = new int[6]
			{
				1,
				3,
				1,
				1,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1202,
			MidiNumbers = new int[6]
			{
				43,
				0,
				53,
				58,
				62,
				0
			},
			key = "G",
			suffix = "m7",
			Fingers = new int[6]
			{
				1,
				0,
				2,
				3,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1203,
			MidiNumbers = new int[6]
			{
				0,
				55,
				62,
				65,
				70,
				74
			},
			key = "G",
			suffix = "m7",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				1,
				2,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1204,
			MidiNumbers = new int[6]
			{
				0,
				55,
				58,
				65,
				70,
				0
			},
			key = "G",
			suffix = "m7",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1205,
			MidiNumbers = new int[6]
			{
				43,
				50,
				55,
				58,
				65,
				67
			},
			key = "G",
			suffix = "m7",
			Fingers = new int[6]
			{
				1,
				2,
				3,
				1,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1206,
			MidiNumbers = new int[6]
			{
				0,
				55,
				58,
				65,
				67,
				0
			},
			key = "G",
			suffix = "m7",
			Fingers = new int[6]
			{
				0,
				3,
				1,
				4,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1207,
			MidiNumbers = new int[6]
			{
				0,
				0,
				55,
				62,
				65,
				70
			},
			key = "G",
			suffix = "m7",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				4,
				2,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1208,
			MidiNumbers = new int[6]
			{
				43,
				47,
				50,
				55,
				59,
				66
			},
			key = "G",
			suffix = "Maj7",
			Fingers = new int[6]
			{
				3,
				2,
				0,
				0,
				0,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1209,
			MidiNumbers = new int[6]
			{
				0,
				0,
				55,
				59,
				62,
				66
			},
			key = "G",
			suffix = "Maj7",
			Fingers = new int[6]
			{
				0,
				0,
				4,
				3,
				2,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1210,
			MidiNumbers = new int[6]
			{
				0,
				0,
				55,
				62,
				66,
				71
			},
			key = "G",
			suffix = "Maj7",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				3,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1211,
			MidiNumbers = new int[6]
			{
				0,
				55,
				62,
				66,
				71,
				74
			},
			key = "G",
			suffix = "Maj7",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				2,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1212,
			MidiNumbers = new int[6]
			{
				43,
				0,
				54,
				59,
				62,
				0
			},
			key = "G",
			suffix = "Maj7",
			Fingers = new int[6]
			{
				1,
				0,
				3,
				4,
				2,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1213,
			MidiNumbers = new int[6]
			{
				0,
				55,
				59,
				62,
				66,
				0
			},
			key = "G",
			suffix = "Maj7",
			Fingers = new int[6]
			{
				0,
				4,
				3,
				1,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1214,
			MidiNumbers = new int[6]
			{
				43,
				50,
				54,
				59,
				62,
				67
			},
			key = "G",
			suffix = "Maj7",
			Fingers = new int[6]
			{
				1,
				4,
				2,
				3,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1215,
			MidiNumbers = new int[6]
			{
				43,
				50,
				55,
				0,
				0,
				0
			},
			key = "G",
			suffix = "5",
			Fingers = new int[6]
			{
				1,
				3,
				4,
				0,
				0,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1216,
			MidiNumbers = new int[6]
			{
				0,
				55,
				62,
				67,
				0,
				0
			},
			key = "G",
			suffix = "5",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				4,
				0,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1217,
			MidiNumbers = new int[6]
			{
				0,
				0,
				0,
				67,
				74,
				79
			},
			key = "G",
			suffix = "5",
			Fingers = new int[6]
			{
				0,
				0,
				0,
				1,
				4,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1218,
			MidiNumbers = new int[6]
			{
				0,
				0,
				55,
				62,
				67,
				0
			},
			key = "G",
			suffix = "5",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1219,
			MidiNumbers = new int[6]
			{
				0,
				55,
				59,
				63,
				67,
				0
			},
			key = "G",
			suffix = "+",
			Fingers = new int[6]
			{
				0,
				4,
				3,
				1,
				2,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1220,
			MidiNumbers = new int[6]
			{
				0,
				0,
				55,
				59,
				63,
				67
			},
			key = "G",
			suffix = "+",
			Fingers = new int[6]
			{
				0,
				0,
				4,
				2,
				3,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1221,
			MidiNumbers = new int[6]
			{
				55,
				59,
				63,
				67,
				0,
				0
			},
			key = "G",
			suffix = "+",
			Fingers = new int[6]
			{
				4,
				3,
				2,
				1,
				0,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1222,
			MidiNumbers = new int[6]
			{
				43,
				51,
				55,
				59,
				0,
				0
			},
			key = "G",
			suffix = "+",
			Fingers = new int[6]
			{
				1,
				4,
				3,
				2,
				0,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1223,
			MidiNumbers = new int[6]
			{
				0,
				55,
				0,
				67,
				71,
				75
			},
			key = "G",
			suffix = "+",
			Fingers = new int[6]
			{
				0,
				1,
				0,
				3,
				4,
				2
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1224,
			MidiNumbers = new int[6]
			{
				0,
				55,
				61,
				67,
				70,
				0
			},
			key = "G",
			suffix = "°",
			Fingers = new int[6]
			{
				0,
				1,
				2,
				4,
				3,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1225,
			MidiNumbers = new int[6]
			{
				43,
				0,
				55,
				58,
				61,
				0
			},
			key = "G",
			suffix = "°",
			Fingers = new int[6]
			{
				2,
				0,
				4,
				3,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1226,
			MidiNumbers = new int[6]
			{
				43,
				49,
				55,
				58,
				0,
				0
			},
			key = "G",
			suffix = "°",
			Fingers = new int[6]
			{
				1,
				3,
				4,
				2,
				0,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1227,
			MidiNumbers = new int[6]
			{
				43,
				47,
				50,
				55,
				59,
				64
			},
			key = "G",
			suffix = "6",
			Fingers = new int[6]
			{
				3,
				2,
				0,
				0,
				0,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1228,
			MidiNumbers = new int[6]
			{
				0,
				0,
				55,
				59,
				64,
				67
			},
			key = "G",
			suffix = "6",
			Fingers = new int[6]
			{
				0,
				0,
				3,
				2,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1229,
			MidiNumbers = new int[6]
			{
				0,
				55,
				62,
				67,
				71,
				76
			},
			key = "G",
			suffix = "6",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				3,
				3,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1230,
			MidiNumbers = new int[6]
			{
				0,
				0,
				55,
				62,
				64,
				71
			},
			key = "G",
			suffix = "6",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				2,
				1,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1231,
			MidiNumbers = new int[6]
			{
				43,
				0,
				52,
				59,
				62,
				0
			},
			key = "G",
			suffix = "6",
			Fingers = new int[6]
			{
				2,
				0,
				1,
				4,
				3,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1232,
			MidiNumbers = new int[6]
			{
				43,
				50,
				53,
				59,
				62,
				69
			},
			key = "G",
			suffix = "9",
			Fingers = new int[6]
			{
				1,
				3,
				1,
				2,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1233,
			MidiNumbers = new int[6]
			{
				0,
				55,
				59,
				65,
				69,
				0
			},
			key = "G",
			suffix = "9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1234,
			MidiNumbers = new int[6]
			{
				0,
				55,
				59,
				65,
				69,
				74
			},
			key = "G",
			suffix = "9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				3,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1235,
			MidiNumbers = new int[6]
			{
				55,
				0,
				65,
				69,
				71,
				0
			},
			key = "G",
			suffix = "9",
			Fingers = new int[6]
			{
				3,
				0,
				4,
				2,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1236,
			MidiNumbers = new int[6]
			{
				0,
				0,
				55,
				59,
				65,
				69
			},
			key = "G",
			suffix = "9",
			Fingers = new int[6]
			{
				0,
				0,
				2,
				1,
				4,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1237,
			MidiNumbers = new int[6]
			{
				43,
				47,
				53,
				57,
				62,
				0
			},
			key = "G",
			suffix = "9",
			Fingers = new int[6]
			{
				2,
				1,
				3,
				1,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1238,
			MidiNumbers = new int[6]
			{
				43,
				50,
				53,
				60,
				62,
				69
			},
			key = "G",
			suffix = "11",
			Fingers = new int[6]
			{
				1,
				2,
				1,
				3,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1239,
			MidiNumbers = new int[6]
			{
				0,
				55,
				59,
				65,
				67,
				72
			},
			key = "G",
			suffix = "11",
			Fingers = new int[6]
			{
				0,
				3,
				2,
				4,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1240,
			MidiNumbers = new int[6]
			{
				0,
				55,
				60,
				65,
				71,
				74
			},
			key = "G",
			suffix = "11",
			Fingers = new int[6]
			{
				0,
				1,
				1,
				1,
				3,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1241,
			MidiNumbers = new int[6]
			{
				0,
				0,
				55,
				60,
				65,
				71
			},
			key = "G",
			suffix = "11",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				1,
				2,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1242,
			MidiNumbers = new int[6]
			{
				43,
				0,
				53,
				57,
				59,
				64
			},
			key = "G",
			suffix = "13",
			Fingers = new int[6]
			{
				2,
				0,
				3,
				1,
				0,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1243,
			MidiNumbers = new int[6]
			{
				43,
				0,
				53,
				59,
				64,
				69
			},
			key = "G",
			suffix = "13",
			Fingers = new int[6]
			{
				1,
				0,
				2,
				3,
				4,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1244,
			MidiNumbers = new int[6]
			{
				0,
				55,
				59,
				65,
				69,
				76
			},
			key = "G",
			suffix = "13",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1245,
			MidiNumbers = new int[6]
			{
				55,
				0,
				65,
				69,
				71,
				76
			},
			key = "G",
			suffix = "13",
			Fingers = new int[6]
			{
				3,
				0,
				4,
				2,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1246,
			MidiNumbers = new int[6]
			{
				43,
				50,
				53,
				59,
				64,
				67
			},
			key = "G",
			suffix = "13",
			Fingers = new int[6]
			{
				1,
				3,
				1,
				2,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1247,
			MidiNumbers = new int[6]
			{
				43,
				50,
				53,
				60,
				62,
				67
			},
			key = "G",
			suffix = "7sus4",
			Fingers = new int[6]
			{
				1,
				2,
				1,
				3,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1248,
			MidiNumbers = new int[6]
			{
				0,
				55,
				60,
				65,
				67,
				72
			},
			key = "G",
			suffix = "7sus4",
			Fingers = new int[6]
			{
				0,
				2,
				3,
				4,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1249,
			MidiNumbers = new int[6]
			{
				0,
				55,
				62,
				65,
				72,
				74
			},
			key = "G",
			suffix = "7sus4",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				1,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1250,
			MidiNumbers = new int[6]
			{
				0,
				0,
				55,
				62,
				65,
				72
			},
			key = "G",
			suffix = "7sus4",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				2,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1251,
			MidiNumbers = new int[6]
			{
				0,
				55,
				60,
				65,
				69,
				74
			},
			key = "G",
			suffix = "9sus4",
			Fingers = new int[6]
			{
				0,
				1,
				1,
				1,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1252,
			MidiNumbers = new int[6]
			{
				43,
				0,
				53,
				57,
				60,
				0
			},
			key = "G",
			suffix = "9sus4",
			Fingers = new int[6]
			{
				3,
				0,
				4,
				2,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1253,
			MidiNumbers = new int[6]
			{
				43,
				0,
				53,
				57,
				60,
				65
			},
			key = "G",
			suffix = "9sus4",
			Fingers = new int[6]
			{
				3,
				0,
				4,
				2,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1254,
			MidiNumbers = new int[6]
			{
				0,
				55,
				60,
				65,
				69,
				76
			},
			key = "G",
			suffix = "13sus4",
			Fingers = new int[6]
			{
				0,
				1,
				1,
				1,
				1,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1255,
			MidiNumbers = new int[6]
			{
				43,
				0,
				53,
				60,
				64,
				69
			},
			key = "G",
			suffix = "13sus4",
			Fingers = new int[6]
			{
				1,
				0,
				2,
				4,
				4,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1256,
			MidiNumbers = new int[6]
			{
				43,
				49,
				52,
				58,
				0,
				0
			},
			key = "G",
			suffix = "°7",
			Fingers = new int[6]
			{
				2,
				4,
				1,
				3,
				0,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1257,
			MidiNumbers = new int[6]
			{
				0,
				55,
				61,
				64,
				70,
				0
			},
			key = "G",
			suffix = "°7",
			Fingers = new int[6]
			{
				0,
				2,
				3,
				1,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1258,
			MidiNumbers = new int[6]
			{
				0,
				0,
				55,
				61,
				64,
				70
			},
			key = "G",
			suffix = "°7",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				2,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1259,
			MidiNumbers = new int[6]
			{
				43,
				49,
				55,
				58,
				64,
				67
			},
			key = "G",
			suffix = "°7",
			Fingers = new int[6]
			{
				1,
				2,
				3,
				1,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1260,
			MidiNumbers = new int[6]
			{
				43,
				0,
				52,
				58,
				61,
				0
			},
			key = "G",
			suffix = "°7",
			Fingers = new int[6]
			{
				3,
				0,
				1,
				4,
				2,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1261,
			MidiNumbers = new int[6]
			{
				0,
				55,
				63,
				65,
				71,
				75
			},
			key = "G",
			suffix = "+7",
			Fingers = new int[6]
			{
				0,
				1,
				4,
				1,
				3,
				2
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1262,
			MidiNumbers = new int[6]
			{
				43,
				0,
				53,
				59,
				63,
				0
			},
			key = "G",
			suffix = "+7",
			Fingers = new int[6]
			{
				1,
				0,
				2,
				3,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1263,
			MidiNumbers = new int[6]
			{
				0,
				0,
				55,
				63,
				65,
				71
			},
			key = "G",
			suffix = "+7",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				4,
				2,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1264,
			MidiNumbers = new int[6]
			{
				0,
				55,
				0,
				65,
				71,
				75
			},
			key = "G",
			suffix = "+7",
			Fingers = new int[6]
			{
				0,
				1,
				0,
				2,
				4,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1265,
			MidiNumbers = new int[6]
			{
				0,
				55,
				61,
				65,
				71,
				0
			},
			key = "G",
			suffix = "7♭5",
			Fingers = new int[6]
			{
				0,
				1,
				2,
				1,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1266,
			MidiNumbers = new int[6]
			{
				43,
				0,
				53,
				59,
				61,
				0
			},
			key = "G",
			suffix = "7♭5",
			Fingers = new int[6]
			{
				2,
				0,
				3,
				4,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1267,
			MidiNumbers = new int[6]
			{
				0,
				0,
				55,
				61,
				65,
				71
			},
			key = "G",
			suffix = "7♭5",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				2,
				2,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1268,
			MidiNumbers = new int[6]
			{
				0,
				55,
				61,
				65,
				71,
				77
			},
			key = "G",
			suffix = "7♭5",
			Fingers = new int[6]
			{
				0,
				1,
				2,
				1,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1269,
			MidiNumbers = new int[6]
			{
				0,
				55,
				59,
				65,
				70,
				0
			},
			key = "G",
			suffix = "7♯9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1270,
			MidiNumbers = new int[6]
			{
				43,
				50,
				53,
				59,
				62,
				70
			},
			key = "G",
			suffix = "7♯9",
			Fingers = new int[6]
			{
				1,
				3,
				1,
				2,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1271,
			MidiNumbers = new int[6]
			{
				0,
				0,
				55,
				59,
				65,
				70
			},
			key = "G",
			suffix = "7♯9",
			Fingers = new int[6]
			{
				0,
				0,
				2,
				1,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1272,
			MidiNumbers = new int[6]
			{
				0,
				55,
				59,
				65,
				70,
				73
			},
			key = "G",
			suffix = "7♭5♯9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1273,
			MidiNumbers = new int[6]
			{
				43,
				49,
				53,
				59,
				62,
				67
			},
			key = "G",
			suffix = "7♯11",
			Fingers = new int[6]
			{
				1,
				2,
				1,
				3,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1274,
			MidiNumbers = new int[6]
			{
				0,
				55,
				59,
				65,
				68,
				0
			},
			key = "G",
			suffix = "7♭9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1275,
			MidiNumbers = new int[6]
			{
				43,
				50,
				53,
				59,
				62,
				68
			},
			key = "G",
			suffix = "7♭9",
			Fingers = new int[6]
			{
				1,
				4,
				1,
				2,
				1,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1276,
			MidiNumbers = new int[6]
			{
				0,
				55,
				59,
				65,
				68,
				74
			},
			key = "G",
			suffix = "7♭9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1277,
			MidiNumbers = new int[6]
			{
				0,
				0,
				55,
				59,
				65,
				68
			},
			key = "G",
			suffix = "7♭9",
			Fingers = new int[6]
			{
				0,
				0,
				2,
				1,
				3,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1278,
			MidiNumbers = new int[6]
			{
				0,
				55,
				59,
				65,
				69,
				75
			},
			key = "G",
			suffix = "+9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1279,
			MidiNumbers = new int[6]
			{
				43,
				0,
				53,
				59,
				63,
				69
			},
			key = "G",
			suffix = "+9",
			Fingers = new int[6]
			{
				1,
				0,
				2,
				3,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1280,
			MidiNumbers = new int[6]
			{
				0,
				55,
				62,
				66,
				70,
				74
			},
			key = "G",
			suffix = "mMaj7",
			Fingers = new int[6]
			{
				0,
				1,
				4,
				2,
				3,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1281,
			MidiNumbers = new int[6]
			{
				43,
				0,
				54,
				58,
				62,
				0
			},
			key = "G",
			suffix = "mMaj7",
			Fingers = new int[6]
			{
				1,
				0,
				4,
				2,
				3,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1282,
			MidiNumbers = new int[6]
			{
				43,
				50,
				54,
				58,
				62,
				67
			},
			key = "G",
			suffix = "mMaj7",
			Fingers = new int[6]
			{
				1,
				3,
				2,
				1,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1283,
			MidiNumbers = new int[6]
			{
				0,
				0,
				55,
				62,
				66,
				70
			},
			key = "G",
			suffix = "mMaj7",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				4,
				2
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1284,
			MidiNumbers = new int[6]
			{
				0,
				0,
				55,
				58,
				62,
				66
			},
			key = "G",
			suffix = "mMaj7",
			Fingers = new int[6]
			{
				0,
				0,
				4,
				2,
				3,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1285,
			MidiNumbers = new int[6]
			{
				43,
				47,
				50,
				55,
				59,
				69
			},
			key = "G",
			suffix = "add9",
			Fingers = new int[6]
			{
				2,
				1,
				0,
				0,
				0,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1286,
			MidiNumbers = new int[6]
			{
				43,
				47,
				50,
				57,
				59,
				67
			},
			key = "G",
			suffix = "add9",
			Fingers = new int[6]
			{
				3,
				1,
				0,
				2,
				0,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1287,
			MidiNumbers = new int[6]
			{
				0,
				0,
				55,
				59,
				62,
				69
			},
			key = "G",
			suffix = "add9",
			Fingers = new int[6]
			{
				0,
				0,
				3,
				2,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1288,
			MidiNumbers = new int[6]
			{
				0,
				55,
				62,
				69,
				71,
				74
			},
			key = "G",
			suffix = "add9",
			Fingers = new int[6]
			{
				0,
				1,
				2,
				4,
				3,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1289,
			MidiNumbers = new int[6]
			{
				43,
				0,
				0,
				59,
				62,
				69
			},
			key = "G",
			suffix = "add9",
			Fingers = new int[6]
			{
				1,
				0,
				0,
				3,
				2,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1290,
			MidiNumbers = new int[6]
			{
				0,
				0,
				55,
				58,
				62,
				69
			},
			key = "G",
			suffix = "madd9",
			Fingers = new int[6]
			{
				0,
				0,
				3,
				1,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1291,
			MidiNumbers = new int[6]
			{
				43,
				50,
				55,
				58,
				64,
				67
			},
			key = "G",
			suffix = "m6",
			Fingers = new int[6]
			{
				1,
				2,
				3,
				1,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1292,
			MidiNumbers = new int[6]
			{
				0,
				0,
				55,
				62,
				64,
				70
			},
			key = "G",
			suffix = "m6",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				1,
				2
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1293,
			MidiNumbers = new int[6]
			{
				0,
				55,
				58,
				64,
				67,
				0
			},
			key = "G",
			suffix = "m6",
			Fingers = new int[6]
			{
				0,
				4,
				1,
				3,
				2,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1294,
			MidiNumbers = new int[6]
			{
				43,
				0,
				52,
				58,
				62,
				0
			},
			key = "G",
			suffix = "m6",
			Fingers = new int[6]
			{
				2,
				0,
				1,
				3,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1295,
			MidiNumbers = new int[6]
			{
				0,
				55,
				62,
				64,
				70,
				0
			},
			key = "G",
			suffix = "m6",
			Fingers = new int[6]
			{
				0,
				2,
				4,
				1,
				3,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1296,
			MidiNumbers = new int[6]
			{
				0,
				55,
				58,
				64,
				67,
				74
			},
			key = "G",
			suffix = "m6",
			Fingers = new int[6]
			{
				0,
				3,
				1,
				2,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1297,
			MidiNumbers = new int[6]
			{
				0,
				55,
				0,
				64,
				70,
				74
			},
			key = "G",
			suffix = "m6",
			Fingers = new int[6]
			{
				0,
				2,
				0,
				1,
				4,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1298,
			MidiNumbers = new int[6]
			{
				0,
				55,
				58,
				63,
				67,
				74
			},
			key = "G",
			suffix = "m♭6",
			Fingers = new int[6]
			{
				0,
				3,
				1,
				1,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1299,
			MidiNumbers = new int[6]
			{
				43,
				0,
				51,
				58,
				62,
				0
			},
			key = "G",
			suffix = "m♭6",
			Fingers = new int[6]
			{
				2,
				0,
				1,
				3,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1300,
			MidiNumbers = new int[6]
			{
				43,
				0,
				53,
				58,
				61,
				0
			},
			key = "G",
			suffix = "m7♭5",
			Fingers = new int[6]
			{
				2,
				0,
				3,
				4,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1301,
			MidiNumbers = new int[6]
			{
				0,
				0,
				55,
				61,
				65,
				70
			},
			key = "G",
			suffix = "m7♭5",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				2,
				2,
				2
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1302,
			MidiNumbers = new int[6]
			{
				0,
				55,
				61,
				65,
				70,
				0
			},
			key = "G",
			suffix = "m7♭5",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				2,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1303,
			MidiNumbers = new int[6]
			{
				43,
				49,
				55,
				61,
				65,
				70
			},
			key = "G",
			suffix = "m7♭5",
			Fingers = new int[6]
			{
				1,
				2,
				3,
				4,
				4,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1304,
			MidiNumbers = new int[6]
			{
				0,
				55,
				0,
				65,
				70,
				73
			},
			key = "G",
			suffix = "m7♭5",
			Fingers = new int[6]
			{
				0,
				2,
				0,
				3,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1305,
			MidiNumbers = new int[6]
			{
				43,
				49,
				53,
				58,
				65,
				67
			},
			key = "G",
			suffix = "m7♭5",
			Fingers = new int[6]
			{
				1,
				2,
				1,
				1,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1306,
			MidiNumbers = new int[6]
			{
				43,
				50,
				53,
				58,
				62,
				69
			},
			key = "G",
			suffix = "m9",
			Fingers = new int[6]
			{
				1,
				2,
				1,
				1,
				1,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1307,
			MidiNumbers = new int[6]
			{
				0,
				55,
				58,
				65,
				69,
				0
			},
			key = "G",
			suffix = "m9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1308,
			MidiNumbers = new int[6]
			{
				0,
				55,
				58,
				65,
				69,
				74
			},
			key = "G",
			suffix = "m9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				3,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1309,
			MidiNumbers = new int[6]
			{
				0,
				0,
				55,
				58,
				65,
				69
			},
			key = "G",
			suffix = "m9",
			Fingers = new int[6]
			{
				0,
				0,
				2,
				1,
				4,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1310,
			MidiNumbers = new int[6]
			{
				43,
				0,
				53,
				58,
				62,
				69
			},
			key = "G",
			suffix = "m9",
			Fingers = new int[6]
			{
				2,
				0,
				3,
				3,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1311,
			MidiNumbers = new int[6]
			{
				0,
				55,
				58,
				65,
				67,
				72
			},
			key = "G",
			suffix = "m11",
			Fingers = new int[6]
			{
				0,
				3,
				1,
				4,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1312,
			MidiNumbers = new int[6]
			{
				0,
				55,
				60,
				65,
				70,
				74
			},
			key = "G",
			suffix = "m11",
			Fingers = new int[6]
			{
				0,
				1,
				1,
				1,
				2,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1313,
			MidiNumbers = new int[6]
			{
				43,
				0,
				53,
				58,
				60,
				0
			},
			key = "G",
			suffix = "m11",
			Fingers = new int[6]
			{
				2,
				0,
				3,
				4,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1314,
			MidiNumbers = new int[6]
			{
				0,
				0,
				55,
				60,
				65,
				70
			},
			key = "G",
			suffix = "m11",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				1,
				2,
				2
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1315,
			MidiNumbers = new int[6]
			{
				0,
				55,
				0,
				65,
				70,
				72
			},
			key = "G",
			suffix = "m11",
			Fingers = new int[6]
			{
				0,
				2,
				0,
				3,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1316,
			MidiNumbers = new int[6]
			{
				0,
				55,
				0,
				65,
				70,
				76
			},
			key = "G",
			suffix = "m13",
			Fingers = new int[6]
			{
				0,
				1,
				0,
				2,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1317,
			MidiNumbers = new int[6]
			{
				43,
				50,
				53,
				58,
				64,
				67
			},
			key = "G",
			suffix = "m13",
			Fingers = new int[6]
			{
				1,
				3,
				1,
				1,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1318,
			MidiNumbers = new int[6]
			{
				43,
				0,
				53,
				58,
				64,
				69
			},
			key = "G",
			suffix = "m13",
			Fingers = new int[6]
			{
				2,
				0,
				3,
				3,
				4,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1319,
			MidiNumbers = new int[6]
			{
				0,
				55,
				62,
				65,
				70,
				76
			},
			key = "G",
			suffix = "m13",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				1,
				2,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1320,
			MidiNumbers = new int[6]
			{
				0,
				55,
				58,
				65,
				69,
				76
			},
			key = "G",
			suffix = "m13",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1321,
			MidiNumbers = new int[6]
			{
				0,
				55,
				58,
				66,
				69,
				0
			},
			key = "G",
			suffix = "mMaj9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				4,
				3,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1322,
			MidiNumbers = new int[6]
			{
				43,
				50,
				54,
				58,
				62,
				69
			},
			key = "G",
			suffix = "mMaj9",
			Fingers = new int[6]
			{
				1,
				3,
				2,
				1,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1323,
			MidiNumbers = new int[6]
			{
				0,
				55,
				59,
				66,
				69,
				0
			},
			key = "G",
			suffix = "Maj9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				4,
				3,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1324,
			MidiNumbers = new int[6]
			{
				0,
				0,
				55,
				59,
				66,
				69
			},
			key = "G",
			suffix = "Maj9",
			Fingers = new int[6]
			{
				0,
				0,
				2,
				1,
				4,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1325,
			MidiNumbers = new int[6]
			{
				0,
				55,
				57,
				62,
				66,
				71
			},
			key = "G",
			suffix = "Maj9",
			Fingers = new int[6]
			{
				0,
				3,
				1,
				1,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1326,
			MidiNumbers = new int[6]
			{
				43,
				47,
				54,
				57,
				62,
				66
			},
			key = "G",
			suffix = "Maj9",
			Fingers = new int[6]
			{
				2,
				1,
				4,
				1,
				3,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1327,
			MidiNumbers = new int[6]
			{
				43,
				0,
				54,
				57,
				62,
				0
			},
			key = "G",
			suffix = "Maj9",
			Fingers = new int[6]
			{
				2,
				0,
				4,
				1,
				3,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1328,
			MidiNumbers = new int[6]
			{
				0,
				55,
				0,
				66,
				71,
				76
			},
			key = "G",
			suffix = "Maj13",
			Fingers = new int[6]
			{
				0,
				1,
				0,
				2,
				4,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1329,
			MidiNumbers = new int[6]
			{
				43,
				47,
				52,
				57,
				62,
				66
			},
			key = "G",
			suffix = "Maj13",
			Fingers = new int[6]
			{
				2,
				1,
				1,
				1,
				3,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1330,
			MidiNumbers = new int[6]
			{
				43,
				0,
				54,
				59,
				64,
				69
			},
			key = "G",
			suffix = "Maj13",
			Fingers = new int[6]
			{
				1,
				0,
				2,
				3,
				4,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1331,
			MidiNumbers = new int[6]
			{
				0,
				55,
				61,
				66,
				71,
				74
			},
			key = "G",
			suffix = "Maj7♯11",
			Fingers = new int[6]
			{
				0,
				1,
				2,
				3,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1332,
			MidiNumbers = new int[6]
			{
				43,
				0,
				54,
				59,
				61,
				0
			},
			key = "G",
			suffix = "Maj7♯11",
			Fingers = new int[6]
			{
				2,
				0,
				3,
				4,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1333,
			MidiNumbers = new int[6]
			{
				0,
				0,
				55,
				61,
				66,
				71
			},
			key = "G",
			suffix = "Maj7♯11",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				2,
				3,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1334,
			MidiNumbers = new int[6]
			{
				0,
				55,
				59,
				65,
				69,
				73
			},
			key = "G",
			suffix = "9♭5",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1335,
			MidiNumbers = new int[6]
			{
				43,
				47,
				53,
				57,
				61,
				0
			},
			key = "G",
			suffix = "9♭5",
			Fingers = new int[6]
			{
				2,
				1,
				3,
				1,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1336,
			MidiNumbers = new int[6]
			{
				43,
				49,
				53,
				58,
				65,
				69
			},
			key = "G",
			suffix = "m9♭5",
			Fingers = new int[6]
			{
				1,
				2,
				1,
				1,
				4,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1337,
			MidiNumbers = new int[6]
			{
				0,
				55,
				59,
				65,
				68,
				75
			},
			key = "G",
			suffix = "+7♭9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1338,
			MidiNumbers = new int[6]
			{
				43,
				0,
				53,
				59,
				63,
				68
			},
			key = "G",
			suffix = "+7♭9",
			Fingers = new int[6]
			{
				1,
				0,
				2,
				3,
				3,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1339,
			MidiNumbers = new int[6]
			{
				0,
				55,
				59,
				65,
				70,
				75
			},
			key = "G",
			suffix = "+7♯9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				4,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1340,
			MidiNumbers = new int[6]
			{
				43,
				0,
				53,
				59,
				63,
				70
			},
			key = "G",
			suffix = "+7♯9",
			Fingers = new int[6]
			{
				1,
				0,
				2,
				3,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1341,
			MidiNumbers = new int[6]
			{
				44,
				51,
				56,
				60,
				63,
				68
			},
			key = "A♭",
			suffix = string.Empty,
			Fingers = new int[6]
			{
				1,
				3,
				4,
				2,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1342,
			MidiNumbers = new int[6]
			{
				0,
				56,
				60,
				63,
				68,
				72
			},
			key = "A♭",
			suffix = string.Empty,
			Fingers = new int[6]
			{
				0,
				4,
				3,
				1,
				2,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1343,
			MidiNumbers = new int[6]
			{
				0,
				56,
				63,
				68,
				72,
				75
			},
			key = "A♭",
			suffix = string.Empty,
			Fingers = new int[6]
			{
				0,
				1,
				3,
				3,
				3,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1344,
			MidiNumbers = new int[6]
			{
				44,
				48,
				51,
				56,
				60,
				68
			},
			key = "A♭",
			suffix = string.Empty,
			Fingers = new int[6]
			{
				3,
				2,
				1,
				1,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1345,
			MidiNumbers = new int[6]
			{
				0,
				0,
				56,
				63,
				68,
				72
			},
			key = "A♭",
			suffix = string.Empty,
			Fingers = new int[6]
			{
				0,
				0,
				1,
				2,
				4,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1346,
			MidiNumbers = new int[6]
			{
				44,
				51,
				56,
				59,
				63,
				68
			},
			key = "A♭",
			suffix = "m",
			Fingers = new int[6]
			{
				1,
				3,
				4,
				1,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1347,
			MidiNumbers = new int[6]
			{
				0,
				0,
				56,
				63,
				68,
				71
			},
			key = "A♭",
			suffix = "m",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				4,
				2
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1348,
			MidiNumbers = new int[6]
			{
				0,
				56,
				63,
				68,
				71,
				75
			},
			key = "A♭",
			suffix = "m",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				4,
				2,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1349,
			MidiNumbers = new int[6]
			{
				0,
				0,
				56,
				59,
				63,
				68
			},
			key = "A♭",
			suffix = "m",
			Fingers = new int[6]
			{
				0,
				0,
				3,
				1,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1350,
			MidiNumbers = new int[6]
			{
				44,
				51,
				54,
				60,
				63,
				68
			},
			key = "A♭",
			suffix = "7",
			Fingers = new int[6]
			{
				1,
				3,
				1,
				2,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1351,
			MidiNumbers = new int[6]
			{
				44,
				0,
				54,
				60,
				63,
				0
			},
			key = "A♭",
			suffix = "7",
			Fingers = new int[6]
			{
				1,
				0,
				2,
				4,
				3,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1352,
			MidiNumbers = new int[6]
			{
				0,
				56,
				63,
				66,
				72,
				75
			},
			key = "A♭",
			suffix = "7",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				1,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1353,
			MidiNumbers = new int[6]
			{
				0,
				0,
				56,
				63,
				66,
				72
			},
			key = "A♭",
			suffix = "7",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				2,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1354,
			MidiNumbers = new int[6]
			{
				0,
				56,
				60,
				66,
				68,
				0
			},
			key = "A♭",
			suffix = "7",
			Fingers = new int[6]
			{
				0,
				3,
				2,
				4,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1355,
			MidiNumbers = new int[6]
			{
				44,
				46,
				51,
				56,
				63,
				68
			},
			key = "A♭",
			suffix = "sus2",
			Fingers = new int[6]
			{
				2,
				1,
				1,
				1,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1356,
			MidiNumbers = new int[6]
			{
				0,
				56,
				63,
				68,
				70,
				75
			},
			key = "A♭",
			suffix = "sus2",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				4,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1357,
			MidiNumbers = new int[6]
			{
				0,
				56,
				63,
				70,
				0,
				0
			},
			key = "A♭",
			suffix = "sus2",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				4,
				0,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1358,
			MidiNumbers = new int[6]
			{
				0,
				0,
				56,
				58,
				63,
				70
			},
			key = "A♭",
			suffix = "sus2",
			Fingers = new int[6]
			{
				0,
				0,
				3,
				1,
				2,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1359,
			MidiNumbers = new int[6]
			{
				0,
				0,
				56,
				63,
				68,
				70
			},
			key = "A♭",
			suffix = "sus2",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1360,
			MidiNumbers = new int[6]
			{
				44,
				51,
				56,
				61,
				63,
				68
			},
			key = "A♭",
			suffix = "sus4",
			Fingers = new int[6]
			{
				1,
				2,
				3,
				4,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1361,
			MidiNumbers = new int[6]
			{
				0,
				56,
				61,
				63,
				68,
				0
			},
			key = "A♭",
			suffix = "sus4",
			Fingers = new int[6]
			{
				0,
				3,
				4,
				1,
				2,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1362,
			MidiNumbers = new int[6]
			{
				0,
				56,
				63,
				68,
				73,
				75
			},
			key = "A♭",
			suffix = "sus4",
			Fingers = new int[6]
			{
				0,
				1,
				2,
				3,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1363,
			MidiNumbers = new int[6]
			{
				0,
				0,
				56,
				63,
				68,
				73
			},
			key = "A♭",
			suffix = "sus4",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				4,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1364,
			MidiNumbers = new int[6]
			{
				0,
				0,
				56,
				61,
				63,
				68
			},
			key = "A♭",
			suffix = "sus4",
			Fingers = new int[6]
			{
				0,
				0,
				3,
				4,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1365,
			MidiNumbers = new int[6]
			{
				44,
				51,
				54,
				59,
				63,
				68
			},
			key = "A♭",
			suffix = "m7",
			Fingers = new int[6]
			{
				1,
				3,
				1,
				1,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1366,
			MidiNumbers = new int[6]
			{
				44,
				0,
				54,
				59,
				63,
				0
			},
			key = "A♭",
			suffix = "m7",
			Fingers = new int[6]
			{
				1,
				0,
				2,
				3,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1367,
			MidiNumbers = new int[6]
			{
				0,
				56,
				63,
				66,
				71,
				75
			},
			key = "A♭",
			suffix = "m7",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				1,
				2,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1368,
			MidiNumbers = new int[6]
			{
				44,
				51,
				56,
				59,
				66,
				68
			},
			key = "A♭",
			suffix = "m7",
			Fingers = new int[6]
			{
				1,
				2,
				3,
				1,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1369,
			MidiNumbers = new int[6]
			{
				0,
				56,
				59,
				66,
				71,
				0
			},
			key = "A♭",
			suffix = "m7",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1370,
			MidiNumbers = new int[6]
			{
				0,
				56,
				59,
				66,
				68,
				0
			},
			key = "A♭",
			suffix = "m7",
			Fingers = new int[6]
			{
				0,
				3,
				1,
				4,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1371,
			MidiNumbers = new int[6]
			{
				0,
				0,
				56,
				63,
				66,
				71
			},
			key = "A♭",
			suffix = "m7",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				4,
				2,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1372,
			MidiNumbers = new int[6]
			{
				0,
				0,
				56,
				60,
				63,
				67
			},
			key = "A♭",
			suffix = "Maj7",
			Fingers = new int[6]
			{
				0,
				0,
				4,
				3,
				2,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1373,
			MidiNumbers = new int[6]
			{
				0,
				0,
				56,
				63,
				67,
				72
			},
			key = "A♭",
			suffix = "Maj7",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				3,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1374,
			MidiNumbers = new int[6]
			{
				0,
				56,
				63,
				67,
				72,
				75
			},
			key = "A♭",
			suffix = "Maj7",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				2,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1375,
			MidiNumbers = new int[6]
			{
				44,
				0,
				55,
				60,
				63,
				0
			},
			key = "A♭",
			suffix = "Maj7",
			Fingers = new int[6]
			{
				1,
				0,
				3,
				4,
				2,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1376,
			MidiNumbers = new int[6]
			{
				44,
				51,
				55,
				60,
				63,
				68
			},
			key = "A♭",
			suffix = "Maj7",
			Fingers = new int[6]
			{
				1,
				4,
				2,
				3,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1377,
			MidiNumbers = new int[6]
			{
				44,
				51,
				56,
				0,
				0,
				0
			},
			key = "A♭",
			suffix = "5",
			Fingers = new int[6]
			{
				1,
				3,
				4,
				0,
				0,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1378,
			MidiNumbers = new int[6]
			{
				0,
				56,
				63,
				68,
				0,
				0
			},
			key = "A♭",
			suffix = "5",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				4,
				0,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1379,
			MidiNumbers = new int[6]
			{
				0,
				0,
				0,
				56,
				63,
				68
			},
			key = "A♭",
			suffix = "5",
			Fingers = new int[6]
			{
				0,
				0,
				0,
				1,
				4,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1380,
			MidiNumbers = new int[6]
			{
				0,
				0,
				56,
				63,
				68,
				0
			},
			key = "A♭",
			suffix = "5",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1381,
			MidiNumbers = new int[6]
			{
				0,
				56,
				60,
				64,
				68,
				0
			},
			key = "A♭",
			suffix = "+",
			Fingers = new int[6]
			{
				0,
				4,
				3,
				1,
				2,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1382,
			MidiNumbers = new int[6]
			{
				0,
				0,
				56,
				60,
				64,
				68
			},
			key = "A♭",
			suffix = "+",
			Fingers = new int[6]
			{
				0,
				0,
				4,
				2,
				3,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1383,
			MidiNumbers = new int[6]
			{
				44,
				48,
				52,
				56,
				0,
				0
			},
			key = "A♭",
			suffix = "+",
			Fingers = new int[6]
			{
				4,
				3,
				2,
				1,
				0,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1384,
			MidiNumbers = new int[6]
			{
				44,
				52,
				56,
				60,
				0,
				0
			},
			key = "A♭",
			suffix = "+",
			Fingers = new int[6]
			{
				1,
				4,
				3,
				2,
				0,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1385,
			MidiNumbers = new int[6]
			{
				0,
				56,
				0,
				68,
				72,
				76
			},
			key = "A♭",
			suffix = "+",
			Fingers = new int[6]
			{
				0,
				1,
				0,
				3,
				4,
				2
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1386,
			MidiNumbers = new int[6]
			{
				0,
				56,
				62,
				68,
				71,
				0
			},
			key = "A♭",
			suffix = "°",
			Fingers = new int[6]
			{
				0,
				1,
				2,
				4,
				3,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1387,
			MidiNumbers = new int[6]
			{
				44,
				0,
				56,
				59,
				62,
				0
			},
			key = "A♭",
			suffix = "°",
			Fingers = new int[6]
			{
				2,
				0,
				4,
				3,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1388,
			MidiNumbers = new int[6]
			{
				44,
				50,
				56,
				59,
				0,
				0
			},
			key = "A♭",
			suffix = "°",
			Fingers = new int[6]
			{
				1,
				3,
				4,
				2,
				0,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1389,
			MidiNumbers = new int[6]
			{
				0,
				0,
				56,
				60,
				65,
				68
			},
			key = "A♭",
			suffix = "6",
			Fingers = new int[6]
			{
				0,
				0,
				3,
				2,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1390,
			MidiNumbers = new int[6]
			{
				0,
				0,
				56,
				63,
				65,
				72
			},
			key = "A♭",
			suffix = "6",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1391,
			MidiNumbers = new int[6]
			{
				0,
				56,
				63,
				68,
				72,
				77
			},
			key = "A♭",
			suffix = "6",
			Fingers = new int[6]
			{
				0,
				1,
				2,
				3,
				4,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1392,
			MidiNumbers = new int[6]
			{
				44,
				0,
				53,
				60,
				63,
				0
			},
			key = "A♭",
			suffix = "6",
			Fingers = new int[6]
			{
				2,
				0,
				1,
				4,
				3,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1393,
			MidiNumbers = new int[6]
			{
				44,
				51,
				54,
				60,
				63,
				70
			},
			key = "A♭",
			suffix = "9",
			Fingers = new int[6]
			{
				1,
				3,
				1,
				2,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1394,
			MidiNumbers = new int[6]
			{
				44,
				0,
				54,
				58,
				63,
				68
			},
			key = "A♭",
			suffix = "9",
			Fingers = new int[6]
			{
				2,
				0,
				3,
				1,
				4,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1395,
			MidiNumbers = new int[6]
			{
				0,
				56,
				0,
				65,
				70,
				75
			},
			key = "A♭",
			suffix = "9",
			Fingers = new int[6]
			{
				0,
				2,
				0,
				1,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1396,
			MidiNumbers = new int[6]
			{
				0,
				56,
				60,
				66,
				70,
				75
			},
			key = "A♭",
			suffix = "9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				3,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1397,
			MidiNumbers = new int[6]
			{
				44,
				0,
				54,
				58,
				60,
				0
			},
			key = "A♭",
			suffix = "9",
			Fingers = new int[6]
			{
				3,
				0,
				4,
				2,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1398,
			MidiNumbers = new int[6]
			{
				0,
				0,
				56,
				60,
				66,
				70
			},
			key = "A♭",
			suffix = "9",
			Fingers = new int[6]
			{
				0,
				0,
				2,
				1,
				4,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1399,
			MidiNumbers = new int[6]
			{
				44,
				48,
				54,
				58,
				63,
				0
			},
			key = "A♭",
			suffix = "9",
			Fingers = new int[6]
			{
				2,
				1,
				3,
				1,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1400,
			MidiNumbers = new int[6]
			{
				44,
				51,
				54,
				61,
				63,
				70
			},
			key = "A♭",
			suffix = "11",
			Fingers = new int[6]
			{
				1,
				2,
				1,
				3,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1401,
			MidiNumbers = new int[6]
			{
				0,
				56,
				60,
				66,
				68,
				73
			},
			key = "A♭",
			suffix = "11",
			Fingers = new int[6]
			{
				0,
				3,
				2,
				4,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1402,
			MidiNumbers = new int[6]
			{
				0,
				56,
				61,
				66,
				72,
				75
			},
			key = "A♭",
			suffix = "11",
			Fingers = new int[6]
			{
				0,
				1,
				1,
				1,
				3,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1403,
			MidiNumbers = new int[6]
			{
				0,
				0,
				56,
				61,
				66,
				72
			},
			key = "A♭",
			suffix = "11",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				1,
				2,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1404,
			MidiNumbers = new int[6]
			{
				44,
				0,
				54,
				58,
				60,
				65
			},
			key = "A♭",
			suffix = "13",
			Fingers = new int[6]
			{
				3,
				0,
				4,
				2,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1405,
			MidiNumbers = new int[6]
			{
				44,
				0,
				54,
				60,
				65,
				70
			},
			key = "A♭",
			suffix = "13",
			Fingers = new int[6]
			{
				1,
				0,
				1,
				1,
				2,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1406,
			MidiNumbers = new int[6]
			{
				0,
				56,
				60,
				66,
				70,
				77
			},
			key = "A♭",
			suffix = "13",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1407,
			MidiNumbers = new int[6]
			{
				44,
				51,
				54,
				60,
				65,
				68
			},
			key = "A♭",
			suffix = "13",
			Fingers = new int[6]
			{
				1,
				3,
				1,
				2,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1408,
			MidiNumbers = new int[6]
			{
				44,
				51,
				54,
				61,
				63,
				68
			},
			key = "A♭",
			suffix = "7sus4",
			Fingers = new int[6]
			{
				1,
				2,
				1,
				3,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1409,
			MidiNumbers = new int[6]
			{
				0,
				56,
				61,
				66,
				68,
				73
			},
			key = "A♭",
			suffix = "7sus4",
			Fingers = new int[6]
			{
				0,
				2,
				3,
				4,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1410,
			MidiNumbers = new int[6]
			{
				0,
				56,
				63,
				66,
				73,
				75
			},
			key = "A♭",
			suffix = "7sus4",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				1,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1411,
			MidiNumbers = new int[6]
			{
				0,
				0,
				56,
				63,
				66,
				73
			},
			key = "A♭",
			suffix = "7sus4",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				2,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1412,
			MidiNumbers = new int[6]
			{
				0,
				56,
				61,
				66,
				70,
				75
			},
			key = "A♭",
			suffix = "9sus4",
			Fingers = new int[6]
			{
				0,
				1,
				1,
				1,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1413,
			MidiNumbers = new int[6]
			{
				44,
				0,
				54,
				58,
				61,
				0
			},
			key = "A♭",
			suffix = "9sus4",
			Fingers = new int[6]
			{
				3,
				0,
				4,
				2,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1414,
			MidiNumbers = new int[6]
			{
				44,
				0,
				54,
				58,
				61,
				66
			},
			key = "A♭",
			suffix = "9sus4",
			Fingers = new int[6]
			{
				3,
				0,
				4,
				2,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1415,
			MidiNumbers = new int[6]
			{
				0,
				56,
				61,
				66,
				70,
				77
			},
			key = "A♭",
			suffix = "13sus4",
			Fingers = new int[6]
			{
				0,
				1,
				1,
				1,
				1,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1416,
			MidiNumbers = new int[6]
			{
				44,
				0,
				54,
				61,
				65,
				70
			},
			key = "A♭",
			suffix = "13sus4",
			Fingers = new int[6]
			{
				1,
				0,
				2,
				4,
				4,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1417,
			MidiNumbers = new int[6]
			{
				0,
				56,
				62,
				65,
				71,
				0
			},
			key = "A♭",
			suffix = "°7",
			Fingers = new int[6]
			{
				0,
				2,
				3,
				1,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1418,
			MidiNumbers = new int[6]
			{
				44,
				0,
				53,
				59,
				62,
				0
			},
			key = "A♭",
			suffix = "°7",
			Fingers = new int[6]
			{
				3,
				0,
				1,
				4,
				2,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1419,
			MidiNumbers = new int[6]
			{
				0,
				0,
				56,
				62,
				65,
				71
			},
			key = "A♭",
			suffix = "°7",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				2,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1420,
			MidiNumbers = new int[6]
			{
				0,
				56,
				64,
				66,
				72,
				76
			},
			key = "A♭",
			suffix = "+7",
			Fingers = new int[6]
			{
				0,
				1,
				4,
				1,
				3,
				2
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1421,
			MidiNumbers = new int[6]
			{
				44,
				0,
				54,
				60,
				64,
				0
			},
			key = "A♭",
			suffix = "+7",
			Fingers = new int[6]
			{
				1,
				0,
				2,
				3,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1422,
			MidiNumbers = new int[6]
			{
				0,
				0,
				56,
				64,
				66,
				72
			},
			key = "A♭",
			suffix = "+7",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				4,
				2,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1423,
			MidiNumbers = new int[6]
			{
				0,
				56,
				0,
				66,
				72,
				76
			},
			key = "A♭",
			suffix = "+7",
			Fingers = new int[6]
			{
				0,
				1,
				0,
				2,
				4,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1424,
			MidiNumbers = new int[6]
			{
				0,
				56,
				62,
				66,
				72,
				0
			},
			key = "A♭",
			suffix = "7♭5",
			Fingers = new int[6]
			{
				0,
				1,
				2,
				1,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1425,
			MidiNumbers = new int[6]
			{
				44,
				0,
				54,
				60,
				62,
				0
			},
			key = "A♭",
			suffix = "7♭5",
			Fingers = new int[6]
			{
				2,
				0,
				3,
				4,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1426,
			MidiNumbers = new int[6]
			{
				0,
				0,
				56,
				62,
				66,
				72
			},
			key = "A♭",
			suffix = "7♭5",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				2,
				2,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1427,
			MidiNumbers = new int[6]
			{
				0,
				56,
				62,
				66,
				72,
				78
			},
			key = "A♭",
			suffix = "7♭5",
			Fingers = new int[6]
			{
				0,
				1,
				2,
				1,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1428,
			MidiNumbers = new int[6]
			{
				0,
				56,
				60,
				66,
				71,
				0
			},
			key = "A♭",
			suffix = "7♯9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1429,
			MidiNumbers = new int[6]
			{
				44,
				51,
				54,
				60,
				63,
				71
			},
			key = "A♭",
			suffix = "7♯9",
			Fingers = new int[6]
			{
				1,
				3,
				1,
				2,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1430,
			MidiNumbers = new int[6]
			{
				0,
				0,
				56,
				60,
				66,
				71
			},
			key = "A♭",
			suffix = "7♯9",
			Fingers = new int[6]
			{
				0,
				0,
				2,
				1,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1431,
			MidiNumbers = new int[6]
			{
				0,
				56,
				60,
				66,
				71,
				74
			},
			key = "A♭",
			suffix = "7♭5♯9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1432,
			MidiNumbers = new int[6]
			{
				44,
				50,
				54,
				60,
				63,
				68
			},
			key = "A♭",
			suffix = "7♯11",
			Fingers = new int[6]
			{
				1,
				2,
				1,
				3,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1433,
			MidiNumbers = new int[6]
			{
				0,
				56,
				60,
				66,
				69,
				0
			},
			key = "A♭",
			suffix = "7♭9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1434,
			MidiNumbers = new int[6]
			{
				44,
				51,
				54,
				60,
				63,
				69
			},
			key = "A♭",
			suffix = "7♭9",
			Fingers = new int[6]
			{
				1,
				4,
				1,
				2,
				1,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1435,
			MidiNumbers = new int[6]
			{
				0,
				56,
				60,
				66,
				69,
				75
			},
			key = "A♭",
			suffix = "7♭9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1436,
			MidiNumbers = new int[6]
			{
				0,
				0,
				56,
				60,
				66,
				69
			},
			key = "A♭",
			suffix = "7♭9",
			Fingers = new int[6]
			{
				0,
				0,
				2,
				1,
				3,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1437,
			MidiNumbers = new int[6]
			{
				0,
				56,
				60,
				66,
				70,
				76
			},
			key = "A♭",
			suffix = "+9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1438,
			MidiNumbers = new int[6]
			{
				44,
				0,
				54,
				60,
				64,
				70
			},
			key = "A♭",
			suffix = "+9",
			Fingers = new int[6]
			{
				1,
				0,
				2,
				3,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1439,
			MidiNumbers = new int[6]
			{
				0,
				56,
				63,
				67,
				71,
				75
			},
			key = "A♭",
			suffix = "mMaj7",
			Fingers = new int[6]
			{
				0,
				1,
				4,
				2,
				3,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1440,
			MidiNumbers = new int[6]
			{
				44,
				0,
				55,
				59,
				63,
				0
			},
			key = "A♭",
			suffix = "mMaj7",
			Fingers = new int[6]
			{
				1,
				0,
				4,
				2,
				3,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1441,
			MidiNumbers = new int[6]
			{
				44,
				51,
				55,
				59,
				63,
				68
			},
			key = "A♭",
			suffix = "mMaj7",
			Fingers = new int[6]
			{
				1,
				3,
				2,
				1,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1442,
			MidiNumbers = new int[6]
			{
				0,
				0,
				56,
				63,
				67,
				71
			},
			key = "A♭",
			suffix = "mMaj7",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				4,
				2
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1443,
			MidiNumbers = new int[6]
			{
				0,
				0,
				56,
				59,
				63,
				67
			},
			key = "A♭",
			suffix = "mMaj7",
			Fingers = new int[6]
			{
				0,
				0,
				4,
				2,
				3,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1444,
			MidiNumbers = new int[6]
			{
				0,
				56,
				58,
				63,
				68,
				72
			},
			key = "A♭",
			suffix = "add9",
			Fingers = new int[6]
			{
				0,
				4,
				1,
				1,
				2,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1445,
			MidiNumbers = new int[6]
			{
				0,
				0,
				56,
				60,
				63,
				70
			},
			key = "A♭",
			suffix = "add9",
			Fingers = new int[6]
			{
				0,
				0,
				3,
				2,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1446,
			MidiNumbers = new int[6]
			{
				0,
				56,
				63,
				70,
				72,
				75
			},
			key = "A♭",
			suffix = "add9",
			Fingers = new int[6]
			{
				0,
				1,
				2,
				4,
				3,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1447,
			MidiNumbers = new int[6]
			{
				44,
				0,
				0,
				60,
				63,
				70
			},
			key = "A♭",
			suffix = "add9",
			Fingers = new int[6]
			{
				1,
				0,
				0,
				3,
				2,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1448,
			MidiNumbers = new int[6]
			{
				0,
				0,
				56,
				59,
				63,
				70
			},
			key = "A♭",
			suffix = "madd9",
			Fingers = new int[6]
			{
				0,
				0,
				3,
				1,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1449,
			MidiNumbers = new int[6]
			{
				44,
				51,
				56,
				59,
				65,
				68
			},
			key = "A♭",
			suffix = "m6",
			Fingers = new int[6]
			{
				1,
				2,
				3,
				1,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1450,
			MidiNumbers = new int[6]
			{
				0,
				0,
				56,
				63,
				65,
				71
			},
			key = "A♭",
			suffix = "m6",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				1,
				2
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1451,
			MidiNumbers = new int[6]
			{
				0,
				56,
				59,
				65,
				68,
				0
			},
			key = "A♭",
			suffix = "m6",
			Fingers = new int[6]
			{
				0,
				4,
				1,
				3,
				2,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1452,
			MidiNumbers = new int[6]
			{
				0,
				56,
				63,
				65,
				71,
				0
			},
			key = "A♭",
			suffix = "m6",
			Fingers = new int[6]
			{
				0,
				2,
				4,
				1,
				3,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1453,
			MidiNumbers = new int[6]
			{
				0,
				56,
				59,
				65,
				68,
				75
			},
			key = "A♭",
			suffix = "m6",
			Fingers = new int[6]
			{
				0,
				3,
				1,
				2,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1454,
			MidiNumbers = new int[6]
			{
				44,
				0,
				53,
				59,
				63,
				0
			},
			key = "A♭",
			suffix = "m6",
			Fingers = new int[6]
			{
				2,
				0,
				1,
				3,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1455,
			MidiNumbers = new int[6]
			{
				0,
				56,
				0,
				65,
				71,
				75
			},
			key = "A♭",
			suffix = "m6",
			Fingers = new int[6]
			{
				0,
				2,
				0,
				1,
				4,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1456,
			MidiNumbers = new int[6]
			{
				0,
				56,
				59,
				64,
				68,
				75
			},
			key = "A♭",
			suffix = "m♭6",
			Fingers = new int[6]
			{
				0,
				3,
				1,
				1,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1457,
			MidiNumbers = new int[6]
			{
				44,
				0,
				52,
				59,
				63,
				0
			},
			key = "A♭",
			suffix = "m♭6",
			Fingers = new int[6]
			{
				2,
				0,
				1,
				3,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1458,
			MidiNumbers = new int[6]
			{
				0,
				0,
				56,
				62,
				66,
				71
			},
			key = "A♭",
			suffix = "m7♭5",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				2,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1459,
			MidiNumbers = new int[6]
			{
				44,
				0,
				54,
				59,
				62,
				0
			},
			key = "A♭",
			suffix = "m7♭5",
			Fingers = new int[6]
			{
				2,
				0,
				3,
				4,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1460,
			MidiNumbers = new int[6]
			{
				44,
				50,
				56,
				62,
				66,
				71
			},
			key = "A♭",
			suffix = "m7♭5",
			Fingers = new int[6]
			{
				1,
				2,
				3,
				4,
				4,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1461,
			MidiNumbers = new int[6]
			{
				0,
				56,
				62,
				66,
				71,
				0
			},
			key = "A♭",
			suffix = "m7♭5",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				2,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1462,
			MidiNumbers = new int[6]
			{
				0,
				56,
				0,
				66,
				71,
				74
			},
			key = "A♭",
			suffix = "m7♭5",
			Fingers = new int[6]
			{
				0,
				2,
				0,
				3,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1463,
			MidiNumbers = new int[6]
			{
				44,
				50,
				54,
				59,
				66,
				68
			},
			key = "A♭",
			suffix = "m7♭5",
			Fingers = new int[6]
			{
				1,
				2,
				1,
				1,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1464,
			MidiNumbers = new int[6]
			{
				44,
				51,
				54,
				59,
				63,
				70
			},
			key = "A♭",
			suffix = "m9",
			Fingers = new int[6]
			{
				1,
				2,
				1,
				1,
				1,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1465,
			MidiNumbers = new int[6]
			{
				0,
				56,
				59,
				66,
				70,
				0
			},
			key = "A♭",
			suffix = "m9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1466,
			MidiNumbers = new int[6]
			{
				0,
				56,
				59,
				66,
				70,
				75
			},
			key = "A♭",
			suffix = "m9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				3,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1467,
			MidiNumbers = new int[6]
			{
				0,
				0,
				56,
				59,
				66,
				70
			},
			key = "A♭",
			suffix = "m9",
			Fingers = new int[6]
			{
				0,
				0,
				2,
				1,
				4,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1468,
			MidiNumbers = new int[6]
			{
				44,
				0,
				54,
				59,
				63,
				70
			},
			key = "A♭",
			suffix = "m9",
			Fingers = new int[6]
			{
				2,
				0,
				3,
				3,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1469,
			MidiNumbers = new int[6]
			{
				0,
				56,
				59,
				66,
				68,
				73
			},
			key = "A♭",
			suffix = "m11",
			Fingers = new int[6]
			{
				0,
				3,
				1,
				4,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1470,
			MidiNumbers = new int[6]
			{
				0,
				56,
				61,
				66,
				71,
				75
			},
			key = "A♭",
			suffix = "m11",
			Fingers = new int[6]
			{
				0,
				1,
				1,
				1,
				2,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1471,
			MidiNumbers = new int[6]
			{
				44,
				0,
				54,
				59,
				61,
				0
			},
			key = "A♭",
			suffix = "m11",
			Fingers = new int[6]
			{
				2,
				0,
				3,
				4,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1472,
			MidiNumbers = new int[6]
			{
				0,
				0,
				56,
				61,
				66,
				71
			},
			key = "A♭",
			suffix = "m11",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				1,
				2,
				2
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1473,
			MidiNumbers = new int[6]
			{
				0,
				56,
				0,
				66,
				71,
				73
			},
			key = "A♭",
			suffix = "m11",
			Fingers = new int[6]
			{
				0,
				2,
				0,
				3,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1474,
			MidiNumbers = new int[6]
			{
				0,
				56,
				0,
				66,
				71,
				77
			},
			key = "A♭",
			suffix = "m13",
			Fingers = new int[6]
			{
				0,
				1,
				0,
				2,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1475,
			MidiNumbers = new int[6]
			{
				44,
				51,
				54,
				59,
				65,
				68
			},
			key = "A♭",
			suffix = "m13",
			Fingers = new int[6]
			{
				1,
				3,
				1,
				1,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1476,
			MidiNumbers = new int[6]
			{
				44,
				0,
				54,
				59,
				65,
				70
			},
			key = "A♭",
			suffix = "m13",
			Fingers = new int[6]
			{
				2,
				0,
				3,
				3,
				4,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1477,
			MidiNumbers = new int[6]
			{
				0,
				56,
				63,
				66,
				71,
				77
			},
			key = "A♭",
			suffix = "m13",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				1,
				2,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1478,
			MidiNumbers = new int[6]
			{
				0,
				56,
				59,
				66,
				70,
				77
			},
			key = "A♭",
			suffix = "m13",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1479,
			MidiNumbers = new int[6]
			{
				0,
				56,
				59,
				67,
				70,
				0
			},
			key = "A♭",
			suffix = "mMaj9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				4,
				3,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1480,
			MidiNumbers = new int[6]
			{
				44,
				51,
				55,
				59,
				63,
				70
			},
			key = "A♭",
			suffix = "mMaj9",
			Fingers = new int[6]
			{
				1,
				3,
				2,
				1,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1481,
			MidiNumbers = new int[6]
			{
				0,
				56,
				60,
				67,
				70,
				0
			},
			key = "A♭",
			suffix = "Maj9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				4,
				3,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1482,
			MidiNumbers = new int[6]
			{
				0,
				0,
				56,
				60,
				67,
				70
			},
			key = "A♭",
			suffix = "Maj9",
			Fingers = new int[6]
			{
				0,
				0,
				2,
				1,
				4,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1483,
			MidiNumbers = new int[6]
			{
				0,
				56,
				58,
				63,
				67,
				72
			},
			key = "A♭",
			suffix = "Maj9",
			Fingers = new int[6]
			{
				0,
				3,
				1,
				1,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1484,
			MidiNumbers = new int[6]
			{
				44,
				48,
				55,
				58,
				63,
				67
			},
			key = "A♭",
			suffix = "Maj9",
			Fingers = new int[6]
			{
				2,
				1,
				4,
				1,
				3,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1485,
			MidiNumbers = new int[6]
			{
				44,
				0,
				55,
				58,
				63,
				0
			},
			key = "A♭",
			suffix = "Maj9",
			Fingers = new int[6]
			{
				2,
				0,
				4,
				1,
				3,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1486,
			MidiNumbers = new int[6]
			{
				0,
				56,
				0,
				67,
				72,
				77
			},
			key = "A♭",
			suffix = "Maj13",
			Fingers = new int[6]
			{
				0,
				1,
				0,
				2,
				4,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1487,
			MidiNumbers = new int[6]
			{
				44,
				48,
				53,
				58,
				63,
				67
			},
			key = "A♭",
			suffix = "Maj13",
			Fingers = new int[6]
			{
				2,
				1,
				1,
				1,
				3,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1488,
			MidiNumbers = new int[6]
			{
				44,
				0,
				55,
				60,
				65,
				70
			},
			key = "A♭",
			suffix = "Maj13",
			Fingers = new int[6]
			{
				1,
				0,
				2,
				3,
				4,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1489,
			MidiNumbers = new int[6]
			{
				0,
				56,
				62,
				67,
				72,
				75
			},
			key = "A♭",
			suffix = "Maj7♯11",
			Fingers = new int[6]
			{
				0,
				1,
				2,
				3,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1490,
			MidiNumbers = new int[6]
			{
				44,
				0,
				55,
				60,
				62,
				0
			},
			key = "A♭",
			suffix = "Maj7♯11",
			Fingers = new int[6]
			{
				2,
				0,
				3,
				4,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1491,
			MidiNumbers = new int[6]
			{
				0,
				0,
				56,
				62,
				67,
				72
			},
			key = "A♭",
			suffix = "Maj7♯11",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				2,
				3,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1492,
			MidiNumbers = new int[6]
			{
				0,
				56,
				60,
				66,
				70,
				74
			},
			key = "A♭",
			suffix = "9♭5",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1493,
			MidiNumbers = new int[6]
			{
				44,
				48,
				54,
				58,
				62,
				0
			},
			key = "A♭",
			suffix = "9♭5",
			Fingers = new int[6]
			{
				2,
				1,
				3,
				1,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1494,
			MidiNumbers = new int[6]
			{
				44,
				50,
				54,
				59,
				66,
				70
			},
			key = "A♭",
			suffix = "m9♭5",
			Fingers = new int[6]
			{
				1,
				2,
				1,
				1,
				4,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1495,
			MidiNumbers = new int[6]
			{
				0,
				56,
				60,
				66,
				69,
				76
			},
			key = "A♭",
			suffix = "+7♭9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1496,
			MidiNumbers = new int[6]
			{
				44,
				0,
				54,
				60,
				64,
				69
			},
			key = "A♭",
			suffix = "+7♭9",
			Fingers = new int[6]
			{
				1,
				0,
				2,
				3,
				3,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1497,
			MidiNumbers = new int[6]
			{
				0,
				56,
				60,
				66,
				71,
				76
			},
			key = "A♭",
			suffix = "+7♯9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				4,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1498,
			MidiNumbers = new int[6]
			{
				44,
				0,
				54,
				60,
				64,
				71
			},
			key = "A♭",
			suffix = "+7♯9",
			Fingers = new int[6]
			{
				1,
				0,
				2,
				3,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1499,
			MidiNumbers = new int[6]
			{
				45,
				52,
				57,
				61,
				64,
				69
			},
			key = "A",
			suffix = string.Empty,
			Fingers = new int[6]
			{
				1,
				3,
				4,
				2,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1500,
			MidiNumbers = new int[6]
			{
				0,
				45,
				57,
				61,
				64,
				69
			},
			key = "A",
			suffix = string.Empty,
			Fingers = new int[6]
			{
				0,
				0,
				3,
				2,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1501,
			MidiNumbers = new int[6]
			{
				0,
				57,
				61,
				64,
				69,
				73
			},
			key = "A",
			suffix = string.Empty,
			Fingers = new int[6]
			{
				0,
				4,
				3,
				1,
				2,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1502,
			MidiNumbers = new int[6]
			{
				0,
				57,
				64,
				69,
				73,
				76
			},
			key = "A",
			suffix = string.Empty,
			Fingers = new int[6]
			{
				0,
				1,
				2,
				3,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1503,
			MidiNumbers = new int[6]
			{
				0,
				45,
				52,
				57,
				61,
				64
			},
			key = "A",
			suffix = string.Empty,
			Fingers = new int[6]
			{
				0,
				0,
				1,
				2,
				3,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1504,
			MidiNumbers = new int[6]
			{
				45,
				49,
				52,
				57,
				61,
				69
			},
			key = "A",
			suffix = string.Empty,
			Fingers = new int[6]
			{
				3,
				2,
				1,
				1,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1505,
			MidiNumbers = new int[6]
			{
				0,
				0,
				57,
				64,
				69,
				73
			},
			key = "A",
			suffix = string.Empty,
			Fingers = new int[6]
			{
				0,
				0,
				1,
				2,
				4,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1506,
			MidiNumbers = new int[6]
			{
				0,
				45,
				52,
				57,
				60,
				64
			},
			key = "A",
			suffix = "m",
			Fingers = new int[6]
			{
				0,
				0,
				2,
				3,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1507,
			MidiNumbers = new int[6]
			{
				45,
				52,
				57,
				60,
				64,
				69
			},
			key = "A",
			suffix = "m",
			Fingers = new int[6]
			{
				1,
				3,
				4,
				1,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1508,
			MidiNumbers = new int[6]
			{
				0,
				45,
				52,
				60,
				64,
				69
			},
			key = "A",
			suffix = "m",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				3,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1509,
			MidiNumbers = new int[6]
			{
				0,
				0,
				57,
				64,
				69,
				72
			},
			key = "A",
			suffix = "m",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				4,
				2
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1510,
			MidiNumbers = new int[6]
			{
				0,
				57,
				64,
				69,
				72,
				76
			},
			key = "A",
			suffix = "m",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				4,
				2,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1511,
			MidiNumbers = new int[6]
			{
				0,
				0,
				57,
				60,
				64,
				69
			},
			key = "A",
			suffix = "m",
			Fingers = new int[6]
			{
				0,
				0,
				3,
				1,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1512,
			MidiNumbers = new int[6]
			{
				0,
				45,
				52,
				55,
				61,
				64
			},
			key = "A",
			suffix = "7",
			Fingers = new int[6]
			{
				0,
				0,
				2,
				0,
				3,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1513,
			MidiNumbers = new int[6]
			{
				0,
				45,
				52,
				57,
				61,
				67
			},
			key = "A",
			suffix = "7",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				1,
				1,
				2
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1514,
			MidiNumbers = new int[6]
			{
				45,
				52,
				55,
				61,
				64,
				69
			},
			key = "A",
			suffix = "7",
			Fingers = new int[6]
			{
				1,
				3,
				1,
				2,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1515,
			MidiNumbers = new int[6]
			{
				45,
				0,
				55,
				61,
				64,
				0
			},
			key = "A",
			suffix = "7",
			Fingers = new int[6]
			{
				1,
				0,
				2,
				4,
				3,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1516,
			MidiNumbers = new int[6]
			{
				0,
				45,
				57,
				64,
				67,
				73
			},
			key = "A",
			suffix = "7",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				2,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1517,
			MidiNumbers = new int[6]
			{
				0,
				57,
				64,
				67,
				73,
				76
			},
			key = "A",
			suffix = "7",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				1,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1518,
			MidiNumbers = new int[6]
			{
				0,
				0,
				57,
				64,
				67,
				73
			},
			key = "A",
			suffix = "7",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				2,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1519,
			MidiNumbers = new int[6]
			{
				0,
				57,
				61,
				67,
				69,
				0
			},
			key = "A",
			suffix = "7",
			Fingers = new int[6]
			{
				0,
				3,
				2,
				4,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1520,
			MidiNumbers = new int[6]
			{
				0,
				45,
				52,
				57,
				59,
				64
			},
			key = "A",
			suffix = "sus2",
			Fingers = new int[6]
			{
				0,
				0,
				2,
				3,
				0,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1521,
			MidiNumbers = new int[6]
			{
				0,
				57,
				64,
				69,
				71,
				76
			},
			key = "A",
			suffix = "sus2",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				4,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1522,
			MidiNumbers = new int[6]
			{
				0,
				57,
				64,
				71,
				0,
				0
			},
			key = "A",
			suffix = "sus2",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				4,
				0,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1523,
			MidiNumbers = new int[6]
			{
				0,
				0,
				57,
				59,
				64,
				71
			},
			key = "A",
			suffix = "sus2",
			Fingers = new int[6]
			{
				0,
				0,
				3,
				1,
				2,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1524,
			MidiNumbers = new int[6]
			{
				0,
				0,
				57,
				64,
				69,
				71
			},
			key = "A",
			suffix = "sus2",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1525,
			MidiNumbers = new int[6]
			{
				0,
				45,
				52,
				57,
				62,
				64
			},
			key = "A",
			suffix = "sus4",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				2,
				3,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1526,
			MidiNumbers = new int[6]
			{
				45,
				52,
				57,
				62,
				64,
				69
			},
			key = "A",
			suffix = "sus4",
			Fingers = new int[6]
			{
				1,
				2,
				3,
				4,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1527,
			MidiNumbers = new int[6]
			{
				0,
				57,
				62,
				64,
				69,
				0
			},
			key = "A",
			suffix = "sus4",
			Fingers = new int[6]
			{
				0,
				3,
				4,
				1,
				2,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1528,
			MidiNumbers = new int[6]
			{
				0,
				57,
				64,
				69,
				74,
				76
			},
			key = "A",
			suffix = "sus4",
			Fingers = new int[6]
			{
				0,
				1,
				2,
				3,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1529,
			MidiNumbers = new int[6]
			{
				0,
				0,
				57,
				64,
				69,
				74
			},
			key = "A",
			suffix = "sus4",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				4,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1530,
			MidiNumbers = new int[6]
			{
				0,
				0,
				57,
				62,
				64,
				69
			},
			key = "A",
			suffix = "sus4",
			Fingers = new int[6]
			{
				0,
				0,
				3,
				4,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1531,
			MidiNumbers = new int[6]
			{
				0,
				45,
				52,
				55,
				60,
				64
			},
			key = "A",
			suffix = "m7",
			Fingers = new int[6]
			{
				0,
				0,
				2,
				0,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1532,
			MidiNumbers = new int[6]
			{
				0,
				45,
				52,
				57,
				60,
				67
			},
			key = "A",
			suffix = "m7",
			Fingers = new int[6]
			{
				0,
				0,
				2,
				3,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1533,
			MidiNumbers = new int[6]
			{
				45,
				52,
				55,
				60,
				64,
				69
			},
			key = "A",
			suffix = "m7",
			Fingers = new int[6]
			{
				1,
				3,
				1,
				1,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1534,
			MidiNumbers = new int[6]
			{
				45,
				45,
				55,
				60,
				64,
				64
			},
			key = "A",
			suffix = "m7",
			Fingers = new int[6]
			{
				1,
				0,
				2,
				3,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1535,
			MidiNumbers = new int[6]
			{
				0,
				57,
				64,
				67,
				0,
				76
			},
			key = "A",
			suffix = "m7",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				1,
				0,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1536,
			MidiNumbers = new int[6]
			{
				0,
				57,
				64,
				67,
				72,
				76
			},
			key = "A",
			suffix = "m7",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				1,
				2,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1537,
			MidiNumbers = new int[6]
			{
				45,
				52,
				57,
				60,
				67,
				69
			},
			key = "A",
			suffix = "m7",
			Fingers = new int[6]
			{
				1,
				2,
				3,
				1,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1538,
			MidiNumbers = new int[6]
			{
				45,
				0,
				55,
				60,
				64,
				0
			},
			key = "A",
			suffix = "m7",
			Fingers = new int[6]
			{
				1,
				0,
				2,
				3,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1539,
			MidiNumbers = new int[6]
			{
				0,
				57,
				60,
				67,
				72,
				0
			},
			key = "A",
			suffix = "m7",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1540,
			MidiNumbers = new int[6]
			{
				0,
				57,
				60,
				67,
				69,
				0
			},
			key = "A",
			suffix = "m7",
			Fingers = new int[6]
			{
				0,
				3,
				1,
				4,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1541,
			MidiNumbers = new int[6]
			{
				0,
				0,
				57,
				64,
				67,
				72
			},
			key = "A",
			suffix = "m7",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				4,
				2,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1542,
			MidiNumbers = new int[6]
			{
				0,
				45,
				52,
				56,
				61,
				64
			},
			key = "A",
			suffix = "Maj7",
			Fingers = new int[6]
			{
				0,
				0,
				2,
				1,
				3,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1543,
			MidiNumbers = new int[6]
			{
				0,
				0,
				57,
				61,
				64,
				68
			},
			key = "A",
			suffix = "Maj7",
			Fingers = new int[6]
			{
				0,
				0,
				4,
				3,
				2,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1544,
			MidiNumbers = new int[6]
			{
				0,
				0,
				57,
				64,
				68,
				73
			},
			key = "A",
			suffix = "Maj7",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				2,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1545,
			MidiNumbers = new int[6]
			{
				45,
				0,
				56,
				61,
				64,
				0
			},
			key = "A",
			suffix = "Maj7",
			Fingers = new int[6]
			{
				1,
				0,
				3,
				4,
				2,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1546,
			MidiNumbers = new int[6]
			{
				0,
				57,
				64,
				68,
				73,
				76
			},
			key = "A",
			suffix = "Maj7",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				2,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1547,
			MidiNumbers = new int[6]
			{
				45,
				52,
				56,
				61,
				64,
				69
			},
			key = "A",
			suffix = "Maj7",
			Fingers = new int[6]
			{
				1,
				4,
				2,
				3,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1548,
			MidiNumbers = new int[6]
			{
				0,
				45,
				52,
				57,
				0,
				0
			},
			key = "A",
			suffix = "5",
			Fingers = new int[6]
			{
				0,
				0,
				2,
				3,
				0,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1549,
			MidiNumbers = new int[6]
			{
				45,
				52,
				57,
				0,
				0,
				0
			},
			key = "A",
			suffix = "5",
			Fingers = new int[6]
			{
				1,
				3,
				4,
				0,
				0,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1550,
			MidiNumbers = new int[6]
			{
				0,
				57,
				64,
				69,
				0,
				0
			},
			key = "A",
			suffix = "5",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				4,
				0,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1551,
			MidiNumbers = new int[6]
			{
				0,
				0,
				0,
				57,
				64,
				69
			},
			key = "A",
			suffix = "5",
			Fingers = new int[6]
			{
				0,
				0,
				0,
				1,
				4,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1552,
			MidiNumbers = new int[6]
			{
				0,
				0,
				57,
				64,
				69,
				0
			},
			key = "A",
			suffix = "5",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1553,
			MidiNumbers = new int[6]
			{
				0,
				57,
				61,
				65,
				69,
				0
			},
			key = "A",
			suffix = "+",
			Fingers = new int[6]
			{
				0,
				4,
				3,
				1,
				2,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1554,
			MidiNumbers = new int[6]
			{
				0,
				0,
				57,
				61,
				65,
				69
			},
			key = "A",
			suffix = "+",
			Fingers = new int[6]
			{
				0,
				0,
				4,
				2,
				3,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1555,
			MidiNumbers = new int[6]
			{
				45,
				49,
				53,
				57,
				0,
				0
			},
			key = "A",
			suffix = "+",
			Fingers = new int[6]
			{
				4,
				3,
				2,
				1,
				0,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1556,
			MidiNumbers = new int[6]
			{
				45,
				53,
				57,
				61,
				0,
				0
			},
			key = "A",
			suffix = "+",
			Fingers = new int[6]
			{
				1,
				4,
				3,
				2,
				0,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1557,
			MidiNumbers = new int[6]
			{
				0,
				57,
				0,
				69,
				73,
				77
			},
			key = "A",
			suffix = "+",
			Fingers = new int[6]
			{
				0,
				1,
				0,
				3,
				4,
				2
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1558,
			MidiNumbers = new int[6]
			{
				0,
				57,
				63,
				69,
				72,
				0
			},
			key = "A",
			suffix = "°",
			Fingers = new int[6]
			{
				0,
				1,
				2,
				4,
				3,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1559,
			MidiNumbers = new int[6]
			{
				45,
				0,
				57,
				60,
				63,
				0
			},
			key = "A",
			suffix = "°",
			Fingers = new int[6]
			{
				2,
				0,
				4,
				3,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1560,
			MidiNumbers = new int[6]
			{
				45,
				51,
				57,
				60,
				0,
				0
			},
			key = "A",
			suffix = "°",
			Fingers = new int[6]
			{
				1,
				3,
				4,
				2,
				0,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1561,
			MidiNumbers = new int[6]
			{
				0,
				45,
				52,
				57,
				61,
				66
			},
			key = "A",
			suffix = "6",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				1,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1562,
			MidiNumbers = new int[6]
			{
				0,
				45,
				57,
				64,
				66,
				73
			},
			key = "A",
			suffix = "6",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				2,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1563,
			MidiNumbers = new int[6]
			{
				0,
				0,
				57,
				61,
				66,
				69
			},
			key = "A",
			suffix = "6",
			Fingers = new int[6]
			{
				0,
				0,
				3,
				2,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1564,
			MidiNumbers = new int[6]
			{
				0,
				45,
				54,
				57,
				61,
				64
			},
			key = "A",
			suffix = "6",
			Fingers = new int[6]
			{
				0,
				0,
				4,
				1,
				2,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1565,
			MidiNumbers = new int[6]
			{
				0,
				45,
				61,
				66,
				69,
				64
			},
			key = "A",
			suffix = "6",
			Fingers = new int[6]
			{
				0,
				0,
				2,
				3,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1566,
			MidiNumbers = new int[6]
			{
				0,
				57,
				64,
				69,
				73,
				78
			},
			key = "A",
			suffix = "6",
			Fingers = new int[6]
			{
				0,
				1,
				2,
				3,
				4,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1567,
			MidiNumbers = new int[6]
			{
				0,
				0,
				57,
				64,
				66,
				73
			},
			key = "A",
			suffix = "6",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1568,
			MidiNumbers = new int[6]
			{
				45,
				0,
				54,
				61,
				64,
				0
			},
			key = "A",
			suffix = "6",
			Fingers = new int[6]
			{
				2,
				0,
				1,
				4,
				3,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1569,
			MidiNumbers = new int[6]
			{
				0,
				45,
				52,
				59,
				61,
				67
			},
			key = "A",
			suffix = "9",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				4,
				2,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1570,
			MidiNumbers = new int[6]
			{
				0,
				45,
				55,
				59,
				64,
				69
			},
			key = "A",
			suffix = "9",
			Fingers = new int[6]
			{
				0,
				0,
				2,
				1,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1571,
			MidiNumbers = new int[6]
			{
				0,
				45,
				55,
				61,
				64,
				71
			},
			key = "A",
			suffix = "9",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				2,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1572,
			MidiNumbers = new int[6]
			{
				45,
				52,
				55,
				61,
				64,
				71
			},
			key = "A",
			suffix = "9",
			Fingers = new int[6]
			{
				1,
				3,
				1,
				2,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1573,
			MidiNumbers = new int[6]
			{
				0,
				57,
				61,
				67,
				71,
				0
			},
			key = "A",
			suffix = "9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1574,
			MidiNumbers = new int[6]
			{
				0,
				57,
				61,
				67,
				71,
				76
			},
			key = "A",
			suffix = "9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				3,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1575,
			MidiNumbers = new int[6]
			{
				45,
				0,
				55,
				59,
				61,
				0
			},
			key = "A",
			suffix = "9",
			Fingers = new int[6]
			{
				3,
				0,
				4,
				2,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1576,
			MidiNumbers = new int[6]
			{
				0,
				0,
				57,
				61,
				67,
				71
			},
			key = "A",
			suffix = "9",
			Fingers = new int[6]
			{
				0,
				0,
				2,
				1,
				4,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1577,
			MidiNumbers = new int[6]
			{
				45,
				49,
				55,
				59,
				64,
				0
			},
			key = "A",
			suffix = "9",
			Fingers = new int[6]
			{
				2,
				1,
				3,
				1,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1578,
			MidiNumbers = new int[6]
			{
				45,
				52,
				55,
				62,
				64,
				71
			},
			key = "A",
			suffix = "11",
			Fingers = new int[6]
			{
				1,
				2,
				1,
				3,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1579,
			MidiNumbers = new int[6]
			{
				0,
				45,
				55,
				59,
				62,
				67
			},
			key = "A",
			suffix = "11",
			Fingers = new int[6]
			{
				0,
				0,
				3,
				2,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1580,
			MidiNumbers = new int[6]
			{
				0,
				57,
				61,
				67,
				69,
				74
			},
			key = "A",
			suffix = "11",
			Fingers = new int[6]
			{
				0,
				3,
				2,
				4,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1581,
			MidiNumbers = new int[6]
			{
				0,
				57,
				62,
				67,
				73,
				76
			},
			key = "A",
			suffix = "11",
			Fingers = new int[6]
			{
				0,
				1,
				1,
				1,
				3,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1582,
			MidiNumbers = new int[6]
			{
				0,
				0,
				57,
				62,
				67,
				73
			},
			key = "A",
			suffix = "11",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				1,
				2,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1583,
			MidiNumbers = new int[6]
			{
				0,
				45,
				55,
				59,
				61,
				66
			},
			key = "A",
			suffix = "13",
			Fingers = new int[6]
			{
				0,
				0,
				4,
				3,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1584,
			MidiNumbers = new int[6]
			{
				45,
				0,
				55,
				61,
				66,
				71
			},
			key = "A",
			suffix = "13",
			Fingers = new int[6]
			{
				1,
				0,
				2,
				3,
				4,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1585,
			MidiNumbers = new int[6]
			{
				0,
				57,
				61,
				67,
				71,
				78
			},
			key = "A",
			suffix = "13",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1586,
			MidiNumbers = new int[6]
			{
				45,
				0,
				55,
				59,
				61,
				66
			},
			key = "A",
			suffix = "13",
			Fingers = new int[6]
			{
				3,
				0,
				4,
				2,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1587,
			MidiNumbers = new int[6]
			{
				45,
				52,
				55,
				61,
				66,
				69
			},
			key = "A",
			suffix = "13",
			Fingers = new int[6]
			{
				1,
				3,
				1,
				2,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1588,
			MidiNumbers = new int[6]
			{
				0,
				45,
				52,
				55,
				62,
				64
			},
			key = "A",
			suffix = "7sus4",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				0,
				3,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1589,
			MidiNumbers = new int[6]
			{
				0,
				45,
				52,
				55,
				62,
				67
			},
			key = "A",
			suffix = "7sus4",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				0,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1590,
			MidiNumbers = new int[6]
			{
				45,
				52,
				55,
				62,
				64,
				69
			},
			key = "A",
			suffix = "7sus4",
			Fingers = new int[6]
			{
				1,
				3,
				1,
				4,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1591,
			MidiNumbers = new int[6]
			{
				0,
				57,
				62,
				67,
				69,
				74
			},
			key = "A",
			suffix = "7sus4",
			Fingers = new int[6]
			{
				0,
				2,
				3,
				4,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1592,
			MidiNumbers = new int[6]
			{
				0,
				57,
				64,
				67,
				74,
				76
			},
			key = "A",
			suffix = "7sus4",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				1,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1593,
			MidiNumbers = new int[6]
			{
				0,
				0,
				57,
				64,
				67,
				74
			},
			key = "A",
			suffix = "7sus4",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				2,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1594,
			MidiNumbers = new int[6]
			{
				0,
				57,
				62,
				67,
				71,
				76
			},
			key = "A",
			suffix = "9sus4",
			Fingers = new int[6]
			{
				0,
				1,
				1,
				1,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1595,
			MidiNumbers = new int[6]
			{
				45,
				0,
				55,
				59,
				62,
				0
			},
			key = "A",
			suffix = "9sus4",
			Fingers = new int[6]
			{
				3,
				0,
				4,
				2,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1596,
			MidiNumbers = new int[6]
			{
				45,
				0,
				55,
				59,
				62,
				67
			},
			key = "A",
			suffix = "9sus4",
			Fingers = new int[6]
			{
				3,
				0,
				4,
				2,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1597,
			MidiNumbers = new int[6]
			{
				0,
				57,
				62,
				67,
				71,
				78
			},
			key = "A",
			suffix = "13sus4",
			Fingers = new int[6]
			{
				0,
				1,
				1,
				1,
				1,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1598,
			MidiNumbers = new int[6]
			{
				45,
				0,
				55,
				62,
				66,
				71
			},
			key = "A",
			suffix = "13sus4",
			Fingers = new int[6]
			{
				1,
				0,
				2,
				4,
				4,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1599,
			MidiNumbers = new int[6]
			{
				0,
				0,
				57,
				63,
				66,
				72
			},
			key = "A",
			suffix = "°7",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				2,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1600,
			MidiNumbers = new int[6]
			{
				45,
				0,
				54,
				60,
				63,
				0
			},
			key = "A",
			suffix = "°7",
			Fingers = new int[6]
			{
				3,
				0,
				1,
				4,
				2,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1601,
			MidiNumbers = new int[6]
			{
				0,
				57,
				63,
				66,
				72,
				0
			},
			key = "A",
			suffix = "°7",
			Fingers = new int[6]
			{
				0,
				2,
				3,
				1,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1602,
			MidiNumbers = new int[6]
			{
				0,
				45,
				51,
				57,
				60,
				66
			},
			key = "A",
			suffix = "°7",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				2,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1603,
			MidiNumbers = new int[6]
			{
				0,
				57,
				65,
				67,
				73,
				77
			},
			key = "A",
			suffix = "+7",
			Fingers = new int[6]
			{
				0,
				1,
				4,
				1,
				3,
				2
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1604,
			MidiNumbers = new int[6]
			{
				45,
				0,
				55,
				61,
				65,
				0
			},
			key = "A",
			suffix = "+7",
			Fingers = new int[6]
			{
				1,
				0,
				2,
				3,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1605,
			MidiNumbers = new int[6]
			{
				0,
				0,
				57,
				65,
				67,
				73
			},
			key = "A",
			suffix = "+7",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				4,
				2,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1606,
			MidiNumbers = new int[6]
			{
				0,
				57,
				0,
				67,
				73,
				77
			},
			key = "A",
			suffix = "+7",
			Fingers = new int[6]
			{
				0,
				1,
				0,
				2,
				4,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1607,
			MidiNumbers = new int[6]
			{
				0,
				57,
				63,
				67,
				73,
				0
			},
			key = "A",
			suffix = "7♭5",
			Fingers = new int[6]
			{
				0,
				1,
				2,
				1,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1608,
			MidiNumbers = new int[6]
			{
				45,
				0,
				55,
				61,
				63,
				0
			},
			key = "A",
			suffix = "7♭5",
			Fingers = new int[6]
			{
				2,
				0,
				3,
				4,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1609,
			MidiNumbers = new int[6]
			{
				0,
				0,
				57,
				63,
				67,
				73
			},
			key = "A",
			suffix = "7♭5",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				2,
				2,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1610,
			MidiNumbers = new int[6]
			{
				0,
				57,
				63,
				67,
				73,
				79
			},
			key = "A",
			suffix = "7♭5",
			Fingers = new int[6]
			{
				0,
				1,
				2,
				1,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1611,
			MidiNumbers = new int[6]
			{
				0,
				57,
				61,
				67,
				72,
				0
			},
			key = "A",
			suffix = "7♯9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1612,
			MidiNumbers = new int[6]
			{
				45,
				52,
				55,
				61,
				64,
				72
			},
			key = "A",
			suffix = "7♯9",
			Fingers = new int[6]
			{
				1,
				3,
				1,
				2,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1613,
			MidiNumbers = new int[6]
			{
				0,
				0,
				57,
				61,
				67,
				72
			},
			key = "A",
			suffix = "7♯9",
			Fingers = new int[6]
			{
				0,
				0,
				2,
				1,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1614,
			MidiNumbers = new int[6]
			{
				0,
				57,
				61,
				67,
				72,
				75
			},
			key = "A",
			suffix = "7♭5♯9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1615,
			MidiNumbers = new int[6]
			{
				45,
				51,
				55,
				61,
				64,
				69
			},
			key = "A",
			suffix = "7♯11",
			Fingers = new int[6]
			{
				1,
				2,
				1,
				3,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1616,
			MidiNumbers = new int[6]
			{
				0,
				57,
				61,
				67,
				70,
				0
			},
			key = "A",
			suffix = "7♭9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1617,
			MidiNumbers = new int[6]
			{
				45,
				52,
				55,
				61,
				64,
				70
			},
			key = "A",
			suffix = "7♭9",
			Fingers = new int[6]
			{
				1,
				4,
				1,
				2,
				1,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1618,
			MidiNumbers = new int[6]
			{
				0,
				57,
				61,
				67,
				70,
				76
			},
			key = "A",
			suffix = "7♭9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1619,
			MidiNumbers = new int[6]
			{
				0,
				0,
				57,
				61,
				67,
				70
			},
			key = "A",
			suffix = "7♭9",
			Fingers = new int[6]
			{
				0,
				0,
				2,
				1,
				3,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1620,
			MidiNumbers = new int[6]
			{
				0,
				57,
				61,
				67,
				71,
				77
			},
			key = "A",
			suffix = "+9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1621,
			MidiNumbers = new int[6]
			{
				45,
				0,
				55,
				61,
				65,
				71
			},
			key = "A",
			suffix = "+9",
			Fingers = new int[6]
			{
				1,
				0,
				2,
				3,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1622,
			MidiNumbers = new int[6]
			{
				0,
				57,
				64,
				68,
				72,
				76
			},
			key = "A",
			suffix = "mMaj7",
			Fingers = new int[6]
			{
				0,
				1,
				4,
				2,
				3,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1623,
			MidiNumbers = new int[6]
			{
				45,
				0,
				56,
				60,
				64,
				0
			},
			key = "A",
			suffix = "mMaj7",
			Fingers = new int[6]
			{
				1,
				0,
				4,
				2,
				3,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1624,
			MidiNumbers = new int[6]
			{
				45,
				52,
				56,
				60,
				64,
				69
			},
			key = "A",
			suffix = "mMaj7",
			Fingers = new int[6]
			{
				1,
				3,
				2,
				1,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1625,
			MidiNumbers = new int[6]
			{
				0,
				0,
				57,
				64,
				68,
				72
			},
			key = "A",
			suffix = "mMaj7",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				4,
				2
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1626,
			MidiNumbers = new int[6]
			{
				0,
				0,
				57,
				60,
				64,
				68
			},
			key = "A",
			suffix = "mMaj7",
			Fingers = new int[6]
			{
				0,
				0,
				4,
				2,
				3,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1627,
			MidiNumbers = new int[6]
			{
				0,
				45,
				52,
				59,
				61,
				64
			},
			key = "A",
			suffix = "add9",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				4,
				2,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1628,
			MidiNumbers = new int[6]
			{
				0,
				45,
				57,
				61,
				59,
				64
			},
			key = "A",
			suffix = "add9",
			Fingers = new int[6]
			{
				0,
				0,
				2,
				1,
				0,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1629,
			MidiNumbers = new int[6]
			{
				0,
				0,
				57,
				61,
				64,
				71
			},
			key = "A",
			suffix = "add9",
			Fingers = new int[6]
			{
				0,
				0,
				3,
				2,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1630,
			MidiNumbers = new int[6]
			{
				0,
				57,
				64,
				71,
				73,
				76
			},
			key = "A",
			suffix = "add9",
			Fingers = new int[6]
			{
				0,
				1,
				2,
				4,
				3,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1631,
			MidiNumbers = new int[6]
			{
				45,
				0,
				0,
				61,
				64,
				71
			},
			key = "A",
			suffix = "add9",
			Fingers = new int[6]
			{
				1,
				0,
				0,
				3,
				2,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1632,
			MidiNumbers = new int[6]
			{
				0,
				0,
				57,
				60,
				64,
				71
			},
			key = "A",
			suffix = "madd9",
			Fingers = new int[6]
			{
				0,
				0,
				3,
				1,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1633,
			MidiNumbers = new int[6]
			{
				0,
				45,
				52,
				57,
				60,
				66
			},
			key = "A",
			suffix = "m6",
			Fingers = new int[6]
			{
				0,
				0,
				2,
				3,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1634,
			MidiNumbers = new int[6]
			{
				45,
				52,
				57,
				60,
				66,
				64
			},
			key = "A",
			suffix = "m6",
			Fingers = new int[6]
			{
				1,
				2,
				3,
				1,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1635,
			MidiNumbers = new int[6]
			{
				0,
				0,
				57,
				64,
				66,
				72
			},
			key = "A",
			suffix = "m6",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				1,
				2
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1636,
			MidiNumbers = new int[6]
			{
				0,
				57,
				60,
				66,
				69,
				0
			},
			key = "A",
			suffix = "m6",
			Fingers = new int[6]
			{
				0,
				4,
				1,
				3,
				2,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1637,
			MidiNumbers = new int[6]
			{
				45,
				0,
				54,
				60,
				64,
				0
			},
			key = "A",
			suffix = "m6",
			Fingers = new int[6]
			{
				2,
				0,
				1,
				3,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1638,
			MidiNumbers = new int[6]
			{
				45,
				52,
				57,
				60,
				66,
				69
			},
			key = "A",
			suffix = "m6",
			Fingers = new int[6]
			{
				1,
				2,
				3,
				1,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1639,
			MidiNumbers = new int[6]
			{
				0,
				57,
				64,
				66,
				72,
				0
			},
			key = "A",
			suffix = "m6",
			Fingers = new int[6]
			{
				0,
				2,
				4,
				1,
				3,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1640,
			MidiNumbers = new int[6]
			{
				0,
				57,
				60,
				66,
				69,
				76
			},
			key = "A",
			suffix = "m6",
			Fingers = new int[6]
			{
				0,
				3,
				1,
				2,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1641,
			MidiNumbers = new int[6]
			{
				0,
				57,
				0,
				66,
				72,
				76
			},
			key = "A",
			suffix = "m6",
			Fingers = new int[6]
			{
				0,
				2,
				0,
				1,
				4,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1642,
			MidiNumbers = new int[6]
			{
				0,
				57,
				60,
				65,
				69,
				76
			},
			key = "A",
			suffix = "m♭6",
			Fingers = new int[6]
			{
				0,
				3,
				1,
				1,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1643,
			MidiNumbers = new int[6]
			{
				45,
				0,
				53,
				60,
				64,
				0
			},
			key = "A",
			suffix = "m♭6",
			Fingers = new int[6]
			{
				2,
				0,
				1,
				3,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1644,
			MidiNumbers = new int[6]
			{
				45,
				0,
				55,
				60,
				63,
				0
			},
			key = "A",
			suffix = "m7♭5",
			Fingers = new int[6]
			{
				2,
				0,
				3,
				4,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1645,
			MidiNumbers = new int[6]
			{
				0,
				45,
				51,
				55,
				60,
				0
			},
			key = "A",
			suffix = "m7♭5",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				0,
				2,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1646,
			MidiNumbers = new int[6]
			{
				45,
				51,
				57,
				63,
				67,
				72
			},
			key = "A",
			suffix = "m7♭5",
			Fingers = new int[6]
			{
				1,
				2,
				3,
				4,
				4,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1647,
			MidiNumbers = new int[6]
			{
				0,
				0,
				57,
				63,
				67,
				72
			},
			key = "A",
			suffix = "m7♭5",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				3,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1648,
			MidiNumbers = new int[6]
			{
				0,
				57,
				63,
				67,
				72,
				0
			},
			key = "A",
			suffix = "m7♭5",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				2,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1649,
			MidiNumbers = new int[6]
			{
				0,
				57,
				0,
				67,
				72,
				75
			},
			key = "A",
			suffix = "m7♭5",
			Fingers = new int[6]
			{
				0,
				2,
				0,
				3,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1650,
			MidiNumbers = new int[6]
			{
				45,
				51,
				55,
				60,
				67,
				69
			},
			key = "A",
			suffix = "m7♭5",
			Fingers = new int[6]
			{
				1,
				2,
				1,
				1,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1651,
			MidiNumbers = new int[6]
			{
				45,
				48,
				55,
				59,
				0,
				0
			},
			key = "A",
			suffix = "m9",
			Fingers = new int[6]
			{
				3,
				1,
				4,
				2,
				0,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1652,
			MidiNumbers = new int[6]
			{
				0,
				0,
				57,
				64,
				67,
				71
			},
			key = "A",
			suffix = "m9",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				2,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1653,
			MidiNumbers = new int[6]
			{
				45,
				52,
				55,
				60,
				64,
				71
			},
			key = "A",
			suffix = "m9",
			Fingers = new int[6]
			{
				1,
				3,
				1,
				1,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1654,
			MidiNumbers = new int[6]
			{
				0,
				57,
				60,
				67,
				71,
				0
			},
			key = "A",
			suffix = "m9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1655,
			MidiNumbers = new int[6]
			{
				0,
				57,
				60,
				67,
				71,
				76
			},
			key = "A",
			suffix = "m9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				3,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1656,
			MidiNumbers = new int[6]
			{
				0,
				0,
				57,
				60,
				67,
				71
			},
			key = "A",
			suffix = "m9",
			Fingers = new int[6]
			{
				0,
				0,
				2,
				1,
				4,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1657,
			MidiNumbers = new int[6]
			{
				45,
				0,
				55,
				60,
				64,
				71
			},
			key = "A",
			suffix = "m9",
			Fingers = new int[6]
			{
				2,
				0,
				3,
				3,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1658,
			MidiNumbers = new int[6]
			{
				0,
				57,
				60,
				67,
				69,
				74
			},
			key = "A",
			suffix = "m11",
			Fingers = new int[6]
			{
				0,
				3,
				1,
				4,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1659,
			MidiNumbers = new int[6]
			{
				0,
				57,
				62,
				67,
				72,
				76
			},
			key = "A",
			suffix = "m11",
			Fingers = new int[6]
			{
				0,
				1,
				1,
				1,
				2,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1660,
			MidiNumbers = new int[6]
			{
				45,
				0,
				55,
				60,
				62,
				0
			},
			key = "A",
			suffix = "m11",
			Fingers = new int[6]
			{
				2,
				0,
				3,
				4,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1661,
			MidiNumbers = new int[6]
			{
				0,
				0,
				57,
				62,
				67,
				72
			},
			key = "A",
			suffix = "m11",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				1,
				2,
				2
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1662,
			MidiNumbers = new int[6]
			{
				0,
				57,
				0,
				67,
				72,
				74
			},
			key = "A",
			suffix = "m11",
			Fingers = new int[6]
			{
				0,
				2,
				0,
				3,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1663,
			MidiNumbers = new int[6]
			{
				0,
				57,
				0,
				67,
				72,
				78
			},
			key = "A",
			suffix = "m13",
			Fingers = new int[6]
			{
				0,
				1,
				0,
				2,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1664,
			MidiNumbers = new int[6]
			{
				45,
				52,
				55,
				60,
				66,
				69
			},
			key = "A",
			suffix = "m13",
			Fingers = new int[6]
			{
				1,
				3,
				1,
				1,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1665,
			MidiNumbers = new int[6]
			{
				45,
				0,
				55,
				60,
				66,
				71
			},
			key = "A",
			suffix = "m13",
			Fingers = new int[6]
			{
				2,
				0,
				3,
				3,
				4,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1666,
			MidiNumbers = new int[6]
			{
				0,
				57,
				64,
				67,
				72,
				78
			},
			key = "A",
			suffix = "m13",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				1,
				2,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1667,
			MidiNumbers = new int[6]
			{
				0,
				57,
				60,
				67,
				71,
				78
			},
			key = "A",
			suffix = "m13",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1668,
			MidiNumbers = new int[6]
			{
				0,
				57,
				60,
				68,
				71,
				0
			},
			key = "A",
			suffix = "mMaj9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				4,
				3,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1669,
			MidiNumbers = new int[6]
			{
				45,
				52,
				56,
				60,
				64,
				71
			},
			key = "A",
			suffix = "mMaj9",
			Fingers = new int[6]
			{
				1,
				3,
				2,
				1,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1670,
			MidiNumbers = new int[6]
			{
				0,
				57,
				61,
				68,
				71,
				0
			},
			key = "A",
			suffix = "Maj9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				4,
				3,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1671,
			MidiNumbers = new int[6]
			{
				0,
				0,
				57,
				61,
				68,
				71
			},
			key = "A",
			suffix = "Maj9",
			Fingers = new int[6]
			{
				0,
				0,
				2,
				1,
				4,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1672,
			MidiNumbers = new int[6]
			{
				0,
				57,
				59,
				64,
				68,
				73
			},
			key = "A",
			suffix = "Maj9",
			Fingers = new int[6]
			{
				0,
				3,
				1,
				1,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1673,
			MidiNumbers = new int[6]
			{
				45,
				49,
				56,
				59,
				64,
				68
			},
			key = "A",
			suffix = "Maj9",
			Fingers = new int[6]
			{
				2,
				1,
				4,
				1,
				3,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1674,
			MidiNumbers = new int[6]
			{
				45,
				0,
				56,
				59,
				64,
				0
			},
			key = "A",
			suffix = "Maj9",
			Fingers = new int[6]
			{
				2,
				0,
				4,
				1,
				3,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1675,
			MidiNumbers = new int[6]
			{
				0,
				57,
				0,
				68,
				73,
				78
			},
			key = "A",
			suffix = "Maj13",
			Fingers = new int[6]
			{
				0,
				1,
				0,
				2,
				4,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1676,
			MidiNumbers = new int[6]
			{
				45,
				49,
				54,
				59,
				64,
				68
			},
			key = "A",
			suffix = "Maj13",
			Fingers = new int[6]
			{
				2,
				1,
				1,
				1,
				3,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1677,
			MidiNumbers = new int[6]
			{
				45,
				0,
				56,
				61,
				66,
				71
			},
			key = "A",
			suffix = "Maj13",
			Fingers = new int[6]
			{
				1,
				0,
				2,
				3,
				4,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1678,
			MidiNumbers = new int[6]
			{
				0,
				57,
				63,
				68,
				73,
				76
			},
			key = "A",
			suffix = "Maj7♯11",
			Fingers = new int[6]
			{
				0,
				1,
				2,
				3,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1679,
			MidiNumbers = new int[6]
			{
				45,
				0,
				56,
				61,
				63,
				0
			},
			key = "A",
			suffix = "Maj7♯11",
			Fingers = new int[6]
			{
				2,
				0,
				3,
				4,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1680,
			MidiNumbers = new int[6]
			{
				0,
				0,
				57,
				63,
				68,
				73
			},
			key = "A",
			suffix = "Maj7♯11",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				2,
				3,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1681,
			MidiNumbers = new int[6]
			{
				0,
				57,
				61,
				67,
				71,
				75
			},
			key = "A",
			suffix = "9♭5",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1682,
			MidiNumbers = new int[6]
			{
				45,
				49,
				55,
				59,
				63,
				0
			},
			key = "A",
			suffix = "9♭5",
			Fingers = new int[6]
			{
				2,
				1,
				3,
				1,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1683,
			MidiNumbers = new int[6]
			{
				45,
				51,
				55,
				60,
				67,
				71
			},
			key = "A",
			suffix = "m9♭5",
			Fingers = new int[6]
			{
				1,
				2,
				1,
				1,
				4,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1684,
			MidiNumbers = new int[6]
			{
				0,
				57,
				61,
				67,
				70,
				77
			},
			key = "A",
			suffix = "+7♭9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1685,
			MidiNumbers = new int[6]
			{
				45,
				0,
				55,
				61,
				65,
				70
			},
			key = "A",
			suffix = "+7♭9",
			Fingers = new int[6]
			{
				1,
				0,
				2,
				3,
				3,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1686,
			MidiNumbers = new int[6]
			{
				0,
				57,
				61,
				67,
				72,
				77
			},
			key = "A",
			suffix = "+7♯9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				4,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1687,
			MidiNumbers = new int[6]
			{
				45,
				0,
				55,
				61,
				65,
				72
			},
			key = "A",
			suffix = "+7♯9",
			Fingers = new int[6]
			{
				1,
				0,
				2,
				3,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1688,
			MidiNumbers = new int[6]
			{
				0,
				46,
				53,
				58,
				62,
				65
			},
			key = "B♭",
			suffix = string.Empty,
			Fingers = new int[6]
			{
				0,
				1,
				3,
				3,
				3,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1689,
			MidiNumbers = new int[6]
			{
				46,
				53,
				58,
				62,
				65,
				70
			},
			key = "B♭",
			suffix = string.Empty,
			Fingers = new int[6]
			{
				1,
				3,
				4,
				2,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1690,
			MidiNumbers = new int[6]
			{
				0,
				0,
				58,
				65,
				70,
				74
			},
			key = "B♭",
			suffix = string.Empty,
			Fingers = new int[6]
			{
				0,
				0,
				1,
				2,
				4,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1691,
			MidiNumbers = new int[6]
			{
				0,
				58,
				65,
				70,
				74,
				77
			},
			key = "B♭",
			suffix = string.Empty,
			Fingers = new int[6]
			{
				0,
				1,
				3,
				3,
				3,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1692,
			MidiNumbers = new int[6]
			{
				0,
				58,
				62,
				65,
				70,
				74
			},
			key = "B♭",
			suffix = string.Empty,
			Fingers = new int[6]
			{
				0,
				4,
				3,
				1,
				2,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1693,
			MidiNumbers = new int[6]
			{
				46,
				50,
				53,
				58,
				62,
				70
			},
			key = "B♭",
			suffix = string.Empty,
			Fingers = new int[6]
			{
				3,
				2,
				1,
				1,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1694,
			MidiNumbers = new int[6]
			{
				0,
				46,
				53,
				58,
				61,
				65
			},
			key = "B♭",
			suffix = "m",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				4,
				2,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1695,
			MidiNumbers = new int[6]
			{
				46,
				53,
				58,
				61,
				65,
				70
			},
			key = "B♭",
			suffix = "m",
			Fingers = new int[6]
			{
				1,
				3,
				4,
				1,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1696,
			MidiNumbers = new int[6]
			{
				0,
				0,
				58,
				65,
				70,
				73
			},
			key = "B♭",
			suffix = "m",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				4,
				2
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1697,
			MidiNumbers = new int[6]
			{
				0,
				58,
				65,
				70,
				73,
				77
			},
			key = "B♭",
			suffix = "m",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				4,
				2,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1698,
			MidiNumbers = new int[6]
			{
				0,
				0,
				58,
				61,
				65,
				70
			},
			key = "B♭",
			suffix = "m",
			Fingers = new int[6]
			{
				0,
				0,
				3,
				1,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1699,
			MidiNumbers = new int[6]
			{
				0,
				46,
				53,
				56,
				62,
				65
			},
			key = "B♭",
			suffix = "7",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				1,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1700,
			MidiNumbers = new int[6]
			{
				46,
				53,
				56,
				62,
				65,
				70
			},
			key = "B♭",
			suffix = "7",
			Fingers = new int[6]
			{
				1,
				3,
				1,
				2,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1701,
			MidiNumbers = new int[6]
			{
				46,
				0,
				56,
				62,
				65,
				0
			},
			key = "B♭",
			suffix = "7",
			Fingers = new int[6]
			{
				1,
				0,
				2,
				4,
				3,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1702,
			MidiNumbers = new int[6]
			{
				0,
				0,
				58,
				65,
				68,
				74
			},
			key = "B♭",
			suffix = "7",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				2,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1703,
			MidiNumbers = new int[6]
			{
				0,
				58,
				62,
				68,
				70,
				0
			},
			key = "B♭",
			suffix = "7",
			Fingers = new int[6]
			{
				0,
				3,
				2,
				4,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1704,
			MidiNumbers = new int[6]
			{
				0,
				46,
				53,
				58,
				60,
				65
			},
			key = "B♭",
			suffix = "sus2",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				4,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1705,
			MidiNumbers = new int[6]
			{
				46,
				53,
				58,
				60,
				0,
				0
			},
			key = "B♭",
			suffix = "sus2",
			Fingers = new int[6]
			{
				2,
				3,
				4,
				1,
				0,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1706,
			MidiNumbers = new int[6]
			{
				0,
				58,
				65,
				70,
				72,
				77
			},
			key = "B♭",
			suffix = "sus2",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				4,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1707,
			MidiNumbers = new int[6]
			{
				0,
				46,
				53,
				60,
				0,
				0
			},
			key = "B♭",
			suffix = "sus2",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				4,
				0,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1708,
			MidiNumbers = new int[6]
			{
				0,
				0,
				58,
				60,
				65,
				72
			},
			key = "B♭",
			suffix = "sus2",
			Fingers = new int[6]
			{
				0,
				0,
				3,
				1,
				2,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1709,
			MidiNumbers = new int[6]
			{
				0,
				0,
				58,
				65,
				70,
				72
			},
			key = "B♭",
			suffix = "sus2",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1710,
			MidiNumbers = new int[6]
			{
				0,
				46,
				53,
				58,
				63,
				65
			},
			key = "B♭",
			suffix = "sus4",
			Fingers = new int[6]
			{
				0,
				1,
				2,
				3,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1711,
			MidiNumbers = new int[6]
			{
				46,
				53,
				58,
				63,
				65,
				70
			},
			key = "B♭",
			suffix = "sus4",
			Fingers = new int[6]
			{
				1,
				2,
				3,
				4,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1712,
			MidiNumbers = new int[6]
			{
				0,
				0,
				58,
				65,
				70,
				75
			},
			key = "B♭",
			suffix = "sus4",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				4,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1713,
			MidiNumbers = new int[6]
			{
				0,
				0,
				58,
				63,
				65,
				70
			},
			key = "B♭",
			suffix = "sus4",
			Fingers = new int[6]
			{
				0,
				0,
				3,
				4,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1714,
			MidiNumbers = new int[6]
			{
				0,
				46,
				53,
				56,
				61,
				65
			},
			key = "B♭",
			suffix = "m7",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				1,
				2,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1715,
			MidiNumbers = new int[6]
			{
				46,
				53,
				56,
				61,
				65,
				70
			},
			key = "B♭",
			suffix = "m7",
			Fingers = new int[6]
			{
				1,
				3,
				1,
				1,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1716,
			MidiNumbers = new int[6]
			{
				46,
				0,
				56,
				61,
				65,
				0
			},
			key = "B♭",
			suffix = "m7",
			Fingers = new int[6]
			{
				1,
				0,
				2,
				3,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1717,
			MidiNumbers = new int[6]
			{
				0,
				58,
				65,
				68,
				73,
				77
			},
			key = "B♭",
			suffix = "m7",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				1,
				2,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1718,
			MidiNumbers = new int[6]
			{
				46,
				53,
				58,
				61,
				68,
				70
			},
			key = "B♭",
			suffix = "m7",
			Fingers = new int[6]
			{
				1,
				2,
				3,
				1,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1719,
			MidiNumbers = new int[6]
			{
				0,
				58,
				61,
				68,
				73,
				0
			},
			key = "B♭",
			suffix = "m7",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1720,
			MidiNumbers = new int[6]
			{
				0,
				58,
				61,
				68,
				70,
				0
			},
			key = "B♭",
			suffix = "m7",
			Fingers = new int[6]
			{
				0,
				3,
				1,
				4,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1721,
			MidiNumbers = new int[6]
			{
				0,
				0,
				58,
				65,
				68,
				73
			},
			key = "B♭",
			suffix = "m7",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				4,
				2,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1722,
			MidiNumbers = new int[6]
			{
				0,
				46,
				53,
				57,
				62,
				65
			},
			key = "B♭",
			suffix = "Maj7",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				2,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1723,
			MidiNumbers = new int[6]
			{
				0,
				0,
				58,
				62,
				65,
				69
			},
			key = "B♭",
			suffix = "Maj7",
			Fingers = new int[6]
			{
				0,
				0,
				4,
				3,
				2,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1724,
			MidiNumbers = new int[6]
			{
				0,
				0,
				58,
				65,
				69,
				74
			},
			key = "B♭",
			suffix = "Maj7",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				3,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1725,
			MidiNumbers = new int[6]
			{
				46,
				0,
				57,
				62,
				65,
				0
			},
			key = "B♭",
			suffix = "Maj7",
			Fingers = new int[6]
			{
				1,
				0,
				3,
				4,
				2,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1726,
			MidiNumbers = new int[6]
			{
				46,
				53,
				57,
				62,
				65,
				70
			},
			key = "B♭",
			suffix = "Maj7",
			Fingers = new int[6]
			{
				1,
				4,
				2,
				3,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1727,
			MidiNumbers = new int[6]
			{
				0,
				46,
				53,
				58,
				0,
				0
			},
			key = "B♭",
			suffix = "5",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				4,
				0,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1728,
			MidiNumbers = new int[6]
			{
				46,
				53,
				58,
				0,
				0,
				0
			},
			key = "B♭",
			suffix = "5",
			Fingers = new int[6]
			{
				1,
				3,
				4,
				0,
				0,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1729,
			MidiNumbers = new int[6]
			{
				0,
				46,
				53,
				58,
				0,
				0
			},
			key = "B♭",
			suffix = "5",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				4,
				0,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1730,
			MidiNumbers = new int[6]
			{
				0,
				0,
				0,
				58,
				65,
				70
			},
			key = "B♭",
			suffix = "5",
			Fingers = new int[6]
			{
				0,
				0,
				0,
				1,
				4,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1731,
			MidiNumbers = new int[6]
			{
				0,
				0,
				58,
				65,
				70,
				0
			},
			key = "B♭",
			suffix = "5",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1732,
			MidiNumbers = new int[6]
			{
				0,
				58,
				62,
				66,
				70,
				0
			},
			key = "B♭",
			suffix = "+",
			Fingers = new int[6]
			{
				0,
				4,
				3,
				1,
				2,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1733,
			MidiNumbers = new int[6]
			{
				0,
				0,
				58,
				62,
				66,
				70
			},
			key = "B♭",
			suffix = "+",
			Fingers = new int[6]
			{
				0,
				0,
				4,
				2,
				3,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1734,
			MidiNumbers = new int[6]
			{
				46,
				50,
				54,
				58,
				0,
				0
			},
			key = "B♭",
			suffix = "+",
			Fingers = new int[6]
			{
				4,
				3,
				2,
				1,
				0,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1735,
			MidiNumbers = new int[6]
			{
				46,
				54,
				58,
				62,
				0,
				0
			},
			key = "B♭",
			suffix = "+",
			Fingers = new int[6]
			{
				1,
				4,
				3,
				2,
				0,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1736,
			MidiNumbers = new int[6]
			{
				0,
				46,
				0,
				58,
				62,
				66
			},
			key = "B♭",
			suffix = "+",
			Fingers = new int[6]
			{
				0,
				1,
				0,
				3,
				4,
				2
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1737,
			MidiNumbers = new int[6]
			{
				0,
				46,
				52,
				58,
				61,
				0
			},
			key = "B♭",
			suffix = "°",
			Fingers = new int[6]
			{
				0,
				1,
				2,
				4,
				3,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1738,
			MidiNumbers = new int[6]
			{
				46,
				0,
				58,
				61,
				64,
				0
			},
			key = "B♭",
			suffix = "°",
			Fingers = new int[6]
			{
				2,
				0,
				4,
				3,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1739,
			MidiNumbers = new int[6]
			{
				46,
				52,
				58,
				61,
				0,
				0
			},
			key = "B♭",
			suffix = "°",
			Fingers = new int[6]
			{
				1,
				3,
				4,
				2,
				0,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1740,
			MidiNumbers = new int[6]
			{
				0,
				46,
				53,
				58,
				62,
				67
			},
			key = "B♭",
			suffix = "6",
			Fingers = new int[6]
			{
				0,
				1,
				2,
				3,
				4,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1741,
			MidiNumbers = new int[6]
			{
				0,
				0,
				58,
				65,
				67,
				74
			},
			key = "B♭",
			suffix = "6",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1742,
			MidiNumbers = new int[6]
			{
				0,
				0,
				58,
				62,
				67,
				70
			},
			key = "B♭",
			suffix = "6",
			Fingers = new int[6]
			{
				0,
				0,
				3,
				2,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1743,
			MidiNumbers = new int[6]
			{
				0,
				46,
				53,
				55,
				62,
				0
			},
			key = "B♭",
			suffix = "6",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				0,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1744,
			MidiNumbers = new int[6]
			{
				46,
				0,
				55,
				62,
				65,
				0
			},
			key = "B♭",
			suffix = "6",
			Fingers = new int[6]
			{
				2,
				0,
				1,
				4,
				3,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1745,
			MidiNumbers = new int[6]
			{
				0,
				46,
				50,
				56,
				60,
				0
			},
			key = "B♭",
			suffix = "9",
			Fingers = new int[6]
			{
				0,
				1,
				0,
				3,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1746,
			MidiNumbers = new int[6]
			{
				46,
				53,
				56,
				62,
				65,
				72
			},
			key = "B♭",
			suffix = "9",
			Fingers = new int[6]
			{
				1,
				3,
				1,
				2,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1747,
			MidiNumbers = new int[6]
			{
				46,
				0,
				56,
				60,
				65,
				70
			},
			key = "B♭",
			suffix = "9",
			Fingers = new int[6]
			{
				2,
				0,
				3,
				1,
				4,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1748,
			MidiNumbers = new int[6]
			{
				0,
				58,
				62,
				68,
				72,
				77
			},
			key = "B♭",
			suffix = "9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				3,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1749,
			MidiNumbers = new int[6]
			{
				46,
				0,
				56,
				60,
				62,
				0
			},
			key = "B♭",
			suffix = "9",
			Fingers = new int[6]
			{
				3,
				0,
				4,
				2,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1750,
			MidiNumbers = new int[6]
			{
				0,
				0,
				58,
				62,
				68,
				72
			},
			key = "B♭",
			suffix = "9",
			Fingers = new int[6]
			{
				0,
				0,
				2,
				1,
				4,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1751,
			MidiNumbers = new int[6]
			{
				46,
				50,
				56,
				60,
				65,
				0
			},
			key = "B♭",
			suffix = "9",
			Fingers = new int[6]
			{
				2,
				1,
				3,
				1,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1752,
			MidiNumbers = new int[6]
			{
				46,
				53,
				56,
				63,
				65,
				72
			},
			key = "B♭",
			suffix = "11",
			Fingers = new int[6]
			{
				1,
				2,
				1,
				3,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1753,
			MidiNumbers = new int[6]
			{
				0,
				58,
				62,
				68,
				70,
				75
			},
			key = "B♭",
			suffix = "11",
			Fingers = new int[6]
			{
				0,
				3,
				2,
				4,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1754,
			MidiNumbers = new int[6]
			{
				0,
				46,
				51,
				56,
				62,
				65
			},
			key = "B♭",
			suffix = "11",
			Fingers = new int[6]
			{
				0,
				1,
				1,
				1,
				3,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1755,
			MidiNumbers = new int[6]
			{
				0,
				0,
				58,
				63,
				68,
				74
			},
			key = "B♭",
			suffix = "11",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				1,
				2,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1756,
			MidiNumbers = new int[6]
			{
				46,
				0,
				56,
				60,
				62,
				67
			},
			key = "B♭",
			suffix = "13",
			Fingers = new int[6]
			{
				3,
				0,
				4,
				2,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1757,
			MidiNumbers = new int[6]
			{
				46,
				0,
				56,
				62,
				67,
				72
			},
			key = "B♭",
			suffix = "13",
			Fingers = new int[6]
			{
				1,
				0,
				1,
				1,
				2,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1758,
			MidiNumbers = new int[6]
			{
				0,
				58,
				62,
				68,
				72,
				79
			},
			key = "B♭",
			suffix = "13",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1759,
			MidiNumbers = new int[6]
			{
				46,
				53,
				56,
				62,
				67,
				70
			},
			key = "B♭",
			suffix = "13",
			Fingers = new int[6]
			{
				1,
				3,
				1,
				2,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1760,
			MidiNumbers = new int[6]
			{
				0,
				46,
				53,
				56,
				63,
				65
			},
			key = "B♭",
			suffix = "7sus4",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				1,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1761,
			MidiNumbers = new int[6]
			{
				46,
				53,
				56,
				63,
				65,
				70
			},
			key = "B♭",
			suffix = "7sus4",
			Fingers = new int[6]
			{
				1,
				2,
				1,
				3,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1762,
			MidiNumbers = new int[6]
			{
				0,
				58,
				63,
				68,
				70,
				75
			},
			key = "B♭",
			suffix = "7sus4",
			Fingers = new int[6]
			{
				0,
				2,
				3,
				4,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1763,
			MidiNumbers = new int[6]
			{
				0,
				0,
				58,
				65,
				68,
				75
			},
			key = "B♭",
			suffix = "7sus4",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				2,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1764,
			MidiNumbers = new int[6]
			{
				0,
				46,
				51,
				56,
				60,
				65
			},
			key = "B♭",
			suffix = "9sus4",
			Fingers = new int[6]
			{
				0,
				1,
				1,
				1,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1765,
			MidiNumbers = new int[6]
			{
				46,
				0,
				56,
				60,
				63,
				0
			},
			key = "B♭",
			suffix = "9sus4",
			Fingers = new int[6]
			{
				3,
				0,
				4,
				2,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1766,
			MidiNumbers = new int[6]
			{
				46,
				0,
				56,
				60,
				63,
				68
			},
			key = "B♭",
			suffix = "9sus4",
			Fingers = new int[6]
			{
				3,
				0,
				4,
				2,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1767,
			MidiNumbers = new int[6]
			{
				0,
				46,
				51,
				56,
				60,
				67
			},
			key = "B♭",
			suffix = "13sus4",
			Fingers = new int[6]
			{
				0,
				1,
				1,
				1,
				1,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1768,
			MidiNumbers = new int[6]
			{
				46,
				0,
				56,
				63,
				67,
				72
			},
			key = "B♭",
			suffix = "13sus4",
			Fingers = new int[6]
			{
				1,
				0,
				2,
				4,
				4,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1769,
			MidiNumbers = new int[6]
			{
				0,
				46,
				52,
				55,
				61,
				0
			},
			key = "B♭",
			suffix = "°7",
			Fingers = new int[6]
			{
				0,
				1,
				2,
				0,
				3,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1770,
			MidiNumbers = new int[6]
			{
				46,
				52,
				55,
				61,
				0,
				0
			},
			key = "B♭",
			suffix = "°7",
			Fingers = new int[6]
			{
				2,
				4,
				1,
				3,
				0,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1771,
			MidiNumbers = new int[6]
			{
				0,
				58,
				64,
				67,
				73,
				0
			},
			key = "B♭",
			suffix = "°7",
			Fingers = new int[6]
			{
				0,
				2,
				3,
				1,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1772,
			MidiNumbers = new int[6]
			{
				0,
				0,
				58,
				64,
				67,
				73
			},
			key = "B♭",
			suffix = "°7",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				2,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1773,
			MidiNumbers = new int[6]
			{
				46,
				0,
				55,
				61,
				64,
				0
			},
			key = "B♭",
			suffix = "°7",
			Fingers = new int[6]
			{
				3,
				0,
				1,
				4,
				2,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1774,
			MidiNumbers = new int[6]
			{
				0,
				46,
				54,
				56,
				62,
				66
			},
			key = "B♭",
			suffix = "+7",
			Fingers = new int[6]
			{
				0,
				1,
				4,
				1,
				3,
				2
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1775,
			MidiNumbers = new int[6]
			{
				46,
				0,
				56,
				62,
				66,
				0
			},
			key = "B♭",
			suffix = "+7",
			Fingers = new int[6]
			{
				1,
				0,
				2,
				3,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1776,
			MidiNumbers = new int[6]
			{
				0,
				0,
				58,
				66,
				68,
				74
			},
			key = "B♭",
			suffix = "+7",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				4,
				2,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1777,
			MidiNumbers = new int[6]
			{
				0,
				46,
				0,
				56,
				62,
				66
			},
			key = "B♭",
			suffix = "+7",
			Fingers = new int[6]
			{
				0,
				1,
				0,
				2,
				4,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1778,
			MidiNumbers = new int[6]
			{
				0,
				46,
				52,
				56,
				62,
				0
			},
			key = "B♭",
			suffix = "7♭5",
			Fingers = new int[6]
			{
				0,
				1,
				2,
				1,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1779,
			MidiNumbers = new int[6]
			{
				46,
				0,
				56,
				62,
				64,
				0
			},
			key = "B♭",
			suffix = "7♭5",
			Fingers = new int[6]
			{
				2,
				0,
				3,
				4,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1780,
			MidiNumbers = new int[6]
			{
				0,
				0,
				58,
				64,
				68,
				74
			},
			key = "B♭",
			suffix = "7♭5",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				2,
				2,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1781,
			MidiNumbers = new int[6]
			{
				0,
				46,
				52,
				56,
				62,
				68
			},
			key = "B♭",
			suffix = "7♭5",
			Fingers = new int[6]
			{
				0,
				1,
				2,
				1,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1782,
			MidiNumbers = new int[6]
			{
				0,
				58,
				62,
				68,
				73,
				0
			},
			key = "B♭",
			suffix = "7♯9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1783,
			MidiNumbers = new int[6]
			{
				46,
				53,
				56,
				62,
				65,
				73
			},
			key = "B♭",
			suffix = "7♯9",
			Fingers = new int[6]
			{
				1,
				3,
				1,
				2,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1784,
			MidiNumbers = new int[6]
			{
				0,
				0,
				58,
				62,
				68,
				73
			},
			key = "B♭",
			suffix = "7♯9",
			Fingers = new int[6]
			{
				0,
				0,
				2,
				1,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1785,
			MidiNumbers = new int[6]
			{
				0,
				58,
				62,
				68,
				73,
				76
			},
			key = "B♭",
			suffix = "7♭5♯9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1786,
			MidiNumbers = new int[6]
			{
				46,
				52,
				56,
				62,
				65,
				70
			},
			key = "B♭",
			suffix = "7♯11",
			Fingers = new int[6]
			{
				1,
				2,
				1,
				3,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1787,
			MidiNumbers = new int[6]
			{
				0,
				58,
				62,
				68,
				71,
				0
			},
			key = "B♭",
			suffix = "7♭9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1788,
			MidiNumbers = new int[6]
			{
				46,
				53,
				56,
				62,
				65,
				71
			},
			key = "B♭",
			suffix = "7♭9",
			Fingers = new int[6]
			{
				1,
				4,
				1,
				2,
				1,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1789,
			MidiNumbers = new int[6]
			{
				0,
				58,
				62,
				68,
				71,
				77
			},
			key = "B♭",
			suffix = "7♭9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1790,
			MidiNumbers = new int[6]
			{
				0,
				0,
				58,
				62,
				68,
				71
			},
			key = "B♭",
			suffix = "7♭9",
			Fingers = new int[6]
			{
				0,
				0,
				2,
				1,
				3,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1791,
			MidiNumbers = new int[6]
			{
				0,
				58,
				62,
				68,
				72,
				78
			},
			key = "B♭",
			suffix = "+9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1792,
			MidiNumbers = new int[6]
			{
				46,
				0,
				56,
				62,
				66,
				72
			},
			key = "B♭",
			suffix = "+9",
			Fingers = new int[6]
			{
				1,
				0,
				2,
				3,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1793,
			MidiNumbers = new int[6]
			{
				0,
				46,
				53,
				57,
				61,
				65
			},
			key = "B♭",
			suffix = "mMaj7",
			Fingers = new int[6]
			{
				0,
				1,
				4,
				2,
				3,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1794,
			MidiNumbers = new int[6]
			{
				46,
				0,
				57,
				61,
				65,
				0
			},
			key = "B♭",
			suffix = "mMaj7",
			Fingers = new int[6]
			{
				1,
				0,
				4,
				2,
				3,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1795,
			MidiNumbers = new int[6]
			{
				46,
				53,
				57,
				61,
				65,
				70
			},
			key = "B♭",
			suffix = "mMaj7",
			Fingers = new int[6]
			{
				1,
				3,
				2,
				1,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1796,
			MidiNumbers = new int[6]
			{
				0,
				0,
				58,
				65,
				69,
				73
			},
			key = "B♭",
			suffix = "mMaj7",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				4,
				2
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1797,
			MidiNumbers = new int[6]
			{
				0,
				0,
				58,
				61,
				65,
				69
			},
			key = "B♭",
			suffix = "mMaj7",
			Fingers = new int[6]
			{
				0,
				0,
				4,
				2,
				3,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1798,
			MidiNumbers = new int[6]
			{
				0,
				0,
				58,
				62,
				65,
				72
			},
			key = "B♭",
			suffix = "add9",
			Fingers = new int[6]
			{
				0,
				0,
				3,
				2,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1799,
			MidiNumbers = new int[6]
			{
				0,
				46,
				53,
				60,
				62,
				65
			},
			key = "B♭",
			suffix = "add9",
			Fingers = new int[6]
			{
				0,
				1,
				2,
				4,
				3,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1800,
			MidiNumbers = new int[6]
			{
				46,
				0,
				0,
				62,
				65,
				72
			},
			key = "B♭",
			suffix = "add9",
			Fingers = new int[6]
			{
				1,
				0,
				0,
				3,
				2,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1801,
			MidiNumbers = new int[6]
			{
				0,
				0,
				58,
				61,
				65,
				72
			},
			key = "B♭",
			suffix = "madd9",
			Fingers = new int[6]
			{
				0,
				0,
				3,
				1,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1802,
			MidiNumbers = new int[6]
			{
				46,
				53,
				58,
				61,
				67,
				70
			},
			key = "B♭",
			suffix = "m6",
			Fingers = new int[6]
			{
				1,
				2,
				3,
				1,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1803,
			MidiNumbers = new int[6]
			{
				0,
				0,
				58,
				65,
				67,
				73
			},
			key = "B♭",
			suffix = "m6",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				1,
				2
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1804,
			MidiNumbers = new int[6]
			{
				0,
				46,
				53,
				55,
				61,
				0
			},
			key = "B♭",
			suffix = "m6",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				0,
				2,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1805,
			MidiNumbers = new int[6]
			{
				46,
				0,
				55,
				61,
				65,
				0
			},
			key = "B♭",
			suffix = "m6",
			Fingers = new int[6]
			{
				2,
				0,
				1,
				3,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1806,
			MidiNumbers = new int[6]
			{
				0,
				58,
				61,
				67,
				70,
				0
			},
			key = "B♭",
			suffix = "m6",
			Fingers = new int[6]
			{
				0,
				4,
				1,
				3,
				2,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1807,
			MidiNumbers = new int[6]
			{
				0,
				58,
				65,
				67,
				73,
				0
			},
			key = "B♭",
			suffix = "m6",
			Fingers = new int[6]
			{
				0,
				2,
				4,
				1,
				3,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1808,
			MidiNumbers = new int[6]
			{
				0,
				58,
				61,
				67,
				70,
				77
			},
			key = "B♭",
			suffix = "m6",
			Fingers = new int[6]
			{
				0,
				3,
				1,
				2,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1809,
			MidiNumbers = new int[6]
			{
				0,
				58,
				0,
				67,
				73,
				77
			},
			key = "B♭",
			suffix = "m6",
			Fingers = new int[6]
			{
				0,
				2,
				0,
				1,
				4,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1810,
			MidiNumbers = new int[6]
			{
				0,
				58,
				61,
				66,
				70,
				77
			},
			key = "B♭",
			suffix = "m♭6",
			Fingers = new int[6]
			{
				0,
				3,
				1,
				1,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1811,
			MidiNumbers = new int[6]
			{
				46,
				0,
				54,
				61,
				65,
				0
			},
			key = "B♭",
			suffix = "m♭6",
			Fingers = new int[6]
			{
				2,
				0,
				1,
				3,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1812,
			MidiNumbers = new int[6]
			{
				0,
				0,
				58,
				64,
				68,
				73
			},
			key = "B♭",
			suffix = "m7♭5",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				2,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1813,
			MidiNumbers = new int[6]
			{
				46,
				0,
				56,
				61,
				64,
				0
			},
			key = "B♭",
			suffix = "m7♭5",
			Fingers = new int[6]
			{
				2,
				0,
				3,
				4,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1814,
			MidiNumbers = new int[6]
			{
				0,
				46,
				52,
				56,
				61,
				0
			},
			key = "B♭",
			suffix = "m7♭5",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				2,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1815,
			MidiNumbers = new int[6]
			{
				46,
				52,
				58,
				64,
				68,
				73
			},
			key = "B♭",
			suffix = "m7♭5",
			Fingers = new int[6]
			{
				1,
				2,
				3,
				4,
				4,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1816,
			MidiNumbers = new int[6]
			{
				0,
				58,
				0,
				68,
				73,
				76
			},
			key = "B♭",
			suffix = "m7♭5",
			Fingers = new int[6]
			{
				0,
				2,
				0,
				3,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1817,
			MidiNumbers = new int[6]
			{
				46,
				52,
				56,
				61,
				68,
				70
			},
			key = "B♭",
			suffix = "m7♭5",
			Fingers = new int[6]
			{
				1,
				2,
				1,
				1,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1818,
			MidiNumbers = new int[6]
			{
				46,
				53,
				56,
				61,
				65,
				72
			},
			key = "B♭",
			suffix = "m9",
			Fingers = new int[6]
			{
				1,
				2,
				1,
				1,
				1,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1819,
			MidiNumbers = new int[6]
			{
				0,
				58,
				61,
				68,
				72,
				0
			},
			key = "B♭",
			suffix = "m9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1820,
			MidiNumbers = new int[6]
			{
				0,
				58,
				61,
				68,
				72,
				77
			},
			key = "B♭",
			suffix = "m9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				3,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1821,
			MidiNumbers = new int[6]
			{
				0,
				0,
				58,
				61,
				68,
				72
			},
			key = "B♭",
			suffix = "m9",
			Fingers = new int[6]
			{
				0,
				0,
				2,
				1,
				4,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1822,
			MidiNumbers = new int[6]
			{
				46,
				0,
				56,
				61,
				65,
				72
			},
			key = "B♭",
			suffix = "m9",
			Fingers = new int[6]
			{
				2,
				0,
				3,
				3,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1823,
			MidiNumbers = new int[6]
			{
				0,
				58,
				61,
				68,
				70,
				75
			},
			key = "B♭",
			suffix = "m11",
			Fingers = new int[6]
			{
				0,
				3,
				1,
				4,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1824,
			MidiNumbers = new int[6]
			{
				0,
				46,
				51,
				56,
				61,
				65
			},
			key = "B♭",
			suffix = "m11",
			Fingers = new int[6]
			{
				0,
				1,
				1,
				1,
				2,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1825,
			MidiNumbers = new int[6]
			{
				46,
				0,
				56,
				61,
				63,
				0
			},
			key = "B♭",
			suffix = "m11",
			Fingers = new int[6]
			{
				2,
				0,
				3,
				4,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1826,
			MidiNumbers = new int[6]
			{
				0,
				0,
				58,
				63,
				68,
				73
			},
			key = "B♭",
			suffix = "m11",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				1,
				2,
				2
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1827,
			MidiNumbers = new int[6]
			{
				0,
				58,
				0,
				68,
				73,
				75
			},
			key = "B♭",
			suffix = "m11",
			Fingers = new int[6]
			{
				0,
				2,
				0,
				3,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1828,
			MidiNumbers = new int[6]
			{
				0,
				46,
				0,
				56,
				61,
				67
			},
			key = "B♭",
			suffix = "m13",
			Fingers = new int[6]
			{
				0,
				1,
				0,
				2,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1829,
			MidiNumbers = new int[6]
			{
				46,
				53,
				56,
				61,
				67,
				70
			},
			key = "B♭",
			suffix = "m13",
			Fingers = new int[6]
			{
				1,
				3,
				1,
				1,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1830,
			MidiNumbers = new int[6]
			{
				46,
				0,
				56,
				61,
				67,
				72
			},
			key = "B♭",
			suffix = "m13",
			Fingers = new int[6]
			{
				2,
				0,
				3,
				3,
				4,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1831,
			MidiNumbers = new int[6]
			{
				0,
				46,
				53,
				56,
				61,
				67
			},
			key = "B♭",
			suffix = "m13",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				1,
				2,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1832,
			MidiNumbers = new int[6]
			{
				0,
				58,
				61,
				68,
				72,
				79
			},
			key = "B♭",
			suffix = "m13",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1833,
			MidiNumbers = new int[6]
			{
				0,
				58,
				61,
				69,
				72,
				0
			},
			key = "B♭",
			suffix = "mMaj9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				4,
				3,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1834,
			MidiNumbers = new int[6]
			{
				46,
				53,
				57,
				61,
				65,
				72
			},
			key = "B♭",
			suffix = "mMaj9",
			Fingers = new int[6]
			{
				1,
				3,
				2,
				1,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1835,
			MidiNumbers = new int[6]
			{
				0,
				58,
				62,
				69,
				72,
				0
			},
			key = "B♭",
			suffix = "Maj9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				4,
				3,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1836,
			MidiNumbers = new int[6]
			{
				0,
				0,
				58,
				62,
				69,
				72
			},
			key = "B♭",
			suffix = "Maj9",
			Fingers = new int[6]
			{
				0,
				0,
				2,
				1,
				4,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1837,
			MidiNumbers = new int[6]
			{
				0,
				58,
				60,
				65,
				69,
				74
			},
			key = "B♭",
			suffix = "Maj9",
			Fingers = new int[6]
			{
				0,
				3,
				1,
				1,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1838,
			MidiNumbers = new int[6]
			{
				46,
				50,
				57,
				60,
				65,
				69
			},
			key = "B♭",
			suffix = "Maj9",
			Fingers = new int[6]
			{
				2,
				1,
				4,
				1,
				3,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1839,
			MidiNumbers = new int[6]
			{
				46,
				0,
				57,
				60,
				65,
				0
			},
			key = "B♭",
			suffix = "Maj9",
			Fingers = new int[6]
			{
				2,
				0,
				4,
				1,
				3,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1840,
			MidiNumbers = new int[6]
			{
				0,
				46,
				0,
				57,
				62,
				67
			},
			key = "B♭",
			suffix = "Maj13",
			Fingers = new int[6]
			{
				0,
				1,
				0,
				2,
				4,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1841,
			MidiNumbers = new int[6]
			{
				46,
				50,
				55,
				60,
				65,
				69
			},
			key = "B♭",
			suffix = "Maj13",
			Fingers = new int[6]
			{
				2,
				1,
				1,
				1,
				3,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1842,
			MidiNumbers = new int[6]
			{
				46,
				0,
				57,
				62,
				67,
				72
			},
			key = "B♭",
			suffix = "Maj13",
			Fingers = new int[6]
			{
				1,
				0,
				2,
				3,
				4,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1843,
			MidiNumbers = new int[6]
			{
				0,
				46,
				52,
				57,
				62,
				65
			},
			key = "B♭",
			suffix = "Maj7♯11",
			Fingers = new int[6]
			{
				0,
				1,
				2,
				3,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1844,
			MidiNumbers = new int[6]
			{
				46,
				0,
				57,
				62,
				64,
				0
			},
			key = "B♭",
			suffix = "Maj7♯11",
			Fingers = new int[6]
			{
				2,
				0,
				3,
				4,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1845,
			MidiNumbers = new int[6]
			{
				0,
				0,
				58,
				64,
				69,
				74
			},
			key = "B♭",
			suffix = "Maj7♯11",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				2,
				3,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1846,
			MidiNumbers = new int[6]
			{
				0,
				58,
				62,
				68,
				72,
				76
			},
			key = "B♭",
			suffix = "9♭5",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1847,
			MidiNumbers = new int[6]
			{
				46,
				50,
				56,
				60,
				64,
				0
			},
			key = "B♭",
			suffix = "9♭5",
			Fingers = new int[6]
			{
				2,
				1,
				3,
				1,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1848,
			MidiNumbers = new int[6]
			{
				46,
				52,
				56,
				61,
				68,
				72
			},
			key = "B♭",
			suffix = "m9♭5",
			Fingers = new int[6]
			{
				1,
				2,
				1,
				1,
				4,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1849,
			MidiNumbers = new int[6]
			{
				0,
				58,
				62,
				68,
				71,
				78
			},
			key = "B♭",
			suffix = "+7♭9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1850,
			MidiNumbers = new int[6]
			{
				46,
				0,
				56,
				62,
				66,
				71
			},
			key = "B♭",
			suffix = "+7♭9",
			Fingers = new int[6]
			{
				1,
				0,
				2,
				3,
				3,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1851,
			MidiNumbers = new int[6]
			{
				0,
				58,
				62,
				68,
				73,
				78
			},
			key = "B♭",
			suffix = "+7♯9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				4,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1852,
			MidiNumbers = new int[6]
			{
				46,
				0,
				56,
				62,
				66,
				73
			},
			key = "B♭",
			suffix = "+7♯9",
			Fingers = new int[6]
			{
				1,
				0,
				2,
				3,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1853,
			MidiNumbers = new int[6]
			{
				0,
				47,
				54,
				59,
				63,
				66
			},
			key = "B",
			suffix = string.Empty,
			Fingers = new int[6]
			{
				0,
				1,
				3,
				3,
				3,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1854,
			MidiNumbers = new int[6]
			{
				47,
				54,
				59,
				63,
				66,
				71
			},
			key = "B",
			suffix = string.Empty,
			Fingers = new int[6]
			{
				1,
				3,
				4,
				2,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1855,
			MidiNumbers = new int[6]
			{
				0,
				0,
				59,
				66,
				71,
				75
			},
			key = "B",
			suffix = string.Empty,
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				4,
				2
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1856,
			MidiNumbers = new int[6]
			{
				0,
				59,
				63,
				66,
				71,
				75
			},
			key = "B",
			suffix = string.Empty,
			Fingers = new int[6]
			{
				0,
				4,
				3,
				1,
				2,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1857,
			MidiNumbers = new int[6]
			{
				47,
				51,
				54,
				59,
				63,
				71
			},
			key = "B",
			suffix = string.Empty,
			Fingers = new int[6]
			{
				3,
				2,
				1,
				1,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1858,
			MidiNumbers = new int[6]
			{
				0,
				47,
				54,
				59,
				62,
				66
			},
			key = "B",
			suffix = "m",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				4,
				2,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1859,
			MidiNumbers = new int[6]
			{
				47,
				54,
				59,
				62,
				66,
				71
			},
			key = "B",
			suffix = "m",
			Fingers = new int[6]
			{
				1,
				3,
				4,
				1,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1860,
			MidiNumbers = new int[6]
			{
				0,
				0,
				59,
				66,
				71,
				74
			},
			key = "B",
			suffix = "m",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				4,
				2
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1861,
			MidiNumbers = new int[6]
			{
				0,
				0,
				59,
				62,
				66,
				71
			},
			key = "B",
			suffix = "m",
			Fingers = new int[6]
			{
				0,
				0,
				3,
				1,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1862,
			MidiNumbers = new int[6]
			{
				0,
				0,
				59,
				66,
				69,
				75
			},
			key = "B",
			suffix = "7",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				2,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1863,
			MidiNumbers = new int[6]
			{
				0,
				47,
				51,
				57,
				59,
				66
			},
			key = "B",
			suffix = "7",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				0,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1864,
			MidiNumbers = new int[6]
			{
				0,
				47,
				54,
				57,
				63,
				66
			},
			key = "B",
			suffix = "7",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				1,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1865,
			MidiNumbers = new int[6]
			{
				47,
				54,
				57,
				63,
				66,
				71
			},
			key = "B",
			suffix = "7",
			Fingers = new int[6]
			{
				1,
				3,
				1,
				4,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1866,
			MidiNumbers = new int[6]
			{
				47,
				0,
				57,
				62,
				67,
				71
			},
			key = "B",
			suffix = "7",
			Fingers = new int[6]
			{
				1,
				0,
				2,
				2,
				4,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1867,
			MidiNumbers = new int[6]
			{
				0,
				59,
				63,
				69,
				71,
				0
			},
			key = "B",
			suffix = "7",
			Fingers = new int[6]
			{
				0,
				3,
				2,
				4,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1868,
			MidiNumbers = new int[6]
			{
				47,
				0,
				57,
				63,
				66,
				0
			},
			key = "B",
			suffix = "7",
			Fingers = new int[6]
			{
				1,
				0,
				2,
				4,
				3,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1869,
			MidiNumbers = new int[6]
			{
				0,
				47,
				54,
				59,
				61,
				66
			},
			key = "B",
			suffix = "sus2",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				4,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1870,
			MidiNumbers = new int[6]
			{
				47,
				49,
				54,
				61,
				59,
				0
			},
			key = "B",
			suffix = "sus2",
			Fingers = new int[6]
			{
				4,
				1,
				2,
				3,
				0,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1871,
			MidiNumbers = new int[6]
			{
				0,
				47,
				54,
				61,
				0,
				0
			},
			key = "B",
			suffix = "sus2",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				4,
				0,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1872,
			MidiNumbers = new int[6]
			{
				0,
				0,
				59,
				61,
				66,
				73
			},
			key = "B",
			suffix = "sus2",
			Fingers = new int[6]
			{
				0,
				0,
				3,
				1,
				2,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1873,
			MidiNumbers = new int[6]
			{
				0,
				0,
				59,
				66,
				71,
				73
			},
			key = "B",
			suffix = "sus2",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1874,
			MidiNumbers = new int[6]
			{
				0,
				47,
				54,
				59,
				64,
				66
			},
			key = "B",
			suffix = "sus4",
			Fingers = new int[6]
			{
				0,
				1,
				2,
				3,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1875,
			MidiNumbers = new int[6]
			{
				47,
				54,
				59,
				64,
				66,
				71
			},
			key = "B",
			suffix = "sus4",
			Fingers = new int[6]
			{
				1,
				3,
				3,
				3,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1876,
			MidiNumbers = new int[6]
			{
				0,
				0,
				59,
				66,
				71,
				76
			},
			key = "B",
			suffix = "sus4",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				4,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1877,
			MidiNumbers = new int[6]
			{
				0,
				0,
				59,
				64,
				66,
				71
			},
			key = "B",
			suffix = "sus4",
			Fingers = new int[6]
			{
				0,
				0,
				3,
				4,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1878,
			MidiNumbers = new int[6]
			{
				0,
				47,
				50,
				57,
				59,
				66
			},
			key = "B",
			suffix = "m7",
			Fingers = new int[6]
			{
				0,
				1,
				0,
				3,
				0,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1879,
			MidiNumbers = new int[6]
			{
				0,
				47,
				54,
				57,
				62,
				66
			},
			key = "B",
			suffix = "m7",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				1,
				2,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1880,
			MidiNumbers = new int[6]
			{
				47,
				54,
				57,
				62,
				66,
				71
			},
			key = "B",
			suffix = "m7",
			Fingers = new int[6]
			{
				1,
				3,
				1,
				1,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1881,
			MidiNumbers = new int[6]
			{
				47,
				0,
				57,
				62,
				66,
				0
			},
			key = "B",
			suffix = "m7",
			Fingers = new int[6]
			{
				1,
				0,
				2,
				3,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1882,
			MidiNumbers = new int[6]
			{
				0,
				47,
				50,
				57,
				62,
				0
			},
			key = "B",
			suffix = "m7",
			Fingers = new int[6]
			{
				0,
				1,
				0,
				2,
				3,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1883,
			MidiNumbers = new int[6]
			{
				47,
				54,
				59,
				62,
				69,
				71
			},
			key = "B",
			suffix = "m7",
			Fingers = new int[6]
			{
				1,
				2,
				3,
				1,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1884,
			MidiNumbers = new int[6]
			{
				0,
				59,
				62,
				69,
				74,
				0
			},
			key = "B",
			suffix = "m7",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1885,
			MidiNumbers = new int[6]
			{
				0,
				59,
				62,
				69,
				71,
				0
			},
			key = "B",
			suffix = "m7",
			Fingers = new int[6]
			{
				0,
				3,
				1,
				4,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1886,
			MidiNumbers = new int[6]
			{
				0,
				0,
				59,
				66,
				69,
				74
			},
			key = "B",
			suffix = "m7",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				4,
				2,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1887,
			MidiNumbers = new int[6]
			{
				0,
				47,
				54,
				58,
				63,
				66
			},
			key = "B",
			suffix = "Maj7",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				2,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1888,
			MidiNumbers = new int[6]
			{
				0,
				0,
				59,
				63,
				66,
				70
			},
			key = "B",
			suffix = "Maj7",
			Fingers = new int[6]
			{
				0,
				0,
				4,
				3,
				2,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1889,
			MidiNumbers = new int[6]
			{
				0,
				0,
				59,
				66,
				70,
				75
			},
			key = "B",
			suffix = "Maj7",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				2,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1890,
			MidiNumbers = new int[6]
			{
				47,
				0,
				58,
				63,
				66,
				0
			},
			key = "B",
			suffix = "Maj7",
			Fingers = new int[6]
			{
				1,
				0,
				3,
				4,
				2,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1891,
			MidiNumbers = new int[6]
			{
				47,
				54,
				58,
				63,
				66,
				71
			},
			key = "B",
			suffix = "Maj7",
			Fingers = new int[6]
			{
				1,
				4,
				2,
				3,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1892,
			MidiNumbers = new int[6]
			{
				0,
				47,
				54,
				59,
				0,
				0
			},
			key = "B",
			suffix = "5",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				4,
				0,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1893,
			MidiNumbers = new int[6]
			{
				47,
				54,
				59,
				0,
				0,
				0
			},
			key = "B",
			suffix = "5",
			Fingers = new int[6]
			{
				1,
				3,
				4,
				0,
				0,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1894,
			MidiNumbers = new int[6]
			{
				0,
				0,
				0,
				59,
				66,
				71
			},
			key = "B",
			suffix = "5",
			Fingers = new int[6]
			{
				0,
				0,
				0,
				1,
				4,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1895,
			MidiNumbers = new int[6]
			{
				0,
				0,
				59,
				66,
				71,
				0
			},
			key = "B",
			suffix = "5",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1896,
			MidiNumbers = new int[6]
			{
				0,
				59,
				63,
				67,
				71,
				0
			},
			key = "B",
			suffix = "+",
			Fingers = new int[6]
			{
				0,
				4,
				3,
				1,
				2,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1897,
			MidiNumbers = new int[6]
			{
				0,
				0,
				59,
				63,
				67,
				71
			},
			key = "B",
			suffix = "+",
			Fingers = new int[6]
			{
				0,
				0,
				4,
				2,
				3,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1898,
			MidiNumbers = new int[6]
			{
				47,
				51,
				55,
				59,
				0,
				0
			},
			key = "B",
			suffix = "+",
			Fingers = new int[6]
			{
				4,
				3,
				2,
				1,
				0,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1899,
			MidiNumbers = new int[6]
			{
				47,
				55,
				59,
				63,
				0,
				0
			},
			key = "B",
			suffix = "+",
			Fingers = new int[6]
			{
				1,
				4,
				3,
				2,
				0,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1900,
			MidiNumbers = new int[6]
			{
				0,
				47,
				0,
				59,
				63,
				67
			},
			key = "B",
			suffix = "+",
			Fingers = new int[6]
			{
				0,
				1,
				0,
				3,
				4,
				2
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1901,
			MidiNumbers = new int[6]
			{
				0,
				47,
				53,
				59,
				62,
				0
			},
			key = "B",
			suffix = "°",
			Fingers = new int[6]
			{
				0,
				1,
				2,
				4,
				3,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1902,
			MidiNumbers = new int[6]
			{
				47,
				0,
				59,
				62,
				65,
				0
			},
			key = "B",
			suffix = "°",
			Fingers = new int[6]
			{
				2,
				0,
				4,
				3,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1903,
			MidiNumbers = new int[6]
			{
				47,
				53,
				59,
				62,
				0,
				0
			},
			key = "B",
			suffix = "°",
			Fingers = new int[6]
			{
				1,
				3,
				4,
				2,
				0,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1904,
			MidiNumbers = new int[6]
			{
				0,
				47,
				51,
				56,
				59,
				66
			},
			key = "B",
			suffix = "6",
			Fingers = new int[6]
			{
				0,
				3,
				1,
				2,
				0,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1905,
			MidiNumbers = new int[6]
			{
				0,
				47,
				54,
				59,
				63,
				68
			},
			key = "B",
			suffix = "6",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				3,
				3,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1906,
			MidiNumbers = new int[6]
			{
				0,
				0,
				59,
				66,
				68,
				75
			},
			key = "B",
			suffix = "6",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1907,
			MidiNumbers = new int[6]
			{
				0,
				0,
				59,
				63,
				68,
				71
			},
			key = "B",
			suffix = "6",
			Fingers = new int[6]
			{
				0,
				0,
				3,
				2,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1908,
			MidiNumbers = new int[6]
			{
				47,
				0,
				56,
				63,
				66,
				0
			},
			key = "B",
			suffix = "6",
			Fingers = new int[6]
			{
				2,
				0,
				1,
				4,
				3,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1909,
			MidiNumbers = new int[6]
			{
				0,
				47,
				51,
				57,
				61,
				0
			},
			key = "B",
			suffix = "9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1910,
			MidiNumbers = new int[6]
			{
				47,
				54,
				57,
				63,
				66,
				73
			},
			key = "B",
			suffix = "9",
			Fingers = new int[6]
			{
				1,
				3,
				1,
				2,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1911,
			MidiNumbers = new int[6]
			{
				47,
				0,
				57,
				61,
				66,
				71
			},
			key = "B",
			suffix = "9",
			Fingers = new int[6]
			{
				2,
				0,
				3,
				1,
				4,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1912,
			MidiNumbers = new int[6]
			{
				0,
				47,
				51,
				57,
				61,
				66
			},
			key = "B",
			suffix = "9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				3,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1913,
			MidiNumbers = new int[6]
			{
				47,
				0,
				57,
				61,
				63,
				0
			},
			key = "B",
			suffix = "9",
			Fingers = new int[6]
			{
				3,
				0,
				4,
				2,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1914,
			MidiNumbers = new int[6]
			{
				0,
				0,
				59,
				63,
				69,
				73
			},
			key = "B",
			suffix = "9",
			Fingers = new int[6]
			{
				0,
				0,
				2,
				1,
				4,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1915,
			MidiNumbers = new int[6]
			{
				47,
				51,
				57,
				61,
				66,
				0
			},
			key = "B",
			suffix = "9",
			Fingers = new int[6]
			{
				2,
				1,
				3,
				1,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1916,
			MidiNumbers = new int[6]
			{
				47,
				54,
				57,
				64,
				66,
				73
			},
			key = "B",
			suffix = "11",
			Fingers = new int[6]
			{
				1,
				2,
				1,
				3,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1917,
			MidiNumbers = new int[6]
			{
				0,
				59,
				63,
				69,
				71,
				76
			},
			key = "B",
			suffix = "11",
			Fingers = new int[6]
			{
				0,
				3,
				2,
				4,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1918,
			MidiNumbers = new int[6]
			{
				0,
				47,
				52,
				57,
				63,
				66
			},
			key = "B",
			suffix = "11",
			Fingers = new int[6]
			{
				0,
				1,
				1,
				1,
				3,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1919,
			MidiNumbers = new int[6]
			{
				0,
				0,
				59,
				64,
				69,
				75
			},
			key = "B",
			suffix = "11",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				1,
				2,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1920,
			MidiNumbers = new int[6]
			{
				47,
				0,
				57,
				61,
				63,
				68
			},
			key = "B",
			suffix = "13",
			Fingers = new int[6]
			{
				3,
				0,
				4,
				2,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1921,
			MidiNumbers = new int[6]
			{
				47,
				0,
				57,
				63,
				68,
				73
			},
			key = "B",
			suffix = "13",
			Fingers = new int[6]
			{
				1,
				0,
				2,
				3,
				4,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1922,
			MidiNumbers = new int[6]
			{
				0,
				47,
				51,
				57,
				61,
				68
			},
			key = "B",
			suffix = "13",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1923,
			MidiNumbers = new int[6]
			{
				47,
				54,
				57,
				63,
				68,
				71
			},
			key = "B",
			suffix = "13",
			Fingers = new int[6]
			{
				1,
				3,
				1,
				2,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1924,
			MidiNumbers = new int[6]
			{
				0,
				47,
				52,
				57,
				59,
				64
			},
			key = "B",
			suffix = "7sus4",
			Fingers = new int[6]
			{
				0,
				1,
				2,
				3,
				0,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1925,
			MidiNumbers = new int[6]
			{
				47,
				54,
				57,
				64,
				66,
				71
			},
			key = "B",
			suffix = "7sus4",
			Fingers = new int[6]
			{
				1,
				3,
				1,
				4,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1926,
			MidiNumbers = new int[6]
			{
				0,
				59,
				64,
				69,
				71,
				76
			},
			key = "B",
			suffix = "7sus4",
			Fingers = new int[6]
			{
				0,
				2,
				3,
				4,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1927,
			MidiNumbers = new int[6]
			{
				0,
				47,
				54,
				57,
				64,
				66
			},
			key = "B",
			suffix = "7sus4",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				1,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1928,
			MidiNumbers = new int[6]
			{
				0,
				0,
				59,
				66,
				69,
				76
			},
			key = "B",
			suffix = "7sus4",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				2,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1929,
			MidiNumbers = new int[6]
			{
				0,
				47,
				52,
				57,
				61,
				66
			},
			key = "B",
			suffix = "9sus4",
			Fingers = new int[6]
			{
				0,
				1,
				1,
				1,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1930,
			MidiNumbers = new int[6]
			{
				47,
				0,
				57,
				61,
				64,
				0
			},
			key = "B",
			suffix = "9sus4",
			Fingers = new int[6]
			{
				3,
				0,
				4,
				2,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1931,
			MidiNumbers = new int[6]
			{
				47,
				0,
				57,
				61,
				64,
				69
			},
			key = "B",
			suffix = "9sus4",
			Fingers = new int[6]
			{
				3,
				0,
				4,
				2,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1932,
			MidiNumbers = new int[6]
			{
				0,
				47,
				52,
				57,
				61,
				68
			},
			key = "B",
			suffix = "13sus4",
			Fingers = new int[6]
			{
				0,
				1,
				1,
				1,
				1,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1933,
			MidiNumbers = new int[6]
			{
				47,
				0,
				57,
				64,
				68,
				73
			},
			key = "B",
			suffix = "13sus4",
			Fingers = new int[6]
			{
				1,
				0,
				2,
				4,
				4,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1934,
			MidiNumbers = new int[6]
			{
				0,
				47,
				53,
				56,
				62,
				0
			},
			key = "B",
			suffix = "°7",
			Fingers = new int[6]
			{
				0,
				2,
				3,
				1,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1935,
			MidiNumbers = new int[6]
			{
				47,
				53,
				56,
				62,
				0,
				0
			},
			key = "B",
			suffix = "°7",
			Fingers = new int[6]
			{
				2,
				4,
				1,
				3,
				0,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1936,
			MidiNumbers = new int[6]
			{
				47,
				0,
				56,
				62,
				65,
				0
			},
			key = "B",
			suffix = "°7",
			Fingers = new int[6]
			{
				3,
				0,
				1,
				4,
				2,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1937,
			MidiNumbers = new int[6]
			{
				0,
				0,
				59,
				65,
				68,
				74
			},
			key = "B",
			suffix = "°7",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				2,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1938,
			MidiNumbers = new int[6]
			{
				0,
				47,
				55,
				57,
				63,
				67
			},
			key = "B",
			suffix = "+7",
			Fingers = new int[6]
			{
				0,
				1,
				4,
				1,
				3,
				2
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1939,
			MidiNumbers = new int[6]
			{
				47,
				0,
				57,
				63,
				67,
				0
			},
			key = "B",
			suffix = "+7",
			Fingers = new int[6]
			{
				1,
				0,
				2,
				3,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1940,
			MidiNumbers = new int[6]
			{
				0,
				0,
				59,
				67,
				69,
				75
			},
			key = "B",
			suffix = "+7",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				4,
				2,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1941,
			MidiNumbers = new int[6]
			{
				0,
				47,
				0,
				57,
				63,
				67
			},
			key = "B",
			suffix = "+7",
			Fingers = new int[6]
			{
				0,
				1,
				0,
				2,
				4,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1942,
			MidiNumbers = new int[6]
			{
				0,
				47,
				53,
				57,
				63,
				0
			},
			key = "B",
			suffix = "7♭5",
			Fingers = new int[6]
			{
				0,
				1,
				2,
				1,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1943,
			MidiNumbers = new int[6]
			{
				47,
				0,
				57,
				63,
				65,
				0
			},
			key = "B",
			suffix = "7♭5",
			Fingers = new int[6]
			{
				2,
				0,
				3,
				4,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1944,
			MidiNumbers = new int[6]
			{
				0,
				0,
				59,
				65,
				69,
				75
			},
			key = "B",
			suffix = "7♭5",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				2,
				2,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1945,
			MidiNumbers = new int[6]
			{
				0,
				47,
				53,
				57,
				63,
				69
			},
			key = "B",
			suffix = "7♭5",
			Fingers = new int[6]
			{
				0,
				1,
				2,
				1,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1946,
			MidiNumbers = new int[6]
			{
				0,
				47,
				51,
				57,
				62,
				0
			},
			key = "B",
			suffix = "7♯9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1947,
			MidiNumbers = new int[6]
			{
				47,
				54,
				57,
				63,
				66,
				74
			},
			key = "B",
			suffix = "7♯9",
			Fingers = new int[6]
			{
				1,
				3,
				1,
				2,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1948,
			MidiNumbers = new int[6]
			{
				0,
				0,
				59,
				63,
				69,
				74
			},
			key = "B",
			suffix = "7♯9",
			Fingers = new int[6]
			{
				0,
				0,
				2,
				1,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1949,
			MidiNumbers = new int[6]
			{
				0,
				47,
				51,
				57,
				62,
				65
			},
			key = "B",
			suffix = "7♭5♯9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1950,
			MidiNumbers = new int[6]
			{
				47,
				53,
				57,
				63,
				66,
				71
			},
			key = "B",
			suffix = "7♯11",
			Fingers = new int[6]
			{
				1,
				2,
				1,
				3,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1951,
			MidiNumbers = new int[6]
			{
				0,
				47,
				51,
				57,
				60,
				0
			},
			key = "B",
			suffix = "7♭9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1952,
			MidiNumbers = new int[6]
			{
				47,
				54,
				57,
				63,
				66,
				72
			},
			key = "B",
			suffix = "7♭9",
			Fingers = new int[6]
			{
				1,
				4,
				1,
				2,
				1,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1953,
			MidiNumbers = new int[6]
			{
				0,
				47,
				51,
				57,
				60,
				66
			},
			key = "B",
			suffix = "7♭9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1954,
			MidiNumbers = new int[6]
			{
				0,
				0,
				59,
				63,
				69,
				72
			},
			key = "B",
			suffix = "7♭9",
			Fingers = new int[6]
			{
				0,
				0,
				2,
				1,
				3,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1955,
			MidiNumbers = new int[6]
			{
				0,
				47,
				51,
				57,
				61,
				67
			},
			key = "B",
			suffix = "+9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1956,
			MidiNumbers = new int[6]
			{
				47,
				0,
				57,
				63,
				67,
				73
			},
			key = "B",
			suffix = "+9",
			Fingers = new int[6]
			{
				1,
				0,
				2,
				3,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1957,
			MidiNumbers = new int[6]
			{
				0,
				47,
				54,
				58,
				62,
				66
			},
			key = "B",
			suffix = "mMaj7",
			Fingers = new int[6]
			{
				0,
				1,
				4,
				2,
				3,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1958,
			MidiNumbers = new int[6]
			{
				47,
				0,
				58,
				62,
				66,
				0
			},
			key = "B",
			suffix = "mMaj7",
			Fingers = new int[6]
			{
				1,
				0,
				4,
				2,
				3,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1959,
			MidiNumbers = new int[6]
			{
				47,
				54,
				58,
				62,
				66,
				71
			},
			key = "B",
			suffix = "mMaj7",
			Fingers = new int[6]
			{
				1,
				3,
				2,
				1,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1960,
			MidiNumbers = new int[6]
			{
				0,
				0,
				59,
				66,
				70,
				74
			},
			key = "B",
			suffix = "mMaj7",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				4,
				2
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1961,
			MidiNumbers = new int[6]
			{
				0,
				0,
				59,
				62,
				66,
				70
			},
			key = "B",
			suffix = "mMaj7",
			Fingers = new int[6]
			{
				0,
				0,
				4,
				2,
				3,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1962,
			MidiNumbers = new int[6]
			{
				0,
				0,
				59,
				63,
				66,
				73
			},
			key = "B",
			suffix = "add9",
			Fingers = new int[6]
			{
				0,
				0,
				3,
				2,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1963,
			MidiNumbers = new int[6]
			{
				0,
				47,
				54,
				61,
				63,
				66
			},
			key = "B",
			suffix = "add9",
			Fingers = new int[6]
			{
				0,
				1,
				2,
				4,
				3,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1964,
			MidiNumbers = new int[6]
			{
				47,
				0,
				0,
				63,
				66,
				73
			},
			key = "B",
			suffix = "add9",
			Fingers = new int[6]
			{
				1,
				0,
				0,
				3,
				2,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1965,
			MidiNumbers = new int[6]
			{
				0,
				0,
				59,
				62,
				66,
				73
			},
			key = "B",
			suffix = "madd9",
			Fingers = new int[6]
			{
				0,
				0,
				3,
				1,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1966,
			MidiNumbers = new int[6]
			{
				0,
				47,
				50,
				56,
				59,
				0
			},
			key = "B",
			suffix = "m6",
			Fingers = new int[6]
			{
				0,
				3,
				0,
				1,
				0,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1967,
			MidiNumbers = new int[6]
			{
				47,
				54,
				59,
				62,
				68,
				71
			},
			key = "B",
			suffix = "m6",
			Fingers = new int[6]
			{
				1,
				2,
				3,
				1,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1968,
			MidiNumbers = new int[6]
			{
				0,
				0,
				59,
				66,
				68,
				74
			},
			key = "B",
			suffix = "m6",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				1,
				2
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1969,
			MidiNumbers = new int[6]
			{
				0,
				47,
				54,
				56,
				62,
				0
			},
			key = "B",
			suffix = "m6",
			Fingers = new int[6]
			{
				0,
				2,
				4,
				1,
				3,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1970,
			MidiNumbers = new int[6]
			{
				47,
				0,
				56,
				62,
				66,
				0
			},
			key = "B",
			suffix = "m6",
			Fingers = new int[6]
			{
				2,
				0,
				1,
				3,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1971,
			MidiNumbers = new int[6]
			{
				0,
				59,
				62,
				68,
				71,
				0
			},
			key = "B",
			suffix = "m6",
			Fingers = new int[6]
			{
				0,
				4,
				1,
				3,
				2,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1972,
			MidiNumbers = new int[6]
			{
				0,
				59,
				62,
				68,
				71,
				78
			},
			key = "B",
			suffix = "m6",
			Fingers = new int[6]
			{
				0,
				3,
				1,
				2,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1973,
			MidiNumbers = new int[6]
			{
				0,
				47,
				0,
				56,
				62,
				66
			},
			key = "B",
			suffix = "m6",
			Fingers = new int[6]
			{
				0,
				2,
				0,
				1,
				4,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1974,
			MidiNumbers = new int[6]
			{
				0,
				59,
				62,
				67,
				71,
				78
			},
			key = "B",
			suffix = "m♭6",
			Fingers = new int[6]
			{
				0,
				3,
				1,
				1,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1975,
			MidiNumbers = new int[6]
			{
				47,
				0,
				55,
				62,
				66,
				0
			},
			key = "B",
			suffix = "m♭6",
			Fingers = new int[6]
			{
				2,
				0,
				1,
				3,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1976,
			MidiNumbers = new int[6]
			{
				0,
				0,
				59,
				65,
				69,
				74
			},
			key = "B",
			suffix = "m7♭5",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				3,
				3,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1977,
			MidiNumbers = new int[6]
			{
				47,
				0,
				57,
				62,
				65,
				0
			},
			key = "B",
			suffix = "m7♭5",
			Fingers = new int[6]
			{
				2,
				0,
				3,
				4,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1978,
			MidiNumbers = new int[6]
			{
				0,
				47,
				53,
				57,
				62,
				0
			},
			key = "B",
			suffix = "m7♭5",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				2,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1979,
			MidiNumbers = new int[6]
			{
				47,
				53,
				59,
				65,
				69,
				74
			},
			key = "B",
			suffix = "m7♭5",
			Fingers = new int[6]
			{
				1,
				2,
				3,
				4,
				4,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1980,
			MidiNumbers = new int[6]
			{
				0,
				47,
				0,
				57,
				62,
				65
			},
			key = "B",
			suffix = "m7♭5",
			Fingers = new int[6]
			{
				0,
				2,
				0,
				3,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1981,
			MidiNumbers = new int[6]
			{
				47,
				53,
				57,
				62,
				69,
				71
			},
			key = "B",
			suffix = "m7♭5",
			Fingers = new int[6]
			{
				1,
				2,
				1,
				1,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1982,
			MidiNumbers = new int[6]
			{
				0,
				47,
				50,
				57,
				61,
				0
			},
			key = "B",
			suffix = "m9",
			Fingers = new int[6]
			{
				0,
				2,
				0,
				3,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1983,
			MidiNumbers = new int[6]
			{
				47,
				54,
				57,
				62,
				66,
				73
			},
			key = "B",
			suffix = "m9",
			Fingers = new int[6]
			{
				1,
				3,
				1,
				1,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1984,
			MidiNumbers = new int[6]
			{
				0,
				59,
				62,
				69,
				73,
				0
			},
			key = "B",
			suffix = "m9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				4,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1985,
			MidiNumbers = new int[6]
			{
				0,
				59,
				62,
				69,
				73,
				78
			},
			key = "B",
			suffix = "m9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				3,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1986,
			MidiNumbers = new int[6]
			{
				0,
				0,
				59,
				62,
				69,
				73
			},
			key = "B",
			suffix = "m9",
			Fingers = new int[6]
			{
				0,
				0,
				2,
				1,
				4,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1987,
			MidiNumbers = new int[6]
			{
				47,
				0,
				57,
				62,
				66,
				73
			},
			key = "B",
			suffix = "m9",
			Fingers = new int[6]
			{
				2,
				0,
				3,
				3,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1988,
			MidiNumbers = new int[6]
			{
				0,
				59,
				62,
				69,
				71,
				76
			},
			key = "B",
			suffix = "m11",
			Fingers = new int[6]
			{
				0,
				3,
				1,
				4,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1989,
			MidiNumbers = new int[6]
			{
				0,
				47,
				52,
				57,
				62,
				66
			},
			key = "B",
			suffix = "m11",
			Fingers = new int[6]
			{
				0,
				1,
				1,
				1,
				2,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1990,
			MidiNumbers = new int[6]
			{
				47,
				0,
				57,
				62,
				64,
				0
			},
			key = "B",
			suffix = "m11",
			Fingers = new int[6]
			{
				2,
				0,
				3,
				4,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1991,
			MidiNumbers = new int[6]
			{
				0,
				0,
				59,
				64,
				69,
				74
			},
			key = "B",
			suffix = "m11",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				1,
				2,
				2
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1992,
			MidiNumbers = new int[6]
			{
				0,
				59,
				0,
				69,
				74,
				76
			},
			key = "B",
			suffix = "m11",
			Fingers = new int[6]
			{
				0,
				2,
				0,
				3,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1993,
			MidiNumbers = new int[6]
			{
				0,
				47,
				0,
				57,
				62,
				68
			},
			key = "B",
			suffix = "m13",
			Fingers = new int[6]
			{
				0,
				1,
				0,
				2,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1994,
			MidiNumbers = new int[6]
			{
				47,
				54,
				57,
				62,
				68,
				71
			},
			key = "B",
			suffix = "m13",
			Fingers = new int[6]
			{
				1,
				3,
				1,
				1,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1995,
			MidiNumbers = new int[6]
			{
				47,
				0,
				57,
				62,
				68,
				73
			},
			key = "B",
			suffix = "m13",
			Fingers = new int[6]
			{
				2,
				0,
				3,
				3,
				4,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1996,
			MidiNumbers = new int[6]
			{
				0,
				47,
				54,
				57,
				62,
				68
			},
			key = "B",
			suffix = "m13",
			Fingers = new int[6]
			{
				0,
				1,
				3,
				1,
				2,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1997,
			MidiNumbers = new int[6]
			{
				0,
				59,
				62,
				69,
				73,
				80
			},
			key = "B",
			suffix = "m13",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1998,
			MidiNumbers = new int[6]
			{
				0,
				59,
				62,
				70,
				73,
				0
			},
			key = "B",
			suffix = "mMaj9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				4,
				3,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 1999,
			MidiNumbers = new int[6]
			{
				47,
				54,
				58,
				62,
				66,
				73
			},
			key = "B",
			suffix = "mMaj9",
			Fingers = new int[6]
			{
				1,
				3,
				2,
				1,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 2000,
			MidiNumbers = new int[6]
			{
				0,
				47,
				51,
				58,
				61,
				0
			},
			key = "B",
			suffix = "Maj9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				4,
				3,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 2001,
			MidiNumbers = new int[6]
			{
				0,
				0,
				59,
				63,
				70,
				73
			},
			key = "B",
			suffix = "Maj9",
			Fingers = new int[6]
			{
				0,
				0,
				2,
				1,
				4,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 2002,
			MidiNumbers = new int[6]
			{
				0,
				59,
				61,
				66,
				70,
				75
			},
			key = "B",
			suffix = "Maj9",
			Fingers = new int[6]
			{
				0,
				3,
				1,
				1,
				1,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 2003,
			MidiNumbers = new int[6]
			{
				47,
				51,
				58,
				61,
				66,
				70
			},
			key = "B",
			suffix = "Maj9",
			Fingers = new int[6]
			{
				2,
				1,
				4,
				1,
				3,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 2004,
			MidiNumbers = new int[6]
			{
				47,
				0,
				58,
				61,
				66,
				0
			},
			key = "B",
			suffix = "Maj9",
			Fingers = new int[6]
			{
				2,
				0,
				4,
				1,
				3,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 2005,
			MidiNumbers = new int[6]
			{
				0,
				47,
				0,
				58,
				63,
				68
			},
			key = "B",
			suffix = "Maj13",
			Fingers = new int[6]
			{
				0,
				1,
				0,
				2,
				4,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 2006,
			MidiNumbers = new int[6]
			{
				47,
				51,
				56,
				61,
				66,
				70
			},
			key = "B",
			suffix = "Maj13",
			Fingers = new int[6]
			{
				2,
				1,
				1,
				1,
				3,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 2007,
			MidiNumbers = new int[6]
			{
				47,
				0,
				58,
				63,
				68,
				73
			},
			key = "B",
			suffix = "Maj13",
			Fingers = new int[6]
			{
				1,
				0,
				2,
				3,
				4,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 2008,
			MidiNumbers = new int[6]
			{
				0,
				47,
				53,
				58,
				63,
				66
			},
			key = "B",
			suffix = "Maj7♯11",
			Fingers = new int[6]
			{
				0,
				1,
				2,
				3,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 2009,
			MidiNumbers = new int[6]
			{
				47,
				0,
				58,
				63,
				65,
				0
			},
			key = "B",
			suffix = "Maj7♯11",
			Fingers = new int[6]
			{
				2,
				0,
				3,
				4,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 2010,
			MidiNumbers = new int[6]
			{
				0,
				0,
				59,
				65,
				70,
				75
			},
			key = "B",
			suffix = "Maj7♯11",
			Fingers = new int[6]
			{
				0,
				0,
				1,
				2,
				3,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 2011,
			MidiNumbers = new int[6]
			{
				0,
				47,
				51,
				57,
				61,
				65
			},
			key = "B",
			suffix = "9♭5",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				4,
				1
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 2012,
			MidiNumbers = new int[6]
			{
				47,
				51,
				57,
				61,
				65,
				0
			},
			key = "B",
			suffix = "9♭5",
			Fingers = new int[6]
			{
				2,
				1,
				3,
				1,
				1,
				0
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 2013,
			MidiNumbers = new int[6]
			{
				47,
				53,
				57,
				62,
				69,
				73
			},
			key = "B",
			suffix = "m9♭5",
			Fingers = new int[6]
			{
				1,
				2,
				1,
				1,
				4,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 2014,
			MidiNumbers = new int[6]
			{
				0,
				47,
				51,
				57,
				60,
				67
			},
			key = "B",
			suffix = "+7♭9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				1,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 2015,
			MidiNumbers = new int[6]
			{
				47,
				0,
				57,
				63,
				67,
				72
			},
			key = "B",
			suffix = "+7♭9",
			Fingers = new int[6]
			{
				1,
				0,
				2,
				3,
				3,
				3
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 2016,
			MidiNumbers = new int[6]
			{
				0,
				47,
				51,
				57,
				62,
				67
			},
			key = "B",
			suffix = "+7♯9",
			Fingers = new int[6]
			{
				0,
				2,
				1,
				3,
				4,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 2017,
			MidiNumbers = new int[6]
			{
				47,
				0,
				57,
				63,
				67,
				74
			},
			key = "B",
			suffix = "+7♯9",
			Fingers = new int[6]
			{
				1,
				0,
				2,
				3,
				3,
				4
			},
			IsSolo = false
		},
		new ChordNameCorrespondance
		{
			chordId = 2018,
			MidiNumbers = new int[6]
			{
				40,
				0,
				0,
				0,
				0,
				0
			},
			key = "String1",
			suffix = "Fret00-MIDI040",
			Fingers = new int[6],
			IsSolo = true
		},
		new ChordNameCorrespondance
		{
			chordId = 2019,
			MidiNumbers = new int[6]
			{
				41,
				0,
				0,
				0,
				0,
				0
			},
			key = "String1",
			suffix = "Fret01-MIDI041",
			Fingers = new int[6],
			IsSolo = true
		},
		new ChordNameCorrespondance
		{
			chordId = 2020,
			MidiNumbers = new int[6]
			{
				42,
				0,
				0,
				0,
				0,
				0
			},
			key = "String1",
			suffix = "Fret02-MIDI042",
			Fingers = new int[6],
			IsSolo = true
		},
		new ChordNameCorrespondance
		{
			chordId = 2021,
			MidiNumbers = new int[6]
			{
				43,
				0,
				0,
				0,
				0,
				0
			},
			key = "String1",
			suffix = "Fret03-MIDI043",
			Fingers = new int[6],
			IsSolo = true
		},
		new ChordNameCorrespondance
		{
			chordId = 2022,
			MidiNumbers = new int[6]
			{
				44,
				0,
				0,
				0,
				0,
				0
			},
			key = "String1",
			suffix = "Fret04-MIDI044",
			Fingers = new int[6],
			IsSolo = true
		},
		new ChordNameCorrespondance
		{
			chordId = 2023,
			MidiNumbers = new int[6]
			{
				45,
				0,
				0,
				0,
				0,
				0
			},
			key = "String1",
			suffix = "Fret05-MIDI045",
			Fingers = new int[6],
			IsSolo = true
		},
		new ChordNameCorrespondance
		{
			chordId = 2024,
			MidiNumbers = new int[6]
			{
				46,
				0,
				0,
				0,
				0,
				0
			},
			key = "String1",
			suffix = "Fret06-MIDI046",
			Fingers = new int[6],
			IsSolo = true
		},
		new ChordNameCorrespondance
		{
			chordId = 2025,
			MidiNumbers = new int[6]
			{
				47,
				0,
				0,
				0,
				0,
				0
			},
			key = "String1",
			suffix = "Fret07-MIDI047",
			Fingers = new int[6],
			IsSolo = true
		},
		new ChordNameCorrespondance
		{
			chordId = 2026,
			MidiNumbers = new int[6]
			{
				48,
				0,
				0,
				0,
				0,
				0
			},
			key = "String1",
			suffix = "Fret08-MIDI048",
			Fingers = new int[6],
			IsSolo = true
		},
		new ChordNameCorrespondance
		{
			chordId = 2027,
			MidiNumbers = new int[6]
			{
				49,
				0,
				0,
				0,
				0,
				0
			},
			key = "String1",
			suffix = "Fret09-MIDI049",
			Fingers = new int[6],
			IsSolo = true
		},
		new ChordNameCorrespondance
		{
			chordId = 2028,
			MidiNumbers = new int[6]
			{
				50,
				0,
				0,
				0,
				0,
				0
			},
			key = "String1",
			suffix = "Fret10-MIDI050",
			Fingers = new int[6],
			IsSolo = true
		},
		new ChordNameCorrespondance
		{
			chordId = 2029,
			MidiNumbers = new int[6]
			{
				51,
				0,
				0,
				0,
				0,
				0
			},
			key = "String1",
			suffix = "Fret11-MIDI051",
			Fingers = new int[6],
			IsSolo = true
		},
		new ChordNameCorrespondance
		{
			chordId = 2030,
			MidiNumbers = new int[6]
			{
				52,
				0,
				0,
				0,
				0,
				0
			},
			key = "String1",
			suffix = "Fret12-MIDI052",
			Fingers = new int[6],
			IsSolo = true
		},
		new ChordNameCorrespondance
		{
			chordId = 2031,
			MidiNumbers = new int[6]
			{
				53,
				0,
				0,
				0,
				0,
				0
			},
			key = "String1",
			suffix = "Fret13-MIDI053",
			Fingers = new int[6],
			IsSolo = true
		},
		new ChordNameCorrespondance
		{
			chordId = 2032,
			MidiNumbers = new int[6]
			{
				54,
				0,
				0,
				0,
				0,
				0
			},
			key = "String1",
			suffix = "Fret14-MIDI054",
			Fingers = new int[6],
			IsSolo = true
		},
		new ChordNameCorrespondance
		{
			chordId = 2033,
			MidiNumbers = new int[6]
			{
				55,
				0,
				0,
				0,
				0,
				0
			},
			key = "String1",
			suffix = "Fret15-MIDI055",
			Fingers = new int[6],
			IsSolo = true
		},
		new ChordNameCorrespondance
		{
			chordId = 2034,
			MidiNumbers = new int[6]
			{
				56,
				0,
				0,
				0,
				0,
				0
			},
			key = "String1",
			suffix = "Fret16-MIDI056",
			Fingers = new int[6],
			IsSolo = true
		},
		new ChordNameCorrespondance
		{
			chordId = 2035,
			MidiNumbers = new int[6]
			{
				57,
				0,
				0,
				0,
				0,
				0
			},
			key = "String1",
			suffix = "Fret17-MIDI057",
			Fingers = new int[6],
			IsSolo = true
		},
		new ChordNameCorrespondance
		{
			chordId = 2036,
			MidiNumbers = new int[6]
			{
				58,
				0,
				0,
				0,
				0,
				0
			},
			key = "String1",
			suffix = "Fret18-MIDI058",
			Fingers = new int[6],
			IsSolo = true
		},
		new ChordNameCorrespondance
		{
			chordId = 2037,
			MidiNumbers = new int[6]
			{
				59,
				0,
				0,
				0,
				0,
				0
			},
			key = "String1",
			suffix = "Fret19-MIDI059",
			Fingers = new int[6],
			IsSolo = true
		},
		new ChordNameCorrespondance
		{
			chordId = 2038,
			MidiNumbers = new int[6]
			{
				0,
				45,
				0,
				0,
				0,
				0
			},
			key = "String2",
			suffix = "Fret00-MIDI045",
			Fingers = new int[6],
			IsSolo = true
		},
		new ChordNameCorrespondance
		{
			chordId = 2039,
			MidiNumbers = new int[6]
			{
				0,
				46,
				0,
				0,
				0,
				0
			},
			key = "String2",
			suffix = "Fret01-MIDI046",
			Fingers = new int[6],
			IsSolo = true
		},
		new ChordNameCorrespondance
		{
			chordId = 2040,
			MidiNumbers = new int[6]
			{
				0,
				47,
				0,
				0,
				0,
				0
			},
			key = "String2",
			suffix = "Fret02-MIDI047",
			Fingers = new int[6],
			IsSolo = true
		},
		new ChordNameCorrespondance
		{
			chordId = 2041,
			MidiNumbers = new int[6]
			{
				0,
				48,
				0,
				0,
				0,
				0
			},
			key = "String2",
			suffix = "Fret03-MIDI048",
			Fingers = new int[6],
			IsSolo = true
		},
		new ChordNameCorrespondance
		{
			chordId = 2042,
			MidiNumbers = new int[6]
			{
				0,
				49,
				0,
				0,
				0,
				0
			},
			key = "String2",
			suffix = "Fret04-MIDI049",
			Fingers = new int[6],
			IsSolo = true
		},
		new ChordNameCorrespondance
		{
			chordId = 2043,
			MidiNumbers = new int[6]
			{
				0,
				50,
				0,
				0,
				0,
				0
			},
			key = "String2",
			suffix = "Fret05-MIDI050",
			Fingers = new int[6],
			IsSolo = true
		},
		new ChordNameCorrespondance
		{
			chordId = 2044,
			MidiNumbers = new int[6]
			{
				0,
				51,
				0,
				0,
				0,
				0
			},
			key = "String2",
			suffix = "Fret06-MIDI051",
			Fingers = new int[6],
			IsSolo = true
		},
		new ChordNameCorrespondance
		{
			chordId = 2045,
			MidiNumbers = new int[6]
			{
				0,
				52,
				0,
				0,
				0,
				0
			},
			key = "String2",
			suffix = "Fret07-MIDI052",
			Fingers = new int[6],
			IsSolo = true
		},
		new ChordNameCorrespondance
		{
			chordId = 2046,
			MidiNumbers = new int[6]
			{
				0,
				53,
				0,
				0,
				0,
				0
			},
			key = "String2",
			suffix = "Fret08-MIDI053",
			Fingers = new int[6],
			IsSolo = true
		},
		new ChordNameCorrespondance
		{
			chordId = 2047,
			MidiNumbers = new int[6]
			{
				0,
				54,
				0,
				0,
				0,
				0
			},
			key = "String2",
			suffix = "Fret09-MIDI054",
			Fingers = new int[6],
			IsSolo = true
		},
		new ChordNameCorrespondance
		{
			chordId = 2048,
			MidiNumbers = new int[6]
			{
				0,
				55,
				0,
				0,
				0,
				0
			},
			key = "String2",
			suffix = "Fret10-MIDI055",
			Fingers = new int[6],
			IsSolo = true
		},
		new ChordNameCorrespondance
		{
			chordId = 2049,
			MidiNumbers = new int[6]
			{
				0,
				56,
				0,
				0,
				0,
				0
			},
			key = "String2",
			suffix = "Fret11-MIDI056",
			Fingers = new int[6],
			IsSolo = true
		},
		new ChordNameCorrespondance
		{
			chordId = 2050,
			MidiNumbers = new int[6]
			{
				0,
				57,
				0,
				0,
				0,
				0
			},
			key = "String2",
			suffix = "Fret12-MIDI057",
			Fingers = new int[6],
			IsSolo = true
		},
		new ChordNameCorrespondance
		{
			chordId = 2051,
			MidiNumbers = new int[6]
			{
				0,
				58,
				0,
				0,
				0,
				0
			},
			key = "String2",
			suffix = "Fret13-MIDI058",
			Fingers = new int[6],
			IsSolo = true
		},
		new ChordNameCorrespondance
		{
			chordId = 2052,
			MidiNumbers = new int[6]
			{
				0,
				59,
				0,
				0,
				0,
				0
			},
			key = "String2",
			suffix = "Fret14-MIDI059",
			Fingers = new int[6],
			IsSolo = true
		},
		new ChordNameCorrespondance
		{
			chordId = 2053,
			MidiNumbers = new int[6]
			{
				0,
				60,
				0,
				0,
				0,
				0
			},
			key = "String2",
			suffix = "Fret15-MIDI060",
			Fingers = new int[6],
			IsSolo = true
		},
		new ChordNameCorrespondance
		{
			chordId = 2054,
			MidiNumbers = new int[6]
			{
				0,
				61,
				0,
				0,
				0,
				0
			},
			key = "String2",
			suffix = "Fret16-MIDI061",
			Fingers = new int[6],
			IsSolo = true
		},
		new ChordNameCorrespondance
		{
			chordId = 2055,
			MidiNumbers = new int[6]
			{
				0,
				62,
				0,
				0,
				0,
				0
			},
			key = "String2",
			suffix = "Fret17-MIDI062",
			Fingers = new int[6],
			IsSolo = true
		},
		new ChordNameCorrespondance
		{
			chordId = 2056,
			MidiNumbers = new int[6]
			{
				0,
				63,
				0,
				0,
				0,
				0
			},
			key = "String2",
			suffix = "Fret18-MIDI063",
			Fingers = new int[6],
			IsSolo = true
		},
		new ChordNameCorrespondance
		{
			chordId = 2057,
			MidiNumbers = new int[6]
			{
				0,
				64,
				0,
				0,
				0,
				0
			},
			key = "String2",
			suffix = "Fret19-MIDI064",
			Fingers = new int[6],
			IsSolo = true
		},
		new ChordNameCorrespondance
		{
			chordId = 2058,
			MidiNumbers = new int[6]
			{
				0,
				0,
				50,
				0,
				0,
				0
			},
			key = "String3",
			suffix = "Fret00-MIDI050",
			Fingers = new int[6],
			IsSolo = true
		},
		new ChordNameCorrespondance
		{
			chordId = 2059,
			MidiNumbers = new int[6]
			{
				0,
				0,
				51,
				0,
				0,
				0
			},
			key = "String3",
			suffix = "Fret01-MIDI051",
			Fingers = new int[6],
			IsSolo = true
		},
		new ChordNameCorrespondance
		{
			chordId = 2060,
			MidiNumbers = new int[6]
			{
				0,
				0,
				52,
				0,
				0,
				0
			},
			key = "String3",
			suffix = "Fret02-MIDI052",
			Fingers = new int[6],
			IsSolo = true
		},
		new ChordNameCorrespondance
		{
			chordId = 2061,
			MidiNumbers = new int[6]
			{
				0,
				0,
				53,
				0,
				0,
				0
			},
			key = "String3",
			suffix = "Fret03-MIDI053",
			Fingers = new int[6],
			IsSolo = true
		},
		new ChordNameCorrespondance
		{
			chordId = 2062,
			MidiNumbers = new int[6]
			{
				0,
				0,
				54,
				0,
				0,
				0
			},
			key = "String3",
			suffix = "Fret04-MIDI054",
			Fingers = new int[6],
			IsSolo = true
		},
		new ChordNameCorrespondance
		{
			chordId = 2063,
			MidiNumbers = new int[6]
			{
				0,
				0,
				55,
				0,
				0,
				0
			},
			key = "String3",
			suffix = "Fret05-MIDI055",
			Fingers = new int[6],
			IsSolo = true
		},
		new ChordNameCorrespondance
		{
			chordId = 2064,
			MidiNumbers = new int[6]
			{
				0,
				0,
				56,
				0,
				0,
				0
			},
			key = "String3",
			suffix = "Fret06-MIDI056",
			Fingers = new int[6],
			IsSolo = true
		},
		new ChordNameCorrespondance
		{
			chordId = 2065,
			MidiNumbers = new int[6]
			{
				0,
				0,
				57,
				0,
				0,
				0
			},
			key = "String3",
			suffix = "Fret07-MIDI057",
			Fingers = new int[6],
			IsSolo = true
		},
		new ChordNameCorrespondance
		{
			chordId = 2066,
			MidiNumbers = new int[6]
			{
				0,
				0,
				58,
				0,
				0,
				0
			},
			key = "String3",
			suffix = "Fret08-MIDI058",
			Fingers = new int[6],
			IsSolo = true
		},
		new ChordNameCorrespondance
		{
			chordId = 2067,
			MidiNumbers = new int[6]
			{
				0,
				0,
				59,
				0,
				0,
				0
			},
			key = "String3",
			suffix = "Fret09-MIDI059",
			Fingers = new int[6],
			IsSolo = true
		},
		new ChordNameCorrespondance
		{
			chordId = 2068,
			MidiNumbers = new int[6]
			{
				0,
				0,
				60,
				0,
				0,
				0
			},
			key = "String3",
			suffix = "Fret10-MIDI060",
			Fingers = new int[6],
			IsSolo = true
		},
		new ChordNameCorrespondance
		{
			chordId = 2069,
			MidiNumbers = new int[6]
			{
				0,
				0,
				61,
				0,
				0,
				0
			},
			key = "String3",
			suffix = "Fret11-MIDI061",
			Fingers = new int[6],
			IsSolo = true
		},
		new ChordNameCorrespondance
		{
			chordId = 2070,
			MidiNumbers = new int[6]
			{
				0,
				0,
				62,
				0,
				0,
				0
			},
			key = "String3",
			suffix = "Fret12-MIDI062",
			Fingers = new int[6],
			IsSolo = true
		},
		new ChordNameCorrespondance
		{
			chordId = 2071,
			MidiNumbers = new int[6]
			{
				0,
				0,
				63,
				0,
				0,
				0
			},
			key = "String3",
			suffix = "Fret13-MIDI063",
			Fingers = new int[6],
			IsSolo = true
		},
		new ChordNameCorrespondance
		{
			chordId = 2072,
			MidiNumbers = new int[6]
			{
				0,
				0,
				64,
				0,
				0,
				0
			},
			key = "String3",
			suffix = "Fret14-MIDI064",
			Fingers = new int[6],
			IsSolo = true
		},
		new ChordNameCorrespondance
		{
			chordId = 2073,
			MidiNumbers = new int[6]
			{
				0,
				0,
				65,
				0,
				0,
				0
			},
			key = "String3",
			suffix = "Fret15-MIDI065",
			Fingers = new int[6],
			IsSolo = true
		},
		new ChordNameCorrespondance
		{
			chordId = 2074,
			MidiNumbers = new int[6]
			{
				0,
				0,
				66,
				0,
				0,
				0
			},
			key = "String3",
			suffix = "Fret16-MIDI066",
			Fingers = new int[6],
			IsSolo = true
		},
		new ChordNameCorrespondance
		{
			chordId = 2075,
			MidiNumbers = new int[6]
			{
				0,
				0,
				67,
				0,
				0,
				0
			},
			key = "String3",
			suffix = "Fret17-MIDI067",
			Fingers = new int[6],
			IsSolo = true
		},
		new ChordNameCorrespondance
		{
			chordId = 2076,
			MidiNumbers = new int[6]
			{
				0,
				0,
				68,
				0,
				0,
				0
			},
			key = "String3",
			suffix = "Fret18-MIDI068",
			Fingers = new int[6],
			IsSolo = true
		},
		new ChordNameCorrespondance
		{
			chordId = 2077,
			MidiNumbers = new int[6]
			{
				0,
				0,
				69,
				0,
				0,
				0
			},
			key = "String3",
			suffix = "Fret19-MIDI069",
			Fingers = new int[6],
			IsSolo = true
		},
		new ChordNameCorrespondance
		{
			chordId = 2078,
			MidiNumbers = new int[6]
			{
				0,
				0,
				0,
				55,
				0,
				0
			},
			key = "String4",
			suffix = "Fret00-MIDI055",
			Fingers = new int[6],
			IsSolo = true
		},
		new ChordNameCorrespondance
		{
			chordId = 2079,
			MidiNumbers = new int[6]
			{
				0,
				0,
				0,
				56,
				0,
				0
			},
			key = "String4",
			suffix = "Fret01-MIDI056",
			Fingers = new int[6],
			IsSolo = true
		},
		new ChordNameCorrespondance
		{
			chordId = 2080,
			MidiNumbers = new int[6]
			{
				0,
				0,
				0,
				57,
				0,
				0
			},
			key = "String4",
			suffix = "Fret02-MIDI057",
			Fingers = new int[6],
			IsSolo = true
		},
		new ChordNameCorrespondance
		{
			chordId = 2081,
			MidiNumbers = new int[6]
			{
				0,
				0,
				0,
				58,
				0,
				0
			},
			key = "String4",
			suffix = "Fret03-MIDI058",
			Fingers = new int[6],
			IsSolo = true
		},
		new ChordNameCorrespondance
		{
			chordId = 2082,
			MidiNumbers = new int[6]
			{
				0,
				0,
				0,
				59,
				0,
				0
			},
			key = "String4",
			suffix = "Fret04-MIDI059",
			Fingers = new int[6],
			IsSolo = true
		},
		new ChordNameCorrespondance
		{
			chordId = 2083,
			MidiNumbers = new int[6]
			{
				0,
				0,
				0,
				60,
				0,
				0
			},
			key = "String4",
			suffix = "Fret05-MIDI060",
			Fingers = new int[6],
			IsSolo = true
		},
		new ChordNameCorrespondance
		{
			chordId = 2084,
			MidiNumbers = new int[6]
			{
				0,
				0,
				0,
				61,
				0,
				0
			},
			key = "String4",
			suffix = "Fret06-MIDI061",
			Fingers = new int[6],
			IsSolo = true
		},
		new ChordNameCorrespondance
		{
			chordId = 2085,
			MidiNumbers = new int[6]
			{
				0,
				0,
				0,
				62,
				0,
				0
			},
			key = "String4",
			suffix = "Fret07-MIDI062",
			Fingers = new int[6],
			IsSolo = true
		},
		new ChordNameCorrespondance
		{
			chordId = 2086,
			MidiNumbers = new int[6]
			{
				0,
				0,
				0,
				63,
				0,
				0
			},
			key = "String4",
			suffix = "Fret08-MIDI063",
			Fingers = new int[6],
			IsSolo = true
		},
		new ChordNameCorrespondance
		{
			chordId = 2087,
			MidiNumbers = new int[6]
			{
				0,
				0,
				0,
				64,
				0,
				0
			},
			key = "String4",
			suffix = "Fret09-MIDI064",
			Fingers = new int[6],
			IsSolo = true
		},
		new ChordNameCorrespondance
		{
			chordId = 2088,
			MidiNumbers = new int[6]
			{
				0,
				0,
				0,
				65,
				0,
				0
			},
			key = "String4",
			suffix = "Fret10-MIDI065",
			Fingers = new int[6],
			IsSolo = true
		},
		new ChordNameCorrespondance
		{
			chordId = 2089,
			MidiNumbers = new int[6]
			{
				0,
				0,
				0,
				66,
				0,
				0
			},
			key = "String4",
			suffix = "Fret11-MIDI066",
			Fingers = new int[6],
			IsSolo = true
		},
		new ChordNameCorrespondance
		{
			chordId = 2090,
			MidiNumbers = new int[6]
			{
				0,
				0,
				0,
				67,
				0,
				0
			},
			key = "String4",
			suffix = "Fret12-MIDI067",
			Fingers = new int[6],
			IsSolo = true
		},
		new ChordNameCorrespondance
		{
			chordId = 2091,
			MidiNumbers = new int[6]
			{
				0,
				0,
				0,
				68,
				0,
				0
			},
			key = "String4",
			suffix = "Fret13-MIDI068",
			Fingers = new int[6],
			IsSolo = true
		},
		new ChordNameCorrespondance
		{
			chordId = 2092,
			MidiNumbers = new int[6]
			{
				0,
				0,
				0,
				69,
				0,
				0
			},
			key = "String4",
			suffix = "Fret14-MIDI069",
			Fingers = new int[6],
			IsSolo = true
		},
		new ChordNameCorrespondance
		{
			chordId = 2093,
			MidiNumbers = new int[6]
			{
				0,
				0,
				0,
				70,
				0,
				0
			},
			key = "String4",
			suffix = "Fret15-MIDI070",
			Fingers = new int[6],
			IsSolo = true
		},
		new ChordNameCorrespondance
		{
			chordId = 2094,
			MidiNumbers = new int[6]
			{
				0,
				0,
				0,
				71,
				0,
				0
			},
			key = "String4",
			suffix = "Fret16-MIDI071",
			Fingers = new int[6],
			IsSolo = true
		},
		new ChordNameCorrespondance
		{
			chordId = 2095,
			MidiNumbers = new int[6]
			{
				0,
				0,
				0,
				72,
				0,
				0
			},
			key = "String4",
			suffix = "Fret17-MIDI072",
			Fingers = new int[6],
			IsSolo = true
		},
		new ChordNameCorrespondance
		{
			chordId = 2096,
			MidiNumbers = new int[6]
			{
				0,
				0,
				0,
				73,
				0,
				0
			},
			key = "String4",
			suffix = "Fret18-MIDI073",
			Fingers = new int[6],
			IsSolo = true
		},
		new ChordNameCorrespondance
		{
			chordId = 2097,
			MidiNumbers = new int[6]
			{
				0,
				0,
				0,
				74,
				0,
				0
			},
			key = "String4",
			suffix = "Fret19-MIDI074",
			Fingers = new int[6],
			IsSolo = true
		},
		new ChordNameCorrespondance
		{
			chordId = 2098,
			MidiNumbers = new int[6]
			{
				0,
				0,
				0,
				0,
				59,
				0
			},
			key = "String5",
			suffix = "Fret00-MIDI059",
			Fingers = new int[6],
			IsSolo = true
		},
		new ChordNameCorrespondance
		{
			chordId = 2099,
			MidiNumbers = new int[6]
			{
				0,
				0,
				0,
				0,
				60,
				0
			},
			key = "String5",
			suffix = "Fret01-MIDI060",
			Fingers = new int[6],
			IsSolo = true
		},
		new ChordNameCorrespondance
		{
			chordId = 2100,
			MidiNumbers = new int[6]
			{
				0,
				0,
				0,
				0,
				61,
				0
			},
			key = "String5",
			suffix = "Fret02-MIDI061",
			Fingers = new int[6],
			IsSolo = true
		},
		new ChordNameCorrespondance
		{
			chordId = 2101,
			MidiNumbers = new int[6]
			{
				0,
				0,
				0,
				0,
				62,
				0
			},
			key = "String5",
			suffix = "Fret03-MIDI062",
			Fingers = new int[6],
			IsSolo = true
		},
		new ChordNameCorrespondance
		{
			chordId = 2102,
			MidiNumbers = new int[6]
			{
				0,
				0,
				0,
				0,
				63,
				0
			},
			key = "String5",
			suffix = "Fret04-MIDI063",
			Fingers = new int[6],
			IsSolo = true
		},
		new ChordNameCorrespondance
		{
			chordId = 2103,
			MidiNumbers = new int[6]
			{
				0,
				0,
				0,
				0,
				64,
				0
			},
			key = "String5",
			suffix = "Fret05-MIDI064",
			Fingers = new int[6],
			IsSolo = true
		},
		new ChordNameCorrespondance
		{
			chordId = 2104,
			MidiNumbers = new int[6]
			{
				0,
				0,
				0,
				0,
				65,
				0
			},
			key = "String5",
			suffix = "Fret06-MIDI065",
			Fingers = new int[6],
			IsSolo = true
		},
		new ChordNameCorrespondance
		{
			chordId = 2105,
			MidiNumbers = new int[6]
			{
				0,
				0,
				0,
				0,
				66,
				0
			},
			key = "String5",
			suffix = "Fret07-MIDI066",
			Fingers = new int[6],
			IsSolo = true
		},
		new ChordNameCorrespondance
		{
			chordId = 2106,
			MidiNumbers = new int[6]
			{
				0,
				0,
				0,
				0,
				67,
				0
			},
			key = "String5",
			suffix = "Fret08-MIDI067",
			Fingers = new int[6],
			IsSolo = true
		},
		new ChordNameCorrespondance
		{
			chordId = 2107,
			MidiNumbers = new int[6]
			{
				0,
				0,
				0,
				0,
				68,
				0
			},
			key = "String5",
			suffix = "Fret09-MIDI068",
			Fingers = new int[6],
			IsSolo = true
		},
		new ChordNameCorrespondance
		{
			chordId = 2108,
			MidiNumbers = new int[6]
			{
				0,
				0,
				0,
				0,
				69,
				0
			},
			key = "String5",
			suffix = "Fret10-MIDI069",
			Fingers = new int[6],
			IsSolo = true
		},
		new ChordNameCorrespondance
		{
			chordId = 2109,
			MidiNumbers = new int[6]
			{
				0,
				0,
				0,
				0,
				70,
				0
			},
			key = "String5",
			suffix = "Fret11-MIDI070",
			Fingers = new int[6],
			IsSolo = true
		},
		new ChordNameCorrespondance
		{
			chordId = 2110,
			MidiNumbers = new int[6]
			{
				0,
				0,
				0,
				0,
				71,
				0
			},
			key = "String5",
			suffix = "Fret12-MIDI071",
			Fingers = new int[6],
			IsSolo = true
		},
		new ChordNameCorrespondance
		{
			chordId = 2111,
			MidiNumbers = new int[6]
			{
				0,
				0,
				0,
				0,
				72,
				0
			},
			key = "String5",
			suffix = "Fret13-MIDI072",
			Fingers = new int[6],
			IsSolo = true
		},
		new ChordNameCorrespondance
		{
			chordId = 2112,
			MidiNumbers = new int[6]
			{
				0,
				0,
				0,
				0,
				73,
				0
			},
			key = "String5",
			suffix = "Fret14-MIDI073",
			Fingers = new int[6],
			IsSolo = true
		},
		new ChordNameCorrespondance
		{
			chordId = 2113,
			MidiNumbers = new int[6]
			{
				0,
				0,
				0,
				0,
				74,
				0
			},
			key = "String5",
			suffix = "Fret15-MIDI074",
			Fingers = new int[6],
			IsSolo = true
		},
		new ChordNameCorrespondance
		{
			chordId = 2114,
			MidiNumbers = new int[6]
			{
				0,
				0,
				0,
				0,
				75,
				0
			},
			key = "String5",
			suffix = "Fret16-MIDI075",
			Fingers = new int[6],
			IsSolo = true
		},
		new ChordNameCorrespondance
		{
			chordId = 2115,
			MidiNumbers = new int[6]
			{
				0,
				0,
				0,
				0,
				76,
				0
			},
			key = "String5",
			suffix = "Fret17-MIDI076",
			Fingers = new int[6],
			IsSolo = true
		},
		new ChordNameCorrespondance
		{
			chordId = 2116,
			MidiNumbers = new int[6]
			{
				0,
				0,
				0,
				0,
				77,
				0
			},
			key = "String5",
			suffix = "Fret18-MIDI077",
			Fingers = new int[6],
			IsSolo = true
		},
		new ChordNameCorrespondance
		{
			chordId = 2117,
			MidiNumbers = new int[6]
			{
				0,
				0,
				0,
				0,
				78,
				0
			},
			key = "String5",
			suffix = "Fret19-MIDI078",
			Fingers = new int[6],
			IsSolo = true
		},
		new ChordNameCorrespondance
		{
			chordId = 2118,
			MidiNumbers = new int[6]
			{
				0,
				0,
				0,
				0,
				0,
				64
			},
			key = "String6",
			suffix = "Fret00-MIDI064",
			Fingers = new int[6],
			IsSolo = true
		},
		new ChordNameCorrespondance
		{
			chordId = 2119,
			MidiNumbers = new int[6]
			{
				0,
				0,
				0,
				0,
				0,
				65
			},
			key = "String6",
			suffix = "Fret01-MIDI065",
			Fingers = new int[6],
			IsSolo = true
		},
		new ChordNameCorrespondance
		{
			chordId = 2120,
			MidiNumbers = new int[6]
			{
				0,
				0,
				0,
				0,
				0,
				66
			},
			key = "String6",
			suffix = "Fret02-MIDI066",
			Fingers = new int[6],
			IsSolo = true
		},
		new ChordNameCorrespondance
		{
			chordId = 2121,
			MidiNumbers = new int[6]
			{
				0,
				0,
				0,
				0,
				0,
				67
			},
			key = "String6",
			suffix = "Fret03-MIDI067",
			Fingers = new int[6],
			IsSolo = true
		},
		new ChordNameCorrespondance
		{
			chordId = 2122,
			MidiNumbers = new int[6]
			{
				0,
				0,
				0,
				0,
				0,
				68
			},
			key = "String6",
			suffix = "Fret04-MIDI068",
			Fingers = new int[6],
			IsSolo = true
		},
		new ChordNameCorrespondance
		{
			chordId = 2123,
			MidiNumbers = new int[6]
			{
				0,
				0,
				0,
				0,
				0,
				69
			},
			key = "String6",
			suffix = "Fret05-MIDI069",
			Fingers = new int[6],
			IsSolo = true
		},
		new ChordNameCorrespondance
		{
			chordId = 2124,
			MidiNumbers = new int[6]
			{
				0,
				0,
				0,
				0,
				0,
				70
			},
			key = "String6",
			suffix = "Fret06-MIDI070",
			Fingers = new int[6],
			IsSolo = true
		},
		new ChordNameCorrespondance
		{
			chordId = 2125,
			MidiNumbers = new int[6]
			{
				0,
				0,
				0,
				0,
				0,
				71
			},
			key = "String6",
			suffix = "Fret07-MIDI071",
			Fingers = new int[6],
			IsSolo = true
		},
		new ChordNameCorrespondance
		{
			chordId = 2126,
			MidiNumbers = new int[6]
			{
				0,
				0,
				0,
				0,
				0,
				72
			},
			key = "String6",
			suffix = "Fret08-MIDI072",
			Fingers = new int[6],
			IsSolo = true
		},
		new ChordNameCorrespondance
		{
			chordId = 2127,
			MidiNumbers = new int[6]
			{
				0,
				0,
				0,
				0,
				0,
				73
			},
			key = "String6",
			suffix = "Fret09-MIDI073",
			Fingers = new int[6],
			IsSolo = true
		},
		new ChordNameCorrespondance
		{
			chordId = 2128,
			MidiNumbers = new int[6]
			{
				0,
				0,
				0,
				0,
				0,
				74
			},
			key = "String6",
			suffix = "Fret10-MIDI074",
			Fingers = new int[6],
			IsSolo = true
		},
		new ChordNameCorrespondance
		{
			chordId = 2129,
			MidiNumbers = new int[6]
			{
				0,
				0,
				0,
				0,
				0,
				75
			},
			key = "String6",
			suffix = "Fret11-MIDI075",
			Fingers = new int[6],
			IsSolo = true
		},
		new ChordNameCorrespondance
		{
			chordId = 2130,
			MidiNumbers = new int[6]
			{
				0,
				0,
				0,
				0,
				0,
				76
			},
			key = "String6",
			suffix = "Fret12-MIDI076",
			Fingers = new int[6],
			IsSolo = true
		},
		new ChordNameCorrespondance
		{
			chordId = 2131,
			MidiNumbers = new int[6]
			{
				0,
				0,
				0,
				0,
				0,
				77
			},
			key = "String6",
			suffix = "Fret13-MIDI077",
			Fingers = new int[6],
			IsSolo = true
		},
		new ChordNameCorrespondance
		{
			chordId = 2132,
			MidiNumbers = new int[6]
			{
				0,
				0,
				0,
				0,
				0,
				78
			},
			key = "String6",
			suffix = "Fret14-MIDI078",
			Fingers = new int[6],
			IsSolo = true
		},
		new ChordNameCorrespondance
		{
			chordId = 2133,
			MidiNumbers = new int[6]
			{
				0,
				0,
				0,
				0,
				0,
				79
			},
			key = "String6",
			suffix = "Fret15-MIDI079",
			Fingers = new int[6],
			IsSolo = true
		},
		new ChordNameCorrespondance
		{
			chordId = 2134,
			MidiNumbers = new int[6]
			{
				0,
				0,
				0,
				0,
				0,
				80
			},
			key = "String6",
			suffix = "Fret16-MIDI080",
			Fingers = new int[6],
			IsSolo = true
		},
		new ChordNameCorrespondance
		{
			chordId = 2135,
			MidiNumbers = new int[6]
			{
				0,
				0,
				0,
				0,
				0,
				81
			},
			key = "String6",
			suffix = "Fret17-MIDI081",
			Fingers = new int[6],
			IsSolo = true
		},
		new ChordNameCorrespondance
		{
			chordId = 2136,
			MidiNumbers = new int[6]
			{
				0,
				0,
				0,
				0,
				0,
				82
			},
			key = "String6",
			suffix = "Fret18-MIDI082",
			Fingers = new int[6],
			IsSolo = true
		},
		new ChordNameCorrespondance
		{
			chordId = 2137,
			MidiNumbers = new int[6]
			{
				0,
				0,
				0,
				0,
				0,
				83
			},
			key = "String6",
			suffix = "Fret19-MIDI083",
			Fingers = new int[6],
			IsSolo = true
		},
		new ChordNameCorrespondance
		{
			chordId = 2138,
			MidiNumbers = emptyChordMidiNumbers,
			key = "NONE",
			suffix = string.Empty,
			Fingers = new int[6],
			IsSolo = false
		}
	};

	public static Dictionary<int, ChordContainer> Map
	{
		get
		{
			if (_internalMap != null)
			{
				return _internalMap;
			}
			Dictionary<int, ChordContainer> dictionary = new Dictionary<int, ChordContainer>();
			ChordNameCorrespondance[] nameMap = NameMap;
			for (int i = 0; i < nameMap.Length; i++)
			{
				ChordNameCorrespondance chordNameCorrespondance = nameMap[i];
				int num = GenerateMidiNumbersArrayHash(chordNameCorrespondance.MidiNumbers);
				ChordContainer chordContainer = default(ChordContainer);
				chordContainer.MidiChord = new MidiChord(chordNameCorrespondance.MidiNumbers);
				chordContainer.Chord = new Chord
				{
					Hash = num,
					Name = chordNameCorrespondance.Name,
					IsSolo = chordNameCorrespondance.IsSolo,
					Fingers = chordNameCorrespondance.Fingers,
					Key = chordNameCorrespondance.key,
					Suffix = chordNameCorrespondance.suffix
				};
				ChordContainer value = chordContainer;
				if (dictionary.ContainsKey(num))
				{
					Debug.Log("****** ------  old : " + dictionary[num].Chord.Name + " new: " + chordNameCorrespondance.Name);
				}
				else
				{
					dictionary.Add(num, value);
				}
			}
			_internalMap = dictionary;
			return _internalMap;
		}
	}

	public static Chord ChordFromMidi(MidiChord midiChord)
	{
		return Map[GenerateMidiNumbersArrayHash(midiChord.MidiNumbers)].Chord;
	}

	public static IChord ChordFromMidi(IMidiChord midiChord)
	{
		return Map[GenerateMidiNumbersArrayHash(midiChord.GetMidiNumbers())].Chord;
	}

	public static MidiChord ChordToMidi(Chord chord)
	{
		return Map[chord.Hash].MidiChord;
	}

	public static IMidiChord ChordToMidi(IChord chord)
	{
		return Map[chord.GetHash()].MidiChord;
	}

	public static int GenerateMidiNumbersArrayHash(int[] array)
	{
		string text = array.Aggregate(string.Empty, (string s, int i) => s + i.ToString("000"));
		return text.GetHashCode();
	}
}
