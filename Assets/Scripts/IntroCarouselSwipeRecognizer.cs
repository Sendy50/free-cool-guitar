using System.Collections;
using UnityEngine;

public class IntroCarouselSwipeRecognizer : MonoBehaviour
{
    public delegate void Swipe(bool leftDirection);

    private const int kInvalidTouchId = -777;

    private const int kVeryFarTouch = -7777;

    public float kDetectionTime = 0.2f;

    public float kMoveTreshold = 18f;

    private int _currentTouchId;

    private float _currentX;

    private Coroutine _detectionDelayCoroutine;

    private Coroutine _inputCoroutine;

    private bool _isInDetectionDelay;

    private float _previousX;

    private void Awake()
    {
        ResetDetection();
    }

    public event Swipe SwipeDetected;

    public void StartInputCoroutine()
    {
        if (_inputCoroutine == null)
        {
            ResetDetection();
            _inputCoroutine = StartCoroutine(InputRoutine());
        }
    }

    public void StopInputCoroutine()
    {
        if (_inputCoroutine != null)
        {
            StopCoroutine(_inputCoroutine);
            _inputCoroutine = null;
        }
    }

    private void StartDetectionDelayCoroutine()
    {
        if (_detectionDelayCoroutine == null)
            _detectionDelayCoroutine = StartCoroutine(DetectionDelayRoutine(kDetectionTime));
    }

    private void StopDetectionDelayCoroutine()
    {
        if (_detectionDelayCoroutine != null)
        {
            StopCoroutine(_detectionDelayCoroutine);
            _detectionDelayCoroutine = null;
        }
    }

    private IEnumerator InputRoutine()
    {
        while (true)
        {
            var touches = Input.touches;
            for (var i = 0; i < touches.Length; i++)
            {
                var touch = touches[i];
                _currentX = touch.position.x;
                if (touch.phase == TouchPhase.Began)
                {
                    if (_currentTouchId == -777)
                        _currentTouchId = touch.fingerId;
                    else if (touch.fingerId != _currentTouchId) continue;
                }

                switch (touch.phase)
                {
                    case TouchPhase.Began:
                        StartDetectionDelayCoroutine();
                        _previousX = _currentX;
                        break;
                    case TouchPhase.Moved:
                        if (touch.fingerId == _currentTouchId)
                        {
                            if (Mathf.Abs(_currentX - _previousX) >= kMoveTreshold)
                                OnSwipeDetected(_currentX < _previousX);
                            _previousX = _currentX;
                        }

                        break;
                    case TouchPhase.Ended:
                        ResetDetection();
                        break;
                    case TouchPhase.Canceled:
                        ResetDetection();
                        break;
                }
            }

            yield return null;
        }
    }

    private IEnumerator DetectionDelayRoutine(float delayInSec)
    {
        _isInDetectionDelay = true;
        yield return new WaitForSeconds(delayInSec);
        _isInDetectionDelay = false;
    }

    private void OnSwipeDetected(bool leftDirection)
    {
        if (SwipeDetected != null && _isInDetectionDelay) SwipeDetected(leftDirection);
        ResetDetection();
    }

    private void ResetDetection()
    {
        _isInDetectionDelay = false;
        _currentX = -7777f;
        _previousX = -7777f;
        _currentTouchId = -777;
        StopDetectionDelayCoroutine();
    }
}