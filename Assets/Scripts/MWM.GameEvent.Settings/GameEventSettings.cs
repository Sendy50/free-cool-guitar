using JetBrains.Annotations;
using MWM.Core.Settings;
using UnityEngine;

namespace MWM.GameEvent.Settings
{
    [CreateAssetMenu(menuName = "MWM/GameEvent/GameEventSettings", fileName = "GameEventSettings.asset")]
    public class GameEventSettings : ScriptableObject
    {
        public CoreSettings InternalCoreSettings;

        public AppsFlyerVariables AppsFlyerConfigForPrototypeVersion;

        public AppsFlyerVariables AppsFlyerConfigForMwmVersion;

        [UsedImplicitly] public string EndPointUrlForDevelopment;

        [UsedImplicitly] public string EndPointUrlForProduction;

        public AppsFlyerVariables AppsFlyerConfig => InternalCoreSettings.CurrentBuildTarget != BuildTarget.Prototype
            ? AppsFlyerConfigForMwmVersion
            : AppsFlyerConfigForPrototypeVersion;

        public string EndPoint => EndPointUrlForProduction;
    }
}