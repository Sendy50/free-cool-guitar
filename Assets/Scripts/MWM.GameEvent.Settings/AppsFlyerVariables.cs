using System;

namespace MWM.GameEvent.Settings
{
    [Serializable]
    public class AppsFlyerVariables
    {
        public string AppsFlyerDevKeyForAndroid;

        public string AppsFlyerDevKeyForIos;

        public string AppleAppId;
    }
}