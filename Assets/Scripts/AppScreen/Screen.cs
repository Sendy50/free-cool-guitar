namespace AppScreen
{
    public interface Screen
    {
        void Show(ScreenTransitionOrientation orientation = ScreenTransitionOrientation.Bottom);

        void Hide(ScreenTransitionOrientation orientation = ScreenTransitionOrientation.Bottom);
    }
}