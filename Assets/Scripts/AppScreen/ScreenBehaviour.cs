using System;
using JetBrains.Annotations;
using UnityEngine;

namespace AppScreen
{
    public class ScreenBehaviour : MonoBehaviour, Screen
    {
        public enum Visibility
        {
            Showing,
            Shown,
            Hiding,
            Hidden
        }

        private static readonly int ShowBottomAnimationHash = Animator.StringToHash("ShowBottom");

        private static readonly int HideBottomAnimationHash = Animator.StringToHash("HideBottom");

        private static readonly int ShowLeftAnimationHash = Animator.StringToHash("ShowLeft");

        private static readonly int HideLeftAnimationHash = Animator.StringToHash("HideLeft");

        private static readonly int ShowRightAnimationHash = Animator.StringToHash("ShowRight");

        private static readonly int HideRightAnimationHash = Animator.StringToHash("HideRight");

        [SerializeField] private Canvas _internalCanvas;

        [SerializeField] private Animator _internalAnimatorController;

        private Visibility _currentVisibility = Visibility.Hidden;

        private bool _hideCalledBeforeInit;

        private bool _initialized;

        private bool _showCalledBeforeInit;

        protected virtual void Start()
        {
            if (_internalCanvas.enabled)
            {
                SetInternalVisibility(Visibility.Showing);
                SetInternalVisibility(Visibility.Shown);
            }

            _initialized = true;
            if (_showCalledBeforeInit && _currentVisibility == Visibility.Hidden)
                Show();
            else if (_hideCalledBeforeInit && _currentVisibility == Visibility.Shown) Hide();
        }

        public virtual void Show(ScreenTransitionOrientation orientation = ScreenTransitionOrientation.Bottom)
        {
            if (!_initialized)
            {
                _showCalledBeforeInit = true;
                _hideCalledBeforeInit = false;
            }
            else if (_currentVisibility != Visibility.Shown && _currentVisibility != 0)
            {
                if (_currentVisibility == Visibility.Hiding)
                {
                    orientation = ScreenTransitionOrientation.None;
                    ResetInternalAnimatorController();
                }

                SetInternalVisibility(Visibility.Showing);
                _internalCanvas.enabled = true;
                switch (orientation)
                {
                    case ScreenTransitionOrientation.Bottom:
                        _internalAnimatorController.Play(ShowBottomAnimationHash);
                        break;
                    case ScreenTransitionOrientation.Left:
                        _internalAnimatorController.Play(ShowLeftAnimationHash);
                        break;
                    case ScreenTransitionOrientation.Right:
                        _internalAnimatorController.Play(ShowRightAnimationHash);
                        break;
                    case ScreenTransitionOrientation.None:
                        SetInternalVisibility(Visibility.Shown);
                        break;
                    default:
                        throw new ArgumentOutOfRangeException("orientation", orientation, null);
                }
            }
        }

        public void Hide(ScreenTransitionOrientation orientation = ScreenTransitionOrientation.Bottom)
        {
            if (!_initialized)
            {
                _showCalledBeforeInit = false;
                _hideCalledBeforeInit = true;
            }
            else if (_currentVisibility != Visibility.Hidden && _currentVisibility != Visibility.Hiding)
            {
                if (_currentVisibility == Visibility.Showing)
                {
                    orientation = ScreenTransitionOrientation.None;
                    ResetInternalAnimatorController();
                }

                SetInternalVisibility(Visibility.Hiding);
                switch (orientation)
                {
                    case ScreenTransitionOrientation.Bottom:
                        _internalAnimatorController.Play(HideBottomAnimationHash);
                        break;
                    case ScreenTransitionOrientation.Left:
                        _internalAnimatorController.Play(HideLeftAnimationHash);
                        break;
                    case ScreenTransitionOrientation.Right:
                        _internalAnimatorController.Play(HideRightAnimationHash);
                        break;
                    case ScreenTransitionOrientation.None:
                        _internalCanvas.enabled = false;
                        SetInternalVisibility(Visibility.Hidden);
                        break;
                    default:
                        throw new ArgumentOutOfRangeException("orientation", orientation, null);
                }
            }
        }

        protected Visibility GetVisibility()
        {
            return _currentVisibility;
        }

        protected virtual void OnVisibilityChanged()
        {
        }

        private void SetInternalVisibility(Visibility visibility)
        {
            if (_currentVisibility != visibility)
            {
                _currentVisibility = visibility;
                OnVisibilityChanged();
            }
        }

        private void ResetInternalAnimatorController()
        {
            _internalAnimatorController.enabled = false;
            _internalAnimatorController.Rebind();
            _internalAnimatorController.enabled = true;
        }

        [UsedImplicitly]
        private void OnShowAnimationEnd()
        {
            if (_currentVisibility != 0) throw new Exception("There are problem with the state, cannot be Showing");
            SetInternalVisibility(Visibility.Shown);
        }

        [UsedImplicitly]
        private void OnHideAnimationEnd()
        {
            if (_currentVisibility != Visibility.Hiding)
                throw new Exception("There are problem with the state, cannot be Hiding");
            _internalCanvas.enabled = false;
            SetInternalVisibility(Visibility.Hidden);
        }
    }
}