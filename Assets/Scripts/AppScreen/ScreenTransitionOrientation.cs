namespace AppScreen
{
    public enum ScreenTransitionOrientation
    {
        None,
        Bottom,
        Left,
        Right
    }
}