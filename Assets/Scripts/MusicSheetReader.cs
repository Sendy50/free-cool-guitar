using System;
using System.Collections;
using UnityEngine;

public class MusicSheetReader : MonoBehaviour, ICurrentChordProvider
{
    public GameProgression GameProgression;

    private StoppableCoroutine _readChrodRoutine;

    private StoppableCoroutine _readUserActionRoutine;

    public MusicSheet MusicSheet { get; private set; }

    public IActionValidatorReader ActionValidatorReader { get; private set; }

    public event Action<ICurrentChordProvider, IMidiChord> OnCurrentChodeDidChange;

    public IMidiChord GetCurrentMidiChord()
    {
        return MusicSheet.GetCurrentMidiChord();
    }

    public bool GetIsMuteRepresentationEnabled()
    {
        return MusicSheet.GetIsMuteRepresentationEnabled();
    }

    public void PreparRead(MusicSheet musicSheet, IActionValidatorReader actionValidatorReader)
    {
        if (MusicSheet != null) MusicSheet.OnCurrentChodeDidChange -= OnCurrentChodeDidChangeCallback;
        MusicSheet = musicSheet;
        ActionValidatorReader = actionValidatorReader;
        musicSheet.Reset();
        MusicSheet.OnCurrentChodeDidChange += OnCurrentChodeDidChangeCallback;
    }

    public void StartRead()
    {
        _readUserActionRoutine.StartRoutine(this, UpdateUserActionRoutine(MusicSheet, GameProgression));
        _readChrodRoutine.StartRoutine(this, UpdateChordRoutine(MusicSheet, ActionValidatorReader));
    }

    public void StopRead()
    {
        _readUserActionRoutine.StopRoutine(this);
        _readChrodRoutine.StopRoutine(this);
    }

    private void OnCurrentChodeDidChangeCallback(ICurrentChordProvider sender, IMidiChord midiChord)
    {
        if (OnCurrentChodeDidChange != null) OnCurrentChodeDidChange(this, midiChord);
    }

    private IEnumerator UpdateUserActionRoutine(MusicSheet musicSheet, GameProgression gameProgression)
    {
        do
        {
            var currentUserAction = musicSheet.CurrentUserAction;
            yield return new WaitUntil(() => gameProgression.CurrentTime >= currentUserAction.time);
        } while (musicSheet.MoveNextUserAction());

        _readUserActionRoutine.OnRoutineCompleted();
    }

    private IEnumerator UpdateChordRoutine(MusicSheet musicSheet, IActionValidatorReader actionValidatorReader)
    {
        if (musicSheet.Chords.Length == 0) yield break;
        yield return new WaitUntil(() =>
            actionValidatorReader.actionValidators != null && actionValidatorReader.currentActionValidator != null);
        var actionValidators = actionValidatorReader.actionValidators;
        for (var i = 0; i < actionValidators.Length - 1; i++)
        {
            if (!musicSheet.IsNextChordExist)
            {
                _readChrodRoutine.OnRoutineCompleted();
                yield break;
            }

            var pendingValidation = actionValidators[i];
            var nextChord = musicSheet.NextChordEvent;
            yield return new WaitUntil(() =>
                pendingValidation.validationStatus != UserActionValidationStatus.unstarted);
            yield return new WaitUntil(() =>
                pendingValidation.validationStatus != UserActionValidationStatus.detecting);
            var nextValidation = actionValidators[i + 1];
            var nextValidationTime = nextValidation.interleave.Middle();
            if (nextValidationTime + 1E-05f >= nextChord.time)
            {
                yield return new WaitUntil(() => nextValidation.interleave.IsOnInterleave());
                musicSheet.MoveNextChord();
            }
        }

        _readChrodRoutine.OnRoutineCompleted();
    }
}