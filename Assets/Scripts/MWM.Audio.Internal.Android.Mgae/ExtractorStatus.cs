namespace MWM.Audio.Internal.Android.Mgae
{
    public enum ExtractorStatus
    {
        InProgress,
        Error,
        Success
    }
}