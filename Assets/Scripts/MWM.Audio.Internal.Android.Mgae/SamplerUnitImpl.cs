using System;
using JetBrains.Annotations;
using UnityEngine;

namespace MWM.Audio.Internal.Android.Mgae
{
    public class SamplerUnitImpl : SamplerUnit
    {
        private readonly jvalue[] _playSampleArgs;

        private readonly IntPtr _playSampleMethodPtr;

        private readonly jvalue[] _stopSampleArgs;

        private readonly IntPtr _stopSampleMethodPtr;

        private readonly AndroidJavaObject _this;

        private readonly IntPtr _thisObjPtr;

        public SamplerUnitImpl(AndroidJavaObject javaSampler)
        {
            _this = javaSampler;
            _thisObjPtr = _this.GetRawObject();
            _playSampleMethodPtr = AndroidJNI.GetMethodID(_this.GetRawClass(), "playSample", "(I)V");
            _playSampleArgs = new jvalue[1];
            _stopSampleMethodPtr = AndroidJNI.GetMethodID(_this.GetRawClass(), "stopSample", "(I)V");
            _stopSampleArgs = new jvalue[1];
        }

        public event Action<int> SampleStarted;

        public event Action<int> SampleStopped;

        public void Load(SamplePathHolder samplePathHolder)
        {
            _this.Call("load", samplePathHolder.GetAndroidJavaObject());
        }

        public void PlaySample(int sampleIndex)
        {
            _playSampleArgs[0].i = sampleIndex;
            AndroidJNI.CallVoidMethod(_thisObjPtr, _playSampleMethodPtr, _playSampleArgs);
        }

        public void StopSample(int sampleIndex)
        {
            _stopSampleArgs[0].i = sampleIndex;
            AndroidJNI.CallVoidMethod(_thisObjPtr, _stopSampleMethodPtr, _stopSampleArgs);
        }

        public float GetDuration(int sampleIndex)
        {
            return _this.Call<float>("getDuration", sampleIndex);
        }

        private void SetListener(Listener listener)
        {
            _this.Call("setListener", listener);
        }

        private void TriggerSampleStarted(int sampleIndex)
        {
            if (SampleStarted != null) SampleStarted(sampleIndex);
        }

        private void TriggerSampleStopped(int sampleIndex)
        {
            if (SampleStopped != null) SampleStopped(sampleIndex);
        }

        public class Listener : AndroidJavaProxy
        {
            private SamplerUnitImpl _samplerUnitImpl;

            public Listener()
                : base("com.mwm.sdk.audioengine.musicgaming.SamplerUnit$Listener")
            {
            }

            public void Attach(SamplerUnitImpl samplerUnitImpl)
            {
                _samplerUnitImpl = samplerUnitImpl;
                _samplerUnitImpl.SetListener(this);
            }

            [UsedImplicitly]
            private void onSampleStarted(int sampleIndex)
            {
                if (_samplerUnitImpl != null) _samplerUnitImpl.TriggerSampleStarted(sampleIndex);
            }

            [UsedImplicitly]
            private void onSampleStopped(int sampleIndex)
            {
                if (_samplerUnitImpl != null) _samplerUnitImpl.TriggerSampleStopped(sampleIndex);
            }
        }
    }
}