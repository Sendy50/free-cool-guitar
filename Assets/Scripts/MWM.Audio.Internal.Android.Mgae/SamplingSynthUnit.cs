namespace MWM.Audio.Internal.Android.Mgae
{
    public interface SamplingSynthUnit
    {
        InstrumentConfig CreateInternalInstrument(string instrumentResourcePath);

        InstrumentConfig CreateExternalInstrument(string instrumentDiskPath);

        void Load(InstrumentConfig instrumentConfig);

        void PlayNote(int trackIndex, int midiNote, int velocity);

        void StopNote(int trackIndex, int midiNote);

        void SetPedalStatus(int trackIndex, bool enable);

        void SetNumberOfTracks(int numberOfTracks);

        void StopAllNotes(int trackIndex);
    }
}