using UnityEngine;

namespace MWM.Audio.Internal.Android.Mgae
{
    public class AudioEngineImpl : AudioEngine
    {
        private readonly MultiPlayerUnitImpl _multiPlayerUnit;

        private readonly SamplerUnitImpl _samplerUnit;

        private readonly SamplingSynthUnitImpl _samplingSynthUnit;
        private readonly AndroidJavaObject _this;

        public AudioEngineImpl(AndroidJavaObject context, AudioEngineConfig audioEngineConfig,
            SamplerConfig samplerConfig, SamplingSynthConfig samplingSynthConfig, MultiPlayerConfig multiPlayerConfig)
        {
            Debug.LogError("========================");
            _this = new AndroidJavaObject("com.mwm.sdk.audioengine.musicgaming.AudioEngine", context,
                audioEngineConfig?.GetAndroidJavaObject(), samplerConfig?.GetAndroidJavaObject(),
                samplingSynthConfig?.GetAndroidJavaObject(), multiPlayerConfig?.GetAndroidJavaObject());
            if (multiPlayerConfig != null)
            {
                var multiPlayerUnit = _this.Call<AndroidJavaObject>("getMultiPlayerUnit");
                _multiPlayerUnit = new MultiPlayerUnitImpl(multiPlayerUnit);
            }

            if (samplerConfig != null)
            {
                var javaSampler = _this.Call<AndroidJavaObject>("getSamplerUnit");
                _samplerUnit = new SamplerUnitImpl(javaSampler);
                var listener = new SamplerUnitImpl.Listener();
                listener.Attach(_samplerUnit);
            }

            if (samplingSynthConfig != null)
            {
                var samplingSynthUnit = _this.Call<AndroidJavaObject>("getSamplingSynthUnit");
                _samplingSynthUnit = new SamplingSynthUnitImpl(samplingSynthUnit);
            }
        }

        public MultiPlayerUnit GetMultiPlayerUnit()
        {
            return _multiPlayerUnit;
        }

        public SamplerUnit GetSamplerUnit()
        {
            return _samplerUnit;
        }

        public SamplingSynthUnit GetSamplingSynthUnit()
        {
            return _samplingSynthUnit;
        }

        public void Destroy()
        {
            _this.Call("destroy");
        }
    }
}