using System;
using JetBrains.Annotations;
using UnityEngine;

namespace MWM.Audio.Internal.Android.Mgae
{
    public class MultiPlayerUnitImpl : MultiPlayerUnit
    {
        private readonly jvalue[] _getPositionArgs;

        private readonly IntPtr _getPositionMethodPtr;

        private readonly AndroidJavaObject _this;

        private readonly IntPtr _thisObjPtr;

        public MultiPlayerUnitImpl(AndroidJavaObject multiPlayerUnit)
        {
            _this = multiPlayerUnit;
            _thisObjPtr = _this.GetRawObject();
            _getPositionMethodPtr = AndroidJNI.GetMethodID(_this.GetRawClass(), "getPosition", "(I)D");
            _getPositionArgs = new jvalue[1];
            var playerListener = new PlayerListener(this);
            _this.Call("setListener", playerListener);
        }

        public event Action<int, bool> PlayingStateChanged;

        public event Action<int> EndOfFileReached;

        public void LoadInternalMusic(int playerIndex, string assetPath)
        {
            _this.Call("loadMusic", playerIndex, "asset:///" + assetPath + ".mp3");
        }

        public void LoadExternalMusic(int playerIndex, string diskPath)
        {
            _this.Call("loadMusic", playerIndex, "file:///" + diskPath);
        }

        public void PlayMusic(int playerIndex)
        {
            _this.Call("playMusic", playerIndex);
        }

        public void PauseMusic(int playerIndex)
        {
            _this.Call("pauseMusic", playerIndex);
        }

        public void StopMusic(int playerIndex)
        {
            PauseMusic(playerIndex);
            Seek(playerIndex, 0f);
        }

        public void Seek(int playerIndex, float position)
        {
            _this.Call("seek", playerIndex, position);
        }

        public void SetVolume(int playerIndex, float volume)
        {
            _this.Call("setVolume", playerIndex, volume);
        }

        public float GetVolume(int playerIndex)
        {
            return _this.Call<float>("getVolume", playerIndex);
        }

        public void SetSpeed(int playerIndex, float speed)
        {
            _this.Call("setSpeed", playerIndex, speed);
        }

        public float GetSpeed(int playerIndex)
        {
            return _this.Call<float>("getSpeed", playerIndex);
        }

        public double GetDuration(int playerIndex)
        {
            return _this.Call<double>("getDuration", playerIndex);
        }

        public double GetPosition(int playerIndex)
        {
            _getPositionArgs[0].i = playerIndex;
            return AndroidJNI.CallDoubleMethod(_thisObjPtr, _getPositionMethodPtr, _getPositionArgs);
        }

        public bool IsPlaying(int playerIndex)
        {
            return _this.Call<bool>("isPlaying", playerIndex);
        }

        public void SetRepeat(int playerIndex, bool repeat)
        {
            _this.Call("setRepeat", playerIndex, repeat);
        }

        public bool IsRepeat(int playerIndex)
        {
            return _this.Call<bool>("isRepeat", playerIndex);
        }

        private void TriggerPlayingStateChanged(int playerIndex, bool newPlayingState)
        {
            if (PlayingStateChanged != null) PlayingStateChanged(playerIndex, newPlayingState);
        }

        private void TriggerEndOfFileReached(int playerIndex)
        {
            if (EndOfFileReached != null) EndOfFileReached(playerIndex);
        }

        private class PlayerListener : AndroidJavaProxy
        {
            private readonly MultiPlayerUnitImpl _multiPlayerUnitImpl;

            public PlayerListener(MultiPlayerUnitImpl multiPlayerUnitImpl)
                : base("com.mwm.sdk.audioengine.musicgaming.MultiPlayerUnit$Listener")
            {
                _multiPlayerUnitImpl = multiPlayerUnitImpl;
            }

            [UsedImplicitly]
            private void onPlayingStateChanged(int playerIndex, bool newPlayingState)
            {
                _multiPlayerUnitImpl.TriggerPlayingStateChanged(playerIndex, newPlayingState);
            }

            [UsedImplicitly]
            private void onEndOfFileReached(int playerIndex)
            {
                _multiPlayerUnitImpl.TriggerEndOfFileReached(playerIndex);
            }
        }
    }
}