using UnityEngine;

namespace MWM.Audio.Internal.Android.Mgae
{
    public class SamplingSynthConfig
    {
        private readonly AndroidJavaObject _this;

        public SamplingSynthConfig(int numberTracks, int maxNumberSimultaneousNotes)
        {
            _this = new AndroidJavaObject("com.mwm.sdk.audioengine.musicgaming.SamplingSynthConfig", numberTracks,
                maxNumberSimultaneousNotes);
        }

        public AndroidJavaObject GetAndroidJavaObject()
        {
            return _this;
        }
    }
}