using UnityEngine;

namespace MWM.Audio.Internal.Android.Mgae
{
    public class AudioEngineConfig
    {
        private readonly AndroidJavaObject _this;

        public AudioEngineConfig(int nbChannels, int sharedMode)
        {
            _this = new AndroidJavaObject("com.mwm.sdk.audioengine.musicgaming.AudioEngineConfig", nbChannels,
                sharedMode);
        }

        public AndroidJavaObject GetAndroidJavaObject()
        {
            return _this;
        }
    }
}