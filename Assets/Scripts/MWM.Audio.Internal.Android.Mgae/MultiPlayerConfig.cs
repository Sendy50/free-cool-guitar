using UnityEngine;

namespace MWM.Audio.Internal.Android.Mgae
{
    public class MultiPlayerConfig
    {
        private readonly AndroidJavaObject _this;

        public MultiPlayerConfig(int numberPlayers)
        {
            _this = new AndroidJavaObject("com.mwm.sdk.audioengine.musicgaming.MultiPlayerConfig", numberPlayers);
        }

        public AndroidJavaObject GetAndroidJavaObject()
        {
            return _this;
        }
    }
}