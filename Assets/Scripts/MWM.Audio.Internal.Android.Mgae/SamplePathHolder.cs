using UnityEngine;

namespace MWM.Audio.Internal.Android.Mgae
{
    public class SamplePathHolder
    {
        private readonly AndroidJavaObject _this;

        public SamplePathHolder()
        {
            _this = new AndroidJavaObject("com.mwm.sdk.audioengine.musicgaming.SamplerUnit$SamplePathHolder");
        }

        public void AddResourcePath(string resourcePath)
        {
            _this.Call("addAssetPath", resourcePath);
        }

        public void AddDiskPath(string diskPath)
        {
            _this.Call("addDiskPath", diskPath);
        }

        public AndroidJavaObject GetAndroidJavaObject()
        {
            return _this;
        }
    }
}