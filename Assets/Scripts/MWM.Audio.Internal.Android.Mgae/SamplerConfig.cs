using UnityEngine;

namespace MWM.Audio.Internal.Android.Mgae
{
    public class SamplerConfig
    {
        private readonly AndroidJavaObject _this;

        public SamplerConfig(int nbSamplePlayer, int nbChannelsInput)
        {
            _this = new AndroidJavaObject("com.mwm.sdk.audioengine.musicgaming.SamplerConfig", nbSamplePlayer,
                nbChannelsInput);
        }

        public AndroidJavaObject GetAndroidJavaObject()
        {
            return _this;
        }
    }
}