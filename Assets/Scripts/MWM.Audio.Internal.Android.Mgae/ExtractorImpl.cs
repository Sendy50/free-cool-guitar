using System.Collections.Generic;
using UnityEngine;

namespace MWM.Audio.Internal.Android.Mgae
{
    public class ExtractorImpl : Extractor
    {
        private readonly AndroidJavaObject _context;
        private readonly AndroidJavaClass _extractionClass;

        public ExtractorImpl()
        {
            _extractionClass = new AndroidJavaClass("com.mwm.sdk.audioengine.musicgaming.Extraction");
            var androidJavaClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            _context = androidJavaClass.GetStatic<AndroidJavaObject>("currentActivity");
        }

        public ExtractorRequest CreateRequestForSamplerFiles(List<string> resourcePaths, List<string> diskPaths)
        {
            var javaRequest = _extractionClass.CallStatic<AndroidJavaObject>("createRequestForSamplerFiles");
            return CreateExtractorRequest(javaRequest, resourcePaths, diskPaths);
        }

        public ExtractorRequest CreateRequestForSamplingSynthFiles(List<string> resourcePaths, List<string> diskPaths)
        {
            var javaRequest = _extractionClass.CallStatic<AndroidJavaObject>("createRequestForSamplingSynthFiles");
            return CreateExtractorRequest(javaRequest, resourcePaths, diskPaths);
        }

        public ExtractorJob Extract(ExtractorRequest extractorRequest)
        {
            var extractorJob =
                _extractionClass.CallStatic<AndroidJavaObject>("extract", _context,
                    extractorRequest.GetAndroidJavaObject());
            return new ExtractorJob(extractorJob);
        }

        public void RemoveUnusedFiles(ExtractorRequest extractorRequest)
        {
            _extractionClass.CallStatic("removeUnusedFiles", _context, extractorRequest.GetAndroidJavaObject());
        }

        private static ExtractorRequest CreateExtractorRequest(AndroidJavaObject javaRequest,
            List<string> resourcePaths, List<string> diskPaths)
        {
            var count = resourcePaths.Count;
            for (var i = 0; i < count; i++)
            {
                var text = resourcePaths[i];
                javaRequest.Call("addAssetPath", text);
            }

            var count2 = diskPaths.Count;
            for (var j = 0; j < count2; j++)
            {
                var text2 = diskPaths[j];
                javaRequest.Call("addDiskPath", text2);
            }

            return new ExtractorRequest(javaRequest);
        }
    }
}