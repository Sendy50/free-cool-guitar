namespace MWM.Audio.Internal.Android.Mgae
{
    public interface AudioEngine
    {
        MultiPlayerUnit GetMultiPlayerUnit();

        SamplerUnit GetSamplerUnit();

        SamplingSynthUnit GetSamplingSynthUnit();

        void Destroy();
    }
}