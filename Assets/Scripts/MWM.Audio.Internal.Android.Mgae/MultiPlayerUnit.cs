using System;

namespace MWM.Audio.Internal.Android.Mgae
{
    public interface MultiPlayerUnit
    {
        event Action<int, bool> PlayingStateChanged;

        event Action<int> EndOfFileReached;

        void LoadInternalMusic(int playerIndex, string assetPath);

        void LoadExternalMusic(int playerIndex, string diskPath);

        void PlayMusic(int playerIndex);

        void PauseMusic(int playerIndex);

        void StopMusic(int playerIndex);

        void Seek(int playerIndex, float position);

        void SetVolume(int playerIndex, float volume);

        float GetVolume(int playerIndex);

        void SetSpeed(int playerIndex, float speed);

        float GetSpeed(int playerIndex);

        double GetDuration(int playerIndex);

        double GetPosition(int playerIndex);

        bool IsPlaying(int playerIndex);

        void SetRepeat(int playerIndex, bool repeat);

        bool IsRepeat(int playerIndex);
    }
}