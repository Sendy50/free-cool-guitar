using System;
using UnityEngine;

namespace MWM.Audio.Internal.Android.Mgae
{
    public class SamplingSynthUnitImpl : SamplingSynthUnit
    {
        private readonly jvalue[] _playNoteArgs;

        private readonly IntPtr _playNoteMethodPtr;

        private readonly jvalue[] _stopNoteArgs;

        private readonly IntPtr _stopNoteMethodPtr;
        private readonly AndroidJavaObject _this;

        private readonly IntPtr _thisObjPtr;

        public SamplingSynthUnitImpl(AndroidJavaObject samplingSynthUnit)
        {
            _this = samplingSynthUnit;
            _thisObjPtr = _this.GetRawObject();
            _playNoteMethodPtr = AndroidJNI.GetMethodID(_this.GetRawClass(), "playNote", "(III)V");
            _playNoteArgs = new jvalue[3];
            _stopNoteMethodPtr = AndroidJNI.GetMethodID(_this.GetRawClass(), "stopNote", "(II)V");
            _stopNoteArgs = new jvalue[3];
        }

        public InstrumentConfig CreateInternalInstrument(string instrumentResourcePath)
        {
            var instrumentJavaObject =
                _this.Call<AndroidJavaObject>("createInternalInstrument", instrumentResourcePath);
            return new InstrumentConfig(instrumentJavaObject);
        }

        public InstrumentConfig CreateExternalInstrument(string instrumentDiskPath)
        {
            var instrumentJavaObject = _this.Call<AndroidJavaObject>("createExternalInstrument", instrumentDiskPath);
            return new InstrumentConfig(instrumentJavaObject);
        }

        public void Load(InstrumentConfig instrumentConfig)
        {
            _this.Call("load", instrumentConfig.GetAndroidJavaObject());
        }

        public void PlayNote(int trackIndex, int midiNote, int velocity)
        {
            _playNoteArgs[0].i = trackIndex;
            _playNoteArgs[1].i = midiNote;
            _playNoteArgs[2].i = velocity;
            AndroidJNI.CallVoidMethod(_thisObjPtr, _playNoteMethodPtr, _playNoteArgs);
        }

        public void StopNote(int trackIndex, int midiNote)
        {
            _stopNoteArgs[0].i = trackIndex;
            _stopNoteArgs[1].i = midiNote;
            AndroidJNI.CallVoidMethod(_thisObjPtr, _stopNoteMethodPtr, _stopNoteArgs);
        }

        public void SetPedalStatus(int trackIndex, bool enable)
        {
            _this.Call("setPedalStatus", trackIndex, enable);
        }

        public void SetNumberOfTracks(int numberOfTracks)
        {
            _this.Call("setNumberOfTracks", numberOfTracks);
        }

        public void StopAllNotes(int trackIndex)
        {
            _this.Call("stopAllNotes", trackIndex);
        }
    }
}