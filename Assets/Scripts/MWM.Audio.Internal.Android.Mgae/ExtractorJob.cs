using System;
using UnityEngine;

namespace MWM.Audio.Internal.Android.Mgae
{
    public class ExtractorJob
    {
        private readonly AndroidJavaObject _this;

        public ExtractorJob(AndroidJavaObject extractorJob)
        {
            _this = extractorJob;
        }

        public ExtractorStatus GetStatus()
        {
            var androidJavaObject = _this.Call<AndroidJavaObject>("getStatus");
            var value = androidJavaObject.Call<string>("name");
            try
            {
                return (ExtractorStatus) Enum.Parse(typeof(ExtractorStatus), value);
            }
            catch (ArgumentException)
            {
                return ExtractorStatus.Error;
            }
        }
    }
}