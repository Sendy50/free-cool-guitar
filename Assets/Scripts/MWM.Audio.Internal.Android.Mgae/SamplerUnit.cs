using System;

namespace MWM.Audio.Internal.Android.Mgae
{
    public interface SamplerUnit
    {
        event Action<int> SampleStarted;

        event Action<int> SampleStopped;

        void Load(SamplePathHolder samplePathHolder);

        void PlaySample(int sampleIndex);

        void StopSample(int sampleIndex);

        float GetDuration(int sampleIndex);
    }
}