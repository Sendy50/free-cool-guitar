using System.Collections.Generic;

namespace MWM.Audio.Internal.Android.Mgae
{
    public interface Extractor
    {
        ExtractorRequest CreateRequestForSamplerFiles(List<string> resourcePaths, List<string> diskPaths);

        ExtractorRequest CreateRequestForSamplingSynthFiles(List<string> resourcePaths, List<string> diskPaths);

        ExtractorJob Extract(ExtractorRequest extractorRequest);

        void RemoveUnusedFiles(ExtractorRequest extractorRequest);
    }
}