namespace MWM.Audio.Internal.Android.Mgae
{
    public static class AudioFileUtils
    {
        public static string ConvertSampleNameToAssetPath(string sampleName)
        {
            return "SamplesMWM/" + sampleName + ".opus";
        }

        public static string ConvertMusicNameToAssetPath(string musicName)
        {
            return "AllMusic/" + musicName + ".mp3";
        }
    }
}