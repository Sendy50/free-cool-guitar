using UnityEngine;

namespace MWM.Audio.Internal.Android.Mgae
{
    public class ExtractorRequest
    {
        private readonly AndroidJavaObject _this;

        public ExtractorRequest(AndroidJavaObject androidJavaObject)
        {
            _this = androidJavaObject;
        }

        public AndroidJavaObject GetAndroidJavaObject()
        {
            return _this;
        }
    }
}