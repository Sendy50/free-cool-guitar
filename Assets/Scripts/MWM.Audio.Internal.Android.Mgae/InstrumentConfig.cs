using UnityEngine;

namespace MWM.Audio.Internal.Android.Mgae
{
    public class InstrumentConfig
    {
        private readonly AndroidJavaObject _this;

        public InstrumentConfig(AndroidJavaObject instrumentJavaObject)
        {
            _this = instrumentJavaObject;
        }

        public AndroidJavaObject GetAndroidJavaObject()
        {
            return _this;
        }

        public string[] GetNoteFilePaths()
        {
            return _this.Call<string[]>("getNoteFilePaths");
        }

        public bool IsInternalInstrument()
        {
            return _this.Call<bool>("isInternalInstrument");
        }
    }
}