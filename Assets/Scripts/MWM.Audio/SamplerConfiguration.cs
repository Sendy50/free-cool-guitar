using System;
using System.Collections.Generic;

namespace MWM.Audio
{
    public class SamplerConfiguration
    {
        private readonly Dictionary<string, string> _externalSamples;
        private readonly Dictionary<string, string> _internalSamples;

        public SamplerConfiguration()
        {
            _internalSamples = new Dictionary<string, string>();
            _externalSamples = new Dictionary<string, string>();
        }

        public void AddInternal(string id, string resourcePath)
        {
            EnsureIdNotAlreadyAdded(id);
            _internalSamples.Add(id, resourcePath);
        }

        public void AddExternal(string id, string diskPath)
        {
            EnsureIdNotAlreadyAdded(id);
            _externalSamples.Add(id, diskPath);
        }

        public IEnumerable<KeyValuePair<string, string>> GetInternalSamples()
        {
            return _internalSamples;
        }

        public IEnumerable<KeyValuePair<string, string>> GetExternalSamples()
        {
            return _externalSamples;
        }

        private void EnsureIdNotAlreadyAdded(string id)
        {
            if (_internalSamples.ContainsKey(id))
                throw new ApplicationException("There is already an internal sample with the id " + id);
            if (_externalSamples.ContainsKey(id))
                throw new ApplicationException("There is already an external sample with the id " + id);
        }
    }
}