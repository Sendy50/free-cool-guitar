namespace MWM.Audio
{
    public interface MultiPlayerUnit
    {
        Player GetPlayer(int index);
    }
}