using System;

namespace MWM.Audio
{
    public interface SamplingSynthUnit
    {
        void LoadExternal(string diskDirectoryPath, Action<LoadingResult> completion);

        void LoadInternal(string resourceDirectoryPath, Action<LoadingResult> completion);

        void NoteOn(int trackIndex, int noteNumber, int velocity);

        void NoteOff(int trackIndex, int noteNumber);

        void PedalOn(int trackIndex);

        void PedalOff(int trackIndex);

        void StopAllNotes(bool clearAudio);

        void SetNumberTracks(int nbTracks);
    }
}