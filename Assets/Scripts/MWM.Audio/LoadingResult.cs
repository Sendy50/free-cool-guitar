namespace MWM.Audio
{
    public class LoadingResult
    {
        private readonly string _description;
        private readonly LoadingStatus _loadingStatus;

        private LoadingResult(LoadingStatus loadingStatus, string description = "")
        {
            _loadingStatus = loadingStatus;
            _description = description;
        }

        public bool IsSuccess()
        {
            return _loadingStatus == LoadingStatus.Ok;
        }

        public LoadingStatus GetStatus()
        {
            return _loadingStatus;
        }

        public string GetDescription()
        {
            return _description;
        }

        public static LoadingResult Ok()
        {
            return new LoadingResult(LoadingStatus.Ok, string.Empty);
        }

        public static LoadingResult AudioFormatNotSupported(string description = "")
        {
            return new LoadingResult(LoadingStatus.AudioFormatNotSupported, description);
        }

        public static LoadingResult AlreadyLoadingError(string description = "")
        {
            return new LoadingResult(LoadingStatus.AlreadyLoading, description);
        }

        public static LoadingResult InstrumentLoadingFailure(string description = "")
        {
            return new LoadingResult(LoadingStatus.Failed, description);
        }

        public static LoadingResult ErrorNotEnoughSpace(string description = "")
        {
            return new LoadingResult(LoadingStatus.NotEnoughSpace, description);
        }
    }
}