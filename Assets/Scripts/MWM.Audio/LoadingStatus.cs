namespace MWM.Audio
{
    public enum LoadingStatus
    {
        Ok,
        NotEnoughSpace,
        AudioFormatNotSupported,
        AlreadyLoading,
        Failed
    }
}