using System;

namespace MWM.Audio
{
    public interface SamplerUnit
    {
        event Action<string> SampleStarted;

        event Action<string> SampleStopped;

        void Load(SamplerConfiguration configuration, Action<LoadingResult> completion);

        void Start(string sampleId);

        void Stop(string sampleId);

        float GetTotalTime(string sampleId);

        void StopAll();
    }
}