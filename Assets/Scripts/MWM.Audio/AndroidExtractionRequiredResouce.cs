using System.Collections;
using MWM.Core.StartApplication;
using UnityEngine;
using UnityEngine.UI;

namespace MWM.Audio
{
    public class AndroidExtractionRequiredResouce : IRequiredResource
    {
        private const string AssetFolderToExtract = "game_audio/";

        private const string EffectsDirectory = "game_audio/SamplesMWM";

        private readonly AndroidJavaObject _musicPlayerImpl;

        public bool NeedToBeLoaded()
        {
            return false;
        }

        public IEnumerator Load(Text statusText)
        {
            return null;
        }
    }
}