using System;

namespace MWM.Audio
{
    public interface Player
    {
        event Action<Player, bool> PlayingStateChanged;

        event Action<Player> EndOfFileReached;

        void LoadExternal(string diskPath, Action<LoadingResult> completion);

        void LoadInternal(string resourcePath, Action<LoadingResult> completion);

        void Play();

        void Pause();

        void Stop();

        bool IsPlaying();

        float GetTotalTime();

        float GetCurrentTime();

        void Seek(float time);

        void SetVolume(float newVolume);

        float GetVolume();

        void SetSpeed(float newSpeed);

        float GetSpeed();

        void SetRepeat(bool enable);

        bool IsRepeat();
    }
}