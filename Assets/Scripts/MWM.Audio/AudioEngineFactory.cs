using System;
using MWM.Audio.Internal.Android;
using UnityEngine;

namespace MWM.Audio
{
    public class AudioEngineFactory : MonoBehaviour
    {
        public void CreateForApp(int nbPlayers, int nbSamples, int preferredNbTracks,
            Action<MusicGamingAudioEngine> completion)
        {
            AndroidEngineFactory.Create(this, nbPlayers, nbSamples, preferredNbTracks, completion);
        }

        public void CreateForGame(int nbPlayers, int nbSamples, int preferredNbTracks,
            Action<MusicGamingAudioEngine> completion)
        {
            AndroidEngineFactory.Create(this, nbPlayers, nbSamples, preferredNbTracks, completion);
        }
    }
}