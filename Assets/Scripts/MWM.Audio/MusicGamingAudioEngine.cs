namespace MWM.Audio
{
    public interface MusicGamingAudioEngine
    {
        MultiPlayerUnit GetMultiPlayerUnit();

        SamplerUnit GetSamplerUnit();

        SamplingSynthUnit GetSamplingSynthUnit();

        void SetAudioRendering(bool enable);

        void Destroy();
    }
}