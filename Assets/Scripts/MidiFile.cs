using System;

[Serializable]
public class MidiFile
{
    public float startTime;

    public float duration;

    public MidiTrack[] tracks;
}