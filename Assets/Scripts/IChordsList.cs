using System;
using System.Collections.Generic;

public interface IChordsList
{
    event Action<IChordsList> OnSelectedChodsListChanged;

    void AddChord(IMidiChord chord);

    bool RemoveChord(IMidiChord chord);

    List<IMidiChord> GetPlayableChords();
}