public interface IChord
{
    int GetHash();

    string GetName();

    bool GetIsSolo();

    int[] GetFingers();

    string GetKey();

    string GetSuffix();
}