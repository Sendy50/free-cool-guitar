using UnityEngine;

namespace Store.Android
{
    public class GooglePlaySubInfos
    {
        public readonly string PurchaseToken;
        public readonly string Sku;

        public GooglePlaySubInfos(string sku, string purchaseToken)
        {
            Sku = sku;
            PurchaseToken = purchaseToken;
        }

        public static GooglePlaySubInfos GetGooglePlaySubInfosFromJsonString(string json)
        {
            var mWMGooglePlayReceipt = JsonUtility.FromJson<MWMGooglePlayReceipt>(json);
            var mWMGooglePlayReceiptPayloadContainer =
                JsonUtility.FromJson<MWMGooglePlayReceiptPayloadContainer>(mWMGooglePlayReceipt.Payload);
            mWMGooglePlayReceipt.ReceiptPayload =
                JsonUtility.FromJson<MWMGooglePlayReceiptPayload>(mWMGooglePlayReceiptPayloadContainer.json);
            return new GooglePlaySubInfos(mWMGooglePlayReceipt.ReceiptPayload.productId,
                mWMGooglePlayReceipt.ReceiptPayload.purchaseToken);
        }
    }
}