using System;
using UnityEngine;

namespace Store.Android
{
    [Serializable]
    public class MWMGooglePlayReceipt
    {
        [SerializeField] public string TransactionID;

        [SerializeField] public string Payload;

        public MWMGooglePlayReceiptPayload ReceiptPayload;
    }
}