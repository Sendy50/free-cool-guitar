using System;
using UnityEngine;

namespace Store.Android
{
    [Serializable]
    public class MWMGooglePlayReceiptPayload
    {
        [SerializeField] public string packageName;

        [SerializeField] public string productId;

        [SerializeField] public string purchaseToken;
    }
}