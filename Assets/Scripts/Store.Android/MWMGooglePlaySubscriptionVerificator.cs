using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using UnityEngine;
using UnityEngine.Networking;

namespace Store.Android
{
    public class MWMGooglePlaySubscriptionVerificator
    {
        public MWMGooglePlaySubscriptionVerificator(MonoBehaviour monoBehaviour, string endAppId, string endAppKey,
            string eventInstallationId, string endInstallationId = null)
        {
            MonoBehaviour = monoBehaviour;
            EndAppId = endAppId;
            EndAppKey = endAppKey;
            EventInstallationId = eventInstallationId;
            EndInstallationId = endInstallationId;
        }

        public string EndAppId { get; }

        public string EndAppKey { get; }

        public string EventInstallationId { get; }

        public string EndInstallationId { get; }

        public MonoBehaviour MonoBehaviour { get; }

        public void VerifySubscription(List<GooglePlaySubInfos> subsInfos, Action<Dictionary<string, bool>> completion)
        {
            var dictionary = new Dictionary<string, object>();
            dictionary.Add("install_id", EventInstallationId);
            dictionary.Add("bundle", Application.identifier);
            var dictionary2 = dictionary;
            var list = new List<Dictionary<string, string>>();
            for (var i = 0; i < subsInfos.Count; i++)
            {
                var googlePlaySubInfos = subsInfos[i];
                var dictionary3 = new Dictionary<string, string>();
                dictionary3.Add("sku", googlePlaySubInfos.Sku);
                dictionary3.Add("token", googlePlaySubInfos.PurchaseToken);
                var item = dictionary3;
                list.Add(item);
            }

            dictionary2.Add("in_apps", list);
            var s = JsonConvert.SerializeObject(dictionary2);
            var bytes = new UTF8Encoding().GetBytes(s);
            var request = new UnityWebRequest(GetVerifyUrl(Debug.isDebugBuild))
            {
                uploadHandler = new UploadHandlerRaw(bytes),
                downloadHandler = new DownloadHandlerBuffer(),
                method = "POST"
            };
            AddCustomHeaders(request);
            MonoBehaviour.StartCoroutine(POST(request, delegate
            {
                if (!request.isNetworkError && !request.isHttpError)
                {
                    var dictionary4 = new Dictionary<string, bool>();
                    var text = request.downloadHandler.text;
                    var jObject = JObject.Parse(text);
                    var jArray = jObject["validation"].ToObject<JArray>();
                    for (var j = 0; j < jArray.Count; j++)
                    {
                        var jToken = jArray[j];
                        dictionary4.Add(jToken["sku"].ToObject<string>(), jToken["validated"].ToObject<bool>());
                    }

                    completion(dictionary4);
                }
            }));
        }

        private IEnumerator POST(UnityWebRequest request, Action<UnityWebRequest> completion)
        {
            yield return request.SendWebRequest();
            completion(request);
        }

        private void AddCustomHeaders(UnityWebRequest request)
        {
            request.SetRequestHeader("Content-Type", "application/json");
            request.SetRequestHeader("X-End-App-Id", EndAppId);
            request.SetRequestHeader("X-End-Key", EndAppKey);
        }

        private static string GetVerifyUrl(bool isDevelopementBuild)
        {
            return !isDevelopementBuild
                ? "https://djit-end.appspot.com/0.4/in_app/validation/android"
                : "https://dev-dot-djit-end.appspot.com/0.4/in_app/validation/android";
        }
    }
}