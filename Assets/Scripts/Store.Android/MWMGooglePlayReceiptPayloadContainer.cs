using System;
using UnityEngine;

namespace Store.Android
{
    [Serializable]
    public class MWMGooglePlayReceiptPayloadContainer
    {
        [SerializeField] public string json;
    }
}