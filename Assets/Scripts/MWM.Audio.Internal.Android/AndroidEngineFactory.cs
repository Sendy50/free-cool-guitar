using System;
using JetBrains.Annotations;
using MWM.Audio.Internal.Android.Mgae;
using UnityEngine;

namespace MWM.Audio.Internal.Android
{
    public static class AndroidEngineFactory
    {
        [UsedImplicitly]
        public static void Create(MonoBehaviour monoBehaviour, int nbPlayers, int nbSamples, int preferredNbTracks,
            Action<MusicGamingAudioEngine> completion)
        {
            var audioEngineConfig = new AudioEngineConfig(2, 0);
            var multiPlayerConfig = nbPlayers <= 0 ? null : new MultiPlayerConfig(nbPlayers);
            var samplerConfig = nbSamples <= 0 ? null : new SamplerConfig(nbSamples, 2);
            var samplingSynthConfig = preferredNbTracks <= 0 ? null : new SamplingSynthConfig(preferredNbTracks, 30);
            var androidJavaClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            var @static = androidJavaClass.GetStatic<AndroidJavaObject>("currentActivity");
            var audioEngineImpl = new AudioEngineImpl(@static, audioEngineConfig, samplerConfig, samplingSynthConfig,
                multiPlayerConfig);
            var multiPlayerUnit = multiPlayerConfig == null
                ? null
                : new AndroidMultiPlayer(audioEngineImpl.GetMultiPlayerUnit(), nbPlayers);
            var samplerUnit = samplerConfig == null
                ? null
                : new AndroidSampler(monoBehaviour, audioEngineImpl.GetSamplerUnit());
            var samplingSynthUnit = samplingSynthConfig == null
                ? null
                : new AndroidSamplingSynthUnit(monoBehaviour, preferredNbTracks,
                    audioEngineImpl.GetSamplingSynthUnit());
            var obj = new AndroidMusicGamingAudioEngine(audioEngineImpl, multiPlayerUnit, samplerUnit,
                samplingSynthUnit);
            completion(obj);
        }
    }
}