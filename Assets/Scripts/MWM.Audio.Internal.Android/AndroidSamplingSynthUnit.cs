using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using MWM.Audio.Internal.Android.Mgae;
using UnityEngine;

namespace MWM.Audio.Internal.Android
{
    public class AndroidSamplingSynthUnit : SamplingSynthUnit
    {
        private readonly MonoBehaviour _monoBehaviour;

        private readonly Mgae.SamplingSynthUnit _samplingSynthUnit;

        private int _numberOfTracks;

        public AndroidSamplingSynthUnit(MonoBehaviour monoBehaviour, int initialNumberOfTracks,
            Mgae.SamplingSynthUnit samplingSynthUnit)
        {
            _monoBehaviour = monoBehaviour;
            _samplingSynthUnit = samplingSynthUnit;
            _numberOfTracks = initialNumberOfTracks;
        }

        public void LoadExternal(string diskDirectoryPath, Action<LoadingResult> completion)
        {
            var instrumentDiskPath = diskDirectoryPath + "/instrument_config.json";
            var instrumentConfig = _samplingSynthUnit.CreateExternalInstrument(instrumentDiskPath);
            _monoBehaviour.StartCoroutine(LoadInstrument(instrumentConfig, completion));
        }

        public void LoadInternal(string resourceDirectoryPath, Action<LoadingResult> completion)
        {
            var instrumentResourcePath = resourceDirectoryPath + "/instrument_config.json";
            var instrumentConfig = _samplingSynthUnit.CreateInternalInstrument(instrumentResourcePath);
            _monoBehaviour.StartCoroutine(LoadInstrument(instrumentConfig, completion));
        }


        public void NoteOn(int trackIndex, int noteNumber, int velocity)
        {
            _samplingSynthUnit.PlayNote(trackIndex, noteNumber, velocity);
        }

        public void NoteOff(int trackIndex, int noteNumber)
        {
            _samplingSynthUnit.StopNote(trackIndex, noteNumber);
        }

        public void PedalOn(int trackIndex)
        {
            _samplingSynthUnit.SetPedalStatus(trackIndex, true);
        }

        public void PedalOff(int trackIndex)
        {
            _samplingSynthUnit.SetPedalStatus(trackIndex, false);
        }

        public void StopAllNotes(bool clearAudio)
        {
            for (var i = 0; i < _numberOfTracks; i++) _samplingSynthUnit.StopAllNotes(i);
        }

        public void SetNumberTracks(int nbTracks)
        {
            _numberOfTracks = nbTracks;
            _samplingSynthUnit.SetNumberOfTracks(nbTracks);
        }

        private IEnumerator LoadInstrument(InstrumentConfig instrumentConfig, Action<LoadingResult> completion)
        {
            var isInternalInstrument = instrumentConfig.IsInternalInstrument();
            var noteFilePaths = instrumentConfig.GetNoteFilePaths();
            var noteFilePathsList = noteFilePaths.ToList();
            var resourceFiles = !isInternalInstrument ? new List<string>() : noteFilePathsList;
            var diskFiles = !isInternalInstrument ? noteFilePathsList : new List<string>();
            var extractorImpl = new ExtractorImpl();
            var request = extractorImpl.CreateRequestForSamplingSynthFiles(resourceFiles, diskFiles);
            extractorImpl.RemoveUnusedFiles(request);
            var extractorJob = extractorImpl.Extract(request);
            yield return new WaitUntil(() => extractorJob.GetStatus() != ExtractorStatus.InProgress);
            var extractorStatus = extractorJob.GetStatus();
            if (extractorStatus != ExtractorStatus.Success) throw new ApplicationException("Extraction failed");
            _samplingSynthUnit.Load(instrumentConfig);
            completion(LoadingResult.Ok());
        }
    }
}