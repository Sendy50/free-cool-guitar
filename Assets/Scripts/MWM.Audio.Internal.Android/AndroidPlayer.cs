using System;

namespace MWM.Audio.Internal.Android
{
    public class AndroidPlayer : Player
    {
        private readonly Mgae.MultiPlayerUnit _androidMultiPlayerUnit;

        private readonly int _index;

        public AndroidPlayer(int index, Mgae.MultiPlayerUnit androidMultiPlayerUnit)
        {
            _index = index;
            _androidMultiPlayerUnit = androidMultiPlayerUnit;
            _androidMultiPlayerUnit.PlayingStateChanged += delegate(int playerIndex, bool playingState)
            {
                if (playerIndex == _index && PlayingStateChanged != null) PlayingStateChanged(this, playingState);
            };
            _androidMultiPlayerUnit.EndOfFileReached += delegate(int playerIndex)
            {
                if (playerIndex == _index && EndOfFileReached != null) EndOfFileReached(this);
            };
        }

        public event Action<Player, bool> PlayingStateChanged;

        public event Action<Player> EndOfFileReached;

        public void LoadExternal(string diskPath, Action<LoadingResult> completion)
        {
            Stop();
            _androidMultiPlayerUnit.LoadExternalMusic(_index, diskPath);
            completion(LoadingResult.Ok());
        }

        public void LoadInternal(string resourcePath, Action<LoadingResult> completion)
        {
            Stop();
            _androidMultiPlayerUnit.LoadInternalMusic(_index, resourcePath);
            completion(LoadingResult.Ok());
        }

        public void Play()
        {
            _androidMultiPlayerUnit.PlayMusic(_index);
        }

        public void Pause()
        {
            _androidMultiPlayerUnit.PauseMusic(_index);
        }

        public void Stop()
        {
            _androidMultiPlayerUnit.StopMusic(_index);
        }

        public bool IsPlaying()
        {
            return _androidMultiPlayerUnit.IsPlaying(_index);
        }

        public float GetTotalTime()
        {
            return (float) _androidMultiPlayerUnit.GetDuration(_index);
        }

        public float GetCurrentTime()
        {
            return (float) _androidMultiPlayerUnit.GetPosition(_index);
        }

        public void Seek(float position)
        {
            _androidMultiPlayerUnit.Seek(_index, position);
        }

        public void SetVolume(float newVolume)
        {
            _androidMultiPlayerUnit.SetVolume(_index, newVolume);
        }

        public float GetVolume()
        {
            return _androidMultiPlayerUnit.GetVolume(_index);
        }

        public void SetSpeed(float newSpeed)
        {
            _androidMultiPlayerUnit.SetSpeed(_index, newSpeed);
        }

        public float GetSpeed()
        {
            return _androidMultiPlayerUnit.GetSpeed(_index);
        }

        public void SetRepeat(bool enable)
        {
            _androidMultiPlayerUnit.SetRepeat(_index, enable);
        }

        public bool IsRepeat()
        {
            return _androidMultiPlayerUnit.IsRepeat(_index);
        }
    }
}