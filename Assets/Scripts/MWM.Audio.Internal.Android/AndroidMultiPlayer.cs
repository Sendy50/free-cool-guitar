using System;

namespace MWM.Audio.Internal.Android
{
    public class AndroidMultiPlayer : MultiPlayerUnit
    {
        private readonly Player[] _players;

        public AndroidMultiPlayer(Mgae.MultiPlayerUnit multiPlayerUnit, int nbPlayers)
        {
            _players = new Player[nbPlayers];
            for (var i = 0; i < nbPlayers; i++)
            {
                var androidPlayer = new AndroidPlayer(i, multiPlayerUnit);
                _players[i] = androidPlayer;
            }
        }

        public Player GetPlayer(int index)
        {
            if (index < 0 || index >= _players.Length)
                throw new IndexOutOfRangeException("Player index range: [0; " + _players.Length + "] Found: " + index);
            return _players[index];
        }
    }
}