using JetBrains.Annotations;
using MWM.Audio.Internal.Android.Mgae;

namespace MWM.Audio.Internal.Android
{
    public class AndroidMusicGamingAudioEngine : MusicGamingAudioEngineImpl
    {
        private readonly AudioEngine _audioEngine;

        public AndroidMusicGamingAudioEngine(AudioEngine audioEngine, [CanBeNull] MultiPlayerUnit multiPlayerUnit,
            [CanBeNull] SamplerUnit samplerUnit, [CanBeNull] SamplingSynthUnit samplingSynthUnit)
            : base(multiPlayerUnit, samplerUnit, samplingSynthUnit)
        {
            _audioEngine = audioEngine;
        }

        public override void Destroy()
        {
            base.Destroy();
            _audioEngine.Destroy();
        }
    }
}