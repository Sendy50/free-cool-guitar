using System;
using System.Collections;
using System.Collections.Generic;
using MWM.Audio.Internal.Android.Mgae;
using UnityEngine;

namespace MWM.Audio.Internal.Android
{
    public class AndroidSampler : SamplerUnit
    {
        private readonly List<string> _loadedSampleIds;

        private readonly MonoBehaviour _monoBehaviour;

        private readonly Mgae.SamplerUnit _samplerUnit;

        private UnityDispatcher _androidSamplerDispatcher;

        public AndroidSampler(MonoBehaviour monoBehaviour, Mgae.SamplerUnit samplerUnit)
        {
            _monoBehaviour = monoBehaviour;
            _samplerUnit = samplerUnit;
            _loadedSampleIds = new List<string>();
            var gameObject = new GameObject("AndroidSamplerUnityDispatcher");
            var unityDispatcher = gameObject.AddComponent<UnityDispatcher>();
            unityDispatcher.SetAndroidSampler(this);
            unityDispatcher.transform.parent = monoBehaviour.transform;
            samplerUnit.SampleStarted += unityDispatcher.SamplerUnitOnSampleStarted;
            samplerUnit.SampleStopped += unityDispatcher.SamplerUnitOnSampleStopped;
        }

        public event Action<string> SampleStarted;

        public event Action<string> SampleStopped;

        public void Load(SamplerConfiguration configuration, Action<LoadingResult> completion)
        {
            var routine = LoadJob(configuration, completion);
            _monoBehaviour.StartCoroutine(routine);
        }

        public void Start(string sampleId)
        {
            var sampleIndex = GetSampleIndex(sampleId);
            _samplerUnit.PlaySample(sampleIndex);
        }

        public void Stop(string sampleId)
        {
            var sampleIndex = GetSampleIndex(sampleId);
            _samplerUnit.StopSample(sampleIndex);
        }

        public float GetTotalTime(string sampleId)
        {
            var sampleIndex = GetSampleIndex(sampleId);
            return _samplerUnit.GetDuration(sampleIndex);
        }

        public void StopAll()
        {
            var count = _loadedSampleIds.Count;
            for (var i = 0; i < count; i++) _samplerUnit.StopSample(i);
        }

        private int GetSampleIndex(string sampleId)
        {
            return _loadedSampleIds.IndexOf(sampleId);
        }

        private string GetSampleId(int sampleIndex)
        {
            if (sampleIndex < 0 || sampleIndex >= _loadedSampleIds.Count)
                throw new IndexOutOfRangeException("Index must be in range [0; " + _loadedSampleIds.Count +
                                                   "[ Found: " + sampleIndex);
            return _loadedSampleIds[sampleIndex];
        }

        private IEnumerator LoadJob(SamplerConfiguration samplerConfiguration, Action<LoadingResult> completion)
        {
            _loadedSampleIds.Clear();
            var resourcePaths = new List<string>();
            var diskPaths = new List<string>();
            var internalSamples = samplerConfiguration.GetInternalSamples();
            var externalSamples = samplerConfiguration.GetExternalSamples();
            var newSampleIds = new List<string>();
            var samplePathHolder = new SamplePathHolder();
            foreach (var item in internalSamples)
            {
                var key = item.Key;
                var text = item.Value + ".opus";
                resourcePaths.Add(text);
                newSampleIds.Add(key);
                samplePathHolder.AddResourcePath(text);
            }

            foreach (var item2 in externalSamples)
            {
                var key2 = item2.Key;
                var value = item2.Value;
                diskPaths.Add(value);
                newSampleIds.Add(key2);
                samplePathHolder.AddDiskPath(value);
            }

            var extractor = new ExtractorImpl();
            var request = extractor.CreateRequestForSamplerFiles(resourcePaths, diskPaths);
            extractor.RemoveUnusedFiles(request);
            var extractorJob = extractor.Extract(request);
            yield return new WaitUntil(() => extractorJob.GetStatus() != ExtractorStatus.InProgress);
            if (extractorJob.GetStatus() == ExtractorStatus.Error) throw new ApplicationException("Extraction failed");
            _samplerUnit.Load(samplePathHolder);
            _loadedSampleIds.AddRange(newSampleIds);
            completion(LoadingResult.Ok());
        }

        private void SamplerUnitOnSampleStopped(int sampleIndex)
        {
            var sampleId = GetSampleId(sampleIndex);
            if (SampleStopped != null) SampleStopped(sampleId);
        }

        private void SamplerUnitOnSampleStarted(int sampleIndex)
        {
            var sampleId = GetSampleId(sampleIndex);
            if (SampleStarted != null) SampleStarted(sampleId);
        }

        private class UnityDispatcher : MonoBehaviour
        {
            private const int MessageTypeSampleStopped = 30000000;

            private const int MessageTypeSampleStarted = 20000000;

            private readonly Queue<int> _messages = new Queue<int>();

            private AndroidSampler _androidSampler;

            private void Update()
            {
                lock (_messages)
                {
                    while (_messages.Count > 0)
                    {
                        var num = _messages.Dequeue();
                        if (num >= 30000000)
                        {
                            var sampleIndex = num - 30000000;
                            _androidSampler.SamplerUnitOnSampleStopped(sampleIndex);
                        }
                        else if (num >= 20000000)
                        {
                            var sampleIndex2 = num - 20000000;
                            _androidSampler.SamplerUnitOnSampleStarted(sampleIndex2);
                        }
                    }
                }
            }

            public void SetAndroidSampler(AndroidSampler androidSampler)
            {
                _androidSampler = androidSampler;
            }

            public void SamplerUnitOnSampleStopped(int sampleIndex)
            {
                lock (_messages)
                {
                    _messages.Enqueue(30000000 + sampleIndex);
                }
            }

            public void SamplerUnitOnSampleStarted(int sampleIndex)
            {
                lock (_messages)
                {
                    _messages.Enqueue(20000000 + sampleIndex);
                }
            }
        }
    }
}