namespace MWM.Ads
{
    public enum PlacementInteractionState
    {
        None,
        Clicked,
        Rewarded
    }
}