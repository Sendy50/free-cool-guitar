using System;
using MWM.Core.Settings;
using UnityEngine;

namespace MWM.Ads
{
    public abstract class Placement : ScriptableObject
    {
        public CoreSettings InternalCoreSettings;

        [Header("Prototype ids")] public string InternalPlacementIdIos;

        public string InternalPlacementIdAndroid;

        [Header("MWM ids")] public string InternalPlacementIdIosMWM;

        public string InternalPlacementIdAndroidMWM;

        [HideInInspector] public PlacementLoadingState LoadingState;

        [HideInInspector] public PlacementDisplayState DisplayState;

        [HideInInspector] public PlacementInteractionState InteractionState;

        private Action<PlacementInteractionState> _closingCompletionAction;

        private Action<PlacementShowHideResult> _displayCompletionAction;

        private Action<PlacementLoadingState> _loadingCompletionAction;

        public string GetPlacementId()
        {
            return InternalCoreSettings.CurrentBuildTarget != BuildTarget.Prototype
                ? InternalPlacementIdAndroidMWM
                : InternalPlacementIdAndroid;
        }

        public abstract PlacementType GetPlacementType();

        public void PrepareForNewLoading()
        {
            LoadingState = PlacementLoadingState.NotLoaded;
            _loadingCompletionAction = null;
        }

        public void PrepareForNewDisplay()
        {
            DisplayState = PlacementDisplayState.NotDisplayed;
            InteractionState = PlacementInteractionState.None;
            _displayCompletionAction = null;
            _closingCompletionAction = null;
        }

        public void SetLoadingCompletionAction(Action<PlacementLoadingState> loadingCompletionAction)
        {
            _loadingCompletionAction = loadingCompletionAction;
        }

        public void NotifyLoadingCompleted()
        {
            if (_loadingCompletionAction != null) _loadingCompletionAction(LoadingState);
        }

        public void SetDisplayCompletionAction(Action<PlacementShowHideResult> displayCompletionAction)
        {
            _displayCompletionAction = displayCompletionAction;
        }

        public void SetClosingCompletionAction(Action<PlacementInteractionState> closingCompletionAction)
        {
            _closingCompletionAction = closingCompletionAction;
        }

        public void NotifyDisplayStateDidChange()
        {
            switch (DisplayState)
            {
                case PlacementDisplayState.Displayed:
                    if (_displayCompletionAction != null)
                        _displayCompletionAction(PlacementShowHideResult.ShowHideSucceeded);
                    break;
                case PlacementDisplayState.NotDisplayed:
                    if (_closingCompletionAction != null) _closingCompletionAction(InteractionState);
                    break;
            }
        }
    }
}