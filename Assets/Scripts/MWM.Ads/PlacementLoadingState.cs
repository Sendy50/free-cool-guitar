namespace MWM.Ads
{
    public enum PlacementLoadingState
    {
        NotLoaded,
        Failure,
        Expired,
        Loading,
        Loaded
    }
}