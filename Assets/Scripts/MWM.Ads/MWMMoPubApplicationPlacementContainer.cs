using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace MWM.Ads
{
    public class MWMMoPubApplicationPlacementContainer : ScriptableObject
    {
        public List<Placement> ApplicationPlacements;

        public Placement GetPlacementWithName(string placementName)
        {
            if (placementName == null) return null;
            return ApplicationPlacements.FirstOrDefault(placement => placement.name.Equals(placementName));
        }

        public Placement GetPlacementWithId(string placementId)
        {
            if (placementId == null) return null;
            return ApplicationPlacements.FirstOrDefault(placement => placement.GetPlacementId().Equals(placementId));
        }

        public string[] GetAllPlacementIds()
        {
            var source = ApplicationPlacements.Where(placement => placement.GetPlacementId().Length > 0);
            return source.Select(placement => placement.GetPlacementId()).ToArray();
        }

        public string[] GetPlacementIdsForType(PlacementType type)
        {
            var source = ApplicationPlacements.Where(placement => placement.GetPlacementId().Length > 0);
            source = source.Where(placement => placement.GetPlacementType() == type);
            return source.Select(placement => placement.GetPlacementId()).ToArray();
        }

        public static string[] GetPlacementIdsForTypeFromList(PlacementType type, List<Placement> sourceList)
        {
            var source = sourceList.Where(placement => placement.GetPlacementId().Length > 0);
            source = source.Where(placement => placement.GetPlacementType() == type);
            return source.Select(placement => placement.GetPlacementId()).ToArray();
        }
    }
}