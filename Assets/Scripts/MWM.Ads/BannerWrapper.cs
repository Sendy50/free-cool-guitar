using System;
using System.Collections.Generic;

namespace MWM.Ads
{
    public class BannerWrapper : PlacementTypeWrapper
    {
        private const double _bannerRecreateTreshold = 30.0;
        private DateTime _lastBannerCreationRequestDate;

        private MWMMoPubWrapper _parentWrapper;

        private Placement _uniqueBannerPlacement;

        public float BannerHeight { get; private set; }

        public PlacementDisplayState BannerDisplayState => !(_uniqueBannerPlacement == null)
            ? _uniqueBannerPlacement.DisplayState
            : PlacementDisplayState.NotDisplayed;

        public event BannerDisplayStateDidChange BannerDisplayStateDidChangeEvent;

        public override void Initialize(List<Placement> placements,
            MWMMoPubApplicationPlacementContainer placementsContainer, MWMMoPubWrapper parrentWrapper)
        {
            _placementsContainer = placementsContainer;
            BannerHeight = 0f;
            _uniqueBannerPlacement = null;
            _parentWrapper = parrentWrapper;
            var placementIdsForTypeFromList =
                MWMMoPubApplicationPlacementContainer.GetPlacementIdsForTypeFromList(PlacementType.Banner, placements);
            if (placementIdsForTypeFromList.Length > 1)
                throw new Exception("[BannerWrapper] multiple banners detected, only one should exist");
            // MoPubAndroid.LoadBannerPluginsForAdUnits(placementIdsForTypeFromList);
            _lastBannerCreationRequestDate = DateTime.Now.AddSeconds(-30.0);
        }

        public override void RegisterToMoPubEvents()
        {
            // MoPubManager.OnAdLoadedEvent += delegate(string adUnitId, float height)
            // {
            // 	if (_uniqueBannerPlacement == null)
            // 	{
            // 		throw new Exception("[MoPubWrapper][Banner] placement not found");
            // 	}
            // 	BannerHeight = height;
            // 	_uniqueBannerPlacement.LoadingState = PlacementLoadingState.Loaded;
            // 	_uniqueBannerPlacement.NotifyLoadingCompleted();
            // 	_uniqueBannerPlacement.DisplayState = PlacementDisplayState.Displayed;
            // 	OnBannerDisplayStateDidChange(_uniqueBannerPlacement);
            // 	_parentWrapper.OnAdDisplayNativeEventTriggered(PlacementType.Banner, "mopub", _uniqueBannerPlacement.GetPlacementId());
            // };
            // MoPubManager.OnAdFailedEvent += delegate
            // {
            // 	if (_uniqueBannerPlacement == null)
            // 	{
            // 		throw new Exception("[MoPubWrapper][Banner] placement not found");
            // 	}
            // 	_uniqueBannerPlacement.LoadingState = PlacementLoadingState.Failure;
            // 	_uniqueBannerPlacement.NotifyLoadingCompleted();
            // 	_uniqueBannerPlacement.PrepareForNewLoading();
            // };
        }

        public override string GetLastRequestedPlacementName()
        {
            return _uniqueBannerPlacement.name;
        }

        public override string GetLastDisplayedPlacementName()
        {
            return _uniqueBannerPlacement.name;
        }

        public override void LoadContent(string placementName, Action<PlacementLoadingState> loadingResult)
        {
            _uniqueBannerPlacement = _placementsContainer.GetPlacementWithName(placementName);
            if (_uniqueBannerPlacement == null)
            {
                loadingResult(PlacementLoadingState.Failure);
                return;
            }

            if (_uniqueBannerPlacement.LoadingState == PlacementLoadingState.Loading)
            {
                loadingResult(PlacementLoadingState.Loading);
                return;
            }

            if (_uniqueBannerPlacement.LoadingState == PlacementLoadingState.Loaded)
            {
                loadingResult(PlacementLoadingState.Loaded);
                return;
            }

            _uniqueBannerPlacement.LoadingState = PlacementLoadingState.Loading;
            _uniqueBannerPlacement.SetLoadingCompletionAction(loadingResult);
            _parentWrapper.OnAdRequestNativeEventTriggered(PlacementType.Banner, "mopub",
                _uniqueBannerPlacement.GetPlacementId());
            _lastBannerCreationRequestDate = DateTime.Now;
            // MoPubAndroid.CreateBanner(_uniqueBannerPlacement.GetPlacementId(), MoPubBase.AdPosition.BottomCenter);
        }

        public override void DisplayContent(string placementName, Action<PlacementShowHideResult> displayCompletion,
            Action<PlacementInteractionState> closingCompletion = null)
        {
            var placementWithName = _placementsContainer.GetPlacementWithName(placementName);
            if (placementWithName == null)
            {
                displayCompletion(PlacementShowHideResult.ShowHideFailed);
                return;
            }

            placementWithName.SetDisplayCompletionAction(displayCompletion);
            placementWithName.SetClosingCompletionAction(closingCompletion);
            if (placementWithName.LoadingState != PlacementLoadingState.Loaded)
            {
                displayCompletion(PlacementShowHideResult.ShowHideFailed);
                return;
            }

            switch (placementWithName.DisplayState)
            {
                case PlacementDisplayState.Displayed:
                    displayCompletion(PlacementShowHideResult.ShowHideSucceeded);
                    break;
                case PlacementDisplayState.NotDisplayed:
                    // MoPubAndroid.ShowBanner(placementWithName.GetPlacementId(), shouldShow: true);
                    placementWithName.DisplayState = PlacementDisplayState.Displayed;
                    OnBannerDisplayStateDidChange(placementWithName);
                    break;
            }
        }

        public void HideContent(string placementName)
        {
            var placementWithName = _placementsContainer.GetPlacementWithName(placementName);
            if (!(placementWithName == null) && placementWithName.DisplayState == PlacementDisplayState.Displayed)
            {
                // MoPubAndroid.ShowBanner(placementWithName.GetPlacementId(), shouldShow: false);
                placementWithName.DisplayState = PlacementDisplayState.NotDisplayed;
                OnBannerDisplayStateDidChange(placementWithName);
            }
        }

        public void ReloadContent(string placementName, Action<PlacementLoadingState> loadingResult)
        {
            var placementWithName = _placementsContainer.GetPlacementWithName(placementName);
            if (placementWithName == null)
            {
                loadingResult(PlacementLoadingState.Failure);
            }
            else if (DateTime.Now.Subtract(_lastBannerCreationRequestDate).TotalSeconds < 30.0)
            {
                loadingResult(PlacementLoadingState.Failure);
            }
            else if (placementWithName.DisplayState == PlacementDisplayState.Displayed)
            {
                HideContent(placementName);
                ReloadContent(placementName, loadingResult);
            }
            else if (placementWithName.LoadingState == PlacementLoadingState.Loaded)
            {
                // MoPubAndroid.DestroyBanner(placementWithName.GetPlacementId());
                placementWithName.PrepareForNewLoading();
                ReloadContent(placementName, loadingResult);
            }
            else
            {
                placementWithName.PrepareForNewDisplay();
                LoadContent(placementName, loadingResult);
            }
        }

        private void OnBannerDisplayStateDidChange(Placement placement)
        {
            placement.NotifyDisplayStateDidChange();
            if (BannerDisplayStateDidChangeEvent != null)
                BannerDisplayStateDidChangeEvent(placement.DisplayState, BannerHeight);
        }
    }
}