using System;
using System.Collections.Generic;

namespace MWM.Ads
{
    public class InterstitialWrapper : PlacementTypeWrapper
    {
        private string _lastDisplayedPlacementName;
        private string _lastRequestedPlacementName;

        private MWMMoPubWrapper _parentWrapper;

        public override void Initialize(List<Placement> placements,
            MWMMoPubApplicationPlacementContainer placementsContainer, MWMMoPubWrapper parrentWrapper)
        {
            _placementsContainer = placementsContainer;
            _parentWrapper = parrentWrapper;
            var placementIdsForTypeFromList =
                MWMMoPubApplicationPlacementContainer.GetPlacementIdsForTypeFromList(PlacementType.Interstitial,
                    placements);
            // MoPubAndroid.LoadInterstitialPluginsForAdUnits(placementIdsForTypeFromList);
            _lastRequestedPlacementName = null;
            _lastDisplayedPlacementName = null;
        }

        public override void RegisterToMoPubEvents()
        {
            // MoPubManager.OnInterstitialFailedEvent += delegate(string adUnitId, string errorMessage)
            // {
            // 	Placement placementWithId5 = _placementsContainer.GetPlacementWithId(adUnitId);
            // 	if (placementWithId5 == null)
            // 	{
            // 		throw new Exception("[MoPubWrapper][Interstitial] placement not found");
            // 	}
            // 	placementWithId5.LoadingState = PlacementLoadingState.Failure;
            // 	placementWithId5.NotifyLoadingCompleted();
            // 	placementWithId5.PrepareForNewLoading();
            // };
            // MoPubManager.OnInterstitialLoadedEvent += delegate(string id)
            // {
            // 	Placement placementWithId4 = _placementsContainer.GetPlacementWithId(id);
            // 	if (placementWithId4 == null)
            // 	{
            // 		throw new Exception("[MoPubWrapper][Interstitial] placement not found");
            // 	}
            // 	placementWithId4.LoadingState = PlacementLoadingState.Loaded;
            // 	placementWithId4.NotifyLoadingCompleted();
            // };
            // MoPubManager.OnInterstitialExpiredEvent += delegate(string id)
            // {
            // 	Placement placementWithId3 = _placementsContainer.GetPlacementWithId(id);
            // 	if (placementWithId3 == null)
            // 	{
            // 		throw new Exception("[MoPubWrapper][Interstitial] placement not found");
            // 	}
            // 	placementWithId3.LoadingState = PlacementLoadingState.Expired;
            // };
            // MoPubManager.OnInterstitialDismissedEvent += delegate(string id)
            // {
            // 	Placement placementWithId2 = _placementsContainer.GetPlacementWithId(id);
            // 	if (placementWithId2 == null)
            // 	{
            // 		throw new Exception("[MoPubWrapper][Interstitial] placement not found");
            // 	}
            // 	placementWithId2.DisplayState = PlacementDisplayState.NotDisplayed;
            // 	placementWithId2.NotifyDisplayStateDidChange();
            // 	placementWithId2.PrepareForNewDisplay();
            // };
            // MoPubManager.OnInterstitialShownEvent += delegate(string id)
            // {
            // 	Placement placementWithId = _placementsContainer.GetPlacementWithId(id);
            // 	if (placementWithId == null)
            // 	{
            // 		throw new Exception("[MoPubWrapper][Interstitial] placement not found");
            // 	}
            // 	placementWithId.DisplayState = PlacementDisplayState.Displayed;
            // 	placementWithId.NotifyDisplayStateDidChange();
            // };
        }

        public override void LoadContent(string placementName, Action<PlacementLoadingState> loadingResult)
        {
            var placementWithName = _placementsContainer.GetPlacementWithName(placementName);
            if (placementWithName == null)
            {
                loadingResult(PlacementLoadingState.Failure);
                return;
            }

            if (placementWithName.LoadingState == PlacementLoadingState.Loading)
            {
                loadingResult(PlacementLoadingState.Loading);
                return;
            }

            if (placementWithName.LoadingState == PlacementLoadingState.Loaded)
            {
                loadingResult(PlacementLoadingState.Loaded);
                return;
            }

            placementWithName.LoadingState = PlacementLoadingState.Loading;
            placementWithName.SetLoadingCompletionAction(loadingResult);
            _lastRequestedPlacementName = placementName;
            _parentWrapper.OnAdRequestNativeEventTriggered(PlacementType.Interstitial, "mopub",
                placementWithName.GetPlacementId());
            // MoPubAndroid.RequestInterstitialAd(placementWithName.GetPlacementId(), string.Empty, string.Empty);
        }

        public override void DisplayContent(string placementName, Action<PlacementShowHideResult> displayCompletion,
            Action<PlacementInteractionState> closingCompletion = null)
        {
            var placementWithName = _placementsContainer.GetPlacementWithName(placementName);
            if (placementWithName == null)
            {
                displayCompletion(PlacementShowHideResult.ShowHideFailed);
            }
            else if (placementWithName.LoadingState == PlacementLoadingState.Loaded)
            {
                placementWithName.SetDisplayCompletionAction(displayCompletion);
                placementWithName.SetClosingCompletionAction(closingCompletion);
                _lastDisplayedPlacementName = placementName;
                _parentWrapper.OnAdDisplayNativeEventTriggered(PlacementType.Interstitial, "mopub",
                    placementWithName.GetPlacementId());
                // MoPubAndroid.ShowInterstitialAd(placementWithName.GetPlacementId());
                placementWithName.PrepareForNewLoading();
            }
            else
            {
                displayCompletion(PlacementShowHideResult.ShowHideFailed);
            }
        }

        public override string GetLastRequestedPlacementName()
        {
            return _lastRequestedPlacementName;
        }

        public override string GetLastDisplayedPlacementName()
        {
            return _lastDisplayedPlacementName;
        }
    }
}