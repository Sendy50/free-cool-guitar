using System;
using System.Collections;
using System.Collections.Generic;
using MWM.GameEvent;
using UnityEngine;
using EventType = MWM.GameEvent.EventType;

namespace MWM.Ads
{
    public class MWMMoPubEventWaiter
    {
        public readonly MonoBehaviour monoBehaviour;

        private readonly Dictionary<string, Waiter> _pendings;

        public MWMMoPubEventWaiter(MonoBehaviour monoBehaviour)
        {
            this.monoBehaviour = monoBehaviour;
            _pendings = new Dictionary<string, Waiter>();
        }

        public void HandleNewEventData(AdEvent ev)
        {
            Waiter waiter = null;
            var type = ev.type;
            if (_pendings.ContainsKey(type))
            {
                waiter = _pendings[type];
            }

            if (waiter == null)
            {
                waiter = new Waiter();
                _pendings[type] = waiter;
                var waiter2 = waiter;
                waiter2.OnTrigger =
                    (Action<Waiter>) Delegate.Combine(waiter2.OnTrigger, new Action<Waiter>(OnWaiterTrigger));
            }

            if (ev.advertiser == "mopub")
            {
                waiter.secondary = ev;
            }
            else
            {
                waiter.priority = ev;
            }

            waiter.StartWaiting(monoBehaviour);
        }

        private void OnWaiterTrigger(Waiter sender)
        {
            var adEvent = sender.priority ?? sender.secondary;
            if (adEvent != null)
            {
                _pendings.Remove(adEvent.type);
                GameEventSender.Instance().SendEvent(EventType.AdDisplayed, adEvent.ToJSONString());
            }
        }

        public class Waiter
        {
            public const float Duration = 2f;

            private bool hasStartedToWait;

            public Action<Waiter> OnTrigger = delegate { };

            public AdEvent priority;

            public AdEvent secondary;

            public void StartWaiting(MonoBehaviour monoBehaviour)
            {
                if (!hasStartedToWait)
                {
                    hasStartedToWait = true;
                    monoBehaviour.StartCoroutine(WaitRoutine());
                }
            }

            private IEnumerator WaitRoutine()
            {
                yield return new WaitForSeconds(2f);
                if (OnTrigger != null)
                {
                    OnTrigger(this);
                }
            }
        }
    }
}