using System;
using System.Collections.Generic;
using MWM.GameEvent;
using UnityEngine;
using EventType = MWM.GameEvent.EventType;

namespace MWM.Ads
{
    public class MWMMoPubWrapper : MonoBehaviour
    {
        private static MWMMoPubWrapper _instance;
        public MWMMoPubConfiguration Configuration;

        public MWMMoPubApplicationPlacementContainer PlacementsContainer;

        private MonoBehaviour _coroutineLauncher;

        private MWMMoPubEventWaiter _displayAdsEventWaiter;

        private Dictionary<PlacementType, PlacementTypeWrapper> _enabledPlacementsTypeWrappers;

        public MonoBehaviour coroutineLauncher
        {
            get => _coroutineLauncher;
            set
            {
                _coroutineLauncher = value;
                var rewardVideoWrapper =
                    _enabledPlacementsTypeWrappers[PlacementType.RewardVideo] as RewardVideoWrapper;
                if (rewardVideoWrapper != null) rewardVideoWrapper.coroutineLauncher = value;
            }
        }

        public float rewardVideoLoadingTimeout
        {
            set
            {
                var rewardVideoWrapper =
                    _enabledPlacementsTypeWrappers[PlacementType.RewardVideo] as RewardVideoWrapper;
                if (rewardVideoWrapper != null) rewardVideoWrapper.loadingTimeout = value;
            }
        }

        private void Awake()
        {
            if (_instance == null)
            {
                _instance = this;
                InitializeAdProviders();
                PrepareWrappers();
                Initialize(PlacementsContainer.ApplicationPlacements);
                DontDestroyOnLoad(this);
            }
            else
            {
                Destroy(this);
            }
        }

        public event BannerDisplayStateDidChange BannerDisplayStateDidChangeEvent;

        public static MWMMoPubWrapper Instance()
        {
            if (_instance == null) throw new Exception("No MWMMoPubWrapper has been attached to a scene.");
            return _instance;
        }

        public static bool IsInstanciated()
        {
            return _instance != null;
        }

        private void InitializeAdProviders()
        {
        }

        private void PrepareWrappers()
        {
            _enabledPlacementsTypeWrappers = new Dictionary<PlacementType, PlacementTypeWrapper>();
            if (Configuration.Interstitials)
            {
                var value = new InterstitialWrapper();
                _enabledPlacementsTypeWrappers.Add(PlacementType.Interstitial, value);
            }

            if (Configuration.Banners)
            {
                var bannerWrapper = new BannerWrapper();
                _enabledPlacementsTypeWrappers.Add(PlacementType.Banner, bannerWrapper);
                bannerWrapper.BannerDisplayStateDidChangeEvent += OnBannerDisplayStateDidChange;
            }

            if (Configuration.RewardedVideos)
            {
                var rewardVideoWrapper = new RewardVideoWrapper();
                rewardVideoWrapper.coroutineLauncher = coroutineLauncher;
                _enabledPlacementsTypeWrappers.Add(PlacementType.RewardVideo, rewardVideoWrapper);
            }
        }

        private void Initialize(List<Placement> placements)
        {
            // _displayAdsEventWaiter = new MWMMoPubEventWaiter(this);
            // MoPubBase.SdkConfiguration sdkConfiguration = default(MoPubBase.SdkConfiguration);
            // sdkConfiguration.AdUnitId = placements[0].GetPlacementId();
            // sdkConfiguration.NetworksToInit = new MoPubBase.RewardedNetwork[6]
            // {
            // 	MoPubBase.RewardedNetwork.AdColony,
            // 	MoPubBase.RewardedNetwork.AdMob,
            // 	MoPubBase.RewardedNetwork.Chartboost,
            // 	MoPubBase.RewardedNetwork.Unity,
            // 	MoPubBase.RewardedNetwork.Facebook,
            // 	MoPubBase.RewardedNetwork.Vungle
            // };
            // MoPubBase.SdkConfiguration sdkConfiguration2 = sdkConfiguration;
            // MoPubAndroid.InitializeSdk(sdkConfiguration2);
            // foreach (PlacementTypeWrapper value in _enabledPlacementsTypeWrappers.Values)
            // {
            // 	value.Initialize(placements, PlacementsContainer, this);
            // }
            // RegisterToMoPubEvents();
            // foreach (Placement applicationPlacement in PlacementsContainer.ApplicationPlacements)
            // {
            // 	applicationPlacement.PrepareForNewLoading();
            // }
        }

        public bool ShouldShowConsentDialog()
        {
            return false; //MoPubAndroid.ShouldShowConsentDialog;
        }

        public void GrantConsentGdpr()
        {
            // MoPubAndroid.PartnerApi.GrantConsent();
        }

        public bool MoPubIsInitialized()
        {
            return false; //MoPubAndroid.IsSdkInitialized;
        }

        private void RegisterToMoPubEvents()
        {
            foreach (var value in _enabledPlacementsTypeWrappers.Values) value.RegisterToMoPubEvents();
        }

        public void LoadContent(PlacementType type, string placementName, Action<PlacementLoadingState> loadingResult,
            bool displayWhenReady = false)
        {
            CheckPlacementTypeAvailability(type);
            _enabledPlacementsTypeWrappers[type].LoadContent(placementName, loadingResult);
        }

        public void DisplayContent(PlacementType type, string placementName,
            Action<PlacementShowHideResult> displayCompletion,
            Action<PlacementInteractionState> closingCompletion = null)
        {
            CheckPlacementTypeAvailability(type);
            _enabledPlacementsTypeWrappers[type].DisplayContent(placementName, displayCompletion, closingCompletion);
        }

        public void HideBanner(string placementName)
        {
            CheckPlacementTypeAvailability(PlacementType.Banner);
            ((BannerWrapper) _enabledPlacementsTypeWrappers[PlacementType.Banner]).HideContent(placementName);
        }

        public void ReloadBanner(string placementName, Action<PlacementLoadingState> loadingResult)
        {
            CheckPlacementTypeAvailability(PlacementType.Banner);
            ((BannerWrapper) _enabledPlacementsTypeWrappers[PlacementType.Banner]).ReloadContent(placementName,
                loadingResult);
        }

        public void RegisterBannerDisplayStateEventCallback(BannerDisplayStateDidChange callback)
        {
            CheckPlacementTypeAvailability(PlacementType.Banner);
            BannerDisplayStateDidChangeEvent += callback;
            var bannerWrapper = (BannerWrapper) _enabledPlacementsTypeWrappers[PlacementType.Banner];
            callback(bannerWrapper.BannerDisplayState, bannerWrapper.BannerHeight);
        }

        public void UnRegisterBannerDisplayStateEventCallback(BannerDisplayStateDidChange callback)
        {
            BannerDisplayStateDidChangeEvent -= callback;
        }

        protected internal void OnAdDisplayNativeEventTriggered(PlacementType placementType, string adServiceName,
            string placement)
        {
            var ev = new AdEvent(adServiceName, GetLastDisplayedPlacementName(placementType), placement,
                placementType.EventStringValue());
            _displayAdsEventWaiter.HandleNewEventData(ev);
        }

        protected internal void OnAdRequestNativeEventTriggered(PlacementType placementType, string adServiceName,
            string placement)
        {
            var adEvent = new AdEvent(adServiceName, GetLastRequestedPlacementName(placementType), placement,
                placementType.EventStringValue());
            GameEventSender.Instance().SendEvent(EventType.AdRequested, adEvent.ToJSONString());
        }

        private void CheckPlacementTypeAvailability(PlacementType type)
        {
            switch (type)
            {
                case PlacementType.Interstitial:
                    if (!Configuration.Interstitials) throw new Exception("Interstitial placements were not enabled");
                    break;
                case PlacementType.RewardVideo:
                    if (!Configuration.RewardedVideos) throw new Exception("RewardVideo placements were not enabled");
                    break;
                case PlacementType.Banner:
                    if (!Configuration.Banners) throw new Exception("Banner placements were not enabled");
                    break;
            }
        }

        private void OnBannerDisplayStateDidChange(PlacementDisplayState newDisplayState, float bannerheight)
        {
            if (BannerDisplayStateDidChangeEvent != null)
                BannerDisplayStateDidChangeEvent(newDisplayState, bannerheight);
        }

        private string GetLastRequestedPlacementName(PlacementType type)
        {
            return _enabledPlacementsTypeWrappers[type].GetLastRequestedPlacementName();
        }

        private string GetLastDisplayedPlacementName(PlacementType type)
        {
            return _enabledPlacementsTypeWrappers[type].GetLastDisplayedPlacementName();
        }
    }
}