using UnityEngine;
using UnityEngine.UI;

namespace MWM.Ads
{
    public class BannerUIResizer : MonoBehaviour
    {
        private const float BannerHeight = 50f;

        private GameObject bannerBackground;

        private void Start()
        {
            bannerBackground = new GameObject();
            bannerBackground.transform.parent = transform;
            bannerBackground.AddComponent<Image>();
            bannerBackground.GetComponent<Image>().color = Color.black;
            bannerBackground.GetComponent<RectTransform>().anchoredPosition3D = Vector3.zero;
            bannerBackground.GetComponent<RectTransform>().anchorMin = Vector2.zero;
            bannerBackground.GetComponent<RectTransform>().anchorMax = new Vector2(1f, 0f);
            bannerBackground.GetComponent<RectTransform>().sizeDelta = Vector2.zero;
            bannerBackground.GetComponent<RectTransform>().localScale = Vector2.one;
            bannerBackground.GetComponent<RectTransform>().offsetMax = Vector2.zero;
            bannerBackground.GetComponent<RectTransform>().offsetMin = Vector2.zero;
            MWMMoPubWrapper.Instance().RegisterBannerDisplayStateEventCallback(OnBannerDisplayStateDidChange);
        }

        private void OnDestroy()
        {
            MWMMoPubWrapper.Instance().UnRegisterBannerDisplayStateEventCallback(OnBannerDisplayStateDidChange);
        }

        private void OnBannerDisplayStateDidChange(PlacementDisplayState newDisplayState, float bannerheight)
        {
            var num = 4;
            switch (newDisplayState)
            {
                case PlacementDisplayState.Displayed:
                    GetComponent<RectTransform>().offsetMin = new Vector2(0f, 50f * num);
                    bannerBackground.GetComponent<RectTransform>().offsetMin = new Vector2(0f, -50f * num);
                    break;
                case PlacementDisplayState.NotDisplayed:
                    GetComponent<RectTransform>().offsetMin = new Vector2(0f, 0f);
                    bannerBackground.GetComponent<RectTransform>().offsetMin = Vector2.zero;
                    break;
            }
        }
    }
}