using System;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.UI;

namespace MWM.Ads
{
    public class MWMConsentDialog : MonoBehaviour
    {
        public Button InternalPlayButton;

        public Toggle InternalToggleTos;

        public Toggle InternalToggleYears;

        public Toggle InternalToggleConsent;

        private MWMMoPubWrapper _mwmMoPubWrapper;

        private void Start()
        {
            InternalPlayButton.interactable = false;
        }

        public event Action OnAcceptedClickedEvent;

        public void Show(MWMMoPubWrapper mwmMoPubWrapper)
        {
            if (mwmMoPubWrapper == null) throw new Exception("mwmMoPubWrapper cannot be null");
            _mwmMoPubWrapper = mwmMoPubWrapper;
            gameObject.SetActive(true);
        }

        [UsedImplicitly]
        public void OnToggleValueChanged()
        {
            InternalPlayButton.interactable =
                InternalToggleTos.isOn && InternalToggleYears.isOn && InternalToggleConsent.isOn;
        }

        [UsedImplicitly]
        public void OnPrivacyPolicyClicked()
        {
            Application.OpenURL("http://google.com/");
        }

        [UsedImplicitly]
        public void OnValidateClicked()
        {
            _mwmMoPubWrapper.GrantConsentGdpr();
            gameObject.SetActive(false);
            if (OnAcceptedClickedEvent != null) OnAcceptedClickedEvent();
        }
    }
}