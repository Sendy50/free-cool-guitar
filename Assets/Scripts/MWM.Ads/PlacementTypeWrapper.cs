using System;
using System.Collections.Generic;

namespace MWM.Ads
{
    public abstract class PlacementTypeWrapper
    {
        protected MWMMoPubApplicationPlacementContainer _placementsContainer;

        public abstract void Initialize(List<Placement> placements,
            MWMMoPubApplicationPlacementContainer placementsContainer, MWMMoPubWrapper parrentWrapper);

        public abstract void RegisterToMoPubEvents();

        public abstract void LoadContent(string placementName, Action<PlacementLoadingState> loadingResult);

        public abstract void DisplayContent(string placementName, Action<PlacementShowHideResult> displayCompletion,
            Action<PlacementInteractionState> closingCompletion = null);

        public abstract string GetLastRequestedPlacementName();

        public abstract string GetLastDisplayedPlacementName();
    }
}