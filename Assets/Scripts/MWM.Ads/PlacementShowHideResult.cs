namespace MWM.Ads
{
    public enum PlacementShowHideResult
    {
        ShowHideFailed,
        ShowHideSucceeded
    }
}