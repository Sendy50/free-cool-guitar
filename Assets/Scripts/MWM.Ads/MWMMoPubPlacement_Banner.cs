using UnityEngine;

namespace MWM.Ads
{
    [CreateAssetMenu(menuName = "MWM/MoPub/Banner Placement (only one per project for now)")]
    public class MWMMoPubPlacement_Banner : Placement
    {
        public override PlacementType GetPlacementType()
        {
            return PlacementType.Banner;
        }
    }
}