namespace MWM.Ads
{
    public enum PlacementType
    {
        Interstitial,
        RewardVideo,
        Banner
    }
}