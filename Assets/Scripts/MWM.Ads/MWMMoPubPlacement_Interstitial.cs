using UnityEngine;

namespace MWM.Ads
{
    [CreateAssetMenu(menuName = "MWM/MoPub/Interstitial Placement")]
    public class MWMMoPubPlacement_Interstitial : Placement
    {
        public override PlacementType GetPlacementType()
        {
            return PlacementType.Interstitial;
        }
    }
}