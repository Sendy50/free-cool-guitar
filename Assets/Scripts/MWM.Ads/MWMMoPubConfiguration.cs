using MWM.Core.Settings;
using UnityEngine;

namespace MWM.Ads
{
    public class MWMMoPubConfiguration : ScriptableObject
    {
        public CoreSettings InternalCoreSettings;

        public bool Interstitials;

        public bool Banners;

        public bool RewardedVideos;

        public string PrototypeAppLovinSdkKey =
            "TQKorH6tLTl4mT38_g3Abkk6FvyCMbXv1tht8okUGTT11b90HCBtxwzBpvN9Vw3XnmOvT6OqaxrbX_cqCpg7id";

        public string MWMAppLovinSdkKey =
            "TQKorH6tLTl4mT38_g3Abkk6FvyCMbXv1tht8okUGTT11b90HCBtxwzBpvN9Vw3XnmOvT6OqaxrbX_cqCpg7id";

        public string AppLovinSdkKey => InternalCoreSettings.CurrentBuildTarget != BuildTarget.Prototype
            ? MWMAppLovinSdkKey
            : PrototypeAppLovinSdkKey;
    }
}