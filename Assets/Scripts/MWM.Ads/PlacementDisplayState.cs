namespace MWM.Ads
{
    public enum PlacementDisplayState
    {
        NotDisplayed,
        Displayed
    }
}