namespace MWM.Ads
{
    public delegate void BannerDisplayStateDidChange(PlacementDisplayState newDisplayState, float bannerHeight);
}