using System;

namespace MWM.Ads
{
    public static class PlacementTypeExtensions
    {
        public static string EventStringValue(this PlacementType type)
        {
            switch (type)
            {
                case PlacementType.Banner:
                    return "BANNER";
                case PlacementType.Interstitial:
                    return "INTERSTITIAL";
                case PlacementType.RewardVideo:
                    return "REWARD";
                default:
                    throw new Exception("Unknown PlacementType");
            }
        }
    }
}