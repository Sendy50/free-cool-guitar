using UnityEngine;

namespace MWM.Ads
{
    [CreateAssetMenu(menuName = "MWM/MoPub/Reward Video Placement")]
    public class MWMMoPubPlacement_RewardVideo : Placement
    {
        public override PlacementType GetPlacementType()
        {
            return PlacementType.RewardVideo;
        }
    }
}