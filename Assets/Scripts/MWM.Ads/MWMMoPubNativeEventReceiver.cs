using System.Linq;
using UnityEngine;

namespace MWM.Ads
{
    public class MWMMoPubNativeEventReceiver : MonoBehaviour
    {
        private static readonly char[] ParameterSeparator = new char[3]
        {
            '#',
            '*',
            '#'
        };

        public MWMMoPubWrapper InternalMopubWrapper;

        public void lowLevelInterstitialDisplayed(string parameters)
        {
            var eventData = StringParametersToData(parameters);
            InternalMopubWrapper.OnAdDisplayNativeEventTriggered(PlacementType.Interstitial, eventData.adServiceName,
                eventData.placement);
        }

        public void lowLevelBannerDisplayed(string parameters)
        {
            var eventData = StringParametersToData(parameters);
            InternalMopubWrapper.OnAdDisplayNativeEventTriggered(PlacementType.Banner, eventData.adServiceName,
                eventData.placement);
        }

        public void lowLevelRewardVideoDisplayed(string parameters)
        {
            var eventData = StringParametersToData(parameters);
            InternalMopubWrapper.OnAdDisplayNativeEventTriggered(PlacementType.RewardVideo, eventData.adServiceName,
                eventData.placement);
        }

        public void lowLevelInterstitialRequested(string parameters)
        {
            var eventData = StringParametersToData(parameters);
            InternalMopubWrapper.OnAdRequestNativeEventTriggered(PlacementType.Interstitial, eventData.adServiceName,
                eventData.placement);
        }

        public void lowLevelBannerRequested(string parameters)
        {
            var eventData = StringParametersToData(parameters);
            InternalMopubWrapper.OnAdRequestNativeEventTriggered(PlacementType.Banner, eventData.adServiceName,
                eventData.placement);
        }

        public void lowLevelRewardVideoDRequested(string parameters)
        {
            var eventData = StringParametersToData(parameters);
            InternalMopubWrapper.OnAdRequestNativeEventTriggered(PlacementType.RewardVideo, eventData.adServiceName,
                eventData.placement);
        }

        private static EventData StringParametersToData(string stringParameters)
        {
            var array = stringParameters.Split(ParameterSeparator);
            var num = array.Count();
            if (num == 0 || num == 1) return new EventData(stringParameters, string.Empty);
            var adServiceName = array.First();
            var text = array[num - 1];
            if (string.IsNullOrEmpty(text)) return new EventData(adServiceName, string.Empty);
            return new EventData(adServiceName, text);
        }

        private struct EventData
        {
            public readonly string adServiceName;

            public readonly string placement;

            public EventData(string adServiceName, string placement)
            {
                this.adServiceName = adServiceName;
                this.placement = placement;
            }
        }
    }
}