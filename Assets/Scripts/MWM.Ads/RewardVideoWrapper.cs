using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MWM.Ads
{
    public class RewardVideoWrapper : PlacementTypeWrapper
    {
        private string _lastDisplayedPlacementName;
        private string _lastRequestedPlacementName;

        private MWMMoPubWrapper _parentWrapper;

        public MonoBehaviour coroutineLauncher;

        public float loadingTimeout = 10f;

        public override void Initialize(List<Placement> placements,
            MWMMoPubApplicationPlacementContainer placementsContainer, MWMMoPubWrapper parentWrapper)
        {
            _placementsContainer = placementsContainer;
            _parentWrapper = parentWrapper;
            var placementIdsForTypeFromList =
                MWMMoPubApplicationPlacementContainer.GetPlacementIdsForTypeFromList(PlacementType.RewardVideo,
                    placements);
            // MoPubAndroid.LoadRewardedVideoPluginsForAdUnits(placementIdsForTypeFromList);
            _lastRequestedPlacementName = null;
            _lastDisplayedPlacementName = null;
        }

        public override void RegisterToMoPubEvents()
        {
            // MoPubManager.OnRewardedVideoExpiredEvent += delegate(string id)
            // {
            // 	Placement placementWithId7 = _placementsContainer.GetPlacementWithId(id);
            // 	if (placementWithId7 == null)
            // 	{
            // 		throw new Exception("[MoPubWrapper][RewardVideo] placement not found");
            // 	}
            // 	placementWithId7.LoadingState = PlacementLoadingState.Expired;
            // };
            // MoPubManager.OnRewardedVideoFailedEvent += delegate(string adUnitId, string errorMessage)
            // {
            // 	Placement placementWithId6 = _placementsContainer.GetPlacementWithId(adUnitId);
            // 	if (placementWithId6 == null)
            // 	{
            // 		throw new Exception("[MoPubWrapper][RewardVideo] placement not found");
            // 	}
            // 	placementWithId6.LoadingState = PlacementLoadingState.Failure;
            // 	placementWithId6.NotifyLoadingCompleted();
            // 	placementWithId6.PrepareForNewLoading();
            // };
            // MoPubManager.OnRewardedVideoLoadedEvent += delegate(string id)
            // {
            // 	Placement placementWithId5 = _placementsContainer.GetPlacementWithId(id);
            // 	if (placementWithId5 == null)
            // 	{
            // 		throw new Exception("[MoPubWrapper][RewardVideo] placement not found");
            // 	}
            // 	placementWithId5.LoadingState = PlacementLoadingState.Loaded;
            // 	placementWithId5.NotifyLoadingCompleted();
            // };
            // MoPubManager.OnRewardedVideoFailedToPlayEvent += delegate(string adUnitId, string errorMessage)
            // {
            // 	Placement placementWithId4 = _placementsContainer.GetPlacementWithId(adUnitId);
            // 	if (placementWithId4 == null)
            // 	{
            // 		throw new Exception("[MoPubWrapper][RewardVideo] placement not found");
            // 	}
            // };
            // MoPubManager.OnRewardedVideoReceivedRewardEvent += delegate(string adUnitId, string label, float amount)
            // {
            // 	Placement placementWithId3 = _placementsContainer.GetPlacementWithId(adUnitId);
            // 	if (placementWithId3 == null)
            // 	{
            // 		throw new Exception("[MoPubWrapper][RewardVideo] placement not found");
            // 	}
            // 	placementWithId3.InteractionState = PlacementInteractionState.Rewarded;
            // };
            // MoPubManager.OnRewardedVideoShownEvent += delegate(string id)
            // {
            // 	Placement placementWithId2 = _placementsContainer.GetPlacementWithId(id);
            // 	if (placementWithId2 == null)
            // 	{
            // 		throw new Exception("[MoPubWrapper][RewardVideo] placement not found");
            // 	}
            // 	placementWithId2.DisplayState = PlacementDisplayState.Displayed;
            // 	placementWithId2.NotifyDisplayStateDidChange();
            // };
            // MoPubManager.OnRewardedVideoClosedEvent += delegate(string id)
            // {
            // 	Placement placementWithId = _placementsContainer.GetPlacementWithId(id);
            // 	if (placementWithId == null)
            // 	{
            // 		throw new Exception("[MoPubWrapper][RewardVideo] placement not found");
            // 	}
            // 	placementWithId.DisplayState = PlacementDisplayState.NotDisplayed;
            // 	placementWithId.NotifyDisplayStateDidChange();
            // 	placementWithId.PrepareForNewDisplay();
            // };
        }

        public override void LoadContent(string placementName, Action<PlacementLoadingState> loadingResult)
        {
            var placementWithName = _placementsContainer.GetPlacementWithName(placementName);
            if (placementWithName == null)
            {
                loadingResult(PlacementLoadingState.Failure);
                return;
            }

            if (placementWithName.LoadingState == PlacementLoadingState.Loading)
            {
                loadingResult(PlacementLoadingState.Loading);
                return;
            }

            if (placementWithName.LoadingState == PlacementLoadingState.Loaded)
            {
                loadingResult(PlacementLoadingState.Loaded);
                return;
            }

            placementWithName.LoadingState = PlacementLoadingState.Loading;
            placementWithName.SetLoadingCompletionAction(loadingResult);
            _lastRequestedPlacementName = placementName;
            _parentWrapper.OnAdRequestNativeEventTriggered(PlacementType.RewardVideo, "mopub",
                placementWithName.GetPlacementId());
            // MoPubAndroid.RequestRewardedVideo(placementWithName.GetPlacementId());
            coroutineLauncher.StartCoroutine(loadingTimeoutRoutine(placementWithName));
        }

        private IEnumerator loadingTimeoutRoutine(Placement placement)
        {
            var timer = 0f;
            var tick = 0.5f;
            do
            {
                yield return new WaitForSeconds(tick);
                timer += tick;
                if (placement.LoadingState == PlacementLoadingState.Loading && timer > loadingTimeout)
                {
                    placement.LoadingState = PlacementLoadingState.Failure;
                    placement.NotifyLoadingCompleted();
                    placement.PrepareForNewLoading();
                    break;
                }
            } while (placement.LoadingState == PlacementLoadingState.Loading);
        }

        public override void DisplayContent(string placementName, Action<PlacementShowHideResult> displayCompletion,
            Action<PlacementInteractionState> closingCompletion = null)
        {
            var placementWithName = _placementsContainer.GetPlacementWithName(placementName);
            if (placementWithName == null)
            {
                displayCompletion(PlacementShowHideResult.ShowHideFailed);
            }
            else if (placementWithName.LoadingState == PlacementLoadingState.Loaded)
            {
                placementWithName.SetDisplayCompletionAction(displayCompletion);
                placementWithName.SetClosingCompletionAction(closingCompletion);
                _lastDisplayedPlacementName = placementName;
                _parentWrapper.OnAdDisplayNativeEventTriggered(PlacementType.RewardVideo, "mopub",
                    placementWithName.GetPlacementId());
                // MoPubAndroid.ShowRewardedVideo(placementWithName.GetPlacementId());
                placementWithName.PrepareForNewLoading();
            }
            else
            {
                displayCompletion(PlacementShowHideResult.ShowHideFailed);
            }
        }

        public override string GetLastRequestedPlacementName()
        {
            return _lastRequestedPlacementName;
        }

        public override string GetLastDisplayedPlacementName()
        {
            return _lastDisplayedPlacementName;
        }
    }
}