using UnityEngine;

namespace Utils
{
    public class Device : IDeviceInfoProvider
    {
        private static IDeviceInfoProvider _defaultProvider;

        public string GetCountryCode()
        {
            return CountryCode();
        }

        public string GetDeviceModel()
        {
            return SystemInfo.deviceModel;
        }

        public bool GetIsIpad()
        {
            return IsIpad();
        }

        public DevicePlatform GetPlatformType()
        {
            return DevicePlatform.Android;
        }

        public DevicePlatform GetTargetType()
        {
            return DevicePlatform.Android;
        }

        public string GetDevicePlatformName()
        {
            switch (GetPlatformType())
            {
                case DevicePlatform.Editor:
                    return "editor";
                case DevicePlatform.Android:
                    return "android";
                case DevicePlatform.Ios:
                    return "ios";
                default:
                    return "notsupported";
            }
        }

        // [DllImport("__Internal", CallingConvention = CallingConvention.Cdecl)]
        // private static extern IntPtr mwmGetCountryCode();
        //
        // [DllImport("__Internal", CallingConvention = CallingConvention.Cdecl)]
        // private static extern int mwmFreeMem(IntPtr ptr);

        public static IDeviceInfoProvider Default()
        {
            if (_defaultProvider == null) _defaultProvider = new Device();
            return _defaultProvider;
        }

        public static bool IsIpad()
        {
            return SystemInfo.deviceModel.Contains("iPad");
        }

        private static string CountryCode()
        {
            var androidJavaClass = new AndroidJavaClass("java.util.Locale");
            var androidJavaObject = androidJavaClass.CallStatic<AndroidJavaObject>("getDefault");
            return androidJavaObject != null ? androidJavaObject.Call<string>("getCountry").ToUpper() : string.Empty;
        }
    }
}