using System;
using System.Collections.Generic;

namespace Utils
{
    public static class Extension
    {
        public static void Populate<T>(this T[] array, Func<int, T> populateFunc)
        {
            for (var i = 0; i < array.Length; i++)
            {
                array[i] = populateFunc(i);
            }
        }

        public static void Fill<T>(this T[] array, T value)
        {
            for (var i = 0; i < array.Length; i++)
            {
                array[i] = value;
            }
        }

        public static List<int> IndexesWhere<T>(this IEnumerable<T> enumerable, Predicate<T> predicate)
        {
            var list = new List<int>();
            var num = 0;
            foreach (var item in enumerable)
            {
                if (predicate(item))
                {
                    list.Add(num);
                }

                num++;
            }

            return list;
        }
    }
}