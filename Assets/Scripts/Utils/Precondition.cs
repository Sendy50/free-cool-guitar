using System;

namespace Utils
{
    public static class Precondition
    {
        public static void CheckNotNull(object o)
        {
            if (o == null) throw new Exception("Object is null");
        }
    }
}