using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Utils
{
    public class ButtonApplyColorExtension : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler,
        IPointerDownHandler, IPointerUpHandler, IEventSystemHandler
    {
        [SerializeField] private Image _targetImage;

        [SerializeField] private Color _normalColor;

        [SerializeField] private Color _highlightedColor;

        [SerializeField] private Color _pressedColor;

        public void OnPointerDown(PointerEventData eventData)
        {
            _targetImage.color = _pressedColor;
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            _targetImage.color = _highlightedColor;
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            _targetImage.color = _normalColor;
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            _targetImage.color = _normalColor;
        }
    }
}