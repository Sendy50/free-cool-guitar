using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace Utils
{
    public class FpsCounter : MonoBehaviour
    {
        [SerializeField] private Text _fpsText;

        [SerializeField] private float _updateFrequencyInSeconds;

        private void Awake()
        {
            StartCoroutine(FpsCoroutine());
        }

        private IEnumerator FpsCoroutine()
        {
            var waitForSeconds = new WaitForSeconds(_updateFrequencyInSeconds);
            while (true)
            {
                var lastFrameCount = Time.frameCount;
                var lastTime = Time.realtimeSinceStartup;
                yield return waitForSeconds;
                var timeSpan = Time.realtimeSinceStartup - lastTime;
                var frameCount = Time.frameCount - lastFrameCount;
                var framePerSec = Mathf.RoundToInt(frameCount / timeSpan);
                _fpsText.text = framePerSec.ToString();
            }
        }
    }
}