namespace Utils
{
    public interface IDeviceInfoProvider
    {
        string GetCountryCode();

        string GetDeviceModel();

        DevicePlatform GetPlatformType();

        DevicePlatform GetTargetType();

        string GetDevicePlatformName();

        bool GetIsIpad();
    }
}