namespace Utils
{
    public enum DevicePlatform
    {
        Android,
        Ios,
        Editor,
        NotSupported
    }
}