using UnityEngine;

namespace MWM.Audio.Internal.iOS
{
    public class IOSAudioEngineEventsReciver : MonoBehaviour
    {
        private IOSMultiPlayer _multiPlayer;

        private IOSSamplerUnit _sampler;

        private IOSSamplingSynth _samplingSynth;

        public void SetMultiPlayer(IOSMultiPlayer multiPlayer)
        {
            _multiPlayer = multiPlayer;
        }

        public void SetSampler(IOSSamplerUnit sampler)
        {
            _sampler = sampler;
        }

        public void SetSamplingSynth(IOSSamplingSynth samplingSynth)
        {
            _samplingSynth = samplingSynth;
        }

        public void OnMultiPlayerLoadExternalFileSuccess(string message)
        {
            var playerIndex = int.Parse(message);
            _multiPlayer.NotifiyLoadExternalCompleted(playerIndex, LoadingResult.Ok());
        }

        public void OnMultiPlayerLoadInternalFileSuccess(string message)
        {
            var playerIndex = int.Parse(message);
            _multiPlayer.NotifiyLoadInternalCompleted(playerIndex, LoadingResult.Ok());
        }

        public void OnMultiPlayerLoadExternalFileFailed(string message)
        {
            var playerIndex = int.Parse(message);
            _multiPlayer.NotifiyLoadExternalCompleted(playerIndex,
                LoadingResult.AudioFormatNotSupported("An error occurred"));
        }

        public void OnMultiPlayerLoadInternalFileFailded(string message)
        {
            var playerIndex = int.Parse(message);
            _multiPlayer.NotifiyLoadInternalCompleted(playerIndex,
                LoadingResult.AudioFormatNotSupported("An error occurred"));
        }

        public void OnMultiPlayerPlayingOn(string message)
        {
            var playerIndex = int.Parse(message);
            _multiPlayer.NotifyPlayingStateChanged(playerIndex, true);
        }

        public void OnMultiPlayerPlayingOff(string message)
        {
            var playerIndex = int.Parse(message);
            _multiPlayer.NotifyPlayingStateChanged(playerIndex, false);
        }

        public void OnMultiPlayerReachedEndOfFile(string message)
        {
            var playerIndex = int.Parse(message);
            _multiPlayer.NotifyPlayerReachedEndOfFile(playerIndex);
        }

        public void OnSamplerLoadSuccessEvent(string message)
        {
            var sampleIndex = int.Parse(message);
            _sampler.NotifyLoadCompleted(sampleIndex, LoadingResult.Ok());
        }

        public void OnSamplerLoadFailedEvent(string message)
        {
            var sampleIndex = int.Parse(message);
            _sampler.NotifyLoadCompleted(sampleIndex, LoadingResult.AudioFormatNotSupported(string.Empty));
        }

        public void OnSamplerUnloadSuccessEvent(string message)
        {
            var sampleIndex = int.Parse(message);
            _sampler.NotifyUnloadCompleted(sampleIndex, true);
        }

        public void OnSamplerUnloadFailedEvent(string message)
        {
            var sampleIndex = int.Parse(message);
            _sampler.NotifyUnloadCompleted(sampleIndex, false);
        }

        public void OnSamplerStartedtEvent(string message)
        {
            var sampleIndex = int.Parse(message);
            _sampler.NotifySamplerStarted(sampleIndex);
        }

        public void OnSamplerStoppedEvent(string message)
        {
            var sampleIndex = int.Parse(message);
            _sampler.NotifySamplerStopped(sampleIndex);
        }

        public void OnSamplingSynthInstrumentExternalLoadingSucceeded()
        {
            _samplingSynth.OnExternalLoadingInstrumentSucceeded(LoadingResult.Ok());
        }

        public void OnSamplingSynthInstrumentExternalLoadingFailed()
        {
            _samplingSynth.OnExternalLoadingInstrumentFailed(LoadingResult.InstrumentLoadingFailure(string.Empty));
        }

        public void OnSamplingSynthInstrumentInternalLoadingSucceeded()
        {
            _samplingSynth.OnInternalLoadingInstrumentSucceeded(LoadingResult.Ok());
        }

        public void OnSamplingSynthInstrumentInternalLoadingFailed()
        {
            _samplingSynth.OnInternalLoadingInstrumentFailed(LoadingResult.InstrumentLoadingFailure(string.Empty));
        }
    }
}