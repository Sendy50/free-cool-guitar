using System;
using UnityEngine;

namespace MWM.Audio.Internal.iOS
{
    public class IOSEngineFactory
    {
        // [DllImport("__Internal")]
        // private static extern void initAudioEngineForApp(int nbActivatedSampler, bool activateSamplingSynth, bool activateMultiPlayer);
        //
        // [DllImport("__Internal")]
        // private static extern void initAudioEngineForGame(int nbActivatedSampler, bool activateSamplingSynth, bool activateMultiPlayer);

        public static void CreateForApp(int nbPlayers, int nbSamples, int preferredNbTracks,
            MonoBehaviour monoBehaviour, Action<MusicGamingAudioEngine> completion)
        {
            var gameObject = new GameObject("IOSAudioEngineEventsReceiver");
            gameObject.AddComponent<IOSAudioEngineEventsReciver>();
            gameObject.transform.parent = monoBehaviour.gameObject.transform;
            // initAudioEngineForApp(nbSamples, preferredNbTracks > 0, nbPlayers > 0);
            var iOSMultiPlayer = new IOSMultiPlayer(nbPlayers);
            var iOSSamplerUnit = new IOSSamplerUnit(nbSamples);
            var iOSSamplingSynth = new IOSSamplingSynth(preferredNbTracks);
            var component = gameObject.GetComponent<IOSAudioEngineEventsReciver>();
            component.SetMultiPlayer(iOSMultiPlayer);
            component.SetSampler(iOSSamplerUnit);
            component.SetSamplingSynth(iOSSamplingSynth);
            var obj = new IOSMusicGamingAudioEngine(iOSMultiPlayer, iOSSamplerUnit, iOSSamplingSynth);
            completion(obj);
        }

        public static void CreateForGame(int nbPlayers, int nbSamples, int preferredNbTracks,
            MonoBehaviour monoBehaviour, Action<MusicGamingAudioEngine> completion)
        {
            var gameObject = new GameObject("IOSAudioEngineEventsReceiver");
            gameObject.AddComponent<IOSAudioEngineEventsReciver>();
            gameObject.transform.parent = monoBehaviour.gameObject.transform;
            // initAudioEngineForGame(nbSamples, preferredNbTracks > 0, nbPlayers > 0);
            var iOSMultiPlayer = new IOSMultiPlayer(nbPlayers);
            var iOSSamplerUnit = new IOSSamplerUnit(nbSamples);
            var iOSSamplingSynth = new IOSSamplingSynth(preferredNbTracks);
            var component = gameObject.GetComponent<IOSAudioEngineEventsReciver>();
            component.SetMultiPlayer(iOSMultiPlayer);
            component.SetSampler(iOSSamplerUnit);
            component.SetSamplingSynth(iOSSamplingSynth);
            var obj = new IOSMusicGamingAudioEngine(iOSMultiPlayer, iOSSamplerUnit, iOSSamplingSynth);
            completion(obj);
        }
    }
}