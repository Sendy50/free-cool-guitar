using System;

namespace MWM.Audio.Internal.iOS
{
    public class IOSSamplingSynth : SamplingSynthUnit
    {
        private int _nbTracks;
        private Action<LoadingResult> _pendingLoadExternalCompletion;

        private Action<LoadingResult> _pendingLoadInternalCompletion;

        public IOSSamplingSynth(int preferredNbTracks)
        {
            _nbTracks = preferredNbTracks;
        }

        // [DllImport("__Internal")]
        // private static extern void sampling_synth_loadExternal(string diskDirectoryPath, int nbTracks);
        //
        // [DllImport("__Internal")]
        // private static extern void sampling_synth_loadInternal(string resourceDirectoryPath, int nbTracks);
        //
        // [DllImport("__Internal")]
        // private static extern void sampling_synth_noteOn(int trackIndex, int noteNumber, int velocity);
        //
        // [DllImport("__Internal")]
        // private static extern void sampling_synth_noteOff(int trackIndex, int noteNumber);
        //
        // [DllImport("__Internal")]
        // private static extern void sampling_synth_pedalOn(int trackIndex);
        //
        // [DllImport("__Internal")]
        // private static extern void sampling_synth_pedalOff(int trackIndex);
        //
        // [DllImport("__Internal")]
        // private static extern void sampling_synth_stopAllNotes(bool clearAudio);
        //
        // [DllImport("__Internal")]
        // private static extern void sampling_synth_setNumberTracks(int nbTracks);

        public void LoadExternal(string diskDirectoryPath, Action<LoadingResult> completion)
        {
            _pendingLoadExternalCompletion = completion;
            // sampling_synth_loadExternal(diskDirectoryPath, _nbTracks);
        }

        public void LoadInternal(string resourceDirectoryPath, Action<LoadingResult> completion)
        {
            _pendingLoadInternalCompletion = completion;
            // sampling_synth_loadInternal(resourceDirectoryPath, _nbTracks);
        }

        public void NoteOn(int trackIndex, int noteNumber, int velocity)
        {
            // sampling_synth_noteOn(trackIndex, noteNumber, velocity);
        }

        public void NoteOff(int trackIndex, int noteNumber)
        {
            // sampling_synth_noteOff(trackIndex, noteNumber);
        }

        public void PedalOn(int trackIndex)
        {
            // sampling_synth_pedalOn(trackIndex);
        }

        public void PedalOff(int trackIndex)
        {
            // sampling_synth_pedalOff(trackIndex);
        }

        public void StopAllNotes(bool clearAudio)
        {
            // sampling_synth_stopAllNotes(clearAudio);
        }

        public void SetNumberTracks(int nbTracks)
        {
            _nbTracks = nbTracks;
            // sampling_synth_setNumberTracks(nbTracks);
        }

        public void OnInternalLoadingInstrumentSucceeded(LoadingResult result)
        {
            if (_pendingLoadInternalCompletion != null)
            {
                _pendingLoadInternalCompletion(result);
                _pendingLoadInternalCompletion = null;
            }
        }

        public void OnInternalLoadingInstrumentFailed(LoadingResult result)
        {
            if (_pendingLoadInternalCompletion != null)
            {
                _pendingLoadInternalCompletion(result);
                _pendingLoadInternalCompletion = null;
            }
        }

        public void OnExternalLoadingInstrumentSucceeded(LoadingResult result)
        {
            if (_pendingLoadExternalCompletion != null)
            {
                _pendingLoadExternalCompletion(result);
                _pendingLoadExternalCompletion = null;
            }
        }

        public void OnExternalLoadingInstrumentFailed(LoadingResult result)
        {
            if (_pendingLoadExternalCompletion != null)
            {
                _pendingLoadExternalCompletion(result);
                _pendingLoadExternalCompletion = null;
            }
        }
    }
}