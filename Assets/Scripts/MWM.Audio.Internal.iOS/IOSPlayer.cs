using System;

namespace MWM.Audio.Internal.iOS
{
    public class IOSPlayer : Player
    {
        private Action<LoadingResult> pendingLoadExternalCompletion;

        private Action<LoadingResult> pendingLoadInternalCompletion;

        public IOSPlayer(int index)
        {
            this.index = index;
        }

        public int index { get; }

        public event Action<Player, bool> PlayingStateChanged;

        public event Action<Player> EndOfFileReached;

        // [DllImport("__Internal")]
        // private static extern void multiPlayer_loadInternalFile(int playerIndex, string cRelativeFilePath);
        //
        // [DllImport("__Internal")]
        // private static extern void multiPlayer_loadExternalFile(int playerIndex, string cAbsoluteFilePath);
        //
        // [DllImport("__Internal")]
        // private static extern void multiPlayer_play(int playerIndex);
        //
        // [DllImport("__Internal")]
        // private static extern void multiPlayer_pause(int playerIndex);
        //
        // [DllImport("__Internal")]
        // private static extern void multiPlayer_stop(int playerIndex);
        //
        // [DllImport("__Internal")]
        // private static extern bool multiPlayer_isPlaying(int playerIndex);
        //
        // [DllImport("__Internal")]
        // private static extern float multiPlayer_getTotalTime(int playerIndex);
        //
        // [DllImport("__Internal")]
        // private static extern float multiPlayer_getCurrentTime(int playerIndex);
        //
        // [DllImport("__Internal")]
        // private static extern void multiPlayer_seek(int playerIndex, float time);
        //
        // [DllImport("__Internal")]
        // private static extern void multiPlayer_setVolume(int playerIndex, float newVolume);
        //
        // [DllImport("__Internal")]
        // private static extern float multiPlayer_getVolume(int playerIndex);
        //
        // [DllImport("__Internal")]
        // private static extern void multiPlayer_setSpeed(int playerIndex, float newSpeed);
        //
        // [DllImport("__Internal")]
        // private static extern float multiPlayer_getSpeed(int playerIndex);
        //
        // [DllImport("__Internal")]
        // private static extern void multiPlayer_setReapeat(int playerIndex, bool enable);
        //
        // [DllImport("__Internal")]
        // private static extern bool multiPlayer_getIsReapeat(int playerIndex);

        public void LoadExternal(string diskPath, Action<LoadingResult> completion)
        {
            pendingLoadExternalCompletion = completion;
            // multiPlayer_loadExternalFile(index, diskPath);
        }

        public void LoadInternal(string resourceName, Action<LoadingResult> completion)
        {
            pendingLoadInternalCompletion = completion;
            // multiPlayer_loadInternalFile(index, resourceName + ".mp3");
        }

        public void Play()
        {
            // multiPlayer_play(index);
        }

        public void Pause()
        {
            // multiPlayer_pause(index);
        }

        public void Stop()
        {
            // multiPlayer_stop(index);
        }

        public bool IsPlaying()
        {
            return false; //multiPlayer_isPlaying(index);
        }

        public float GetTotalTime()
        {
            return 0; //multiPlayer_getTotalTime(index);
        }

        public float GetCurrentTime()
        {
            return 0; //multiPlayer_getCurrentTime(index);
        }

        public void Seek(float time)
        {
            // multiPlayer_seek(index, time);
        }

        public void SetVolume(float newVolume)
        {
            // multiPlayer_setVolume(index, newVolume);
        }

        public float GetVolume()
        {
            return 0; //multiPlayer_getVolume(index);
        }

        public void SetSpeed(float newSpeed)
        {
            // multiPlayer_setSpeed(index, newSpeed);
        }

        public float GetSpeed()
        {
            return 0; //multiPlayer_getSpeed(index);
        }

        public void SetRepeat(bool enable)
        {
            // multiPlayer_setReapeat(index, enable);
        }

        public bool IsRepeat()
        {
            return false; //multiPlayer_getIsReapeat(index);
        }

        public void NotifiyLoadExternalCompleted(LoadingResult result)
        {
            if (pendingLoadExternalCompletion != null)
            {
                pendingLoadExternalCompletion(result);
                pendingLoadExternalCompletion = null;
            }
        }

        public void NotifiyLoadInternalCompleted(LoadingResult result)
        {
            if (pendingLoadInternalCompletion != null)
            {
                pendingLoadInternalCompletion(result);
                pendingLoadInternalCompletion = null;
            }
        }

        public void NotifyPlayingStateChanged(bool isPlaying)
        {
            if (PlayingStateChanged != null) PlayingStateChanged(this, isPlaying);
        }

        public void NotifyPlayerReachedEndOfFile()
        {
            if (EndOfFileReached != null) EndOfFileReached(this);
        }
    }
}