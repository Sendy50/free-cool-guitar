using System;

namespace MWM.Audio.Internal.iOS
{
    public class IOSMultiPlayer : MultiPlayerUnit
    {
        private readonly IOSPlayer[] _players;

        public IOSMultiPlayer(int nbPlayers)
        {
            _players = new IOSPlayer[nbPlayers];
            for (var i = 0; i < nbPlayers; i++)
            {
                // multiPlayer_allocPlayer(i);
                var iOSPlayer = new IOSPlayer(i);
                _players[i] = iOSPlayer;
            }

            // multiPlayer_startMutliPlayerObservation();
        }

        // [DllImport("__Internal")]
        // private static extern void multiPlayer_allocPlayer(int playerId);
        //
        // [DllImport("__Internal")]
        // private static extern void multiPlayer_startMutliPlayerObservation();

        public Player GetPlayer(int index)
        {
            return GetIOSPlayer(index);
        }

        private IOSPlayer GetIOSPlayer(int index)
        {
            if (index < 0 || index >= _players.Length)
                throw new ApplicationException("Index must be in range [0;" + _players.Length + "] Found: " + index);
            return _players[index];
        }

        public void NotifiyLoadExternalCompleted(int playerIndex, LoadingResult result)
        {
            GetIOSPlayer(playerIndex).NotifiyLoadExternalCompleted(result);
        }

        public void NotifiyLoadInternalCompleted(int playerIndex, LoadingResult result)
        {
            GetIOSPlayer(playerIndex).NotifiyLoadInternalCompleted(result);
        }

        public void NotifyPlayingStateChanged(int playerIndex, bool isPlaying)
        {
            GetIOSPlayer(playerIndex).NotifyPlayingStateChanged(isPlaying);
        }

        public void NotifyPlayerReachedEndOfFile(int playerIndex)
        {
            GetIOSPlayer(playerIndex).NotifyPlayerReachedEndOfFile();
        }
    }
}