using System;
using System.Collections.Generic;
using System.Linq;

namespace MWM.Audio.Internal.iOS
{
    public class IOSSamplerUnit : SamplerUnit
    {
        private readonly Dictionary<string, int> _idToNativeIndexMap;

        private readonly Stack<SampleTask> _loadingStack;

        private readonly int _maxSamples;

        private Action<LoadingResult> _pendingLoadCompletion;

        private readonly Dictionary<string, Sample> _samplesMap;

        private readonly Stack<SampleTask> _unloadingStack;

        public IOSSamplerUnit(int nbSamples)
        {
            _maxSamples = nbSamples;
            _samplesMap = new Dictionary<string, Sample>();
            _idToNativeIndexMap = new Dictionary<string, int>();
            _unloadingStack = new Stack<SampleTask>();
            _loadingStack = new Stack<SampleTask>();
            // startSamplerObservation();
        }

        public event Action<string> SampleStarted;

        public event Action<string> SampleStopped;

        // [DllImport("__Internal")]
        // private static extern void startSamplerObservation();
        //
        // [DllImport("__Internal")]
        // private static extern void sampler_loadInternal(int samplerIndex, string cRelativeFilePath);
        //
        // [DllImport("__Internal")]
        // private static extern void sampler_loadExternal(int samplerIndex, string cAbsoluteFilePath);
        //
        // [DllImport("__Internal")]
        // private static extern void sampler_unload(int samplerIndex);
        //
        // [DllImport("__Internal")]
        // private static extern void sampler_start(int samplerIndex);
        //
        // [DllImport("__Internal")]
        // private static extern void sampler_stop(int samplerIndex);
        //
        // [DllImport("__Internal")]
        // private static extern float sampler_getTotalTime(int samplerIndex);
        //
        // [DllImport("__Internal")]
        // private static extern void sampler_stopAll();

        public void Load(SamplerConfiguration configuration, Action<LoadingResult> completion)
        {
            if (_loadingStack.Count != 0 || _unloadingStack.Count != 0)
            {
                completion(LoadingResult.AlreadyLoadingError(string.Empty));
                return;
            }

            var first = from c in configuration.GetInternalSamples()
                select new Sample(c.Key, Sample.Source.local, c.Value);
            var second = from c in configuration.GetExternalSamples()
                select new Sample(c.Key, Sample.Source.external, c.Value);
            var samples = first.Concat(second);
            var source = from kv in _samplesMap
                where !samples.Any(c => c.id == kv.Key)
                select kv.Value;
            var source2 = source.Select(c => new SampleTask(c, Unload));
            var source3 = samples.Where(c => !_samplesMap.ContainsKey(c.id));
            var source4 = source3.Select(c => new SampleTask(c, Load));
            FillStack(_unloadingStack, source2);
            FillStack(_loadingStack, source4);
            if (_unloadingStack.Count > 0)
            {
                _pendingLoadCompletion = completion;
                _unloadingStack.Peek().execute();
            }
            else if (_loadingStack.Count > 0)
            {
                _pendingLoadCompletion = completion;
                _loadingStack.Peek().execute();
            }
            else
            {
                completion(LoadingResult.Ok());
            }
        }

        public void Start(string sampleId)
        {
            if (!_samplesMap.Any(c => c.Value.id == sampleId)) throw new Exception("try to start a sample not loaded");
            // sampler_start(GetNativeIndex(sampleId));
        }

        public void Stop(string sampleId)
        {
            // sampler_stop(GetNativeIndex(sampleId));
        }

        public float GetTotalTime(string sampleId)
        {
            return 0; //sampler_getTotalTime(GetNativeIndex(sampleId));
        }

        public void StopAll()
        {
            // sampler_stopAll();
        }

        private void Load(Sample sample)
        {
            sample.SetNatifIndex(GenerateNativeIndex(sample.id));
            switch (sample.source)
            {
                case Sample.Source.local:
                    // sampler_loadInternal(sample.natifIndex, sample.path);
                    break;
                case Sample.Source.external:
                    // sampler_loadExternal(sample.natifIndex, sample.path);
                    break;
            }
        }

        private void Unload(Sample sample)
        {
            ReleaseSampleId(sample.id);
            // sampler_unload(sample.natifIndex);
        }

        private int GenerateNativeIndex(string id)
        {
            if (_idToNativeIndexMap.ContainsKey(id)) return _idToNativeIndexMap[id];
            for (var i = 0; i < _maxSamples; i++)
                if (!_idToNativeIndexMap.ContainsValue(i))
                {
                    _idToNativeIndexMap[id] = i;
                    return i;
                }

            throw new Exception("Unable to find an available native for the id: " + id);
        }

        private int GetNativeIndex(string id)
        {
            return _idToNativeIndexMap[id];
        }

        private string GetSamplerIdFromNativeIndex(int nativeIndex)
        {
            return _idToNativeIndexMap.First(c => c.Value == nativeIndex).Key;
        }

        private void ReleaseSampleId(string id)
        {
            if (_idToNativeIndexMap.ContainsKey(id)) _idToNativeIndexMap.Remove(id);
        }

        public void NotifyLoadCompleted(int sampleIndex, LoadingResult result)
        {
            var sampleTask = _loadingStack.Pop();
            var sample = sampleTask.sample;
            _samplesMap[sample.id] = sample;
            if (_loadingStack.Count == 0)
            {
                if (_pendingLoadCompletion != null)
                {
                    var pendingLoadCompletion = _pendingLoadCompletion;
                    _pendingLoadCompletion = null;
                    pendingLoadCompletion(LoadingResult.Ok());
                }
            }
            else
            {
                _loadingStack.Peek().execute();
            }
        }

        public void NotifyUnloadCompleted(int sampleIndex, bool success)
        {
            var sampleTask = _unloadingStack.Pop();
            var sample = sampleTask.sample;
            _samplesMap.Remove(sample.id);
            if (_unloadingStack.Count == 0)
            {
                if (_loadingStack.Count > 0)
                {
                    _loadingStack.Peek().execute();
                }
                else if (_pendingLoadCompletion != null)
                {
                    var pendingLoadCompletion = _pendingLoadCompletion;
                    _pendingLoadCompletion = null;
                    pendingLoadCompletion(LoadingResult.Ok());
                }
            }
            else
            {
                _unloadingStack.Peek().execute();
            }
        }

        public void NotifySamplerStarted(int sampleIndex)
        {
            if (SampleStarted != null) SampleStarted(GetSamplerIdFromNativeIndex(sampleIndex));
        }

        public void NotifySamplerStopped(int sampleIndex)
        {
            if (SampleStopped != null) SampleStopped(GetSamplerIdFromNativeIndex(sampleIndex));
        }

        private static void FillStack<T>(Stack<T> stack, IEnumerable<T> source)
        {
            foreach (var item in source) stack.Push(item);
        }

        private class Sample
        {
            public enum Source
            {
                local,
                external
            }

            public readonly string id;

            public readonly string path;

            public readonly Source source;

            public Sample(string id, Source source, string path)
            {
                this.id = id;
                this.source = source;
                this.path = path + ".mp3";
                natifIndex = -1;
            }

            public int natifIndex { get; private set; }

            public void SetNatifIndex(int natifIndex)
            {
                this.natifIndex = natifIndex;
            }
        }

        private class SampleTask
        {
            public readonly Action<Sample> job;
            public readonly Sample sample;

            public SampleTask(Sample sample, Action<Sample> job)
            {
                this.sample = sample;
                this.job = job;
            }

            public void execute()
            {
                job(sample);
            }
        }
    }
}