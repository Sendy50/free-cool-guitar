using JetBrains.Annotations;

namespace MWM.Audio.Internal.iOS
{
    public class IOSMusicGamingAudioEngine : MusicGamingAudioEngineImpl
    {
        public IOSMusicGamingAudioEngine([CanBeNull] MultiPlayerUnit multiPlayerUnit,
            [CanBeNull] SamplerUnit samplerUnit, [CanBeNull] SamplingSynthUnit samplingSynthUnit)
            : base(multiPlayerUnit, samplerUnit, samplingSynthUnit)
        {
        }

        // [DllImport("__Internal")]
        // private static extern void setAudioRendering(bool enable);

        public override void SetAudioRendering(bool enable)
        {
            // setAudioRendering(enable);
        }
    }
}