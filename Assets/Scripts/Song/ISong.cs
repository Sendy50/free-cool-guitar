using UnityEngine;

namespace Song
{
    public interface ISong
    {
        string GetId();

        TextAsset GetJson();

        string GetAccompanimentName();

        GuitarAppMidiEventParser.ParsingResult GetParsedJson();

        string GetName();

        string GetArtist();

        bool GetIsTutorialTrack();

        SongType GetSongType();

        int GetCatalogVersion();
    }
}