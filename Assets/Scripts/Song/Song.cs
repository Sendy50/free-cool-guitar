using UnityEngine;

namespace Song
{
    [CreateAssetMenu]
    public class Song : ScriptableObject, ISong
    {
        public string id;

        public TextAsset JSON;

        public string accompanimentName;

        public string Name;

        public string artist;

        public bool isTutoTrack;

        public SongType type = SongType.Unlockable;

        public int catalogVersion;

        private GuitarAppMidiEventParser.ParsingResult? _parsedJSON;

        public GuitarAppMidiEventParser.ParsingResult parsedJSON
        {
            get
            {
                var parsedJSON = _parsedJSON;
                if (parsedJSON.HasValue)
                {
                    return _parsedJSON.Value;
                }

                _parsedJSON = GuitarAppMidiEventParser.ParseMidiFile(JSON);
                return _parsedJSON.Value;
            }
        }

        public string GetId()
        {
            return id;
        }

        public TextAsset GetJson()
        {
            return JSON;
        }

        public string GetAccompanimentName()
        {
            return accompanimentName;
        }

        public GuitarAppMidiEventParser.ParsingResult GetParsedJson()
        {
            return parsedJSON;
        }

        public string GetName()
        {
            return Name;
        }

        public string GetArtist()
        {
            return artist;
        }

        public bool GetIsTutorialTrack()
        {
            return isTutoTrack;
        }

        public SongType GetSongType()
        {
            return type;
        }

        public int GetCatalogVersion()
        {
            return catalogVersion;
        }
    }
}