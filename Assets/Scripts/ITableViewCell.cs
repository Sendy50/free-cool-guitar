public interface ITableViewCell
{
    int GetCellIndex();

    void prepareForReuse();
}