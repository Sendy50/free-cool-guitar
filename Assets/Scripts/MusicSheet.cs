using System;

public class MusicSheet : ICurrentChordProvider
{
    public MusicSheet(IUserAction[] userActions, MidiChordEvent[] chords)
    {
        UserActions = userActions;
        Chords = chords;
        Reset();
    }

    public IUserAction[] UserActions { get; private set; }

    public MidiChordEvent[] Chords { get; private set; }

    public bool HasReachedEnd { get; private set; }

    public IUserAction CurrentUserAction => UserActions[CurrentUserActionIndex];

    public MidiChordEvent CurrentChord => Chords[CurrentChordsIndex];

    public bool IsNextChordExist => CurrentChordsIndex + 1 < Chords.Length;

    public MidiChordEvent NextChordEvent => Chords[CurrentChordsIndex + 1];

    public int CurrentUserActionIndex { get; private set; }

    public int CurrentChordsIndex { get; private set; }

    public event Action<ICurrentChordProvider, IMidiChord> OnCurrentChodeDidChange;

    public IMidiChord GetCurrentMidiChord()
    {
        return CurrentChord.MidiChord;
    }

    public bool GetIsMuteRepresentationEnabled()
    {
        return false;
    }

    public bool MoveNextUserAction()
    {
        if (++CurrentUserActionIndex >= UserActions.Length)
        {
            CurrentUserActionIndex = UserActions.Length - 1;
            HasReachedEnd = true;
            return false;
        }

        return true;
    }

    public bool MoveNextChord()
    {
        if (++CurrentChordsIndex >= Chords.Length)
        {
            CurrentChordsIndex = Chords.Length - 1;
            return false;
        }

        if (OnCurrentChodeDidChange != null)
        {
            OnCurrentChodeDidChange(this, GetCurrentMidiChord());
        }

        return true;
    }

    public void Reset()
    {
        CurrentUserActionIndex = 0;
        CurrentChordsIndex = 0;
        HasReachedEnd = false;
        if (OnCurrentChodeDidChange != null)
        {
            OnCurrentChodeDidChange(this, GetCurrentMidiChord());
        }
    }

    public void Dispose()
    {
        UserActions = null;
        Chords = null;
    }
}