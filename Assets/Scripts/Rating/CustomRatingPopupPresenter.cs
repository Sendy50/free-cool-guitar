using System;
using GuitarApplication;
using MWM.GameEvent;
using Utils;

namespace Rating
{
    public class CustomRatingPopupPresenter
    {
        private Action _closeCompletion;

        private readonly IGameEventSender _gameEventSender;

        private readonly IRatingRequest _ratingRequest;
        private readonly IStatistics _statistics;

        private readonly ICustomRatingPopupView _view;

        public CustomRatingPopupPresenter(IStatistics statistics, ICustomRatingPopupView view,
            IRatingRequest ratingRequest, IGameEventSender gameEventSender)
        {
            Precondition.CheckNotNull(statistics);
            Precondition.CheckNotNull(view);
            Precondition.CheckNotNull(ratingRequest);
            Precondition.CheckNotNull(gameEventSender);
            _statistics = statistics;
            _view = view;
            _ratingRequest = ratingRequest;
            _gameEventSender = gameEventSender;
        }

        public void OnShow(Action closeCompletion = null)
        {
            _closeCompletion = closeCompletion;
            _statistics.SetDidDisplayRatingRequestThisSession(true);
            _statistics.IncrementNumRatingRequestDisplay();
            _gameEventSender.SendEvent(EventType.CustomRatingPopupDisplayed, string.Empty);
        }

        public void OnUserAcceptToReview()
        {
            _gameEventSender.SendEvent(EventType.CustomRatingPopupClickOk, string.Empty);
            _statistics.IncrementNumRatingRequestAccepted();
            _ratingRequest.RequestReview();
            Hide();
        }

        public void OnUserDenyToReview()
        {
            _gameEventSender.SendEvent(EventType.CustomRatingPopupClickNoThanks, string.Empty);
            Hide();
        }

        private void Hide()
        {
            _view.Hide();
            if (_closeCompletion != null)
            {
                _closeCompletion();
            }
        }
    }
}