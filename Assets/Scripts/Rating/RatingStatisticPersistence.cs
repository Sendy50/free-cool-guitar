using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

namespace Rating
{
    [Serializable]
    public class RatingStatisticPersistence
    {
        private const string PersistentFileName = "/ratingStatistics.dat";

        public int NumRatingRequestForThisVersion;

        public int NumRatingAcceptedForThisVersion;

        public static RatingStatisticPersistence loadFromFile()
        {
            if (File.Exists(Application.persistentDataPath + "/ratingStatistics.dat"))
            {
                var binaryFormatter = new BinaryFormatter();
                var serializationStream =
                    File.Open(Application.persistentDataPath + "/ratingStatistics.dat", FileMode.Open);
                return (RatingStatisticPersistence) binaryFormatter.Deserialize(serializationStream);
            }

            return new RatingStatisticPersistence();
        }

        public void saveToFile()
        {
            var binaryFormatter = new BinaryFormatter();
            var serializationStream = File.Create(Application.persistentDataPath + "/ratingStatistics.dat");
            binaryFormatter.Serialize(serializationStream, this);
        }
    }
}