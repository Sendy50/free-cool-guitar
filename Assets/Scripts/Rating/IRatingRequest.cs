namespace Rating
{
    public interface IRatingRequest
    {
        void RequestReview();
    }
}