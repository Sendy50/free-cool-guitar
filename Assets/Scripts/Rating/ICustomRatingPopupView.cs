using System;

namespace Rating
{
    public interface ICustomRatingPopupView
    {
        void Show(Action closeCompletion);

        void Hide();
    }
}