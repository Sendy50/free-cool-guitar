using UnityEngine;

namespace Rating
{
    public class RatingRequestAndroid : IRatingRequest
    {
        public void RequestReview()
        {
            Application.OpenURL("https://play.google.com/store/apps/details?id=com.mwm.guitar");
        }
    }
}