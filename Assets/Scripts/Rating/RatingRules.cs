using Config;
using GuitarApplication;
using Utils;

namespace Rating
{
    public static class RatingRules
    {
        public const int MaxRatingRequestDisplayPerVersion = 3;

        public const int MinNbSessionsForNotFreeApp = 2;

        public const int MinNbGameForFreeVersion = 2;

        public const int MinNbGameForNotFreeVersionAndroidSubscribed = 5;

        public const int MinNbGameForNotFreeVersionAndroidNotSubscribed = 10;

        public const int MinNbGameForNotFreeVersionIos = 1;

        public static bool ShouldDisplayRatingRequest(IStatistics statistics,
            ConfigStorageContainer.ConfigStorage configStorage, IDeviceInfoProvider deviceInfo, bool isSubscribed)
        {
            if (!CommonStrategy(statistics)) return false;
            if (configStorage.GetConfig().HasWonSubscriptionValue) return FreeStrategy(statistics);
            if (deviceInfo.GetTargetType() == DevicePlatform.Android)
                return NotFreeStrategyAndroid(statistics, isSubscribed);
            return NotFreeStrategyIos(statistics);
        }

        private static bool CommonStrategy(IStatistics statistics)
        {
            if (statistics.GetDidDisplayRatingRequestThisSession()) return false;
            if (statistics.GetNumRatingRequestAcceptedForCurrentAppVersion() > 0) return false;
            if (statistics.GetNumRatingRequestDisplayForCurrentAppVersion() > 3) return false;
            return true;
        }

        private static bool NotFreeStrategyAndroid(IStatistics statistics, bool isSubscribed)
        {
            return (isSubscribed && statistics.GetTotalNumberOfGames() >= 5 ||
                    statistics.GetTotalNumberOfGames() >= 10) && statistics.GetNumberAppLaunches() >= 2;
        }

        private static bool NotFreeStrategyIos(IStatistics statistics)
        {
            return statistics.GetTotalNumberOfGames() >= 1;
        }

        private static bool FreeStrategy(IStatistics statistics)
        {
            return statistics.GetTotalNumberOfGames() >= 2;
        }
    }
}