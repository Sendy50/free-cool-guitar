using System;
using GuitarApplication;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.UI;

namespace Rating
{
    public class CustomRatingPopup : MonoBehaviour, ICustomRatingPopupView
    {
        [SerializeField] private Image _background;

        [SerializeField] private Material _backgroundBlurMaterial;

        private Action _closeCompletion;

        private CustomRatingPopupPresenter _presenter;

        private void Awake()
        {
            _presenter = new CustomRatingPopupPresenter(Statistics.Instance, this, CreateRatingExtension(),
                ApplicationGraph.GetGameEventSender());
        }

        private void OnEnable()
        {
            var qualityManager = ApplicationGraph.GetQualityManager();
            if (qualityManager.GetQualityOption() != 0)
            {
                _background.material = _backgroundBlurMaterial;
                var screentTextureId = Shader.PropertyToID("_ScreenTexture");
                var applicationRouter = ApplicationGraph.GetApplicationRouter();
                applicationRouter.GetBlurObject().cameraBlur.onCameraBlurRenderImage +=
                    delegate(RenderTexture originalTex, RenderTexture blurTex, bool blurActive)
                    {
                        _background.material.SetTexture(screentTextureId, !blurActive ? originalTex : blurTex);
                    };
            }
            else
            {
                _background.material = null;
            }
        }

        public void Show(Action closeCompletion = null)
        {
            gameObject.SetActive(true);
            _presenter.OnShow(closeCompletion);
        }

        public void Hide()
        {
            gameObject.SetActive(false);
        }

        public void SetIsDisplayed(bool display)
        {
            gameObject.SetActive(display);
        }

        [UsedImplicitly]
        public void OnUserAcceptToReview()
        {
            _presenter.OnUserAcceptToReview();
        }

        [UsedImplicitly]
        public void OnUserDenyToReview()
        {
            _presenter.OnUserDenyToReview();
        }

        private IRatingRequest CreateRatingExtension()
        {
            return new RatingRequestAndroid();
        }
    }
}