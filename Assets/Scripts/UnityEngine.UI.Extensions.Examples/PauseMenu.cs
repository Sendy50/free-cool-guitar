namespace UnityEngine.UI.Extensions.Examples
{
    public class PauseMenu : SimpleMenu<PauseMenu>
    {
        public void OnQuitPressed()
        {
            Hide();
            Destroy(gameObject);
            SimpleMenu<GameMenu>.Hide();
        }
    }
}