using System;

namespace UnityEngine.UI.Extensions.Examples
{
    public class ScrollingCalendar : MonoBehaviour
    {
        public RectTransform monthsScrollingPanel;

        public RectTransform yearsScrollingPanel;

        public RectTransform daysScrollingPanel;

        public GameObject yearsButtonPrefab;

        public GameObject monthsButtonPrefab;

        public GameObject daysButtonPrefab;

        public RectTransform monthCenter;

        public RectTransform yearsCenter;

        public RectTransform daysCenter;

        public InputField inputFieldDays;

        public InputField inputFieldMonths;

        public InputField inputFieldYears;

        public Text dateText;

        private GameObject[] daysButtons;

        private int daysSet;

        private UIVerticalScroller daysVerticalScroller;

        private GameObject[] monthsButtons;

        private int monthsSet;

        private UIVerticalScroller monthsVerticalScroller;

        private GameObject[] yearsButtons;

        private int yearsSet;

        private UIVerticalScroller yearsVerticalScroller;

        public void Awake()
        {
            InitializeYears();
            InitializeMonths();
            InitializeDays();
            monthsVerticalScroller = new UIVerticalScroller(monthsScrollingPanel, monthsButtons, monthCenter);
            yearsVerticalScroller = new UIVerticalScroller(yearsScrollingPanel, yearsButtons, yearsCenter);
            daysVerticalScroller = new UIVerticalScroller(daysScrollingPanel, daysButtons, daysCenter);
            monthsVerticalScroller.Start();
            yearsVerticalScroller.Start();
            daysVerticalScroller.Start();
        }

        private void Update()
        {
            monthsVerticalScroller.Update();
            yearsVerticalScroller.Update();
            daysVerticalScroller.Update();
            var results = daysVerticalScroller.GetResults();
            var results2 = monthsVerticalScroller.GetResults();
            var results3 = yearsVerticalScroller.GetResults();
            results = results.EndsWith("1") && results != "11" ? results + "st" :
                results.EndsWith("2") && results != "12" ? results + "nd" :
                !results.EndsWith("3") || !(results != "13") ? results + "th" : results + "rd";
            dateText.text = results2 + " " + results + " " + results3;
        }

        private void InitializeYears()
        {
            var num = int.Parse(DateTime.Now.ToString("yyyy"));
            var array = new int[num + 1 - 1900];
            yearsButtons = new GameObject[array.Length];
            for (var i = 0; i < array.Length; i++)
            {
                array[i] = 1900 + i;
                var gameObject = Instantiate(yearsButtonPrefab, new Vector3(0f, i * 80, 0f),
                    Quaternion.Euler(new Vector3(0f, 0f, 0f)));
                gameObject.transform.SetParent(yearsScrollingPanel, false);
                gameObject.transform.localScale = new Vector3(1f, 1f, 1f);
                gameObject.GetComponentInChildren<Text>().text = string.Empty + array[i];
                gameObject.name = "Year_" + array[i];
                gameObject.AddComponent<CanvasGroup>();
                yearsButtons[i] = gameObject;
            }
        }

        private void InitializeMonths()
        {
            var array = new int[12];
            monthsButtons = new GameObject[array.Length];
            for (var i = 0; i < array.Length; i++)
            {
                var text = string.Empty;
                array[i] = i;
                var gameObject = Instantiate(monthsButtonPrefab, new Vector3(0f, i * 80, 0f),
                    Quaternion.Euler(new Vector3(0f, 0f, 0f)));
                gameObject.transform.SetParent(monthsScrollingPanel, false);
                gameObject.transform.localScale = new Vector3(1f, 1f, 1f);
                switch (i)
                {
                    case 0:
                        text = "Jan";
                        break;
                    case 1:
                        text = "Feb";
                        break;
                    case 2:
                        text = "Mar";
                        break;
                    case 3:
                        text = "Apr";
                        break;
                    case 4:
                        text = "May";
                        break;
                    case 5:
                        text = "Jun";
                        break;
                    case 6:
                        text = "Jul";
                        break;
                    case 7:
                        text = "Aug";
                        break;
                    case 8:
                        text = "Sep";
                        break;
                    case 9:
                        text = "Oct";
                        break;
                    case 10:
                        text = "Nov";
                        break;
                    case 11:
                        text = "Dec";
                        break;
                }

                gameObject.GetComponentInChildren<Text>().text = text;
                gameObject.name = "Month_" + array[i];
                gameObject.AddComponent<CanvasGroup>();
                monthsButtons[i] = gameObject;
            }
        }

        private void InitializeDays()
        {
            var array = new int[31];
            daysButtons = new GameObject[array.Length];
            for (var i = 0; i < array.Length; i++)
            {
                array[i] = i + 1;
                var gameObject = Instantiate(daysButtonPrefab, new Vector3(0f, i * 80, 0f),
                    Quaternion.Euler(new Vector3(0f, 0f, 0f)));
                gameObject.transform.SetParent(daysScrollingPanel, false);
                gameObject.transform.localScale = new Vector3(1f, 1f, 1f);
                gameObject.GetComponentInChildren<Text>().text = string.Empty + array[i];
                gameObject.name = "Day_" + array[i];
                gameObject.AddComponent<CanvasGroup>();
                daysButtons[i] = gameObject;
            }
        }

        public void SetDate()
        {
            daysSet = int.Parse(inputFieldDays.text) - 1;
            monthsSet = int.Parse(inputFieldMonths.text) - 1;
            yearsSet = int.Parse(inputFieldYears.text) - 1900;
            daysVerticalScroller.SnapToElement(daysSet);
            monthsVerticalScroller.SnapToElement(monthsSet);
            yearsVerticalScroller.SnapToElement(yearsSet);
        }

        public void DaysScrollUp()
        {
            daysVerticalScroller.ScrollUp();
        }

        public void DaysScrollDown()
        {
            daysVerticalScroller.ScrollDown();
        }

        public void MonthsScrollUp()
        {
            monthsVerticalScroller.ScrollUp();
        }

        public void MonthsScrollDown()
        {
            monthsVerticalScroller.ScrollDown();
        }

        public void YearsScrollUp()
        {
            yearsVerticalScroller.ScrollUp();
        }

        public void YearsScrollDown()
        {
            yearsVerticalScroller.ScrollDown();
        }
    }
}