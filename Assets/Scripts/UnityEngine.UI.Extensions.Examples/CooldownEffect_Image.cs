namespace UnityEngine.UI.Extensions.Examples
{
    [RequireComponent(typeof(Image))]
    public class CooldownEffect_Image : MonoBehaviour
    {
        public CooldownButton cooldown;

        public Text displayText;

        private string originalText;

        private Image target;

        private void Start()
        {
            if (cooldown == null) Debug.LogError("Missing Cooldown Button assignment");
            target = GetComponent<Image>();
        }

        private void Update()
        {
            target.fillAmount = Mathf.Lerp(0f, 1f, cooldown.CooldownTimeRemaining / cooldown.CooldownTimeout);
            if ((bool) displayText) displayText.text = $"{cooldown.CooldownPercentComplete}%";
        }

        private void OnEnable()
        {
            if ((bool) displayText) originalText = displayText.text;
        }

        private void OnDisable()
        {
            if ((bool) displayText) displayText.text = originalText;
        }
    }
}