public struct Chord : IChord
{
    public int Hash;

    public string Name;

    public bool IsSolo;

    public int[] Fingers;

    public string Key;

    public string Suffix;

    public int GetHash()
    {
        return Hash;
    }

    public string GetName()
    {
        return Name;
    }

    public bool GetIsSolo()
    {
        return IsSolo;
    }

    public int[] GetFingers()
    {
        return Fingers;
    }

    public string GetKey()
    {
        return Key;
    }

    public string GetSuffix()
    {
        return Suffix;
    }

    public bool IsEmptyChord()
    {
        return Hash == ChordMap.emptyChordMidiNumbersHash;
    }
}