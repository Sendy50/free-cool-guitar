using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class View : MonoBehaviour
{
    public float AnimationDuration = 0.3f;

    public RectTransform BackgroundPanel;

    public GameObject Canvas;

    public CanvasScaler CanvasScaler;

    private float _canvasHeight => CanvasScaler.referenceResolution.y;

    public bool IsDisplayed { get; private set; }

    private void Awake()
    {
        IsDisplayed = Canvas.activeSelf;
        var y = !IsDisplayed ? 0f - _canvasHeight : 0f;
        var anchoredPosition = BackgroundPanel.anchoredPosition;
        anchoredPosition.y = y;
        BackgroundPanel.anchoredPosition = anchoredPosition;
    }

    public void Present(bool animated)
    {
        if (!IsDisplayed)
        {
            Canvas.SetActive(true);
            if (!animated)
            {
                var anchoredPosition = BackgroundPanel.anchoredPosition;
                anchoredPosition.y = 0f;
                BackgroundPanel.anchoredPosition = anchoredPosition;
                IsDisplayed = true;
            }
            else
            {
                StartCoroutine(UpdateYPos(0f, AnimationDuration));
            }
        }
    }

    public void Dismiss(bool animated)
    {
        if (IsDisplayed)
        {
            if (!animated)
            {
                var anchoredPosition = BackgroundPanel.anchoredPosition;
                anchoredPosition.y = 0f - _canvasHeight;
                BackgroundPanel.anchoredPosition = anchoredPosition;
                IsDisplayed = false;
                Canvas.SetActive(false);
            }
            else
            {
                StartCoroutine(UpdateYPos(0f - _canvasHeight, AnimationDuration));
            }
        }
    }

    private IEnumerator UpdateYPos(float destinationY, float duration)
    {
        var time = 0f;
        var startY = BackgroundPanel.anchoredPosition.y;
        while (time <= duration)
        {
            time += Time.deltaTime;
            var pos = BackgroundPanel.anchoredPosition;
            pos.y = Mathf.Lerp(startY, destinationY, time / duration);
            BackgroundPanel.anchoredPosition = pos;
            yield return null;
        }

        if (destinationY == 0f)
        {
            IsDisplayed = true;
            yield break;
        }

        IsDisplayed = false;
        Canvas.SetActive(false);
    }
}