using UnityEngine;

public class TestTableViewDelegate : MonoBehaviour, TableViewDelegate
{
    public TableView tableView;

    private void Start()
    {
        tableView.tableViewDelegate = this;
        tableView.startTableView();
    }

    public int numberOfRows()
    {
        return 50;
    }

    public void configureTableViewCellAtIndex(ITableViewCell cell, int index)
    {
    }

    public void topCellIndexChange(int topCellIndex)
    {
    }
}