using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

namespace SongCatalog
{
    public class SongIdsPlayedStorageImpl : SongIdsPlayedStorage
    {
        private static readonly string SongsIdsPlayedFilePath = Application.persistentDataPath + "/songs_ids_played";

        public HashSet<string> ReadSongsIdsPlayed()
        {
            if (!File.Exists(SongsIdsPlayedFilePath)) return new HashSet<string>();
            var serializationStream = File.OpenRead(SongsIdsPlayedFilePath);
            var binaryFormatter = new BinaryFormatter();
            var collection = (List<string>) binaryFormatter.Deserialize(serializationStream);
            return new HashSet<string>(collection);
        }

        public void WriteSongsIdsPlayed(IEnumerable<string> songsIdsPlayedHashSet)
        {
            var graph = songsIdsPlayedHashSet.ToList();
            var serializationStream = File.Create(SongsIdsPlayedFilePath);
            var binaryFormatter = new BinaryFormatter();
            binaryFormatter.Serialize(serializationStream, graph);
        }
    }
}