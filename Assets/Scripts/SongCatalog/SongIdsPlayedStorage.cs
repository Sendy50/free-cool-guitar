using System.Collections.Generic;

namespace SongCatalog
{
    public interface SongIdsPlayedStorage
    {
        HashSet<string> ReadSongsIdsPlayed();

        void WriteSongsIdsPlayed(IEnumerable<string> samplePacksIdsPlayedHashSet);
    }
}