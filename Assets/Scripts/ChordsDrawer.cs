using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChordsDrawer : MonoBehaviour, IChordsDrawer
{
    private static readonly int[] midiEmptyRef = new int[6]
    {
        40,
        45,
        50,
        55,
        59,
        64
    };

    private static int[] possibleStartFrets = new int[5]
    {
        1,
        3,
        5,
        7,
        9
    };

    private static int[] pointFret = new int[4]
    {
        3,
        5,
        7,
        9
    };

    public Text chordTitleText;

    public List<Text> fretNumberTexts;

    public Image backgroundImage;

    public Sprite topGuitarSprite;

    public Sprite guitarSprite;

    public List<StringFingerIndicator> fingersIndicators;

    private float yCrossPos;

    private void Awake()
    {
        yCrossPos = fingersIndicators[0].rectTransform.anchorMin.y;
    }

    public void DrawChord(IChordContainer chord)
    {
        chordTitleText.text = chord.GetChord().GetName();
        var midiNumbers = chord.GetMidiChord().GetMidiNumbers();
        var fingers = chord.GetChord().GetFingers();
        var num = 24;
        var num2 = 0;
        for (var i = 0; i < midiNumbers.Length; i++)
        {
            var num3 = midiNumbers[i];
            if (num3 > 0 && fingers[i] > 0)
            {
                var a = num3 - midiEmptyRef[i];
                num = Mathf.Min(a, num);
                num2 = Mathf.Max(a, num2);
            }
        }

        if (num2 - num < 4 && num < 3) num = 1;
        for (var j = 0; j < fretNumberTexts.Count; j++) fretNumberTexts[j].text = (num + j).ToString();
        backgroundImage.sprite = num != 1 ? guitarSprite : topGuitarSprite;
        for (var k = 0; k < fingers.Length; k++)
        {
            var stringFingerIndicator = fingersIndicators[k];
            stringFingerIndicator.gameObject.SetActive(true);
            var num4 = fingers[k];
            if (num4 == 0 && midiNumbers[k] == 0)
            {
                stringFingerIndicator.numberText.gameObject.SetActive(false);
                stringFingerIndicator.crossImage.gameObject.SetActive(true);
                stringFingerIndicator.rectTransform.anchorMin =
                    new Vector2(stringFingerIndicator.rectTransform.anchorMin.x, yCrossPos);
                stringFingerIndicator.rectTransform.anchorMax = stringFingerIndicator.rectTransform.anchorMin;
                continue;
            }

            if (num4 == 0)
            {
                stringFingerIndicator.gameObject.SetActive(false);
                continue;
            }

            stringFingerIndicator.numberText.text = num4.ToString();
            var num5 = midiNumbers[k];
            var index = num5 - midiEmptyRef[k] - num;
            stringFingerIndicator.numberText.gameObject.SetActive(true);
            stringFingerIndicator.numberText.text = num4.ToString();
            stringFingerIndicator.crossImage.gameObject.SetActive(false);
            var y = fretNumberTexts[index].GetComponent<RectTransform>().anchorMin.y;
            stringFingerIndicator.rectTransform.anchorMin =
                new Vector2(stringFingerIndicator.rectTransform.anchorMin.x, y);
            stringFingerIndicator.rectTransform.anchorMax = stringFingerIndicator.rectTransform.anchorMin;
        }
    }
}