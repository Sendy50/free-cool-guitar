public class MusicPedalMidiEvent : MidiEvent
{
    public bool OnOffValue;

    public int trackIDX;

    public MusicPedalMidiEvent(float time, bool onOffValue, int track)
        : base(time)
    {
        OnOffValue = onOffValue;
        trackIDX = track;
    }
}