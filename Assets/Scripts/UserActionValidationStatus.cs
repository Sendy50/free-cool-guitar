public enum UserActionValidationStatus
{
    unstarted,
    validated,
    unvalidated,
    detecting
}