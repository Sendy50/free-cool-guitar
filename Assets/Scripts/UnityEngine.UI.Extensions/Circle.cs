using System;

namespace UnityEngine.UI.Extensions
{
    public class Circle
    {
        [SerializeField] private int steps;

        [SerializeField] private float xAxis;

        [SerializeField] private float yAxis;

        public Circle(float radius)
        {
            xAxis = radius;
            yAxis = radius;
            steps = 1;
        }

        public Circle(float radius, int steps)
        {
            xAxis = radius;
            yAxis = radius;
            this.steps = steps;
        }

        public Circle(float xAxis, float yAxis)
        {
            this.xAxis = xAxis;
            this.yAxis = yAxis;
            steps = 10;
        }

        public Circle(float xAxis, float yAxis, int steps)
        {
            this.xAxis = xAxis;
            this.yAxis = yAxis;
            this.steps = steps;
        }

        public float X
        {
            get => xAxis;
            set => xAxis = value;
        }

        public float Y
        {
            get => yAxis;
            set => yAxis = value;
        }

        public int Steps
        {
            get => steps;
            set => steps = value;
        }

        public Vector2 Evaluate(float t)
        {
            var num = 360f / steps;
            var f = (float) Math.PI / 180f * num * t;
            var x = Mathf.Sin(f) * xAxis;
            var y = Mathf.Cos(f) * yAxis;
            return new Vector2(x, y);
        }

        public void Evaluate(float t, out Vector2 eval)
        {
            var num = 360f / steps;
            var f = (float) Math.PI / 180f * num * t;
            eval.x = Mathf.Sin(f) * xAxis;
            eval.y = Mathf.Cos(f) * yAxis;
        }
    }
}