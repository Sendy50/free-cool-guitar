using System;
using System.Collections.Generic;

namespace UnityEngine.UI.Extensions
{
    [AddComponentMenu("UI/Effects/Extensions/Gradient2")]
    public class Gradient2 : BaseMeshEffect
    {
        public enum Blend
        {
            Override,
            Add,
            Multiply
        }

        public enum Type
        {
            Horizontal,
            Vertical,
            Radial,
            Diamond
        }

        [SerializeField] private Type _gradientType;

        [SerializeField] private Blend _blendMode = Blend.Multiply;

        [SerializeField] [Range(-1f, 1f)] private float _offset;

        [SerializeField] private UnityEngine.Gradient _effectGradient = new UnityEngine.Gradient
        {
            colorKeys = new GradientColorKey[2]
            {
                new GradientColorKey(Color.black, 0f),
                new GradientColorKey(Color.white, 1f)
            }
        };

        public Blend BlendMode
        {
            get => _blendMode;
            set
            {
                _blendMode = value;
                graphic.SetVerticesDirty();
            }
        }

        public UnityEngine.Gradient EffectGradient
        {
            get => _effectGradient;
            set
            {
                _effectGradient = value;
                graphic.SetVerticesDirty();
            }
        }

        public Type GradientType
        {
            get => _gradientType;
            set
            {
                _gradientType = value;
                graphic.SetVerticesDirty();
            }
        }

        public float Offset
        {
            get => _offset;
            set
            {
                _offset = value;
                graphic.SetVerticesDirty();
            }
        }

        public override void ModifyMesh(VertexHelper helper)
        {
            if (!IsActive() || helper.currentVertCount == 0) return;
            var list = new List<UIVertex>();
            helper.GetUIVertexStream(list);
            var count = list.Count;
            switch (GradientType)
            {
                case Type.Horizontal:
                {
                    var num27 = list[0].position.x;
                    var num28 = list[0].position.x;
                    var num29 = 0f;
                    for (var num30 = count - 1; num30 >= 1; num30--)
                    {
                        num29 = list[num30].position.x;
                        if (num29 > num28)
                            num28 = num29;
                        else if (num29 < num27) num27 = num29;
                    }

                    var num31 = 1f / (num28 - num27);
                    UIVertex vertex4 = default;
                    for (var num32 = 0; num32 < helper.currentVertCount; num32++)
                    {
                        helper.PopulateUIVertex(ref vertex4, num32);
                        vertex4.color = BlendColor(vertex4.color,
                            EffectGradient.Evaluate((vertex4.position.x - num27) * num31 - Offset));
                        helper.SetUIVertex(vertex4, num32);
                    }

                    break;
                }
                case Type.Vertical:
                {
                    var num16 = list[0].position.y;
                    var num17 = list[0].position.y;
                    var num18 = 0f;
                    for (var num19 = count - 1; num19 >= 1; num19--)
                    {
                        num18 = list[num19].position.y;
                        if (num18 > num17)
                            num17 = num18;
                        else if (num18 < num16) num16 = num18;
                    }

                    var num20 = 1f / (num17 - num16);
                    UIVertex vertex2 = default;
                    for (var l = 0; l < helper.currentVertCount; l++)
                    {
                        helper.PopulateUIVertex(ref vertex2, l);
                        vertex2.color = BlendColor(vertex2.color,
                            EffectGradient.Evaluate((vertex2.position.y - num16) * num20 - Offset));
                        helper.SetUIVertex(vertex2, l);
                    }

                    break;
                }
                case Type.Diamond:
                {
                    var num21 = list[0].position.y;
                    var num22 = list[0].position.y;
                    var num23 = 0f;
                    for (var num24 = count - 1; num24 >= 1; num24--)
                    {
                        num23 = list[num24].position.y;
                        if (num23 > num22)
                            num22 = num23;
                        else if (num23 < num21) num21 = num23;
                    }

                    var num25 = 1f / (num22 - num21);
                    helper.Clear();
                    for (var m = 0; m < count; m++) helper.AddVert(list[m]);
                    var d3 = (num21 + num22) / 2f;
                    UIVertex v3 = default;
                    v3.position = (Vector3.right + Vector3.up) * d3 + Vector3.forward * list[0].position.z;
                    v3.normal = list[0].normal;
                    v3.color = Color.white;
                    helper.AddVert(v3);
                    for (var n = 1; n < count; n++) helper.AddTriangle(n - 1, n, count);
                    helper.AddTriangle(0, count - 1, count);
                    UIVertex vertex3 = default;
                    for (var num26 = 0; num26 < helper.currentVertCount; num26++)
                    {
                        helper.PopulateUIVertex(ref vertex3, num26);
                        vertex3.color = BlendColor(vertex3.color,
                            EffectGradient.Evaluate(Vector3.Distance(vertex3.position, v3.position) * num25 - Offset));
                        helper.SetUIVertex(vertex3, num26);
                    }

                    break;
                }
                case Type.Radial:
                {
                    var num = list[0].position.x;
                    var num2 = list[0].position.x;
                    var num3 = list[0].position.y;
                    var num4 = list[0].position.y;
                    var num5 = 0f;
                    var num6 = 0f;
                    for (var num7 = count - 1; num7 >= 1; num7--)
                    {
                        num5 = list[num7].position.x;
                        if (num5 > num2)
                            num2 = num5;
                        else if (num5 < num) num = num5;
                        num6 = list[num7].position.y;
                        if (num6 > num4)
                            num4 = num6;
                        else if (num6 < num3) num3 = num6;
                    }

                    var num8 = 1f / (num2 - num);
                    var num9 = 1f / (num4 - num3);
                    helper.Clear();
                    var num10 = (num2 + num) / 2f;
                    var num11 = (num3 + num4) / 2f;
                    var num12 = (num2 - num) / 2f;
                    var num13 = (num4 - num3) / 2f;
                    UIVertex v = default;
                    v.position = Vector3.right * num10 + Vector3.up * num11 + Vector3.forward * list[0].position.z;
                    v.normal = list[0].normal;
                    v.color = Color.white;
                    var num14 = 64;
                    for (var i = 0; i < num14; i++)
                    {
                        UIVertex v2 = default;
                        var num15 = i * 360f / num14;
                        var d = Mathf.Cos((float) Math.PI / 180f * num15) * num12;
                        var d2 = Mathf.Sin((float) Math.PI / 180f * num15) * num13;
                        v2.position = Vector3.right * d + Vector3.up * d2 + Vector3.forward * list[0].position.z;
                        v2.normal = list[0].normal;
                        v2.color = Color.white;
                        helper.AddVert(v2);
                    }

                    helper.AddVert(v);
                    for (var j = 1; j < num14; j++) helper.AddTriangle(j - 1, j, num14);
                    helper.AddTriangle(0, num14 - 1, num14);
                    UIVertex vertex = default;
                    for (var k = 0; k < helper.currentVertCount; k++)
                    {
                        helper.PopulateUIVertex(ref vertex, k);
                        vertex.color = BlendColor(vertex.color,
                            EffectGradient.Evaluate(
                                Mathf.Sqrt(Mathf.Pow(Mathf.Abs(vertex.position.x - num10) * num8, 2f) +
                                           Mathf.Pow(Mathf.Abs(vertex.position.y - num11) * num9, 2f)) * 2f - Offset));
                        helper.SetUIVertex(vertex, k);
                    }

                    break;
                }
            }
        }

        private Color BlendColor(Color colorA, Color colorB)
        {
            switch (BlendMode)
            {
                case Blend.Add:
                    return colorA + colorB;
                case Blend.Multiply:
                    return colorA * colorB;
                default:
                    return colorB;
            }
        }
    }
}