using UnityEngine.EventSystems;

namespace UnityEngine.UI.Extensions
{
    [AddComponentMenu("UI/Extensions/RescalePanels/RescalePanel")]
    public class RescalePanel : MonoBehaviour, IPointerDownHandler, IDragHandler, IEventSystemHandler
    {
        public Vector2 minSize;

        public Vector2 maxSize;

        private Vector2 currentPointerPosition;

        private Transform goTransform;

        private Vector2 previousPointerPosition;

        private RectTransform rectTransform;

        private Vector2 sizeDelta;

        private RectTransform thisRectTransform;

        private void Awake()
        {
            rectTransform = transform.parent.GetComponent<RectTransform>();
            goTransform = transform.parent;
            thisRectTransform = GetComponent<RectTransform>();
            sizeDelta = thisRectTransform.sizeDelta;
        }

        public void OnDrag(PointerEventData data)
        {
            if (!(rectTransform == null))
            {
                var localScale = goTransform.localScale;
                RectTransformUtility.ScreenPointToLocalPointInRectangle(rectTransform, data.position,
                    data.pressEventCamera, out currentPointerPosition);
                var vector = currentPointerPosition - previousPointerPosition;
                localScale += new Vector3((0f - vector.y) * 0.001f, (0f - vector.y) * 0.001f, 0f);
                localScale = new Vector3(Mathf.Clamp(localScale.x, minSize.x, maxSize.x),
                    Mathf.Clamp(localScale.y, minSize.y, maxSize.y), 1f);
                goTransform.localScale = localScale;
                previousPointerPosition = currentPointerPosition;
                var num = sizeDelta.x / goTransform.localScale.x;
                var vector2 = new Vector2(num, num);
                thisRectTransform.sizeDelta = vector2;
            }
        }

        public void OnPointerDown(PointerEventData data)
        {
            rectTransform.SetAsLastSibling();
            RectTransformUtility.ScreenPointToLocalPointInRectangle(rectTransform, data.position, data.pressEventCamera,
                out previousPointerPosition);
        }
    }
}