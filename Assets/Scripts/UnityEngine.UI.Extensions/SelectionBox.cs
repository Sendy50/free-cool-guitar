using System;
using System.Collections.Generic;
using UnityEngine.Events;

namespace UnityEngine.UI.Extensions
{
    [RequireComponent(typeof(Canvas))]
    [AddComponentMenu("UI/Extensions/Selection Box")]
    public class SelectionBox : MonoBehaviour
    {
        public Color color;

        public Sprite art;

        public RectTransform selectionMask;

        private RectTransform boxRect;

        private IBoxSelectable clickedAfterDrag;

        private IBoxSelectable clickedBeforeDrag;

        public SelectionEvent onSelectionChange = new SelectionEvent();

        private Vector2 origin;

        private MonoBehaviour[] selectableGroup;

        private IBoxSelectable[] selectables;

        private void Start()
        {
            ValidateCanvas();
            CreateBoxRect();
            ResetBoxRect();
        }

        private void Update()
        {
            BeginSelection();
            DragSelection();
            EndSelection();
        }

        private void ValidateCanvas()
        {
            var component = gameObject.GetComponent<Canvas>();
            if (component.renderMode != 0)
                throw new Exception("SelectionBox component must be placed on a canvas in Screen Space Overlay mode.");
            var component2 = gameObject.GetComponent<CanvasScaler>();
            if ((bool) component2 && component2.enabled &&
                (!Mathf.Approximately(component2.scaleFactor, 1f) || component2.uiScaleMode != 0))
            {
                Destroy(component2);
                Debug.LogWarning(
                    "SelectionBox component is on a gameObject with a Canvas Scaler component. As of now, Canvas Scalers without the default settings throw off the coordinates of the selection box. Canvas Scaler has been removed.");
            }
        }

        private void SetSelectableGroup(IEnumerable<MonoBehaviour> behaviourCollection)
        {
            if (behaviourCollection == null)
            {
                selectableGroup = null;
                return;
            }

            var list = new List<MonoBehaviour>();
            foreach (var item in behaviourCollection)
                if (item is IBoxSelectable)
                    list.Add(item);
            selectableGroup = list.ToArray();
        }

        private void CreateBoxRect()
        {
            var gameObject = new GameObject();
            gameObject.name = "Selection Box";
            gameObject.transform.parent = transform;
            gameObject.AddComponent<Image>();
            boxRect = gameObject.transform as RectTransform;
        }

        private void ResetBoxRect()
        {
            var component = boxRect.GetComponent<Image>();
            component.color = color;
            component.sprite = art;
            origin = Vector2.zero;
            boxRect.anchoredPosition = Vector2.zero;
            boxRect.sizeDelta = Vector2.zero;
            boxRect.anchorMax = Vector2.zero;
            boxRect.anchorMin = Vector2.zero;
            boxRect.pivot = Vector2.zero;
            boxRect.gameObject.SetActive(false);
        }

        private void BeginSelection()
        {
            if (!Input.GetMouseButtonDown(0)) return;
            boxRect.gameObject.SetActive(true);
            origin = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
            if (!PointIsValidAgainstSelectionMask(origin))
            {
                ResetBoxRect();
                return;
            }

            boxRect.anchoredPosition = origin;
            var array = selectableGroup != null ? selectableGroup : FindObjectsOfType<MonoBehaviour>();
            var list = new List<IBoxSelectable>();
            var array2 = array;
            foreach (var monoBehaviour in array2)
            {
                var boxSelectable = monoBehaviour as IBoxSelectable;
                if (boxSelectable != null)
                {
                    list.Add(boxSelectable);
                    if (!Input.GetKey(KeyCode.LeftShift)) boxSelectable.selected = false;
                }
            }

            selectables = list.ToArray();
            clickedBeforeDrag = GetSelectableAtMousePosition();
        }

        private bool PointIsValidAgainstSelectionMask(Vector2 screenPoint)
        {
            if (!selectionMask) return true;
            var screenPointCamera = GetScreenPointCamera(selectionMask);
            return RectTransformUtility.RectangleContainsScreenPoint(selectionMask, screenPoint, screenPointCamera);
        }

        private IBoxSelectable GetSelectableAtMousePosition()
        {
            if (!PointIsValidAgainstSelectionMask(Input.mousePosition)) return null;
            var array = selectables;
            foreach (var boxSelectable in array)
            {
                var rectTransform = boxSelectable.transform as RectTransform;
                if ((bool) rectTransform)
                {
                    var screenPointCamera = GetScreenPointCamera(rectTransform);
                    if (RectTransformUtility.RectangleContainsScreenPoint(rectTransform, Input.mousePosition,
                        screenPointCamera)) return boxSelectable;
                    continue;
                }

                var magnitude = boxSelectable.transform.GetComponent<Renderer>().bounds.extents.magnitude;
                var screenPointOfSelectable = GetScreenPointOfSelectable(boxSelectable);
                if (Vector2.Distance(screenPointOfSelectable, Input.mousePosition) <= magnitude) return boxSelectable;
            }

            return null;
        }

        private void DragSelection()
        {
            if (Input.GetMouseButton(0) && boxRect.gameObject.activeSelf)
            {
                var a = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
                var sizeDelta = a - origin;
                var anchoredPosition = origin;
                if (sizeDelta.x < 0f)
                {
                    anchoredPosition.x = a.x;
                    sizeDelta.x = 0f - sizeDelta.x;
                }

                if (sizeDelta.y < 0f)
                {
                    anchoredPosition.y = a.y;
                    sizeDelta.y = 0f - sizeDelta.y;
                }

                boxRect.anchoredPosition = anchoredPosition;
                boxRect.sizeDelta = sizeDelta;
                var array = selectables;
                foreach (var boxSelectable in array)
                {
                    Vector3 v = GetScreenPointOfSelectable(boxSelectable);
                    boxSelectable.preSelected = RectTransformUtility.RectangleContainsScreenPoint(boxRect, v, null) &&
                                                PointIsValidAgainstSelectionMask(v);
                }

                if (clickedBeforeDrag != null) clickedBeforeDrag.preSelected = true;
            }
        }

        private void ApplySingleClickDeselection()
        {
            if (clickedBeforeDrag != null && clickedAfterDrag != null && clickedBeforeDrag.selected &&
                clickedBeforeDrag.transform == clickedAfterDrag.transform)
            {
                clickedBeforeDrag.selected = false;
                clickedBeforeDrag.preSelected = false;
            }
        }

        private void ApplyPreSelections()
        {
            var array = selectables;
            foreach (var boxSelectable in array)
                if (boxSelectable.preSelected)
                {
                    boxSelectable.selected = true;
                    boxSelectable.preSelected = false;
                }
        }

        private Vector2 GetScreenPointOfSelectable(IBoxSelectable selectable)
        {
            var rectTransform = selectable.transform as RectTransform;
            if ((bool) rectTransform)
            {
                var screenPointCamera = GetScreenPointCamera(rectTransform);
                return RectTransformUtility.WorldToScreenPoint(screenPointCamera, selectable.transform.position);
            }

            return Camera.main.WorldToScreenPoint(selectable.transform.position);
        }

        private Camera GetScreenPointCamera(RectTransform rectTransform)
        {
            Canvas canvas = null;
            var rectTransform2 = rectTransform;
            do
            {
                canvas = rectTransform2.GetComponent<Canvas>();
                if ((bool) canvas && !canvas.isRootCanvas) canvas = null;
                rectTransform2 = (RectTransform) rectTransform2.parent;
            } while (canvas == null);

            switch (canvas.renderMode)
            {
                case RenderMode.ScreenSpaceOverlay:
                    return null;
                case RenderMode.ScreenSpaceCamera:
                    return !canvas.worldCamera ? Camera.main : canvas.worldCamera;
                default:
                    return Camera.main;
            }
        }

        public IBoxSelectable[] GetAllSelected()
        {
            if (selectables == null) return new IBoxSelectable[0];
            var list = new List<IBoxSelectable>();
            var array = selectables;
            foreach (var boxSelectable in array)
                if (boxSelectable.selected)
                    list.Add(boxSelectable);
            return list.ToArray();
        }

        private void EndSelection()
        {
            if (Input.GetMouseButtonUp(0) && boxRect.gameObject.activeSelf)
            {
                clickedAfterDrag = GetSelectableAtMousePosition();
                ApplySingleClickDeselection();
                ApplyPreSelections();
                ResetBoxRect();
                onSelectionChange.Invoke(GetAllSelected());
            }
        }

        public class SelectionEvent : UnityEvent<IBoxSelectable[]>
        {
        }
    }
}