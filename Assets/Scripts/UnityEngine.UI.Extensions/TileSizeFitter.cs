using System;
using UnityEngine.EventSystems;

namespace UnityEngine.UI.Extensions
{
    [ExecuteInEditMode]
    [RequireComponent(typeof(RectTransform))]
    [AddComponentMenu("Layout/Extensions/Tile Size Fitter")]
    public class TileSizeFitter : UIBehaviour, ILayoutSelfController, ILayoutController
    {
        [SerializeField] private Vector2 m_Border = Vector2.zero;

        [SerializeField] private Vector2 m_TileSize = Vector2.zero;

        [NonSerialized] private RectTransform m_Rect;

        private DrivenRectTransformTracker m_Tracker;

        public Vector2 Border
        {
            get => m_Border;
            set
            {
                if (SetPropertyUtility.SetStruct(ref m_Border, value)) SetDirty();
            }
        }

        public Vector2 TileSize
        {
            get => m_TileSize;
            set
            {
                if (SetPropertyUtility.SetStruct(ref m_TileSize, value)) SetDirty();
            }
        }

        private RectTransform rectTransform
        {
            get
            {
                if (m_Rect == null) m_Rect = GetComponent<RectTransform>();
                return m_Rect;
            }
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            SetDirty();
        }

        protected override void OnDisable()
        {
            m_Tracker.Clear();
            LayoutRebuilder.MarkLayoutForRebuild(rectTransform);
            base.OnDisable();
        }

        protected override void OnRectTransformDimensionsChange()
        {
            UpdateRect();
        }

        public virtual void SetLayoutHorizontal()
        {
        }

        public virtual void SetLayoutVertical()
        {
        }

        private void UpdateRect()
        {
            if (IsActive())
            {
                m_Tracker.Clear();
                m_Tracker.Add(this, rectTransform,
                    DrivenTransformProperties.Anchors | DrivenTransformProperties.AnchoredPosition);
                rectTransform.anchorMin = Vector2.zero;
                rectTransform.anchorMax = Vector2.one;
                rectTransform.anchoredPosition = Vector2.zero;
                m_Tracker.Add(this, rectTransform, DrivenTransformProperties.SizeDelta);
                var a = GetParentSize() - Border;
                if (TileSize.x > 0.001f)
                    a.x -= Mathf.Floor(a.x / TileSize.x) * TileSize.x;
                else
                    a.x = 0f;
                if (TileSize.y > 0.001f)
                    a.y -= Mathf.Floor(a.y / TileSize.y) * TileSize.y;
                else
                    a.y = 0f;
                rectTransform.sizeDelta = -a;
            }
        }

        private Vector2 GetParentSize()
        {
            var rectTransform = this.rectTransform.parent as RectTransform;
            if (!rectTransform) return Vector2.zero;
            return rectTransform.rect.size;
        }

        protected void SetDirty()
        {
            if (IsActive()) UpdateRect();
        }
    }
}