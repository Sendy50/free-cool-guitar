using System;
using System.Collections.Generic;
using UnityEngine.Sprites;

namespace UnityEngine.UI.Extensions
{
    [AddComponentMenu("UI/Extensions/Primitives/UILineRenderer")]
    [RequireComponent(typeof(RectTransform))]
    public class UILineRenderer : UIPrimitiveBase
    {
        public enum BezierType
        {
            None,
            Quick,
            Basic,
            Improved,
            Catenary
        }

        public enum JoinType
        {
            Bevel,
            Miter
        }

        private const float MIN_MITER_JOIN = (float) Math.PI / 12f;

        private const float MIN_BEVEL_NICE_JOIN = (float) Math.PI / 6f;

        private static Vector2 UV_TOP_LEFT;

        private static Vector2 UV_BOTTOM_LEFT;

        private static Vector2 UV_TOP_CENTER_LEFT;

        private static Vector2 UV_TOP_CENTER_RIGHT;

        private static Vector2 UV_BOTTOM_CENTER_LEFT;

        private static Vector2 UV_BOTTOM_CENTER_RIGHT;

        private static Vector2 UV_TOP_RIGHT;

        private static Vector2 UV_BOTTOM_RIGHT;

        private static Vector2[] startUvs;

        private static Vector2[] middleUvs;

        private static Vector2[] endUvs;

        private static Vector2[] fullUvs;

        [SerializeField] [Tooltip("Points to draw lines between\n Can be improved using the Resolution Option")]
        internal Vector2[] m_points;

        [SerializeField] [Tooltip("Thickness of the line")]
        internal float lineThickness = 2f;

        [SerializeField]
        [Tooltip("Use the relative bounds of the Rect Transform (0,0 -> 0,1) or screen space coordinates")]
        internal bool relativeSize;

        [SerializeField] [Tooltip("Do the points identify a single line or split pairs of lines")]
        internal bool lineList;

        [SerializeField] [Tooltip("Add end caps to each line\nMultiple caps when used with Line List")]
        internal bool lineCaps;

        [SerializeField] [Tooltip("Resolution of the Bezier curve, different to line Resolution")]
        internal int bezierSegmentsPerCurve = 10;

        [Tooltip("The type of Join used between lines, Square/Mitre or Curved/Bevel")]
        public JoinType LineJoins;

        [Tooltip(
            "Bezier method to apply to line, see docs for options\nCan't be used in conjunction with Resolution as Bezier already changes the resolution")]
        public BezierType BezierMode;

        [HideInInspector] public bool drivenExternally;

        public float LineThickness
        {
            get => lineThickness;
            set
            {
                lineThickness = value;
                SetAllDirty();
            }
        }

        public bool RelativeSize
        {
            get => relativeSize;
            set
            {
                relativeSize = value;
                SetAllDirty();
            }
        }

        public bool LineList
        {
            get => lineList;
            set
            {
                lineList = value;
                SetAllDirty();
            }
        }

        public bool LineCaps
        {
            get => lineCaps;
            set
            {
                lineCaps = value;
                SetAllDirty();
            }
        }

        public int BezierSegmentsPerCurve
        {
            get => bezierSegmentsPerCurve;
            set => bezierSegmentsPerCurve = value;
        }

        public Vector2[] Points
        {
            get => m_points;
            set
            {
                if (m_points != value)
                {
                    m_points = value;
                    SetAllDirty();
                }
            }
        }

        protected override void OnPopulateMesh(VertexHelper vh)
        {
            if (m_points == null) return;
            GeneratedUVs();
            var array = m_points;
            if (BezierMode != 0 && BezierMode != BezierType.Catenary && m_points.Length > 3)
            {
                var bezierPath = new BezierPath();
                bezierPath.SetControlPoints(array);
                bezierPath.SegmentsPerCurve = bezierSegmentsPerCurve;
                // array = (BezierMode switch
                // {
                // 	BezierType.Basic => bezierPath.GetDrawingPoints0(), 
                // 	BezierType.Improved => bezierPath.GetDrawingPoints1(), 
                // 	_ => bezierPath.GetDrawingPoints2(), 
                // }).ToArray();

                List<Vector2> drawingPoints;
                switch (BezierMode)
                {
                    case BezierType.Basic:
                        drawingPoints = bezierPath.GetDrawingPoints0();
                        break;
                    case BezierType.Improved:
                        drawingPoints = bezierPath.GetDrawingPoints1();
                        break;
                    default:
                        drawingPoints = bezierPath.GetDrawingPoints2();
                        break;
                }

                array = drawingPoints.ToArray();
            }

            if (BezierMode == BezierType.Catenary && m_points.Length == 2)
            {
                var cableCurve = new CableCurve(array);
                cableCurve.slack = Resoloution;
                cableCurve.steps = BezierSegmentsPerCurve;
                array = cableCurve.Points();
            }

            if (ImproveResolution != 0) array = IncreaseResolution(array);
            var num = relativeSize ? rectTransform.rect.width : 1f;
            var num2 = relativeSize ? rectTransform.rect.height : 1f;
            var num3 = (0f - rectTransform.pivot.x) * num;
            var num4 = (0f - rectTransform.pivot.y) * num2;
            vh.Clear();
            var list = new List<UIVertex[]>();
            if (lineList)
                for (var i = 1; i < array.Length; i += 2)
                {
                    var vector = array[i - 1];
                    var vector2 = array[i];
                    vector = new Vector2(vector.x * num + num3, vector.y * num2 + num4);
                    vector2 = new Vector2(vector2.x * num + num3, vector2.y * num2 + num4);
                    if (lineCaps) list.Add(CreateLineCap(vector, vector2, SegmentType.Start));
                    list.Add(CreateLineSegment(vector, vector2, SegmentType.Middle));
                    if (lineCaps) list.Add(CreateLineCap(vector, vector2, SegmentType.End));
                }
            else
                for (var j = 1; j < array.Length; j++)
                {
                    var vector3 = array[j - 1];
                    var vector4 = array[j];
                    vector3 = new Vector2(vector3.x * num + num3, vector3.y * num2 + num4);
                    vector4 = new Vector2(vector4.x * num + num3, vector4.y * num2 + num4);
                    if (lineCaps && j == 1) list.Add(CreateLineCap(vector3, vector4, SegmentType.Start));
                    list.Add(CreateLineSegment(vector3, vector4, SegmentType.Middle));
                    if (lineCaps && j == array.Length - 1) list.Add(CreateLineCap(vector3, vector4, SegmentType.End));
                }

            for (var k = 0; k < list.Count; k++)
            {
                if (!lineList && k < list.Count - 1)
                {
                    var v = list[k][1].position - list[k][2].position;
                    var v2 = list[k + 1][2].position - list[k + 1][1].position;
                    var num5 = Vector2.Angle(v, v2) * ((float) Math.PI / 180f);
                    var num6 = Mathf.Sign(Vector3.Cross(v.normalized, v2.normalized).z);
                    var num7 = lineThickness / (2f * Mathf.Tan(num5 / 2f));
                    var position = list[k][2].position - v.normalized * num7 * num6;
                    var position2 = list[k][3].position + v.normalized * num7 * num6;
                    var joinType = LineJoins;
                    if (joinType == JoinType.Miter)
                    {
                        if (num7 < v.magnitude / 2f && num7 < v2.magnitude / 2f && num5 > (float) Math.PI / 12f)
                        {
                            list[k][2].position = position;
                            list[k][3].position = position2;
                            list[k + 1][0].position = position2;
                            list[k + 1][1].position = position;
                        }
                        else
                        {
                            joinType = JoinType.Bevel;
                        }
                    }

                    if (joinType == JoinType.Bevel)
                    {
                        if (num7 < v.magnitude / 2f && num7 < v2.magnitude / 2f && num5 > (float) Math.PI / 6f)
                        {
                            if (num6 < 0f)
                            {
                                list[k][2].position = position;
                                list[k + 1][1].position = position;
                            }
                            else
                            {
                                list[k][3].position = position2;
                                list[k + 1][0].position = position2;
                            }
                        }

                        var verts = new UIVertex[4]
                        {
                            list[k][2],
                            list[k][3],
                            list[k + 1][0],
                            list[k + 1][1]
                        };
                        vh.AddUIVertexQuad(verts);
                    }
                }

                vh.AddUIVertexQuad(list[k]);
            }

            if (vh.currentVertCount > 64000)
            {
                Debug.LogError("Max Verticies size is 64000, current mesh vertcies count is [" + vh.currentVertCount +
                               "] - Cannot Draw");
                vh.Clear();
            }
        }

        private UIVertex[] CreateLineCap(Vector2 start, Vector2 end, SegmentType type)
        {
            switch (type)
            {
                case SegmentType.Start:
                {
                    var start2 = start - (end - start).normalized * lineThickness / 2f;
                    return CreateLineSegment(start2, start, SegmentType.Start);
                }
                case SegmentType.End:
                {
                    var end2 = end + (end - start).normalized * lineThickness / 2f;
                    return CreateLineSegment(end, end2, SegmentType.End);
                }
                default:
                    Debug.LogError(
                        "Bad SegmentType passed in to CreateLineCap. Must be SegmentType.Start or SegmentType.End");
                    return null;
            }
        }

        private UIVertex[] CreateLineSegment(Vector2 start, Vector2 end, SegmentType type)
        {
            var b = new Vector2(start.y - end.y, end.x - start.x).normalized * lineThickness / 2f;
            var vector = start - b;
            var vector2 = start + b;
            var vector3 = end + b;
            var vector4 = end - b;
            switch (type)
            {
                case SegmentType.Start:
                    return SetVbo(new Vector2[4] {vector, vector2, vector3, vector4}, startUvs);
                case SegmentType.End:
                    return SetVbo(new Vector2[4] {vector, vector2, vector3, vector4}, endUvs);
                case SegmentType.Full:
                    return SetVbo(new Vector2[4] {vector, vector2, vector3, vector4}, fullUvs);
                default:
                    return SetVbo(new Vector2[4] {vector, vector2, vector3, vector4}, middleUvs);
            }
        }

        protected override void GeneratedUVs()
        {
            if (activeSprite != null)
            {
                var outerUV = DataUtility.GetOuterUV(activeSprite);
                var innerUV = DataUtility.GetInnerUV(activeSprite);
                UV_TOP_LEFT = new Vector2(outerUV.x, outerUV.y);
                UV_BOTTOM_LEFT = new Vector2(outerUV.x, outerUV.w);
                UV_TOP_CENTER_LEFT = new Vector2(innerUV.x, innerUV.y);
                UV_TOP_CENTER_RIGHT = new Vector2(innerUV.z, innerUV.y);
                UV_BOTTOM_CENTER_LEFT = new Vector2(innerUV.x, innerUV.w);
                UV_BOTTOM_CENTER_RIGHT = new Vector2(innerUV.z, innerUV.w);
                UV_TOP_RIGHT = new Vector2(outerUV.z, outerUV.y);
                UV_BOTTOM_RIGHT = new Vector2(outerUV.z, outerUV.w);
            }
            else
            {
                UV_TOP_LEFT = Vector2.zero;
                UV_BOTTOM_LEFT = new Vector2(0f, 1f);
                UV_TOP_CENTER_LEFT = new Vector2(0.5f, 0f);
                UV_TOP_CENTER_RIGHT = new Vector2(0.5f, 0f);
                UV_BOTTOM_CENTER_LEFT = new Vector2(0.5f, 1f);
                UV_BOTTOM_CENTER_RIGHT = new Vector2(0.5f, 1f);
                UV_TOP_RIGHT = new Vector2(1f, 0f);
                UV_BOTTOM_RIGHT = Vector2.one;
            }

            startUvs = new Vector2[4]
            {
                UV_TOP_LEFT,
                UV_BOTTOM_LEFT,
                UV_BOTTOM_CENTER_LEFT,
                UV_TOP_CENTER_LEFT
            };
            middleUvs = new Vector2[4]
            {
                UV_TOP_CENTER_LEFT,
                UV_BOTTOM_CENTER_LEFT,
                UV_BOTTOM_CENTER_RIGHT,
                UV_TOP_CENTER_RIGHT
            };
            endUvs = new Vector2[4]
            {
                UV_TOP_CENTER_RIGHT,
                UV_BOTTOM_CENTER_RIGHT,
                UV_BOTTOM_RIGHT,
                UV_TOP_RIGHT
            };
            fullUvs = new Vector2[4]
            {
                UV_TOP_LEFT,
                UV_BOTTOM_LEFT,
                UV_BOTTOM_RIGHT,
                UV_TOP_RIGHT
            };
        }

        protected override void ResolutionToNativeSize(float distance)
        {
            if (UseNativeSize)
            {
                m_Resolution = distance / (activeSprite.rect.width / pixelsPerUnit);
                lineThickness = activeSprite.rect.height / pixelsPerUnit;
            }
        }

        private enum SegmentType
        {
            Start,
            Middle,
            End,
            Full
        }
    }
}