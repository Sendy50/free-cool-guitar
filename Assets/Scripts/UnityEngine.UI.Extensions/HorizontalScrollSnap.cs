using UnityEngine.EventSystems;

namespace UnityEngine.UI.Extensions
{
    [RequireComponent(typeof(ScrollRect))]
    [AddComponentMenu("Layout/Extensions/Horizontal Scroll Snap")]
    public class HorizontalScrollSnap : ScrollSnapBase, IEndDragHandler, IEventSystemHandler
    {
        private void Start()
        {
            _isVertical = false;
            _childAnchorPoint = new Vector2(0f, 0.5f);
            _currentPage = StartingScreen;
            panelDimensions = gameObject.GetComponent<RectTransform>().rect;
            UpdateLayout();
        }

        private void Update()
        {
            if (!_lerp && _scroll_rect.velocity == Vector2.zero)
            {
                if (!_settled && !_pointerDown && !IsRectSettledOnaPage(_screensContainer.localPosition))
                    ScrollToClosestElement();
                return;
            }

            if (_lerp)
            {
                _screensContainer.localPosition = Vector3.Lerp(_screensContainer.localPosition, _lerp_target,
                    transitionSpeed * Time.deltaTime);
                if (Vector3.Distance(_screensContainer.localPosition, _lerp_target) < 0.1f)
                {
                    _screensContainer.localPosition = _lerp_target;
                    _lerp = false;
                    EndScreenChange();
                }
            }

            CurrentPage = GetPageforPosition(_screensContainer.localPosition);
            if (!_pointerDown && (_scroll_rect.velocity.x > 0.01 || _scroll_rect.velocity.x < 0.01) &&
                IsRectMovingSlowerThanThreshold(0f)) ScrollToClosestElement();
        }

        private void OnEnable()
        {
            InitialiseChildObjectsFromScene();
            DistributePages();
            if ((bool) MaskArea) UpdateVisible();
            if (JumpOnEnable || !RestartOnEnable) SetScrollContainerPosition();
            if (RestartOnEnable) GoToScreen(StartingScreen);
        }

        private void OnRectTransformDimensionsChange()
        {
            if (_childAnchorPoint != Vector2.zero) UpdateLayout();
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            _pointerDown = false;
            if (!_scroll_rect.horizontal) return;
            var num = Vector3.Distance(_startPosition, _screensContainer.localPosition);
            if (UseFastSwipe && num < panelDimensions.width && num >= FastSwipeThreshold)
            {
                _scroll_rect.velocity = Vector3.zero;
                if (_startPosition.x - _screensContainer.localPosition.x > 0f)
                    NextScreen();
                else
                    PreviousScreen();
            }
        }

        private bool IsRectMovingSlowerThanThreshold(float startingSpeed)
        {
            return _scroll_rect.velocity.x > startingSpeed && _scroll_rect.velocity.x < SwipeVelocityThreshold ||
                   _scroll_rect.velocity.x < startingSpeed && _scroll_rect.velocity.x > -SwipeVelocityThreshold;
        }

        private void DistributePages()
        {
            _screens = _screensContainer.childCount;
            _scroll_rect.horizontalNormalizedPosition = 0f;
            var num = 0;
            var num2 = 0f;
            var num3 = 0f;
            var num4 = _childSize = (int) panelDimensions.width * (PageStep != 0f ? PageStep : 3f);
            for (var i = 0; i < _screensContainer.transform.childCount; i++)
            {
                var component = _screensContainer.transform.GetChild(i).gameObject.GetComponent<RectTransform>();
                num3 = num + (int) (i * num4);
                component.sizeDelta = new Vector2(panelDimensions.width, panelDimensions.height);
                component.anchoredPosition = new Vector2(num3, 0f);
                var vector = component.pivot = _childAnchorPoint;
                vector = component.anchorMin = component.anchorMax = vector;
            }

            num2 = num3 + num * -1;
            _screensContainer.offsetMax = new Vector2(num2, 0f);
        }

        public void AddChild(GameObject GO)
        {
            AddChild(GO, false);
        }

        public void AddChild(GameObject GO, bool WorldPositionStays)
        {
            _scroll_rect.horizontalNormalizedPosition = 0f;
            GO.transform.SetParent(_screensContainer, WorldPositionStays);
            InitialiseChildObjectsFromScene();
            DistributePages();
            if ((bool) MaskArea) UpdateVisible();
            SetScrollContainerPosition();
        }

        public void RemoveChild(int index, out GameObject ChildRemoved)
        {
            RemoveChild(index, false, out ChildRemoved);
        }

        public void RemoveChild(int index, bool WorldPositionStays, out GameObject ChildRemoved)
        {
            ChildRemoved = null;
            if (index >= 0 && index <= _screensContainer.childCount)
            {
                _scroll_rect.horizontalNormalizedPosition = 0f;
                var child = _screensContainer.transform.GetChild(index);
                child.SetParent(null, WorldPositionStays);
                ChildRemoved = child.gameObject;
                InitialiseChildObjectsFromScene();
                DistributePages();
                if ((bool) MaskArea) UpdateVisible();
                if (_currentPage > _screens - 1) CurrentPage = _screens - 1;
                SetScrollContainerPosition();
            }
        }

        public void RemoveAllChildren(out GameObject[] ChildrenRemoved)
        {
            RemoveAllChildren(false, out ChildrenRemoved);
        }

        public void RemoveAllChildren(bool WorldPositionStays, out GameObject[] ChildrenRemoved)
        {
            var childCount = _screensContainer.childCount;
            ChildrenRemoved = new GameObject[childCount];
            for (var num = childCount - 1; num >= 0; num--)
            {
                ChildrenRemoved[num] = _screensContainer.GetChild(num).gameObject;
                ChildrenRemoved[num].transform.SetParent(null, WorldPositionStays);
            }

            _scroll_rect.horizontalNormalizedPosition = 0f;
            CurrentPage = 0;
            InitialiseChildObjectsFromScene();
            DistributePages();
            if ((bool) MaskArea) UpdateVisible();
        }

        private void SetScrollContainerPosition()
        {
            _scrollStartPosition = _screensContainer.localPosition.x;
            _scroll_rect.horizontalNormalizedPosition = _currentPage / (float) (_screens - 1);
            OnCurrentScreenChange(_currentPage);
        }

        public void UpdateLayout()
        {
            _lerp = false;
            DistributePages();
            if ((bool) MaskArea) UpdateVisible();
            SetScrollContainerPosition();
            OnCurrentScreenChange(_currentPage);
        }
    }
}