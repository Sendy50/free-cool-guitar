using System.Collections.Generic;

namespace UnityEngine.UI.Extensions
{
    [AddComponentMenu("UI/Effects/Extensions/Letter Spacing")]
    public class LetterSpacing : BaseMeshEffect
    {
        [SerializeField] private float m_spacing;

        protected LetterSpacing()
        {
        }

        public float spacing
        {
            get => m_spacing;
            set
            {
                if (m_spacing != value)
                {
                    m_spacing = value;
                    if (graphic != null) graphic.SetVerticesDirty();
                }
            }
        }

        public override void ModifyMesh(VertexHelper vh)
        {
            if (!IsActive()) return;
            var list = new List<UIVertex>();
            vh.GetUIVertexStream(list);
            var component = GetComponent<Text>();
            if (component == null)
            {
                Debug.LogWarning("LetterSpacing: Missing Text component");
                return;
            }

            var array = component.text.Split('\n');
            var num = spacing * component.fontSize / 100f;
            var num2 = 0f;
            var num3 = 0;
            switch (component.alignment)
            {
                case TextAnchor.UpperLeft:
                case TextAnchor.MiddleLeft:
                case TextAnchor.LowerLeft:
                    num2 = 0f;
                    break;
                case TextAnchor.UpperCenter:
                case TextAnchor.MiddleCenter:
                case TextAnchor.LowerCenter:
                    num2 = 0.5f;
                    break;
                case TextAnchor.UpperRight:
                case TextAnchor.MiddleRight:
                case TextAnchor.LowerRight:
                    num2 = 1f;
                    break;
            }

            foreach (var text in array)
            {
                var num4 = (text.Length - 1) * num * num2;
                for (var j = 0; j < text.Length; j++)
                {
                    var index = num3 * 6;
                    var index2 = num3 * 6 + 1;
                    var index3 = num3 * 6 + 2;
                    var index4 = num3 * 6 + 3;
                    var index5 = num3 * 6 + 4;
                    var num5 = num3 * 6 + 5;
                    if (num5 > list.Count - 1) return;
                    var value = list[index];
                    var value2 = list[index2];
                    var value3 = list[index3];
                    var value4 = list[index4];
                    var value5 = list[index5];
                    var value6 = list[num5];
                    var vector = Vector3.right * (num * j - num4);
                    value.position += vector;
                    value2.position += vector;
                    value3.position += vector;
                    value4.position += vector;
                    value5.position += vector;
                    value6.position += vector;
                    list[index] = value;
                    list[index2] = value2;
                    list[index3] = value3;
                    list[index4] = value4;
                    list[index5] = value5;
                    list[num5] = value6;
                    num3++;
                }

                num3++;
            }

            vh.Clear();
            vh.AddUIVertexTriangleStream(list);
        }
    }
}