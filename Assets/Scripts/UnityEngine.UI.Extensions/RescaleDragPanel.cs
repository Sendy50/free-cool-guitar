using UnityEngine.EventSystems;

namespace UnityEngine.UI.Extensions
{
    [AddComponentMenu("UI/Extensions/RescalePanels/RescaleDragPanel")]
    public class RescaleDragPanel : MonoBehaviour, IPointerDownHandler, IDragHandler, IEventSystemHandler
    {
        private RectTransform canvasRectTransform;

        private Transform goTransform;

        private RectTransform panelRectTransform;
        private Vector2 pointerOffset;

        private void Awake()
        {
            var componentInParent = GetComponentInParent<Canvas>();
            if (componentInParent != null)
            {
                canvasRectTransform = componentInParent.transform as RectTransform;
                panelRectTransform = transform.parent as RectTransform;
                goTransform = transform.parent;
            }
        }

        public void OnDrag(PointerEventData data)
        {
            if (!(panelRectTransform == null))
            {
                var screenPoint = ClampToWindow(data);
                if (RectTransformUtility.ScreenPointToLocalPointInRectangle(canvasRectTransform, screenPoint,
                    data.pressEventCamera, out var localPoint))
                    panelRectTransform.localPosition = localPoint - new Vector2(
                        pointerOffset.x * goTransform.localScale.x, pointerOffset.y * goTransform.localScale.y);
            }
        }

        public void OnPointerDown(PointerEventData data)
        {
            panelRectTransform.SetAsLastSibling();
            RectTransformUtility.ScreenPointToLocalPointInRectangle(panelRectTransform, data.position,
                data.pressEventCamera, out pointerOffset);
        }

        private Vector2 ClampToWindow(PointerEventData data)
        {
            var position = data.position;
            var array = new Vector3[4];
            canvasRectTransform.GetWorldCorners(array);
            var x = Mathf.Clamp(position.x, array[0].x, array[2].x);
            var y = Mathf.Clamp(position.y, array[0].y, array[2].y);
            return new Vector2(x, y);
        }
    }
}