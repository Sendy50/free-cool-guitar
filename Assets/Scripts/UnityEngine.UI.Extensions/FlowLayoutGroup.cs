using System.Collections.Generic;

namespace UnityEngine.UI.Extensions
{
    [AddComponentMenu("Layout/Extensions/Flow Layout Group")]
    public class FlowLayoutGroup : LayoutGroup
    {
        public float SpacingX;

        public float SpacingY;

        public bool ExpandHorizontalSpacing;

        public bool ChildForceExpandWidth;

        public bool ChildForceExpandHeight;

        private readonly IList<RectTransform> _rowList = new List<RectTransform>();

        private float _layoutHeight;

        protected bool IsCenterAlign => childAlignment == TextAnchor.LowerCenter ||
                                        childAlignment == TextAnchor.MiddleCenter ||
                                        childAlignment == TextAnchor.UpperCenter;

        protected bool IsRightAlign => childAlignment == TextAnchor.LowerRight ||
                                       childAlignment == TextAnchor.MiddleRight ||
                                       childAlignment == TextAnchor.UpperRight;

        protected bool IsMiddleAlign => childAlignment == TextAnchor.MiddleLeft ||
                                        childAlignment == TextAnchor.MiddleRight ||
                                        childAlignment == TextAnchor.MiddleCenter;

        protected bool IsLowerAlign => childAlignment == TextAnchor.LowerLeft ||
                                       childAlignment == TextAnchor.LowerRight ||
                                       childAlignment == TextAnchor.LowerCenter;

        public override void CalculateLayoutInputHorizontal()
        {
            base.CalculateLayoutInputHorizontal();
            var totalMin = GetGreatestMinimumChildWidth() + padding.left + padding.right;
            SetLayoutInputForAxis(totalMin, -1f, -1f, 0);
        }

        public override void SetLayoutHorizontal()
        {
            SetLayout(rectTransform.rect.width, 0, false);
        }

        public override void SetLayoutVertical()
        {
            SetLayout(rectTransform.rect.width, 1, false);
        }

        public override void CalculateLayoutInputVertical()
        {
            _layoutHeight = SetLayout(rectTransform.rect.width, 1, true);
        }

        public float SetLayout(float width, int axis, bool layoutInput)
        {
            var height = this.rectTransform.rect.height;
            var num = this.rectTransform.rect.width - padding.left - padding.right;
            var num2 = !IsLowerAlign ? padding.top : (float) padding.bottom;
            var num3 = 0f;
            var num4 = 0f;
            for (var i = 0; i < rectChildren.Count; i++)
            {
                var index = !IsLowerAlign ? i : rectChildren.Count - 1 - i;
                var rectTransform = rectChildren[index];
                var preferredSize = LayoutUtility.GetPreferredSize(rectTransform, 0);
                var preferredSize2 = LayoutUtility.GetPreferredSize(rectTransform, 1);
                preferredSize = Mathf.Min(preferredSize, num);
                if (num3 + preferredSize > num)
                {
                    num3 -= SpacingX;
                    if (!layoutInput)
                    {
                        var yOffset = CalculateRowVerticalOffset(height, num2, num4);
                        LayoutRow(_rowList, num3, num4, num, padding.left, yOffset, axis);
                    }

                    _rowList.Clear();
                    num2 += num4;
                    num2 += SpacingY;
                    num4 = 0f;
                    num3 = 0f;
                }

                num3 += preferredSize;
                _rowList.Add(rectTransform);
                if (preferredSize2 > num4) num4 = preferredSize2;
                if (i < rectChildren.Count - 1) num3 += SpacingX;
            }

            if (!layoutInput)
            {
                var yOffset2 = CalculateRowVerticalOffset(height, num2, num4);
                num3 -= SpacingX;
                LayoutRow(_rowList, num3, num4, num - (_rowList.Count <= 1 ? 0f : SpacingX), padding.left, yOffset2,
                    axis);
            }

            _rowList.Clear();
            num2 += num4;
            num2 += !IsLowerAlign ? padding.bottom : padding.top;
            if (layoutInput && axis == 1) SetLayoutInputForAxis(num2, num2, -1f, axis);
            return num2;
        }

        private float CalculateRowVerticalOffset(float groupHeight, float yOffset, float currentRowHeight)
        {
            if (IsLowerAlign) return groupHeight - yOffset - currentRowHeight;
            if (IsMiddleAlign) return groupHeight * 0.5f - _layoutHeight * 0.5f + yOffset;
            return yOffset;
        }

        protected void LayoutRow(IList<RectTransform> contents, float rowWidth, float rowHeight, float maxWidth,
            float xOffset, float yOffset, int axis)
        {
            var num = xOffset;
            if (!ChildForceExpandWidth && IsCenterAlign)
                num += (maxWidth - rowWidth) * 0.5f;
            else if (!ChildForceExpandWidth && IsRightAlign) num += maxWidth - rowWidth;
            var num2 = 0f;
            var num3 = 0f;
            if (ChildForceExpandWidth)
            {
                num2 = (maxWidth - rowWidth) / _rowList.Count;
            }
            else if (ExpandHorizontalSpacing)
            {
                num3 = (maxWidth - rowWidth) / (_rowList.Count - 1);
                if (_rowList.Count > 1)
                {
                    if (IsCenterAlign)
                        num -= num3 * 0.5f * (_rowList.Count - 1);
                    else if (IsRightAlign) num -= num3 * (_rowList.Count - 1);
                }
            }

            for (var i = 0; i < _rowList.Count; i++)
            {
                var index = !IsLowerAlign ? i : _rowList.Count - 1 - i;
                var rect = _rowList[index];
                var a = LayoutUtility.GetPreferredSize(rect, 0) + num2;
                var num4 = LayoutUtility.GetPreferredSize(rect, 1);
                if (ChildForceExpandHeight) num4 = rowHeight;
                a = Mathf.Min(a, maxWidth);
                var num5 = yOffset;
                if (IsMiddleAlign)
                    num5 += (rowHeight - num4) * 0.5f;
                else if (IsLowerAlign) num5 += rowHeight - num4;
                if (ExpandHorizontalSpacing && i > 0) num += num3;
                if (axis == 0)
                    SetChildAlongAxis(rect, 0, num, a);
                else
                    SetChildAlongAxis(rect, 1, num5, num4);
                if (i < _rowList.Count - 1) num += a + SpacingX;
            }
        }

        public float GetGreatestMinimumChildWidth()
        {
            var num = 0f;
            for (var i = 0; i < rectChildren.Count; i++)
            {
                var minWidth = LayoutUtility.GetMinWidth(rectChildren[i]);
                num = Mathf.Max(minWidth, num);
            }

            return num;
        }
    }
}