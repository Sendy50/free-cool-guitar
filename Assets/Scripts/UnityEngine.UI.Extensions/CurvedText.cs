namespace UnityEngine.UI.Extensions
{
    [RequireComponent(typeof(Text), typeof(RectTransform))]
    [AddComponentMenu("UI/Effects/Extensions/Curved Text")]
    public class CurvedText : BaseMeshEffect
    {
        [SerializeField] private AnimationCurve _curveForText = AnimationCurve.Linear(0f, 0f, 1f, 10f);

        [SerializeField] private float _curveMultiplier = 1f;

        private RectTransform rectTrans;

        public AnimationCurve CurveForText
        {
            get => _curveForText;
            set
            {
                _curveForText = value;
                graphic.SetVerticesDirty();
            }
        }

        public float CurveMultiplier
        {
            get => _curveMultiplier;
            set
            {
                _curveMultiplier = value;
                graphic.SetVerticesDirty();
            }
        }

        protected override void Awake()
        {
            base.Awake();
            rectTrans = GetComponent<RectTransform>();
            OnRectTransformDimensionsChange();
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            rectTrans = GetComponent<RectTransform>();
            OnRectTransformDimensionsChange();
        }

        protected override void OnRectTransformDimensionsChange()
        {
            if ((bool) rectTrans)
            {
                var key = _curveForText[_curveForText.length - 1];
                key.time = rectTrans.rect.width;
                _curveForText.MoveKey(_curveForText.length - 1, key);
            }
        }

        public override void ModifyMesh(VertexHelper vh)
        {
            var currentVertCount = vh.currentVertCount;
            if (IsActive() && currentVertCount != 0)
                for (var i = 0; i < vh.currentVertCount; i++)
                {
                    UIVertex vertex = default;
                    vh.PopulateUIVertex(ref vertex, i);
                    vertex.position.y +=
                        _curveForText.Evaluate(rectTrans.rect.width * rectTrans.pivot.x + vertex.position.x) *
                        _curveMultiplier;
                    vh.SetUIVertex(vertex, i);
                }
        }
    }
}