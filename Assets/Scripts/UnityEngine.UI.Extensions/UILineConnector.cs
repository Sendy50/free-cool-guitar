namespace UnityEngine.UI.Extensions
{
    [AddComponentMenu("UI/Extensions/UI Line Connector")]
    [RequireComponent(typeof(UILineRenderer))]
    [ExecuteInEditMode]
    public class UILineConnector : MonoBehaviour
    {
        public RectTransform[] transforms;

        private RectTransform canvas;

        private UILineRenderer lr;

        private Vector2[] previousPositions;

        private RectTransform rt;

        private void Awake()
        {
            canvas = GetComponentInParent<RectTransform>().GetParentCanvas().GetComponent<RectTransform>();
            rt = GetComponent<RectTransform>();
            lr = GetComponent<UILineRenderer>();
        }

        private void Update()
        {
            if (transforms == null || transforms.Length < 1) return;
            if (previousPositions != null && previousPositions.Length == transforms.Length)
            {
                var flag = false;
                for (var i = 0; i < transforms.Length; i++)
                    if (!flag && previousPositions[i] != transforms[i].anchoredPosition)
                        flag = true;
                if (!flag) return;
            }

            var pivot = rt.pivot;
            var pivot2 = canvas.pivot;
            var array = new Vector3[transforms.Length];
            var array2 = new Vector3[transforms.Length];
            var array3 = new Vector2[transforms.Length];
            for (var j = 0; j < transforms.Length; j++)
            {
                ref var reference = ref array[j];
                reference = transforms[j].TransformPoint(pivot);
            }

            for (var k = 0; k < transforms.Length; k++)
            {
                ref var reference2 = ref array2[k];
                reference2 = canvas.InverseTransformPoint(array[k]);
            }

            for (var l = 0; l < transforms.Length; l++)
            {
                ref var reference3 = ref array3[l];
                reference3 = new Vector2(array2[l].x, array2[l].y);
            }

            lr.Points = array3;
            lr.RelativeSize = false;
            lr.drivenExternally = true;
            previousPositions = new Vector2[transforms.Length];
            for (var m = 0; m < transforms.Length; m++)
            {
                ref var reference4 = ref previousPositions[m];
                reference4 = transforms[m].anchoredPosition;
            }
        }
    }
}