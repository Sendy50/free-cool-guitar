using UnityEngine.EventSystems;

namespace UnityEngine.UI.Extensions
{
    [AddComponentMenu("Event/VR Input Module")]
    public class VRInputModule : BaseInputModule
    {
        public static GameObject targetObject;

        private static VRInputModule _singleton;

        private static bool mouseClicked;

        public static Vector3 cursorPosition;

        private int counter;

        protected override void Awake()
        {
            _singleton = this;
        }

        public override void Process()
        {
            if (targetObject == null) mouseClicked = false;
        }

        public static void PointerSubmit(GameObject obj)
        {
            targetObject = obj;
            mouseClicked = true;
            if (mouseClicked)
            {
                var baseEventData = new BaseEventData(_singleton.eventSystem);
                baseEventData.selectedObject = targetObject;
                ExecuteEvents.Execute(targetObject, baseEventData, ExecuteEvents.submitHandler);
                print("clicked " + targetObject.name);
                mouseClicked = false;
            }
        }

        public static void PointerExit(GameObject obj)
        {
            print("PointerExit " + obj.name);
            var eventData = new PointerEventData(_singleton.eventSystem);
            ExecuteEvents.Execute(obj, eventData, ExecuteEvents.pointerExitHandler);
            ExecuteEvents.Execute(obj, eventData, ExecuteEvents.deselectHandler);
        }

        public static void PointerEnter(GameObject obj)
        {
            print("PointerEnter " + obj.name);
            var pointerEventData = new PointerEventData(_singleton.eventSystem);
            pointerEventData.pointerEnter = obj;
            RaycastResult raycastResult = default;
            raycastResult.worldPosition = cursorPosition;
            var raycastResult3 = pointerEventData.pointerCurrentRaycast = raycastResult;
            ExecuteEvents.Execute(obj, pointerEventData, ExecuteEvents.pointerEnterHandler);
        }
    }
}