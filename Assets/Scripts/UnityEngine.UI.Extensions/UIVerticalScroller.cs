using UnityEngine.Events;

namespace UnityEngine.UI.Extensions
{
    [RequireComponent(typeof(ScrollRect))]
    [AddComponentMenu("Layout/Extensions/Vertical Scroller")]
    public class UIVerticalScroller : MonoBehaviour
    {
        [Tooltip("Scrollable area (content of desired ScrollRect)")]
        public RectTransform _scrollingPanel;

        [Tooltip("Elements to populate inside the scroller")]
        public GameObject[] _arrayOfElements;

        [Tooltip("Center display area (position of zoomed content)")]
        public RectTransform _center;

        [Tooltip("Select the item to be in center on start. (optional)")]
        public int StartingIndex = -1;

        [Tooltip("Button to go to the next page. (optional)")]
        public GameObject ScrollUpButton;

        [Tooltip("Button to go to the previous page. (optional)")]
        public GameObject ScrollDownButton;

        [Tooltip("Event fired when a specific item is clicked, exposes index number of item. (optional)")]
        public UnityEvent<int> ButtonClicked;

        private float deltaY;

        private float[] distance;

        private float[] distReposition;

        private int elementLength;

        private int minElementsNum;

        private string result;

        public UIVerticalScroller()
        {
        }

        public UIVerticalScroller(RectTransform scrollingPanel, GameObject[] arrayOfElements, RectTransform center)
        {
            _scrollingPanel = scrollingPanel;
            _arrayOfElements = arrayOfElements;
            _center = center;
        }

        public void Awake()
        {
            var component = GetComponent<ScrollRect>();
            if (!_scrollingPanel) _scrollingPanel = component.content;
            if (!_center)
                Debug.LogError("Please define the RectTransform for the Center viewport of the scrollable area");
            if (_arrayOfElements != null && _arrayOfElements.Length != 0) return;
            var childCount = component.content.childCount;
            if (childCount > 0)
            {
                _arrayOfElements = new GameObject[childCount];
                for (var i = 0; i < childCount; i++) _arrayOfElements[i] = component.content.GetChild(i).gameObject;
            }
        }

        public void Start()
        {
            if (_arrayOfElements.Length < 1)
            {
                Debug.Log("No child content found, exiting..");
                return;
            }

            elementLength = _arrayOfElements.Length;
            distance = new float[elementLength];
            distReposition = new float[elementLength];
            deltaY = _arrayOfElements[0].GetComponent<RectTransform>().rect.height * elementLength / 3f * 2f;
            var anchoredPosition = new Vector2(_scrollingPanel.anchoredPosition.x, 0f - deltaY);
            _scrollingPanel.anchoredPosition = anchoredPosition;
            for (var i = 0; i < _arrayOfElements.Length; i++) AddListener(_arrayOfElements[i], i);
            if ((bool) ScrollUpButton)
                ScrollUpButton.GetComponent<Button>().onClick.AddListener(delegate { ScrollUp(); });
            if ((bool) ScrollDownButton)
                ScrollDownButton.GetComponent<Button>().onClick.AddListener(delegate { ScrollDown(); });
            if (StartingIndex > -1)
            {
                StartingIndex = StartingIndex <= _arrayOfElements.Length ? StartingIndex : _arrayOfElements.Length - 1;
                SnapToElement(StartingIndex);
            }
        }

        public void Update()
        {
            if (_arrayOfElements.Length < 1) return;
            for (var i = 0; i < elementLength; i++)
            {
                distReposition[i] = _center.GetComponent<RectTransform>().position.y -
                                    _arrayOfElements[i].GetComponent<RectTransform>().position.y;
                distance[i] = Mathf.Abs(distReposition[i]);
                var num = Mathf.Max(0.7f, 1f / (1f + distance[i] / 200f));
                _arrayOfElements[i].GetComponent<RectTransform>().transform.localScale = new Vector3(num, num, 1f);
            }

            var num2 = Mathf.Min(distance);
            for (var j = 0; j < elementLength; j++)
            {
                _arrayOfElements[j].GetComponent<CanvasGroup>().interactable = false;
                if (num2 == distance[j])
                {
                    minElementsNum = j;
                    _arrayOfElements[j].GetComponent<CanvasGroup>().interactable = true;
                    result = _arrayOfElements[j].GetComponentInChildren<Text>().text;
                }
            }

            ScrollingElements(0f - _arrayOfElements[minElementsNum].GetComponent<RectTransform>().anchoredPosition.y);
        }

        private void AddListener(GameObject button, int index)
        {
            button.GetComponent<Button>().onClick.AddListener(delegate { DoSomething(index); });
        }

        private void DoSomething(int index)
        {
            if (ButtonClicked != null) ButtonClicked.Invoke(index);
        }

        private void ScrollingElements(float position)
        {
            var y = Mathf.Lerp(_scrollingPanel.anchoredPosition.y, position, Time.deltaTime * 1f);
            var anchoredPosition = new Vector2(_scrollingPanel.anchoredPosition.x, y);
            _scrollingPanel.anchoredPosition = anchoredPosition;
        }

        public string GetResults()
        {
            return result;
        }

        public void SnapToElement(int element)
        {
            var num = _arrayOfElements[0].GetComponent<RectTransform>().rect.height * element;
            var anchoredPosition = new Vector2(_scrollingPanel.anchoredPosition.x, 0f - num);
            _scrollingPanel.anchoredPosition = anchoredPosition;
        }

        public void ScrollUp()
        {
            var num = _arrayOfElements[0].GetComponent<RectTransform>().rect.height / 1.2f;
            var b = new Vector2(_scrollingPanel.anchoredPosition.x, _scrollingPanel.anchoredPosition.y - num);
            _scrollingPanel.anchoredPosition = Vector2.Lerp(_scrollingPanel.anchoredPosition, b, 1f);
        }

        public void ScrollDown()
        {
            var num = _arrayOfElements[0].GetComponent<RectTransform>().rect.height / 1.2f;
            var anchoredPosition =
                new Vector2(_scrollingPanel.anchoredPosition.x, _scrollingPanel.anchoredPosition.y + num);
            _scrollingPanel.anchoredPosition = anchoredPosition;
        }
    }
}