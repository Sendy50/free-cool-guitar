using System;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace UnityEngine.UI.Extensions
{
    [AddComponentMenu("UI/Extensions/Segmented Control")]
    [RequireComponent(typeof(RectTransform))]
    public class SegmentedControl : UIBehaviour
    {
        [SerializeField]
        [Tooltip(
            "A GameObject with an Image to use as a separator between segments. Size of the RectTransform will determine the size of the separator used.\nNote, make sure to disable the separator GO so that it does not affect the scene")]
        private Graphic m_separator;

        [SerializeField] [Tooltip("When True, it allows each button to be toggled on/off")]
        private bool m_allowSwitchingOff;

        [SerializeField] [Tooltip("The selected default for the control (zero indexed array)")]
        private int m_selectedSegmentIndex = -1;

        [SerializeField] [Tooltip("Event to fire once the selection has been changed")]
        private SegmentSelectedEvent m_onValueChanged = new SegmentSelectedEvent();

        [SerializeField] public Color selectedColor;

        private Selectable[] m_segments;

        private float m_separatorWidth;

        protected internal Selectable selectedSegment;

        protected SegmentedControl()
        {
        }

        protected float SeparatorWidth
        {
            get
            {
                if (m_separatorWidth == 0f && (bool) separator)
                {
                    m_separatorWidth = separator.rectTransform.rect.width;
                    var component = separator.GetComponent<Image>();
                    if ((bool) component) m_separatorWidth /= component.pixelsPerUnit;
                }

                return m_separatorWidth;
            }
        }

        public Selectable[] segments
        {
            get
            {
                if (m_segments == null || m_segments.Length == 0) m_segments = GetChildSegments();
                return m_segments;
            }
        }

        public Graphic separator
        {
            get => m_separator;
            set
            {
                m_separator = value;
                m_separatorWidth = 0f;
                LayoutSegments();
            }
        }

        public bool allowSwitchingOff
        {
            get => m_allowSwitchingOff;
            set => m_allowSwitchingOff = value;
        }

        public int selectedSegmentIndex
        {
            get => Array.IndexOf(segments, selectedSegment);
            set
            {
                value = Math.Max(value, -1);
                value = Math.Min(value, segments.Length - 1);
                m_selectedSegmentIndex = value;
                if (value == -1)
                {
                    if ((bool) selectedSegment)
                    {
                        selectedSegment.GetComponent<Segment>().selected = false;
                        selectedSegment = null;
                    }
                }
                else
                {
                    segments[value].GetComponent<Segment>().selected = true;
                }
            }
        }

        public SegmentSelectedEvent onValueChanged
        {
            get => m_onValueChanged;
            set => m_onValueChanged = value;
        }

        protected override void Start()
        {
            base.Start();
            LayoutSegments();
            if (m_selectedSegmentIndex != -1) selectedSegmentIndex = m_selectedSegmentIndex;
        }

        private Selectable[] GetChildSegments()
        {
            var componentsInChildren = GetComponentsInChildren<Selectable>();
            if (componentsInChildren.Length < 2)
                throw new InvalidOperationException("A segmented control must have at least two Button children");
            for (var i = 0; i < componentsInChildren.Length; i++)
            {
                var segment = componentsInChildren[i].GetComponent<Segment>();
                if (segment == null) segment = componentsInChildren[i].gameObject.AddComponent<Segment>();
                segment.index = i;
            }

            return componentsInChildren;
        }

        public void SetAllSegmentsOff()
        {
            selectedSegment = null;
        }

        private void RecreateSprites()
        {
            for (var i = 0; i < segments.Length; i++)
            {
                if (segments[i].image == null) continue;
                var sprite = segments[i].image.sprite;
                if (sprite.border.x != 0f && sprite.border.z != 0f)
                {
                    var rect = sprite.rect;
                    var border = sprite.border;
                    if (i > 0)
                    {
                        rect.xMin = border.x;
                        border.x = 0f;
                    }

                    if (i < segments.Length - 1)
                    {
                        rect.xMax = border.z;
                        border.z = 0f;
                    }

                    segments[i].image.sprite = Sprite.Create(sprite.texture, rect, sprite.pivot, sprite.pixelsPerUnit,
                        0u, SpriteMeshType.FullRect, border);
                }
            }
        }

        public void LayoutSegments()
        {
            RecreateSprites();
            var rectTransform = this.transform as RectTransform;
            var num = rectTransform.rect.width / segments.Length - SeparatorWidth * (segments.Length - 1);
            for (var i = 0; i < segments.Length; i++)
            {
                var num2 = (num + SeparatorWidth) * i;
                var component = segments[i].GetComponent<RectTransform>();
                component.anchorMin = Vector2.zero;
                component.anchorMax = Vector2.zero;
                component.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, num2, num);
                component.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Top, 0f, rectTransform.rect.height);
                if ((bool) separator && i > 0)
                {
                    var transform = gameObject.transform.Find("Separator " + i);
                    var graphic = !(transform != null)
                        ? Instantiate(separator.gameObject).GetComponent<Graphic>()
                        : transform.GetComponent<Graphic>();
                    graphic.gameObject.name = "Separator " + i;
                    graphic.gameObject.SetActive(true);
                    graphic.rectTransform.SetParent(this.transform, false);
                    graphic.rectTransform.anchorMin = Vector2.zero;
                    graphic.rectTransform.anchorMax = Vector2.zero;
                    graphic.rectTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, num2 - SeparatorWidth,
                        SeparatorWidth);
                    graphic.rectTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Top, 0f,
                        rectTransform.rect.height);
                }
            }
        }

        [Serializable]
        public class SegmentSelectedEvent : UnityEvent<int>
        {
        }
    }
}