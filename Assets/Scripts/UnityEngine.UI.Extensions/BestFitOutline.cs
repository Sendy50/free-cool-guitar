using System.Collections.Generic;

namespace UnityEngine.UI.Extensions
{
    [AddComponentMenu("UI/Effects/Extensions/BestFit Outline")]
    public class BestFitOutline : Shadow
    {
        protected BestFitOutline()
        {
        }

        public override void ModifyMesh(Mesh mesh)
        {
            if (IsActive())
            {
                var list = new List<UIVertex>();
                using (var vertexHelper = new VertexHelper(mesh))
                {
                    vertexHelper.GetUIVertexStream(list);
                }

                var component = GetComponent<Text>();
                var num = 1f;
                if ((bool) component && component.resizeTextForBestFit)
                    num = component.cachedTextGenerator.fontSizeUsedForBestFit /
                          (float) (component.resizeTextMaxSize - 1);
                var start = 0;
                var count = list.Count;
                ApplyShadowZeroAlloc(list, effectColor, start, list.Count, effectDistance.x * num,
                    effectDistance.y * num);
                start = count;
                count = list.Count;
                ApplyShadowZeroAlloc(list, effectColor, start, list.Count, effectDistance.x * num,
                    (0f - effectDistance.y) * num);
                start = count;
                count = list.Count;
                ApplyShadowZeroAlloc(list, effectColor, start, list.Count, (0f - effectDistance.x) * num,
                    effectDistance.y * num);
                start = count;
                count = list.Count;
                ApplyShadowZeroAlloc(list, effectColor, start, list.Count, (0f - effectDistance.x) * num,
                    (0f - effectDistance.y) * num);
                var vertexHelper2 = new VertexHelper();
                vertexHelper2.AddUIVertexTriangleStream(list);
                vertexHelper2.FillMesh(mesh);
            }
        }
    }
}