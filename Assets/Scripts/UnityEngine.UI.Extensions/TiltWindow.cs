namespace UnityEngine.UI.Extensions
{
    public class TiltWindow : MonoBehaviour
    {
        public Vector2 range = new Vector2(5f, 3f);

        private Vector2 mRot = Vector2.zero;

        private Quaternion mStart;

        private Transform mTrans;

        private void Start()
        {
            mTrans = transform;
            mStart = mTrans.localRotation;
        }

        private void Update()
        {
            var mousePosition = Input.mousePosition;
            var num = Screen.width * 0.5f;
            var num2 = Screen.height * 0.5f;
            var x = Mathf.Clamp((mousePosition.x - num) / num, -1f, 1f);
            var y = Mathf.Clamp((mousePosition.y - num2) / num2, -1f, 1f);
            mRot = Vector2.Lerp(mRot, new Vector2(x, y), Time.deltaTime * 5f);
            mTrans.localRotation = mStart * Quaternion.Euler((0f - mRot.y) * range.y, mRot.x * range.x, 0f);
        }
    }
}