namespace UnityEngine.UI.Extensions
{
    [AddComponentMenu("UI/Extensions/HoverTooltip")]
    public class HoverTooltip : MonoBehaviour
    {
        public int horizontalPadding;

        public int verticalPadding;

        public Text thisText;

        public HorizontalLayoutGroup hlG;

        public RectTransform bgImage;

        private Image bgImageSource;

        private float currentXScaleFactor;

        private float currentYScaleFactor;

        private float defaultXOffset;

        private float defaultYOffset;

        private bool firstUpdate;

        private Camera GUICamera;

        private RenderMode GUIMode;

        private bool inside;

        private Vector3 lowerLeft;

        private float tooltipRealHeight;

        private float tooltipRealWidth;

        private Vector3 upperRight;

        private void Start()
        {
            GUICamera = GameObject.Find("GUICamera").GetComponent<Camera>();
            GUIMode = transform.parent.parent.GetComponent<Canvas>().renderMode;
            bgImageSource = bgImage.GetComponent<Image>();
            inside = false;
            HideTooltipVisibility();
            transform.parent.gameObject.SetActive(false);
        }

        private void Update()
        {
            LayoutInit();
            if (inside && GUIMode == RenderMode.ScreenSpaceCamera) OnScreenSpaceCamera();
        }

        public void SetTooltip(string text)
        {
            NewTooltip();
            thisText.text = text;
            OnScreenSpaceCamera();
        }

        public void SetTooltip(string[] texts)
        {
            NewTooltip();
            var text = string.Empty;
            var num = 0;
            foreach (var text2 in texts)
            {
                text = num != 0 ? text + "\n" + text2 : text + text2;
                num++;
            }

            thisText.text = text;
            OnScreenSpaceCamera();
        }

        public void SetTooltip(string text, bool test)
        {
            NewTooltip();
            thisText.text = text;
            OnScreenSpaceCamera();
        }

        public void OnScreenSpaceCamera()
        {
            var position = GUICamera.ScreenToViewportPoint(Input.mousePosition);
            var num = 0f;
            var num2 = 0f;
            var num3 = GUICamera.ViewportToScreenPoint(position).x + tooltipRealWidth * bgImage.pivot.x;
            if (num3 > upperRight.x)
            {
                var num4 = upperRight.x - num3;
                num2 = !(num4 > defaultXOffset * 0.75) ? defaultXOffset - tooltipRealWidth * 2f : num4;
                var position2 = new Vector3(GUICamera.ViewportToScreenPoint(position).x + num2, 0f, 0f);
                position.x = GUICamera.ScreenToViewportPoint(position2).x;
            }

            num3 = GUICamera.ViewportToScreenPoint(position).x - tooltipRealWidth * bgImage.pivot.x;
            if (num3 < lowerLeft.x)
            {
                var num5 = lowerLeft.x - num3;
                num2 = !(num5 < defaultXOffset * 0.75 - tooltipRealWidth) ? tooltipRealWidth * 2f : 0f - num5;
                var position3 = new Vector3(GUICamera.ViewportToScreenPoint(position).x - num2, 0f, 0f);
                position.x = GUICamera.ScreenToViewportPoint(position3).x;
            }

            num3 = GUICamera.ViewportToScreenPoint(position).y -
                   (bgImage.sizeDelta.y * currentYScaleFactor * bgImage.pivot.y - tooltipRealHeight);
            if (num3 > upperRight.y)
            {
                var num6 = upperRight.y - num3;
                num = bgImage.sizeDelta.y * currentYScaleFactor * bgImage.pivot.y;
                num = !(num6 > defaultYOffset * 0.75) ? defaultYOffset - tooltipRealHeight * 2f : num6;
                var position4 = new Vector3(position.x, GUICamera.ViewportToScreenPoint(position).y + num, 0f);
                position.y = GUICamera.ScreenToViewportPoint(position4).y;
            }

            num3 = GUICamera.ViewportToScreenPoint(position).y -
                   bgImage.sizeDelta.y * currentYScaleFactor * bgImage.pivot.y;
            if (num3 < lowerLeft.y)
            {
                var num7 = lowerLeft.y - num3;
                num = bgImage.sizeDelta.y * currentYScaleFactor * bgImage.pivot.y;
                num = !(num7 < defaultYOffset * 0.75 - tooltipRealHeight) ? tooltipRealHeight * 2f : num7;
                var position5 = new Vector3(position.x, GUICamera.ViewportToScreenPoint(position).y + num, 0f);
                position.y = GUICamera.ScreenToViewportPoint(position5).y;
            }

            transform.parent.transform.position = new Vector3(GUICamera.ViewportToWorldPoint(position).x,
                GUICamera.ViewportToWorldPoint(position).y, 0f);
            transform.parent.gameObject.SetActive(true);
            inside = true;
        }

        public void HideTooltip()
        {
            if (GUIMode == RenderMode.ScreenSpaceCamera && this != null)
            {
                transform.parent.gameObject.SetActive(false);
                inside = false;
                HideTooltipVisibility();
            }
        }

        private void LayoutInit()
        {
            if (firstUpdate)
            {
                firstUpdate = false;
                bgImage.sizeDelta = new Vector2(hlG.preferredWidth + horizontalPadding,
                    hlG.preferredHeight + verticalPadding);
                defaultYOffset = bgImage.sizeDelta.y * currentYScaleFactor * bgImage.pivot.y;
                defaultXOffset = bgImage.sizeDelta.x * currentXScaleFactor * bgImage.pivot.x;
                tooltipRealHeight = bgImage.sizeDelta.y * currentYScaleFactor;
                tooltipRealWidth = bgImage.sizeDelta.x * currentXScaleFactor;
                ActivateTooltipVisibility();
            }
        }

        private void NewTooltip()
        {
            firstUpdate = true;
            lowerLeft = GUICamera.ViewportToScreenPoint(new Vector3(0f, 0f, 0f));
            upperRight = GUICamera.ViewportToScreenPoint(new Vector3(1f, 1f, 0f));
            currentYScaleFactor = Screen.height / transform.root.GetComponent<CanvasScaler>().referenceResolution.y;
            currentXScaleFactor = Screen.width / transform.root.GetComponent<CanvasScaler>().referenceResolution.x;
        }

        public void ActivateTooltipVisibility()
        {
            var color = thisText.color;
            thisText.color = new Color(color.r, color.g, color.b, 1f);
            bgImageSource.color = new Color(bgImageSource.color.r, bgImageSource.color.g, bgImageSource.color.b, 0.8f);
        }

        public void HideTooltipVisibility()
        {
            var color = thisText.color;
            thisText.color = new Color(color.r, color.g, color.b, 0f);
            bgImageSource.color = new Color(bgImageSource.color.r, bgImageSource.color.g, bgImageSource.color.b, 0f);
        }
    }
}