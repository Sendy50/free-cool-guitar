namespace UnityEngine.UI.Extensions
{
    [AddComponentMenu("UI/Extensions/Bound Tooltip/Tooltip Item")]
    public class BoundTooltipItem : MonoBehaviour
    {
        private static BoundTooltipItem instance;
        public Text TooltipText;

        public Vector3 ToolTipOffset;

        public bool IsActive => gameObject.activeSelf;

        public static BoundTooltipItem Instance
        {
            get
            {
                if (instance == null) instance = FindObjectOfType<BoundTooltipItem>();
                return instance;
            }
        }

        private void Awake()
        {
            instance = this;
            if (!TooltipText) TooltipText = GetComponentInChildren<Text>();
            HideTooltip();
        }

        public void ShowTooltip(string text, Vector3 pos)
        {
            if (TooltipText.text != text) TooltipText.text = text;
            transform.position = pos + ToolTipOffset;
            gameObject.SetActive(true);
        }

        public void HideTooltip()
        {
            gameObject.SetActive(false);
        }
    }
}