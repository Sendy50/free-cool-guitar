namespace UnityEngine.UI.Extensions
{
    [RequireComponent(typeof(InputField))]
    [AddComponentMenu("UI/Extensions/InputFocus")]
    public class InputFocus : MonoBehaviour
    {
        public bool _ignoreNextActivation;
        protected InputField _inputField;

        private void Start()
        {
            _inputField = GetComponent<InputField>();
        }

        private void Update()
        {
            if (Input.GetKeyUp(KeyCode.Return) && !_inputField.isFocused)
            {
                if (_ignoreNextActivation)
                {
                    _ignoreNextActivation = false;
                    return;
                }

                _inputField.Select();
                _inputField.ActivateInputField();
            }
        }

        public void buttonPressed()
        {
            var flag = _inputField.text == string.Empty;
            _inputField.text = string.Empty;
            if (!flag)
            {
                _inputField.Select();
                _inputField.ActivateInputField();
            }
        }

        public void OnEndEdit(string textString)
        {
            if (Input.GetKeyDown(KeyCode.Return))
            {
                var flag = _inputField.text == string.Empty;
                _inputField.text = string.Empty;
                if (flag) _ignoreNextActivation = true;
            }
        }
    }
}