namespace UnityEngine.UI.Extensions
{
    public static class UIExtensionMethods
    {
        public static Canvas GetParentCanvas(this RectTransform rt)
        {
            var rectTransform = rt;
            var canvas = rt.GetComponent<Canvas>();
            var num = 0;
            while (canvas == null || num > 50)
            {
                canvas = rt.GetComponentInParent<Canvas>();
                if (canvas == null)
                {
                    rectTransform = rectTransform.parent.GetComponent<RectTransform>();
                    num++;
                }
            }

            return canvas;
        }
    }
}