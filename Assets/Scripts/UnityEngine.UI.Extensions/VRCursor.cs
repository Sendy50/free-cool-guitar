namespace UnityEngine.UI.Extensions
{
    [AddComponentMenu("UI/Extensions/VR Cursor")]
    public class VRCursor : MonoBehaviour
    {
        public float xSens;

        public float ySens;

        private Collider currentCollider;

        private void Update()
        {
            Vector3 position = default;
            position.x = Input.mousePosition.x * xSens;
            position.y = Input.mousePosition.y * ySens - 1f;
            position.z = transform.position.z;
            transform.position = position;
            VRInputModule.cursorPosition = transform.position;
            if (Input.GetMouseButtonDown(0) && (bool) currentCollider)
                VRInputModule.PointerSubmit(currentCollider.gameObject);
        }

        private void OnTriggerEnter(Collider other)
        {
            VRInputModule.PointerEnter(other.gameObject);
            currentCollider = other;
        }

        private void OnTriggerExit(Collider other)
        {
            VRInputModule.PointerExit(other.gameObject);
            currentCollider = null;
        }
    }
}