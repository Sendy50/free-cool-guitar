namespace UnityEngine.UI.Extensions
{
    public class ExampleSelectable : MonoBehaviour, IBoxSelectable
    {
        private Image image;

        private SpriteRenderer spriteRenderer;

        private Text text;

        private void Start()
        {
            spriteRenderer = transform.GetComponent<SpriteRenderer>();
            image = transform.GetComponent<Image>();
            text = transform.GetComponent<Text>();
        }

        private void Update()
        {
            var color = Color.white;
            if (preSelected) color = Color.yellow;
            if (selected) color = Color.green;
            if ((bool) spriteRenderer)
                spriteRenderer.color = color;
            else if ((bool) text)
                text.color = color;
            else if ((bool) image)
                image.color = color;
            else if ((bool) GetComponent<Renderer>()) GetComponent<Renderer>().material.color = color;
        }

        public bool selected { get; set; }

        public bool preSelected { get; set; }

        // public Transform get_transform()
        // {
        // 	return base.transform;
        // }
    }
}