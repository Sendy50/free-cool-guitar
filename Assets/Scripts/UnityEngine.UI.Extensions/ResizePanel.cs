using UnityEngine.EventSystems;

namespace UnityEngine.UI.Extensions
{
    [AddComponentMenu("UI/Extensions/RescalePanels/ResizePanel")]
    public class ResizePanel : MonoBehaviour, IPointerDownHandler, IDragHandler, IEventSystemHandler
    {
        public Vector2 minSize;

        public Vector2 maxSize;

        private Vector2 currentPointerPosition;

        private Vector2 previousPointerPosition;

        private float ratio;

        private RectTransform rectTransform;

        private void Awake()
        {
            rectTransform = transform.parent.GetComponent<RectTransform>();
            var width = rectTransform.rect.width;
            var height = rectTransform.rect.height;
            ratio = height / width;
            minSize = new Vector2(0.1f * width, 0.1f * height);
            maxSize = new Vector2(10f * width, 10f * height);
        }

        public void OnDrag(PointerEventData data)
        {
            if (!(rectTransform == null))
            {
                var sizeDelta = rectTransform.sizeDelta;
                RectTransformUtility.ScreenPointToLocalPointInRectangle(rectTransform, data.position,
                    data.pressEventCamera, out currentPointerPosition);
                var vector = currentPointerPosition - previousPointerPosition;
                sizeDelta += new Vector2(vector.x, ratio * vector.x);
                sizeDelta = new Vector2(Mathf.Clamp(sizeDelta.x, minSize.x, maxSize.x),
                    Mathf.Clamp(sizeDelta.y, minSize.y, maxSize.y));
                rectTransform.sizeDelta = sizeDelta;
                previousPointerPosition = currentPointerPosition;
            }
        }

        public void OnPointerDown(PointerEventData data)
        {
            rectTransform.SetAsLastSibling();
            RectTransformUtility.ScreenPointToLocalPointInRectangle(rectTransform, data.position, data.pressEventCamera,
                out previousPointerPosition);
        }
    }
}