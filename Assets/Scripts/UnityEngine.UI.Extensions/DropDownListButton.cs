namespace UnityEngine.UI.Extensions
{
    [RequireComponent(typeof(RectTransform), typeof(Button))]
    public class DropDownListButton
    {
        public Button btn;

        public Image btnImg;

        public GameObject gameobject;

        public Image img;
        public RectTransform rectTransform;

        public Text txt;

        public DropDownListButton(GameObject btnObj)
        {
            gameobject = btnObj;
            rectTransform = btnObj.GetComponent<RectTransform>();
            btnImg = btnObj.GetComponent<Image>();
            btn = btnObj.GetComponent<Button>();
            txt = rectTransform.Find("Text").GetComponent<Text>();
            img = rectTransform.Find("Image").GetComponent<Image>();
        }
    }
}