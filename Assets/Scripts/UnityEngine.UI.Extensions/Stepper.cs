using System;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace UnityEngine.UI.Extensions
{
    [AddComponentMenu("UI/Extensions/Stepper")]
    [RequireComponent(typeof(RectTransform))]
    public class Stepper : UIBehaviour
    {
        [SerializeField] [Tooltip("The current step value of the control")]
        private int _value;

        [SerializeField]
        [Tooltip("The minimum step value allowed by the control. When reached it will disable the '-' button")]
        private int _minimum;

        [SerializeField]
        [Tooltip("The maximum step value allowed by the control. When reached it will disable the '+' button")]
        private int _maximum = 100;

        [SerializeField] [Tooltip("The step increment used to increment / decrement the step value")]
        private int _step = 1;

        [SerializeField] [Tooltip("Does the step value loop around from end to end")]
        private bool _wrap;

        [SerializeField]
        [Tooltip(
            "A GameObject with an Image to use as a separator between segments. Size of the RectTransform will determine the size of the separator used.\nNote, make sure to disable the separator GO so that it does not affect the scene")]
        private Graphic _separator;

        [SerializeField] private StepperValueChangedEvent _onValueChanged = new StepperValueChangedEvent();

        private float _separatorWidth;

        private Selectable[] _sides;

        protected Stepper()
        {
        }

        private float separatorWidth
        {
            get
            {
                if (_separatorWidth == 0f && (bool) separator)
                {
                    _separatorWidth = separator.rectTransform.rect.width;
                    var component = separator.GetComponent<Image>();
                    if ((bool) component) _separatorWidth /= component.pixelsPerUnit;
                }

                return _separatorWidth;
            }
        }

        public Selectable[] sides
        {
            get
            {
                if (_sides == null || _sides.Length == 0) _sides = GetSides();
                return _sides;
            }
        }

        public int value
        {
            get => _value;
            set => _value = value;
        }

        public int minimum
        {
            get => _minimum;
            set => _minimum = value;
        }

        public int maximum
        {
            get => _maximum;
            set => _maximum = value;
        }

        public int step
        {
            get => _step;
            set => _step = value;
        }

        public bool wrap
        {
            get => _wrap;
            set => _wrap = value;
        }

        public Graphic separator
        {
            get => _separator;
            set
            {
                _separator = value;
                _separatorWidth = 0f;
                LayoutSides(sides);
            }
        }

        public StepperValueChangedEvent onValueChanged
        {
            get => _onValueChanged;
            set => _onValueChanged = value;
        }

        private Selectable[] GetSides()
        {
            var componentsInChildren = GetComponentsInChildren<Selectable>();
            if (componentsInChildren.Length != 2)
                throw new InvalidOperationException("A stepper must have two Button children");
            for (var i = 0; i < 2; i++)
            {
                var component = componentsInChildren[i].GetComponent<StepperSide>();
                if (component == null) component = componentsInChildren[i].gameObject.AddComponent<StepperSide>();
            }

            if (!wrap) DisableAtExtremes(componentsInChildren);
            LayoutSides(componentsInChildren);
            return componentsInChildren;
        }

        public void StepUp()
        {
            Step(step);
        }

        public void StepDown()
        {
            Step(-step);
        }

        private void Step(int amount)
        {
            value += amount;
            if (wrap)
            {
                if (value > maximum) value = minimum;
                if (value < minimum) value = maximum;
            }
            else
            {
                value = Math.Max(minimum, value);
                value = Math.Min(maximum, value);
                DisableAtExtremes(sides);
            }

            _onValueChanged.Invoke(value);
        }

        private void DisableAtExtremes(Selectable[] sides)
        {
            sides[0].interactable = wrap || value > minimum;
            sides[1].interactable = wrap || value < maximum;
        }

        private void RecreateSprites(Selectable[] sides)
        {
            for (var i = 0; i < 2; i++)
            {
                if (sides[i].image == null) continue;
                var sprite = sides[i].image.sprite;
                if (sprite.border.x != 0f && sprite.border.z != 0f)
                {
                    var rect = sprite.rect;
                    var border = sprite.border;
                    if (i == 0)
                    {
                        rect.xMax = border.z;
                        border.z = 0f;
                    }
                    else
                    {
                        rect.xMin = border.x;
                        border.x = 0f;
                    }

                    sides[i].image.sprite = Sprite.Create(sprite.texture, rect, sprite.pivot, sprite.pixelsPerUnit, 0u,
                        SpriteMeshType.FullRect, border);
                }
            }
        }

        public void LayoutSides(Selectable[] sides = null)
        {
            sides = sides ?? this.sides;
            RecreateSprites(sides);
            var rectTransform = this.transform as RectTransform;
            var num = rectTransform.rect.width / 2f - separatorWidth;
            for (var i = 0; i < 2; i++)
            {
                var inset = i != 0 ? num + separatorWidth : 0f;
                var component = sides[i].GetComponent<RectTransform>();
                component.anchorMin = Vector2.zero;
                component.anchorMax = Vector2.zero;
                component.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, inset, num);
                component.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Top, 0f, rectTransform.rect.height);
            }

            if ((bool) separator)
            {
                var transform = gameObject.transform.Find("Separator");
                var graphic = !(transform != null)
                    ? Instantiate(separator.gameObject).GetComponent<Graphic>()
                    : transform.GetComponent<Graphic>();
                graphic.gameObject.name = "Separator";
                graphic.gameObject.SetActive(true);
                graphic.rectTransform.SetParent(this.transform, false);
                graphic.rectTransform.anchorMin = Vector2.zero;
                graphic.rectTransform.anchorMax = Vector2.zero;
                graphic.rectTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, num, separatorWidth);
                graphic.rectTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Top, 0f,
                    rectTransform.rect.height);
            }
        }

        [Serializable]
        public class StepperValueChangedEvent : UnityEvent<int>
        {
        }
    }
}