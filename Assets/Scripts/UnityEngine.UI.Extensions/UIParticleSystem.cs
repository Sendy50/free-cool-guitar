using System;

namespace UnityEngine.UI.Extensions
{
    [ExecuteInEditMode]
    [RequireComponent(typeof(CanvasRenderer), typeof(ParticleSystem))]
    [AddComponentMenu("UI/Effects/Extensions/UIParticleSystem")]
    public class UIParticleSystem : MaskableGraphic
    {
        [Tooltip(
            "Having this enabled run the system in LateUpdate rather than in Update making it faster but less precise (more clunky)")]
        public bool fixedTime = true;

        private readonly UIVertex[] _quad = new UIVertex[4];

        private Transform _transform;

        private Material currentMaterial;

        private Texture currentTexture;

        private Vector4 imageUV = Vector4.zero;

        private ParticleSystem.MainModule mainModule;

        private ParticleSystem.Particle[] particles;

        private ParticleSystemRenderer pRenderer;

        private ParticleSystem pSystem;

        private ParticleSystem.TextureSheetAnimationModule textureSheetAnimation;

        private int textureSheetAnimationFrames;

        private Vector2 textureSheetAnimationFrameSize;

        public override Texture mainTexture => currentTexture;

        protected override void Awake()
        {
            base.Awake();
            if (!Initialize()) enabled = false;
        }

        private void Update()
        {
            if (!fixedTime && Application.isPlaying)
            {
                pSystem.Simulate(Time.unscaledDeltaTime, false, false, true);
                SetAllDirty();
                if (currentMaterial != null && currentTexture != currentMaterial.mainTexture || material != null &&
                    currentMaterial != null && material.shader != currentMaterial.shader)
                {
                    pSystem = null;
                    Initialize();
                }
            }
        }

        private void LateUpdate()
        {
            if (!Application.isPlaying)
            {
                SetAllDirty();
            }
            else if (fixedTime)
            {
                pSystem.Simulate(Time.unscaledDeltaTime, false, false, true);
                SetAllDirty();
                if (currentMaterial != null && currentTexture != currentMaterial.mainTexture || material != null &&
                    currentMaterial != null && material.shader != currentMaterial.shader)
                {
                    pSystem = null;
                    Initialize();
                }
            }

            if (!(material == currentMaterial))
            {
                pSystem = null;
                Initialize();
            }
        }

        protected bool Initialize()
        {
            if (_transform == null) _transform = transform;
            if (pSystem == null)
            {
                pSystem = GetComponent<ParticleSystem>();
                if (pSystem == null) return false;
                mainModule = pSystem.main;
                if (pSystem.main.maxParticles > 14000) mainModule.maxParticles = 14000;
                pRenderer = pSystem.GetComponent<ParticleSystemRenderer>();
                if (pRenderer != null) pRenderer.enabled = false;
                var shader = Shader.Find("UI Extensions/Particles/Additive");
                var material = new Material(shader);
                if (this.material == null) this.material = material;
                currentMaterial = this.material;
                if ((bool) currentMaterial && currentMaterial.HasProperty("_MainTex"))
                {
                    currentTexture = currentMaterial.mainTexture;
                    if (currentTexture == null) currentTexture = Texture2D.whiteTexture;
                }

                this.material = currentMaterial;
                mainModule.scalingMode = ParticleSystemScalingMode.Hierarchy;
                particles = null;
            }

            if (particles == null) particles = new ParticleSystem.Particle[pSystem.main.maxParticles];
            imageUV = new Vector4(0f, 0f, 1f, 1f);
            textureSheetAnimation = pSystem.textureSheetAnimation;
            textureSheetAnimationFrames = 0;
            textureSheetAnimationFrameSize = Vector2.zero;
            if (textureSheetAnimation.enabled)
            {
                textureSheetAnimationFrames = textureSheetAnimation.numTilesX * textureSheetAnimation.numTilesY;
                textureSheetAnimationFrameSize = new Vector2(1f / textureSheetAnimation.numTilesX,
                    1f / textureSheetAnimation.numTilesY);
            }

            return true;
        }

        protected override void OnPopulateMesh(VertexHelper vh)
        {
            vh.Clear();
            if (!gameObject.activeInHierarchy) return;
            var zero = Vector2.zero;
            var zero2 = Vector2.zero;
            var zero3 = Vector2.zero;
            var num = pSystem.GetParticles(particles);
            for (var i = 0; i < num; i++)
            {
                var particle = particles[i];
                Vector2 a = mainModule.simulationSpace != 0
                    ? _transform.InverseTransformPoint(particle.position)
                    : particle.position;
                var num2 = (0f - particle.rotation) * ((float) Math.PI / 180f);
                var f = num2 + (float) Math.PI / 2f;
                var currentColor = particle.GetCurrentColor(pSystem);
                var num3 = particle.GetCurrentSize(pSystem) * 0.5f;
                if (mainModule.scalingMode == ParticleSystemScalingMode.Shape) a /= canvas.scaleFactor;
                var vector = imageUV;
                if (textureSheetAnimation.enabled)
                {
                    var num4 = 1f - particle.remainingLifetime / particle.startLifetime;
                    if (textureSheetAnimation.frameOverTime.curveMin != null)
                        num4 = textureSheetAnimation.frameOverTime.curveMin.Evaluate(1f - particle.remainingLifetime /
                            particle.startLifetime);
                    else if (textureSheetAnimation.frameOverTime.curve != null)
                        num4 = textureSheetAnimation.frameOverTime.curve.Evaluate(1f - particle.remainingLifetime /
                            particle.startLifetime);
                    else if (textureSheetAnimation.frameOverTime.constant > 0f)
                        num4 = textureSheetAnimation.frameOverTime.constant -
                               particle.remainingLifetime / particle.startLifetime;
                    num4 = Mathf.Repeat(num4 * textureSheetAnimation.cycleCount, 1f);
                    var num5 = 0;
                    switch (textureSheetAnimation.animation)
                    {
                        case ParticleSystemAnimationType.WholeSheet:
                            num5 = Mathf.FloorToInt(num4 * textureSheetAnimationFrames);
                            break;
                        case ParticleSystemAnimationType.SingleRow:
                        {
                            num5 = Mathf.FloorToInt(num4 * textureSheetAnimation.numTilesX);
                            var rowIndex = textureSheetAnimation.rowIndex;
                            num5 += rowIndex * textureSheetAnimation.numTilesX;
                            break;
                        }
                    }

                    num5 %= textureSheetAnimationFrames;
                    vector.x = num5 % textureSheetAnimation.numTilesX * textureSheetAnimationFrameSize.x;
                    vector.y = Mathf.FloorToInt(num5 / textureSheetAnimation.numTilesX) *
                               textureSheetAnimationFrameSize.y;
                    vector.z = vector.x + textureSheetAnimationFrameSize.x;
                    vector.w = vector.y + textureSheetAnimationFrameSize.y;
                }

                zero.x = vector.x;
                zero.y = vector.y;
                ref var reference = ref _quad[0];
                reference = UIVertex.simpleVert;
                _quad[0].color = currentColor;
                _quad[0].uv0 = zero;
                zero.x = vector.x;
                zero.y = vector.w;
                ref var reference2 = ref _quad[1];
                reference2 = UIVertex.simpleVert;
                _quad[1].color = currentColor;
                _quad[1].uv0 = zero;
                zero.x = vector.z;
                zero.y = vector.w;
                ref var reference3 = ref _quad[2];
                reference3 = UIVertex.simpleVert;
                _quad[2].color = currentColor;
                _quad[2].uv0 = zero;
                zero.x = vector.z;
                zero.y = vector.y;
                ref var reference4 = ref _quad[3];
                reference4 = UIVertex.simpleVert;
                _quad[3].color = currentColor;
                _quad[3].uv0 = zero;
                if (num2 == 0f)
                {
                    zero2.x = a.x - num3;
                    zero2.y = a.y - num3;
                    zero3.x = a.x + num3;
                    zero3.y = a.y + num3;
                    zero.x = zero2.x;
                    zero.y = zero2.y;
                    _quad[0].position = zero;
                    zero.x = zero2.x;
                    zero.y = zero3.y;
                    _quad[1].position = zero;
                    zero.x = zero3.x;
                    zero.y = zero3.y;
                    _quad[2].position = zero;
                    zero.x = zero3.x;
                    zero.y = zero2.y;
                    _quad[3].position = zero;
                }
                else
                {
                    var b = new Vector2(Mathf.Cos(num2), Mathf.Sin(num2)) * num3;
                    var b2 = new Vector2(Mathf.Cos(f), Mathf.Sin(f)) * num3;
                    _quad[0].position = a - b - b2;
                    _quad[1].position = a - b + b2;
                    _quad[2].position = a + b + b2;
                    _quad[3].position = a + b - b2;
                }

                vh.AddUIVertexQuad(_quad);
            }
        }
    }
}