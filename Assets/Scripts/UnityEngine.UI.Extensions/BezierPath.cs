using System.Collections.Generic;

namespace UnityEngine.UI.Extensions
{
    public class BezierPath
    {
        private readonly List<Vector2> controlPoints;

        private int curveCount;

        public float DIVISION_THRESHOLD = -0.99f;

        public float MINIMUM_SQR_DISTANCE = 0.01f;
        public int SegmentsPerCurve = 10;

        public BezierPath()
        {
            controlPoints = new List<Vector2>();
        }

        public void SetControlPoints(List<Vector2> newControlPoints)
        {
            controlPoints.Clear();
            controlPoints.AddRange(newControlPoints);
            curveCount = (controlPoints.Count - 1) / 3;
        }

        public void SetControlPoints(Vector2[] newControlPoints)
        {
            controlPoints.Clear();
            controlPoints.AddRange(newControlPoints);
            curveCount = (controlPoints.Count - 1) / 3;
        }

        public List<Vector2> GetControlPoints()
        {
            return controlPoints;
        }

        public void Interpolate(List<Vector2> segmentPoints, float scale)
        {
            controlPoints.Clear();
            if (segmentPoints.Count < 2) return;
            for (var i = 0; i < segmentPoints.Count; i++)
                if (i == 0)
                {
                    var vector = segmentPoints[i];
                    var a = segmentPoints[i + 1];
                    var a2 = a - vector;
                    var item = vector + scale * a2;
                    controlPoints.Add(vector);
                    controlPoints.Add(item);
                }
                else if (i == segmentPoints.Count - 1)
                {
                    var b = segmentPoints[i - 1];
                    var vector2 = segmentPoints[i];
                    var a3 = vector2 - b;
                    var item2 = vector2 - scale * a3;
                    controlPoints.Add(item2);
                    controlPoints.Add(vector2);
                }
                else
                {
                    var b2 = segmentPoints[i - 1];
                    var vector3 = segmentPoints[i];
                    var a4 = segmentPoints[i + 1];
                    var normalized = (a4 - b2).normalized;
                    var item3 = vector3 - scale * normalized * (vector3 - b2).magnitude;
                    var item4 = vector3 + scale * normalized * (a4 - vector3).magnitude;
                    controlPoints.Add(item3);
                    controlPoints.Add(vector3);
                    controlPoints.Add(item4);
                }

            curveCount = (controlPoints.Count - 1) / 3;
        }

        public void SamplePoints(List<Vector2> sourcePoints, float minSqrDistance, float maxSqrDistance, float scale)
        {
            if (sourcePoints.Count < 2) return;
            var stack = new Stack<Vector2>();
            stack.Push(sourcePoints[0]);
            var vector = sourcePoints[1];
            var num = 2;
            for (num = 2; num < sourcePoints.Count; num++)
            {
                if ((vector - sourcePoints[num]).sqrMagnitude > minSqrDistance &&
                    (stack.Peek() - sourcePoints[num]).sqrMagnitude > maxSqrDistance) stack.Push(vector);
                vector = sourcePoints[num];
            }

            var vector2 = stack.Pop();
            var vector3 = stack.Peek();
            var normalized = (vector3 - vector).normalized;
            var magnitude = (vector - vector2).magnitude;
            var magnitude2 = (vector2 - vector3).magnitude;
            vector2 += normalized * ((magnitude2 - magnitude) / 2f);
            stack.Push(vector2);
            stack.Push(vector);
            Interpolate(new List<Vector2>(stack), scale);
        }

        public Vector2 CalculateBezierPoint(int curveIndex, float t)
        {
            var num = curveIndex * 3;
            var p = controlPoints[num];
            var p2 = controlPoints[num + 1];
            var p3 = controlPoints[num + 2];
            var p4 = controlPoints[num + 3];
            return CalculateBezierPoint(t, p, p2, p3, p4);
        }

        public List<Vector2> GetDrawingPoints0()
        {
            var list = new List<Vector2>();
            for (var i = 0; i < curveCount; i++)
            {
                if (i == 0) list.Add(CalculateBezierPoint(i, 0f));
                for (var j = 1; j <= SegmentsPerCurve; j++)
                {
                    var t = j / (float) SegmentsPerCurve;
                    list.Add(CalculateBezierPoint(i, t));
                }
            }

            return list;
        }

        public List<Vector2> GetDrawingPoints1()
        {
            var list = new List<Vector2>();
            for (var i = 0; i < controlPoints.Count - 3; i += 3)
            {
                var p = controlPoints[i];
                var p2 = controlPoints[i + 1];
                var p3 = controlPoints[i + 2];
                var p4 = controlPoints[i + 3];
                if (i == 0) list.Add(CalculateBezierPoint(0f, p, p2, p3, p4));
                for (var j = 1; j <= SegmentsPerCurve; j++)
                {
                    var t = j / (float) SegmentsPerCurve;
                    list.Add(CalculateBezierPoint(t, p, p2, p3, p4));
                }
            }

            return list;
        }

        public List<Vector2> GetDrawingPoints2()
        {
            var list = new List<Vector2>();
            for (var i = 0; i < curveCount; i++)
            {
                var list2 = FindDrawingPoints(i);
                if (i != 0) list2.RemoveAt(0);
                list.AddRange(list2);
            }

            return list;
        }

        private List<Vector2> FindDrawingPoints(int curveIndex)
        {
            var list = new List<Vector2>();
            var item = CalculateBezierPoint(curveIndex, 0f);
            var item2 = CalculateBezierPoint(curveIndex, 1f);
            list.Add(item);
            list.Add(item2);
            FindDrawingPoints(curveIndex, 0f, 1f, list, 1);
            return list;
        }

        private int FindDrawingPoints(int curveIndex, float t0, float t1, List<Vector2> pointList, int insertionIndex)
        {
            var a = CalculateBezierPoint(curveIndex, t0);
            var vector = CalculateBezierPoint(curveIndex, t1);
            if ((a - vector).sqrMagnitude < MINIMUM_SQR_DISTANCE) return 0;
            var num = (t0 + t1) / 2f;
            var vector2 = CalculateBezierPoint(curveIndex, num);
            var normalized = (a - vector2).normalized;
            var normalized2 = (vector - vector2).normalized;
            if (Vector2.Dot(normalized, normalized2) > DIVISION_THRESHOLD || Mathf.Abs(num - 0.5f) < 0.0001f)
            {
                var num2 = 0;
                num2 += FindDrawingPoints(curveIndex, t0, num, pointList, insertionIndex);
                pointList.Insert(insertionIndex + num2, vector2);
                num2++;
                return num2 + FindDrawingPoints(curveIndex, num, t1, pointList, insertionIndex + num2);
            }

            return 0;
        }

        private Vector2 CalculateBezierPoint(float t, Vector2 p0, Vector2 p1, Vector2 p2, Vector2 p3)
        {
            var num = 1f - t;
            var num2 = t * t;
            var num3 = num * num;
            var d = num3 * num;
            var d2 = num2 * t;
            var a = d * p0;
            a += 3f * num3 * t * p1;
            a += 3f * num * num2 * p2;
            return a + d2 * p3;
        }
    }
}