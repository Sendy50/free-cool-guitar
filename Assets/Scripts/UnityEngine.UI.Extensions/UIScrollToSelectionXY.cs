using UnityEngine.EventSystems;

namespace UnityEngine.UI.Extensions
{
    [AddComponentMenu("UI/Extensions/UI ScrollTo Selection XY")]
    [RequireComponent(typeof(ScrollRect))]
    public class UIScrollToSelectionXY : MonoBehaviour
    {
        public float scrollSpeed = 10f;

        [SerializeField] private RectTransform layoutListGroup;

        private RectTransform currentCanvas;

        private bool scrollToSelection = true;

        private RectTransform scrollWindow;

        private RectTransform targetScrollObject;

        private ScrollRect targetScrollRect;

        private void Start()
        {
            targetScrollRect = GetComponent<ScrollRect>();
            scrollWindow = targetScrollRect.GetComponent<RectTransform>();
        }

        private void Update()
        {
            ScrollRectToLevelSelection();
        }

        private void ScrollRectToLevelSelection()
        {
            var current = EventSystem.current;
            if (targetScrollRect == null || layoutListGroup == null || scrollWindow == null) return;
            var rectTransform = !(current.currentSelectedGameObject != null)
                ? null
                : current.currentSelectedGameObject.GetComponent<RectTransform>();
            if (rectTransform != targetScrollObject) scrollToSelection = true;
            if (!(rectTransform == null) && scrollToSelection &&
                !(rectTransform.transform.parent != layoutListGroup.transform))
            {
                var flag = false;
                var flag2 = false;
                if (targetScrollRect.vertical)
                {
                    var num = 0f - rectTransform.anchoredPosition.y;
                    var y = layoutListGroup.anchoredPosition.y;
                    var num2 = 0f;
                    num2 = y - num;
                    targetScrollRect.verticalNormalizedPosition +=
                        num2 / layoutListGroup.sizeDelta.y * Time.deltaTime * scrollSpeed;
                    flag2 = Mathf.Abs(num2) < 2f;
                }

                if (targetScrollRect.horizontal)
                {
                    var num3 = 0f - rectTransform.anchoredPosition.x;
                    var x = layoutListGroup.anchoredPosition.x;
                    var num4 = 0f;
                    num4 = x - num3;
                    targetScrollRect.horizontalNormalizedPosition +=
                        num4 / layoutListGroup.sizeDelta.x * Time.deltaTime * scrollSpeed;
                    flag = Mathf.Abs(num4) < 2f;
                }

                if (flag && flag2) scrollToSelection = false;
                targetScrollObject = rectTransform;
            }
        }
    }
}