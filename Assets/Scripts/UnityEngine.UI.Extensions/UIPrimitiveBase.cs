using System;
using System.Collections.Generic;

namespace UnityEngine.UI.Extensions
{
    public class UIPrimitiveBase : MaskableGraphic, ILayoutElement, ICanvasRaycastFilter
    {
        protected static Material s_ETC1DefaultUI;

        [SerializeField] private Sprite m_Sprite;

        [SerializeField] private ResolutionMode m_improveResolution;

        [SerializeField] protected float m_Resolution;

        [SerializeField] private bool m_useNativeSize;

        internal float m_EventAlphaThreshold = 1f;

        [NonSerialized] private Sprite m_OverrideSprite;

        protected UIPrimitiveBase()
        {
            useLegacyMeshGeneration = false;
        }

        public Sprite sprite
        {
            get => m_Sprite;
            set
            {
                if (SetPropertyUtility.SetClass(ref m_Sprite, value)) GeneratedUVs();
                SetAllDirty();
            }
        }

        public Sprite overrideSprite
        {
            get => activeSprite;
            set
            {
                if (SetPropertyUtility.SetClass(ref m_OverrideSprite, value)) GeneratedUVs();
                SetAllDirty();
            }
        }

        protected Sprite activeSprite => !(m_OverrideSprite != null) ? sprite : m_OverrideSprite;

        public float eventAlphaThreshold
        {
            get => m_EventAlphaThreshold;
            set => m_EventAlphaThreshold = value;
        }

        public ResolutionMode ImproveResolution
        {
            get => m_improveResolution;
            set
            {
                m_improveResolution = value;
                SetAllDirty();
            }
        }

        public float Resoloution
        {
            get => m_Resolution;
            set
            {
                m_Resolution = value;
                SetAllDirty();
            }
        }

        public bool UseNativeSize
        {
            get => m_useNativeSize;
            set
            {
                m_useNativeSize = value;
                SetAllDirty();
            }
        }

        public static Material defaultETC1GraphicMaterial
        {
            get
            {
                if (s_ETC1DefaultUI == null) s_ETC1DefaultUI = Canvas.GetETC1SupportedCanvasMaterial();
                return s_ETC1DefaultUI;
            }
        }

        public override Texture mainTexture
        {
            get
            {
                if (activeSprite == null)
                {
                    if (material != null && material.mainTexture != null) return material.mainTexture;
                    return s_WhiteTexture;
                }

                return activeSprite.texture;
            }
        }

        public bool hasBorder
        {
            get
            {
                if (activeSprite != null) return activeSprite.border.sqrMagnitude > 0f;
                return false;
            }
        }

        public float pixelsPerUnit
        {
            get
            {
                var num = 100f;
                if ((bool) activeSprite) num = activeSprite.pixelsPerUnit;
                var num2 = 100f;
                if ((bool) canvas) num2 = canvas.referencePixelsPerUnit;
                return num / num2;
            }
        }

        public override Material material
        {
            get
            {
                if (m_Material != null) return m_Material;
                if ((bool) activeSprite && activeSprite.associatedAlphaSplitTexture != null)
                    return defaultETC1GraphicMaterial;
                return defaultMaterial;
            }
            set => base.material = value;
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            SetAllDirty();
        }

        public virtual bool IsRaycastLocationValid(Vector2 screenPoint, Camera eventCamera)
        {
            if (m_EventAlphaThreshold >= 1f) return true;
            var overrideSprite = this.overrideSprite;
            if (overrideSprite == null) return true;
            RectTransformUtility.ScreenPointToLocalPointInRectangle(rectTransform, screenPoint, eventCamera,
                out var localPoint);
            var pixelAdjustedRect = GetPixelAdjustedRect();
            localPoint.x += rectTransform.pivot.x * pixelAdjustedRect.width;
            localPoint.y += rectTransform.pivot.y * pixelAdjustedRect.height;
            localPoint = MapCoordinate(localPoint, pixelAdjustedRect);
            var textureRect = overrideSprite.textureRect;
            var vector = new Vector2(localPoint.x / textureRect.width, localPoint.y / textureRect.height);
            var x = Mathf.Lerp(textureRect.x, textureRect.xMax, vector.x) / overrideSprite.texture.width;
            var y = Mathf.Lerp(textureRect.y, textureRect.yMax, vector.y) / overrideSprite.texture.height;
            try
            {
                return overrideSprite.texture.GetPixelBilinear(x, y).a >= m_EventAlphaThreshold;
            }
            catch (UnityException ex)
            {
                Debug.LogError(
                    "Using clickAlphaThreshold lower than 1 on Image whose sprite texture cannot be read. " +
                    ex.Message + " Also make sure to disable sprite packing for this sprite.", this);
                return true;
            }
        }

        public virtual float minWidth => 0f;

        public virtual float preferredWidth
        {
            get
            {
                if (overrideSprite == null) return 0f;
                return overrideSprite.rect.size.x / pixelsPerUnit;
            }
        }

        public virtual float flexibleWidth => -1f;

        public virtual float minHeight => 0f;

        public virtual float preferredHeight
        {
            get
            {
                if (overrideSprite == null) return 0f;
                return overrideSprite.rect.size.y / pixelsPerUnit;
            }
        }

        public virtual float flexibleHeight => -1f;

        public virtual int layoutPriority => 0;

        public virtual void CalculateLayoutInputHorizontal()
        {
        }

        public virtual void CalculateLayoutInputVertical()
        {
        }

        protected UIVertex[] SetVbo(Vector2[] vertices, Vector2[] uvs)
        {
            var array = new UIVertex[4];
            for (var i = 0; i < vertices.Length; i++)
            {
                var simpleVert = UIVertex.simpleVert;
                simpleVert.color = color;
                simpleVert.position = vertices[i];
                simpleVert.uv0 = uvs[i];
                array[i] = simpleVert;
            }

            return array;
        }

        protected Vector2[] IncreaseResolution(Vector2[] input)
        {
            var list = new List<Vector2>();
            switch (ImproveResolution)
            {
                case ResolutionMode.PerLine:
                {
                    var num3 = 0f;
                    var num = 0f;
                    for (var j = 0; j < input.Length - 1; j++) num3 += Vector2.Distance(input[j], input[j + 1]);
                    ResolutionToNativeSize(num3);
                    num = num3 / m_Resolution;
                    var num4 = 0;
                    for (var k = 0; k < input.Length - 1; k++)
                    {
                        var vector3 = input[k];
                        list.Add(vector3);
                        var vector4 = input[k + 1];
                        var num5 = Vector2.Distance(vector3, vector4) / num;
                        var num6 = 1f / num5;
                        for (var l = 0; (float) l < num5; l++)
                        {
                            list.Add(Vector2.Lerp(vector3, vector4, l * num6));
                            num4++;
                        }

                        list.Add(vector4);
                    }

                    break;
                }
                case ResolutionMode.PerSegment:
                {
                    for (var i = 0; i < input.Length - 1; i++)
                    {
                        var vector = input[i];
                        list.Add(vector);
                        var vector2 = input[i + 1];
                        ResolutionToNativeSize(Vector2.Distance(vector, vector2));
                        var num = 1f / m_Resolution;
                        for (var num2 = 1f; num2 < m_Resolution; num2 += 1f)
                            list.Add(Vector2.Lerp(vector, vector2, num * num2));
                        list.Add(vector2);
                    }

                    break;
                }
            }

            return list.ToArray();
        }

        protected virtual void GeneratedUVs()
        {
        }

        protected virtual void ResolutionToNativeSize(float distance)
        {
        }

        private Vector2 MapCoordinate(Vector2 local, Rect rect)
        {
            var rect2 = sprite.rect;
            return new Vector2(local.x * rect.width, local.y * rect.height);
        }

        private Vector4 GetAdjustedBorders(Vector4 border, Rect rect)
        {
            for (var i = 0; i <= 1; i++)
            {
                var num = border[i] + border[i + 2];
                if (rect.size[i] < num && num != 0f)
                {
                    var num2 = rect.size[i] / num;
                    border[i] *= num2;
                    border[i + 2] *= num2;
                }
            }

            return border;
        }
    }
}