using System;
using System.Collections.Generic;

namespace UnityEngine.UI.Extensions
{
    [AddComponentMenu("UI/Extensions/Primitives/UILineTextureRenderer")]
    public class UILineTextureRenderer : UIPrimitiveBase
    {
        [SerializeField] private Rect m_UVRect = new Rect(0f, 0f, 1f, 1f);

        [SerializeField] private Vector2[] m_points;

        public float LineThickness = 2f;

        public bool UseMargins;

        public Vector2 Margin;

        public bool relativeSize;

        public Rect uvRect
        {
            get => m_UVRect;
            set
            {
                if (!(m_UVRect == value))
                {
                    m_UVRect = value;
                    SetVerticesDirty();
                }
            }
        }

        public Vector2[] Points
        {
            get => m_points;
            set
            {
                if (m_points != value)
                {
                    m_points = value;
                    SetAllDirty();
                }
            }
        }

        protected override void OnPopulateMesh(VertexHelper vh)
        {
            if (m_points == null || m_points.Length < 2)
                m_points = new Vector2[2]
                {
                    new Vector2(0f, 0f),
                    new Vector2(1f, 1f)
                };
            var num = 24;
            var num2 = rectTransform.rect.width;
            var num3 = rectTransform.rect.height;
            var num4 = (0f - rectTransform.pivot.x) * rectTransform.rect.width;
            var num5 = (0f - rectTransform.pivot.y) * rectTransform.rect.height;
            if (!relativeSize)
            {
                num2 = 1f;
                num3 = 1f;
            }

            var list = new List<Vector2>();
            list.Add(m_points[0]);
            var item = m_points[0] + (m_points[1] - m_points[0]).normalized * num;
            list.Add(item);
            for (var i = 1; i < m_points.Length - 1; i++) list.Add(m_points[i]);
            item = m_points[m_points.Length - 1] -
                   (m_points[m_points.Length - 1] - m_points[m_points.Length - 2]).normalized * num;
            list.Add(item);
            list.Add(m_points[m_points.Length - 1]);
            var array = list.ToArray();
            if (UseMargins)
            {
                num2 -= Margin.x;
                num3 -= Margin.y;
                num4 += Margin.x / 2f;
                num5 += Margin.y / 2f;
            }

            vh.Clear();
            var vector = Vector2.zero;
            var vector2 = Vector2.zero;
            for (var j = 1; j < array.Length; j++)
            {
                var vector3 = array[j - 1];
                var vector4 = array[j];
                vector3 = new Vector2(vector3.x * num2 + num4, vector3.y * num3 + num5);
                vector4 = new Vector2(vector4.x * num2 + num4, vector4.y * num3 + num5);
                var z = Mathf.Atan2(vector4.y - vector3.y, vector4.x - vector3.x) * 180f / (float) Math.PI;
                var v = vector3 + new Vector2(0f, (0f - LineThickness) / 2f);
                var v2 = vector3 + new Vector2(0f, LineThickness / 2f);
                var v3 = vector4 + new Vector2(0f, LineThickness / 2f);
                var v4 = vector4 + new Vector2(0f, (0f - LineThickness) / 2f);
                v = RotatePointAroundPivot(v, vector3, new Vector3(0f, 0f, z));
                v2 = RotatePointAroundPivot(v2, vector3, new Vector3(0f, 0f, z));
                v3 = RotatePointAroundPivot(v3, vector4, new Vector3(0f, 0f, z));
                v4 = RotatePointAroundPivot(v4, vector4, new Vector3(0f, 0f, z));
                var zero = Vector2.zero;
                var vector5 = new Vector2(0f, 1f);
                var vector6 = new Vector2(0.5f, 0f);
                var vector7 = new Vector2(0.5f, 1f);
                var vector8 = new Vector2(1f, 0f);
                var vector9 = new Vector2(1f, 1f);
                var uvs = new Vector2[4]
                {
                    vector6,
                    vector7,
                    vector7,
                    vector6
                };
                if (j > 1)
                    vh.AddUIVertexQuad(SetVbo(new Vector2[4]
                    {
                        vector,
                        vector2,
                        v,
                        v2
                    }, uvs));
                if (j == 1)
                    uvs = new Vector2[4]
                    {
                        zero,
                        vector5,
                        vector7,
                        vector6
                    };
                else if (j == array.Length - 1)
                    uvs = new Vector2[4]
                    {
                        vector6,
                        vector7,
                        vector9,
                        vector8
                    };
                vh.AddUIVertexQuad(SetVbo(new Vector2[4]
                {
                    v,
                    v2,
                    v3,
                    v4
                }, uvs));
                vector = v3;
                vector2 = v4;
            }
        }

        public Vector3 RotatePointAroundPivot(Vector3 point, Vector3 pivot, Vector3 angles)
        {
            var point2 = point - pivot;
            point2 = Quaternion.Euler(angles) * point2;
            point = point2 + pivot;
            return point;
        }
    }
}