using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace UnityEngine.UI.Extensions
{
    [AddComponentMenu("UI/Extensions/TextPic")]
    [ExecuteInEditMode]
    public class TextPic : Text, IPointerClickHandler, IPointerExitHandler, IPointerEnterHandler, ISelectHandler,
        IEventSystemHandler
    {
        private static readonly Regex s_Regex =
            new Regex("<quad name=(.+?) size=(\\d*\\.?\\d+%?) width=(\\d*\\.?\\d+%?) />", RegexOptions.Singleline);

        private static readonly StringBuilder s_TextBuilder = new StringBuilder();

        private static readonly Regex s_HrefRegex =
            new Regex("<a href=([^>\\n\\s]+)>(.*?)(</a>)", RegexOptions.Singleline);

        [SerializeField] [Tooltip("Allow click events to be received by parents, (default) blocks")]
        private bool m_ClickParents;

        public IconName[] inspectorIconList;

        [Tooltip("Global scaling factor for all images")]
        public float ImageScalingFactor = 1f;

        public string hyperlinkColor = "blue";

        [SerializeField] public Vector2 imageOffset = Vector2.zero;

        public bool isCreating_m_HrefInfos = true;

        [SerializeField] private HrefClickEvent m_OnHrefClick = new HrefClickEvent();

        private readonly List<GameObject> culled_ImagesPool = new List<GameObject>();

        private readonly List<HrefInfo> m_HrefInfos = new List<HrefInfo>();

        private readonly List<Image> m_ImagesPool = new List<Image>();

        private readonly List<int> m_ImagesVertexIndex = new List<int>();

        private Button button;

        private bool clearImages;

        private string fixedString;

        private Selectable highlightselectable;

        private string m_OutputText;

        private readonly List<Vector2> positions = new List<Vector2>();

        private string previousText = string.Empty;

        private readonly Object thisLock = new Object();

        public bool AllowClickParents
        {
            get => m_ClickParents;
            set => m_ClickParents = value;
        }

        public HrefClickEvent onHrefClick
        {
            get => m_OnHrefClick;
            set => m_OnHrefClick = value;
        }

        private new void Start()
        {
            button = GetComponentInParent<Button>();
            if (button != null)
            {
                var canvasGroup = GetComponent<CanvasGroup>();
                if (canvasGroup == null) canvasGroup = gameObject.AddComponent<CanvasGroup>();
                canvasGroup.blocksRaycasts = false;
                highlightselectable = canvasGroup.GetComponent<Selectable>();
            }
            else
            {
                highlightselectable = GetComponent<Selectable>();
            }

            Reset_m_HrefInfos();
            base.Start();
        }

        private void Update()
        {
            lock (thisLock)
            {
                if (clearImages)
                {
                    for (var i = 0; i < culled_ImagesPool.Count; i++) DestroyImmediate(culled_ImagesPool[i]);
                    culled_ImagesPool.Clear();
                    clearImages = false;
                }
            }

            if (previousText != text) Reset_m_HrefInfos();
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            RectTransformUtility.ScreenPointToLocalPointInRectangle(rectTransform, eventData.position,
                eventData.pressEventCamera, out var localPoint);
            foreach (var hrefInfo in m_HrefInfos)
            {
                var boxes = hrefInfo.boxes;
                for (var i = 0; i < boxes.Count; i++)
                    if (boxes[i].Contains(localPoint))
                    {
                        m_OnHrefClick.Invoke(hrefInfo.name);
                        return;
                    }
            }
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            if (m_ImagesPool.Count < 1) return;
            foreach (var item in m_ImagesPool)
                if (highlightselectable != null && highlightselectable.isActiveAndEnabled)
                    item.color = highlightselectable.colors.highlightedColor;
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            if (m_ImagesPool.Count < 1) return;
            foreach (var item in m_ImagesPool)
                if (highlightselectable != null && highlightselectable.isActiveAndEnabled)
                    item.color = highlightselectable.colors.normalColor;
                else
                    item.color = color;
        }

        public void OnSelect(BaseEventData eventData)
        {
            if (m_ImagesPool.Count < 1) return;
            foreach (var item in m_ImagesPool)
                if (highlightselectable != null && highlightselectable.isActiveAndEnabled)
                    item.color = highlightselectable.colors.highlightedColor;
        }

        public override void SetVerticesDirty()
        {
            base.SetVerticesDirty();
            UpdateQuadImage();
        }

        protected void UpdateQuadImage()
        {
            m_OutputText = GetOutputText();
            m_ImagesVertexIndex.Clear();
            var enumerator = s_Regex.Matches(m_OutputText).GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    var match = (Match) enumerator.Current;
                    var index = match.Index;
                    var item = index * 4 + 3;
                    m_ImagesVertexIndex.Add(item);
                    m_ImagesPool.RemoveAll(image => image == null);
                    if (m_ImagesPool.Count == 0) GetComponentsInChildren(m_ImagesPool);
                    if (m_ImagesVertexIndex.Count > m_ImagesPool.Count)
                    {
                        var gameObject = DefaultControls.CreateImage(default);
                        gameObject.layer = this.gameObject.layer;
                        var rectTransform = gameObject.transform as RectTransform;
                        if ((bool) rectTransform)
                        {
                            rectTransform.SetParent(this.rectTransform);
                            rectTransform.localPosition = Vector3.zero;
                            rectTransform.localRotation = Quaternion.identity;
                            rectTransform.localScale = Vector3.one;
                        }

                        m_ImagesPool.Add(gameObject.GetComponent<Image>());
                    }

                    var value = match.Groups[1].Value;
                    var image2 = m_ImagesPool[m_ImagesVertexIndex.Count - 1];
                    var vector = Vector2.zero;
                    if ((image2.sprite == null || image2.sprite.name != value) && inspectorIconList != null &&
                        inspectorIconList.Length > 0)
                    {
                        var array = inspectorIconList;
                        for (var i = 0; i < array.Length; i++)
                        {
                            var iconName = array[i];
                            if (iconName.name == value)
                            {
                                image2.sprite = iconName.sprite;
                                image2.rectTransform.sizeDelta = new Vector2(
                                    fontSize * ImageScalingFactor * iconName.scale.x,
                                    fontSize * ImageScalingFactor * iconName.scale.y);
                                vector = iconName.offset;
                                break;
                            }
                        }
                    }

                    image2.enabled = true;
                    if (positions.Count == m_ImagesPool.Count)
                    {
                        var vector3 = image2.rectTransform.anchoredPosition =
                            positions[m_ImagesVertexIndex.Count - 1] += vector;
                    }
                }
            }
            finally
            {
                IDisposable disposable;
                if ((disposable = enumerator as IDisposable) != null) disposable.Dispose();
            }

            for (var j = m_ImagesVertexIndex.Count; j < m_ImagesPool.Count; j++)
                if ((bool) m_ImagesPool[j])
                {
                    m_ImagesPool[j].gameObject.SetActive(false);
                    m_ImagesPool[j].gameObject.hideFlags = HideFlags.HideAndDontSave;
                    culled_ImagesPool.Add(m_ImagesPool[j].gameObject);
                    m_ImagesPool.Remove(m_ImagesPool[j]);
                }

            if (culled_ImagesPool.Count > 1) clearImages = true;
        }

        protected override void OnPopulateMesh(VertexHelper toFill)
        {
            var text = m_Text;
            m_Text = GetOutputText();
            base.OnPopulateMesh(toFill);
            m_Text = text;
            positions.Clear();
            UIVertex vertex = default;
            for (var i = 0; i < m_ImagesVertexIndex.Count; i++)
            {
                var num = m_ImagesVertexIndex[i];
                var rectTransform = m_ImagesPool[i].rectTransform;
                var sizeDelta = rectTransform.sizeDelta;
                if (num < toFill.currentVertCount)
                {
                    toFill.PopulateUIVertex(ref vertex, num);
                    positions.Add(new Vector2(vertex.position.x + sizeDelta.x / 2f,
                        vertex.position.y + sizeDelta.y / 2f) + imageOffset);
                    toFill.PopulateUIVertex(ref vertex, num - 3);
                    var position = vertex.position;
                    var num2 = num;
                    var num3 = num - 3;
                    while (num2 > num3)
                    {
                        toFill.PopulateUIVertex(ref vertex, num);
                        vertex.position = position;
                        toFill.SetUIVertex(vertex, num2);
                        num2--;
                    }
                }
            }

            if (m_ImagesVertexIndex.Count != 0) m_ImagesVertexIndex.Clear();
            foreach (var hrefInfo in m_HrefInfos)
            {
                hrefInfo.boxes.Clear();
                if (hrefInfo.startIndex >= toFill.currentVertCount) continue;
                toFill.PopulateUIVertex(ref vertex, hrefInfo.startIndex);
                var position2 = vertex.position;
                var bounds = new Bounds(position2, Vector3.zero);
                var j = hrefInfo.startIndex;
                for (var endIndex = hrefInfo.endIndex; j < endIndex && j < toFill.currentVertCount; j++)
                {
                    toFill.PopulateUIVertex(ref vertex, j);
                    position2 = vertex.position;
                    if (position2.x < bounds.min.x)
                    {
                        hrefInfo.boxes.Add(new Rect(bounds.min, bounds.size));
                        bounds = new Bounds(position2, Vector3.zero);
                    }
                    else
                    {
                        bounds.Encapsulate(position2);
                    }
                }

                hrefInfo.boxes.Add(new Rect(bounds.min, bounds.size));
            }

            UpdateQuadImage();
        }

        protected string GetOutputText()
        {
            s_TextBuilder.Length = 0;
            var num = 0;
            fixedString = text;
            if (inspectorIconList != null && inspectorIconList.Length > 0)
            {
                var array = inspectorIconList;
                for (var i = 0; i < array.Length; i++)
                {
                    var iconName = array[i];
                    if (iconName.name != null && iconName.name != string.Empty)
                        fixedString = fixedString.Replace(iconName.name,
                            "<quad name=" + iconName.name + " size=" + fontSize + " width=1 />");
                }
            }

            var num2 = 0;
            var enumerator = s_HrefRegex.Matches(fixedString).GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    var match = (Match) enumerator.Current;
                    s_TextBuilder.Append(fixedString.Substring(num, match.Index - num));
                    s_TextBuilder.Append("<color=" + hyperlinkColor + ">");
                    var group = match.Groups[1];
                    if (isCreating_m_HrefInfos)
                    {
                        var hrefInfo = new HrefInfo();
                        hrefInfo.startIndex = s_TextBuilder.Length * 4;
                        hrefInfo.endIndex = (s_TextBuilder.Length + match.Groups[2].Length - 1) * 4 + 3;
                        hrefInfo.name = group.Value;
                        var item = hrefInfo;
                        m_HrefInfos.Add(item);
                    }
                    else if (m_HrefInfos.Count > 0)
                    {
                        m_HrefInfos[num2].startIndex = s_TextBuilder.Length * 4;
                        m_HrefInfos[num2].endIndex = (s_TextBuilder.Length + match.Groups[2].Length - 1) * 4 + 3;
                        num2++;
                    }

                    s_TextBuilder.Append(match.Groups[2].Value);
                    s_TextBuilder.Append("</color>");
                    num = match.Index + match.Length;
                }
            }
            finally
            {
                IDisposable disposable;
                if ((disposable = enumerator as IDisposable) != null) disposable.Dispose();
            }

            if (isCreating_m_HrefInfos) isCreating_m_HrefInfos = false;
            s_TextBuilder.Append(fixedString.Substring(num, fixedString.Length - num));
            return s_TextBuilder.ToString();
        }

        private void Reset_m_HrefInfos()
        {
            previousText = text;
            m_HrefInfos.Clear();
            isCreating_m_HrefInfos = true;
        }

        [Serializable]
        public struct IconName
        {
            public string name;

            public Sprite sprite;

            public Vector2 offset;

            public Vector2 scale;
        }

        [Serializable]
        public class HrefClickEvent : UnityEvent<string>
        {
        }

        private class HrefInfo
        {
            public readonly List<Rect> boxes = new List<Rect>();

            public int endIndex;

            public string name;
            public int startIndex;
        }
    }
}