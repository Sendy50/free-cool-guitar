namespace UnityEngine.UI.Extensions
{
    [AddComponentMenu("Layout/Extensions/Table Layout Group")]
    public class TableLayoutGroup : LayoutGroup
    {
        public enum Corner
        {
            UpperLeft,
            UpperRight,
            LowerLeft,
            LowerRight
        }

        [SerializeField] protected Corner startCorner;

        [SerializeField] protected float[] columnWidths = new float[1]
        {
            96f
        };

        [SerializeField] protected float minimumRowHeight = 32f;

        [SerializeField] protected bool flexibleRowHeight = true;

        [SerializeField] protected float columnSpacing;

        [SerializeField] protected float rowSpacing;

        private float[] preferredRowHeights;

        public Corner StartCorner
        {
            get => startCorner;
            set => SetProperty(ref startCorner, value);
        }

        public float[] ColumnWidths
        {
            get => columnWidths;
            set => SetProperty(ref columnWidths, value);
        }

        public float MinimumRowHeight
        {
            get => minimumRowHeight;
            set => SetProperty(ref minimumRowHeight, value);
        }

        public bool FlexibleRowHeight
        {
            get => flexibleRowHeight;
            set => SetProperty(ref flexibleRowHeight, value);
        }

        public float ColumnSpacing
        {
            get => columnSpacing;
            set => SetProperty(ref columnSpacing, value);
        }

        public float RowSpacing
        {
            get => rowSpacing;
            set => SetProperty(ref rowSpacing, value);
        }

        public override void CalculateLayoutInputHorizontal()
        {
            base.CalculateLayoutInputHorizontal();
            float num = padding.horizontal;
            var num2 = Mathf.Min(rectChildren.Count, columnWidths.Length);
            for (var i = 0; i < num2; i++)
            {
                num += columnWidths[i];
                num += columnSpacing;
            }

            num -= columnSpacing;
            SetLayoutInputForAxis(num, num, 0f, 0);
        }

        public override void CalculateLayoutInputVertical()
        {
            var num = columnWidths.Length;
            var num2 = Mathf.CeilToInt(rectChildren.Count / (float) num);
            preferredRowHeights = new float[num2];
            float num3 = padding.vertical;
            float num4 = padding.vertical;
            if (num2 > 1)
            {
                var num5 = (num2 - 1) * rowSpacing;
                num3 += num5;
                num4 += num5;
            }

            if (flexibleRowHeight)
            {
                var num6 = 0f;
                var num7 = 0f;
                for (var i = 0; i < num2; i++)
                {
                    num6 = minimumRowHeight;
                    num7 = minimumRowHeight;
                    for (var j = 0; j < num; j++)
                    {
                        var num8 = i * num + j;
                        if (num8 == rectChildren.Count) break;
                        num7 = Mathf.Max(LayoutUtility.GetPreferredHeight(rectChildren[num8]), num7);
                        num6 = Mathf.Max(LayoutUtility.GetMinHeight(rectChildren[num8]), num6);
                    }

                    num3 += num6;
                    num4 += num7;
                    preferredRowHeights[i] = num7;
                }
            }
            else
            {
                for (var k = 0; k < num2; k++) preferredRowHeights[k] = minimumRowHeight;
                num3 += num2 * minimumRowHeight;
                num4 = num3;
            }

            num4 = Mathf.Max(num3, num4);
            SetLayoutInputForAxis(num3, num4, 1f, 1);
        }

        public override void SetLayoutHorizontal()
        {
            if (columnWidths.Length == 0) columnWidths = new float[1];
            var num = columnWidths.Length;
            var num2 = (int) startCorner % 2;
            var num3 = 0f;
            var num4 = 0f;
            var num5 = Mathf.Min(rectChildren.Count, columnWidths.Length);
            for (var i = 0; i < num5; i++)
            {
                num4 += columnWidths[i];
                num4 += columnSpacing;
            }

            num4 -= columnSpacing;
            num3 = GetStartOffset(0, num4);
            if (num2 == 1) num3 += num4;
            var num6 = num3;
            for (var j = 0; j < rectChildren.Count; j++)
            {
                var num7 = j % num;
                if (num7 == 0) num6 = num3;
                if (num2 == 1) num6 -= columnWidths[num7];
                SetChildAlongAxis(rectChildren[j], 0, num6, columnWidths[num7]);
                num6 = num2 != 1 ? num6 + (columnWidths[num7] + columnSpacing) : num6 - columnSpacing;
            }
        }

        public override void SetLayoutVertical()
        {
            var num = columnWidths.Length;
            var num2 = preferredRowHeights.Length;
            var num3 = (int) startCorner / 2;
            var num4 = 0f;
            var num5 = 0f;
            for (var i = 0; i < num2; i++) num5 += preferredRowHeights[i];
            if (num2 > 1) num5 += (num2 - 1) * rowSpacing;
            num4 = GetStartOffset(1, num5);
            if (num3 == 1) num4 += num5;
            var num6 = num4;
            for (var j = 0; j < num2; j++)
            {
                if (num3 == 1) num6 -= preferredRowHeights[j];
                for (var k = 0; k < num; k++)
                {
                    var num7 = j * num + k;
                    if (num7 == rectChildren.Count) break;
                    SetChildAlongAxis(rectChildren[num7], 1, num6, preferredRowHeights[j]);
                }

                num6 = num3 != 1 ? num6 + (preferredRowHeights[j] + rowSpacing) : num6 - rowSpacing;
            }

            preferredRowHeights = null;
        }
    }
}