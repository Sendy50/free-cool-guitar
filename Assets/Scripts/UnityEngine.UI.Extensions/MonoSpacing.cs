using System.Collections.Generic;

namespace UnityEngine.UI.Extensions
{
    [AddComponentMenu("UI/Effects/Extensions/Mono Spacing")]
    [RequireComponent(typeof(Text))]
    [RequireComponent(typeof(RectTransform))]
    public class MonoSpacing : BaseMeshEffect
    {
        [SerializeField] private float m_spacing;

        public float HalfCharWidth = 1f;

        public bool UseHalfCharWidth;

        private RectTransform rectTransform;

        private Text text;

        protected MonoSpacing()
        {
        }

        public float Spacing
        {
            get => m_spacing;
            set
            {
                if (m_spacing != value)
                {
                    m_spacing = value;
                    if (graphic != null) graphic.SetVerticesDirty();
                }
            }
        }

        protected override void Awake()
        {
            text = GetComponent<Text>();
            if (text == null)
                Debug.LogWarning("MonoSpacing: Missing Text component");
            else
                rectTransform = text.GetComponent<RectTransform>();
        }

        public override void ModifyMesh(VertexHelper vh)
        {
            if (!IsActive()) return;
            var list = new List<UIVertex>();
            vh.GetUIVertexStream(list);
            var array = this.text.text.Split('\n');
            var num = Spacing * this.text.fontSize / 100f;
            var num2 = 0f;
            var num3 = 0;
            switch (this.text.alignment)
            {
                case TextAnchor.UpperLeft:
                case TextAnchor.MiddleLeft:
                case TextAnchor.LowerLeft:
                    num2 = 0f;
                    break;
                case TextAnchor.UpperCenter:
                case TextAnchor.MiddleCenter:
                case TextAnchor.LowerCenter:
                    num2 = 0.5f;
                    break;
                case TextAnchor.UpperRight:
                case TextAnchor.MiddleRight:
                case TextAnchor.LowerRight:
                    num2 = 1f;
                    break;
            }

            foreach (var text in array)
            {
                var num4 = (text.Length - 1) * num * num2 - (num2 - 0.5f) * rectTransform.rect.width;
                var num5 = 0f - num4 + num / 2f * (1f - num2 * 2f);
                for (var j = 0; j < text.Length; j++)
                {
                    var index = num3 * 6;
                    var index2 = num3 * 6 + 1;
                    var index3 = num3 * 6 + 2;
                    var index4 = num3 * 6 + 3;
                    var index5 = num3 * 6 + 4;
                    var num6 = num3 * 6 + 5;
                    if (num6 > list.Count - 1) return;
                    var value = list[index];
                    var value2 = list[index2];
                    var value3 = list[index3];
                    var value4 = list[index4];
                    var value5 = list[index5];
                    var value6 = list[num6];
                    var x = (value2.position - value.position).x;
                    var flag = UseHalfCharWidth && x < HalfCharWidth;
                    var num7 = !flag ? 0f : (0f - num) / 4f;
                    value.position += new Vector3(0f - value.position.x + num5 + -0.5f * x + num7, 0f, 0f);
                    value2.position += new Vector3(0f - value2.position.x + num5 + 0.5f * x + num7, 0f, 0f);
                    value3.position += new Vector3(0f - value3.position.x + num5 + 0.5f * x + num7, 0f, 0f);
                    value4.position += new Vector3(0f - value4.position.x + num5 + 0.5f * x + num7, 0f, 0f);
                    value5.position += new Vector3(0f - value5.position.x + num5 + -0.5f * x + num7, 0f, 0f);
                    value6.position += new Vector3(0f - value6.position.x + num5 + -0.5f * x + num7, 0f, 0f);
                    num5 = !flag ? num5 + num : num5 + num / 2f;
                    list[index] = value;
                    list[index2] = value2;
                    list[index3] = value3;
                    list[index4] = value4;
                    list[index5] = value5;
                    list[num6] = value6;
                    num3++;
                }

                num3++;
            }

            vh.Clear();
            vh.AddUIVertexTriangleStream(list);
        }
    }
}