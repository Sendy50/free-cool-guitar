namespace UnityEngine.UI.Extensions
{
    [AddComponentMenu("Layout/Extensions/Curved Layout")]
    public class CurvedLayout : LayoutGroup
    {
        public Vector3 CurveOffset;

        [Tooltip("axis along which to place the items, Normalized before use")]
        public Vector3 itemAxis;

        [Tooltip("size of each item along the Normalized axis")]
        public float itemSize;

        public float centerpoint = 0.5f;

        protected override void OnEnable()
        {
            base.OnEnable();
            CalculateRadial();
        }

        public override void SetLayoutHorizontal()
        {
        }

        public override void SetLayoutVertical()
        {
        }

        public override void CalculateLayoutInputVertical()
        {
            CalculateRadial();
        }

        public override void CalculateLayoutInputHorizontal()
        {
            CalculateRadial();
        }

        private void CalculateRadial()
        {
            m_Tracker.Clear();
            if (transform.childCount == 0) return;
            var pivot = new Vector2((int) childAlignment % 3 * 0.5f, (int) childAlignment / 3 * 0.5f);
            var a = new Vector3(GetStartOffset(0, GetTotalPreferredSize(0)),
                GetStartOffset(1, GetTotalPreferredSize(1)), 0f);
            var num = 0f;
            var num2 = 1f / transform.childCount;
            var b = itemAxis.normalized * itemSize;
            for (var i = 0; i < transform.childCount; i++)
            {
                var rectTransform = (RectTransform) transform.GetChild(i);
                if (rectTransform != null)
                {
                    m_Tracker.Add(this, rectTransform,
                        DrivenTransformProperties.Anchors | DrivenTransformProperties.AnchoredPosition |
                        DrivenTransformProperties.Pivot);
                    var a2 = a + b;
                    a = rectTransform.localPosition = a2 + (num - centerpoint) * CurveOffset;
                    rectTransform.pivot = pivot;
                    var vector4 = rectTransform.anchorMin = rectTransform.anchorMax = new Vector2(0.5f, 0.5f);
                    num += num2;
                }
            }
        }
    }
}