namespace UnityEngine.UI.Extensions
{
    [ExecuteInEditMode]
    [AddComponentMenu("UI/Effects/Extensions/SoftMaskScript")]
    public class SoftMaskScript : MonoBehaviour
    {
        [Tooltip("The area that is to be used as the container.")]
        public RectTransform MaskArea;

        [Tooltip("Texture to be used to do the soft alpha")]
        public Texture AlphaMask;

        [Tooltip("At what point to apply the alpha min range 0-1")] [Range(0f, 1f)]
        public float CutOff;

        [Tooltip("Implement a hard blend based on the Cutoff")]
        public bool HardBlend;

        [Tooltip("Flip the masks alpha value")]
        public bool FlipAlphaMask;

        [Tooltip(
            "If a different Mask Scaling Rect is given, and this value is true, the area around the mask will not be clipped")]
        public bool DontClipMaskScalingRect;

        private readonly Vector3[] m_CanvasCorners = new Vector3[4];

        private readonly Vector3[] m_WorldCorners = new Vector3[4];

        private Canvas cachedCanvas;

        private Transform cachedCanvasTransform;

        private Vector2 maskOffset = Vector2.zero;

        private Vector2 maskScale = Vector2.one;
        private Material mat;

        private void Start()
        {
            if (MaskArea == null) MaskArea = GetComponent<RectTransform>();
            var component = GetComponent<Text>();
            if (component != null)
            {
                mat = new Material(Shader.Find("UI Extensions/SoftMaskShader"));
                component.material = mat;
                cachedCanvas = component.canvas;
                cachedCanvasTransform = cachedCanvas.transform;
                if (transform.parent.GetComponent<Mask>() == null) transform.parent.gameObject.AddComponent<Mask>();
                transform.parent.GetComponent<Mask>().enabled = false;
            }
            else
            {
                var component2 = GetComponent<Graphic>();
                if (component2 != null)
                {
                    mat = new Material(Shader.Find("UI Extensions/SoftMaskShader"));
                    component2.material = mat;
                    cachedCanvas = component2.canvas;
                    cachedCanvasTransform = cachedCanvas.transform;
                }
            }
        }

        private void Update()
        {
            if (cachedCanvas != null) SetMask();
        }

        private void SetMask()
        {
            var canvasRect = GetCanvasRect();
            var size = canvasRect.size;
            maskScale.Set(1f / size.x, 1f / size.y);
            maskOffset = -canvasRect.min;
            maskOffset.Scale(maskScale);
            mat.SetTextureOffset("_AlphaMask", maskOffset);
            mat.SetTextureScale("_AlphaMask", maskScale);
            mat.SetTexture("_AlphaMask", AlphaMask);
            mat.SetFloat("_HardBlend", HardBlend ? 1 : 0);
            mat.SetInt("_FlipAlphaMask", FlipAlphaMask ? 1 : 0);
            mat.SetInt("_NoOuterClip", DontClipMaskScalingRect ? 1 : 0);
            mat.SetFloat("_CutOff", CutOff);
        }

        public Rect GetCanvasRect()
        {
            if (cachedCanvas == null) return default;
            MaskArea.GetWorldCorners(m_WorldCorners);
            for (var i = 0; i < 4; i++)
            {
                ref var reference = ref m_CanvasCorners[i];
                reference = cachedCanvasTransform.InverseTransformPoint(m_WorldCorners[i]);
            }

            return new Rect(m_CanvasCorners[0].x, m_CanvasCorners[0].y, m_CanvasCorners[2].x - m_CanvasCorners[0].x,
                m_CanvasCorners[2].y - m_CanvasCorners[0].y);
        }
    }
}