using AssetBundles.GuitarSkin;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.UI;

public class ChordUI : MonoBehaviour
{
    public delegate void OnChordUITouched(ChordUI sender);

    public delegate void OnRemoveButtonTouched(ChordUI sender);

    public int index;
    
    public int hash;

    public Text chordName;

    public Text chordSubName;

    public Text withoutSuffixLetter;

    public Image OnOffImage;

    public Button removeButton;

    public Color selectedTextColor;

    public Color unselectedTextColor;

    public ChordUIHitbox hitbox;

    public Sprite OnBtn;
    public Sprite OffBtn;

    [SerializeField] public GuitarHolder guitarHolder;

    private IGuitarSkin _lastSkin;

    private bool _selected;

    private void Start()
    {
        OnSelectedGuitarChanged(guitarHolder.selectedGuitar);
        guitarHolder.OnSelectedGuitarChanged += OnSelectedGuitarChanged;
        hitbox.OnChordUITouchedDelegate += OnTouchDown;
    }

    private void OnDestroy()
    {
        guitarHolder.OnSelectedGuitarChanged -= OnSelectedGuitarChanged;
    }

    public event OnChordUITouched OnChordUITouchedDelegate;

    public event OnRemoveButtonTouched OnRemoveButtonTouchedDelegate;

    public void SetTouchDetectionEnable(bool enable)
    {
        hitbox.enabled = enable;
    }

    public void SetSelected(bool selected)
    {
        if (selected != _selected)
        {
            _selected = selected;
            SyncUiWithSelectedState();
        }
    }

    [UsedImplicitly]
    public void OnRemoveButtonClick()
    {
        if (OnRemoveButtonTouchedDelegate != null) OnRemoveButtonTouchedDelegate(this);
    }

    private void OnSelectedGuitarChanged(IGuitar guitar)
    {
        guitar.GetSkinProxy().GetSkin(delegate(IGuitarSkin skin, bool success)
        {
            if (success)
            {
                _lastSkin = skin;
                SyncUiWithSelectedState();
            }
        });
    }

    private void OnTouchDown()
    {
        if (this.OnChordUITouchedDelegate != null)
        {
            // if (IAPManager.Instance.IsPrimeMember())
            // {
                this.OnChordUITouchedDelegate(this);
            // }
            // else
            // {
            //     if (index == 0)
            //     {
            //         this.OnChordUITouchedDelegate(this);
            //     }
            //     else
            //     {
            //         IntroManager.Instance.ShowOpenSubChordDialog(this);
            //         Debug.LogError("<== Show Chord Subcription Dialog ==>");
            //     }
            // }
			
        }
    }

    private void SyncUiWithSelectedState()
    {
        if (_lastSkin != null)
        {
            if (_selected)
            {
                OnOffImage.sprite = OnBtn;//astSkin.GetChordButtonOn();
                chordName.color = selectedTextColor;
                withoutSuffixLetter.color = selectedTextColor;
                chordSubName.color = selectedTextColor;
            }
            else
            {
                OnOffImage.sprite = OffBtn;//_lastSkin.GetChordButtonOff();
                chordName.color = unselectedTextColor;
                withoutSuffixLetter.color = unselectedTextColor;
                chordSubName.color = unselectedTextColor;
            }
        }
    }
}