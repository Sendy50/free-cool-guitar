﻿using AssetBundles.GuitarSkin;
using GuitarApplication;
using MWM.Audio;
using MWM.Guitar.GuitarStyle;
using UnityEngine;

public class ChordsStrings : MonoBehaviour
{
    public delegate void OnStringTouched(GuitarString sender, int stringIndex);

    public GuitarString[] guitarStrings;

    public bool[] EnabledStringIndexes = new bool[6]
    {
        true,
        true,
        true,
        true,
        true,
        true
    };

    public GuitarHolder guitarHolder;

    private IMidiChord _previouseChord;

    private SamplingSynthUnit _samplingSynth;

    public ICurrentChordProvider chordProvider { get; private set; }

    private void Start()
    {
        _samplingSynth = ApplicationGraph.GetMusicGamingAudioEngine().GetSamplingSynthUnit();
        guitarHolder.OnSelectedGuitarChanged += OnSelectedGuitarChanged;
        OnSelectedGuitarChanged(guitarHolder.selectedGuitar);
        var array = guitarStrings;
        foreach (var guitarString in array)
        {
            var component = guitarString.GetComponent<ChordsStringCell>();
            component.OnStringTouchedDelegate += HandleStringTouched;
        }
    }

    private void OnDestroy()
    {
        var array = guitarStrings;
        foreach (var guitarString in array)
        {
            var component = guitarString.GetComponent<ChordsStringCell>();
            component.OnStringTouchedDelegate -= HandleStringTouched;
        }

        guitarHolder.OnSelectedGuitarChanged -= OnSelectedGuitarChanged;
    }

    public event OnStringTouched OnStringTouchedDelegate;

    public void SetupChordProvider(ICurrentChordProvider chordProvider)
    {
        if (this.chordProvider != null) this.chordProvider.OnCurrentChodeDidChange -= OnCurrentChodeDidChangeCallback;
        this.chordProvider = chordProvider;
        _previouseChord = MidiChord.DefaultInvalideChord();
        if (chordProvider == null)
        {
            HideAllMutedStrings();
        }
        else if (chordProvider.GetIsMuteRepresentationEnabled())
        {
            UpdateMutedStrings(chordProvider.GetCurrentMidiChord());
            this.chordProvider.OnCurrentChodeDidChange += OnCurrentChodeDidChangeCallback;
        }
        else
        {
            HideAllMutedStrings();
        }
    }

    public void HandleStringTouched(GuitarString touchedString, int stringIndex)
    {
        if (chordProvider != null && EnabledStringIndexes[stringIndex])
        {
            var currentMidiChord = chordProvider.GetCurrentMidiChord();
            var num = currentMidiChord.GetMidiNumbers()[stringIndex];
            if (num == 0 && currentMidiChord.IsSolo()) num = ChordMap.emptyChordMidiNumbers[stringIndex];
            if (!currentMidiChord.IsEqual(_previouseChord))
            {
                _samplingSynth.StopAllNotes(true);
                _previouseChord = currentMidiChord;
            }

            _samplingSynth.NoteOn(0, num, 63);
            if (num != 0) touchedString.StartOscillation();
            if (OnStringTouchedDelegate != null) OnStringTouchedDelegate(touchedString, stringIndex);
        }
    }

    private void OnCurrentChodeDidChangeCallback(ICurrentChordProvider sender, IMidiChord midiChord)
    {
        UpdateMutedStrings(midiChord);
    }

    private void UpdateMutedStrings(IMidiChord midiChord)
    {
        if (midiChord.IsEmptyChord())
        {
            HideAllMutedStrings();
            return;
        }

        if (midiChord.IsSolo())
        {
            HideAllMutedStrings();
            return;
        }

        var midiNumbers = midiChord.GetMidiNumbers();
        for (var i = 0; i < midiNumbers.Length; i++)
            if (midiNumbers[i] == 0)
                guitarStrings[i].ShowMuteIndicator();
            else
                guitarStrings[i].HideMuteIndicator();
    }

    private void HideAllMutedStrings()
    {
        var array = guitarStrings;
        foreach (var guitarString in array) guitarString.HideMuteIndicator();
    }

    private void OnSelectedGuitarChanged(IGuitar guitar)
    {
        guitar.GetSkinProxy().GetSkin(delegate(IGuitarSkin skin, bool success)
        {
            Debug.LogError("OnSelectedGuitarChanged skin null : "+ (skin == null));
            var strings = skin.GetStrings();
            var stringShadow = skin.GetStringShadow();
            for (var i = 0; i < guitarStrings.Length; i++)
            {
                guitarStrings[i].ChangeStringAsset(strings[i]);
                guitarStrings[i].ChangeStringShadowAsset(stringShadow);
            }
        });
    }
}