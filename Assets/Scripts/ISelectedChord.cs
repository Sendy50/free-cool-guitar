using System;

public interface ISelectedChord
{
    event Action<ISelectedChord, IMidiChord> OnSelectedChodChanged;

    void SelectChord(IMidiChord chord);

    IMidiChord GetCurrentMidiChord();

    int GetCurrentMidiChordIndex();
}