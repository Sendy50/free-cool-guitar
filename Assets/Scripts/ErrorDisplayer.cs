﻿public class ErrorDisplayer : IErrorDisplayer
{
    public void Display(string title, string message)
    {
        var mNPopup = new MNPopup(title, message);
        mNPopup.AddAction("OK", delegate { });
        mNPopup.Show();
    }
}