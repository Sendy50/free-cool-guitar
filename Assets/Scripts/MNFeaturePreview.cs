using UnityEngine;

public class MNFeaturePreview : MonoBehaviour
{
    protected int buttonHeight = 50;

    protected int buttonWidth = 200;

    protected float StartX = 10f;

    protected float StartY = 20f;
    protected GUIStyle style;

    protected float XButtonStep = 220f;

    protected float XStartPos = 10f;

    protected float YButtonStep = 60f;

    protected float YLableStep = 40f;

    protected float YStartPos = 10f;

    public virtual void Start()
    {
        InitStyles();
    }

    protected virtual void InitStyles()
    {
        style = new GUIStyle();
        style.normal.textColor = Color.white;
        style.fontSize = 16;
        style.fontStyle = FontStyle.BoldAndItalic;
        style.alignment = TextAnchor.UpperLeft;
        style.wordWrap = true;
    }

    public void UpdateToStartPos()
    {
        StartY = YStartPos;
        StartX = XStartPos;
    }
}