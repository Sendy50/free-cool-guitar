using GuitarApplication;

namespace i18n
{
    public static class StringExtensions
    {
        public static string Translate(this string str)
        {
            var localizationManager = ApplicationGraph.GetLocalizationManager();
            return localizationManager == null ? str : localizationManager.GetLocalizedValue(str.Trim(' ', '\n'));
        }
    }
}