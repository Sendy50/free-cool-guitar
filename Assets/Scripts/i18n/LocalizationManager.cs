using System.Collections.Generic;
using UnityEngine;

namespace i18n
{
    public class LocalizationManager
    {
        private Dictionary<string, string> _defaultlocalizedText;
        private Dictionary<string, string> _localizedText;

        public LocalizationManager()
        {
            LoadLocalizedText(Application.systemLanguage);
        }

        private void LoadLocalizedText(SystemLanguage language)
        {
            _localizedText = new Dictionary<string, string>();
            _defaultlocalizedText = new Dictionary<string, string>();
            var textAsset = Resources.Load("i18n/" + language) as TextAsset;
            if ((bool) textAsset)
            {
                var text = textAsset.text;
                var localizationData = JsonUtility.FromJson<LocalizationData>(text);
                for (var i = 0; i < localizationData.items.Length; i++)
                {
                    _localizedText.Add(localizationData.items[i].key, localizationData.items[i].value);
                }

                if (language == SystemLanguage.English)
                {
                    _defaultlocalizedText = _localizedText;
                    return;
                }

                var textAsset2 = Resources.Load("i18n/" + SystemLanguage.English) as TextAsset;
                if ((bool) textAsset)
                {
                    var text2 = textAsset2.text;
                    var localizationData2 = JsonUtility.FromJson<LocalizationData>(text2);
                    var items = localizationData2.items;
                    foreach (var localizationItem in items)
                    {
                        _defaultlocalizedText.Add(localizationItem.key, localizationItem.value);
                    }
                }
            }
            else if (language != SystemLanguage.English)
            {
                Debug.LogWarning("Cannot find file! Using English");
                LoadLocalizedText(SystemLanguage.English);
            }
            else
            {
                Debug.LogWarning("Cannot find file!");
            }
        }

        public string GetLocalizedValue(string key)
        {
            if (_localizedText.ContainsKey(key))
            {
                return _localizedText[key];
            }

            return !_defaultlocalizedText.ContainsKey(key) ? key : _defaultlocalizedText[key];
        }
    }
}