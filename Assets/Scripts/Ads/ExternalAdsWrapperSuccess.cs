using System;
using System.Collections;
using MWM.Ads;
using UnityEngine;

namespace Ads
{
    public class ExternalAdsWrapperSuccess : ExternalAdsWrapper
    {
        private Action<PlacementInteractionState> _closingCompletion;
        private readonly MonoBehaviour _coroutineExecutor;

        private Coroutine _loadingRoutine;

        private readonly int _maxWaitingDuration;

        private PlacementType _type;

        private bool loadedFlag;

        private bool loadingFlag;

        public ExternalAdsWrapperSuccess(MonoBehaviour coroutineExecutor, IFakeEditorAdView fakeView,
            int maxWaitingDuration = 5)
        {
            _coroutineExecutor = coroutineExecutor;
            this.fakeView = fakeView;
            loadingFlag = false;
            _maxWaitingDuration = maxWaitingDuration;
            fakeView.SetVisibility(false);
        }

        public IFakeEditorAdView fakeView { get; }

        public void LoadContent(PlacementType type, string placementName, Action<PlacementLoadingState> loadingResult,
            bool displayWhenReady = false)
        {
            if (loadingFlag)
            {
                loadingResult?.Invoke(PlacementLoadingState.Loading);
                return;
            }

            if (loadedFlag && type == _type)
            {
                loadingResult?.Invoke(PlacementLoadingState.Loaded);
                return;
            }

            _type = type;
            _coroutineExecutor.StartCoroutine(LoadRoutine(loadingResult));
        }

        public void DisplayContent(PlacementType type, string placementName,
            Action<PlacementShowHideResult> displayCompletion,
            Action<PlacementInteractionState> closingCompletion = null)
        {
            if (!loadedFlag)
            {
                displayCompletion?.Invoke(PlacementShowHideResult.ShowHideFailed);
                return;
            }

            if (_type != type)
            {
                displayCompletion?.Invoke(PlacementShowHideResult.ShowHideFailed);
                return;
            }

            _closingCompletion = closingCompletion;
            fakeView.OnClose += OnClickClose;
            fakeView.SetVisibility(true);
            loadedFlag = false;
            displayCompletion?.Invoke(PlacementShowHideResult.ShowHideSucceeded);
        }

        public bool IsInitialized()
        {
            return true;
        }

        public void RegisterToInitialization(Action onInitialization)
        {
            onInitialization();
        }

        public void UnregisterToInitialization(Action onInitialization)
        {
        }

        private IEnumerator LoadRoutine(Action<PlacementLoadingState> loadingResult)
        {
            loadingFlag = true;
            yield return new WaitForSeconds(_maxWaitingDuration);
            loadingFlag = false;
            loadedFlag = true;
            loadingResult(PlacementLoadingState.Loaded);
        }

        private void OnClickClose(IFakeEditorAdView view)
        {
            if (_closingCompletion != null)
            {
                if (_type == PlacementType.RewardVideo)
                {
                    _closingCompletion(PlacementInteractionState.Rewarded);
                }
                else
                {
                    _closingCompletion(PlacementInteractionState.Clicked);
                }
            }
        }
    }
}