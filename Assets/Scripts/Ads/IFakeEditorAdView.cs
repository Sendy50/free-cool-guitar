using System;

namespace Ads
{
    public interface IFakeEditorAdView
    {
        event Action<IFakeEditorAdView> OnClose;

        void SetVisibility(bool visibility);
    }
}