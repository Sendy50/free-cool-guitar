using System;
using JetBrains.Annotations;
using UnityEngine;

namespace Ads
{
    public class FakeEditorAdView : MonoBehaviour, IFakeEditorAdView
    {
        public event Action<IFakeEditorAdView> OnClose;

        public void SetVisibility(bool visibility)
        {
            gameObject.SetActive(visibility);
        }

        [UsedImplicitly]
        public void OnClickClose()
        {
            SetVisibility(false);
            if (OnClose != null) OnClose(this);
        }
    }
}