using System;
using MWM.Ads;

namespace Ads
{
    public interface ExternalAdsWrapper
    {
        void LoadContent(PlacementType type, string placementName, Action<PlacementLoadingState> loadingResult,
            bool displayWhenReady = false);

        void DisplayContent(PlacementType type, string placementName, Action<PlacementShowHideResult> displayCompletion,
            Action<PlacementInteractionState> closingCompletion = null);

        bool IsInitialized();

        void RegisterToInitialization(Action onInitialization);

        void UnregisterToInitialization(Action onInitialization);
    }
}