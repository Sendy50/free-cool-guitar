using System;
using JetBrains.Annotations;
using MWM.Ads;
using MWM.Audio;
using Utils;

namespace Ads
{
    public class ExternalAdsWrapperImpl : ExternalAdsWrapper
    {
        private readonly MWMMoPubWrapper _moPubWrapper;

        private readonly MusicGamingAudioEngineRef _audioEngine;

        public ExternalAdsWrapperImpl(MWMMoPubWrapper moPubWrapper, MusicGamingAudioEngineRef audioEngine)
        {
            Precondition.CheckNotNull(moPubWrapper);
            Precondition.CheckNotNull(audioEngine);
            _moPubWrapper = moPubWrapper;
            _audioEngine = audioEngine;
        }

        public void LoadContent(PlacementType type, string placementName, Action<PlacementLoadingState> loadingResult,
            bool displayWhenReady = false)
        {
            if (!IsInitialized())
                throw new Exception(
                    "Should not call LoadContent before mopub is initialized, call IsSdkInitialized before");
            _moPubWrapper.LoadContent(type, placementName, loadingResult, displayWhenReady);
        }

        public void DisplayContent(PlacementType type, string placementName,
            Action<PlacementShowHideResult> displayCompletion,
            Action<PlacementInteractionState> closingCompletion = null)
        {
            if (!IsInitialized())
                throw new Exception(
                    "Should not call DisplayContent before mopub is initialized, call IsSdkInitialized before");
            if (_audioEngine.engine != null) _audioEngine.engine.SetAudioRendering(false);
            _moPubWrapper.DisplayContent(type, placementName, displayCompletion,
                delegate(PlacementInteractionState state)
                {
                    if (_audioEngine.engine != null) _audioEngine.engine.SetAudioRendering(true);
                    if (closingCompletion != null) closingCompletion(state);
                    LoadContent(type, placementName, delegate { });
                });
        }

        public bool IsInitialized()
        {
            return _moPubWrapper.MoPubIsInitialized();
        }

        public void RegisterToInitialization(Action onInitialization)
        {
            // _moPubWrapper.OnInitializedEvent += onInitialization;
        }

        public void UnregisterToInitialization(Action onInitialization)
        {
            // _moPubWrapper.OnInitializedEvent -= onInitialization;
        }

        public class MusicGamingAudioEngineRef
        {
            [CanBeNull] public MusicGamingAudioEngine engine;
        }
    }
}