// using AudienceNetwork;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AdViewTest : MonoBehaviour
{
    // private AdView adView;

    private void Awake()
    {
        // var adView = this.adView = new AdView("YOUR_PLACEMENT_ID", AdSize.BANNER_HEIGHT_50);
        // this.adView.Register(gameObject);
        // this.adView.AdViewDidLoad = delegate
        // {
        //     Debug.Log("Ad view loaded.");
        //     this.adView.Show(100.0);
        // };
        // adView.AdViewDidFailWithError = delegate(string error)
        // {
        //     Debug.Log("Ad view failed to load with error: " + error);
        // };
        // adView.AdViewWillLogImpression = delegate { Debug.Log("Ad view logged impression."); };
        // adView.AdViewDidClick = delegate { Debug.Log("Ad view clicked."); };
        // adView.LoadAd();
    }

    private void OnDestroy()
    {
        // if ((bool) adView) adView.Dispose();
        Debug.Log("AdViewTest was destroyed!");
    }

    public void NextScene()
    {
        SceneManager.LoadScene("NativeAdScene");
    }
}