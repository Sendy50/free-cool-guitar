using System;
using System.Collections;
using UnityEngine;

public class StrumSpawnObject : SpawnObject
{
    public float goodFadOutDuration = 0.5f;

    public NormalStrum[] NormalStrums;

    public GoodStrum[] GoodStrums;

    public MissStrum[] MissStrums;

    public Animator tutoAnimator;

    [HideInInspector] public ParticleSystem[] comboParticles;

    private ScoreManager _scoreManager;

    public int stringIndex { get; private set; }

    private void Awake()
    {
        SetOrientation(StrumUserAction.Orientation.Down);
    }

    public override void OnCatched()
    {
        StartCoroutine(OnCatchedRoutine());
    }

    public override void OnMissed()
    {
        StartCoroutine(OnMissRoutine());
    }

    public override void PlayTutoAnimation()
    {
        if (tutoAnimator.gameObject.activeSelf)
            switch (stringIndex)
            {
                case 0:
                    tutoAnimator.SetBool("Strum6TutoAnim", true);
                    break;
                case 1:
                    tutoAnimator.SetBool("Strum5TutoAnim", true);
                    break;
                case 2:
                    tutoAnimator.SetBool("Strum4TutoAnim", true);
                    break;
                case 3:
                    tutoAnimator.SetBool("Strum3TutoAnim", true);
                    break;
            }
    }

    public override void StopTutoAnimation()
    {
        if (tutoAnimator.gameObject.activeSelf)
        {
            tutoAnimator.SetBool("Strum3TutoAnim", false);
            tutoAnimator.SetBool("Strum4TutoAnim", false);
            tutoAnimator.SetBool("Strum5TutoAnim", false);
            tutoAnimator.SetBool("Strum6TutoAnim", false);
        }
    }

    private IEnumerator OnCatchedRoutine()
    {
        NormalStrums[stringIndex].gameObject.SetActive(false);
        var good = GoodStrums[stringIndex];
        good.gameObject.SetActive(true);
        var goodParticle = good.validateParticle;
        mover.StopMovement();
        goodParticle.Play();
        var fadoutCoroutine = StartCoroutine(FadeOut(good, goodFadOutDuration));
        yield return new WaitUntil(() => !goodParticle.isPlaying);
        yield return fadoutCoroutine;
        Reuse();
    }

    private IEnumerator OnMissRoutine()
    {
        NormalStrums[stringIndex].gameObject.SetActive(false);
        MissStrums[stringIndex].gameObject.SetActive(true);
        yield return new WaitForSeconds(0.5f);
        Reuse();
    }

    private IEnumerator FadeOut(GoodStrum strum, float duration)
    {
        var time = 0f;
        var strumStartColor = strum.strumSprite.color;
        var lineStartColor = strum.lineSprite.color;
        var strumEndColor = new Color(strumStartColor.r, strumStartColor.g, strumStartColor.b, 0f);
        var lineEndColor = new Color(lineStartColor.r, lineStartColor.g, lineStartColor.b, 0f);
        while (time < duration)
        {
            yield return null;
            time += Time.deltaTime;
            var t = time / duration;
            strum.strumSprite.color = Color.Lerp(strumStartColor, strumEndColor, t);
            strum.lineSprite.color = Color.Lerp(lineStartColor, lineEndColor, t);
        }

        strum.strumSprite.color = strumEndColor;
        strum.lineSprite.color = lineEndColor;
    }

    public void Reuse()
    {
        gameObject.SetActive(false);
        actionIndex = -1;
        var normalStrums = NormalStrums;
        for (var i = 0; i < normalStrums.Length; i++)
        {
            var normalStrum = normalStrums[i];
            normalStrum.gameObject.SetActive(false);
        }

        var goodStrums = GoodStrums;
        for (var j = 0; j < goodStrums.Length; j++)
        {
            var goodStrum = goodStrums[j];
            goodStrum.strumSprite.color = Color.white;
            goodStrum.lineSprite.color = Color.white;
            goodStrum.gameObject.SetActive(false);
        }

        var missStrums = MissStrums;
        for (var k = 0; k < missStrums.Length; k++)
        {
            var missStrum = missStrums[k];
            missStrum.gameObject.SetActive(false);
        }

        if (_scoreManager != null)
        {
            _scoreManager.OnNewComboDelegate -= OnNewCombo;
            _scoreManager.OnResteDelegate -= OnScoreReste;
        }

        UpdateComboParticle(ScoreManager.Combo.Default());
        StopTutoAnimation();
        if (pool != null) (pool.Target as Pool<StrumSpawnObject>)?.ReleaseObject(this);
    }

    public void OnSpawned(ScoreManager scoreManager)
    {
        _scoreManager = scoreManager;
        UpdateComboParticle(scoreManager.currentCombo);
        _scoreManager.OnNewComboDelegate += OnNewCombo;
        _scoreManager.OnResteDelegate += OnScoreReste;
    }

    public void SetNbStrings(int nbStrings)
    {
        stringIndex = 6 - nbStrings;
        NormalStrums[stringIndex].gameObject.SetActive(true);
    }

    public void SetOrientation(StrumUserAction.Orientation orientation)
    {
        var rotation = gameObject.transform.rotation;
        switch (orientation)
        {
            case StrumUserAction.Orientation.Up:
                rotation.z = 180f;
                break;
            case StrumUserAction.Orientation.Down:
                rotation.z = 0f;
                break;
        }

        gameObject.transform.rotation = rotation;
    }

    private void OnNewCombo(ScoreManager sender, ScoreManager.Combo combo, int consecutiveCatch)
    {
        UpdateComboParticle(combo);
    }

    private void OnScoreReste()
    {
        UpdateComboParticle(_scoreManager.currentCombo);
    }

    private void UpdateComboParticle(ScoreManager.Combo combo)
    {
        var num = (int) combo.scoreMultiplicateur - 2;
        if (num < 0)
        {
            var array = comboParticles;
            foreach (var particleSystem in array)
            {
                particleSystem.Stop();
                particleSystem.gameObject.SetActive(false);
            }

            return;
        }

        for (var j = 0; j < comboParticles.Length; j++)
        {
            var particleSystem2 = comboParticles[j];
            if (j == num)
            {
                particleSystem2.gameObject.SetActive(true);
                particleSystem2.Play();
            }
            else
            {
                particleSystem2.Stop();
                particleSystem2.gameObject.SetActive(false);
            }
        }
    }

    [Serializable]
    public struct NormalStrum
    {
        public GameObject gameObject;

        public SpriteRenderer StrumLine;
    }

    [Serializable]
    public struct GoodStrum
    {
        public GameObject gameObject;

        public ParticleSystem validateParticle;

        public SpriteRenderer strumSprite;

        public SpriteRenderer lineSprite;
    }

    [Serializable]
    public struct MissStrum
    {
        public GameObject gameObject;
    }
}