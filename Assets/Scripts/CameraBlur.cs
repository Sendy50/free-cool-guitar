using UnityEngine;

[ExecuteInEditMode]
public class CameraBlur : MonoBehaviour
{
    public delegate void CameraBlurRenderImageDelegate(RenderTexture originalTexture, RenderTexture blurTexture,
        bool blurActive);

    [Space(10f)] [Header("Blur Param")] public Material BlurMaterial;

    [Range(0f, 10f)] public int Iterations;

    [Range(0f, 4f)] public int DownRes;

    public bool isActive;

    private void OnRenderImage(RenderTexture src, RenderTexture dst)
    {
        Graphics.Blit(src, dst);
        if (!isActive)
        {
            if (onCameraBlurRenderImage != null) onCameraBlurRenderImage(src, null, false);
            return;
        }

        var width = src.width >> DownRes;
        var height = src.height >> DownRes;
        var renderTexture = RenderTexture.GetTemporary(width, height);
        Graphics.Blit(src, renderTexture);
        for (var i = 0; i < Iterations; i++)
        {
            var temporary = RenderTexture.GetTemporary(width, height);
            Graphics.Blit(renderTexture, temporary, BlurMaterial);
            RenderTexture.ReleaseTemporary(renderTexture);
            renderTexture = temporary;
        }

        if (onCameraBlurRenderImage != null) onCameraBlurRenderImage(src, renderTexture, true);
        RenderTexture.ReleaseTemporary(renderTexture);
    }

    public event CameraBlurRenderImageDelegate onCameraBlurRenderImage;

    public void startBlur()
    {
        isActive = true;
    }

    public void startUnBlur()
    {
        isActive = false;
    }
}