﻿using UnityEngine;

[CreateAssetMenu()]
public class SpriteScriptableObject : ScriptableObject 
{
    public Sprite sprite;
}