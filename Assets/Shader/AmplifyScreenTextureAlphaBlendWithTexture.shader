Shader "Amplify/ScreenTextureAlphaBlendWithTexture" {
	Properties {
		[PerRendererData] _MainTex ("Sprite Texture", 2D) = "white" {}
		_Color ("Tint", Vector) = (1,1,1,1)
		_StencilComp ("Stencil Comparison", Float) = 8
		_Stencil ("Stencil ID", Float) = 0
		_StencilOp ("Stencil Operation", Float) = 0
		_StencilWriteMask ("Stencil Write Mask", Float) = 255
		_StencilReadMask ("Stencil Read Mask", Float) = 255
		_ColorMask ("Color Mask", Float) = 15
		[Toggle(UNITY_UI_ALPHACLIP)] _UseUIAlphaClip ("Use Alpha Clip", Float) = 0
		_ScreenTexture ("ScreenTexture", 2D) = "white" {}
		[HideInInspector] _texcoord ("", 2D) = "white" {}
	}
	SubShader {
		Tags { "CanUseSpriteAtlas" = "true" "IGNOREPROJECTOR" = "true" "PreviewType" = "Plane" "QUEUE" = "Transparent" "RenderType" = "Transparent" }
		Pass {
			Name "DEFAULT"
			Tags { "CanUseSpriteAtlas" = "true" "IGNOREPROJECTOR" = "true" "PreviewType" = "Plane" "QUEUE" = "Transparent" "RenderType" = "Transparent" }
			Blend SrcAlpha OneMinusSrcAlpha, SrcAlpha OneMinusSrcAlpha
			ColorMask 0 -1
			ZWrite Off
			Cull Off
			Stencil {
				ReadMask 0
				WriteMask 0
				Comp Disabled
				Pass Keep
				Fail Keep
				ZFail Keep
			}
			GpuProgramID 20615
			Program "vp" {
				SubProgram "gles hw_tier00 " {
					"!!GLES
					#version 100
					
					#ifdef VERTEX
					attribute vec4 _glesVertex;
					attribute vec4 _glesColor;
					attribute vec4 _glesMultiTexCoord0;
					uniform highp vec4 _ProjectionParams;
					uniform highp mat4 unity_ObjectToWorld;
					uniform highp mat4 unity_MatrixVP;
					uniform lowp vec4 _Color;
					varying lowp vec4 xlv_COLOR;
					varying mediump vec2 xlv_TEXCOORD0;
					varying highp vec4 xlv_TEXCOORD1;
					varying highp vec4 xlv_TEXCOORD2;
					void main ()
					{
					  highp vec4 tmpvar_1;
					  tmpvar_1 = _glesVertex;
					  highp vec2 tmpvar_2;
					  tmpvar_2 = _glesMultiTexCoord0.xy;
					  lowp vec4 tmpvar_3;
					  mediump vec2 tmpvar_4;
					  highp vec4 tmpvar_5;
					  tmpvar_5.w = tmpvar_1.w;
					  highp vec4 tmpvar_6;
					  highp vec4 tmpvar_7;
					  tmpvar_7.w = 1.0;
					  tmpvar_7.xyz = tmpvar_1.xyz;
					  tmpvar_6 = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_7));
					  highp vec4 o_8;
					  highp vec4 tmpvar_9;
					  tmpvar_9 = (tmpvar_6 * 0.5);
					  highp vec2 tmpvar_10;
					  tmpvar_10.x = tmpvar_9.x;
					  tmpvar_10.y = (tmpvar_9.y * _ProjectionParams.x);
					  o_8.xy = (tmpvar_10 + tmpvar_9.w);
					  o_8.zw = tmpvar_6.zw;
					  tmpvar_5.xyz = _glesVertex.xyz;
					  highp vec4 tmpvar_11;
					  tmpvar_11.w = 1.0;
					  tmpvar_11.xyz = tmpvar_5.xyz;
					  tmpvar_4 = tmpvar_2;
					  tmpvar_3 = (_glesColor * _Color);
					  gl_Position = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_11));
					  xlv_COLOR = tmpvar_3;
					  xlv_TEXCOORD0 = tmpvar_4;
					  xlv_TEXCOORD1 = tmpvar_5;
					  xlv_TEXCOORD2 = o_8;
					}
					
					
					#endif
					#ifdef FRAGMENT
					uniform highp vec4 _ProjectionParams;
					uniform lowp vec4 _Color;
					uniform highp vec4 _ClipRect;
					uniform sampler2D _MainTex;
					uniform sampler2D _ScreenTexture;
					uniform highp vec4 _MainTex_ST;
					varying mediump vec2 xlv_TEXCOORD0;
					varying highp vec4 xlv_TEXCOORD1;
					varying highp vec4 xlv_TEXCOORD2;
					void main ()
					{
					  lowp vec4 tmpvar_1;
					  mediump vec4 color_2;
					  highp vec4 break7_g1_3;
					  highp vec4 break3_g1_4;
					  highp vec4 o_5;
					  o_5.xzw = xlv_TEXCOORD2.xzw;
					  o_5.y = (xlv_TEXCOORD2.w * 0.5);
					  o_5.y = (((xlv_TEXCOORD2.y - o_5.y) * _ProjectionParams.x) + o_5.y);
					  highp vec4 tmpvar_6;
					  tmpvar_6 = (o_5 / xlv_TEXCOORD2.w);
					  lowp vec4 tmpvar_7;
					  tmpvar_7 = texture2D (_ScreenTexture, tmpvar_6.xy);
					  break3_g1_4 = tmpvar_7;
					  break7_g1_3 = _Color;
					  highp float tmpvar_8;
					  tmpvar_8 = (1.0 - break7_g1_3.w);
					  highp vec4 tmpvar_9;
					  tmpvar_9.xyz = (((break3_g1_4.xyz * break3_g1_4.w) * tmpvar_8) + (break7_g1_3.xyz * break7_g1_3.w));
					  tmpvar_9.w = ((break3_g1_4.w * tmpvar_8) + break7_g1_3.w);
					  highp vec2 tmpvar_10;
					  tmpvar_10 = ((xlv_TEXCOORD0 * _MainTex_ST.xy) + _MainTex_ST.zw);
					  lowp vec4 tmpvar_11;
					  tmpvar_11 = texture2D (_MainTex, tmpvar_10);
					  highp vec4 tmpvar_12;
					  tmpvar_12.xyz = tmpvar_9.xyz;
					  tmpvar_12.w = tmpvar_11.w;
					  color_2 = tmpvar_12;
					  highp float tmpvar_13;
					  highp vec2 tmpvar_14;
					  tmpvar_14.x = float((_ClipRect.z >= xlv_TEXCOORD1.x));
					  tmpvar_14.y = float((_ClipRect.w >= xlv_TEXCOORD1.y));
					  highp vec2 tmpvar_15;
					  tmpvar_15 = (vec2(greaterThanEqual (xlv_TEXCOORD1.xy, _ClipRect.xy)) * tmpvar_14);
					  tmpvar_13 = (tmpvar_15.x * tmpvar_15.y);
					  color_2.w = (color_2.w * tmpvar_13);
					  tmpvar_1 = color_2;
					  gl_FragData[0] = tmpvar_1;
					}
					
					
					#endif"
				}
				SubProgram "gles hw_tier01 " {
					"!!GLES
					#version 100
					
					#ifdef VERTEX
					attribute vec4 _glesVertex;
					attribute vec4 _glesColor;
					attribute vec4 _glesMultiTexCoord0;
					uniform highp vec4 _ProjectionParams;
					uniform highp mat4 unity_ObjectToWorld;
					uniform highp mat4 unity_MatrixVP;
					uniform lowp vec4 _Color;
					varying lowp vec4 xlv_COLOR;
					varying mediump vec2 xlv_TEXCOORD0;
					varying highp vec4 xlv_TEXCOORD1;
					varying highp vec4 xlv_TEXCOORD2;
					void main ()
					{
					  highp vec4 tmpvar_1;
					  tmpvar_1 = _glesVertex;
					  highp vec2 tmpvar_2;
					  tmpvar_2 = _glesMultiTexCoord0.xy;
					  lowp vec4 tmpvar_3;
					  mediump vec2 tmpvar_4;
					  highp vec4 tmpvar_5;
					  tmpvar_5.w = tmpvar_1.w;
					  highp vec4 tmpvar_6;
					  highp vec4 tmpvar_7;
					  tmpvar_7.w = 1.0;
					  tmpvar_7.xyz = tmpvar_1.xyz;
					  tmpvar_6 = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_7));
					  highp vec4 o_8;
					  highp vec4 tmpvar_9;
					  tmpvar_9 = (tmpvar_6 * 0.5);
					  highp vec2 tmpvar_10;
					  tmpvar_10.x = tmpvar_9.x;
					  tmpvar_10.y = (tmpvar_9.y * _ProjectionParams.x);
					  o_8.xy = (tmpvar_10 + tmpvar_9.w);
					  o_8.zw = tmpvar_6.zw;
					  tmpvar_5.xyz = _glesVertex.xyz;
					  highp vec4 tmpvar_11;
					  tmpvar_11.w = 1.0;
					  tmpvar_11.xyz = tmpvar_5.xyz;
					  tmpvar_4 = tmpvar_2;
					  tmpvar_3 = (_glesColor * _Color);
					  gl_Position = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_11));
					  xlv_COLOR = tmpvar_3;
					  xlv_TEXCOORD0 = tmpvar_4;
					  xlv_TEXCOORD1 = tmpvar_5;
					  xlv_TEXCOORD2 = o_8;
					}
					
					
					#endif
					#ifdef FRAGMENT
					uniform highp vec4 _ProjectionParams;
					uniform lowp vec4 _Color;
					uniform highp vec4 _ClipRect;
					uniform sampler2D _MainTex;
					uniform sampler2D _ScreenTexture;
					uniform highp vec4 _MainTex_ST;
					varying mediump vec2 xlv_TEXCOORD0;
					varying highp vec4 xlv_TEXCOORD1;
					varying highp vec4 xlv_TEXCOORD2;
					void main ()
					{
					  lowp vec4 tmpvar_1;
					  mediump vec4 color_2;
					  highp vec4 break7_g1_3;
					  highp vec4 break3_g1_4;
					  highp vec4 o_5;
					  o_5.xzw = xlv_TEXCOORD2.xzw;
					  o_5.y = (xlv_TEXCOORD2.w * 0.5);
					  o_5.y = (((xlv_TEXCOORD2.y - o_5.y) * _ProjectionParams.x) + o_5.y);
					  highp vec4 tmpvar_6;
					  tmpvar_6 = (o_5 / xlv_TEXCOORD2.w);
					  lowp vec4 tmpvar_7;
					  tmpvar_7 = texture2D (_ScreenTexture, tmpvar_6.xy);
					  break3_g1_4 = tmpvar_7;
					  break7_g1_3 = _Color;
					  highp float tmpvar_8;
					  tmpvar_8 = (1.0 - break7_g1_3.w);
					  highp vec4 tmpvar_9;
					  tmpvar_9.xyz = (((break3_g1_4.xyz * break3_g1_4.w) * tmpvar_8) + (break7_g1_3.xyz * break7_g1_3.w));
					  tmpvar_9.w = ((break3_g1_4.w * tmpvar_8) + break7_g1_3.w);
					  highp vec2 tmpvar_10;
					  tmpvar_10 = ((xlv_TEXCOORD0 * _MainTex_ST.xy) + _MainTex_ST.zw);
					  lowp vec4 tmpvar_11;
					  tmpvar_11 = texture2D (_MainTex, tmpvar_10);
					  highp vec4 tmpvar_12;
					  tmpvar_12.xyz = tmpvar_9.xyz;
					  tmpvar_12.w = tmpvar_11.w;
					  color_2 = tmpvar_12;
					  highp float tmpvar_13;
					  highp vec2 tmpvar_14;
					  tmpvar_14.x = float((_ClipRect.z >= xlv_TEXCOORD1.x));
					  tmpvar_14.y = float((_ClipRect.w >= xlv_TEXCOORD1.y));
					  highp vec2 tmpvar_15;
					  tmpvar_15 = (vec2(greaterThanEqual (xlv_TEXCOORD1.xy, _ClipRect.xy)) * tmpvar_14);
					  tmpvar_13 = (tmpvar_15.x * tmpvar_15.y);
					  color_2.w = (color_2.w * tmpvar_13);
					  tmpvar_1 = color_2;
					  gl_FragData[0] = tmpvar_1;
					}
					
					
					#endif"
				}
				SubProgram "gles hw_tier02 " {
					"!!GLES
					#version 100
					
					#ifdef VERTEX
					attribute vec4 _glesVertex;
					attribute vec4 _glesColor;
					attribute vec4 _glesMultiTexCoord0;
					uniform highp vec4 _ProjectionParams;
					uniform highp mat4 unity_ObjectToWorld;
					uniform highp mat4 unity_MatrixVP;
					uniform lowp vec4 _Color;
					varying lowp vec4 xlv_COLOR;
					varying mediump vec2 xlv_TEXCOORD0;
					varying highp vec4 xlv_TEXCOORD1;
					varying highp vec4 xlv_TEXCOORD2;
					void main ()
					{
					  highp vec4 tmpvar_1;
					  tmpvar_1 = _glesVertex;
					  highp vec2 tmpvar_2;
					  tmpvar_2 = _glesMultiTexCoord0.xy;
					  lowp vec4 tmpvar_3;
					  mediump vec2 tmpvar_4;
					  highp vec4 tmpvar_5;
					  tmpvar_5.w = tmpvar_1.w;
					  highp vec4 tmpvar_6;
					  highp vec4 tmpvar_7;
					  tmpvar_7.w = 1.0;
					  tmpvar_7.xyz = tmpvar_1.xyz;
					  tmpvar_6 = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_7));
					  highp vec4 o_8;
					  highp vec4 tmpvar_9;
					  tmpvar_9 = (tmpvar_6 * 0.5);
					  highp vec2 tmpvar_10;
					  tmpvar_10.x = tmpvar_9.x;
					  tmpvar_10.y = (tmpvar_9.y * _ProjectionParams.x);
					  o_8.xy = (tmpvar_10 + tmpvar_9.w);
					  o_8.zw = tmpvar_6.zw;
					  tmpvar_5.xyz = _glesVertex.xyz;
					  highp vec4 tmpvar_11;
					  tmpvar_11.w = 1.0;
					  tmpvar_11.xyz = tmpvar_5.xyz;
					  tmpvar_4 = tmpvar_2;
					  tmpvar_3 = (_glesColor * _Color);
					  gl_Position = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_11));
					  xlv_COLOR = tmpvar_3;
					  xlv_TEXCOORD0 = tmpvar_4;
					  xlv_TEXCOORD1 = tmpvar_5;
					  xlv_TEXCOORD2 = o_8;
					}
					
					
					#endif
					#ifdef FRAGMENT
					uniform highp vec4 _ProjectionParams;
					uniform lowp vec4 _Color;
					uniform highp vec4 _ClipRect;
					uniform sampler2D _MainTex;
					uniform sampler2D _ScreenTexture;
					uniform highp vec4 _MainTex_ST;
					varying mediump vec2 xlv_TEXCOORD0;
					varying highp vec4 xlv_TEXCOORD1;
					varying highp vec4 xlv_TEXCOORD2;
					void main ()
					{
					  lowp vec4 tmpvar_1;
					  mediump vec4 color_2;
					  highp vec4 break7_g1_3;
					  highp vec4 break3_g1_4;
					  highp vec4 o_5;
					  o_5.xzw = xlv_TEXCOORD2.xzw;
					  o_5.y = (xlv_TEXCOORD2.w * 0.5);
					  o_5.y = (((xlv_TEXCOORD2.y - o_5.y) * _ProjectionParams.x) + o_5.y);
					  highp vec4 tmpvar_6;
					  tmpvar_6 = (o_5 / xlv_TEXCOORD2.w);
					  lowp vec4 tmpvar_7;
					  tmpvar_7 = texture2D (_ScreenTexture, tmpvar_6.xy);
					  break3_g1_4 = tmpvar_7;
					  break7_g1_3 = _Color;
					  highp float tmpvar_8;
					  tmpvar_8 = (1.0 - break7_g1_3.w);
					  highp vec4 tmpvar_9;
					  tmpvar_9.xyz = (((break3_g1_4.xyz * break3_g1_4.w) * tmpvar_8) + (break7_g1_3.xyz * break7_g1_3.w));
					  tmpvar_9.w = ((break3_g1_4.w * tmpvar_8) + break7_g1_3.w);
					  highp vec2 tmpvar_10;
					  tmpvar_10 = ((xlv_TEXCOORD0 * _MainTex_ST.xy) + _MainTex_ST.zw);
					  lowp vec4 tmpvar_11;
					  tmpvar_11 = texture2D (_MainTex, tmpvar_10);
					  highp vec4 tmpvar_12;
					  tmpvar_12.xyz = tmpvar_9.xyz;
					  tmpvar_12.w = tmpvar_11.w;
					  color_2 = tmpvar_12;
					  highp float tmpvar_13;
					  highp vec2 tmpvar_14;
					  tmpvar_14.x = float((_ClipRect.z >= xlv_TEXCOORD1.x));
					  tmpvar_14.y = float((_ClipRect.w >= xlv_TEXCOORD1.y));
					  highp vec2 tmpvar_15;
					  tmpvar_15 = (vec2(greaterThanEqual (xlv_TEXCOORD1.xy, _ClipRect.xy)) * tmpvar_14);
					  tmpvar_13 = (tmpvar_15.x * tmpvar_15.y);
					  color_2.w = (color_2.w * tmpvar_13);
					  tmpvar_1 = color_2;
					  gl_FragData[0] = tmpvar_1;
					}
					
					
					#endif"
				}
				SubProgram "gles3 hw_tier00 " {
					"!!GLES3
					#ifdef VERTEX
					#version 300 es
					
					uniform 	vec4 _ProjectionParams;
					uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
					uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
					uniform 	mediump vec4 _Color;
					in highp vec4 in_POSITION0;
					in highp vec4 in_COLOR0;
					in highp vec2 in_TEXCOORD0;
					out mediump vec4 vs_COLOR0;
					out mediump vec2 vs_TEXCOORD0;
					out highp vec4 vs_TEXCOORD1;
					out highp vec4 vs_TEXCOORD2;
					vec4 u_xlat0;
					vec4 u_xlat1;
					void main()
					{
					    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
					    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
					    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
					    u_xlat0 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
					    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
					    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat0.xxxx + u_xlat1;
					    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat0.zzzz + u_xlat1;
					    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat0.wwww + u_xlat1;
					    gl_Position = u_xlat0;
					    u_xlat1 = in_COLOR0 * _Color;
					    vs_COLOR0 = u_xlat1;
					    vs_TEXCOORD0.xy = in_TEXCOORD0.xy;
					    vs_TEXCOORD1 = in_POSITION0;
					    u_xlat0.y = u_xlat0.y * _ProjectionParams.x;
					    u_xlat1.xzw = u_xlat0.xwy * vec3(0.5, 0.5, 0.5);
					    vs_TEXCOORD2.zw = u_xlat0.zw;
					    vs_TEXCOORD2.xy = u_xlat1.zz + u_xlat1.xw;
					    return;
					}
					
					#endif
					#ifdef FRAGMENT
					#version 300 es
					
					precision highp int;
					uniform 	vec4 _ProjectionParams;
					uniform 	mediump vec4 _Color;
					uniform 	vec4 _ClipRect;
					uniform 	vec4 _MainTex_ST;
					uniform lowp sampler2D _ScreenTexture;
					uniform lowp sampler2D _MainTex;
					in mediump vec2 vs_TEXCOORD0;
					in highp vec4 vs_TEXCOORD1;
					in highp vec4 vs_TEXCOORD2;
					layout(location = 0) out mediump vec4 SV_Target0;
					vec4 u_xlat0;
					mediump vec3 u_xlat16_0;
					lowp vec4 u_xlat10_0;
					vec4 u_xlat1;
					mediump vec3 u_xlat16_1;
					bvec4 u_xlatb1;
					float u_xlat2;
					vec2 u_xlat3;
					lowp float u_xlat10_3;
					mediump float u_xlat16_6;
					void main()
					{
					    u_xlat0.x = vs_TEXCOORD2.w * 0.5;
					    u_xlat2 = (-vs_TEXCOORD2.w) * 0.5 + vs_TEXCOORD2.y;
					    u_xlat0.y = u_xlat2 * _ProjectionParams.x + u_xlat0.x;
					    u_xlat0.x = vs_TEXCOORD2.x;
					    u_xlat0.xy = u_xlat0.xy / vs_TEXCOORD2.ww;
					    u_xlat10_0 = texture(_ScreenTexture, u_xlat0.xy);
					    u_xlat16_0.xyz = u_xlat10_0.www * u_xlat10_0.xyz;
					    u_xlat16_6 = (-_Color.w) + 1.0;
					    u_xlat16_1.xyz = _Color.www * _Color.xyz;
					    u_xlat0.xyz = u_xlat16_0.xyz * vec3(u_xlat16_6) + u_xlat16_1.xyz;
					    u_xlatb1.xy = greaterThanEqual(vs_TEXCOORD1.xyxx, _ClipRect.xyxx).xy;
					    u_xlatb1.zw = greaterThanEqual(_ClipRect.zzzw, vs_TEXCOORD1.xxxy).zw;
					    u_xlat1 = mix(vec4(0.0, 0.0, 0.0, 0.0), vec4(1.0, 1.0, 1.0, 1.0), vec4(u_xlatb1));
					    u_xlat1.xy = vec2(u_xlat1.z * u_xlat1.x, u_xlat1.w * u_xlat1.y);
					    u_xlat1.x = u_xlat1.y * u_xlat1.x;
					    u_xlat3.xy = vs_TEXCOORD0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
					    u_xlat10_3 = texture(_MainTex, u_xlat3.xy).w;
					    u_xlat0.w = u_xlat1.x * u_xlat10_3;
					    SV_Target0 = u_xlat0;
					    return;
					}
					
					#endif"
				}
				SubProgram "gles3 hw_tier01 " {
					"!!GLES3
					#ifdef VERTEX
					#version 300 es
					
					uniform 	vec4 _ProjectionParams;
					uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
					uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
					uniform 	mediump vec4 _Color;
					in highp vec4 in_POSITION0;
					in highp vec4 in_COLOR0;
					in highp vec2 in_TEXCOORD0;
					out mediump vec4 vs_COLOR0;
					out mediump vec2 vs_TEXCOORD0;
					out highp vec4 vs_TEXCOORD1;
					out highp vec4 vs_TEXCOORD2;
					vec4 u_xlat0;
					vec4 u_xlat1;
					void main()
					{
					    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
					    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
					    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
					    u_xlat0 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
					    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
					    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat0.xxxx + u_xlat1;
					    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat0.zzzz + u_xlat1;
					    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat0.wwww + u_xlat1;
					    gl_Position = u_xlat0;
					    u_xlat1 = in_COLOR0 * _Color;
					    vs_COLOR0 = u_xlat1;
					    vs_TEXCOORD0.xy = in_TEXCOORD0.xy;
					    vs_TEXCOORD1 = in_POSITION0;
					    u_xlat0.y = u_xlat0.y * _ProjectionParams.x;
					    u_xlat1.xzw = u_xlat0.xwy * vec3(0.5, 0.5, 0.5);
					    vs_TEXCOORD2.zw = u_xlat0.zw;
					    vs_TEXCOORD2.xy = u_xlat1.zz + u_xlat1.xw;
					    return;
					}
					
					#endif
					#ifdef FRAGMENT
					#version 300 es
					
					precision highp int;
					uniform 	vec4 _ProjectionParams;
					uniform 	mediump vec4 _Color;
					uniform 	vec4 _ClipRect;
					uniform 	vec4 _MainTex_ST;
					uniform lowp sampler2D _ScreenTexture;
					uniform lowp sampler2D _MainTex;
					in mediump vec2 vs_TEXCOORD0;
					in highp vec4 vs_TEXCOORD1;
					in highp vec4 vs_TEXCOORD2;
					layout(location = 0) out mediump vec4 SV_Target0;
					vec4 u_xlat0;
					mediump vec3 u_xlat16_0;
					lowp vec4 u_xlat10_0;
					vec4 u_xlat1;
					mediump vec3 u_xlat16_1;
					bvec4 u_xlatb1;
					float u_xlat2;
					vec2 u_xlat3;
					lowp float u_xlat10_3;
					mediump float u_xlat16_6;
					void main()
					{
					    u_xlat0.x = vs_TEXCOORD2.w * 0.5;
					    u_xlat2 = (-vs_TEXCOORD2.w) * 0.5 + vs_TEXCOORD2.y;
					    u_xlat0.y = u_xlat2 * _ProjectionParams.x + u_xlat0.x;
					    u_xlat0.x = vs_TEXCOORD2.x;
					    u_xlat0.xy = u_xlat0.xy / vs_TEXCOORD2.ww;
					    u_xlat10_0 = texture(_ScreenTexture, u_xlat0.xy);
					    u_xlat16_0.xyz = u_xlat10_0.www * u_xlat10_0.xyz;
					    u_xlat16_6 = (-_Color.w) + 1.0;
					    u_xlat16_1.xyz = _Color.www * _Color.xyz;
					    u_xlat0.xyz = u_xlat16_0.xyz * vec3(u_xlat16_6) + u_xlat16_1.xyz;
					    u_xlatb1.xy = greaterThanEqual(vs_TEXCOORD1.xyxx, _ClipRect.xyxx).xy;
					    u_xlatb1.zw = greaterThanEqual(_ClipRect.zzzw, vs_TEXCOORD1.xxxy).zw;
					    u_xlat1 = mix(vec4(0.0, 0.0, 0.0, 0.0), vec4(1.0, 1.0, 1.0, 1.0), vec4(u_xlatb1));
					    u_xlat1.xy = vec2(u_xlat1.z * u_xlat1.x, u_xlat1.w * u_xlat1.y);
					    u_xlat1.x = u_xlat1.y * u_xlat1.x;
					    u_xlat3.xy = vs_TEXCOORD0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
					    u_xlat10_3 = texture(_MainTex, u_xlat3.xy).w;
					    u_xlat0.w = u_xlat1.x * u_xlat10_3;
					    SV_Target0 = u_xlat0;
					    return;
					}
					
					#endif"
				}
				SubProgram "gles3 hw_tier02 " {
					"!!GLES3
					#ifdef VERTEX
					#version 300 es
					
					uniform 	vec4 _ProjectionParams;
					uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
					uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
					uniform 	mediump vec4 _Color;
					in highp vec4 in_POSITION0;
					in highp vec4 in_COLOR0;
					in highp vec2 in_TEXCOORD0;
					out mediump vec4 vs_COLOR0;
					out mediump vec2 vs_TEXCOORD0;
					out highp vec4 vs_TEXCOORD1;
					out highp vec4 vs_TEXCOORD2;
					vec4 u_xlat0;
					vec4 u_xlat1;
					void main()
					{
					    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
					    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
					    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
					    u_xlat0 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
					    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
					    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat0.xxxx + u_xlat1;
					    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat0.zzzz + u_xlat1;
					    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat0.wwww + u_xlat1;
					    gl_Position = u_xlat0;
					    u_xlat1 = in_COLOR0 * _Color;
					    vs_COLOR0 = u_xlat1;
					    vs_TEXCOORD0.xy = in_TEXCOORD0.xy;
					    vs_TEXCOORD1 = in_POSITION0;
					    u_xlat0.y = u_xlat0.y * _ProjectionParams.x;
					    u_xlat1.xzw = u_xlat0.xwy * vec3(0.5, 0.5, 0.5);
					    vs_TEXCOORD2.zw = u_xlat0.zw;
					    vs_TEXCOORD2.xy = u_xlat1.zz + u_xlat1.xw;
					    return;
					}
					
					#endif
					#ifdef FRAGMENT
					#version 300 es
					
					precision highp int;
					uniform 	vec4 _ProjectionParams;
					uniform 	mediump vec4 _Color;
					uniform 	vec4 _ClipRect;
					uniform 	vec4 _MainTex_ST;
					uniform lowp sampler2D _ScreenTexture;
					uniform lowp sampler2D _MainTex;
					in mediump vec2 vs_TEXCOORD0;
					in highp vec4 vs_TEXCOORD1;
					in highp vec4 vs_TEXCOORD2;
					layout(location = 0) out mediump vec4 SV_Target0;
					vec4 u_xlat0;
					mediump vec3 u_xlat16_0;
					lowp vec4 u_xlat10_0;
					vec4 u_xlat1;
					mediump vec3 u_xlat16_1;
					bvec4 u_xlatb1;
					float u_xlat2;
					vec2 u_xlat3;
					lowp float u_xlat10_3;
					mediump float u_xlat16_6;
					void main()
					{
					    u_xlat0.x = vs_TEXCOORD2.w * 0.5;
					    u_xlat2 = (-vs_TEXCOORD2.w) * 0.5 + vs_TEXCOORD2.y;
					    u_xlat0.y = u_xlat2 * _ProjectionParams.x + u_xlat0.x;
					    u_xlat0.x = vs_TEXCOORD2.x;
					    u_xlat0.xy = u_xlat0.xy / vs_TEXCOORD2.ww;
					    u_xlat10_0 = texture(_ScreenTexture, u_xlat0.xy);
					    u_xlat16_0.xyz = u_xlat10_0.www * u_xlat10_0.xyz;
					    u_xlat16_6 = (-_Color.w) + 1.0;
					    u_xlat16_1.xyz = _Color.www * _Color.xyz;
					    u_xlat0.xyz = u_xlat16_0.xyz * vec3(u_xlat16_6) + u_xlat16_1.xyz;
					    u_xlatb1.xy = greaterThanEqual(vs_TEXCOORD1.xyxx, _ClipRect.xyxx).xy;
					    u_xlatb1.zw = greaterThanEqual(_ClipRect.zzzw, vs_TEXCOORD1.xxxy).zw;
					    u_xlat1 = mix(vec4(0.0, 0.0, 0.0, 0.0), vec4(1.0, 1.0, 1.0, 1.0), vec4(u_xlatb1));
					    u_xlat1.xy = vec2(u_xlat1.z * u_xlat1.x, u_xlat1.w * u_xlat1.y);
					    u_xlat1.x = u_xlat1.y * u_xlat1.x;
					    u_xlat3.xy = vs_TEXCOORD0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
					    u_xlat10_3 = texture(_MainTex, u_xlat3.xy).w;
					    u_xlat0.w = u_xlat1.x * u_xlat10_3;
					    SV_Target0 = u_xlat0;
					    return;
					}
					
					#endif"
				}
				SubProgram "gles hw_tier00 " {
					Keywords { "UNITY_UI_ALPHACLIP" }
					"!!GLES
					#version 100
					
					#ifdef VERTEX
					attribute vec4 _glesVertex;
					attribute vec4 _glesColor;
					attribute vec4 _glesMultiTexCoord0;
					uniform highp vec4 _ProjectionParams;
					uniform highp mat4 unity_ObjectToWorld;
					uniform highp mat4 unity_MatrixVP;
					uniform lowp vec4 _Color;
					varying lowp vec4 xlv_COLOR;
					varying mediump vec2 xlv_TEXCOORD0;
					varying highp vec4 xlv_TEXCOORD1;
					varying highp vec4 xlv_TEXCOORD2;
					void main ()
					{
					  highp vec4 tmpvar_1;
					  tmpvar_1 = _glesVertex;
					  highp vec2 tmpvar_2;
					  tmpvar_2 = _glesMultiTexCoord0.xy;
					  lowp vec4 tmpvar_3;
					  mediump vec2 tmpvar_4;
					  highp vec4 tmpvar_5;
					  tmpvar_5.w = tmpvar_1.w;
					  highp vec4 tmpvar_6;
					  highp vec4 tmpvar_7;
					  tmpvar_7.w = 1.0;
					  tmpvar_7.xyz = tmpvar_1.xyz;
					  tmpvar_6 = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_7));
					  highp vec4 o_8;
					  highp vec4 tmpvar_9;
					  tmpvar_9 = (tmpvar_6 * 0.5);
					  highp vec2 tmpvar_10;
					  tmpvar_10.x = tmpvar_9.x;
					  tmpvar_10.y = (tmpvar_9.y * _ProjectionParams.x);
					  o_8.xy = (tmpvar_10 + tmpvar_9.w);
					  o_8.zw = tmpvar_6.zw;
					  tmpvar_5.xyz = _glesVertex.xyz;
					  highp vec4 tmpvar_11;
					  tmpvar_11.w = 1.0;
					  tmpvar_11.xyz = tmpvar_5.xyz;
					  tmpvar_4 = tmpvar_2;
					  tmpvar_3 = (_glesColor * _Color);
					  gl_Position = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_11));
					  xlv_COLOR = tmpvar_3;
					  xlv_TEXCOORD0 = tmpvar_4;
					  xlv_TEXCOORD1 = tmpvar_5;
					  xlv_TEXCOORD2 = o_8;
					}
					
					
					#endif
					#ifdef FRAGMENT
					uniform highp vec4 _ProjectionParams;
					uniform lowp vec4 _Color;
					uniform highp vec4 _ClipRect;
					uniform sampler2D _MainTex;
					uniform sampler2D _ScreenTexture;
					uniform highp vec4 _MainTex_ST;
					varying mediump vec2 xlv_TEXCOORD0;
					varying highp vec4 xlv_TEXCOORD1;
					varying highp vec4 xlv_TEXCOORD2;
					void main ()
					{
					  lowp vec4 tmpvar_1;
					  mediump vec4 color_2;
					  highp vec4 break7_g1_3;
					  highp vec4 break3_g1_4;
					  highp vec4 o_5;
					  o_5.xzw = xlv_TEXCOORD2.xzw;
					  o_5.y = (xlv_TEXCOORD2.w * 0.5);
					  o_5.y = (((xlv_TEXCOORD2.y - o_5.y) * _ProjectionParams.x) + o_5.y);
					  highp vec4 tmpvar_6;
					  tmpvar_6 = (o_5 / xlv_TEXCOORD2.w);
					  lowp vec4 tmpvar_7;
					  tmpvar_7 = texture2D (_ScreenTexture, tmpvar_6.xy);
					  break3_g1_4 = tmpvar_7;
					  break7_g1_3 = _Color;
					  highp float tmpvar_8;
					  tmpvar_8 = (1.0 - break7_g1_3.w);
					  highp vec4 tmpvar_9;
					  tmpvar_9.xyz = (((break3_g1_4.xyz * break3_g1_4.w) * tmpvar_8) + (break7_g1_3.xyz * break7_g1_3.w));
					  tmpvar_9.w = ((break3_g1_4.w * tmpvar_8) + break7_g1_3.w);
					  highp vec2 tmpvar_10;
					  tmpvar_10 = ((xlv_TEXCOORD0 * _MainTex_ST.xy) + _MainTex_ST.zw);
					  lowp vec4 tmpvar_11;
					  tmpvar_11 = texture2D (_MainTex, tmpvar_10);
					  highp vec4 tmpvar_12;
					  tmpvar_12.xyz = tmpvar_9.xyz;
					  tmpvar_12.w = tmpvar_11.w;
					  color_2 = tmpvar_12;
					  highp float tmpvar_13;
					  highp vec2 tmpvar_14;
					  tmpvar_14.x = float((_ClipRect.z >= xlv_TEXCOORD1.x));
					  tmpvar_14.y = float((_ClipRect.w >= xlv_TEXCOORD1.y));
					  highp vec2 tmpvar_15;
					  tmpvar_15 = (vec2(greaterThanEqual (xlv_TEXCOORD1.xy, _ClipRect.xy)) * tmpvar_14);
					  tmpvar_13 = (tmpvar_15.x * tmpvar_15.y);
					  color_2.w = (color_2.w * tmpvar_13);
					  mediump float x_16;
					  x_16 = (color_2.w - 0.001);
					  if ((x_16 < 0.0)) {
					    discard;
					  };
					  tmpvar_1 = color_2;
					  gl_FragData[0] = tmpvar_1;
					}
					
					
					#endif"
				}
				SubProgram "gles hw_tier01 " {
					Keywords { "UNITY_UI_ALPHACLIP" }
					"!!GLES
					#version 100
					
					#ifdef VERTEX
					attribute vec4 _glesVertex;
					attribute vec4 _glesColor;
					attribute vec4 _glesMultiTexCoord0;
					uniform highp vec4 _ProjectionParams;
					uniform highp mat4 unity_ObjectToWorld;
					uniform highp mat4 unity_MatrixVP;
					uniform lowp vec4 _Color;
					varying lowp vec4 xlv_COLOR;
					varying mediump vec2 xlv_TEXCOORD0;
					varying highp vec4 xlv_TEXCOORD1;
					varying highp vec4 xlv_TEXCOORD2;
					void main ()
					{
					  highp vec4 tmpvar_1;
					  tmpvar_1 = _glesVertex;
					  highp vec2 tmpvar_2;
					  tmpvar_2 = _glesMultiTexCoord0.xy;
					  lowp vec4 tmpvar_3;
					  mediump vec2 tmpvar_4;
					  highp vec4 tmpvar_5;
					  tmpvar_5.w = tmpvar_1.w;
					  highp vec4 tmpvar_6;
					  highp vec4 tmpvar_7;
					  tmpvar_7.w = 1.0;
					  tmpvar_7.xyz = tmpvar_1.xyz;
					  tmpvar_6 = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_7));
					  highp vec4 o_8;
					  highp vec4 tmpvar_9;
					  tmpvar_9 = (tmpvar_6 * 0.5);
					  highp vec2 tmpvar_10;
					  tmpvar_10.x = tmpvar_9.x;
					  tmpvar_10.y = (tmpvar_9.y * _ProjectionParams.x);
					  o_8.xy = (tmpvar_10 + tmpvar_9.w);
					  o_8.zw = tmpvar_6.zw;
					  tmpvar_5.xyz = _glesVertex.xyz;
					  highp vec4 tmpvar_11;
					  tmpvar_11.w = 1.0;
					  tmpvar_11.xyz = tmpvar_5.xyz;
					  tmpvar_4 = tmpvar_2;
					  tmpvar_3 = (_glesColor * _Color);
					  gl_Position = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_11));
					  xlv_COLOR = tmpvar_3;
					  xlv_TEXCOORD0 = tmpvar_4;
					  xlv_TEXCOORD1 = tmpvar_5;
					  xlv_TEXCOORD2 = o_8;
					}
					
					
					#endif
					#ifdef FRAGMENT
					uniform highp vec4 _ProjectionParams;
					uniform lowp vec4 _Color;
					uniform highp vec4 _ClipRect;
					uniform sampler2D _MainTex;
					uniform sampler2D _ScreenTexture;
					uniform highp vec4 _MainTex_ST;
					varying mediump vec2 xlv_TEXCOORD0;
					varying highp vec4 xlv_TEXCOORD1;
					varying highp vec4 xlv_TEXCOORD2;
					void main ()
					{
					  lowp vec4 tmpvar_1;
					  mediump vec4 color_2;
					  highp vec4 break7_g1_3;
					  highp vec4 break3_g1_4;
					  highp vec4 o_5;
					  o_5.xzw = xlv_TEXCOORD2.xzw;
					  o_5.y = (xlv_TEXCOORD2.w * 0.5);
					  o_5.y = (((xlv_TEXCOORD2.y - o_5.y) * _ProjectionParams.x) + o_5.y);
					  highp vec4 tmpvar_6;
					  tmpvar_6 = (o_5 / xlv_TEXCOORD2.w);
					  lowp vec4 tmpvar_7;
					  tmpvar_7 = texture2D (_ScreenTexture, tmpvar_6.xy);
					  break3_g1_4 = tmpvar_7;
					  break7_g1_3 = _Color;
					  highp float tmpvar_8;
					  tmpvar_8 = (1.0 - break7_g1_3.w);
					  highp vec4 tmpvar_9;
					  tmpvar_9.xyz = (((break3_g1_4.xyz * break3_g1_4.w) * tmpvar_8) + (break7_g1_3.xyz * break7_g1_3.w));
					  tmpvar_9.w = ((break3_g1_4.w * tmpvar_8) + break7_g1_3.w);
					  highp vec2 tmpvar_10;
					  tmpvar_10 = ((xlv_TEXCOORD0 * _MainTex_ST.xy) + _MainTex_ST.zw);
					  lowp vec4 tmpvar_11;
					  tmpvar_11 = texture2D (_MainTex, tmpvar_10);
					  highp vec4 tmpvar_12;
					  tmpvar_12.xyz = tmpvar_9.xyz;
					  tmpvar_12.w = tmpvar_11.w;
					  color_2 = tmpvar_12;
					  highp float tmpvar_13;
					  highp vec2 tmpvar_14;
					  tmpvar_14.x = float((_ClipRect.z >= xlv_TEXCOORD1.x));
					  tmpvar_14.y = float((_ClipRect.w >= xlv_TEXCOORD1.y));
					  highp vec2 tmpvar_15;
					  tmpvar_15 = (vec2(greaterThanEqual (xlv_TEXCOORD1.xy, _ClipRect.xy)) * tmpvar_14);
					  tmpvar_13 = (tmpvar_15.x * tmpvar_15.y);
					  color_2.w = (color_2.w * tmpvar_13);
					  mediump float x_16;
					  x_16 = (color_2.w - 0.001);
					  if ((x_16 < 0.0)) {
					    discard;
					  };
					  tmpvar_1 = color_2;
					  gl_FragData[0] = tmpvar_1;
					}
					
					
					#endif"
				}
				SubProgram "gles hw_tier02 " {
					Keywords { "UNITY_UI_ALPHACLIP" }
					"!!GLES
					#version 100
					
					#ifdef VERTEX
					attribute vec4 _glesVertex;
					attribute vec4 _glesColor;
					attribute vec4 _glesMultiTexCoord0;
					uniform highp vec4 _ProjectionParams;
					uniform highp mat4 unity_ObjectToWorld;
					uniform highp mat4 unity_MatrixVP;
					uniform lowp vec4 _Color;
					varying lowp vec4 xlv_COLOR;
					varying mediump vec2 xlv_TEXCOORD0;
					varying highp vec4 xlv_TEXCOORD1;
					varying highp vec4 xlv_TEXCOORD2;
					void main ()
					{
					  highp vec4 tmpvar_1;
					  tmpvar_1 = _glesVertex;
					  highp vec2 tmpvar_2;
					  tmpvar_2 = _glesMultiTexCoord0.xy;
					  lowp vec4 tmpvar_3;
					  mediump vec2 tmpvar_4;
					  highp vec4 tmpvar_5;
					  tmpvar_5.w = tmpvar_1.w;
					  highp vec4 tmpvar_6;
					  highp vec4 tmpvar_7;
					  tmpvar_7.w = 1.0;
					  tmpvar_7.xyz = tmpvar_1.xyz;
					  tmpvar_6 = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_7));
					  highp vec4 o_8;
					  highp vec4 tmpvar_9;
					  tmpvar_9 = (tmpvar_6 * 0.5);
					  highp vec2 tmpvar_10;
					  tmpvar_10.x = tmpvar_9.x;
					  tmpvar_10.y = (tmpvar_9.y * _ProjectionParams.x);
					  o_8.xy = (tmpvar_10 + tmpvar_9.w);
					  o_8.zw = tmpvar_6.zw;
					  tmpvar_5.xyz = _glesVertex.xyz;
					  highp vec4 tmpvar_11;
					  tmpvar_11.w = 1.0;
					  tmpvar_11.xyz = tmpvar_5.xyz;
					  tmpvar_4 = tmpvar_2;
					  tmpvar_3 = (_glesColor * _Color);
					  gl_Position = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_11));
					  xlv_COLOR = tmpvar_3;
					  xlv_TEXCOORD0 = tmpvar_4;
					  xlv_TEXCOORD1 = tmpvar_5;
					  xlv_TEXCOORD2 = o_8;
					}
					
					
					#endif
					#ifdef FRAGMENT
					uniform highp vec4 _ProjectionParams;
					uniform lowp vec4 _Color;
					uniform highp vec4 _ClipRect;
					uniform sampler2D _MainTex;
					uniform sampler2D _ScreenTexture;
					uniform highp vec4 _MainTex_ST;
					varying mediump vec2 xlv_TEXCOORD0;
					varying highp vec4 xlv_TEXCOORD1;
					varying highp vec4 xlv_TEXCOORD2;
					void main ()
					{
					  lowp vec4 tmpvar_1;
					  mediump vec4 color_2;
					  highp vec4 break7_g1_3;
					  highp vec4 break3_g1_4;
					  highp vec4 o_5;
					  o_5.xzw = xlv_TEXCOORD2.xzw;
					  o_5.y = (xlv_TEXCOORD2.w * 0.5);
					  o_5.y = (((xlv_TEXCOORD2.y - o_5.y) * _ProjectionParams.x) + o_5.y);
					  highp vec4 tmpvar_6;
					  tmpvar_6 = (o_5 / xlv_TEXCOORD2.w);
					  lowp vec4 tmpvar_7;
					  tmpvar_7 = texture2D (_ScreenTexture, tmpvar_6.xy);
					  break3_g1_4 = tmpvar_7;
					  break7_g1_3 = _Color;
					  highp float tmpvar_8;
					  tmpvar_8 = (1.0 - break7_g1_3.w);
					  highp vec4 tmpvar_9;
					  tmpvar_9.xyz = (((break3_g1_4.xyz * break3_g1_4.w) * tmpvar_8) + (break7_g1_3.xyz * break7_g1_3.w));
					  tmpvar_9.w = ((break3_g1_4.w * tmpvar_8) + break7_g1_3.w);
					  highp vec2 tmpvar_10;
					  tmpvar_10 = ((xlv_TEXCOORD0 * _MainTex_ST.xy) + _MainTex_ST.zw);
					  lowp vec4 tmpvar_11;
					  tmpvar_11 = texture2D (_MainTex, tmpvar_10);
					  highp vec4 tmpvar_12;
					  tmpvar_12.xyz = tmpvar_9.xyz;
					  tmpvar_12.w = tmpvar_11.w;
					  color_2 = tmpvar_12;
					  highp float tmpvar_13;
					  highp vec2 tmpvar_14;
					  tmpvar_14.x = float((_ClipRect.z >= xlv_TEXCOORD1.x));
					  tmpvar_14.y = float((_ClipRect.w >= xlv_TEXCOORD1.y));
					  highp vec2 tmpvar_15;
					  tmpvar_15 = (vec2(greaterThanEqual (xlv_TEXCOORD1.xy, _ClipRect.xy)) * tmpvar_14);
					  tmpvar_13 = (tmpvar_15.x * tmpvar_15.y);
					  color_2.w = (color_2.w * tmpvar_13);
					  mediump float x_16;
					  x_16 = (color_2.w - 0.001);
					  if ((x_16 < 0.0)) {
					    discard;
					  };
					  tmpvar_1 = color_2;
					  gl_FragData[0] = tmpvar_1;
					}
					
					
					#endif"
				}
				SubProgram "gles3 hw_tier00 " {
					Keywords { "UNITY_UI_ALPHACLIP" }
					"!!GLES3
					#ifdef VERTEX
					#version 300 es
					
					uniform 	vec4 _ProjectionParams;
					uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
					uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
					uniform 	mediump vec4 _Color;
					in highp vec4 in_POSITION0;
					in highp vec4 in_COLOR0;
					in highp vec2 in_TEXCOORD0;
					out mediump vec4 vs_COLOR0;
					out mediump vec2 vs_TEXCOORD0;
					out highp vec4 vs_TEXCOORD1;
					out highp vec4 vs_TEXCOORD2;
					vec4 u_xlat0;
					vec4 u_xlat1;
					void main()
					{
					    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
					    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
					    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
					    u_xlat0 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
					    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
					    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat0.xxxx + u_xlat1;
					    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat0.zzzz + u_xlat1;
					    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat0.wwww + u_xlat1;
					    gl_Position = u_xlat0;
					    u_xlat1 = in_COLOR0 * _Color;
					    vs_COLOR0 = u_xlat1;
					    vs_TEXCOORD0.xy = in_TEXCOORD0.xy;
					    vs_TEXCOORD1 = in_POSITION0;
					    u_xlat0.y = u_xlat0.y * _ProjectionParams.x;
					    u_xlat1.xzw = u_xlat0.xwy * vec3(0.5, 0.5, 0.5);
					    vs_TEXCOORD2.zw = u_xlat0.zw;
					    vs_TEXCOORD2.xy = u_xlat1.zz + u_xlat1.xw;
					    return;
					}
					
					#endif
					#ifdef FRAGMENT
					#version 300 es
					
					precision highp int;
					uniform 	vec4 _ProjectionParams;
					uniform 	mediump vec4 _Color;
					uniform 	vec4 _ClipRect;
					uniform 	vec4 _MainTex_ST;
					uniform lowp sampler2D _ScreenTexture;
					uniform lowp sampler2D _MainTex;
					in mediump vec2 vs_TEXCOORD0;
					in highp vec4 vs_TEXCOORD1;
					in highp vec4 vs_TEXCOORD2;
					layout(location = 0) out mediump vec4 SV_Target0;
					vec4 u_xlat0;
					bvec4 u_xlatb0;
					mediump float u_xlat16_1;
					lowp vec4 u_xlat10_1;
					vec2 u_xlat2;
					mediump vec3 u_xlat16_2;
					bool u_xlatb2;
					mediump vec3 u_xlat16_3;
					vec2 u_xlat4;
					lowp float u_xlat10_4;
					float u_xlat6;
					mediump float u_xlat16_14;
					void main()
					{
					    u_xlatb0.xy = greaterThanEqual(vs_TEXCOORD1.xyxx, _ClipRect.xyxx).xy;
					    u_xlatb0.zw = greaterThanEqual(_ClipRect.zzzw, vs_TEXCOORD1.xxxy).zw;
					    u_xlat0 = mix(vec4(0.0, 0.0, 0.0, 0.0), vec4(1.0, 1.0, 1.0, 1.0), vec4(u_xlatb0));
					    u_xlat0.xy = vec2(u_xlat0.z * u_xlat0.x, u_xlat0.w * u_xlat0.y);
					    u_xlat0.x = u_xlat0.y * u_xlat0.x;
					    u_xlat4.xy = vs_TEXCOORD0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
					    u_xlat10_4 = texture(_MainTex, u_xlat4.xy).w;
					    u_xlat16_1 = u_xlat10_4 * u_xlat0.x + -0.00100000005;
					    u_xlat0.w = u_xlat0.x * u_xlat10_4;
					#ifdef UNITY_ADRENO_ES3
					    u_xlatb2 = !!(u_xlat16_1<0.0);
					#else
					    u_xlatb2 = u_xlat16_1<0.0;
					#endif
					    if((int(u_xlatb2) * int(0xffffffffu))!=0){discard;}
					    u_xlat2.x = vs_TEXCOORD2.w * 0.5;
					    u_xlat6 = (-vs_TEXCOORD2.w) * 0.5 + vs_TEXCOORD2.y;
					    u_xlat2.y = u_xlat6 * _ProjectionParams.x + u_xlat2.x;
					    u_xlat2.x = vs_TEXCOORD2.x;
					    u_xlat2.xy = u_xlat2.xy / vs_TEXCOORD2.ww;
					    u_xlat10_1 = texture(_ScreenTexture, u_xlat2.xy);
					    u_xlat16_2.xyz = u_xlat10_1.www * u_xlat10_1.xyz;
					    u_xlat16_14 = (-_Color.w) + 1.0;
					    u_xlat16_3.xyz = _Color.www * _Color.xyz;
					    u_xlat0.xyz = u_xlat16_2.xyz * vec3(u_xlat16_14) + u_xlat16_3.xyz;
					    SV_Target0 = u_xlat0;
					    return;
					}
					
					#endif"
				}
				SubProgram "gles3 hw_tier01 " {
					Keywords { "UNITY_UI_ALPHACLIP" }
					"!!GLES3
					#ifdef VERTEX
					#version 300 es
					
					uniform 	vec4 _ProjectionParams;
					uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
					uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
					uniform 	mediump vec4 _Color;
					in highp vec4 in_POSITION0;
					in highp vec4 in_COLOR0;
					in highp vec2 in_TEXCOORD0;
					out mediump vec4 vs_COLOR0;
					out mediump vec2 vs_TEXCOORD0;
					out highp vec4 vs_TEXCOORD1;
					out highp vec4 vs_TEXCOORD2;
					vec4 u_xlat0;
					vec4 u_xlat1;
					void main()
					{
					    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
					    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
					    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
					    u_xlat0 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
					    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
					    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat0.xxxx + u_xlat1;
					    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat0.zzzz + u_xlat1;
					    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat0.wwww + u_xlat1;
					    gl_Position = u_xlat0;
					    u_xlat1 = in_COLOR0 * _Color;
					    vs_COLOR0 = u_xlat1;
					    vs_TEXCOORD0.xy = in_TEXCOORD0.xy;
					    vs_TEXCOORD1 = in_POSITION0;
					    u_xlat0.y = u_xlat0.y * _ProjectionParams.x;
					    u_xlat1.xzw = u_xlat0.xwy * vec3(0.5, 0.5, 0.5);
					    vs_TEXCOORD2.zw = u_xlat0.zw;
					    vs_TEXCOORD2.xy = u_xlat1.zz + u_xlat1.xw;
					    return;
					}
					
					#endif
					#ifdef FRAGMENT
					#version 300 es
					
					precision highp int;
					uniform 	vec4 _ProjectionParams;
					uniform 	mediump vec4 _Color;
					uniform 	vec4 _ClipRect;
					uniform 	vec4 _MainTex_ST;
					uniform lowp sampler2D _ScreenTexture;
					uniform lowp sampler2D _MainTex;
					in mediump vec2 vs_TEXCOORD0;
					in highp vec4 vs_TEXCOORD1;
					in highp vec4 vs_TEXCOORD2;
					layout(location = 0) out mediump vec4 SV_Target0;
					vec4 u_xlat0;
					bvec4 u_xlatb0;
					mediump float u_xlat16_1;
					lowp vec4 u_xlat10_1;
					vec2 u_xlat2;
					mediump vec3 u_xlat16_2;
					bool u_xlatb2;
					mediump vec3 u_xlat16_3;
					vec2 u_xlat4;
					lowp float u_xlat10_4;
					float u_xlat6;
					mediump float u_xlat16_14;
					void main()
					{
					    u_xlatb0.xy = greaterThanEqual(vs_TEXCOORD1.xyxx, _ClipRect.xyxx).xy;
					    u_xlatb0.zw = greaterThanEqual(_ClipRect.zzzw, vs_TEXCOORD1.xxxy).zw;
					    u_xlat0 = mix(vec4(0.0, 0.0, 0.0, 0.0), vec4(1.0, 1.0, 1.0, 1.0), vec4(u_xlatb0));
					    u_xlat0.xy = vec2(u_xlat0.z * u_xlat0.x, u_xlat0.w * u_xlat0.y);
					    u_xlat0.x = u_xlat0.y * u_xlat0.x;
					    u_xlat4.xy = vs_TEXCOORD0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
					    u_xlat10_4 = texture(_MainTex, u_xlat4.xy).w;
					    u_xlat16_1 = u_xlat10_4 * u_xlat0.x + -0.00100000005;
					    u_xlat0.w = u_xlat0.x * u_xlat10_4;
					#ifdef UNITY_ADRENO_ES3
					    u_xlatb2 = !!(u_xlat16_1<0.0);
					#else
					    u_xlatb2 = u_xlat16_1<0.0;
					#endif
					    if((int(u_xlatb2) * int(0xffffffffu))!=0){discard;}
					    u_xlat2.x = vs_TEXCOORD2.w * 0.5;
					    u_xlat6 = (-vs_TEXCOORD2.w) * 0.5 + vs_TEXCOORD2.y;
					    u_xlat2.y = u_xlat6 * _ProjectionParams.x + u_xlat2.x;
					    u_xlat2.x = vs_TEXCOORD2.x;
					    u_xlat2.xy = u_xlat2.xy / vs_TEXCOORD2.ww;
					    u_xlat10_1 = texture(_ScreenTexture, u_xlat2.xy);
					    u_xlat16_2.xyz = u_xlat10_1.www * u_xlat10_1.xyz;
					    u_xlat16_14 = (-_Color.w) + 1.0;
					    u_xlat16_3.xyz = _Color.www * _Color.xyz;
					    u_xlat0.xyz = u_xlat16_2.xyz * vec3(u_xlat16_14) + u_xlat16_3.xyz;
					    SV_Target0 = u_xlat0;
					    return;
					}
					
					#endif"
				}
				SubProgram "gles3 hw_tier02 " {
					Keywords { "UNITY_UI_ALPHACLIP" }
					"!!GLES3
					#ifdef VERTEX
					#version 300 es
					
					uniform 	vec4 _ProjectionParams;
					uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
					uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
					uniform 	mediump vec4 _Color;
					in highp vec4 in_POSITION0;
					in highp vec4 in_COLOR0;
					in highp vec2 in_TEXCOORD0;
					out mediump vec4 vs_COLOR0;
					out mediump vec2 vs_TEXCOORD0;
					out highp vec4 vs_TEXCOORD1;
					out highp vec4 vs_TEXCOORD2;
					vec4 u_xlat0;
					vec4 u_xlat1;
					void main()
					{
					    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
					    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
					    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
					    u_xlat0 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
					    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
					    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat0.xxxx + u_xlat1;
					    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat0.zzzz + u_xlat1;
					    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat0.wwww + u_xlat1;
					    gl_Position = u_xlat0;
					    u_xlat1 = in_COLOR0 * _Color;
					    vs_COLOR0 = u_xlat1;
					    vs_TEXCOORD0.xy = in_TEXCOORD0.xy;
					    vs_TEXCOORD1 = in_POSITION0;
					    u_xlat0.y = u_xlat0.y * _ProjectionParams.x;
					    u_xlat1.xzw = u_xlat0.xwy * vec3(0.5, 0.5, 0.5);
					    vs_TEXCOORD2.zw = u_xlat0.zw;
					    vs_TEXCOORD2.xy = u_xlat1.zz + u_xlat1.xw;
					    return;
					}
					
					#endif
					#ifdef FRAGMENT
					#version 300 es
					
					precision highp int;
					uniform 	vec4 _ProjectionParams;
					uniform 	mediump vec4 _Color;
					uniform 	vec4 _ClipRect;
					uniform 	vec4 _MainTex_ST;
					uniform lowp sampler2D _ScreenTexture;
					uniform lowp sampler2D _MainTex;
					in mediump vec2 vs_TEXCOORD0;
					in highp vec4 vs_TEXCOORD1;
					in highp vec4 vs_TEXCOORD2;
					layout(location = 0) out mediump vec4 SV_Target0;
					vec4 u_xlat0;
					bvec4 u_xlatb0;
					mediump float u_xlat16_1;
					lowp vec4 u_xlat10_1;
					vec2 u_xlat2;
					mediump vec3 u_xlat16_2;
					bool u_xlatb2;
					mediump vec3 u_xlat16_3;
					vec2 u_xlat4;
					lowp float u_xlat10_4;
					float u_xlat6;
					mediump float u_xlat16_14;
					void main()
					{
					    u_xlatb0.xy = greaterThanEqual(vs_TEXCOORD1.xyxx, _ClipRect.xyxx).xy;
					    u_xlatb0.zw = greaterThanEqual(_ClipRect.zzzw, vs_TEXCOORD1.xxxy).zw;
					    u_xlat0 = mix(vec4(0.0, 0.0, 0.0, 0.0), vec4(1.0, 1.0, 1.0, 1.0), vec4(u_xlatb0));
					    u_xlat0.xy = vec2(u_xlat0.z * u_xlat0.x, u_xlat0.w * u_xlat0.y);
					    u_xlat0.x = u_xlat0.y * u_xlat0.x;
					    u_xlat4.xy = vs_TEXCOORD0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
					    u_xlat10_4 = texture(_MainTex, u_xlat4.xy).w;
					    u_xlat16_1 = u_xlat10_4 * u_xlat0.x + -0.00100000005;
					    u_xlat0.w = u_xlat0.x * u_xlat10_4;
					#ifdef UNITY_ADRENO_ES3
					    u_xlatb2 = !!(u_xlat16_1<0.0);
					#else
					    u_xlatb2 = u_xlat16_1<0.0;
					#endif
					    if((int(u_xlatb2) * int(0xffffffffu))!=0){discard;}
					    u_xlat2.x = vs_TEXCOORD2.w * 0.5;
					    u_xlat6 = (-vs_TEXCOORD2.w) * 0.5 + vs_TEXCOORD2.y;
					    u_xlat2.y = u_xlat6 * _ProjectionParams.x + u_xlat2.x;
					    u_xlat2.x = vs_TEXCOORD2.x;
					    u_xlat2.xy = u_xlat2.xy / vs_TEXCOORD2.ww;
					    u_xlat10_1 = texture(_ScreenTexture, u_xlat2.xy);
					    u_xlat16_2.xyz = u_xlat10_1.www * u_xlat10_1.xyz;
					    u_xlat16_14 = (-_Color.w) + 1.0;
					    u_xlat16_3.xyz = _Color.www * _Color.xyz;
					    u_xlat0.xyz = u_xlat16_2.xyz * vec3(u_xlat16_14) + u_xlat16_3.xyz;
					    SV_Target0 = u_xlat0;
					    return;
					}
					
					#endif"
				}
			}
			Program "fp" {
				SubProgram "gles hw_tier00 " {
					"!!GLES"
				}
				SubProgram "gles hw_tier01 " {
					"!!GLES"
				}
				SubProgram "gles hw_tier02 " {
					"!!GLES"
				}
				SubProgram "gles3 hw_tier00 " {
					"!!GLES3"
				}
				SubProgram "gles3 hw_tier01 " {
					"!!GLES3"
				}
				SubProgram "gles3 hw_tier02 " {
					"!!GLES3"
				}
				SubProgram "gles hw_tier00 " {
					Keywords { "UNITY_UI_ALPHACLIP" }
					"!!GLES"
				}
				SubProgram "gles hw_tier01 " {
					Keywords { "UNITY_UI_ALPHACLIP" }
					"!!GLES"
				}
				SubProgram "gles hw_tier02 " {
					Keywords { "UNITY_UI_ALPHACLIP" }
					"!!GLES"
				}
				SubProgram "gles3 hw_tier00 " {
					Keywords { "UNITY_UI_ALPHACLIP" }
					"!!GLES3"
				}
				SubProgram "gles3 hw_tier01 " {
					Keywords { "UNITY_UI_ALPHACLIP" }
					"!!GLES3"
				}
				SubProgram "gles3 hw_tier02 " {
					Keywords { "UNITY_UI_ALPHACLIP" }
					"!!GLES3"
				}
			}
		}
	}
	CustomEditor "ASEMaterialInspector"
}