﻿using System;
using UnityEngine;

public class PlugInTest : MonoBehaviour
{
    //const string pluginName = "com.sendy.unitytestl.MyPlugIn";
    const string pluginName = "nikss.paradva.unitynikbilling.MyPlugIn";
    //const string pluginName = "com.sendy.pickimage.MyPlugIn";

    static AndroidJavaClass _pluginClass;
    static AndroidJavaObject _pluginInstance;

    /***
 * responce code
 int SERVICE_TIMEOUT = -3;
 int FEATURE_NOT_SUPPORTED = -2;
 int SERVICE_DISCONNECTED = -1;
 int OK = 0;
 int USER_CANCELED = 1;
 int SERVICE_UNAVAILABLE = 2;
 int BILLING_UNAVAILABLE = 3;
 int ITEM_UNAVAILABLE = 4;
 int DEVELOPER_ERROR = 5;
 int ERROR = 6;
 int ITEM_ALREADY_OWNED = 7;
 int ITEM_NOT_OWNED = 8;
 */


    class AndroidHistoryCallback : AndroidJavaProxy
    {
        public AndroidHistoryCallback() : base("nikss.paradva.unitynikbilling.PurchaseCheckListner")
        {
            
        }
        public void onPurchasesChecker(bool b)
        {
            Debug.LogError("<== onPurchasesChecker : "+b);
            PlayerPrefs.SetInt("IsPremium",(b == true) ? 1:0);
            PlayerPrefs.Save();
        }
        
    }

    class AndroidPurchageCallback : AndroidJavaProxy
    {
        public AndroidPurchageCallback() : base("nikss.paradva.unitynikbilling.PurchaseListner")
        {
        }


        public void onPurchasesUpdated(int responseCode, string productid, string purchaseToken)
        {
            Debug.LogError("ENTER callback responseCode: " + responseCode);
            Debug.Log("ENTER callback onSuccess: " + productid);
            Debug.Log("ENTER callback purchaseToken: " + purchaseToken);

            if (responseCode == 0)
            {
                Debug.LogError("Purchase Success " + productid);
                IAPManager.Instance.onsuccess();
            }
            else
            {
                Debug.LogError("Purchase Failed " + productid);
                IAPManager.Instance.onFailed();
                // DIYIAPManager.Instance.PurchaseFailedHandler(productid);
            }
        }
    }


    private static AndroidJavaObject currentActivity
    {
        get
        {
            return new AndroidJavaClass("com.unity3d.player.UnityPlayer").GetStatic<AndroidJavaObject>(
                "currentActivity");
        }
    }

    public static AndroidJavaClass PluginClass
    {
        get
        {
            if (_pluginClass == null)
            {
                _pluginClass = new AndroidJavaClass(pluginName);
            }

            return _pluginClass;
        }
    }

    public static AndroidJavaObject PluginInstance
    {
        get
        {
            if (_pluginInstance == null)
            {
                _pluginInstance = PluginClass.CallStatic<AndroidJavaObject>("getInstance");
            }

            return _pluginInstance;
        }
    }

    /**
     * nikss start
     */
    public static void setBranchChannel(String channelid)
    {
        Debug.LogError("channelid :  Nikss==> "+channelid);
        PluginInstance.Call("setBranchChannel", channelid);
    }
    
    public static void setBranchAaid(String aaid)
    {
        Debug.LogError("aaid :  Nikss==> "+aaid);
        PluginInstance.Call("setBranchAaid", aaid);
    }
    /**
     * nikss  end
     */
    
    public static void setBranch2Billing(String channelid, String aaid)
    {
        PluginInstance.Call("setBranch2Billing", channelid, aaid);
    }
    
    
    

    // step2
    public static void billingInit(String[] productids)
    {
        Debug.LogError("<== BILLING INIT START ==> : " + productids[0]);
        PluginInstance.Call("billingInit", currentActivity, new AndroidPurchageCallback(), productids);
        IsPremium(productids[0]);
    }

    // step3
    public static void startPurchase(String productid)
    {
        Debug.LogError("startPurchase " + productid);
        PluginInstance.Call("startPurchase", productid);
    }

    public static void IsPremium(string productId)
    {
        Debug.LogError("<== IsPremium CHECK ==> : " + productId);
        PluginInstance.Call("checkPurchase", productId, new AndroidHistoryCallback());
    }
}