﻿using System;
using System.Collections;
using System.Collections.Generic;
using GuitarApplication;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class IntroManager : MonoBehaviour
{
    public Canvas _canvas;
    public GameObject _introPanel1;
    public GameObject _introPanel2;
    public GameObject _introPanel3;
    public GameObject _dialogPanel1;
    public GameObject _videoPanel;
    public GameObject _dialogPanel2;
    public GameObject _subscribeChordDialog;
    public GameObject _subscribeBackDialog;
    public GameObject _toastView;
    public GameObject _noInternetAccess;
    // public GameObject blackFilter;
    public AudioClip[] Clip;
    public static IntroManager Instance;
    private ChordUI _chordUI;

    private void Awake()
    {
        Instance = this;
    }

    private void Update()
    {
        if (Application.platform == RuntimePlatform.Android)
        {
            if (Input.GetKey(KeyCode.Escape))
            {
                // Insert Code Here (I.E. Load Scene, Etc)
                // OR Application.Quit();
                if(!_canvas.enabled   && !IAPManager.Instance.IsPrimeMember())
                    ShowOpenSubBackDialog();
                
                return;
            }
        }
    }

    public void Retry()
    {
        StartCoroutine(RetryConnection());
    }
    
    public IEnumerator RetryConnection()
    {
        _noInternetAccess.transform.GetChild(2).GetComponent<Button>().interactable = false;
        _noInternetAccess.transform.GetChild(2).transform.GetChild(0).gameObject.SetActive(false);
        _noInternetAccess.transform.GetChild(2).transform.GetChild(1).gameObject.SetActive(true);
       
        StartCoroutine(checkInternetConnection((isConnected) =>
        {
            if (isConnected)
            {
                Debug.LogError("InternetOn");
                // StartCoroutine(Start());
                StartCoroutine(Start());
                BranceAndFirebaseInit._instance.BranchInit();
            }
            else
            {
                Debug.LogError("InternetOff");
            }

        }));
        
        yield return new WaitForSeconds(5);
        _noInternetAccess.transform.GetChild(2).GetComponent<Button>().interactable = true;
        _noInternetAccess.transform.GetChild(2).transform.GetChild(0).gameObject.SetActive(true);
        _noInternetAccess.transform.GetChild(2).transform.GetChild(1).gameObject.SetActive(false);
    }
    
    IEnumerator checkInternetConnection(Action<bool> action){
        WWW www = new WWW("http://google.com");
        yield return www;
        if (www.error != null) {
            action (false);
        } else {
            action (true);
        }
    } 

    
    private IEnumerator Start()
    {
        if (Application.internetReachability != 0)
        {
            //if(!IAPManager.Instance.Init)
            ApplicationGraph.GetApplicationRouter().ShowLoader();
            IAPManager.Instance.intIAP();
            
            Debug.LogError("<== WAIT FOR IAP_INTILIZE ==>");
           // yield return new WaitUntil(() => IAPManager.Instance.Init);
           yield return new WaitForSeconds(0);
            Debug.LogError("<== WAIT COMPLETE FOR IAP_INTILIZE ==>  init : "+IAPManager.Instance.Init);
            
            // _noInternetAccess.transform.GetChild(2).GetComponent<Button>().interactable = true;
            // _noInternetAccess.transform.GetChild(2).transform.GetChild(0).gameObject.SetActive(true);
            // _noInternetAccess.transform.GetChild(2).transform.GetChild(1).gameObject.SetActive(false);
           
            _noInternetAccess.SetActive(false);
            if (!IAPManager.Instance.IsPrimeMember())
            {
                _canvas.enabled = true;
                ApplicationGraph.GetApplicationRouter().HideLoader();
                Screen.orientation = ScreenOrientation.Portrait;
                _introPanel1.SetActive(true);
                _introPanel2.SetActive(false);
                _dialogPanel1.SetActive(false);
                _videoPanel.SetActive(false);
                _dialogPanel2.SetActive(false);
            }
            else
            {
                ApplicationGraph.GetApplicationRouter().HideLoader();
                Screen.orientation = ScreenOrientation.LandscapeLeft;
            }
        }
        else
        {
            if (!IAPManager.Instance.IsPrimeMember())
            {
                _canvas.enabled = true;
                ApplicationGraph.GetApplicationRouter().HideLoader();
                _noInternetAccess.SetActive(true);
            }
            else
            {
                ApplicationGraph.GetApplicationRouter().HideLoader();
                Screen.orientation = ScreenOrientation.LandscapeLeft;
            }
        }
    }
    
    
    public void Panel1NextBtnClick()
    {
        _introPanel1.SetActive(false);
        _introPanel2.SetActive(true);
        GetComponent<AudioSource>().PlayOneShot(Clip[0]);
    }
    
    public void Panel2NextBtnClick()
    {
        _introPanel2.SetActive(false);
        _introPanel3.SetActive(true);
        GetComponent<AudioSource>().PlayOneShot(Clip[1]);
        // BranceAndFirebaseInit._instance.SendBranceEvent("pay_view1");
        // _subscribePanel3.transform.GetChild(1).transform.GetChild(3).transform.GetChild(1).GetComponent<TMP_Text>().text = "3-day free trial,then "+IAPManager.Instance.price +"/month cancel anytime "; 
        // GetComponent<AudioSource>().PlayOneShot(Clip[1]);
        // BranceAndFirebaseInit._instance.ShowCloseBtn(_subscribePanel3.transform.GetChild(2).gameObject);
    }
    
    public void Panel3NextBtnClick(bool Close )
    {
        _videoPanel.SetActive(true);
        _dialogPanel1.SetActive(true);
        _introPanel3.SetActive(false);
        BranceAndFirebaseInit._instance.SendBranceEvent("pay_pageview1");
        GetComponent<AudioSource>().PlayOneShot(Clip[2]);
        _dialogPanel1.transform.GetChild(1).transform.GetChild(0).transform.GetChild(2).transform.GetChild(1).GetComponent<TMP_Text>().text = "3-day free trial,then "+IAPManager.Instance.price +"/year ";
        BranceAndFirebaseInit._instance.ShowCloseBtn(_dialogPanel1.transform.GetChild(2).gameObject);
    }

    // public void Sub3CloseBtnClick(bool Close )
    // {
    //     if(Close)
    //         BranceAndFirebaseInit._instance.SendBranceEvent("pay_close1_click");
    //     
    //     _subscribePanel3.SetActive(false);
    //     //Start 1 Dialog Process
    //     _dialogPanel1.SetActive(true);
    //     BranceAndFirebaseInit._instance.SendBranceEvent("pay_view2");
    //     
    //     
    //     _dialogPanel1.transform.GetChild(0).transform.GetChild(0).transform.GetChild(1).GetComponent<TMP_Text>().text = "3-day free trial,then "+IAPManager.Instance.price +"/month cancel anytime ";
    //     BranceAndFirebaseInit._instance.ShowCloseBtn(_dialogPanel1.transform.GetChild(1).gameObject);
    // }
    
    // public void Sub3PurchageBtnClick()
    // {
    //     BranceAndFirebaseInit._instance.SendBranceEvent("start_trial1_click");
    //     GetComponent<AudioSource>().PlayOneShot(Clip[2]);
    //     IAPManager.Instance.PurchageStart(1);
    // }
    
    public void Dialog1CloseBtnClick(bool Close)
    {
        // if(Close)
        //     BranceAndFirebaseInit._instance.SendBranceEvent("pay_close2_click");
        
       
        // Debug.LogError("1 Dialog 2 Open");
        
        _dialogPanel1.transform.GetChild(2).gameObject.SetActive(false);
        _dialogPanel2.SetActive(true);
        // Debug.LogError("2 Dialog 2 Open");
        
        BranceAndFirebaseInit._instance.SendBranceEvent("redeem_pageview");
        _dialogPanel2.transform.GetChild(0).gameObject.SetActive(false);
        _dialogPanel2.transform.GetChild(1).transform.GetChild(0).transform.GetChild(3).transform.GetChild(0).gameObject.SetActive(true);
        _dialogPanel2.transform.GetChild(1).transform.GetChild(0).transform.GetChild(1).GetComponent<TMP_Text>().text = "After setting, you can play guitar normally. Creating amazing sounds!";
        _dialogPanel2.transform.GetChild(1).transform.GetChild(0).transform.GetChild(3).GetComponent<Button>().interactable = true;
        _dialogPanel2.transform.GetChild(1).transform.GetChild(0).transform.GetChild(4).GetComponent<Button>().interactable = false;
        _dialogPanel2.transform.GetChild(1).transform.GetChild(0).transform.GetChild(4).transform.GetChild(0).gameObject.SetActive(false);
        _dialogPanel2.transform.GetChild(2).gameObject.SetActive(false);
        
    }

    
    public void Dialog1PurchageBtnClick()
    {
        BranceAndFirebaseInit._instance.SendBranceEvent("start_trial1_click");
        IAPManager.Instance.PurchageStart(1);
    }
    
    public void PermissionBtnClick()
    {
        BranceAndFirebaseInit._instance.SendBranceEvent("click_to_set click");
        BranceAndFirebaseInit._instance.SendBranceEvent("redeem_pageview");
       
        _dialogPanel2.transform.GetChild(1).transform.GetChild(0).transform.GetChild(3).transform.GetChild(0).gameObject.SetActive(false);
        _dialogPanel2.transform.GetChild(1).transform.GetChild(0).transform.GetChild(1).GetComponent<TMP_Text>().text = "Start free trial for 3 days, after the period, it will be " + IAPManager.Instance.price + "/year.";//"<color=#94DDFF><b>3-day free trial</b></color>, then "+IAPManager.Instance.price+"/month cancel anytime";
        _dialogPanel2.transform.GetChild(1).transform.GetChild(0).transform.GetChild(3).GetComponent<Button>().interactable = false;
        _dialogPanel2.transform.GetChild(1).transform.GetChild(0).transform.GetChild(4).GetComponent<Button>().interactable = true;
        _dialogPanel2.transform.GetChild(1).transform.GetChild(0).transform.GetChild(4).transform.GetChild(0).gameObject.SetActive(true);
        _dialogPanel2.transform.GetChild(2).gameObject.SetActive(true);
        BranceAndFirebaseInit._instance.ShowCloseBtn(_dialogPanel2.transform.GetChild(0).gameObject);
    }
    
    public void Dialog2CloseBtnClick(bool Close)
    {
        // if(Close)
        //     BranceAndFirebaseInit._instance.SendBranceEvent("pay_close4_click");
        
        //_canvas.enabled = false;
        //_dialogPanel1.SetActive(false);
        _dialogPanel2.SetActive(false);
        BranceAndFirebaseInit._instance.ShowCloseBtn(_dialogPanel1.transform.GetChild(2).gameObject);
        // Screen.orientation = ScreenOrientation.LandscapeLeft;
    }
    
    public void Dialog2PurchageBtnClick()
    {
        BranceAndFirebaseInit._instance.SendBranceEvent("start_trial_click_redeem_page");
        IAPManager.Instance.PurchageStart(2);
    }
    
    public void IntroPanelPurchageSuccess(int Index)
    {
        // blackFilter.SetActive(value: false);
        IAPManager.Instance.UpdatePremium();
        _canvas.enabled = false;
        _dialogPanel1.SetActive(false);
        _videoPanel.SetActive(false);
        _dialogPanel2.SetActive(false);
        Screen.orientation = ScreenOrientation.LandscapeLeft;
        Debug.LogError(" <- PURCHAGE_SUCCESS ->");
       
        if (Index == 1)
        {
            BranceAndFirebaseInit._instance.SendBranceEvent("start_trial1");
        }
        else if(Index == 2)
        {
            BranceAndFirebaseInit._instance.SendBranceEvent("start_trial_redeem_page");
        }
        else if(Index == 9)
        {
            //BranceAndFirebaseInit._instance.SendBranceEvent("start_trial2");
        }
    }
    
    public void IntroPanelPurchageFailed(int Index)
    {
        Debug.LogError(" <- PURCHAGE_FAILED ->");
        if (Index == 1)
        {
            Dialog1CloseBtnClick(false);
        }
        else if(Index == 2)
        {
            Dialog2CloseBtnClick(false);
        }
        else if(Index == 9)
        {
            CloseSubBackDialog(false);
        }
    }

    // public void ShowOpenSubChordDialog(ChordUI chordUI)
    // {
    //     BranceAndFirebaseInit._instance.SendBranceEvent("pay_view2");
    //     _chordUI = chordUI;
    //     _canvas.enabled = true;
    //     _subscribeChordDialog.SetActive(true);
    //     _subscribeChordDialog.transform.GetChild(0).transform.GetChild(0).transform.GetChild(1).GetComponent<TMP_Text>().text = "3-day free trial,then "+IAPManager.Instance.price +"/month cancel anytime ";
    //     BranceAndFirebaseInit._instance.ShowCloseBtn(_subscribeChordDialog.transform.GetChild(1).gameObject);
    //     
    // }
    
    // public void CloseSubChordDialog(bool Close )
    // {
    //     if(Close)
    //         BranceAndFirebaseInit._instance.SendBranceEvent("pay_close2_click");
    //     
    //     // blackFilter.SetActive(value: false);
    //     _canvas.enabled = false;
    //     _subscribeChordDialog.SetActive(false);
    //     _subscribeChordDialog.transform.GetChild(1).gameObject.SetActive(false);
    // }

    // public void SubChordDialogBtnClick()
    // {
    //     BranceAndFirebaseInit._instance.SendBranceEvent("start_trial2_click");
    //     IAPManager.Instance.PurchageStart(8);
    // }
    
    // public void SubChordDialogPurchageSuccess()
    // {
    //     BranceAndFirebaseInit._instance.SendBranceEvent("start_trial2");
    //     CloseSubChordDialog(false);
    //     IntroManager.Instance.ShowOpenSubChordDialog(_chordUI);
    // }
    
    public void ShowOpenSubBackDialog()
    {
        BranceAndFirebaseInit._instance.SendBranceEvent("pay_view2");
        _canvas.enabled = true;
        _subscribeBackDialog.SetActive(true);
        _subscribeBackDialog.transform.GetChild(0).transform.GetChild(0).transform.GetChild(1).GetComponent<TMP_Text>().text = "3-day free trial,then "+IAPManager.Instance.price +"/month cancel anytime ";
        BranceAndFirebaseInit._instance.ShowCloseBtn(_subscribeBackDialog.transform.GetChild(1).gameObject);
    }
    
    public void CloseSubBackDialog(bool Close )
    {
        if(Close)
            BranceAndFirebaseInit._instance.SendBranceEvent("pay_close2_click");
        
        // blackFilter.SetActive(value: false);
        _canvas.enabled = false;
        _subscribeBackDialog.SetActive(false);
        _subscribeBackDialog.transform.GetChild(1).gameObject.SetActive(false);
    }
    
    public void SubBackDialogBtnClick()
    {
        BranceAndFirebaseInit._instance.SendBranceEvent("start_trial2_click");
        IAPManager.Instance.PurchageStart(9);
    }

    public void ShowToast(string msg="")
    {
        if (!_toastView.activeInHierarchy)
        {
            StartCoroutine(ShowToastView(msg));
        }
    }

    private IEnumerator ShowToastView(string msg)
    {
        _toastView.SetActive(false);
        _toastView.SetActive(true);
        _toastView.transform.GetChild(0).GetComponent<Text>().text = msg;
       yield return new WaitForSeconds(2.1f);
       _toastView.SetActive(false);
    }
    
    public void purchageFromStore()
    {
        IAPManager.Instance.PurchageStart(10);
    }
    
}
