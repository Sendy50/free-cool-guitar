﻿using System;
using System.Collections;
using System.Collections.Generic;
using GoogleMobileAds.Api;
using UnityEngine;
using UnityEngine.Advertisements;
using UnityEngine.PlayerLoop;

public class BranceAndFirebaseInit : MonoBehaviour
{
    public static  BranceAndFirebaseInit _instance;
    private string _advertisingId = "";
    public string[] _productId;
    public int Loop3Screen = 0;
    //private string _branchChannelId = "";
    
    

    private void Awake()
    {
        _instance = this;
        //DontDestroyOnLoad(this.gameObject);
    }
    
    
    
    // Start is called before the first frame update
    void Start()
    {
        MobileAds.Initialize(initStatus =>
        {
            Debug.LogError("advertisingId @@@@");
            Debug.LogError("Initialize Admob @@@@");
            Application.RequestAdvertisingIdentifierAsync (
                (string advertisingId, bool trackingEnabled, string error) =>
                {
                    Debug.LogError("advertisingId " + advertisingId + " " + trackingEnabled + " " + error);
                    _advertisingId = advertisingId;
                    //PlugInTest.setBranchAaid(_advertisingId);
                }
            );
        });

        print("@Sendy Branch Init Start");
        Branch.setDebug();
        Branch.setTrackingDisabled(false);
        BranchInit();
    }
    
    
    
    public void BranchInit()
    {
        Debug.LogError("<== BRANCH INIT START ==>");
        PlugInTest.billingInit(_productId);
        Branch.initSession(CallbackWithBranchUniversalObject);
    }

    
    
    // public void CallbackWithBranchUniversalObject(BranchUniversalObject universalObject, BranchLinkProperties linkProperties, string error) {
    //     Debug.LogError("Branch linkProperties : " + linkProperties.channel);
    //    
    //     if (error != null) {
    //         Debug.LogError("<== BRANCH INIT FAILED ==> : "+error);
    //         //BranchInit();
    //     }
    //     else if (linkProperties.controlParams.Count > 0) {
    //         Debug.LogError("Deeplink params : "+ universalObject.ToJsonString() + linkProperties.ToJsonString());
    //     }
    //     else 
    //     {
    //         _branchChannelId = (linkProperties.channel != "") ? linkProperties.channel : ""; 
    //         Debug.LogError("<== BRANCH INIT COMPLETE ==>");
    //         PlugInTest.setBranchChannel(_branchChannelId);
    //         // PlugInTest.setBranch2Billing((linkProperties.channel != "")?linkProperties.channel:"",_advertisingId);
    //         Debug.LogError("Universal Object: " + universalObject.ToJsonString());
    //         Debug.LogError("Link Properties: " + linkProperties.ToJsonString());
    //         // PlugInTest.billingInit(_productId);
    //     }
    // }
    
    public void CallbackWithBranchUniversalObject(BranchUniversalObject universalObject, BranchLinkProperties linkProperties, string error) {
        Debug.LogError("Branch linkProperties : " + linkProperties.channel);
        if (error != null) {
            Debug.LogError("<== BRANCH INIT FAILED ==> : "+error);
            //BranchInit();
        } else {
            Debug.LogError("<== BRANCH INIT COMPLETE ==>");
            PlugInTest.setBranch2Billing((linkProperties.channel != "")?linkProperties.channel:"",_advertisingId);
            Debug.Log("Universal Object: " + universalObject.ToJsonString());
            Debug.Log("Link Properties: " + linkProperties.ToJsonString());
            PlugInTest.billingInit(_productId);
        }
    }

    
    
    // void CallbackWithBranchUniversalObject(BranchUniversalObject buo,BranchLinkProperties linkProps,string error) {
    //     if (error != null) {
    //         System.Console.WriteLine("Error : " + error);
    //     } else if (linkProps.controlParams.Count > 0) {
    //         System.Console.WriteLine("Deeplink params : "+ buo.ToJsonString() + linkProps.ToJsonString());
    //     }
    // }

    public void SendBranceEvent(String eventName)
    {
        Debug.LogError("SendBranceEvent : "+eventName);
        BranchUniversalObject item = new BranchUniversalObject();
        BranchEvent e02 = new BranchEvent (eventName);
        e02.AddCustomData("type", eventName);
        e02.AddContentItem(item);
        Branch.sendEvent (e02);
    }
    
    
    public void ShowCloseBtn(GameObject ob)
    {
        StartCoroutine(showCLoseBtn(ob));
    }

    private IEnumerator showCLoseBtn(GameObject ob)
    {
        yield return new WaitForSeconds(3f);
        ob.SetActive(true);
    }
}
