﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using GuitarApplication;
using TMPro;
using UnityEngine;

public class StoreManager : MonoBehaviour
{
    public GameObject _canvas;
    private string _origineInterfaceName;
    public GameObject _closeBtn;
    public Camera _mainCamera;
    [HideInInspector] public StoreManager _instance;

    public event Action ShowFreeModeInterface;

    public event Action ShowPauseInGameInterface;

    public event Action ShowGuitarSelectionMenuInterface;

    public event Action ShowTrackList;

    private void Awake()
    {
        _instance = this;
    }

    public void ShowStoreFromClickVIPTrack()
    {
        _origineInterfaceName = "track_list";
        ShowStoreScreen();
    }
    
    public void ShowStoreFromClickUnlockAllOnGuitarList()
    {
        _origineInterfaceName = "guitar_selection_menu";
        ShowStoreScreen();
    }

    public void ShowStoreScreen()
    {
        BranceAndFirebaseInit._instance.SendBranceEvent("pay_view2");
        _canvas.GetComponent<Canvas>().enabled = true;
        _canvas.transform.GetChild(0).transform.GetChild(0).transform.GetChild(0).transform.GetChild(1).GetComponent<TMP_Text>().text = "3-day free trial,then "+IAPManager.Instance.price +"/month cancel anytime ";
        BranceAndFirebaseInit._instance.ShowCloseBtn(_closeBtn);
    }
    
    public void HideStoreScreen(bool Close )
    {
        if(Close)
            BranceAndFirebaseInit._instance.SendBranceEvent("pay_close2_click");
       
        _canvas.GetComponent<Canvas>().enabled = false;
        switch (_origineInterfaceName)
        {
            case "guitar_selection_menu":
                ApplicationGraph.GetApplicationRouter().SetPremiumGuitarSelect(false);
                ApplicationGraph.GetApplicationRouter().GetGuitarConfigurationScreen().Awake();
                ApplicationGraph.GetApplicationRouter().GetGuitarConfigurationScreen().Show(_mainCamera);
              
                foreach (var guitarCell in ApplicationGraph.GetApplicationRouter().GetGuitarConfigurationScreen()._guitarCells)
                {
                    guitarCell.UpdateGuitarCell();
                }
                
                   
                break;
            case "track_list":
                ApplicationGraph.GetApplicationRouter().GetTrackCollectionScreen().gameObject.SetActive(value: true);
                ApplicationGraph.GetApplicationRouter().GetTrackCollectionScreen()._presenter._tableView.reloadVisiblesCell();
                break;
        }
        _closeBtn.SetActive(false);
    }
    
    public void SubPurchageBtnClick()
    {
        // BranceAndFirebaseInit._instance.SendBranceEvent("start_trial2_click");
        IAPManager.Instance.PurchageStart(10);
    }

    public void StorePurchageSuccess()
    {
        // BranceAndFirebaseInit._instance.SendBranceEvent("start_trial2");
        IAPManager.Instance.UpdatePremium();
        HideStoreScreen(false);
    }

    public void StorePurchageFailed()
    {
        // PlayerPrefs.SetInt("IsPremium", 1);
        // PlayerPrefs.Save();
        HideStoreScreen(false);
    }
}
