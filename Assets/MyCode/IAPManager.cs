﻿using System;
using System.Collections;
using System.Collections.Generic;
// using EasyMobile;
using UnityEngine;
using UnityEngine.Purchasing;

public class IAPManager : MonoBehaviour, IStoreListener
{
    private IStoreController m_Controller = null;
    public static IAPManager Instance;
    public string price = "$199";
    private int BuyProduct3DaysFreeIndex;

    public string _productId;
    private Product item;
    public bool Init = false;
    public StoreManager _storeManagerScreen;

    private void Awake()
    {
        Instance = this;
       // Debug.LogError();
        intIAP();
    }



    public void intIAP()
    {
        var module = StandardPurchasingModule.Instance();
        module.useMockBillingSystem = true;
        var builder = ConfigurationBuilder.Instance(module);

        // builder.Configure<IGooglePlayConfiguration>().SetPublicKey("MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA2O/9/H7jYjOsLFT/uSy3ZEk5KaNg1xx60RN7yWJaoQZ7qMeLy4hsVB3IpgMXgiYFiKELkBaUEkObiPDlCxcHnWVlhnzJBvTfeCPrYNVOOSJFZrXdotp5L0iS2NVHjnllM+HA1M0W2eSNjdYzdLmZl1bxTpXa4th+dVli9lZu7B7C2ly79i/hGTmvaClzPBNyX+Rtj7Bmo336zh2lYbRdpD5glozUq+10u91PMDPH+jqhx10eyZpiapr8dFqXl5diMiobknw9CgcjxqMTVBQHK6hS0qYKPmUDONquJn280fBs1PTeA6NMG03gb9FLESKFclcuEZtvM8ZwMMRxSLA9GwIDAQAB");

        builder.AddProduct("subscription", ProductType.Subscription, new IDs
        {
            {_productId, GooglePlay.Name}//freecoolguitar.yearly.com
            // {"android.test.item_unavailable", GooglePlay.Name}
        });

        Debug.LogError("<== IAP_INITIALIZE START==>");
        // Now we're ready to initialize Unity IAP.
        UnityPurchasing.Initialize(this, builder);
    }

    public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs purchaseEvent)
    {
        throw new NotImplementedException();
    }

    public void OnPurchaseFailed(Product item, PurchaseFailureReason r)
    {
        Debug.Log("Purchase failed: " + item.definition.id);
        Debug.Log(r);
    }

    public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
    {
        Init = true;
        m_Controller = controller;
        price =  m_Controller.products.all[0].metadata.localizedPriceString;
        Debug.LogError("<== IAP_INITIALIZE SUCCESS ==>");
        Debug.LogError("<== IS PREMIUM : ==> "+ IsPrimeMember());
        
        // var m_Play = extensions.GetExtension<IGooglePlayStoreExtensions>();
        // Dictionary<string, string> dict = m_Play.GetProductJSONDictionary();
        //     item = m_Controller.products.all[0];
        //      // Where(p => p.hasReceipt).Count() > 0;
        //     if (item.receipt != null)
        //     {
        //         if (item.definition.type == ProductType.Subscription)
        //         {
        //             string intro_json = (dict == null || !dict.ContainsKey(item.definition.storeSpecificId)) ? null : dict[item.definition.storeSpecificId];
        //             SubscriptionManager p = new SubscriptionManager(item, intro_json);
        //             SubscriptionInfo info = p.getSubscriptionInfo();
        //             if (info != null)
        //                 Debug.LogError("Intro Price : " + info.getIntroductoryPrice().ToString());
        //             else
        //                 Debug.LogError("Info is null");
        //         }
        //         else
        //         {
        //             Debug.LogError("The product is not a subscription");
        //         }
        //     }
        //     else
        //     {
        //         Debug.LogError("The receipt is null");
        //     }
            
    }

    public void OnInitializeFailed(InitializationFailureReason error)
    {
        Debug.LogError("<== IAP_INITIALIZE FAILED ==>");
        switch (error)
        {
            case InitializationFailureReason.AppNotKnown:
                Debug.LogError("Is your App correctly uploaded on the relevant publisher console?");
                break;
            case InitializationFailureReason.PurchasingUnavailable:
                // Ask the user if billing is disabled in device settings.
                Debug.Log("Billing disabled!");
                break;
            case InitializationFailureReason.NoProductsAvailable:
                // Developer configuration error; check product metadata.
                Debug.Log("No products available for purchase!");
                break;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
       

        _productId = BranceAndFirebaseInit._instance._productId[0];

        // Initialize the IAP module
        
        // InAppPurchasing.InitializePurchasing();
    }

    public void PurchageStart(int index)
    {
        StartCoroutine(checkInternetConnection((isConnected)=>{
            // handle connection status here
            if (isConnected)
            {
                if(m_Controller == null)
                    intIAP();
                
                
                Debug.LogError(" <- PURCHAGE_START ->");
                BuyProduct3DaysFreeIndex = index;
#if UNITY_ANDROID
                PlugInTest.startPurchase(_productId);
#elif UNITY_EDITOR
                StartCoroutine(PurchaseFailedHandlerNew(""));
#endif
            }
            else
            {
                IntroManager.Instance.ShowToast("No Internet connection available");
                Debug.LogError("Internet Not available....");
            }
        }));
        
       
    }
    
    
    IEnumerator checkInternetConnection(Action<bool> action){
        WWW www = new WWW("http://google.com");
        yield return www;
        if (www.error != null) {
            action (false);
        } else {
            action (true);
        }
    } 
    
    
    
    public IEnumerator PurchaseCompletedHandlerNew(String product)
    {
        yield return new WaitForSeconds(1);

        setBranceEvent(_productId);

        Debug.LogError(BuyProduct3DaysFreeIndex + " BuyProduct3DaysFreeIndex ");
        Debug.LogError(_productId + " was purchased for 1 MONTH ");
        Debug.LogError("Purchaged : " + IsPrimeMember());

        if (BuyProduct3DaysFreeIndex <= 9)
        {
            IntroManager.Instance.IntroPanelPurchageSuccess(BuyProduct3DaysFreeIndex);
        }
        else if(BuyProduct3DaysFreeIndex == 10)
        {
            _storeManagerScreen._instance.StorePurchageSuccess();
        }
    }

    
    
    public IEnumerator PurchaseFailedHandlerNew(String product)
    {
        yield return new WaitForSeconds(1);
        Debug.LogError(BuyProduct3DaysFreeIndex + " BuyProduct3DaysFreeIndex ");
        Debug.LogError("The purchase of product " + _productId + " has failed.");


        if (BuyProduct3DaysFreeIndex <= 9)
        {
            IntroManager.Instance.IntroPanelPurchageFailed(BuyProduct3DaysFreeIndex);
        }
        else if(BuyProduct3DaysFreeIndex == 10)
        {
            _storeManagerScreen._instance.StorePurchageFailed();
        }
    }

    
    
    private void setBranceEvent(string product)
    {
        // float pricess = (float)InAppPurchasing.GetProductLocalizedData(product).localizedPrice;
        // string currency = InAppPurchasing.GetProductLocalizedData(product).isoCurrencyCode;
        BranchUniversalObject item = new BranchUniversalObject();
    
        BranchEvent e01 = new BranchEvent(BranchEventType.START_TRIAL);
        e01.SetCurrency(BranchCurrencyType.USD);
        e01.SetDescription(product);
        e01.AddCustomData("ProductId", product);
        e01.AddContentItem(item);
        Branch.sendEvent(e01);
        
        // BranchEvent e02 = new BranchEvent(BranchEventType.PURCHASE);
        // e02.SetCurrency(BranchCurrencyType.USD);
        // e02.SetDescription(product);
        // e02.AddCustomData("ProductId", product);
        // e02.AddContentItem(item);
        // Branch.sendEvent(e02);
    }

    
    
    public void UpdatePremium()
    {
        PlugInTest.IsPremium(_productId);
    }
    
    
    
    public bool IsPrimeMember()
    {
        Debug.LogError("<== IsPrimeMember() CHECK ==> : " + (PlayerPrefs.GetInt("IsPremium",0) == 1));
        // return item.hasReceipt;//InAppPurchasing.IsProductOwned(EM_IAPConstants.Product_product_android_test_item);
        return  (PlayerPrefs.GetInt("IsPremium",0) == 1);//InAppPurchasing.IsProductOwned(EM_IAPConstants.Product_product_android_test_item);
    }
    
    
    
    public void onsuccess()
    {
        StartCoroutine(PurchaseCompletedHandlerNew(""));
    }

    
    
    public void onFailed()
    {
        StartCoroutine(PurchaseFailedHandlerNew(""));
    }

    
    
    
}
